//
//  UserProfileView.swift
//  VLiveShow
//
//  Created by VincentX on 22/12/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit

class UserProfileView: UIView {
    
    @objc @IBOutlet weak var avatarButton: UIButton!
    @objc @IBOutlet weak var usernameLabel: UILabel!
    @objc @IBOutlet weak var genderImageView: UIImageView!
    @objc @IBOutlet weak var followerButton: UIButton!
    @objc @IBOutlet weak var starButton: UIButton!
    @objc @IBOutlet weak var introductionLabel: UILabel!
    @objc @IBOutlet weak var useridLabel: UILabel!
    @objc @IBOutlet weak var voteButton: UIButton!
    @objc @IBOutlet weak var liveButton: UIButton!
    @objc @IBOutlet weak var editButton: UIButton!
    
    var numberOfFollowers: Int = 0
    {
        didSet
        {
            let attrString = NSMutableAttributedString(string: _LS("MINE_FANS"), attributes:
                [
                 NSForegroundColorAttributeName: UIColor.textGray()
                ])
            let numberString = NSAttributedString(string: " \(self.numberOfFollowers)", attributes:
                [
                 NSForegroundColorAttributeName: UIColor.black
                ])
            attrString.append(numberString)
            followerButton.setAttributedTitle(attrString, for: .normal)
        }
    }
    
    var numberOfStarred: Int = 0
    {
        didSet
        {
            let attrString = NSMutableAttributedString(string: _LS("MINE_FOCUS"), attributes:
                [
                 NSForegroundColorAttributeName: UIColor.textGray()
                ])
            let numberString = NSAttributedString(string: " \(self.numberOfStarred)", attributes:
                [
                 NSForegroundColorAttributeName: UIColor.black
                ])
            attrString.append(numberString)
            starButton.setAttributedTitle(attrString, for: .normal)
        }
    }
    
    var numberOfVote: Int = 0
    {
        didSet
        {
            let str = String(format: _LS("VOTE_NUMBER_AND_SUPPORT"), self.numberOfVote)
            voteButton.setTitle(str, for: .normal)
        }
    }
    
    var userId: String?
    {
        didSet
        {
           updateUserIdLabel()
        }
    }
    
    var editable: Bool = false
    {
        didSet
        {
            self.editButton.isHidden = !editable
        }
    }
    
    var location: String?
    {
        didSet
        {
            updateUserIdLabel()
        }
    }
    
    var avatarImage: UIImage?
    {
        set
        {
            self.avatarButton.setImage(newValue, for: .normal)
        }
        
        get
        {
            return self.avatarButton.image(for: .normal)
        }
    }
    
    class func create() -> UserProfileView
    {
        return Bundle.main.loadNibNamed("UserProfileView", owner: nil, options: nil)![0] as! UserProfileView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.shadowColor = UIColor.bgLightGray().cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: -10)
        clearUI()
    }
    
    @IBAction func didPressFollowerButton()
    {
        
    }
    
    @IBAction func didPressStarButton()
    {
        
    }
    
    @IBAction func didTapAvatar()
    {
        
    }
    
    @IBAction func didPressVoteButton()
    {
        
    }
    
    func updateUserIdLabel()
    {
        var str = ""
        if let uid = self.userId
        {
            str = "NO.\(uid)"
        }
        if let loc = self.location
        {
            str.append(" , " + _LS("COMEFROM") + "\(loc)")
        }
        self.useridLabel.text = str
    }
    
    func clearUI()
    {
        avatarButton.setImage(UIImage(named: ""), for: .normal)
        self.usernameLabel.text = ""
        self.genderImageView.image = nil
        self.numberOfFollowers = 0
        self.numberOfStarred = 0
        self.numberOfVote = 0
        self.introductionLabel.text = ""
        self.useridLabel.text = ""
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension UserProfileView
{
    func setUserProfile(userProfile: VLSUserInfoViewModel?)
    {
        guard let usp = userProfile else
        {
            clearUI()
            return
        }
        if usp.userName == nil
        || usp.userName.isEmpty
        {
            self.usernameLabel.text = _LS("MINE_DONOTSETNICKNAME")
        }
        else
        {
            self.usernameLabel.text = usp.userName
        }
        if let avatar = usp.userFaceURL
        {
            self.avatarButton.sd_setImage(with: URL(string: avatar), for: .normal, placeholderImage: UIImage(named: "headshot"), options: [.refreshCached, .retryFailed])
        }
        else
        {
            self.avatarButton.setImage(#imageLiteral(resourceName: "headshot"), for: .normal)
        }
        self.genderImageView.image = nil
        if let gender = usp.sexImageName
        {
            if gender == "MALE"
            {
                self.genderImageView.image = #imageLiteral(resourceName: "sex_male")
            }
            else if gender == "FEMALE"
            {
                self.genderImageView.image = #imageLiteral(resourceName: "sex_female")
            }
        }
        self.numberOfFollowers = usp.fansNumber
        self.numberOfStarred = usp.followNumber
        if let content = usp.userContent,
            !content.isEmpty
        {
            self.introductionLabel.text = content
        }
        else
        {
            self.introductionLabel.text = _LS("MINE_USER_PROFILE_LAZY_NOTHING")
        }
        self.userId = usp.userId
        self.location =  usp.userLocation
        self.numberOfVote = usp.totalVotes.intValue
        self.voteButton.isEnabled = usp.residueVotes.intValue > 0
        if self.voteButton.isEnabled
        {
            self.voteButton.layer.borderColor = UIColor.tagYellow().cgColor
        }
        else
        {
            self.voteButton.layer.borderColor = UIColor.c7c7c7().cgColor
        }
    }
}
