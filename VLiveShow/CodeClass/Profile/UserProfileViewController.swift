//
//  UserProfileViewController.swift
//  VLiveShow
//
//  Created by VincentX on 21/12/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

class UserProfileViewController: VLSBaseViewController, UITableViewDelegate, UITableViewDataSource
{
    
    @IBOutlet weak var tableView: UITableView!;
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var userProfileContainerView: UIView!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var blockButton: UIButton!
    @IBOutlet weak var emptyView: UIView!
    
    weak var profileInfoViewController:ProfileInfoViewController!
    
    @objc var userId: String?
    {
        didSet
        {
            if self.isViewLoaded
            {
                loadUserProfile()
                loadCourseFromServer()
                findRoomFromServer()
            }
        }
    }
    
    @objc var userProfile: VLSUserInfoViewModel?
    {
        set
        {
            if self.isViewLoaded
            {
                self.profileInfoViewController.userProfile = newValue
                self.refreshUI()
            }
        }
        
        get
        {
            if self.isViewLoaded
            {
                return self.profileInfoViewController.userProfile
            }
            return nil
        }
    }
    
    var courses: [VLSCourseModel] = []
    {
        didSet
        {
            if self.isViewLoaded
            {
                self.tableView.reloadData()
                self.checkNeedShowEmpty()
            }
        }
    }
    
    class func create() -> UserProfileViewController
    {
        return UIStoryboard(name: "Profile", bundle: nil).instantiateInitialViewController() as! UserProfileViewController
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "header")
        // NOT implemented sharing profile
        
        let shareBt = UIButton().then {//分享
            $0.setImage(#imageLiteral(resourceName: "share_gray").withRenderingMode(.alwaysTemplate), for: .normal)
            $0.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            $0.addTarget(self, action: #selector(UserProfileViewController.shareAction), for: .touchUpInside)
            $0.tintColor = HEX(COLOR_FONT_FF1130)
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: shareBt)
        
        self.profileInfoViewController = self.childViewControllers[0] as! ProfileInfoViewController
        self.profileInfoViewController.didPressAvatarBlock = {[weak self] in
            self?.didPressAvatarBlock()
        }
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 110
        loadUserProfile()
        loadCourseFromServer()
        findRoomFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.useFusionTheme()
        self.extendedLayoutIncludesOpaqueBars = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return courses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CourseCell", for: indexPath) as! UserProfileCourseCell
        cell.course = courses[indexPath.row]
        return cell
    }
    
    // UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let courseDetailViewController = VLSCourseDetailViewController()
        courseDetailViewController.state = CourseState.course_before
        courseDetailViewController.model = courses[indexPath.row]
        courseDetailViewController.hidesBottomBarWhenPushed = true;
        self.navigationController?.pushViewController(courseDetailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header")
        header?.contentView.backgroundColor = UIColor.white
        let tagTop = 101
        let tagBottom = 102
        if header?.contentView.viewWithTag(tagTop) == nil
        {
            let imageView = UIImageView()
            imageView.tag = tagTop
            imageView.backgroundColor = UIColor(red: 221.0 / 255, green: 221.0 / 255, blue: 221.0 / 255, alpha: 1.0)
            header?.contentView.addSubview(imageView)
            imageView.snp.makeConstraints(
            { (maker) in
                maker.height.equalTo(0.5)
                maker.left.top.right.equalToSuperview()
            })
        }
        
        if header?.contentView.viewWithTag(tagBottom) == nil
        {
            let imageView = UIImageView()
            imageView.tag = tagBottom
            imageView.backgroundColor = UIColor(red: 221.0 / 255, green: 221.0 / 255, blue: 221.0 / 255, alpha: 1.0)
            header?.contentView.addSubview(imageView)
            imageView.snp.makeConstraints(
                { (maker) in
                    maker.height.equalTo(0.5)
                    maker.left.right.equalToSuperview()
                    maker.bottom.equalToSuperview().offset(-0.5)
            })
        }
        
        
        header?.textLabel?.textAlignment = .center
        header?.textLabel?.text = _LS("HIS_COURSES")
        header?.textLabel?.font = UIFont(name: "PingFang SC Regular", size: 14)
        header?.textLabel?.textColor = UIColor.titleColorInLightNavi()
        return header
    }
    
    func refreshUI()
    {
        guard let profile = self.userProfile else
        {
            self.followButton.isEnabled = false
            self.blockButton.isEnabled = false
            return
        }
        let isFollowed = (profile.isAttent == "1")
        let isBlocked = (profile.isBlack == "1")
        
        self.followButton.setImage(isFollowed ? #imageLiteral(resourceName: "ic_yiguanzhu"): #imageLiteral(resourceName: "ic_guanzhu"), for: .normal)
        self.followButton.setTitle(isFollowed ? _LS("MINE_FOCUSED") : _LS("MINE_FOCUS") , for: .normal)
        self.followButton.setTitleColor(isFollowed ? UIColor.c7c7c7() : UIColor.titleColorInLightNavi(), for: .normal)
        self.followButton.isEnabled = !isBlocked
        self.blockButton.isEnabled = !isBlocked
    }
    
    func loadUserProfile()
    {
        if let uid = self.userId
        {
            let index = uid.index(uid.startIndex, offsetBy: 7)
            let uid_ = uid.substring(to: index)
            let callback = ManagerCallBack()
            callback.updateBlock =
            {[weak self] ret in
                self?.userProfile = ret as? VLSUserInfoViewModel
            }
            callback.errorBlock =
            {[weak self] error in
                
            }
            VLSLiveHttpManager.getUserInfo(uid_, callback: callback)
        }
    }
    
    func findRoomFromServer()
    {
        guard let uid = self.userId else
        {
            return
        }
        let callback = ManagerCallBack()
        self.profileInfoViewController.profileView.liveButton.isHidden = true
        callback.updateBlock =
        {[weak self] result in
            if let roomId = result as? String
            {
                //self?.profileInfoViewController.profileView.liveButton.isHidden = false
                self?.profileInfoViewController.roomId = roomId
            }
            else
            {
                self?.profileInfoViewController.profileView.liveButton.isHidden = true
            }
        }
        callback.errorBlock = {_ in
            
        }
        VLSLiveHttpManager.getRoomId(uid, callback: callback)
    }
    
    func loadCourseFromServer()
    {
        guard let uid = self.userId else
        {
            return
        }
        let callback = HttpCallBack()
        callback.doneBlock =
        {[weak self] ret in
            if let responseData = ret as? [String: Any],
                let success = responseData["success"] as? Bool,
                success,
            let resultsDict = responseData["results"] as? [String: Any],
            let list = resultsDict["list"] as? [[String: Any]]
            {
                self?.courses = list.flatMap(
                { (dict) -> VLSCourseModel? in
                    
                    return VLSCourseModel(map: Map(mappingType: .fromJSON, JSON: dict))
                })
            }
            
        }
        callback.errorBlock =
        {[weak self] error in
            
        }
        
        HTTPFacade.getUserCourses(uid, callback: callback)
    }
    
    // MARK: - Follow/Block
    @IBAction func toggleFollow()
    {
        guard let userProfile = self.userProfile else
        {
            return
        }
        let isFollowed = (userProfile.isAttent == "1")
        if (isFollowed)
        {
            cancelFollowing()
        }
        else
        {
            follow()
        }
    }
    
    func follow()
    {
        guard let uidStr = self.userId,
            let uid = Int(uidStr),
            uid != AccountManager.shared().account.uid
            else
        {
            return
        }
        self.showProgressHUD()
        
        let callback = ManagerCallBack()
        callback.updateBlock =
        {[weak self] result in
            self?.userProfile?.isAttent = "1"
            self?.refreshUI()
            self?.hideProgressHUD(afterDelay: 0)
            
        };
        callback.errorBlock =
        {[weak self] _ in
            self?.hideProgressHUD(afterDelay: 0)
        };
        VLSUserTrackingManager.share().trackingUserAction(uidStr, action: "focus_on")
        VLSLiveHttpManager.focus(toUser: uidStr, callback: callback)
    }
    
    func cancelFollowing()
    {
        guard let uidStr = self.userId,
            let uid = Int(uidStr),
            uid != AccountManager.shared().account.uid
            else
        {
            return
        }
        self.showProgressHUD()
        let callback = ManagerCallBack()
        callback.updateBlock =
            {[weak self] result in
                self?.userProfile?.isAttent = "0"
                self?.refreshUI()
                self?.hideProgressHUD(afterDelay: 1)
                
        };
        callback.errorBlock =
            {[weak self] _ in
                self?.hideProgressHUD(afterDelay: 1)
        };
        VLSUserTrackingManager.share().trackingUserAction(uidStr, action: "focus_off")
        VLSLiveHttpManager.cancelFocus(toUser: uidStr, callback: callback)
    }
    
    @IBAction func block()
    {
        guard let uidStr = self.userId,
            let uid = Int(uidStr),
            uid != AccountManager.shared().account.uid
            else
        {
            return
        }
        
        showProgressHUD()
        let callback = ManagerCallBack()
        callback.updateBlock =
        {[weak self]result in
            self?.hideProgressHUD(afterDelay: 0)
            let event = ManagerEvent()
            event.title = _LS("MINE_USER_HEADBLACK")
            self?.success(event)
            self?.userProfile?.isBlack = "1"
            self?.refreshUI()
        }
        callback.errorBlock =
        {[weak self] evt in
            
            self?.hideProgressHUD(afterDelay: 0)
            if let event = evt as? ManagerEvent
            {
                self?.error(event)
            }
        }
        VLSUserTrackingManager.share().trackingUserAction(uidStr, action: "blacklist_on")
        VLSLiveHttpManager.addBlack(uidStr, callback: callback)
    }
    
    func checkNeedShowEmpty()
    {
        let isEmpty = self.courses.count == 0
        self.emptyView.isHidden = !isEmpty
    }
    
    func didPressAvatarBlock()
    {
        if let image = self.profileInfoViewController.profileView.avatarImage
        {
            if let imageInspect = ImageViewInspect(frame: self.view.bounds, andImage: image)
            {
                imageInspect.backgroundColor = UIColor.black
                self.view.addSubview(imageInspect)
            }
        }
    }
    
    //分享
    func shareAction() {
        VLSShareSheetView.shared.shareSheet(view: self.view, item: nil)
        VLSShareSheetView.shared.delegate = self
    }
}

extension UserProfileViewController: VLSShareSheetViewDelegate {
    func xs_actionSheet(_ actionSheet: ShareActionSheet!, clickedButtonIndex index: Int) {
        if index > 5 || !ShareActionSheet.isInstallation(index) {
            self.showAlterviewController(LocalizedString("SHARE_HTML_NO_INSTALL_APP"))
        } else {
            let title = LocalizedString("SHARE_HTML_URL_TITLE") + "-" + String(format: LocalizedString("COURSE_LIST_SHARE_CONTENT"), StringSafty(self.userProfile?.userName))
            let dict = ["coverImageURL": StringSafty(self.userProfile?.userFaceURL), "content": title, "title": title, "overrideTitleInWXTimeline": false, "urlString": StringSafty(String(format: "%@/pages/course/list.html?id=%@", VLIVESHOW_HOST, StringSafty(self.userId)))] as [String : Any]
            self.share(toPlatform: index, messageObject: dict, completion: { (item) in
                actionSheet.dismiss()
                self.shareTrackingLive(after: StringSafty(self.userId), index: index)
            })
        }
    }
}
