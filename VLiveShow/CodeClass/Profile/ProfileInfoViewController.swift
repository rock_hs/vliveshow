//
//  ProfileInfoViewController.swift
//  VLiveShow
//
//  Created by VincentX on 23/12/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit

class ProfileInfoViewController: UIViewController {
    
    @IBOutlet weak var profileView: UserProfileView!
    var roomId: String?
    
    @objc var didPressAvatarBlock: (()->Void)?
    
    @objc var userProfile: VLSUserInfoViewModel?
    {
        didSet
        {
            if self.isViewLoaded
            {
                updateUserProfileView()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didPressFollowersButton()
    {
        let followersVC = VLSFansListViewController()
        if let uidStr = self.userProfile?.userId,
           let uid = Int(uidStr)
        {
            followersVC.userId = uid
            followersVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(followersVC, animated: true)
        }
    }
    
    @IBAction func didPressStarredButton()
    {
        let starredVC = VLSFocusListViewController()
        if let uidStr = self.userProfile?.userId,
            let uid = Int(uidStr)
        {
            starredVC.userId = uid
            starredVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(starredVC, animated: true)
        }
    }
    
    func updateUserProfileView()
    {
        self.profileView.setUserProfile(userProfile: self.userProfile)
    }
    
    // MARK: Voting related
    @IBAction func didPressVoteButton()
    {
        if AccountManager.shared().account.diamondBalance <= 0
        {
            showRechargeAlert()
        }
        else
        {
            showVoteAlert()
        }
    }
    
    func showVoteAlert()
    {
        guard let username = self.userProfile?.userName,
            let residueVotes = self.userProfile?.residueVotes else
        {
            return
        }
        let title = String(format: _LS("VOTE_CONFIRM_TITLE"), username)
        let content = String(format: _LS("VOTE_CONFIRM_CONTENT"), username, residueVotes)
        
        let alertVC = UIAlertController(title: title, message: content, preferredStyle: .alert)
        let okAction = UIAlertAction(title: LocalizedString("VOTE_CONFIRM_DONE"), style: .default, handler:
            {[weak self] (action) in
                if AccountManager.shared().account.diamondBalance <= 0
                {//余额不足
                    self?.showRechargeAlert()
                } else
                { //投票
                    self?.voteAction()
                }
        })
        alertVC.addAction(okAction)
        let cancelAction = UIAlertAction(title: LocalizedString("VOTE_CONFIRM_CANCEL"), style: .cancel, handler: nil)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func showRechargeAlert()
    {
        let alertVC = UIAlertController(title: LocalizedString("VOTE_FAIL_MONEY_TITLE"), message: LocalizedString("VOTE_FAIL_MONEY_CONTENT"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: LocalizedString("VOTE_FAIL_MONEY_DONE"), style: .default, handler:
            { (action) in
                if let paymentVC = UIStoryboard(name: "Payment", bundle: nil).instantiateInitialViewController()
                {
                    paymentVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(paymentVC, animated: true)
                }
        })
        alertVC.addAction(okAction)
        let cancelAction = UIAlertAction(title: LocalizedString("VOTE_FAIL_MONEY_CANCEL"), style: .cancel, handler: nil)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    
    func voteAction()
    {
        guard let uidStr = self.userProfile?.userId else
        {
            return
        }
        let callback = HttpCallBack()
        callback.doneBlock =
            {[weak self] ret in
                var message = LocalizedString("VOTE_SUCCESS_MESSAGE")
                guard let result = ret as? [String: Any] else
                {
                    message = LocalizedString("VOTE_FAIL_MESSAGE")
                    return
                }
                if  let _ = result["success"] as? Bool,
                    let error_code = result["error_code"] as? Int,
                    let voteResults = result["results"] as? [String: Any],
                    let residueVotes = voteResults["residueVotes"] as? Int,
                    let totalVotes = voteResults["totalVotes"] as? Int
                {
                    message = ErrorEvent.error(error_code, msg: StringSafty(result["message"]))
                    if error_code == 0
                    {
                        message = LocalizedString("VOTE_SUCCESS_MESSAGE")
                        //TODO
                        self?.userProfile?.totalVotes = totalVotes as NSNumber!
                        self?.userProfile?.residueVotes = residueVotes as NSNumber!
                        self?.updateUserProfileView()
                        
                    } else if error_code == 34004 {
                        self?.showRechargeAlert() //去充值
                    } else {
                        message = LocalizedString("VOTE_FAIL_CONTENT")
                        // TODO
                    }
                }
        }
        callback.errorBlock =
            {[weak self] err in
                // TODO
                //self?.delegate?.voteRusult!(LocalizedString("VOTE_FAIL_MESSAGE"), dic: nil)
        }
        HTTPFacade.put(APIConfiguration.shared().voteUrl, parameter: ["votedId": uidStr], callback: callback)
    }
    
    @IBAction func enterRoom()
    {
        guard let rid = self.roomId else
        {
            return
        }
        
    }
    
    @IBAction func didPressEditingButton()
    {
        let editVC = MySettingViewController()
        editVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(editVC, animated: true)
    }

    @IBAction func didPressAvatarButton()
    {
        self.didPressAvatarBlock?()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
