//
//  UserProfileCourseCell.swift
//  VLiveShow
//
//  Created by VincentX on 21/12/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit

class UserProfileCourseCell: UITableViewCell
{
    
    var course: VLSCourseModel?
    {
        didSet
        {
            didSetCourse()
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var enterRoomButton: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didPressEnterRoom()
    {
        
    }
    
    func didSetCourse()
    {
        guard let courseModel = self.course else
        {
            coverImageView.image = nil
            tagLabel.text = ""
            dateLabel.text = ""
            timeLabel.text = ""
            return
        }
        if let coverImage = courseModel.picCover,
            let url = URL(string: coverImage)
        {
            coverImageView.sd_setImage(with: url, placeholderImage: UIImage(), options: .refreshCached)
        }
        else
        {
            coverImageView.image = nil
        }
        titleLabel.text = courseModel.courseName
        tagLabel.text = courseModel.courseTypeName
        let startInterval = courseModel.nextLessonStartTime.doubleValue
        let endInterval = courseModel.nextLessonEndTime.doubleValue
        dateLabel.text = Date.dateStr(startInterval, formatter: "yyyy年M月d日 EEE")
        timeLabel.text = Date.dateStr(startInterval, formatter: "aa hh:mm") + " - " + Date.dateStr(endInterval, formatter: "hh:mm")
        enterRoomButton.isHidden = courseModel.roomId == nil
    }

}
