//
//  VLSGifManager.h
//  VLiveShow
//
//  Created by rock on 2017/1/19.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSGifManager : NSObject

+ (void)downloadGif;

@end
