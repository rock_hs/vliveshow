//
//  UINavigationBarExtension.swift
//  VLiveShow
//
//  Created by VincentX on 22/12/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import Foundation

extension UINavigationBar
{
    @objc func useRedTheme()
    {
        self.setBackgroundImage(UIImage(from: UIColor.red), for: .topAttached, barMetrics: .default)
        self.tintColor = HEX(COLOR_BG_FFFFFF)
        self.titleTextAttributes = [NSFontAttributeName : UIFont.systemFont(ofSize: 17),  NSForegroundColorAttributeName : HEX(COLOR_FONT_FFFFFF)]
        self.isTranslucent = false
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    @objc func useLightTheme()
    {
        self.tintColor = UIColor.titleColorInLightNavi()
        var attrDict = self.titleTextAttributes
        if attrDict == nil
        {
            attrDict = [NSFontAttributeName: UIFont.systemFont(ofSize: 17)]
        }
        self.setBackgroundImage(UIImage(from: UIColor.bgLightGray()), for: .topAttached, barMetrics: .default)
        self.backgroundColor = UIColor.bgLightGray()
        attrDict![NSForegroundColorAttributeName] = UIColor.titleColorInLightNavi()
        self.titleTextAttributes = attrDict
        self.isTranslucent = false
        UIApplication.shared.statusBarStyle = .default
    }
    
    @objc func useFusionTheme()
    {
        self.setBackgroundImage(UIImage(), for: .topAttached, barMetrics: .default)
        self.backgroundColor = UIColor.clear
        self.tintColor = UIColor.titleColorInLightNavi()
        self.isTranslucent = true
        self.shadowImage = UIImage()
        UIApplication.shared.statusBarStyle = .default
    }
    
    @objc func useWhiteTheme()
    {
        self.setBackgroundImage(UIImage(from: UIColor.white), for: .topAttached, barMetrics: .default)
        self.tintColor = HEX(COLOR_BG_FFFFFF)
        self.titleTextAttributes = [NSFontAttributeName : UIFont.systemFont(ofSize: 17),  NSForegroundColorAttributeName : HEX(COLOR_FONT_FFFFFF)]
        self.isTranslucent = false
        UIApplication.shared.statusBarStyle = .lightContent
    }
}
