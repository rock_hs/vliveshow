//
//  VLSGifManager.m
//  VLiveShow
//
//  Created by rock on 2017/1/19.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

#import "VLSGifManager.h"

@implementation VLSGifManager

+ (void)downloadGif {
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(NSDictionary* result)
    {
        [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"version"] forKey:@"GIFTVERSION"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        GiftModel * model = [[GiftModel alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [model setGiftPlist:[result objectForKey:@"list"]];
        });
        NSArray * deletedList = [result objectForKey:@"deletedList"];
        if (deletedList.count > 0) {
            [model deletedGiftImg:[result objectForKey:@"deletedList"]];
        }
    };
    callback.errorBlock = ^(id result)
    {
        NSLog(@"result -- %@",result);
    };
    [VLSLiveHttpManager getGiftList:callback];
}

@end
