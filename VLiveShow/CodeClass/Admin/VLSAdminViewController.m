//
//  VLSAdminViewController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAdminViewController.h"
#import "SelectOnLineViewController.h"
#import "VLSVoiceViewController.h"

@implementation VLSAdminViewController
{
    //交换位置用的参数
    VLSVideoItem *moveItem;
    CGPoint point;
    NSMutableArray *requestIds;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
   
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    longPress.minimumPressDuration = 0.7;
    [self.contentView addGestureRecognizer:longPress];
}

#pragma mark - Rotate
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

-(BOOL)shouldAutorotate {
    return NO;
}

//继承自父类 点击屏幕隐藏弹出的view
- (void)touchOnLiveBaseView:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchOnLiveBaseView:touches withEvent:event];
}

#pragma mark - VLSMessageManagerDelegate
#pragma mark - 连线部分

//收到更新主播信息view viewmode为空删除嘉宾；else 添加这个人;
- (void)receiveUpdataAnchorView:(VLSVideoViewModel *)viewModel
{
    if (viewModel == nil) {
        self.contentView.guestView.hidden = YES;
        [self.contentView.anchor setAnchorType:AnchorOnScreen];
    }else{
        self.contentView.guestView.hidden = NO;
        [self.contentView.guestView setGuestID:viewModel.userId];
        [self.contentView.anchor setAnchorType:GuestOnScreen];
    }
}

- (void)videoViewUpdateAnchorView:(VLSVideoViewModel*)model {
    [self receiveUpdataAnchorView:model];
}

#pragma mark - VLSVideoViewDelegate 视频交换位置
- (void)videoViewReplaceUser:(VLSVideoViewModel *)fromModel toUser:(VLSVideoViewModel *)toModel
{
    NSLog(@"replace");
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    callback.errorBlock = ^(id result){
        [self error:result];
    };
    [[VLSAVMultiManager sharedManager] anchorReplacePositionFrom:fromModel.userId to:toModel.userId callback:callback];
    
}

//声音点击
- (void)bottomVoiceClicked:(UIButton *)sender
{
    VLSVoiceViewController *voice = [[VLSVoiceViewController alloc] init];
    voice.videoViews = self.videoView.subviews;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:voice];
    nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    nav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:nav animated:NO completion:^{
    }];
}



// 好友
- (void)bottomFriendClicked:(id)sender
{
    UIButton *button = (UIButton *)sender;
    for (UIView *view in button.subviews) {
        if (view.tag == 1001) {
            [view removeFromSuperview];
        }
    }
    SelectOnLineViewController *SelectVC = [[SelectOnLineViewController alloc]init];
    [self.navigationController pushViewController:SelectVC animated:YES];
}

- (void)longPress:(UILongPressGestureRecognizer *)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan){
        CGPoint point2 = [longPress locationInView:self.videoView];
        moveItem = [self chectPoint:point2];
        if (moveItem) {
            point = [longPress locationInView:moveItem];
            self.contentView.hidden = YES;
            [self.videoView exchangeSubviewAtIndex:[self.videoView.subviews indexOfObject:moveItem] withSubviewAtIndex:[[self.videoView subviews] count] - 1];
            [self.videoView moveBeganWidthItem:moveItem];
        }
        
    }else if (longPress.state == UIGestureRecognizerStateChanged){
        CGPoint movePoint = [longPress locationInView:self.videoView];
        if (moveItem) {
            [moveItem setFrame:CGRectMake( movePoint.x - point.x, movePoint.y - point.y, moveItem.frame.size.width, moveItem.frame.size.height)];
            [self.videoView moveChangeWidthItem:moveItem];
        }
    }else if (longPress.state == UIGestureRecognizerStateEnded){
        if (moveItem) {
            self.contentView.hidden = NO;
            
            CGPoint endPoint = [longPress locationInView:self.videoView];
            CGFloat newX = endPoint.x - point.x ;
            CGFloat newY = endPoint.y - point.y ;
            [self.videoView moveEndWidthItem:moveItem newPoint:CGPointMake(newX, newY)];
        }
    }
}

- (VLSVideoItem *)chectPoint:(CGPoint)checkPoint
{
    return [super chectPoint:checkPoint];
}

@end
