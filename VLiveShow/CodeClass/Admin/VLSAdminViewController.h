//
//  VLSAdminViewController.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMultiLiveViewController.h"

@interface VLSAdminViewController : VLSMultiLiveViewController
@property (nonatomic, assign) KLiveRoomOrientation orientation;
@end
