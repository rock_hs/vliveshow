//
//  HostApplictionCellModel.h
//  VLiveShow
//
//  Created by Davis on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HostApplictionCellModel : NSObject

@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *content;
@property(nonatomic,copy)NSString *key;
@property(nonatomic,copy)NSString *placeholder;

@end
