//
//  HostApplicationModel.h
//  VLiveShow
//
//  Created by Davis on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HostApplicationModel : NSObject



#pragma mark - baseInfoMessage
@property(nonatomic, copy)NSString *applicationId;
@property(nonatomic,copy)NSString *name;

/**
 *
 MALE/FEMALE
 */
@property(nonatomic,copy)NSString *gender;
@property(nonatomic,copy)NSString *phone;
@property(nonatomic,strong)NSDictionary *photo;

@property(nonatomic,strong)NSDictionary *baseInfoForApi;

//@property(nonatomic,copy)NSString *front;
//@property(nonatomic,copy)NSString *back;
//@property(nonatomic,copy)NSString *hold;
#pragma mark - 技能

@property(nonatomic,strong)NSArray *skills;

#pragma mark - 生活

@property(nonatomic,strong)NSArray *lifePhotos;

@end
