//
//  HostApplyCertModel.h
//  VLiveShow
//
//  Created by Davis on 16/8/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HostApplyCertModel : NSObject

@property(nonatomic, copy)NSString *content;
@property(nonatomic, strong)UIImage *image;
@property(nonatomic, copy)NSString *url;
@property(nonatomic, copy)NSString *key;

@end
