//
//  HostApplicationBaseInfoModel.h
//  VLiveShow
//
//  Created by Davis on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HostApplicationBaseInfoModel : NSObject

@property(nonatomic,copy)NSString *name;

/**
 *
    MALE/FEMALE
 */
@property(nonatomic,copy)NSString *gender;

@property(nonatomic,copy)NSString *phone;

@property(nonatomic,copy)NSString *front;
@property(nonatomic,copy)NSString *back;
@property(nonatomic,copy)NSString *hold;

@end
