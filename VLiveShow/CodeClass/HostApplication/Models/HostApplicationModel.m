//
//  HostApplicationModel.m
//  VLiveShow
//
//  Created by Davis on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostApplicationModel.h"

@implementation HostApplicationModel

-(id)init
{
    self = [super init];
    
    self.photo = [NSDictionary new];
    
    return self;
}


- (NSDictionary *)baseInfoForApi
{
    NSMutableDictionary *d =  [NSMutableDictionary new];
    [d setValue:self.name forKey:@"name"];
    [d setValue:self.photo forKey:@"photo"];
    [d setValue:self.gender forKey:@"gender"];
    [d setValue:self.phone forKey:@"phone"];
    if (_applicationId) {
        
        [d setValue:_applicationId forKey:@"applicationId"];

    }
    _baseInfoForApi = d.copy;
    return _baseInfoForApi;
}

@end
