//
//  AttributedString.m
//  PalmDoctorPT
//
//  Created by caiming on 16/1/8.
//  Copyright © 2016年 kangmeng. All rights reserved.
//

#import "AttributedString.h"

@implementation AttributedString

+ (NSAttributedString *)attributeWithLinkTextViewItems:(NSArray *)linkTextViewItems
{
    NSMutableAttributedString *mAttributeString = [[NSMutableAttributedString alloc]initWithString:@""];
    
    for (CMLinkTextViewItem *item in linkTextViewItems) {
        
        [mAttributeString appendAttributedString:[item attributeStringNormal]];
    }
    return mAttributeString;
}

@end


@interface CMLinkTextViewItem()

@property(nonatomic, assign,readwrite)NSRange textRange;

@end

@implementation CMLinkTextViewItem

- (NSAttributedString *)attributeStringNormal;
{
    UIColor *textColor = self.textColor;
    NSRange range = NSMakeRange(0, self.textContent.length);
    NSMutableAttributedString *sub = [[NSMutableAttributedString alloc]initWithString:self.textContent];
    [sub addAttribute:NSForegroundColorAttributeName value:textColor range:range];
    [sub addAttribute:NSFontAttributeName value:self.textFont range:range];
    

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = _textAlignment;
    if (_lineSpacing>0)
    {
        paragraphStyle.lineSpacing = _lineSpacing;
    }
    [sub addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    
    return sub;
}

@end