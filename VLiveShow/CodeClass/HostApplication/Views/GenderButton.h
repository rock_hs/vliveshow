//
//  GenderButton.h
//  VLiveShow
//
//  Created by Davis on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>


@class GenderButton;
@protocol GenderButtonDelegate <NSObject>

- (void)didSelectHostGenderButton:(GenderButton *)button;

@end

@interface GenderButton : UIView

@property(nonatomic, weak)id<GenderButtonDelegate> delegate;

/**
 *  设置 GenderButton 类型
 *
 *  @param gender gender male 表示男   female 女 isChoose YES 为选中， NO 为未选中
 */
-(void)setType:(NSString *)gender isChoose:(BOOL )isChoose;



@end
