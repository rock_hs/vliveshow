//
//  HostApplictionTableViewCell.m
//  VLiveShow
//
//  Created by Davis on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostApplictionTableViewCell.h"
#import "GenderButton.h"


@interface HostApplictionTableViewCell ()<GenderButtonDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UITextField *inputTF;
@property (weak, nonatomic) IBOutlet GenderButton *maleButton;
@property (weak, nonatomic) IBOutlet GenderButton *femaleButton;
@property (weak, nonatomic) IBOutlet UIView *line;

@property (weak, nonatomic) HostApplictionCellModel *model;

@end

@implementation HostApplictionTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    _maleButton.delegate = self;
    _maleButton.tag = 0;
    _femaleButton.delegate = self;
    _femaleButton.tag = 1;
    _inputTF.delegate = self;
}

- (void)reloadData:(HostApplictionCellModel *)model
{
    
    _model = model;
    
    _titleLab.text = model.title;
    
    _maleButton.hidden = YES;
    _femaleButton.hidden = YES;
    _inputTF.hidden = YES;
    
    if ([model.key isEqualToString:@"gender"])
    {
        _maleButton.hidden = NO;
        _femaleButton.hidden = NO;
        
        BOOL maleChoose = [model.content isEqualToString:@"MALE"];
        [_maleButton setType:@"male" isChoose:maleChoose];
        
        BOOL frmaleChoose = [model.content isEqualToString:@"FEMALE"];
        [_femaleButton setType:@"female" isChoose:frmaleChoose];
        
        
    }else
    {
        _inputTF.hidden = NO;
        
        if (model.content.length>0) {
            
            _inputTF.text = model.content;

        }
        _inputTF.placeholder = model.placeholder;
        
    }
    
    _line.hidden = [model.key isEqualToString:@"phone"];
    _inputTF.keyboardType = [model.key isEqualToString:@"phone"]? UIKeyboardTypeNumberPad:UIKeyboardTypeDefault;
    
}

#pragma mark  - GenderButtonDelegate

- (void)didSelectHostGenderButton:(GenderButton *)button
{
    if (button.tag == 0) {
        
        _model.content = @"MALE";
        
    }else
    {
        _model.content = @"FEMALE";
    }
    
    BOOL maleChoose = [_model.content isEqualToString:@"MALE"];
    [_maleButton setType:@"male" isChoose:maleChoose];
    
    BOOL frmaleChoose = [_model.content isEqualToString:@"FEMALE"];
    [_femaleButton setType:@"female" isChoose:frmaleChoose];
    
    if ([self.delegate respondsToSelector:@selector(hostApplictionTableViewCellValueChange)]) {
        
        [self.delegate hostApplictionTableViewCellValueChange];
    }
    
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _model.content = textField.text;
    if ([self.delegate respondsToSelector:@selector(hostApplictionTableViewCellValueChange)]) {
        
        [self.delegate hostApplictionTableViewCellValueChange];
    }
}

@end
