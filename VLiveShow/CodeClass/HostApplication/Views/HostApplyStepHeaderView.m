//
//  HostApplyStepHeaderView.m
//  VLiveShow
//
//  Created by Davis on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostApplyStepHeaderView.h"

@interface HostApplyStepHeaderView ()


@end

@implementation HostApplyStepHeaderView

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self addSubview:self.contentLab];
    }
    
    return self;
}


- (UILabel*)contentLab
{
    if (_contentLab) {
        
        return _contentLab;
    }
    _contentLab = [[UILabel alloc]initWithFrame:CGRectMake(21, 20, 200, 13)];
    return _contentLab;
}

@end
