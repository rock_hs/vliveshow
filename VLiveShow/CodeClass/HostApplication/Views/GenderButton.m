//
//  GenderButton.m
//  VLiveShow
//
//  Created by Davis on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "GenderButton.h"

@interface GenderButton ()

@property (strong, nonatomic)  UIImageView *iconImageV;
@property (strong, nonatomic)  UILabel *contentLab;
@property (strong, nonatomic)  NSDictionary *imageMessage;
@property (strong, nonatomic)  UITapGestureRecognizer *tap;

@end

@implementation GenderButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initSubviews];
    
}

-(void)setType:(NSString *)gender isChoose:(BOOL )isChoose
{
    NSString *isChooseStr = isChoose?@"1":@"0";
    NSString *imageName = [[self.imageMessage objectForKey:gender] objectForKey:isChooseStr];
    _iconImageV.image = [UIImage imageNamed:imageName];
    
    if ([gender isEqualToString:@"male"]) {
        
        _contentLab.text = LocalizedString(@"MINE_PERSONAL_MALE");
        
    }else
    {
        _contentLab.text = LocalizedString(@"MINE_PERSONAL_FEMALE");
    }
    
    UIColor *color = isChoose?[UIColor colorFromHexRGB:@"ff1130"]:[UIColor colorFromHexRGB:@"c6c6c6"];
    _contentLab.textColor = color;
    
}

#pragma mark - onTap

- (void)onTap
{
    if ([self.delegate respondsToSelector:@selector(didSelectHostGenderButton:)]) {
        
        [self.delegate didSelectHostGenderButton:self];
    }
}


#pragma mark - initSubviews

- (void)initSubviews
{
    [self addSubview:self.iconImageV];
    [self addSubview:self.contentLab];
    [self addGestureRecognizer:self.tap];
    
    self.iconImageV.sd_layout.leftSpaceToView(self,0).centerYEqualToView(self).widthIs(16).heightIs(16);
    self.contentLab.sd_layout.leftSpaceToView(self, 24).centerYEqualToView(self).heightIs(20).rightSpaceToView(self,4);
}


- (UIImageView *)iconImageV
{
    if (_iconImageV == nil) {
        
        _iconImageV = [[UIImageView alloc]init];
        _iconImageV.image = [UIImage imageNamed:@"ic_boy_nor"];
        
    }
    return _iconImageV;
}

- (UILabel *)contentLab
{
    if (_contentLab == nil) {
     
        _contentLab = [[UILabel alloc]init];
        _contentLab.font = [UIFont systemFontOfSize:16];
    }
    return _contentLab;
}

- (NSDictionary *)imageMessage
{
    if (nil == _imageMessage) {
        
        _imageMessage = @{
                          @"male":@{
                                  @"0":@"ic_boy_nor",
                                  @"1":@"ic_boy_choose",
                          },
                          @"female":@{
                                  @"0":@"ic_girl_nor",
                                  @"1":@"ic_girl_choose",
                                  }
        
        };
        
    }
    
    
    return _imageMessage;
}

- (UITapGestureRecognizer *)tap
{
    if (_tap) {
        
        return _tap;
    }
    
    _tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTap)];
    
    return _tap;
}

@end
