//
//  HostApplyProgressView.m
//  VLiveShow
//
//  Created by Davis on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostApplyProgressView.h"

@interface HostApplyProgressView ()

@property(nonatomic,strong)UIView *progressView;

@end

@implementation HostApplyProgressView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.layer.masksToBounds = YES;
        [self addSubview:self.progressView];
    }
    
    self.backgroundColor =  [UIColor colorFromHexRGB:@"d8d8d8"];
    
    return self;
}

- (void)setProgressValue:(float)progressValue
{
    _progressValue = progressValue;
    
    CGRect rect = CGRectMake(-1, 0, _progressValue*self.frame.size.width+2, self.frame.size.height);
    
    _progressView.frame = rect;
    
}


- (UIView *)progressView
{
    if (_progressView) {
        
        return _progressView;
    }
    _progressView = [[UIView alloc]initWithFrame:self.bounds];
    _progressView.backgroundColor = [UIColor colorFromHexRGB:@"fe132f"];
    _progressView.layer.masksToBounds = YES;
    _progressView.layer.cornerRadius = 1;
    return _progressView;
}


@end
