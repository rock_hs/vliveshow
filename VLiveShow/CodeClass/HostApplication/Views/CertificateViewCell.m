//
//  CertificateViewCell.m
//  Application
//
//  Created by tom.zhu on 16/8/16.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import "CertificateViewCell.h"
#import "UIView+gradient.h"
#import "HostApplyCertModel.h"

@implementation CertificateViewCell
{
    UIButton *_closeButton;
    HostApplyCertModel *_model;
}
- (void)setType:(CerType)type {
    _type = type;
    if (type == type_cer) {
        [self loadView_cer];
    }else {
        [self loadView_life];
    }
}

- (void)setgradient:(float)progress {
    [_imageView viewGradientWithStartPosition:0
                                   endPositon:1
                                     position:GRADIENT_AXIS_VERTICAL
                              backgroundColor:[UIColor clearColor]
                                    maskColor:[[UIColor blackColor] colorWithAlphaComponent:.7f]
                                     duration:.5f
                                     progress:progress];
}

- (void)loadView_cer {
    if (_loadView) {
        [_loadView removeFromSuperview];
        _loadView = nil;
    }
    if (!_loadView) {
        _loadView = [UIButton buttonWithType:UIButtonTypeCustom];
        _loadView.adjustsImageWhenDisabled = NO;
        _loadView.frame = self.bounds;
        [_loadView addTarget:self action:@selector(loadClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_loadView];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_add"]];
        imageView.frame = CGRectMake((CGRectGetWidth(self.bounds) - CGRectGetWidth(imageView.bounds)) / 2, 30, imageView.bounds.size.width, imageView.bounds.size.height);
        [_loadView addSubview:imageView];
        UILabel *label = [UILabel new];
        [label setFont:[UIFont systemFontOfSize:14]];
        //TODO:Hex color
        [label setTextColor:[UIColor lightGrayColor]];
        //TODO:国际化
        [label setText:LocalizedString(@"idcard_image")];
        [label sizeToFit];
        [label setFrame:CGRectMake((CGRectGetWidth(self.bounds) - CGRectGetWidth(label.bounds)) / 2, self.frame.size.height - label.bounds.size.height - 19, label.bounds.size.width, label.bounds.size.height)];
        [_loadView addSubview:label];
    }
}

- (void)loadView_life {
    if (_loadView) {
        [_loadView removeFromSuperview];
        _loadView = nil;
    }
    if (!_loadView) {
        _loadView = [UIButton buttonWithType:UIButtonTypeCustom];
        _loadView.adjustsImageWhenDisabled = NO;
        _loadView.frame = self.bounds;
        [_loadView setImage:[UIImage imageNamed:@"ic_life"] forState:UIControlStateNormal];
        _loadView.adjustsImageWhenHighlighted = NO;
        [_loadView addTarget:self action:@selector(loadClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_loadView];
    }
}

- (instancetype)initWithFrame:(CGRect)frame type:(CerType)type {
    if (self = [super initWithFrame:frame]) {
        if (type == type_cer) {
            [self loadView_cer];
        }else {
            [self loadView_life];
        }
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self loadView_cer];
    }
    return self;
}

- (void)setModel:(HostApplyCertModel *)model {
    _model = model;
        UIImage *image = model.image ?: nil;
        NSString *urlStr = model.url ?: nil;
    if (image) {
        [self setImage:image];
    }else if (urlStr) {
        [self setimageUrl:urlStr];
    }
    if (nil == urlStr) {
        [self setgradient:0];
    }else {
        [self setgradient:1];
    }
}

- (void)clearView {
    [_imageView removeFromSuperview];
    [_closeButton removeFromSuperview];
}

- (void)prepareImageView {
    _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _imageView.contentMode = UIViewContentModeScaleToFill;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClicked:)];
    _imageView.userInteractionEnabled = YES;
    [_imageView addGestureRecognizer:tap];
    [self.contentView addSubview:_imageView];
    _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _closeButton.frame = CGRectMake(self.bounds.size.width - CLOSEBUTTON_WIDTH, 0, CLOSEBUTTON_WIDTH, CLOSEBUTTON_WIDTH);
    [_closeButton setImage:[UIImage imageNamed:@"ic_cancel"] forState:UIControlStateNormal];
    [self.contentView addSubview:_closeButton];
    [_closeButton addTarget:self action:@selector(closeBtnClicked) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setImage:(UIImage*)image {
    [self prepareImageView];
    _imageView.image = image;
}

- (void)setimageUrl:(NSString*)url {
    [self prepareImageView];
    [_imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"loading_img"]];
}

- (void)closeBtnClicked {
    if (self.closeBlock) {
        self.closeBlock(self);
    }
}

- (void)loadClicked {
    if (self.loadBlock) {
        self.loadBlock();
    }
}

- (void)imageClicked:(UITapGestureRecognizer*)gesture {
    if (self.showimageBlock) {
        self.showimageBlock((UIImageView*)gesture.view);
    }
}

@end