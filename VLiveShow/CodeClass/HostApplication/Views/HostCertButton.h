//
//  HostCertButton.h
//  VLiveShow
//
//  Created by Davis on 16/8/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HostApplyCertModel.h"

@class HostCertButton;
@protocol HostCertButtonDelegate <NSObject>

- (void)didSelectHostCertButton:(HostCertButton *)button;
- (void)shouldDeleteHostImageCertButton:(HostCertButton *)button;


@end

@interface HostCertButton : UIView

@property(nonatomic, weak)id<HostCertButtonDelegate> delegate;


- (void)reloadData:(HostApplyCertModel*)model;

@end
