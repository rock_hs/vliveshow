//
//  HostApplictionTableViewCell.h
//  VLiveShow
//
//  Created by Davis on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HostApplictionCellModel.h"

@protocol HostApplictionTableViewCellDelegate <NSObject>

- (void)hostApplictionTableViewCellValueChange;

@end

@interface HostApplictionTableViewCell : UITableViewCell

@property(nonatomic,weak)id<HostApplictionTableViewCellDelegate> delegate;

- (void)reloadData:(HostApplictionCellModel*)model;

@end

