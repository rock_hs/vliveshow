//
//  XYTag.h
//  AntsSportApp
//
//  Created by Ants on 15/12/16.
//  Copyright © 2015年 Xiaoyi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    TagNormal,
    TagSpecial,
}TagType;

@interface VLSTag : NSObject
@property (nonatomic, strong) NSString *text;

@property (nonatomic, strong) NSString *id_;

@property (nonatomic, strong) UIColor *textNorColor;

@property (nonatomic, strong) UIColor *textSelColor;

@property (nonatomic, assign) BOOL selected;

@property (nonatomic, strong) UIColor *bgColor;

@property (nonatomic, assign) CGFloat cornerRadius;

@property (nonatomic, strong) UIFont *font;

@property (nonatomic, strong) NSString *tags;   //最新列表 跳转tag页面请求网络需要的参数而已

@property (nonatomic, assign) TagType type;

@property (nonatomic, assign) CGFloat widthExpand;

@property (nonatomic, assign) CGFloat heightExpand;

//上下左右的缝隙
@property (nonatomic) CGFloat inset;

@property (nonatomic, strong) id target;

@property (nonatomic) SEL action;

- (instancetype)initWithText:(NSString *)text;

@end
