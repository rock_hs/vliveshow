//
//  XYTagButton.m
//  AntsSportApp
//
//  Created by Ants on 15/12/16.
//  Copyright © 2015年 Xiaoyi. All rights reserved.
//

#import "VLSTagButton.h"

@implementation VLSTagButton

- (CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];
//    size.width += 10;
    return size;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"selected"]) {
        self.selected = [change[@"new"] boolValue];
        self.layer.borderColor = self.selected ? RGB16(COLOR_FONT_FF1130).CGColor : [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 1;
    }
}

@end
