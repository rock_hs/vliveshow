//
//  HostApplyStepHeaderView.h
//  VLiveShow
//
//  Created by Davis on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HostApplyStepHeaderView : UITableViewHeaderFooterView


@property(nonatomic,strong)UILabel *contentLab;

@end
