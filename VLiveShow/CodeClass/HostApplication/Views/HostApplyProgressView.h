//
//  HostApplyProgressView.h
//  VLiveShow
//
//  Created by Davis on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HostApplyProgressView : UIView

/**
 *  进度 0-1
 */
@property(nonatomic,assign)float progressValue;

@end
