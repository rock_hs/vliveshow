//
//  HostApplicationCertCell.h
//  VLiveShow
//
//  Created by Davis on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HostApplicationCertCell;
@class HostCertButton;
@protocol HostApplicationCertCellDelegate <NSObject>

- (void)hostApplicationCertCell:(HostApplicationCertCell*)aCell didSelectIndex:(NSInteger)index;

- (void)shouldhostDeleteImageInCell:(NSInteger)index;

@end

@interface HostApplicationCertCell : UITableViewCell

@property(nonatomic, weak)id<HostApplicationCertCellDelegate> delegate;
@property(nonatomic, strong) NSArray<HostCertButton*> *buttonArray;

- (void)reloadData:(NSArray *)data;

@end
