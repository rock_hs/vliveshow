//
//  XYTagButton.h
//  AntsSportApp
//
//  Created by Ants on 15/12/16.
//  Copyright © 2015年 Xiaoyi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VLSTag;
@interface VLSTagButton : UIButton
@property (nonatomic, strong) VLSTag *xyTag;
@end
