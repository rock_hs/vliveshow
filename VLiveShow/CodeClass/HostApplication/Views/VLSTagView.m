//
//  XYTagView.m
//  AntsSportApp
//
//  Created by Ants on 15/12/16.
//  Copyright © 2015年 Xiaoyi. All rights reserved.
//

#import "VLSTagView.h"

@interface VLSTagView ()
@property (nonatomic, strong) NSMutableArray *tags;
@property (assign) CGFloat intrinsicHeight;
@end

@implementation VLSTagView

-(void)dealloc {
    for (VLSTagButton *btn in self.tags) {
        [btn.xyTag removeObserver:btn forKeyPath:@"selected"];
    }
}

-(CGSize)intrinsicContentSize {
    return CGSizeMake(self.frame.size.width, self.intrinsicHeight);
}

- (void)addTag:(VLSTag *)tag
{
    VLSTagButton *btn = [[VLSTagButton alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    btn.xyTag = tag;
    btn.adjustsImageWhenHighlighted = NO;
    [btn setTitle:tag.text forState:UIControlStateNormal];
    [btn.titleLabel setFont:tag.font];
    [btn setBackgroundColor:tag.bgColor];
    [btn setTitleColor:tag.textNorColor forState:UIControlStateNormal];
    [btn setTitleColor:tag.textSelColor forState:UIControlStateSelected];
    [btn setSelected:tag.selected];
    [btn addTarget:tag.target action:tag.action forControlEvents:UIControlEventTouchUpInside];
    [tag addObserver:btn forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
    
    CGSize size;
    CGSize constraintSize = CGSizeMake(MAXFLOAT, MAXFLOAT);
#ifdef __IPHONE_7_0
    NSMutableParagraphStyle* paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    [paragraphStyle setLineBreakMode:NSLineBreakByCharWrapping];
    
    NSDictionary* stringAttributes = @{NSFontAttributeName: tag.font,
                                       NSParagraphStyleAttributeName: paragraphStyle};
    size = [tag.text boundingRectWithSize: constraintSize
                                  options: NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                               attributes: stringAttributes
                                  context: nil].size;
#else
    size = [tag.text sizeWithFont: tag.font
                constrainedToSize: constraintSize
                    lineBreakMode: NSLineBreakByCharWrapping];
#endif
    //no need to specific button‘s margin
    
    CGFloat i = tag.widthExpand;
    size.width  += i * 2;
    
    i = tag.heightExpand;
    size.height  += i * 2;

    btn.layer.cornerRadius = tag.cornerRadius;
    [btn.layer setMasksToBounds:YES];
    
    //  CGSize size = btn.intrinsicContentSize;
    CGRect r = CGRectMake(0, 0, size.width, size.height);
    [btn setFrame:r];
    
    [self.tags addObject:btn];
    
    [self rearrangeTags];
}

#pragma mark - Tag removal

- (void)removeTagText:(NSString *)text
{
    VLSTagButton *b = nil;
    for (VLSTagButton *t in self.tags) {
        if([text isEqualToString:t.titleLabel.text])
        {
            b = t;
        }
    }
    
    if(!b)
    {
        return;
    }
    
    [b removeFromSuperview];
    [self.tags removeObject:b];
    [self rearrangeTags];
}

- (void)removeAllTags
{
    for (VLSTagButton *t in self.tags) {
        [t removeFromSuperview];
    }
    [self.tags removeAllObjects];
    [self rearrangeTags];
}

- (void)rearrangeTags
{
    self.intrinsicHeight = 0;
    [self.subviews enumerateObjectsUsingBlock:^(UIView* obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    __block float maxY = self.margin.top;
    __block float maxX = self.margin.left;
    __block CGSize size;
    [self.tags enumerateObjectsUsingBlock:^(VLSTagButton *obj, NSUInteger idx, BOOL *stop) {
        size = obj.frame.size;
        [self.subviews enumerateObjectsUsingBlock:^(UIView* obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[VLSTagButton class]]) {
                maxY = MAX(maxY, obj.frame.origin.y);
            }
        }];
        
        [self.subviews enumerateObjectsUsingBlock:^(VLSTagButton *obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[VLSTagButton class]]) {
                if (obj.frame.origin.y == maxY) {
                    maxX = MAX(maxX, obj.frame.origin.x + obj.frame.size.width);
                }
            }
        }];
        
        // Go to a new line if the tag won't fit
        if (size.width + maxX + self.insets > (self.frame.size.width - self.margin.right)) {
            maxY += size.height + self.lineSpace;
            maxX = self.margin.left;
            //don't make a leading space at the beginging of a new line
            obj.frame = (CGRect){maxX, maxY, size.width, size.height};
            [self addSubview:obj];
            return;
        }
        //don't make a leading space at the beginging of a new line
        if (idx == 0) {
            obj.frame = (CGRect){maxX, maxY, size.width, size.height};
            [self addSubview:obj];
            return;
        }
        //obey the insets of the button
        obj.frame = (CGRect){maxX + self.insets, maxY, size.width, size.height};
        [self addSubview:obj];
    }];
    
    CGRect r = self.frame;
    CGFloat n = maxY + size.height + self.margin.bottom;
    self.intrinsicHeight = n > self.intrinsicHeight? n : self.intrinsicHeight;
    [self setFrame:CGRectMake(r.origin.x, r.origin.y, self.frame.size.width, self.intrinsicHeight)];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self rearrangeTags];
}

//buttons
- (NSMutableArray *)tags
{
    if(!_tags)
    {
        _tags = [NSMutableArray array];
    }
    return _tags;
}

@end
