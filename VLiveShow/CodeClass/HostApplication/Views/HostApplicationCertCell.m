//
//  HostApplicationCertCell.m
//  VLiveShow
//
//  Created by Davis on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostApplicationCertCell.h"
#import "HostCertButton.h"
#import "HostApplyCertModel.h"
#import "UIView+gradient.h" 

@interface HostApplicationCertCell ()<HostCertButtonDelegate>

//@property(nonatomic,strong)HostCertButton *button1;

@end

@implementation HostApplicationCertCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self initSubviews];
    }
    
    return self;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)reloadData:(NSArray *)data
{
    for (NSInteger i=0; i<3; i++) {
        
        HostCertButton *btn = [self viewWithTag:i+10];
        
        [btn reloadData:data[i]];
        
        HostApplyCertModel *model = data[i];
        float progress = 0;
        if (model.url) {
            progress = 1;
        }else {
            if (model.image) progress = 0;
            else progress = 1;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [btn viewGradientWithStartPosition:0
                                    endPositon:1
                                      position:GRADIENT_AXIS_VERTICAL
                               backgroundColor:[UIColor clearColor]
                                     maskColor:[[UIColor blackColor] colorWithAlphaComponent:.7f]
                                      duration:.5f
                                      progress:progress];
        });
    }
    
    
    
}


#pragma mark - HostCertButtonDelegate

- (void)didSelectHostCertButton:(HostCertButton *)button
{
    if ([self.delegate respondsToSelector:@selector(hostApplicationCertCell:didSelectIndex:)]) {
        
        [self.delegate  hostApplicationCertCell:self didSelectIndex:button.tag-10];
    }
}

- (void)shouldDeleteHostImageCertButton:(HostCertButton *)button
{
    if ([self.delegate respondsToSelector:@selector(shouldhostDeleteImageInCell:)]) {
        
         [self.delegate shouldhostDeleteImageInCell:button.tag-10];
    }
    
}

#pragma mark - initSubviews

- (void)initSubviews
{
    CGFloat space = 12;
    CGFloat width = (SCREEN_WIDTH-4*12)/3;
    
    CGFloat height = 70;
    NSMutableArray *temAry = @[].mutableCopy;
    for (NSInteger i=0; i<3; i++) {
        
        HostCertButton *btn = [[HostCertButton alloc]initWithFrame: CGRectMake(space+i*(width+space), 0, width, height)];
        btn.delegate = self;
        btn.tag = i+10;
        btn.backgroundColor = [UIColor whiteColor];
        [self addSubview:btn];
        [temAry addObject:btn];
    }
    self.buttonArray = temAry;
}




@end
