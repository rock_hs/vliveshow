//
//  XYTag.m
//  AntsSportApp
//
//  Created by Ants on 15/12/16.
//  Copyright © 2015年 Xiaoyi. All rights reserved.
//

#import "VLSTag.h"

@implementation VLSTag
- (instancetype)initWithText:(NSString *)text
{
    self = [super init];
    if (self)
    {
        _text               = text;
        self.font           = [UIFont systemFontOfSize:14];
        self.textNorColor   = RGB16(COLOR_FONT_9B9B9B);
        self.textSelColor   = RGB16(COLOR_FONT_FF1130);//[UIColor redColor];
        self.bgColor        = [UIColor whiteColor];
        self.selected       = NO;
    }
    
    return self;
}

@end
