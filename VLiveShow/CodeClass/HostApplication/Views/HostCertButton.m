//
//  HostCertButton.m
//  VLiveShow
//
//  Created by Davis on 16/8/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostCertButton.h"

@interface HostCertButton ()

@property (strong, nonatomic)  UIImageView *bgImageV;
@property (strong, nonatomic)  UIImageView *addImageV;
@property (strong, nonatomic)  UILabel *contentLab;
@property (strong, nonatomic)  UIButton *deleteBtn;
@property (strong, nonatomic)  UITapGestureRecognizer *tap;

@property (weak, nonatomic)HostApplyCertModel *model;

@end

@implementation HostCertButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self initSubviews];
    }
    
    return self;
}

- (void)reloadData:(HostApplyCertModel*)model
{
    _model = model;
    _contentLab.text = model.content;
    _bgImageV.image = _model.image;
    
    _bgImageV.hidden = !_model.image;
    _deleteBtn.hidden = !_model.image;
    
    _addImageV.hidden = _model.image;
    _contentLab.hidden = _model.image;
    
    if (model.url && !_model.image)
    {
        
        _bgImageV.hidden = NO;
        _deleteBtn.hidden = NO;
        _addImageV.hidden = YES;
        _contentLab.hidden = YES;
        
        NSURL *url = [NSURL URLWithString:model.url];
        [_bgImageV sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            
            _bgImageV.image = image;
        }];
    }
}

#pragma mark - on tap button action

- (void)onDeleteButtonAction
{
    
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
    if ([self.delegate respondsToSelector:@selector(shouldDeleteHostImageCertButton:)]) {
        
        [self.delegate shouldDeleteHostImageCertButton:self];

    }
}

- (void)onTap
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];

    if ([self.delegate respondsToSelector:@selector(didSelectHostCertButton:)]) {
        
        [self.delegate didSelectHostCertButton:self];
    }
}

#pragma mark - initSubviews
- (void)initSubviews
{
    [self addSubview:self.bgImageV];
    [self addSubview:self.addImageV];
    [self addSubview:self.contentLab];
    [self addSubview:self.deleteBtn];
    [self addGestureRecognizer:self.tap];
}

- (UIImageView *)bgImageV
{
    if (_bgImageV) {
        
        return _bgImageV;
    }
    _bgImageV = [[UIImageView alloc]initWithFrame:self.bounds];
    _bgImageV.layer.masksToBounds = YES;
    _bgImageV.contentMode = UIViewContentModeScaleAspectFill;
    return _bgImageV;
}

- (UIImageView *)addImageV
{
    if (_addImageV) {
        
        return _addImageV;
    }
    _addImageV = [[UIImageView alloc]initWithFrame:CGRectMake(self.bounds.size.width/2-8, 16, 16, 16)];
    _addImageV.image = [UIImage imageNamed:@"ic_add"];
    return _addImageV;
}

- (UILabel *)contentLab
{
    if (_contentLab) {
        
        return _contentLab;
    }
    _contentLab = [[UILabel alloc]initWithFrame:CGRectMake(0, self.bounds.size.height-22, self.bounds.size.width, 14)];
    _contentLab.font = [UIFont systemFontOfSize:14];
    _contentLab.textAlignment = NSTextAlignmentCenter;
    _contentLab.textColor = RGB16(0x9b9b9b);
    return _contentLab;
}


- (UIButton *)deleteBtn
{
    if (_deleteBtn) {
        
        return _deleteBtn;
    }
    _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_deleteBtn setImage:[UIImage imageNamed:@"ic_cancel"] forState:UIControlStateNormal];
    _deleteBtn.frame = CGRectMake(self.bounds.size.width-20, 0, 20, 20);
    [_deleteBtn addTarget:self action:@selector(onDeleteButtonAction) forControlEvents:UIControlEventTouchUpInside];
    _deleteBtn.hidden = YES;
    return _deleteBtn;
}

- (UITapGestureRecognizer *)tap
{
    if (_tap) {
        
        return _tap;
    }
    _tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTap)];
    return _tap;
}

@end
