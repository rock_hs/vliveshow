//
//  XYTagView.h
//  AntsSportApp
//
//  Created by Ants on 15/12/16.
//  Copyright © 2015年 Xiaoyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSTag.h"
#import "VLSTagButton.h"

@interface VLSTagView : UIView
/**
 *  edgeinsets of the shown view
 */
@property (nonatomic, assign) UIEdgeInsets margin;
/**
 *  minimumColumnSpacing
 */
@property (nonatomic, assign) int lineSpace;
/**
 *  minimumInteritemSpacing
 */
@property (nonatomic, assign) CGFloat insets;

- (void)addTag:(VLSTag *)tag;

- (void)removeAllTags;

- (void)removeTagText:(NSString *)text;

@end
