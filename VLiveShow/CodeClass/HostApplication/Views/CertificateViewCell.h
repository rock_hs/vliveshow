//
//  CertificateViewCell.h
//  Application
//
//  Created by tom.zhu on 16/8/16.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HostApplyCertModel;
static NSString *cellID_CertificateView = @"cellID_CertificateView";
static CGFloat CLOSEBUTTON_WIDTH = 28;
static CGFloat MAXCERTIFICATION = 6;
static CGFloat CERTIFICATEVIEWCELL_HEIGHT = 100;
typedef NS_ENUM(int) {
    type_cer,
    type_life,
}CerType;
@interface CertificateViewCell : UICollectionViewCell
@property (nonatomic, assign)       CerType type;
@property (nonatomic, strong)       UIImageView *imageView;
@property (nonatomic, copy)         void (^closeBlock) (CertificateViewCell* cell);
@property (nonatomic, copy)         void (^loadBlock) ();
@property (nonatomic, copy)         void (^showimageBlock) (UIImageView*imageView);
@property (nonatomic, strong)       UIButton *loadView;
/**显示上传提示*/
- (void)clearView;
- (void)setModel:(HostApplyCertModel*)model;
@end
