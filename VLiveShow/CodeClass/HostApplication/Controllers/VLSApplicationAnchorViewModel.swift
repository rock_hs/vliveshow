//
//  VLSApplicationAnchorViewModel.swift
//  VLiveShow
//
//  Created by rock on 2017/2/27.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSApplicationAnchorViewModel: VLSTableViewModel {

    func uploadImage(model: HostApplyCertModel, complete:@escaping completeHandler) {
        let callback = ManagerCallBack()
        callback.updateBlock = { result in
            complete(.success(msg: "", dic: NSDictionarySafty(result)))
        }
        callback.loadBlock = { result in
            complete(.progress(dic: NSDictionarySafty(result)))
        }
        callback.errorBlock = { result in
            complete(.failed(msg: LocalizedString("VLS_UPLOADIMAGE_FAIL")))
        }
        let image = UIImagePNGRepresentation(model.image)
        VLSHostApplicationManager.postHostApplicationImage(image, imageName: model.key, callback: callback)
    }
    
    func deleteImage(url: String, complete:@escaping completeHandler) {
        let callback = ManagerCallBack()
        callback.updateBlock = { result in
            complete(.success(msg: "", dic: NSDictionarySafty(result)))
        }
        callback.errorBlock = { result in
            complete(.failed(msg: LocalizedString("VLS_DELETEIMAGE_FAIL")))
        }
        VLSHostApplicationManager.deleteHostApplicationImage(StringSafty(url), callback: callback)
    }
    
    func submit(dic: NSDictionary, complete:@escaping completeHandler) {
        let callback = ManagerCallBack()
        callback.updateBlock = { result in
            let applicationId = ObjectForKeySafety(NSDictionarySafty(result), key: "applicationId")
            let path = "/host/application/\(StringSafty(applicationId))/life/photo?token=\(StringSafty(AccountManager.shared().account.token))"
            HTTPFacade.postPath(path, param: ["lifePhotos": ObjectForKeySafety(dic, key: "lifeArr")], body: nil, successBlock: { (result) in
                complete(.success(msg: LocalizedString("APPLY_SUCCESS"), dic: NSDictionarySafty(result)))
            }, errorBlcok: { (error) in
                complete(.failed(msg: LocalizedString("APPLY_FAILURE")))
            })
        }
        callback.errorBlock = { result in
            complete(.failed(msg: LocalizedString("APPLY_FAILURE")))
        }
        VLSHostApplicationManager.postHostApplication(["photo": NSDictionarySafty(ObjectForKeySafety(dic, key: "icDic"))], callback: callback)
    }
}
