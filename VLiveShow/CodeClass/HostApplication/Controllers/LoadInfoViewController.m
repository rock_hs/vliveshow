//
//  LoadInfoViewController.m
//  Application
//
//  Created by tom.zhu on 16/8/16.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import "LoadInfoViewController.h"
#import "CertificateViewCell.h"
#import "WaterFLayout.h"
#import "HostApplyProgressView.h"
#import "ServiceViewController.h"
#import "HTTPFacade+get.h"
#import "HostApplyCertModel.h"
#import "VLSPhotoSelectView.h"
#import <AVFoundation/AVFoundation.h>
#import "ImageViewInspect.h"
#import "MBProgressHUD.h"
#import "UIView+gradient.h"
#define BACKCOLOR RGB16(COLOR_BG_F8F8F8)

static CGFloat K_LEFTMARGIN = 21;
static CGFloat K_TITLE_TOPMARGIN = 35;
static CGFloat K_COLLECTIONVIEW_TOPMARGIN = 21;

@interface LoadInfoViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, VLSPhotoSelectViewDelegate>
@property (nonatomic, strong) HostApplyProgressView   *progressView;
@property (nonatomic, strong) UICollectionView        *collectionView;
@property (nonatomic, strong) NSMutableArray          *dataArray;
@property (nonatomic, strong) UIButton                *confirmButton;
@property (nonatomic, strong) UIButton                *checkBox;
@property (nonatomic, strong) VLSPhotoSelectView      *photoSelectView;
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@end

@implementation LoadInfoViewController
- (HostApplyProgressView *)progressView {
    if (nil == _progressView) {
        _progressView = [[HostApplyProgressView alloc]initWithFrame:CGRectMake(0, 62, SCREEN_WIDTH, 2)];
        _progressView.progressValue = 3.0/3.0;
    }
    return _progressView;
}

-(VLSPhotoSelectView *)photoSelectView {
    if (!_photoSelectView) {
        _photoSelectView                 = [[VLSPhotoSelectView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _photoSelectView.delegate        = self;
        _photoSelectView.photoLabel.text = LocalizedString(@"UPLOAD_PHOTO");
        [_photoSelectView showPhotoView:NO animation:NO];
    }
    return _photoSelectView;
}

- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        self.imagePicker               = [[UIImagePickerController alloc] init];
        self.imagePicker.delegate      = self;
        self.imagePicker.allowsEditing = YES;
    }
    return _imagePicker;
}

- (void)uploadTotal:(unsigned long long)total size:(unsigned long long)size image:(UIImage*)image {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSNumber *sizeNum = @(size);
        NSNumber *totalNum = @(total);
        float process = [sizeNum floatValue]/[totalNum floatValue];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.dataArray.count - 1 inSection:0];
        CertificateViewCell *cell = (CertificateViewCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
        [cell.imageView viewGradientWithStartPosition:0
                                           endPositon:1
                                             position:GRADIENT_AXIS_VERTICAL
                                      backgroundColor:[UIColor clearColor]
                                            maskColor:[[UIColor blackColor] colorWithAlphaComponent:.7f]
                                             duration:.5f
                                             progress:process];
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = @[].mutableCopy;
    [self uiconfig];
    [self initNavi];
    [self requestData];
    [self.view addSubview:self.photoSelectView];
}

- (void)initNavi {
    [self.view addSubview:self.navigationView];
    [self.navigationView addSubview:self.progressView];
    //TODO:国际化
    self.navigationItem.title = LocalizedString(@"last_step");
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"3/3" style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)uiconfig {
    self.view.backgroundColor = BACKCOLOR;
    UILabel *label = UILabel.new;
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor lightGrayColor];
    //TODO:国际化
    [label setText:LocalizedString(@"life_image")];
    [label sizeToFit];
    CGRect rect = label.frame;
    rect.origin.x = K_LEFTMARGIN;
    rect.origin.y = K_TITLE_TOPMARGIN + CGRectGetHeight(self.navigationController.navigationBar.frame);
    label.frame = rect;
    [self.view addSubview:label];
    
    WaterFLayout *layout = [WaterFLayout new];
    layout.minimumColumnSpacing = 12;
    layout.minimumInteritemSpacing = 17;
    layout.columnCount = 3;
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(K_LEFTMARGIN, CGRectGetMaxY(label.frame) + K_COLLECTIONVIEW_TOPMARGIN, [UIScreen mainScreen].bounds.size.width - 2 * K_LEFTMARGIN, 100) collectionViewLayout:layout];
    self.collectionView.backgroundColor = BACKCOLOR;
    [self.collectionView registerClass:[CertificateViewCell class] forCellWithReuseIdentifier:cellID_CertificateView];
    [self.view addSubview:self.collectionView];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.bounces = NO;
    [self.collectionView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
    _confirmButton = UIButton.new;
    //TODO:国际化
    [_confirmButton setTitle:LocalizedString(@"confirm") forState:UIControlStateNormal];
    [_confirmButton setTitleColor:RGB16(COLOR_FONT_9B9B9B) forState:UIControlStateNormal];
    [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    _confirmButton.layer.borderWidth = 1;
    _confirmButton.layer.borderColor = RGB16(COLOR_FONT_9B9B9B).CGColor;
    _confirmButton.layer.cornerRadius = 20;
    [_confirmButton addTarget:self action:@selector(confirmClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_confirmButton];
    
    _confirmButton.selected = YES;
    _confirmButton.layer.borderColor = [UIColor clearColor].CGColor;
    _confirmButton.backgroundColor = RGB16(COLOR_FONT_FF1130);
    
    _confirmButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:0 constant:300],
                                [NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:0 constant:40],
                                [NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1 constant:0],
                                [NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:-146],
                                ]];
    [_confirmButton layoutIfNeeded];
    
    rect = CGRectZero;
    UIView *bottomView = UIView.new;
    [self.view addSubview:bottomView];
    _checkBox = [UIButton buttonWithType:UIButtonTypeCustom];
    [_checkBox setImage:[UIImage imageNamed:@"ic_choose"] forState:UIControlStateNormal];
    [_checkBox setImage:[UIImage imageNamed:@"ic_nochoose"] forState:UIControlStateSelected];
    _checkBox.adjustsImageWhenHighlighted = NO;
    [_checkBox addTarget:self action:@selector(agreeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:_checkBox];
    [_checkBox sizeToFit];
    rect = _checkBox.frame;
    
    label = UILabel.new;
    [label setText:LocalizedString(@"agreed")];
    [label setTextColor:RGB16(COLOR_FONT_9B9B9B)];
    [bottomView addSubview:label];
    [label sizeToFit];
    rect.origin.x += rect.size.width + 9;
    rect.size.width = label.frame.size.width;
    rect.size.height = label.frame.size.height;
    label.frame = rect;
    rect.size.width = _checkBox.frame.size.width + 9 + label.frame.size.width;

    UIButton *btnProtocol = UIButton.new;
    [btnProtocol setTitle:LocalizedString(@"live_procotol") forState:UIControlStateNormal];
    [btnProtocol setTitleColor:RGB16(COLOR_FONT_4A4A4A) forState:UIControlStateNormal];
    [bottomView addSubview:btnProtocol];
    [btnProtocol addTarget:self action:@selector(protocolClicke) forControlEvents:UIControlEventTouchUpInside];
    [btnProtocol sizeToFit];
    rect.origin.x = rect.size.width + 9;
    rect.size.width = btnProtocol.frame.size.width;
    rect.size.height = btnProtocol.frame.size.height;
    btnProtocol.frame = rect;
    
    rect.size.width = _checkBox.frame.size.width + 9 + label.frame.size.width + 9 + btnProtocol.frame.size.width;
    rect.origin.y = _confirmButton.frame.origin.y - 26 - rect.size.height;
    rect.origin.x = (SCREEN_WIDTH - rect.size.width) / 2;
    bottomView.frame = rect;
    
    CGRect rectTem = _checkBox.frame;
    rectTem.origin.y = (rect.size.height - _checkBox.frame.size.height) / 2;
    _checkBox.frame = rectTem;
    
    rectTem = label.frame;
    rectTem.origin.y = (rect.size.height - label.frame.size.height) / 2;
    label.frame = rectTem;
}

- (void)requestData {
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"contentSize"]) {
        UICollectionView *collectionView = object;
        CGSize contentSize = collectionView.contentSize;
        CGRect rect = collectionView.frame;
        rect.size.height = contentSize.height;
        collectionView.frame = rect;
    }
}

- (void)agreeClicked:(UIButton*)btn {
    btn.selected ^= 1;
    [self checkStatus];
}

- (void)protocolClicke {
    ServiceViewController *vc = [ServiceViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)confirmClicked:(UIButton*)btn {
    __weak typeof(self) ws = self;
    btn.layer.borderColor = btn.selected ? [UIColor clearColor].CGColor : RGB16(COLOR_FONT_9B9B9B).CGColor;
    btn.backgroundColor = btn.selected ? RGB16(COLOR_FONT_FF1130) : [UIColor clearColor];
    //TODO:request API
    if (!btn.selected) {
        return;
    }
    
    NSMutableArray *ary = @[].mutableCopy;
    [self.dataArray enumerateObjectsUsingBlock:^(HostApplyCertModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.url) {
            [ary addObject:obj.url];
        }
    }];
    NSDictionary *param = @{
                            @"lifePhotos" : ary,
                            };
    NSString *path = [NSString stringWithFormat:@"/host/application/%@/life/photo?token=%@", self.hostApplyModel.applicationId, [AccountManager sharedAccountManager].account.token];
    [HTTPFacade postPath:path param:param body:nil successBlock:^(id  _Nonnull object) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:ws.view animated:1];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = LocalizedString(@"load_completely");
        hud.margin = 10;
        hud.yOffset = 100;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:1];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:ws.view animated:1];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = error.title;
        hud.margin = 10;
        hud.yOffset = 100;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:1];
    }];
}

- (void)checkStatus {
    BOOL status = NO;
    status = !self.checkBox.selected;
    _confirmButton.selected = status;
    _confirmButton.layer.borderColor = status ? [UIColor clearColor].CGColor : RGB16(COLOR_FONT_9B9B9B).CGColor;
    _confirmButton.backgroundColor = status ? RGB16(COLOR_FONT_FF1130) : [UIColor clearColor];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    if (info[@"UIImagePickerControllerOriginalImage"]) {
        __weak typeof(self) ws = self;
        UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
        HostApplyCertModel *model = [HostApplyCertModel new];
        model.image = image;
        [self.dataArray addObject:model];
        [self.collectionView reloadData];
        [self.photoSelectView showPhotoView:NO animation:YES];
        [picker dismissViewControllerAnimated:YES completion:nil];
        //上传证件照
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            PostFileModel *model = PostFileModel.new;
            model.fileName =  @"imageFile";
            model.fileArray = @[UIImageJPEGRepresentation(image, .3f)];
            NSDictionary *param = @{
                                    @"folder" :  @"imageFile",
                                    };
            NSString *url = [[@"/host/application/image" stringByAppendingString:@"?token="] stringByAppendingString:[AccountManager sharedAccountManager].account.token];
            [HTTPFacade uploadPath:url file:model param:param successBlock:^(id  _Nonnull object) {
                NSString *url = object[@"url"];
                [ws.dataArray enumerateObjectsUsingBlock:^(HostApplyCertModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.image == image) {
                        obj.url = url;
                    }
                }];
            } processBlock:^(unsigned long long size, unsigned long long total) {
                [ws uploadTotal:total size:size image:image];
            } errorBlcok:^(ManagerEvent * _Nonnull error) {
                
            }];
        });
    }
}

//TODO: 弹窗提醒用户进入设置页面
- (void)chooseSetting {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"LIVEBEFORE_STARTPLAY") message:LocalizedString(@"LIVEBEFORE_OPENCAMMANDMIC") preferredStyle:UIAlertControllerStyleAlert];
    //修改message
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"LIVEBEFORE_OPENCAMMANDMIC")];
    
    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(0, 11)];
    [alertController setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    // 取消按钮
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"INVITE_CANCEL") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    // 继续按钮
    UIAlertAction *openAction = [UIAlertAction actionWithTitle:LocalizedString(@"LIVEBEFORE_GOOPEN") style:UIAlertActionStyleDefault handler: ^(UIAlertAction * _Nonnull action) {
        
        NSURL * url1 = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        NSURL *url = [NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"];
        if ([[UIApplication sharedApplication] canOpenURL:url])
        {
            [[UIApplication sharedApplication] openURL:url1];
        }
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:openAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

#pragma mark - VLSPhotoSelectViewDelegate
- (void)jumpToSystemPhotoAlbum {
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void)jumpToSystemCamera {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (status) {
        case AVAuthorizationStatusNotDetermined:{
            // 许可对话没有出现，发起授权许可
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    //第一次用户接受
                    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    [self presentViewController:self.imagePicker animated:YES completion:nil];
                }else{
                    [self chooseSetting];
                }
            }];
            break;
        }
        case AVAuthorizationStatusAuthorized:{
            // 已经开启授权，可继续
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.imagePicker animated:YES completion:nil];
            break;
        }
        case AVAuthorizationStatusDenied:{
            [self chooseSetting];
            break;
        }
            
            
        case AVAuthorizationStatusRestricted:
            // 用户明确地拒绝授权，或者相机设备无法访问
        {
            [self chooseSetting];
            break;
            
        }
        default:
            break;
    }
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return MIN(MAXCERTIFICATION, _dataArray.count + 1);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) ws = self;
    CertificateViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID_CertificateView forIndexPath:indexPath]
    ;
    cell.type = type_life;
    cell.loadBlock = ^{
        [ws.photoSelectView showPhotoView:YES animation:YES];
    };
    cell.closeBlock = ^(CertificateViewCell *cell) {
        [ws.dataArray removeObjectAtIndex:[collectionView indexPathForCell:cell].row];
        [collectionView reloadData];
    };
    cell.showimageBlock = ^(UIImageView *imageView){
        ImageViewInspect *imgView = [[ImageViewInspect alloc] initWithFrame:[UIScreen mainScreen].bounds andImage:imageView.image];
        imgView.backgroundColor = [UIColor blackColor];
        imgView.center = ws.navigationController.view.center;
        [ws.navigationController.view addSubview:imgView];
    };
    [cell clearView];
    if (indexPath.row != _dataArray.count) {
        NSInteger row = MIN(_dataArray.count -1, indexPath.row);
        [cell setModel:_dataArray[row]];
    }
    
    return cell;
}

#pragma mark - UICollectionViewFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100, 100);
}

@end
