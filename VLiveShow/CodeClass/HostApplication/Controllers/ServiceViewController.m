//
//  ServiceViewController.m
//  VLiveShow
//
//  Created by tom.zhu on 16/8/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "ServiceViewController.h"

@implementation ServiceViewController
- (void)loadView {
    [super loadView];
    [self uiconfig];
    [self naviconfig];
}

- (void)uiconfig {
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = LocalizedString(@"MINE_SERVICETERMS");
    UIWebView *webView = [[UIWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:webView];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"VProtocol.html" withExtension:nil];
    NSURLRequest *requeset = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requeset];
}

- (void)naviconfig {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
}

@end
