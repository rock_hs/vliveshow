//
//  HostApplicationViewController.m
//  VLiveShow
//
//  Created by Davis on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostApplicationViewController.h"
#import "HostApplicationStepOneVC.h"
#import "VLSHostApplicationManager.h"
#import "AttributedString.h"
#import "AccountManager.h"
#import "LiveBeforeTestViewController.h"
#import "VLSMineViewController.h"
#import "VLiveShow-Swift.h"
#import "TiePhoneNumberController.h"

@interface HostApplicationViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageV;
@property (weak, nonatomic) IBOutlet UIButton *applicationBtn;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;

@property (strong, nonatomic) NSAttributedString *notApply;
@property (strong, nonatomic) NSAttributedString *inReview;
@property (strong, nonatomic) NSAttributedString *applySuccess;
@property (strong, nonatomic) NSAttributedString *applyFailue;

@property (assign, nonatomic) UserApplyStatus userApplyStatus;
@property (copy,   nonatomic) NSString *applicationId;

@end

@implementation HostApplicationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSubviews];
    [self setLucencyNavigation];
    
}

- (void)setLucencyNavigation
{
    if ([self.navigationController.navigationBar respondsToSelector:@selector(shadowImage)]) {
        self.navigationController.navigationBar.shadowImage = [UIImage ImageFromColor:[UIColor clearColor]];
    }
    
    
    
    UIImage *clearImage = [UIImage ImageFromColor:[UIColor clearColor]];
    
    [self.navigationController.navigationBar setBackgroundImage:clearImage
                                                  forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    
    self.navigationController.navigationBar.titleTextAttributes=@{NSForegroundColorAttributeName: [UIColor colorFromHexRGB:@"4a4a4a"],
                                                                  NSFontAttributeName : [UIFont systemFontOfSize:18.0]};
    
    UINavigationBar *naviBar = self.navigationController.navigationBar;
    naviBar.tintColor=[UIColor colorFromHexRGB:@"4a4a4a"];
    naviBar.barTintColor = [UIColor colorFromHexRGB:@"4a4a4a"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden = true;
    _applicationBtn.hidden = YES;
    [self httpRequestGetApplicationStatus];
    
}


#pragma mark - httpRequest

- (void)httpRequestGetApplicationStatus
{
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        [self checkData:result];
        
    };
    
    callback.errorBlock = ^(id result){
        [self error:result];
    };
    
    [VLSHostApplicationManager getHostApplicationStatusCallback:callback];
}

- (void)httpRequestPostApplicationConfirm:(NSString *)applicationId
{
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        
    };
    
    callback.errorBlock = ^(id result){
        [self error:result];
        
    };
    
    [VLSHostApplicationManager postHostApplicationConfirm:_applicationId callback:callback];
}


#pragma mark - 

- (void)checkData:(NSDictionary *)dict
{
    NSString *status = [dict objectForKey:@"status"];
    _applicationId = [dict objectForKey:@"applicationId"];
    
    NSNumber *confirmed = [dict objectForKey:@"confirmed"];
    
    if ([status isEqualToString:@"NotApply"]||[status isEqualToString:@"INITIAL"])
    {
        _userApplyStatus = UserApplyStatusNotApply;
        
    }else if ([status isEqualToString:@"IN_PROGRESS"])
    {
        _userApplyStatus = UserApplyStatusApplyInReview;

    }else if ([status isEqualToString:@"PASSED"])
    {

        _userApplyStatus = confirmed.boolValue?UserApplyStatusApplyFinish:UserApplyStatusApplySuccess;


    }else if ([status isEqualToString:@"REJECTED"])
    {
        _userApplyStatus = confirmed.boolValue?UserApplyStatusNotApply:UserApplyStatusApplyFailure;
    }

    [self reloadView];
}

- (void)reloadView
{
    _applicationBtn.hidden = NO;
    
    if (_userApplyStatus == UserApplyStatusNotApply)
    {
        _contentLab.attributedText = self.notApply;
        [_applicationBtn setTitle:LocalizedString(@"APPLY_IMMEDIATELY") forState:UIControlStateNormal];

    }else if (_userApplyStatus == UserApplyStatusApplyInReview)
    {
        _contentLab.attributedText = self.inReview;
        _applicationBtn.hidden = YES;

    }else if (_userApplyStatus == UserApplyStatusApplySuccess)
    {
        _contentLab.attributedText = self.applySuccess;
        
        [_applicationBtn setTitle:LocalizedString(@"LIVEBEFORE_STARTPLAY") forState:UIControlStateNormal];

    }else if (_userApplyStatus == UserApplyStatusApplyFinish)
    {
        [self showLiveVC];

        
    }else if (_userApplyStatus == UserApplyStatusApplyFailure)
    {
        _contentLab.attributedText = self.applyFailue;
        [_applicationBtn setTitle:LocalizedString(@"LIVEBEFORE_I_KONWEN") forState:UIControlStateNormal];
    }
}

#pragma mark - onAction

- (IBAction)onApplicationBtnAction:(UIButton*)sender
{
    NSString* phone = [AccountManager sharedAccountManager].account.mobilePhone;
    if (!phone || [phone isEqualToString:@""]) {
        TiePhoneNumberController* acc = [[TiePhoneNumberController alloc] init];
        [self.navigationController pushViewController:acc animated:YES];
    } else {
        if (_userApplyStatus == UserApplyStatusNotApply) {
            [self showStepOneVC];
        }else if (_userApplyStatus == UserApplyStatusApplySuccess)
        {
            [self httpRequestPostApplicationConfirm:_applicationId];
            [self showLiveVC];
            
        }else if (_userApplyStatus == UserApplyStatusApplyFailure)
        {
            [self httpRequestPostApplicationConfirm:_applicationId];
            _userApplyStatus = UserApplyStatusNotApply;
            [self reloadView];
        }
    }
}

#pragma mark - show

- (void)showLiveVC
{
    // FIXME: 如果主播已经被剥夺了权限，这里仍然可以开播。。。。
    LiveBeforeTestViewController *anchorTitleVC = [[LiveBeforeTestViewController alloc] init];
    [self presentViewController:anchorTitleVC animated:YES completion:nil];
    self.tabBarController.selectedIndex = 0;
}

- (void)showStepOneVC
{
    VLSApplicationAnchorController *vc = [[VLSApplicationAnchorController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - initSubviews


- (void)initSubviews
{
    _applicationBtn.layer.masksToBounds = YES;
    _applicationBtn.layer.cornerRadius = 20;
    
    NSString *imageName = @"hostApplicationBg";
    if (SCREEN_HEIGHT == 480)
    {
        imageName = @"hostApplicationBg4";
    }
    _backgroundImageV.image = [UIImage imageNamed:imageName];

}



- (NSAttributedString *)notApply
{
    if (_notApply) {
        
        return _notApply;
    }
    
    CMLinkTextViewItem *item1 = [[CMLinkTextViewItem alloc]init];
    item1.textContent = LocalizedString(@"VLIVESHOW_RECRUITING_ANCHOR");
    item1.textColor = [UIColor whiteColor];
    item1.textFont = [UIFont systemFontOfSize:24];
    item1.textAlignment = NSTextAlignmentCenter;

    CMLinkTextViewItem *item2 = [[CMLinkTextViewItem alloc]init];
    item2.textContent = LocalizedString(@"FOR_WORLD_LIVE_YOU");
    item2.textColor = [UIColor whiteColor];
    item2.textFont = [UIFont systemFontOfSize:24];
    item2.textAlignment = NSTextAlignmentCenter;
    
    _notApply = [AttributedString attributeWithLinkTextViewItems:@[item1,item2]];
    
    return _notApply;
}

- (NSAttributedString *)inReview
{
    if (_inReview) {
        
        return _inReview;
    }
    
    CMLinkTextViewItem *item1 = [[CMLinkTextViewItem alloc]init];
    item1.textContent = LocalizedString(@"APPLY_INREVIEW");
    item1.textColor = [UIColor whiteColor];
    item1.textFont = [UIFont systemFontOfSize:24];
    item1.textAlignment = NSTextAlignmentCenter;
    item1.lineSpacing = 19;
    
    CMLinkTextViewItem *item2 = [[CMLinkTextViewItem alloc]init];
    item2.textContent = LocalizedString(@"YOUR_DATA_INREVIEW");
    item2.textColor = [UIColor whiteColor];
    item2.textFont = [UIFont systemFontOfSize:14];
    item2.textAlignment = NSTextAlignmentCenter;
    item1.lineSpacing = 5;

    CMLinkTextViewItem *item3 = [[CMLinkTextViewItem alloc]init];
    item3.textContent = LocalizedString(@"RESULT_FEEDBACK");
    item3.textColor = [UIColor whiteColor];
    item3.textFont = [UIFont systemFontOfSize:14];
    item3.textAlignment = NSTextAlignmentCenter;
    
    
    
    _inReview = [AttributedString attributeWithLinkTextViewItems:@[item1,item2,item3]];
    
    return _inReview;
}
- (NSAttributedString *)applySuccess
{
    if (_applySuccess) {
        
        return _applySuccess;
    }
    
    CMLinkTextViewItem *item1 = [[CMLinkTextViewItem alloc]init];
    item1.textContent = LocalizedString(@"APPLY_SUCCESS");
    item1.textColor = [UIColor whiteColor];
    item1.textFont = [UIFont systemFontOfSize:24];
    item1.textAlignment = NSTextAlignmentCenter;
    item1.lineSpacing = 19;
    
    NSString *nickName = [AccountManager sharedAccountManager].account.nickName;
    nickName = [NSString stringWithFormat:@"\nHi,%@",nickName];
    
    CMLinkTextViewItem *item2 = [[CMLinkTextViewItem alloc]init];
    item2.textContent = nickName;
    item2.textColor = [UIColor whiteColor];
    item2.textFont = [UIFont systemFontOfSize:24];
    item2.textAlignment = NSTextAlignmentCenter;
    item1.lineSpacing = 5;
    
    CMLinkTextViewItem *item3 = [[CMLinkTextViewItem alloc]init];
    item3.textContent = LocalizedString(@"CONGRATULATE_YOU_BECOME_THE_HOST");
    item3.textColor = [UIColor whiteColor];
    item3.textFont = [UIFont systemFontOfSize:16];
    item3.textAlignment = NSTextAlignmentCenter;
    
    
    _applySuccess = [AttributedString attributeWithLinkTextViewItems:@[item1,item2,item3]];
    
    return _applySuccess;
}



- (NSAttributedString *)applyFailue
{
    if (_applyFailue) {
        
        return _applyFailue;
    }
    
    CMLinkTextViewItem *item1 = [[CMLinkTextViewItem alloc]init];
    item1.textContent = LocalizedString(@"APPLY_FAILURE");
    item1.textColor = [UIColor whiteColor];
    item1.textFont = [UIFont systemFontOfSize:24];
    item1.textAlignment = NSTextAlignmentCenter;
    item1.lineSpacing = 19;
    
    NSString *nickName = [AccountManager sharedAccountManager].account.nickName;
    nickName = [NSString stringWithFormat:@"\nHi,%@, ",nickName];
    
    CMLinkTextViewItem *item2 = [[CMLinkTextViewItem alloc]init];
    item2.textContent = nickName;
    item2.textColor = [UIColor whiteColor];
    item2.textFont = [UIFont systemFontOfSize:16];
    item2.textAlignment = NSTextAlignmentCenter;
    item1.lineSpacing = 5;
    
    CMLinkTextViewItem *item3 = [[CMLinkTextViewItem alloc]init];
    item3.textContent = LocalizedString(@"APPLY_FAILURE_PROMPT");
    item3.textColor = [UIColor whiteColor];
    item3.textFont = [UIFont systemFontOfSize:16];
    item3.textAlignment = NSTextAlignmentCenter;
    
    
    _applyFailue = [AttributedString attributeWithLinkTextViewItems:@[item1,item2,item3]];
    
    return _applyFailue;
}

@end
