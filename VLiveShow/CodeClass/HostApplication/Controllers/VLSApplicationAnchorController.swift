//
//  VLSApplicationAnchorController.swift
//  VLiveShow
//
//  Created by rock on 2017/2/27.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSApplicationAnchorController: VLSCollectionViewController {

    var photoView: VLSPhotoSelectView!
    var imagePicker: UIImagePickerController!
    var footerView: VLSApplicationAnchorFooterView!
    var curIndexPath: IndexPath?
    var curUploadNum: NSInteger = 0
    var icDic: NSMutableDictionary = NSMutableDictionary()
    var lifeArr: NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = LocalizedString("APPLICATION_TITLE")
        self.viewModelClass = VLSApplicationAnchorViewModel.self
        
        footerView = VLSApplicationAnchorFooterView().then {
            $0.delegate = self
        }
        self.view.addSubview(footerView)
        footerView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.height.equalTo(120)
        }
        let flowLayout = UICollectionViewFlowLayout().then {
            $0.scrollDirection = .vertical
        }
        collectionView.snp.updateConstraints { (make) in
            make.top.equalTo(self.view)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.bottom.equalTo(footerView.snp.top)
        }
        collectionView.collectionViewLayout = flowLayout
        collectionView.register(VLSApplicationAnchorCell.self, forCellWithReuseIdentifier: "applicationCell")
        collectionView.register(VLSApplicationAnchorSectionView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "sectionHeader")
        self.initDataSource()
    }
    
    func initDataSource() {
        let model0 = HostApplyCertModel().then {
            $0.key = "front"
            $0.content = LocalizedString("ID_CERT_FRONT")
            $0.image = #imageLiteral(resourceName: "live_add")
        }
        let model1 = HostApplyCertModel().then {
            $0.key = "back"
            $0.content = LocalizedString("ID_CERT_BACK")
            $0.image = #imageLiteral(resourceName: "live_add")
        }
        let model2 = HostApplyCertModel().then {
            $0.key = "hold"
            $0.content = LocalizedString("ID_CERT_HOLD")
            $0.image = #imageLiteral(resourceName: "live_add")
        }
        let arr1 = [model0, model1, model2]
        let model10 = HostApplyCertModel().then {
            $0.key = "left_photo"
            $0.content = " "
            $0.image = #imageLiteral(resourceName: "live_lift_photo")
        }
        let arr2 = NSMutableArray(object: model10)
        viewModel.dataSource.add(arr1)
        viewModel.dataSource.add(arr2)
    }
}

extension VLSApplicationAnchorController: VLSPhotoSelectViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, VLSApplicationAnchorFooterViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 40)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "sectionHeader", for: indexPath)
            if let headerView = header as? VLSApplicationAnchorSectionView {
                if indexPath.section == 0 {
                    let attriText = CustomAttributeStrings([(LocalizedString("UPLOAD_ID_CERT"), HEX(COLOR_FONT_4A4A4A), UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_13))), (LocalizedString("MANDATORY"), HEX(COLOR_FONT_FF1130), UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12)))])
                    headerView.title(attriText: attriText)
                } else if indexPath.section == 1 {
                    headerView.title(text: LocalizedString("UPLOAD_LIFE_PHOTO"))
                }
            }
            return header
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width - 20)/3, height: 80)
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.dataSource.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NSArraySafty(ObjectForKeySafety(viewModel.dataSource, index: section)).count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "applicationCell", for: indexPath)
        if let model = ObjectForKeySafety(NSArraySafty(ObjectForKeySafety(viewModel.dataSource, index: indexPath.section)), index: indexPath.row) as? HostApplyCertModel {
            if let cell = cell as? VLSApplicationAnchorCell {
                cell.setNewItem(model: model)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        curIndexPath = indexPath
        if let model = ObjectForKeySafety(NSArraySafty(ObjectForKeySafety(viewModel.dataSource, index: indexPath.section)), index: indexPath.row) as? HostApplyCertModel {
            if StringSafty(model.url) != "" {
                self.deleteImage(model)
            } else {
                if photoView == nil {
                    photoView = VLSPhotoSelectView().then {
                        $0.delegate = self
                        $0.photoLabel.text = LocalizedString("UPLOAD_PHOTO")
                    }
                    self.view.addSubview(photoView)
                    photoView.snp.makeConstraints { (make) in
                        make.top.left.bottom.right.equalTo(self.view)
                    }
                    imagePicker = UIImagePickerController().then {
                        $0.delegate = self
                        $0.allowsEditing = true
                    }
                }
                photoView.showPhotoView(true, animation: true)
            }
        }
    }
    
    //pragma mark - VLSPhotoSelectViewDelegate
    func jumpToSystemPhotoAlbum() {
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func jumpToSystemCamera() {
       imagePicker.sourceType = .camera
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func closePhotoSelectView() {
    }
    
    //pragma mark - UIImagePickerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            photoView.showPhotoView(false, animation: true)
            self.dismiss(animated: true, completion: nil)
            if let curIndexPath = curIndexPath {
                if let model = ObjectForKeySafety(NSArraySafty(ObjectForKeySafety(viewModel.dataSource, index: curIndexPath.section)), index: curIndexPath.row) as? HostApplyCertModel {
                    if curIndexPath.section == 1 && curIndexPath.row < 2 {//个人生活照最多上传3张
                        let newModel = HostApplyCertModel().then {
                            $0.key = "left_photo"
                            $0.image = image
                            $0.url = "vls:"
                        }
                        NSMutableArraySafty(ObjectForKeySafety(viewModel.dataSource, index: curIndexPath.section)).insert(newModel, at: curIndexPath.row)
                        collectionView.insertItems(at: [curIndexPath])
                    } else {
                        model.image = image
                        model.url = "vls:"
                        collectionView.reloadItems(at: [curIndexPath])
                    }
                    if let viewModel = viewModel as? VLSApplicationAnchorViewModel {
                        viewModel.uploadImage(model: model, complete: {[weak self] result in
                            if case .failed(let msg) = result {
                                self?.showHUD(msg)
                            } else if case .success( _, let dic) = result {
                                if let url = ObjectForKeySafety(dic, key: "url") {
                                    if let curmodel = ObjectForKeySafety(NSArraySafty(ObjectForKeySafety(viewModel.dataSource, index: curIndexPath.section)), index: curIndexPath.row) as? HostApplyCertModel {
                                        curmodel.url = StringSafty(url)
                                        if StringSafty(curmodel.key) != "left_photo" {
                                            self?.curUploadNum += 1
                                            self?.footerView.enableSubmit(enable: self?.curUploadNum == 3)
                                            self?.icDic.setValue(StringSafty(curmodel.url), forKey: StringSafty(curmodel.key))
                                        } else {
                                            self?.lifeArr.add(StringSafty(curmodel.url))
                                        }
                                    }
                                }
                            } else if case .progress(let dic) = result {
                                if let size = ObjectForKeySafety(dic, key: "size") as? CGFloat, let total = ObjectForKeySafety(dic, key: "total") as? CGFloat {
                                    if let cell = self?.collectionView.cellForItem(at: curIndexPath) as? VLSApplicationAnchorCell {
                                        DispatchQueue.main.async {
                                            cell.viewGradient(withStartPosition: 0, endPositon: 0, position: GRADIENT_AXIS_VERTICAL, backgroundColor: UIColor.clear, maskColor: UIColor.black.withAlphaComponent(0.8), duration: 0.5, progress: Float(size / total))
                                        }
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }
    }
    
    func deleteImage(_ model: HostApplyCertModel) {
        if let viewModel = viewModel as? VLSApplicationAnchorViewModel {
            viewModel.deleteImage(url: StringSafty(model.url), complete: {[weak self] (result) in
                if case .failed(let msg) = result {
                    self?.showHUD(msg)
                } else if case .success( _, _) = result {
                    if let curIndexPath = self?.curIndexPath, let curmodel = ObjectForKeySafety(NSArraySafty(ObjectForKeySafety(viewModel.dataSource, index: curIndexPath.section)), index: curIndexPath.row) as? HostApplyCertModel {
                        if StringSafty(curmodel.key) != "left_photo" {
                            self?.curUploadNum -= 1
                            self?.footerView.enableSubmit(enable: self?.curUploadNum == 3)
                        }
                        let arr = NSArraySafty(ObjectForKeySafety(viewModel.dataSource, index: curIndexPath.section))
                        if curIndexPath.section == 1 {
                            NSMutableArraySafty(ObjectForKeySafety(viewModel.dataSource, index: curIndexPath.section)).removeObject(at: curIndexPath.row)
                            self?.collectionView.deleteItems(at: [curIndexPath])
                            if let lifeArr = self?.lifeArr, lifeArr.count > curIndexPath.row {
                                lifeArr.removeObject(at: curIndexPath.row)
                            }
                            if arr.count == 2, let lastmodel = arr.lastObject as? HostApplyCertModel, StringSafty(lastmodel.url) != "" {
                                let newModel = HostApplyCertModel().then {
                                    $0.key = "left_photo"
                                    $0.image = #imageLiteral(resourceName: "live_lift_photo")
                                }
                                let newIndexPath = IndexPath(row: 2, section: curIndexPath.section)
                                NSMutableArraySafty(ObjectForKeySafety(viewModel.dataSource, index: curIndexPath.section)).insert(newModel, at: newIndexPath.row)
                                self?.collectionView.insertItems(at: [newIndexPath])
                            }
                            self?.collectionView.reloadData()
                        } else {
                            curmodel.image = #imageLiteral(resourceName: "live_add")
                            curmodel.url = ""
                            self?.collectionView.reloadItems(at: [curIndexPath])
                        }
                    }
                }
            })
        }
    }
    
    func submit() {
        if let viewModel = viewModel as? VLSApplicationAnchorViewModel {
            viewModel.submit(dic: ["icDic": icDic, "lifeArr": lifeArr], complete: { (result) in
                if case .failed(let msg) = result {
                    self.showHUD(msg)
                } else if case .success(let msg, _) = result {
                    self.showHUD(msg)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                }
            })
        }
    }
    
    func checkprotocol() {
        let serviceVC = VLSServiceViewController()
        self.navigationController?.pushViewController(serviceVC, animated: true)
    }
}

class VLSApplicationAnchorSectionView: UICollectionReusableView {
    private var titleLab: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLab = UILabel().then {
            $0.sizeToFit()
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_13))
            $0.textColor = HEX(COLOR_FONT_4A4A4A)
        }
        self.addSubview(titleLab)
        titleLab.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
    }
    
    func title(attriText: NSAttributedString) {
        titleLab.attributedText = attriText
    }
    
    func title(text: String) {
        titleLab.text = text
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol VLSApplicationAnchorFooterViewDelegate: NSObjectProtocol {
    func submit()
    func checkprotocol()
}
class VLSApplicationAnchorFooterView: UICollectionReusableView {
    private var submitBt: UIButton!
    weak var delegate: VLSApplicationAnchorFooterViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let agreeView = UIView().then {
            $0.backgroundColor = UIColor.clear
        }
        self.addSubview(agreeView)
        agreeView.snp.makeConstraints { (make) in
            make.top.centerX.equalTo(self)
            make.height.equalTo(30)
        }
        let agreeBt = UIButton().then {
            $0.setImage(#imageLiteral(resourceName: "live_agree"), for: .normal)
            $0.setTitle(LocalizedString("agreed"), for: .normal)
            $0.setTitleColor(HEX(COLOR_FONT_4A4A4A), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.sizeToFit()
        }
        agreeView.addSubview(agreeBt)
        agreeBt.snp.makeConstraints { (make) in
            make.top.left.bottom.equalTo(agreeView)
        }
        let protoclLab = UILabel().then {
            $0.attributedText = kAttributeStrings([(LocalizedString("VLS_LIVE_PROTOCOL"), HEX(COLOR_FONT_464646), UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14)), NSNumber(value: 1))])
            $0.sizeToFit()
            $0.clickAction(target: self, action: #selector(VLSApplicationAnchorFooterView.checkprotocol))
        }
        agreeView.addSubview(protoclLab)
        protoclLab.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(agreeView)
            make.left.equalTo(agreeBt.snp.right)
        }
        submitBt = UIButton().then {
            $0.setTitle(LocalizedString("confirm"), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.layer.cornerRadius = 18
            $0.layer.borderColor = UIColor.black.withAlphaComponent(0.6).cgColor
            $0.layer.borderWidth = 0.5
            $0.setTitleColor(HEX(COLOR_FONT_464646), for: .normal)
            $0.isEnabled = false
            $0.addTarget(self, action: #selector(VLSApplicationAnchorFooterView.submit), for: .touchUpInside)
        }
        self.addSubview(submitBt)
        submitBt.snp.makeConstraints { (make) in
            make.top.equalTo(agreeView.snp.bottom).offset(10)
            make.height.equalTo(36)
            make.left.equalTo(20)
            make.right.equalTo(-20)
        }
    }
    
    func enableSubmit(enable: Bool) {
        submitBt.isEnabled = enable
        if enable {
            submitBt.backgroundColor = HEX(COLOR_BG_FF1130)
            submitBt.setTitleColor(HEX(COLOR_FONT_FFFFFF), for: .normal)
            submitBt.layer.borderWidth = 0
        } else {
            submitBt.backgroundColor = UIColor.clear
            submitBt.setTitleColor(HEX(COLOR_FONT_464646), for: .normal)
            submitBt.layer.borderWidth = 0.5
        }
    }
    
    func submit() {
        delegate?.submit()
    }
    
    func checkprotocol() {
        delegate?.checkprotocol()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class VLSApplicationAnchorCell: UICollectionViewCell {
    private var backView: UIView!
    private var imageView: UIImageView!
    private var titleLab: UILabel!
    private var deleteBt: UIButton!
    private var model: HostApplyCertModel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.contentView.backgroundColor = UIColor.clear
        backView = UIView().then {
            $0.backgroundColor = HEX(COLOR_BG_FFFFFF)
        }
        self.contentView.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self)
        }
        titleLab = UILabel().then {
            $0.textColor = HEX(COLOR_FONT_4A4A4A)
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12))
            $0.sizeToFit()
            $0.adjustsFontSizeToFitWidth = true
        }
        backView.addSubview(titleLab)
        titleLab.snp.makeConstraints { (make) in
            make.left.right.equalTo(backView)
            make.bottom.equalTo(-6)
        }
        imageView = UIImageView().then {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
            $0.isUserInteractionEnabled = true
        }
        backView.addSubview(imageView)
        deleteBt = UIButton().then {
            $0.setImage(#imageLiteral(resourceName: "live_cancel"), for: .normal)
            $0.isHidden = true
            $0.isEnabled = false
        }
        backView.addSubview(deleteBt)
        deleteBt.snp.makeConstraints { (make) in
            make.top.right.equalTo(backView)
        }
    }
    
    func setNewItem(model: HostApplyCertModel) {
        self.model = model
        imageView.image = model.image
        if StringSafty(model.url) != "" {
            imageView.snp.remakeConstraints { (make) in
                make.top.left.bottom.right.equalTo(backView)
            }
        } else {
            imageView.snp.remakeConstraints { (make) in
                make.center.equalTo(backView)
            }
        }
        titleLab.text = StringSafty(model.content)
        titleLab.isHidden = StringSafty(model.url) != ""
        deleteBt.isHidden = StringSafty(model.url) == ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
