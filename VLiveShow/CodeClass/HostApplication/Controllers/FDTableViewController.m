//
//  FDTableViewController.m
//  Application
//
//  Created by tom.zhu on 16/8/16.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import "FDTableViewController.h"
#import "VLSTagView.h"
#import "CertificateViewCell.h"
#import "WaterFLayout.h"
#import "LoadInfoViewController.h"
#import "HostApplyProgressView.h"
#import "VLSPhotoSelectView.h"
#import "HTTPFacade+get.h"
#import <AVFoundation/AVFoundation.h>
#import "UIAlertController+Custom.h"
#import "HostApplyCertModel.h"
#import "ImageViewInspect.h"
#import "VLSUtily.h"
#import "UIView+gradient.h"
#define BACKCOLOR RGB16(COLOR_BG_F8F8F8)

static CGFloat K_TITLELABEL_TOPMARGIN = 19;
static CGFloat K_TITLELABEL_LEFTMARGIN = 21;
static CGFloat K_TAG_LINESPACE = 12;
static CGFloat K_TAG_INSETS = 13;
//MARK:cell
static NSString *CELLIDENTITY_TITLE = @"CELLIDENTITY_TITLE";
@interface TitleCell : UITableViewCell
- (void)setTitle:(NSString*)text;
@end

@implementation TitleCell
{
    UILabel *_label;
}
- (void)setTitle:(NSString*)text {
    [_label setText:text];
    [_label sizeToFit];
    CGRect rect = _label.frame;
    rect.origin.x = K_TITLELABEL_LEFTMARGIN;
    rect.origin.y = K_TITLELABEL_TOPMARGIN;
    _label.frame = rect;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = BACKCOLOR;
        _label = UILabel.new;
        _label.textColor = RGB16(COLOR_FONT_9B9B9B);
        _label.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_label];
    }
    return self;
}

@end
//MARK:cell
static NSString *CELLIDENTITY_TAGCell = @"CELLIDENTITY_TAGCell";
@interface TAGCell : UITableViewCell
- (void)setTagView:(VLSTagView*)tagView;
@end

@implementation TAGCell
- (void)setTagView:(VLSTagView*)tagView {
    CGRect rect = tagView.frame;
    rect.origin.x = K_TITLELABEL_LEFTMARGIN;
    tagView.frame = rect;
    [self.contentView addSubview:tagView];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = BACKCOLOR;
    }
    return self;
}

@end
//MARK:cell
static NSString *CELLIDENTITY_CERTIFICATION = @"CELLIDENTITY_CERTIFICATION";
@interface CertificationCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, copy) void(^processBlcok) (unsigned long long total, unsigned long long size, UIImage *image);
@property (nonatomic, strong) NSMutableArray<HostApplyCertModel*>   *dataArray;
@property (nonatomic, strong) UICollectionView *collectionView;
/**
 *  取消证件
 */
@property (nonatomic, copy) void (^imagePickBlock) ();
/**
 *  取消证件
 */
@property (nonatomic, copy) void (^confirmedcloseBlock) (CertificateViewCell *cell);
/**
 *  证件显示
 */
@property (nonatomic, copy) void (^showImageBlock) (UIImageView *imageView);
/**
 *  说明collectionView高度变化，此时需要刷新界面
 */
@property (nonatomic, copy) void (^reloadBlock) (CGFloat currentHeight);
/**
 *  证件选中并显示
 */
- (void)pickImage:(UIImage*)image;
/**
 *  取消证件
 */
- (void)removeImage:(CertificateViewCell*)cell;
@end

@implementation CertificationCell
- (void)dealloc {
    [_collectionView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    if (dataArray.count > _dataArray.count) {
        _dataArray = dataArray.mutableCopy;
//        [_collectionView reloadData];
        [_collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:(self.dataArray.count - 1) inSection:0]]];
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = BACKCOLOR;
        _dataArray = @[].mutableCopy;
        WaterFLayout *layout = [WaterFLayout new];
        layout.minimumColumnSpacing = 12;
        layout.minimumInteritemSpacing = 17;
        layout.columnCount = 3;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(K_TITLELABEL_LEFTMARGIN, 0, [UIScreen mainScreen].bounds.size.width - 2 * K_TITLELABEL_LEFTMARGIN, 100) collectionViewLayout:layout];
        _collectionView.backgroundColor = BACKCOLOR;
        [_collectionView registerClass:[CertificateViewCell class] forCellWithReuseIdentifier:cellID_CertificateView];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.bounces = NO;
        [self.contentView addSubview:_collectionView];
        __weak typeof(self) ws = self;
        self.processBlcok = ^(unsigned long long total, unsigned long long size, UIImage *image){
            dispatch_async(dispatch_get_main_queue(), ^{
                NSNumber *sizeNum = @(size);
                NSNumber *totalNum = @(total);
                float process = [sizeNum floatValue]/[totalNum floatValue];
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:ws.dataArray.count - 1 inSection:0];
                CertificateViewCell *cell = (CertificateViewCell*)[ws.collectionView cellForItemAtIndexPath:indexPath];
                [cell.imageView viewGradientWithStartPosition:0
                                                   endPositon:1
                                                     position:GRADIENT_AXIS_VERTICAL
                                              backgroundColor:[UIColor clearColor]
                                                    maskColor:[[UIColor blackColor] colorWithAlphaComponent:.7f]
                                                     duration:.5f
                                                     progress:process];
            });
        };
        
        [_collectionView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)chage context:(void *)context {
    if ([keyPath isEqualToString:@"contentSize"]) {
        UICollectionView *collectionView = object;
        CGSize contentSize = collectionView.contentSize;
        CGRect rect = collectionView.frame;
        rect.size.height = contentSize.height;
        collectionView.frame = rect;
        if (self.reloadBlock) {
            self.reloadBlock(rect.size.height);
        }
    }
}

- (void)pickImage:(UIImage*)image {
    HostApplyCertModel *model = [HostApplyCertModel new];
    model.image = image;
    [_dataArray addObject:model];
    [_collectionView reloadData];
}

- (void)removeImage:(CertificateViewCell*)cell {
    [_dataArray removeObjectAtIndex:[self.collectionView indexPathForCell:cell].row];
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return MIN(MAXCERTIFICATION, _dataArray.count + 1);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CertificateViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID_CertificateView forIndexPath:indexPath]
    ;
    cell.loadBlock = ^{
        if (self.imagePickBlock) {
            self.imagePickBlock();
        }
    };
    cell.closeBlock = ^(CertificateViewCell *cell) {
        if (self.confirmedcloseBlock) {
            self.confirmedcloseBlock(cell);
        }
    };
    cell.showimageBlock = ^(UIImageView *imageView){
        if (self.showImageBlock) {
            self.showImageBlock(imageView);
        }
    };
    [cell clearView];
    if (indexPath.row != _dataArray.count) {
        NSInteger row = MIN(_dataArray.count -1, indexPath.row);
        [cell setModel:_dataArray[row]];
    }

    return cell;
}

#pragma mark - UICollectionViewFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100, 100);
}

@end
//MARK:cell
static CGFloat BUTTON_TOPMARGIN = 22;
static CGFloat BUTTON_HEIGHT = 40;
static NSString *CELLIDENTITY_BUTTON = @"CELLIDENTITY_BUTTON";
@interface BUTTONCell : UITableViewCell
@property (nonatomic, copy  ) void (^nextBlock) ();
@property (nonatomic, strong) UIButton  *button;
@end

@implementation BUTTONCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = BACKCOLOR;
        //上传按钮
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button setTitleColor:RGB16(COLOR_FONT_9B9B9B) forState:UIControlStateNormal];
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        //TODO:国际化
        [_button setTitle:LocalizedString(@"LOGIN_BUTTON_NEXT") forState:UIControlStateNormal];
        _button.layer.borderWidth = 1;
        _button.layer.borderColor = RGB16(COLOR_FONT_9B9B9B).CGColor;
        _button.layer.cornerRadius = 20;
        [_button addTarget:self action:@selector(nextbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _button.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - 300) / 2, BUTTON_TOPMARGIN, 300, BUTTON_HEIGHT);
        [self.contentView addSubview:_button];
    }
    return self;
}

- (void)nextbuttonClicked:(UIButton*)button {
    if (self.nextBlock) {
        self.nextBlock();
    }
}

@end
//MARK:FDTableViewController
@interface FDTableViewController () <VLSPhotoSelectViewDelegate>
@property (nonatomic, strong) UIImagePickerController    *imagePicker;
@property (nonatomic, strong) VLSPhotoSelectView         *photoSelectView;
@property (nonatomic, strong) HostApplyProgressView      *progressView;

@property (nonatomic, strong) UITableView                *tableView;
@property (nonatomic, strong) NSMutableArray             *dataArray_Normal;//所有不需要证书标签
@property (nonatomic, strong) NSMutableArray             *dataArray_Special;//所有需要证书标签
@property (nonatomic, strong) NSMutableArray             *tags_Normal;//选中的标签
@property (nonatomic, strong) NSMutableArray             *tags_Special;//选中的标签
@property (nonatomic, strong) NSMutableArray             *cer_urls;//历史上传认证图片
@property (nonatomic, strong) NSMutableArray             *tags_once;//历史上传技能

@property (nonatomic, assign) CGFloat                    title_height;
@property (nonatomic, assign) CGFloat                    certificationCell_height;

@property (nonatomic, strong) VLSTagView                 *tagView_Normal;
@property (nonatomic, strong) VLSTagView                 *tagView_SpecialView;
@end

@implementation FDTableViewController
- (VLSTagView *)tagView_Normal {
    if (_tagView_Normal == nil) {
        _tagView_Normal = [[VLSTagView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 2 * K_TITLELABEL_LEFTMARGIN, 0)];
        _tagView_Normal.backgroundColor = BACKCOLOR;
        _tagView_Normal.lineSpace = K_TAG_LINESPACE;
        _tagView_Normal.insets = K_TAG_INSETS;
    }
    return _tagView_Normal;
}

- (VLSTagView *)tagView_SpecialView {
    if (_tagView_SpecialView == nil) {
        _tagView_SpecialView            = [[VLSTagView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 2 * K_TITLELABEL_LEFTMARGIN, 0)];
        _tagView_Normal.backgroundColor = BACKCOLOR;
        _tagView_SpecialView.lineSpace  = K_TAG_LINESPACE;
        _tagView_SpecialView.insets     = K_TAG_INSETS;
    }
    return _tagView_SpecialView;
}

- (HostApplyProgressView *)progressView {
    if (nil == _progressView) {
        _progressView               = [[HostApplyProgressView alloc]initWithFrame:CGRectMake(0, 62, SCREEN_WIDTH, 2)];
        _progressView.progressValue = 2.0/3.0;
    }
    return _progressView;
}

-(VLSPhotoSelectView *)photoSelectView {
    if (!_photoSelectView) {
        _photoSelectView                 = [[VLSPhotoSelectView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _photoSelectView.delegate        = self;
        _photoSelectView.photoLabel.text = LocalizedString(@"UPLOAD_PHOTO");
        [_photoSelectView showPhotoView:NO animation:NO];
    }
    return _photoSelectView;
}

- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        self.imagePicker               = [[UIImagePickerController alloc] init];
        self.imagePicker.delegate      = self;
        self.imagePicker.allowsEditing = YES;
        
    }
    return _imagePicker;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //寻找新标签
    BOOL res_tag = NO;
    NSMutableArray *temAry = @[].mutableCopy;
    [self.tags_once enumerateObjectsUsingBlock:^(VLSTag *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.dataArray_Normal enumerateObjectsUsingBlock:^(VLSTag *obj_, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj_.selected) {
                if ([obj.id_ isEqualToString:obj_.id_]) {
                    [temAry addObject:obj];
                }
            }
        }];
        [self.dataArray_Special enumerateObjectsUsingBlock:^(VLSTag *obj_, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj_.selected) {
                if ([obj.id_ isEqualToString:obj_.id_]) {
                    [temAry addObject:obj];
                }
            }
        }];
    }];
    res_tag = temAry.count == self.tags_once.count;
    //寻找新证件照
//    BOOL __block res_url = NO;
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    if ([cell isKindOfClass:[CertificationCell class]]) {
        [((CertificationCell*)cell).dataArray enumerateObjectsUsingBlock:^(HostApplyCertModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.url) {
                [self.cer_urls enumerateObjectsUsingBlock:^(HostApplyCertModel *obj_, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                }];
            }else {
                
            }
        }];
    }
    if (![self.navigationController.viewControllers containsObject:self]) {
        //pop
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tags_Normal  = @[].mutableCopy;
    self.tags_Special = @[].mutableCopy;
    self.cer_urls     = @[].mutableCopy;
    [self uiconfig];
    [self requestData];
    [self initNavi];
}

- (void)uiconfig {
    self.view.backgroundColor      = BACKCOLOR;
    self.tableView                 = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStylePlain];
    self.tableView.backgroundColor = BACKCOLOR;
    [self.tableView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTap)]];
    self.tableView.dataSource      = self;
    self.tableView.delegate        = self;
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces         = NO;
    self.tableView.tableFooterView = UIView.new;
    
    [self.tableView registerClass:[TitleCell class] forCellReuseIdentifier:CELLIDENTITY_TITLE];
    [self.tableView registerClass:[TAGCell class] forCellReuseIdentifier:CELLIDENTITY_TAGCell];
    [self.tableView registerClass:[CertificationCell class] forCellReuseIdentifier:CELLIDENTITY_CERTIFICATION];
    [self.tableView registerClass:[BUTTONCell class] forCellReuseIdentifier:CELLIDENTITY_BUTTON];
    
    UILabel *label = UILabel.new;
    label.font = [UIFont systemFontOfSize:14];
    label.text = @"text";
    [label sizeToFit];
    self.title_height = CGRectGetHeight(label.bounds) + 17 + 19;
    [self.view addSubview:self.photoSelectView];
}

- (void)initNavi {
    [self.view addSubview:self.navigationView];
    [self.navigationView addSubview:self.progressView];
    //TODO:国际化
    self.navigationItem.title = LocalizedString(@"sonn_over");

    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"2/3" style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)requestData {
    __weak typeof(self) ws = self;
    self.dataArray_Normal  = @[].mutableCopy;
    self.dataArray_Special = @[].mutableCopy;
    void (^tag_Nor) (NSDictionary *dic) = ^(NSDictionary *dic){
        VLSTag *tag      = [[VLSTag alloc] initWithText:dic[@"name"]];
        tag.id_          = dic[@"id"];
        tag.cornerRadius = 10;
        tag.bgColor      = [UIColor whiteColor];
        tag.font         = [UIFont systemFontOfSize:14];
        tag.inset        = 5;

        tag.widthExpand  = 10;
        tag.heightExpand = 2;

        tag.type         = TagNormal;
        tag.target       = ws;
        tag.action       = @selector(tagClicked:);
        [ws.tagView_Normal addTag:tag];
        [ws.dataArray_Normal addObject:tag];
    };
    void (^tag_Spe) (NSDictionary *dic) = ^(NSDictionary *dic){
        VLSTag *tag      = [[VLSTag alloc] initWithText:dic[@"name"]];
        tag.id_          = dic[@"id"];
        tag.cornerRadius = 10;
        tag.bgColor      = [UIColor whiteColor];
        tag.font         = [UIFont systemFontOfSize:14];
        tag.inset        = 5;

        tag.widthExpand  = 10;
        tag.heightExpand = 2;

        tag.type         = TagSpecial;
        tag.inset        = 5;
        tag.target       = ws;
        tag.action       = @selector(tagClicked:);
        [ws.tagView_SpecialView addTag:tag];
        [ws.dataArray_Special addObject:tag];
    };
    //选中的标签标识为选中状态
    NSMutableArray *selectedTags = @[].mutableCopy;
    void (^updateStatus) () = ^{
        if (!(selectedTags.count && (self.tags_Normal.count || self.tags_Special))) {
            return ;
        }
        [ws.dataArray_Normal enumerateObjectsUsingBlock:^(VLSTag *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [selectedTags enumerateObjectsUsingBlock:^(VLSTag *obj_, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.id_ isEqualToString:obj_.id_]) {
                    [obj setSelected:[obj.id_ isEqualToString:obj_.id_]];
                    [ws.tags_Normal addObject:obj];
                    return ;
                }
            }];
        }];
        [ws.dataArray_Special enumerateObjectsUsingBlock:^(VLSTag *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [selectedTags enumerateObjectsUsingBlock:^(VLSTag *obj_, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.id_ isEqualToString:obj_.id_]) {
                    [obj setSelected:[obj.id_ isEqualToString:obj_.id_]];
                    [ws.tags_Special addObject:obj];
                    return ;
                }
            }];
        }];
        ws.tags_once = selectedTags.mutableCopy;
        [ws.tableView reloadData];
        [ws checkStatus];
    };
    NSDictionary *param = @{
                            @"token" : [AccountManager sharedAccountManager].account.token,
                            @"needsCertificate" : @0,
                            };
    //获取所有标签
    [HTTPFacade getPath:@"/host/skills" param:param successBlock:^(id result) {
        NSDictionary *dictionary = (NSDictionary*)result;
        NSArray *array = dictionary[@"list"];
        [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj[@"needsCertificate"] boolValue]) {
                tag_Spe(obj);
            }else {
                tag_Nor(obj);
            }
        }];
        updateStatus();
        [ws.tableView reloadData];
    } errorBlcok:^(ManagerEvent *error) {
        
    }];
    
    //获取历史记录
    param = @{
              @"token" : [AccountManager sharedAccountManager].account.token,
              };
    [HTTPFacade getPath:@"/user/skills" param:param successBlock:^(id  _Nonnull object) {
        NSDictionary *dictionary = (NSDictionary*)object;
        NSArray *skills = dictionary[@"skills"];
        [skills enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            VLSTag *tag = [[VLSTag alloc] initWithText:obj[@"name"]];
            tag.id_ = obj[@"id"];
            [selectedTags addObject:tag];
        }];
        NSArray *ary = [dictionary[@"certificates"] isKindOfClass:[NSNull class]] ? @[].copy : dictionary[@"certificates"];
        [ary enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HostApplyCertModel *model = [HostApplyCertModel new];
            model.url = obj;
            [ws.cer_urls addObject:model];
        }];
        
        updateStatus();
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        
    }];
}

- (void)tagClicked:(VLSTagButton*)button {
    VLSTag __block *tag = button.xyTag;
    tag.selected = !tag.selected;
    UIColor *borderColor = tag.selected ? RGB16(COLOR_FONT_FF1130) : [UIColor whiteColor];
    button.layer.borderColor = borderColor.CGColor;
    button.layer.borderWidth = 1;
    BOOL (^containsObject_Nor)(VLSTag*tag) = ^(VLSTag*tagTem){
        BOOL __block res = NO;
        [self.tags_Normal enumerateObjectsUsingBlock:^(VLSTag *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            res = [tagTem.id_ isEqualToString:obj.id_];
            if ([tagTem.id_ isEqualToString:obj.id_]) {
                tag = obj;
            }
        }];
        return res;
    };
    BOOL (^containsObject_Spe)(VLSTag*tag) = ^(VLSTag*tagTem){
        BOOL __block res = NO;
        [self.tags_Special enumerateObjectsUsingBlock:^(VLSTag *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            res = [tagTem.id_ isEqualToString:obj.id_];
            if ([tagTem.id_ isEqualToString:obj.id_]) {
                tag = obj;
                *stop = YES;
            }
        }];
        return res;
    };
    if (tag.type == TagNormal) {
        if (tag.selected) {
            if (!containsObject_Nor(tag)) {
                [self.tags_Normal addObject:tag];
            }
        }else {
            if (containsObject_Nor(tag)) {
                [self.tags_Normal removeObject:tag];
            }
        }
        [self checkStatus];
    }else {
        if (tag.selected) {
            if (!containsObject_Spe(tag)) {
                [self.tags_Special addObject:tag];
            }
        }else {
            if (containsObject_Spe(tag)) {
                [self.tags_Special removeObject:tag];
            }
        }
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            [self checkStatus];
        }];
        [self.tableView reloadData];
        [CATransaction flush];
    }
}

- (void)onTap {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

//删除照片
- (void)removeImage:(HostApplyCertModel*)model {
    [self.cer_urls removeObject:model];
    NSDictionary *param = @{
                            @"token" : [AccountManager sharedAccountManager].account.token,
                            @"url" : model.url ?: @"",
                            };
    [HTTPFacade delePath:@"/host/application/image" param:param successBlock:^(id object) {
        
    } errorBlcok:^(ManagerEvent *error) {
        
    }];
}

- (void)loadApplicationSkills {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSMutableArray *ary = [NSMutableArray new];
    [self.dataArray_Normal enumerateObjectsUsingBlock:^(VLSTag *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.selected) {
            [ary addObject:obj.id_];
        }
    }];
    [self.dataArray_Special enumerateObjectsUsingBlock:^(VLSTag *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.selected) {
            [ary addObject:obj.id_];
        }
    }];
    [param addEntriesFromDictionary:@{@"skillIDs" : ary.copy,}];
    [ary removeAllObjects];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    if ([cell isKindOfClass:[CertificationCell class]]) {
        [((CertificationCell*)cell).dataArray enumerateObjectsUsingBlock:^(HostApplyCertModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.url) {
                [ary addObject:obj.url];
            }
        }];
    }
    if (ary.count) {
        [param addEntriesFromDictionary:@{
                                          @"certificates" : ary,
                                          }];
    }
    
    NSString *path = [NSString stringWithFormat:@"/host/application/%@/skill?token=%@", self.hostApplyModel.applicationId, [AccountManager sharedAccountManager].account.token];
        [HTTPFacade postPath:path param:param body:nil successBlock:^(id  _Nonnull object) {
            
        } errorBlcok:^(ManagerEvent * _Nonnull error) {
            
        }];
}

- (void)checkStatus {
    //iOS10 reloaddata 方法执行时间比较慢
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        BUTTONCell *cell_btn = nil;
        UIButton *button     = nil;
        BOOL status = NO;
        if (self.tags_Special.count) {
            CertificationCell *cell = (CertificationCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
            cell_btn = (BUTTONCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
            button = cell_btn.button;
            if (cell.dataArray.count == 0) {
                status = NO;
            }else {
                status = YES;
            }
        }else {
            cell_btn = (BUTTONCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
            button = cell_btn.button;
            if (self.tags_Normal.count) {
                status = YES;
            }else {
                status = NO;
            }
        }
        button.selected = status;
        button.layer.borderColor = status ? [UIColor clearColor].CGColor : RGB16(COLOR_FONT_9B9B9B).CGColor;
        button.backgroundColor = status ? RGB16(COLOR_FONT_FF1130) : [UIColor clearColor];
    });
}

//TODO: 弹窗提醒用户进入设置页面
- (void)chooseSetting {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"LIVEBEFORE_STARTPLAY") message:LocalizedString(@"LIVEBEFORE_OPENCAMMANDMIC") preferredStyle:UIAlertControllerStyleAlert];
    //修改message
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"LIVEBEFORE_OPENCAMMANDMIC")];
    
    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(0, 11)];
    [alertController setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    // 取消按钮
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"INVITE_CANCEL") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    // 继续按钮
    UIAlertAction *openAction = [UIAlertAction actionWithTitle:LocalizedString(@"LIVEBEFORE_GOOPEN") style:UIAlertActionStyleDefault handler: ^(UIAlertAction * _Nonnull action) {
        
        NSURL * url1 = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        NSURL *url = [NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"];
        if ([[UIApplication sharedApplication] canOpenURL:url])
        {
            [[UIApplication sharedApplication] openURL:url1];
        }
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:openAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    if (info[@"UIImagePickerControllerOriginalImage"]) {
        UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
        CertificationCell *cell = (CertificationCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
        [cell pickImage:image];
        [picker dismissViewControllerAnimated:YES completion:nil];
        [self.photoSelectView showPhotoView:NO animation:YES];
        [self checkStatus];
        //上传证件照
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            PostFileModel *model = PostFileModel.new;
            model.fileName =  @"imageFile";
            model.fileArray = @[UIImageJPEGRepresentation(image, .3f)];
            NSDictionary *param = @{
                                    @"folder" :  @"imageFile",
                                    };
            NSString *url = [[@"/host/application/image" stringByAppendingString:@"?token="] stringByAppendingString:[AccountManager sharedAccountManager].account.token];
            [HTTPFacade uploadPath:url file:model param:param successBlock:^(id  _Nonnull object) {
                NSString *url = object[@"url"];
                [cell.dataArray enumerateObjectsUsingBlock:^(HostApplyCertModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.image == image) {
                        obj.url = url;
                    }
                }];
                [self checkStatus];
            } processBlock:^(unsigned long long size, unsigned long long total) {
                if (cell.processBlcok) {
                    cell.processBlcok(size, total, image);
                }
            } errorBlcok:^(ManagerEvent * _Nonnull error) {
                
            }];
        });
    }
}

#pragma mark - VLSPhotoSelectViewDelegate
- (void)jumpToSystemPhotoAlbum {
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void)jumpToSystemCamera {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (status) {
        case AVAuthorizationStatusNotDetermined:{
            // 许可对话没有出现，发起授权许可
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    //第一次用户接受
                    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    [self presentViewController:self.imagePicker animated:YES completion:nil];
                }else{
                    [self chooseSetting];
                }
            }];
            break;
        }
        case AVAuthorizationStatusAuthorized:{
            // 已经开启授权，可继续
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.imagePicker animated:YES completion:nil];
            break;
        }
        case AVAuthorizationStatusDenied:{
            [self chooseSetting];
            break;
        }
            
            
        case AVAuthorizationStatusRestricted:
            // 用户明确地拒绝授权，或者相机设备无法访问
        {
            [self chooseSetting];
            break;
            
        }
        default:
            break;
    }
}

- (void)closePhotoSelectView {
    
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int num = self.tags_Special.count ? 7 : 5;
    return num;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    __weak typeof(self) ws = self;
    if (indexPath.row == 0) {
        TitleCell *cell_ = [tableView dequeueReusableCellWithIdentifier:CELLIDENTITY_TITLE forIndexPath:indexPath];
        [cell_ setTitle:LocalizedString(@"LIVEBEFORE_MARK_GOOD_FIELD")];
        cell = (UITableViewCell*)cell_;
    }else if (indexPath.row == 1) {
        TAGCell *cell_ = [tableView dequeueReusableCellWithIdentifier:CELLIDENTITY_TAGCell forIndexPath:indexPath];
        [cell_ setTagView:self.tagView_Normal];
        cell = (UITableViewCell*)cell_;
    }else if (indexPath.row == 2) {
        TitleCell *cell_ = [tableView dequeueReusableCellWithIdentifier:CELLIDENTITY_TITLE forIndexPath:indexPath];
        [cell_ setTitle:LocalizedString(@"skillsToConfirm")];
        cell = (UITableViewCell*)cell_;
    }else if (indexPath.row == 3) {
        TAGCell *cell_ = [tableView dequeueReusableCellWithIdentifier:CELLIDENTITY_TAGCell forIndexPath:indexPath];
        [cell_ setTagView:self.tagView_SpecialView];
        cell = (UITableViewCell*)cell_;
    }else {
        if (self.tags_Special.count) {
            if (indexPath.row == 4) {
                TitleCell *cell_ = [tableView dequeueReusableCellWithIdentifier:CELLIDENTITY_TITLE forIndexPath:indexPath];
                [cell_ setTitle:LocalizedString(@"load_credentials")];
                cell = (UITableViewCell*)cell_;
            }else  if (indexPath.row == 5){
                CertificationCell *cell_ = [tableView dequeueReusableCellWithIdentifier:CELLIDENTITY_CERTIFICATION forIndexPath:indexPath];
                __weak typeof(cell_) ws_cell = cell_;
                [cell_ setDataArray:self.cer_urls];
                
                cell_.showImageBlock = ^(UIImageView *imageView){
                    ImageViewInspect *imgView = [[ImageViewInspect alloc] initWithFrame:[UIScreen mainScreen].bounds andImage:imageView.image];
                    imgView.backgroundColor = [UIColor blackColor];
                    imgView.center = ws.navigationController.view.center;
                    [ws.navigationController.view addSubview:imgView];
                };
                cell_.imagePickBlock = ^{
                    [ws.photoSelectView showPhotoView:YES animation:YES];
                };
                cell_.reloadBlock = ^(CGFloat currentHeight){
                    ws.certificationCell_height = currentHeight;
                    [ws.tableView reloadData];
                };
                cell_.confirmedcloseBlock = ^(CertificateViewCell*cell){
                        [UIAlertController customAlertShowInViewController:self
                                                                 withTitle:nil
                                                                   message:LocalizedString(@"DELETE_IMAGE_PROMPT")
                                                         cancelButtonTitle:LocalizedString(@"CANCLE")
                                                    destructiveButtonTitle:LocalizedString(@"YES")
                                                                  tapBlock:^(UIAlertController * _Nonnull controller,
                                                                             UIAlertAction * _Nonnull action,
                                                                             NSInteger buttonIndex) {
                                                                      if (buttonIndex == 1) {
                                                                          [ws removeImage:[ws_cell.dataArray objectAtIndex:[ws_cell.collectionView indexPathForCell:cell].row]];
                                                                          [ws_cell removeImage:cell];
                                                                          [ws checkStatus];
                                                                      }else {
                                                                          
                                                                      }
                                                                  }];
                };
                cell = (UITableViewCell*)cell_;
            }else {
                BUTTONCell *cell_ = [tableView dequeueReusableCellWithIdentifier:CELLIDENTITY_BUTTON forIndexPath:indexPath];
                cell_.nextBlock = ^{
                    if (ws.tags_Special.count) {
                        CertificationCell *cell = (CertificationCell*)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
                        BOOL __block res = NO;
                        [cell.dataArray enumerateObjectsUsingBlock:^(HostApplyCertModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            if (obj.url == nil) {
                                res = YES;
                            }
                        }];
                        if (cell.dataArray.count == 0 || res) {
                            NSLog(@"请上传证件照，或有证件照正在上传中");
                            return ;
                        }else {
                            LoadInfoViewController *vc = [LoadInfoViewController new];
                            vc.hostApplyModel = self.hostApplyModel;
                            [ws.navigationController pushViewController:vc animated:YES];
                            [ws loadApplicationSkills];
                        }
                    }else {
                        if (self.tags_Normal.count) {
                            LoadInfoViewController *vc = [LoadInfoViewController new];
                            vc.hostApplyModel = self.hostApplyModel;
                            [ws.navigationController pushViewController:vc animated:YES];
                            [ws loadApplicationSkills];
                        }else {
                            NSLog(@"请选择擅长领域");
                        }
                    }
                };
                cell = (UITableViewCell*)cell_;
            }
        }else {
            BUTTONCell *cell_ = [tableView dequeueReusableCellWithIdentifier:CELLIDENTITY_BUTTON forIndexPath:indexPath];
            cell_.nextBlock = ^{
                if (ws.tags_Special.count) {
                    CertificationCell *cell = (CertificationCell*)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
                    BOOL __block res = NO;
                    [cell.dataArray enumerateObjectsUsingBlock:^(HostApplyCertModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if (obj.url == nil) {
                            res = YES;
                        }
                    }];
                    if (cell.dataArray.count == 0 || res) {
                        NSLog(@"请上传证件照，或有证件照正在上传中");
                        return ;
                    }else {
                        LoadInfoViewController *vc = [LoadInfoViewController new];
                        vc.hostApplyModel = self.hostApplyModel;
                        [ws.navigationController pushViewController:vc animated:YES];
                        [ws loadApplicationSkills];
                    }
                }else {
                    if (self.tags_Normal.count) {
                        LoadInfoViewController *vc = [LoadInfoViewController new];
                        vc.hostApplyModel = self.hostApplyModel;
                        [ws.navigationController pushViewController:vc animated:YES];
                        [ws loadApplicationSkills];
                    }else {
                        NSLog(@"请选择擅长领域");
                    }
                }
            };
            cell = (UITableViewCell*)cell_;
        }
    }
    cell.contentView.backgroundColor = BACKCOLOR;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0;
    if (indexPath.row < 4) {
        if (indexPath.row % 2 == 0) {
            height = self.title_height;
        }else {
            if (indexPath.row == 1) {
                height = CGRectGetHeight(_tagView_Normal.frame);
            }else {
                height = CGRectGetHeight(_tagView_SpecialView.frame);
            }
        }
    }else {
        if (self.tags_Special.count) {
            if (indexPath.row == 4) {
                height = self.title_height;
            }else if (indexPath.row == 5) {
                CGFloat collectionViewHeight = self.certificationCell_height;
                height = MAX(CERTIFICATEVIEWCELL_HEIGHT, collectionViewHeight);
            }else {
                height = BUTTON_TOPMARGIN + BUTTON_HEIGHT;
            }
        }else {
            height = BUTTON_TOPMARGIN + BUTTON_HEIGHT;
        }
    }
    return height;
}

@end
