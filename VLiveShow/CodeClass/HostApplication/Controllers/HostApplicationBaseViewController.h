//
//  HostApplicationBaseViewController.h
//  VLiveShow
//
//  Created by Davis on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HostApplicationBaseViewController : UIViewController

@property(nonatomic, strong)UIView *navigationView;


-(void)success:(ManagerEvent*)event;

-(void)error:(ManagerEvent*)event;

@end
