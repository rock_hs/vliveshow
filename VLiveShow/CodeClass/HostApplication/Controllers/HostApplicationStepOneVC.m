//
//  HostApplicationStepOneVC.m
//  VLiveShow
//
//  Created by Davis on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostApplicationStepOneVC.h"
#import "HostApplictionTableViewCell.h"
#import "HostApplicationCertCell.h"
#import "HostApplyProgressView.h"
#import "HostApplyStepHeaderView.h"
#import "AttributedString.h"
#import "VLSPhotoSelectView.h"
#import "HostApplyCertModel.h"
#import "VLSHostApplicationManager.h"
#import "UIAlertController+Custom.h"
#import "FDTableViewController.h"
#import "HostApplicationModel.h"
#import "HostCertButton.h"
#import "UIView+gradient.h"

@interface HostApplicationStepOneVC ()<UITableViewDelegate,UITableViewDataSource,HostApplicationCertCellDelegate,HostApplictionTableViewCellDelegate,VLSPhotoSelectViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)HostApplyProgressView *progressView;
@property(nonatomic, strong)NSArray *baseInfoArray;
@property(nonatomic, strong)NSArray *headerTitles;
@property(nonatomic, strong)NSArray *idCardMessages;

@property(nonatomic, strong)UIView *tableFooterView;
@property(nonatomic, strong)UIButton *nextStepBtn;
@property(nonatomic, strong)UITapGestureRecognizer *tap;
@property(nonatomic, strong)VLSPhotoSelectView *photoSelectView;
@property(nonatomic, strong)UIImagePickerController *imagePicker;

@property(nonatomic, weak)HostApplyCertModel *selectCertModel;
@property(nonatomic, strong)HostApplicationModel *hostApplyModel;

@property (nonatomic, assign) BOOL shouldNext;

@end

@implementation HostApplicationStepOneVC

#pragma mark - 

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.shouldNext = NO;
    [self initSubviews];
    [self httpRequestGetHostApplication];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - http request

- (void)httpRequestGetHostApplication
{
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        self.hostApplyModel = [HostApplicationModel mj_objectWithKeyValues:result];
        [self reloadData:result];
        
    };
    callback.errorBlock = ^(id result){
        
//        [self error:result];

    };
    [VLSHostApplicationManager getHostApplicationCallback:callback];
}

- (void)httpRequestUploadImage:(HostApplyCertModel*)certModel
{
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        certModel.url = [result objectForKey:@"url"];
        [self checkHostApplicationMessage];
        
    };
    
    callback.loadBlock = ^(id result){
        
        dispatch_async(dispatch_get_main_queue(), ^{
        HostApplicationCertCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        HostCertButton *btn = (HostCertButton*)[cell.buttonArray objectAtIndex:[self.idCardMessages indexOfObject:certModel]];
        
        NSNumber *sizeNum = [result objectForKey:@"size"];
        NSNumber *totalNum = [result objectForKey:@"total"];
        float process = [sizeNum floatValue]/[totalNum floatValue];
        [btn viewGradientWithStartPosition:0
                                endPositon:1
                                  position:GRADIENT_AXIS_VERTICAL
                           backgroundColor:[UIColor clearColor]
                                 maskColor:[[UIColor blackColor] colorWithAlphaComponent:.7f]
                                  duration:.5f
                                  progress:process];
        });
        
    };
    callback.errorBlock = ^(id result){
        
        
        
    };
    
    NSData *imageData = UIImagePNGRepresentation(certModel.image);

    [VLSHostApplicationManager postHostApplicationImage:imageData
                                              imageName:certModel.key
                                               callback:callback];
}

- (void)httpRequestPostHostApplication
{
    if (self.shouldNext) {
        return;
    }
    self.shouldNext = YES;
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        _hostApplyModel.applicationId = [result objectForKey:@"applicationId"];
        [self showFDTableViewCobtroller];
        self.shouldNext = NO;
        
    };
    callback.errorBlock = ^(id result){
        
        [self error:result];
        self.shouldNext = NO;
    };
    
    [VLSHostApplicationManager postHostApplication:self.hostApplyModel.baseInfoForApi callback:callback];
}


- (void)httpRequestDeleteApplicationImage:(NSString *)imageURL
{
    
    if (!imageURL) {
        
        return;
    }
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        
    };
    callback.errorBlock = ^(id result){
        
        [self error:result];
    };
    
    [VLSHostApplicationManager deleteHostApplicationImage:imageURL callback:callback];

}


#pragma mark - showVC

- (void)showFDTableViewCobtroller
{
    FDTableViewController *vc = FDTableViewController.new;
    vc.hostApplyModel = self.hostApplyModel;
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - on button action

- (void)onNextStepButtonAction
{
    [self httpRequestPostHostApplication];
}

- (void)checkHostApplicationMessage
{
    NSInteger i=0;
    
    for (HostApplictionCellModel *model in self.baseInfoArray) {
        
        if (model.content&&model.content.length>0) {
            
            [self.hostApplyModel setValue:model.content forKey:model.key];
            i++;

        }
    }
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.hostApplyModel.photo];
    
    for (HostApplyCertModel * model in self.idCardMessages) {
        
        if (model.url) {
            
            [dict setValue:model.url forKey:model.key];
            i++;
            
        }
    }
    self.hostApplyModel.photo = dict.copy;
    [self setNextStepBtnEnable:i==6];
}

- (void)reloadData:(NSDictionary *)dict
{
    for (HostApplictionCellModel *model in self.baseInfoArray) {
        
        model.content = [dict objectForKey:model.key];
    }
    
    for (HostApplyCertModel * model in self.idCardMessages) {
        
        if ([dict objectForKey:@"photo"]) {
            
            model.url = [[dict objectForKey:@"photo"] objectForKey:model.key];

        }

    }
    
    [self.tableView reloadData];
    
    [self checkHostApplicationMessage];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 3;
    }
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (indexPath.section == 0) {
        
        HostApplictionTableViewCell *aCell = [tableView dequeueReusableCellWithIdentifier:@"HostApplictionTableViewCell"];
        
        if (nil == aCell) {
            
            aCell = [[[NSBundle mainBundle]loadNibNamed:@"HostApplictionTableViewCell"
                                                  owner:self options:nil] firstObject];
            aCell.selectionStyle = UITableViewCellSelectionStyleNone;
            aCell.delegate = self;
        }
        
        [aCell reloadData:self.baseInfoArray[indexPath.row]];
        
        cell = aCell;
        
    }else
    {
        HostApplicationCertCell *aCell = [tableView dequeueReusableCellWithIdentifier:@"HostApplicationCertCell"];
        
        if (nil == aCell) {

            aCell = [[HostApplicationCertCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HostApplicationCertCell"];
            aCell.selectionStyle = UITableViewCellSelectionStyleNone;
            aCell.delegate = self;
            
            
        }
        
        [aCell reloadData:self.idCardMessages];
        
        cell = aCell;

    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 71;
    }
    
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HostApplyStepHeaderView *aView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"HostApplyStepHeaderView"];
    
    if (aView == nil) {
        
        aView = [[HostApplyStepHeaderView alloc]initWithReuseIdentifier:@"HostApplyStepHeaderView"];
    }
    aView.contentLab.attributedText = self.headerTitles[section];
    return aView;
}

#pragma mark - HostApplicationCertCellDelegate

- (void)hostApplicationCertCell:(HostApplicationCertCell *)aCell didSelectIndex:(NSInteger)index
{
    _selectCertModel = self.idCardMessages[index];
    if (_selectCertModel.image == nil) {
        
        [self.photoSelectView showPhotoView:YES animation:YES];

    }
}

- (void)shouldhostDeleteImageInCell:(NSInteger)index
{
    
    _selectCertModel = self.idCardMessages[index];

    [UIAlertController customAlertShowInViewController:self
                                             withTitle:nil
                                               message:LocalizedString(@"DELETE_IMAGE_PROMPT")
                                     cancelButtonTitle:LocalizedString(@"CANCLE")
                                destructiveButtonTitle:LocalizedString(@"YES")
                                              tapBlock:^(UIAlertController * _Nonnull controller,
                                                         UIAlertAction * _Nonnull action,
                                                         NSInteger buttonIndex) {
                                                  
                                                  if (buttonIndex == 1) {
                                                      
                                                      [self httpRequestDeleteApplicationImage:_selectCertModel.url];
                                                      _selectCertModel.image = nil;
                                                      _selectCertModel.url = nil;
                                                      [_tableView reloadData];
                                                      [self checkHostApplicationMessage];
                                                  }
                  
                                                  
                                              }];
    
    
    

    
}

#pragma mark - HostApplictionTableViewCellDelegate

- (void)hostApplictionTableViewCellValueChange
{
    [self checkHostApplicationMessage];
}


#pragma mark - UIImagePickerDelegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *) picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo {
    if (!error) {
        NSLog(@"保存到相册成功");
    }else{
        NSLog(@"保存到相册出错%@", error);
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    _selectCertModel.image = image;
    [_tableView reloadData];
    [self httpRequestUploadImage:_selectCertModel];
    [self.photoSelectView showPhotoView:NO animation:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - VLSPhotoSelectViewDelegate

- (void)jumpToSystemPhotoAlbum {
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void)jumpToSystemCamera {
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}


- (void)closePhotoSelectView
{
    
}

#pragma mark - 


- (void)setNextStepBtnEnable:(BOOL)enable
{
    _nextStepBtn.enabled = enable;
    _nextStepBtn.backgroundColor = enable?[UIColor redColor]:[UIColor clearColor];
    _nextStepBtn.layer.borderColor = enable?[UIColor clearColor].CGColor:RGB16(0x9b9b9b).CGColor;
    _nextStepBtn.layer.borderWidth = 1;
    UIColor *textColor = enable?[UIColor whiteColor]:RGB16(0x9b9b9b);
    [_nextStepBtn setTitleColor:textColor forState:UIControlStateNormal];
}

- (void)onTap
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

#pragma mark - initSubviews

- (void)initSubviews
{
    self.view.backgroundColor = RGB16(COLOR_BG_F8F8F8);
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.navigationView];
    [self.navigationView addSubview:self.progressView];
    self.navigationItem.title = LocalizedString(@"SIGNUP_VLIVESHOW_HOST");
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"1/3" style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [self.tableView addGestureRecognizer:self.tap];
    
    
    [self.view addSubview:self.photoSelectView];

}

- (UITableView *)tableView
{
    if (nil == _tableView)
    {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds
                                                 style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = self.tableFooterView;
        [_tableView setUserInteractionEnabled:YES];
    }
    return _tableView;
}


- (HostApplyProgressView *)progressView
{
    if (nil == _progressView) {
        
        _progressView = [[HostApplyProgressView alloc]initWithFrame:CGRectMake(0, 62, SCREEN_WIDTH, 2)];
        _progressView.progressValue = 1.0/3.0;
    }
    return _progressView;
}

- (NSArray *)baseInfoArray
{
    if (_baseInfoArray) {
        
        return _baseInfoArray;
        
    }
    
    HostApplictionCellModel *model = [HostApplictionCellModel new];
    model.title = LocalizedString(@"REAl_NAME");
    model.content = @"";
    model.placeholder = LocalizedString(@"INPUT_REAl_NAME");
    model.key = @"name";
    
    HostApplictionCellModel *model1 = [HostApplictionCellModel new];
    model1.title = LocalizedString(@"MINE_PERSONAL_SEX");
    model1.content = @"";
    model1.key = @"gender";
    
    
    HostApplictionCellModel *model2 = [HostApplictionCellModel new];
    model2.title = LocalizedString(@"MINE_PHONENUMBER");
    model2.content = @"";
    model2.key = @"phone";
    model2.placeholder = LocalizedString(@"INPUT_MOBILE_NUMBER");
    _baseInfoArray = @[model,model1,model2];
    
    return _baseInfoArray;
}


- (NSArray *)headerTitles
{
    if (_headerTitles) {
        
        return _headerTitles;
    }
    
    CMLinkTextViewItem *item1 = [[CMLinkTextViewItem alloc]init];
    item1.textContent = LocalizedString(@"BASEINFO");
    item1.textColor = RGB16(0x9b9b9b);
    item1.textFont = [UIFont systemFontOfSize:14];
    
    CMLinkTextViewItem *item2 = [[CMLinkTextViewItem alloc]init];
    item2.textContent = LocalizedString(@"MANDATORY");
    item2.textColor = RGB16(0xff1130);
    item2.textFont = [UIFont systemFontOfSize:12];
    
    NSAttributedString *att1 = [AttributedString attributeWithLinkTextViewItems:@[item1,item2]];
    
    CMLinkTextViewItem *item3 = [[CMLinkTextViewItem alloc]init];
    item3.textContent = LocalizedString(@"UPLOAD_ID_CERT");
    item3.textColor = RGB16(0x9b9b9b);
    item3.textFont = [UIFont systemFontOfSize:14];
    
    
    NSAttributedString *att2 = [AttributedString attributeWithLinkTextViewItems:@[item3,item2]];
    
    _headerTitles = @[att1,att2];
    
    return _headerTitles;
}

- (NSArray *)idCardMessages
{
    if (_idCardMessages) {
        
        return _idCardMessages;
    }
    
    HostApplyCertModel *model1 = [[HostApplyCertModel alloc]init];
    model1.content = LocalizedString(@"ID_CERT_FRONT");
    model1.key = @"front";
    
    HostApplyCertModel *model2 = [[HostApplyCertModel alloc]init];
    model2.content = LocalizedString(@"ID_CERT_BACK");
    model2.key = @"back";

    
    HostApplyCertModel *model3 = [[HostApplyCertModel alloc]init];
    model3.content = LocalizedString(@"ID_CERT_HOLD");
    model3.key = @"hold";

    
    _idCardMessages = @[model1,model2,model3];
    
    return _idCardMessages;
}

- (UIView*)tableFooterView
{
    if (_tableFooterView == nil) {
        
        _tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200.0*SCREEN_HEIGHT/667)];
        [_tableFooterView addSubview:self.nextStepBtn];
    }
    
    return _tableFooterView;
}

- (UIButton *)nextStepBtn
{
    if (_nextStepBtn) {
        
        return _nextStepBtn;
    }
    
    _nextStepBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _nextStepBtn.frame = CGRectMake(38,200.0*SCREEN_HEIGHT/667/2-45 , SCREEN_WIDTH-76, 40);
    [_nextStepBtn addTarget:self action:@selector(onNextStepButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [_nextStepBtn setTitle:LocalizedString(@"LOGIN_BUTTON_NEXT") forState:UIControlStateNormal];
    _nextStepBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    _nextStepBtn.layer.masksToBounds = YES;
    _nextStepBtn.layer.cornerRadius = 20;
    [self setNextStepBtnEnable:NO];
    return _nextStepBtn;
}

- (UITapGestureRecognizer *)tap
{
    if (_tap) {
        
        return _tap;
    }
    _tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTap)];
    return _tap;
}

-(VLSPhotoSelectView *)photoSelectView{
    
    if (!_photoSelectView) {
        
        _photoSelectView = [[VLSPhotoSelectView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _photoSelectView.delegate = self;
        _photoSelectView.photoLabel.text = LocalizedString(@"UPLOAD_PHOTO");
        [_photoSelectView showPhotoView:NO animation:NO];
        
    }
    return _photoSelectView;
}

- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        self.imagePicker = [[UIImagePickerController alloc] init];
        self.imagePicker.delegate = self;
        self.imagePicker.allowsEditing = YES;
        
    }
    return _imagePicker;
}


- (HostApplicationModel *)hostApplyModel
{
    if (_hostApplyModel == nil) {
        
        _hostApplyModel = [[HostApplicationModel alloc]init];
    }
    return _hostApplyModel;
}


@end
