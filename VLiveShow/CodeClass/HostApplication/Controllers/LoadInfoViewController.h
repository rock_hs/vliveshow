//
//  LoadInfoViewController.h
//  Application
//
//  Created by tom.zhu on 16/8/16.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HostApplicationBaseViewController.h"
#import "HostApplicationModel.h"

@interface LoadInfoViewController : HostApplicationBaseViewController
@property(nonatomic, strong)HostApplicationModel *hostApplyModel;
@end
