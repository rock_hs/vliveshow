//
//  HostApplicationViewController.h
//  VLiveShow
//
//  Created by Davis on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostApplicationBaseViewController.h"

typedef NS_ENUM(NSUInteger, UserApplyStatus) {
    
    UserApplyStatusNotApply,
    UserApplyStatusApplyInReview,
    UserApplyStatusApplySuccess,
    UserApplyStatusApplyFinish,
    UserApplyStatusApplyFailure,

};

@interface HostApplicationViewController : HostApplicationBaseViewController

@end
