//
//  HostApplicationBaseViewController.m
//  VLiveShow
//
//  Created by Davis on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HostApplicationBaseViewController.h"
#import "VLSPhoneLoginViewController.h"
#import "MBProgressHUD+Add.h"
#import "AppDelegate.h"

@interface HostApplicationBaseViewController ()

@property(nonatomic,strong)UIView *progressBgView;
@property(nonatomic,strong)UIView *progressView;

@end

@implementation HostApplicationBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initNar];
    
}

- (void)initNar
{
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 20, 20);
    [backButton setBackgroundImage:[UIImage imageNamed:@"ic_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(onBackBtnAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* rightItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil
                                       action:nil];
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, rightItem, nil] animated:NO];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    if (self.navigationController.viewControllers.count == 1) {
        
        [self.navigationItem setLeftBarButtonItems:nil animated:NO];
    }

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onBackBtnAction{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIView *)navigationView
{
    if (_navigationView) {
        
        return _navigationView;
    }
    _navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    _navigationView.backgroundColor = [UIColor whiteColor];
    return _navigationView;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(void)success:(ManagerEvent*)event
{
    ManagerEventType type = event.type;
    switch (type) {
        case ManagerEventToast:
        {
            NSString* title = event.title;
            NSString* info = event.info;
            MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = title;
            hud.detailsLabelText = info;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
            
        }
            break;
            
        default:
            break;
    }
}

-(void)error:(ManagerEvent*)event
{
    ManagerEventType type = event.type;
    switch (type) {
        case ManagerEventToast:
        {
            NSString* title = event.title;
            NSString* info = event.info;
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = title;
            hud.detailsLabelText = info;
            hud.removeFromSuperViewOnHide = YES;
            hud.yOffset = -120;
            [hud hide:YES afterDelay:2];
        }
            break;
            
        case ManagerEventExpired:
        {
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[AccountManager sharedAccountManager] signOut];
            [(AppDelegate *)[UIApplication sharedApplication].delegate  showLoginController];
            
        }
            break;
        case ManagerEventAlert:
        {
            NSString* title = event.title;
            NSString* info = event.info;
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:info message:title preferredStyle:UIAlertControllerStyleAlert];
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"LOGIN_PAGE_LOGIN") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                VLSPhoneLoginViewController *phoneLoginVC = [[VLSPhoneLoginViewController alloc] init];
                [self.navigationController pushViewController:phoneLoginVC animated:YES];
                
            }];
            // 取消按钮
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleCancel handler: nil];
            
            [alertController addAction:okAction];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
}


@end
