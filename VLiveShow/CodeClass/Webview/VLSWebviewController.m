//
//  VLSWebviewController.m
//  VLiveShow
//
//  Created by Davis on 16/9/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSWebviewController.h"
#import "NSObject+JSONSerialization.h"

@interface VLSWebviewController ()<UIWebViewDelegate>

@property(nonatomic,strong)UIWebView *webView;

@end

@implementation VLSWebviewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)initSubview
{
    [self.view addSubview:self.webView];
    self.webView.sd_layout.spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
}

- (UIWebView *)webView
{
    if (_webView) {
        
        _webView = [[UIWebView alloc]init];
        _webView.delegate = self;
    }
    
    return _webView;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    NSString *requestString = [[[request URL] absoluteString] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSLog(@"%@",requestString);
    NSDictionary *requestDict = [NSObject dataFormJsonString:requestString];
    NSLog(@"%@",requestDict);
    
    
    return YES;
}


//网页加载错误
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"加载错误:%@",error);

}

//网页加载成功
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"加载完成");

}

@end
