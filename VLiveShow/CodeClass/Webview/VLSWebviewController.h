//
//  VLSWebviewController.h
//  VLiveShow
//
//  Created by Davis on 16/9/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSWebviewController : UIViewController

@property(nonatomic,copy)NSArray *webpageURL;

@end
