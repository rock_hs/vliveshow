//
//  VLSHeartManager.m
//  VLiveShow
//
//  Created by Cavan on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSHeartManager.h"

@implementation VLSHeartManager

+ (VLSHeartManager *)shareHeartManager {
    static VLSHeartManager *heartManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        heartManager = [[VLSHeartManager alloc] init];
//        heartConfig = [[VLSHeartConfigModel alloc] init];
    });
    return heartManager;
}



@end
