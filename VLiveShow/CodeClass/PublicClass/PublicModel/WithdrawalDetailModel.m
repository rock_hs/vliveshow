//
//  WithdrawalDetailModel.m
//  VLiveShow
//
//  Created by tom.zhu on 16/9/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "WithdrawalDetailModel.h"

@implementation WithdrawalDetailModel
- (instancetype)initWithDic:(NSDictionary*)dic {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"]) {
        self.id_ = value;
    }else if ([key isEqualToString:@"operator"]) {
        self.operator_ = value;
    }
}

@end
