//
//  WithdrawalDetailModel.h
//  VLiveShow
//
//  Created by tom.zhu on 16/9/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WithdrawalDetailModel : NSObject
@property (nonatomic, strong) NSString *account;
@property (nonatomic, strong) NSString *accountBranch;
@property (nonatomic, strong) NSString *accountCity;
@property (nonatomic, strong) NSString *accountNumber;

@property (nonatomic, strong) NSString *accountType;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSString *cashoutNo;
@property (nonatomic, strong) NSNumber *createTime;

@property (nonatomic, strong) NSString *createTimeStr;
@property (nonatomic, strong) NSNumber *earning;
@property (nonatomic, strong) NSNumber *earning2CashRate;
@property (nonatomic, strong) NSNumber *fee;

@property (nonatomic, strong) NSNumber *feeRate;
@property (nonatomic, strong) NSNumber *id_;    //
@property (nonatomic, strong) NSString *operator_;//
@property (nonatomic, strong) NSString *remark;

@property (nonatomic, strong) NSNumber *sharingRate;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) NSNumber *taxRate;
@property (nonatomic, strong) NSNumber *totalAmount;

@property (nonatomic, strong) NSString *transNo;
@property (nonatomic, strong) NSNumber *updateTime;
@property (nonatomic, strong) NSString *updateTimeStr;
@property (nonatomic, strong) NSNumber *userId;
- (instancetype)initWithDic:(NSDictionary*)dic;
@end
