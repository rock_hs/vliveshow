//
//  VLSHeartManager.h
//  VLiveShow
//
//  Created by Cavan on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSHeartModel.h"
#import "VLSHeartConfigModel.h"



@interface VLSHeartManager : NSObject

@property (nonatomic, strong)VLSHeartModel *heartModel;
//@property (nonatomic, strong)VLSHeartConfigModel *heartConfig;

+ (VLSHeartManager *)shareHeartManager;


@end
