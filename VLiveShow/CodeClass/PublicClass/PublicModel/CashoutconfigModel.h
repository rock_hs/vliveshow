//
//  CashoutconfigModel.h
//  VLiveShow
//
//  Created by tom.zhu on 16/9/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CashoutconfigModel : NSObject
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *data;
@property (nonatomic, strong) NSString *endLimitedDate;
@property (nonatomic, strong) NSString *id_;

@property (nonatomic, strong) NSString *maxAmount;
@property (nonatomic, strong) NSString *minAmount;
@property (nonatomic, strong) NSString *multiple;
@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *remark;
@property (nonatomic, strong) NSString *startLimitedDate;
@property (nonatomic, strong) NSNumber *taxRate;
@property (nonatomic, strong) NSString *times;

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *unit;
@property (nonatomic, strong) NSString *updateTime;
- (instancetype)initWithDic:(NSDictionary*)dic;
@end
