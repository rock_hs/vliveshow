//
//  VLSHeartConfigModel.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSHeartConfigModel : NSObject

@property (nonatomic, copy) NSDictionary *firstRatio;

@property (nonatomic, copy) NSDictionary *secondRatio;

@property (nonatomic, copy) NSDictionary *thirdRatio;

@property (nonatomic, copy) NSDictionary *fourRatio;

@end
