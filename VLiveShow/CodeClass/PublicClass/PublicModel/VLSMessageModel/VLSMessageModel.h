//
//  VLSMessageModel.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSMessageModel : NSObject

//主标题|用户昵称
@property (nonatomic, copy) NSString *hostTitle;

//最新副标题内容
@property (nonatomic, copy) NSString *latestContentTitle;

//消息的条数
@property (nonatomic) NSInteger messageNumber;

//用户头像
@property (nonatomic, copy) NSString *userIcon;

//更新时间
@property (nonatomic, copy) NSString *updateTime;

//用户ID
@property (nonatomic, copy) NSString *userID;

@end
