//
//  VLSFansModel.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "BaseModel.h"

@interface VLSFansModel : BaseModel

@property (nonatomic, copy) NSString *nickName;

@property (nonatomic, copy) NSString *desc;

@property (nonatomic, copy) NSDate *timestamp;

@property (nonatomic, copy) NSString *UserID;


@property (nonatomic, assign) BOOL isread;


@end
