//
//  VLSTeamMessageModel.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "BaseModel.h"

@interface VLSTeamMessageModel : BaseModel


@property (nonatomic, copy) NSString *title;
//
@property (nonatomic, copy) NSString *desc;

@property (nonatomic, copy) NSString *content;
//
@property (nonatomic, copy) NSDate *timestamp;
//
@property (nonatomic, copy) NSString *imageURL;
//

@property (nonatomic, copy) NSString *anchorID;

@property (nonatomic, copy) NSString *anchorName;

@property (nonatomic, copy) NSString *roomID;

@property (nonatomic, assign) BOOL isread;

@end
