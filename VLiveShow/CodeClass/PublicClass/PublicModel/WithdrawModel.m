//
//  WithdrawModel.m
//  VLiveShow
//
//  Created by tom.zhu on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "WithdrawModel.h"

@implementation WithdrawModel
- (instancetype)initWithDic:(NSDictionary*)dic {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"balance"]) {
        [self setValuesForKeysWithDictionary:value];
    }
    if ([key isEqualToString:@"id"]) {
        self.id_ = value;
    }
}

@end
