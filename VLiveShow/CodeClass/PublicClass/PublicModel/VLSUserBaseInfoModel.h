//
//  VLSUserInfoModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AvatarsModel.h"

@interface VLSUserBaseInfoModel : NSObject
@property (nonatomic, strong) NSString *userId;//解析id
@property (nonatomic ,strong) NSString *avatars;//解析头像
@property (nonatomic, strong) NSString *gender;//解析性别
@property (nonatomic, strong) NSString *nickName;//解析的用户名
@property (nonatomic, strong) NSString *area;   //区域
@property (nonatomic, strong) NSString *intro;      //个性签名
@property (nonatomic, strong) NSString *isAttent;      //是否关注
@property (nonatomic, strong) NSString *isBlack;      //是否拉黑

//关注
@property (nonatomic, assign) NSInteger followNumber;
//粉丝
@property (nonatomic, assign) NSInteger fansNumber;
//好友
@property (nonatomic, assign) NSInteger friendNumber;
//黑名单
@property (nonatomic, assign) NSInteger blackNumber;

@property (nonatomic, assign) NSString *level;

/** 新增 投票字段 */
@property (nonatomic, strong) NSNumber* residueVotes;   //剩余投票次数
@property (nonatomic, strong) NSNumber* totalVotes;     //总的投票数

@end
