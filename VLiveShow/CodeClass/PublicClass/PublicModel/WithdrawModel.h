//
//  WithdrawModel.h
//  VLiveShow
//
//  Created by tom.zhu on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WithdrawModel : NSObject
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *diamond;
@property (nonatomic, strong) NSString *earning;
@property (nonatomic, strong) NSString *id_;
@property (nonatomic, strong) NSString *updateTime;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSNumber *money;
@property (nonatomic, strong) NSString *rate;
@property (nonatomic, strong) NSArray  *name;   //每个model对应内容的名称，在cell中显示用
- (instancetype)initWithDic:(NSDictionary*)dic;
@end
