//
//  VLSDoubleUpModel.h
//  VLiveShow
//
//  Created by Cavan on 16/8/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "BaseModel.h"

@interface VLSDoubleUpModel : BaseModel


@property (nonatomic, strong)NSString *min;
@property (nonatomic, strong)NSString *max;
@property (nonatomic, strong)NSString *msg;
@property (nonatomic, strong)NSString *ok_url;
@property (nonatomic, strong)NSString *type;


@end
