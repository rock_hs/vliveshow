//
//  VLSHeartModel.h
//  VLiveShow
//
//  Created by Cavan on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "BaseModel.h"

@interface VLSHeartModel : BaseModel

/// 用户标示符
@property (nonatomic, copy)NSString *roomID;
/// 直播类型状态
@property (nonatomic, copy)NSString *type;
///
@property (nonatomic, copy)NSString *name;
///
@property (nonatomic, copy)NSString *data;
///
@property (nonatomic, copy)NSString *remark;
///
@property (nonatomic, copy)NSString *updateTime;
///
@property (nonatomic, copy)NSString *createTime;
/// 直播视频支持数
@property (nonatomic, copy)NSString *maxLiveChannel;
/// 最大嘉宾申请人数
@property (nonatomic, copy)NSString *maxLiveApplication;
/// 限制数据返回数量
@property (nonatomic, copy)NSString *maxLiveApplicationReturn;
/// 心跳间隔
@property (nonatomic, copy)NSString *keepAliveInterval;
/// 心跳超时
@property (nonatomic, copy)NSString *keepAliveTimeout;

@property (nonatomic, assign)NSInteger liveVideoResolution;

@property (nonatomic, strong) NSNumber *broadcastResolutionPortrait;

@property (nonatomic, strong) NSNumber *broadcastResolutionLandscape;

@property (nonatomic, strong) NSNumber *broadcastProfile;
//主屏分辨率等级
@property (nonatomic, assign)NSInteger mainAgoraRtcVideoStream;
//小屏分辨率等级
@property (nonatomic, assign)NSInteger subAgoraRtcVideoStream;
//美颜曝光度0-1
@property (nonatomic, assign)CGFloat liveVideoLighteningFactor;

@property (nonatomic, copy) NSArray *headcount;

@property (nonatomic, assign) NSInteger liveMaxOnlineAvator;

@property (nonatomic, strong) NSArray *liveAnnouncement;

/// 版本更新
@property (nonatomic, strong)NSDictionary *doubleup;

//@property (nonatomic, copy) NSString *

@property (nonatomic, strong) NSNumber* enableH264Profile;

@end
