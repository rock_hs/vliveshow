//
//  VLSMuteModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSMuteModel : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, assign) BOOL muteVoice;
@property (nonatomic, assign) BOOL muteVoiceBySelf;

@end
