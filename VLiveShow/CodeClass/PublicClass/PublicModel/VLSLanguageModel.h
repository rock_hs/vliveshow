//
//  VLSLanguageModel.h
//  VLiveShow
//
//  Created by Cavan on 16/8/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "BaseModel.h"


@interface VLSLanguageModel : BaseModel

@property (nonatomic, strong)NSArray *cn;
@property (nonatomic, strong)NSArray *tw;
@property (nonatomic, strong)NSArray *en;

@end
