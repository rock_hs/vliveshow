//
//  VLSGuestModel.h
//  VLiveShow
//
//  Created by sp on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSGuestModel : NSObject
@property (nonatomic, copy)NSString *userId;//用户id
@property (nonatomic, copy)NSString *nickName;//名字
@property (nonatomic, copy)NSString *avatars;//头像
@end
