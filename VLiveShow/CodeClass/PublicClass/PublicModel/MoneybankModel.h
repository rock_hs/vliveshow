//
//  MoneybankModel.h
//  VLiveShow
//
//  Created by tom.zhu on 16/9/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoneybankModel : NSObject
@property (nonatomic, strong) NSString *account;
@property (nonatomic, strong) NSString *accountBranch;
@property (nonatomic, strong) NSString *accountCity;
@property (nonatomic, strong) NSString *accountNumber;

@property (nonatomic, strong) NSString *accountType;
@property (nonatomic, strong) NSNumber *createTime;
@property (nonatomic, strong) NSNumber *id_;
@property (nonatomic, strong) NSNumber *isVoid;

@property (nonatomic, strong) NSString *updateTime;
@property (nonatomic, strong) NSNumber *userId;
- (instancetype)initWithDic:(NSDictionary*)dic;
@end
