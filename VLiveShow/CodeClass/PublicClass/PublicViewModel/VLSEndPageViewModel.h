//
//  VLSEndPageViewModel.h
//  VLiveShow
//
//  Created by sp on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSEndPageViewModel : NSObject

@property (nonatomic,copy)NSArray *guestArray;
@property (nonatomic,copy)NSString *tscNum;
@property (nonatomic,copy)NSString *gainTicket;
@end
