//
//  VLSUserInfoViewModel.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSUserInfoViewModel.h"

@implementation VLSUserInfoViewModel
- (instancetype) initWidthUserInfoModel:(VLSUserBaseInfoModel *)model
{
    if (self = [super init]) {
        self.userId = model.userId;
        self.userName = model.nickName;
        self.level = model.level;
        self.userFaceURL = model.avatars;
        self.userLocation = model.area;
        self.userContent = model.intro;
        self.followNumber = model.followNumber;
        self.fansNumber = model.fansNumber;
        self.blackNumber = model.blackNumber;
        self.friendNumber = model.friendNumber;
        self.sexImageName = model.gender;
        self.isAttent = model.isAttent;
        self.isBlack = model.isBlack;
        self.residueVotes = model.residueVotes;
        self.totalVotes = model.totalVotes;
    }
    return self;
}

@end
