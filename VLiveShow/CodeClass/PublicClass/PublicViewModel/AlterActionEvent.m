//
//  AlterActionEvent.m
//  VLiveShow
//
//  Created by SuperGT on 16/7/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "AlterActionEvent.h"

@implementation AlterActionEvent

+ (AlterActionEvent *)slterWithActions:(NSArray *)actions title:(NSString *)title info:(NSString *)info
{
    AlterActionEvent *event = [[AlterActionEvent alloc]init];
    event.title = title;
    event.eventType = VLSActionEventAlter;
    event.info = info;
    return event;
}

+ (AlterActionEvent *)sheetWithActions:(NSArray *)actions title:(NSString *)title info:(NSString *)info
{
    AlterActionEvent *event = [[AlterActionEvent alloc]init];
    event.title = title;
    event.eventType = VLSActionEventSheet;
    event.info = info;
    return event;
}

+ (AlterActionEvent *)multiWithActions:(NSArray *)actions title:(NSString *)title info:(NSString *)info
{
    AlterActionEvent *event = [[AlterActionEvent alloc]init];
    event.title = title;
    event.eventType = VLSActionEventMultiAlter;
    event.info = info;
    return event;
}

@end
