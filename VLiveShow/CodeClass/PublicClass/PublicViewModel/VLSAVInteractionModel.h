//
//  VLSAVInteractionModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//  直播交互请求需要使用的中介model

#import <Foundation/Foundation.h>

@interface VLSAVInteractionModel : NSObject
@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) NSString *action;
@property (nonatomic, strong) NSString *fromAccount;
@property (nonatomic, strong) NSString *toAccount;
@property (nonatomic, strong) NSDictionary *layout;

@end
