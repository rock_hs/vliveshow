//
//  AlterActionEvent.h
//  VLiveShow
//
//  Created by SuperGT on 16/7/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, VLSActionEvent) {
    VLSActionEventAlter,
    VLSActionEventSheet,
    VLSActionEventMultiAlter,
};

@interface AlterActionEvent : NSObject

@property (nonatomic, assign) VLSActionEvent eventType;
//@property (nonatomic, copy)   NSString *     firstAction;
//@property (nonatomic, copy)   NSString *     secondAction;
@property (nonatomic, copy)   NSString *     title;
@property (nonatomic, copy)   NSString *     info;
@property (nonatomic, copy)   NSArray<NSString *>  *     actions;


+ (AlterActionEvent *)alterWithActions:(NSArray *)actions title:(NSString *)title info:(NSString *)info;

+ (AlterActionEvent *)sheetWithActions:(NSArray *)actions title:(NSString *)title info:(NSString *)info;

+ (AlterActionEvent *)multiWithActions:(NSArray *)actions title:(NSString *)title info:(NSString *)info;

@end
