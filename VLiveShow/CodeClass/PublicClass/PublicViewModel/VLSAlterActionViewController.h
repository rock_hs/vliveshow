//
//  VLSAlterActionViewController.h
//  VLiveShow
//
//  Created by SuperGT on 16/7/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlterActionEvent.h"

@protocol VLSAlterActionDelegate <NSObject>

- (void)ActionInIndex:(NSInteger)index;

@end

@interface VLSAlterActionViewController : UIViewController

@property (nonatomic, strong) AlterActionEvent *event;

+ (VLSAlterActionViewController *)show:(AlterActionEvent *)event;

@end
