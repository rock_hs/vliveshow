//
//  VLSAlterActionViewController.m
//  VLiveShow
//
//  Created by SuperGT on 16/7/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAlterActionViewController.h"

@implementation VLSAlterActionViewController

+ (VLSAlterActionViewController *)show:(AlterActionEvent *)event
{
    VLSAlterActionViewController *alterVC = [[VLSAlterActionViewController alloc]init];
    alterVC.event = event;
    return alterVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)setUpAlter
{
    
}

- (void)setUpSheet
{

}

- (void)setUpMultiAlter
{

}

@end
