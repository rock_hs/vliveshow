//
//  VLSVLiveViewModel.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//
#import "BaseModel.h"

@interface VLSVLiveViewModel : BaseModel

@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;//消息内容
@property (nonatomic) NSInteger messageNumber;//消息个数

@end
