//
//  VLSMessageViewModel.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMyMessageViewModel.h"

@implementation VLSMyMessageViewModel

- (instancetype) initWithUserInfoModel:(VLSMessageModel *)model{

    if (self == [super init]) {
        self.hostTitle = model.hostTitle;
        self.userIcon = model.userIcon;
        self.userID = model.userID;
        self.latestContentTitle = model.latestContentTitle;
        self.updateTime = model.updateTime;
    }
    return self;
}

@end
