//
//  VLSUserInfoViewModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSUserBaseInfoModel.h"

@interface VLSUserInfoViewModel : NSObject
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userFaceURL;
@property (nonatomic, strong) NSString *userLocation;
@property (nonatomic, strong) NSString *userContent;
@property (nonatomic, strong) NSString *sexImageName;//解析性别
@property (nonatomic, strong) NSString *isAttent;      //是否关注
@property (nonatomic, strong) NSString *isBlack;      //是否拉黑
//关注
@property (nonatomic, assign) NSInteger followNumber;
//粉丝
@property (nonatomic, assign) NSInteger fansNumber;
//好友
@property (nonatomic, assign) NSInteger friendNumber;
//黑名单
@property (nonatomic, assign) NSInteger blackNumber;

/** 新增 投票字段 */
@property (nonatomic, strong) NSNumber* residueVotes;   //剩余投票次数
@property (nonatomic, strong) NSNumber* totalVotes;     //总的投票数

- (instancetype) initWidthUserInfoModel:(VLSUserBaseInfoModel *)model;


@end
