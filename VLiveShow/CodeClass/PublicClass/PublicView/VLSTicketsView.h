//
//  VLSTicketsView.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSTicketsViewDelegate <NSObject>

- (void)VLSTicketsViewTap;

@end

@interface VLSTicketsView : UIView


@property (nonatomic, strong) NSString *lookNum;

@property (nonatomic, weak) id<VLSTicketsViewDelegate> delegate;

- (void)layoutTickets;
//- (void)compareLabelTureNum:(NSString *)Num;
@end
