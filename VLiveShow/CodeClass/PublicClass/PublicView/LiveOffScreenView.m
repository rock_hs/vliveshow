//
//  LiveOffScreenView.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "LiveOffScreenView.h"
#import "VLSLiveHttpManager.h"
#import "NSString+UserFile.h"
#import "VLSMultiLiveViewController.h"

#define LiveOffEndlabelFont 34


@interface LiveOffScreenView ()

@property (nonatomic, strong) UIImageView *BGview;

@property (nonatomic, strong) UILabel *endLabel;

@property (nonatomic, strong) UILabel *LookNum;

@property (nonatomic, strong) UILabel *textLabel;

@property (nonatomic, strong) UIButton *followBtn;

@property (nonatomic, strong) UIButton *backBtn;

/// 微信分享按钮
@property (nonatomic, strong)UIButton *weChatButton;
/// QQ 空间分享按钮
@property (nonatomic, strong)UIButton *zoneButton;
/// 朋友圈分享按钮
@property (nonatomic, strong)UIButton *friendShipButton;
/// QQ 分享按钮
@property (nonatomic, strong)UIButton *qqButton;
/// 微博分享按钮
@property (nonatomic, strong)UIButton *sinaButton;

@property (nonatomic, copy) NSString *roomid;

@end

@implementation LiveOffScreenView

- (instancetype)initWithFrame:(CGRect)frame RoomID:(NSString *)roomid
{
    if (self = [super initWithFrame:frame]) {
        
        self.roomid = roomid;
        
        self.BGview = [[UIImageView alloc]initWithFrame:CGRectZero];
        [self.BGview setImage:[UIImage imageNamed:@"home_bg"]];
        [self addSubview:self.BGview];
        
        /**
         *  ios8自带毛玻璃效果
         */
        UIBlurEffect *beffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *view = [[UIVisualEffectView alloc]initWithEffect:beffect];
        view.frame = self.bounds;
        
        [self addSubview:view];
        
        
        self.endLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.endLabel.font = [UIFont systemFontOfSize:LiveOffEndlabelFont];
        self.endLabel.textColor = [UIColor whiteColor];
        self.endLabel.textAlignment = NSTextAlignmentCenter;
        self.endLabel.text = LocalizedString(@"LIVE_OVER");
        [self addSubview:self.endLabel];
        
        self.LookNum = [[UILabel alloc]initWithFrame:CGRectZero];
        self.LookNum.font = [UIFont systemFontOfSize:SIZE_FONT_20];
        self.LookNum.textAlignment = NSTextAlignmentCenter;
        self.LookNum.text = @"12345人看过";
        self.LookNum.textColor = [UIColor whiteColor];
        [self addSubview:self.LookNum];
        
        self.textLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.textLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.text = LocalizedString(@"LIVE_SHARE");
        [self addSubview:self.textLabel];
        
        self.followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.followBtn setBackgroundImage:[UIImage imageNamed:@"button03"] forState:UIControlStateNormal];
        [self.followBtn addTarget:self action:@selector(followAction) forControlEvents:UIControlEventTouchUpInside];
        [self.followBtn setTitle:LocalizedString(@"FOLLOW_ANCHOR") forState:UIControlStateNormal];
        [self.followBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.followBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        [self.followBtn setTintColor:[UIColor colorWithRed:255/255.f green:120/255.f blue:74/255.f alpha:1.f]];
        
        [self addSubview:self.followBtn];
        
        
        self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.backBtn setBackgroundImage:[UIImage imageNamed:@"button03"] forState:UIControlStateNormal];
        [self.backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [self.backBtn setTitle:LocalizedString(@"BACK_HOME") forState:UIControlStateNormal];
        [self.backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.backBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        [self.backBtn setTintColor:[UIColor colorWithRed:255/255.f green:120/255.f blue:74/255.f alpha:1.f]];
        [self addSubview:self.backBtn];
        
        [self setUpUI];
        [self loadWatchNum];
        
    }
    return self;
}

- (void)setUpUI
{
    self.BGview.sd_layout
    .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
    
    self.endLabel.sd_layout
    .topSpaceToView(self,150)
    .centerXEqualToView(self)
    .heightIs(48);
    [self.endLabel setSingleLineAutoResizeWithMaxWidth:300];
    
    self.LookNum.sd_layout
    .topSpaceToView(self.endLabel,0)
    .centerXEqualToView(self)
    .heightIs(28);
    [self.LookNum setSingleLineAutoResizeWithMaxWidth:300];

    
    self.textLabel.sd_layout
    .topSpaceToView(self.LookNum,50)
    .centerXEqualToView(self)
    .heightIs(20);
    [self.textLabel setSingleLineAutoResizeWithMaxWidth:300];

    [self addSubview:self.weChatButton];
    self.weChatButton.sd_layout
    .topSpaceToView(self.textLabel, 20)
    .centerXEqualToView(self)
    .widthIs(20)
    .heightIs(20);
    
    [self addSubview:self.zoneButton];
    self.zoneButton.sd_layout
    .rightSpaceToView(self.weChatButton, 20)
    .centerYEqualToView(self.weChatButton)
    .widthIs(20)
    .heightIs(20);
    
    [self addSubview:self.friendShipButton];
    self.friendShipButton.sd_layout
    .leftSpaceToView(self.weChatButton, 20)
    .centerYEqualToView(self.weChatButton)
    .widthIs(20)
    .heightIs(20);
    
    [self addSubview:self.qqButton];
    self.qqButton.sd_layout
    .rightSpaceToView(self.zoneButton, 20)
    .centerYEqualToView(self.weChatButton)
    .widthIs(20)
    .heightIs(20);
    
    [self addSubview:self.sinaButton];
    self.sinaButton.sd_layout
    .leftSpaceToView(self.friendShipButton, 20)
    .centerYEqualToView(self.weChatButton)
    .heightIs(20)
    .widthIs(20);
    
    self.followBtn.sd_layout
    .topSpaceToView(self.weChatButton, 30)
    .leftSpaceToView(self, 30)
    .rightSpaceToView(self, 30)
    .heightIs(40);
    
    self.backBtn.sd_layout
    .topSpaceToView(self.followBtn, 30)
    .leftSpaceToView(self,30)
    .rightSpaceToView(self,30)
    .heightIs(40);
    
}

- (void)setLiveOffType:(offType)liveOffType{
    if (liveOffType == AnchorType) {
        self.followBtn.hidden = YES;
        
    }
    _liveOffType = liveOffType;
}

- (NSRange)matchingNumeber:(NSString *)string
{
    
    NSRegularExpression *regular = [NSRegularExpression regularExpressionWithPattern:@"[^\\d]*" options:0 error:NULL];

    NSString *result = [regular stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, [string length]) withTemplate:@""];
    NSRange numRan = [string rangeOfString:result];
    return numRan;
}

- (NSMutableAttributedString *)attributeStr:(NSString *)str
{
    if (!str) {
        return nil;
    }
    
    NSMutableAttributedString *attribuStr = [[NSMutableAttributedString alloc]initWithString:str];
  
    [attribuStr addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(0, str.length)];
    [attribuStr appendAttributedString:[[NSMutableAttributedString alloc]initWithString:LocalizedString(@"LOOK_NUMBER")]];
    
    return attribuStr;
}

/**
 *  关注主播按钮
 *
 *  @return void
 */

- (void)followAction
{
    if ([_delegate respondsToSelector:@selector(followAnchorClick)]) {
        [_delegate followAnchorClick];
    }
}

- (void)backAction
{
    if ([_delegate respondsToSelector:@selector(backBtnClick)]) {
        [_delegate backBtnClick];
    }
}

- (void)loadWatchNum
{
    [self setViewers:@""];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    
    callback.updateBlock = ^(id result){
    
        NSString *showNum ;
        if ([_delegate respondsToSelector:@selector(getLookNumFromMulitVC)]) {
            showNum = [_delegate getLookNumFromMulitVC];
        }
        NSInteger showint;
        NSInteger num = [result integerValue];
        if (showNum) {
            showint = [showNum integerValue];
            
            if (showint >= num) {
                num = showint;
            }
        }
        
        
        
        [self setViewers:[NSString thousandpointsNum:num]];
        [self.LookNum updateLayout];
    };
    callback.errorBlock = ^(id result){
        
    };
    
    [VLSLiveHttpManager getWatchNum:self.roomid callback:callback];
}

#pragma mark - 微信分享按钮

- (void)weChatAction:(UIButton *)button {
   
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
      [self.delegate clickShareBtnIndex:2];
    }
  
}

#pragma mark - qq 空间分享按钮

- (void)zoneAction:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
      [self.delegate clickShareBtnIndex:1];
    }
  
}
#pragma mark - 朋友圈分享按钮

- (void)friendShipAction:(UIButton *)button {
   
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
      [self.delegate clickShareBtnIndex:3];
    }
}

#pragma mark - QQ 分享按钮

- (void)qqAction:(UIButton *)button {
   
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
    [self.delegate clickShareBtnIndex:0];
    }

}
#pragma mark - 微博分享按钮

- (void)sinaAction:(UIButton *)button {
   
  
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
      [self.delegate clickShareBtnIndex:4];
    }
  
}

#pragma mark 公开接口

- (void)setViewers:(NSString *)viewers
{
    self.LookNum.attributedText = [self attributeStr:viewers];
}


- (UIButton *)weChatButton {
    if (!_weChatButton) {
        self.weChatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.weChatButton setImage:[UIImage imageNamed:@"wechat"] forState:UIControlStateNormal];
        [self.weChatButton setImage:[UIImage imageNamed:@"wechat_choose"] forState:UIControlStateSelected];
        [self.weChatButton addTarget:self action:@selector(weChatAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _weChatButton;
}

- (UIButton *)zoneButton {
    if (!_zoneButton) {
        self.zoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.zoneButton setImage:[UIImage imageNamed:@"QQzone"] forState:UIControlStateNormal];
        [self.zoneButton setImage:[UIImage imageNamed:@"QQzone_choose"] forState:UIControlStateSelected];
        [self.zoneButton addTarget:self action:@selector(zoneAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _zoneButton;
}

- (UIButton *)friendShipButton {
    if (!_friendShipButton) {
        self.friendShipButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.friendShipButton setImage:[UIImage imageNamed:@"friends-choose"] forState:UIControlStateNormal];
        [self.friendShipButton setImage:[UIImage imageNamed:@"friends_choose"] forState:UIControlStateSelected];
        [self.friendShipButton addTarget:self action:@selector(friendShipAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _friendShipButton;
}

- (UIButton *)qqButton {
    if (!_qqButton) {
        self.qqButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.qqButton setImage:[UIImage imageNamed:@"QQ"] forState:UIControlStateNormal];
        [self.qqButton setImage:[UIImage imageNamed:@"QQ_choose"] forState:UIControlStateSelected];
        [self.qqButton addTarget:self action:@selector(qqAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _qqButton;
}

- (UIButton *)sinaButton {
    if (!_sinaButton) {
        self.sinaButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.sinaButton setImage:[UIImage imageNamed:@"sina"] forState:UIControlStateNormal];
        [self.sinaButton setImage:[UIImage imageNamed:@"sina_choose"] forState:UIControlStateSelected];
        [self.sinaButton addTarget:self action:@selector(sinaAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sinaButton;
}


@end
