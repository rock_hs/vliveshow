//
//  agoraControlView.m
//  Testdemo
//
//  Created by SuperGT on 16/6/16.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import "agoraControlView.h"
#import "VLSVideoViewModel.h"
#import "agoraControlView.h"
#import "SDAutoLayout/UIView+SDAutoLayout.h"

@interface agoraControlView ()

@property (nonatomic, strong) UILabel *label;

@property (nonatomic, strong) UIView *agoraView;

@property (nonatomic, strong) UIImageView *muteImgage;

@property (nonatomic, assign) BOOL isMute;

@end

@implementation agoraControlView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.muteImgage = [[UIImageView alloc]initWithFrame:CGRectZero];
        [self addSubview:self.muteImgage];
        self.muteImgage.userInteractionEnabled = YES;
        
        [self setUp];
        
        // 添加手势
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapIcon)];
        
        [self addGestureRecognizer:tap];
    }
    
    return self;
}
- (void)setUp
{
    self.muteImgage.sd_layout
    .centerYEqualToView(self)
    .centerXEqualToView(self)
    .heightRatioToView(self,.5f)
    .widthEqualToHeight();
    
}

- (void)setModel:(VLSVideoViewModel *)model
{
    
    self.isMute = model.muteVoice;
    _model = model;
    
}

- (void)setIsMute:(BOOL)isMute
{
    if (isMute) {
        [self.muteImgage setImage:[UIImage imageNamed:@"logo@3x"]];
    }else{
        [self.muteImgage setImage:nil];
    }
    _isMute = isMute;
    self.model.muteVoice = isMute;
}

- (void)tapIcon
{
    if (_isMute) {
        self.isMute = NO;
    }else{
        self.isMute = YES;
    }
}

@end
