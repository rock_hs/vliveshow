//
//  WithdrawalDetailCellTableViewCell.h
//  Withdrawal
//
//  Created by tom.zhu on 16/8/11.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WithdrawalDetailModel;

static NSString *CELLIDENTITY_WITHDRAWDETAIL = @"withdrawdetailidentity";

@interface WithdrawalDetailCellTableViewCell : UITableViewCell
- (void)setmodel:(WithdrawalDetailModel*)model;
@end
