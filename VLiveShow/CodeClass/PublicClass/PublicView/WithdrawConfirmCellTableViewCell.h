//
//  WithdrawConfirmCellTableViewCell.h
//  Withdrawal
//
//  Created by tom.zhu on 16/8/12.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import <UIKit/UIKit.h>
static NSString *WithdrawConfirmCellTableViewCell_ID = @"WithdrawConfirmCellTableViewCell_ID";
@interface WithdrawConfirmCellTableViewCell : UITableViewCell
@property (nonatomic, strong) NSString *maxAvailable;//可提现的金额
@end
