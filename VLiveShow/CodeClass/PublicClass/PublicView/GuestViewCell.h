//
//  GuestViewCell.h
//  VLiveShow
//
//  Created by sp on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSGuestModel.h"
@interface GuestViewCell : UICollectionViewCell
@property (nonatomic,strong)UIImageView *headImageView;
@property (nonatomic,strong)UILabel * nameLabel;
@property (nonatomic,strong)VLSGuestModel *guestModel;
@end
