//
//  VLSPwdInputView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSPwdInputView.h"

@interface VLSPwdInputView ()<UITextFieldDelegate>

@property (nonatomic, strong)UILabel *lineLabel;


@end

@implementation VLSPwdInputView


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doInit];
    }
    return self;
}

- (void)doInit {
    
    [self addSubview:self.pwdTF];
    [self.pwdTF addSubview:self.lineLabel];
    
    self.pwdTF.sd_layout
    .leftSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0)
    .rightSpaceToView(self, 0);
    
    self.lineLabel.sd_layout
    .leftSpaceToView(self.pwdTF, 0)
    .rightSpaceToView(self.pwdTF, 0)
    .bottomSpaceToView(self.pwdTF, 0)
    .heightIs(0.5);
}

- (void)showWarningWithWrongPwd {
    self.lineLabel.backgroundColor = [UIColor redColor];
    self.pwdTF.textColor = [UIColor redColor];
}

- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = placeholder;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
    self.pwdTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_placeholder attributes:attributes];
    
}

- (void)eyesAction:(UIButton *)button {
    self.pwdTF.secureTextEntry = button.selected;
    button.selected = !button.selected;
    [self.pwdTF endEditing:YES];
}

- (void)pwdTFAction {
  
    if (self.pwdTF.textColor == [UIColor redColor]) {
        self.pwdTF.textColor = [UIColor blackColor];
        self.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        if ([self.delegate respondsToSelector:@selector(hidePwdInputWaring)]) {
            [self.delegate hidePwdInputWaring];
        }
    }
    
    if (self.pwdTF.text.length > 5 && self.pwdTF.text.length < 21) {
        if ([self.delegate respondsToSelector:@selector(nextActionEnable:)]) {
            [self.delegate nextActionEnable:YES];
        }
        
    } else {
        if ([self.delegate respondsToSelector:@selector(nextActionEnable:)]) {
            [self.delegate nextActionEnable:NO];
        }
    }
    
    self.pwdTF.text = [self.pwdTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.pwdTF)
    {
        if (textField.secureTextEntry)
        {
            [textField insertText:self.pwdTF.text];
        }
    }
}

#pragma mark - getters

- (UITextField *)pwdTF {
    if (!_pwdTF) {
        
        UILabel *pwdLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        pwdLabel.text = LocalizedString(@"LOGIN_PWD");
        pwdLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton setFrame:CGRectMake(0, 0, 40, 20)];
        [rightButton setImage:[UIImage imageNamed:@"login_eyes_closed"] forState:UIControlStateNormal];
        [rightButton setImage:[UIImage imageNamed:@"login_eyes_open"] forState:UIControlStateSelected];
        [rightButton addTarget:self action:@selector(eyesAction:) forControlEvents:UIControlEventTouchUpInside];
        
        self.pwdTF = [[UITextField alloc] init];
        self.pwdTF.delegate = self;
        self.pwdTF.secureTextEntry = YES;
        self.pwdTF.tintColor = [UIColor grayColor];
        
        self.pwdTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.pwdTF.keyboardType = UIKeyboardTypeASCIICapable;
        self.pwdTF.autocorrectionType = UITextAutocorrectionTypeNo;
        
        self.pwdTF.rightView = rightButton;
        self.pwdTF.leftView = pwdLabel;
        self.pwdTF.rightViewMode = UITextFieldViewModeAlways;
        self.pwdTF.leftViewMode = UITextFieldViewModeAlways;
        
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
        self.pwdTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PAGE_PASSWORD") attributes:attributes];
        
        [self.pwdTF addTarget:self action:@selector(pwdTFAction) forControlEvents:UIControlEventEditingChanged];
    }
    return _pwdTF;
}
- (UILabel *)lineLabel {
    if (!_lineLabel) {
        self.lineLabel = [[UILabel alloc] init];
        self.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
    }
    return _lineLabel;
}

@end
