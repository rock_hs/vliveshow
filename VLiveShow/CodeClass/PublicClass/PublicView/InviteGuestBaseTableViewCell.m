//
//  InviteGuestBaseTableViewCell.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "InviteGuestBaseTableViewCell.h"

@interface InviteGuestBaseTableViewCell ()

@end

@implementation InviteGuestBaseTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.face = [[UIImageView alloc]initWithFrame:CGRectZero];
    self.face.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.face];
    // 头像约束
    self.face.sd_layout
    .topSpaceToView(self.contentView,9)
    .leftSpaceToView(self.contentView,12)
    .heightIs(50)
    .widthIs(50);
    self.face.sd_cornerRadiusFromHeightRatio = @0.5f;
    self.face.layer.masksToBounds = YES;
    
    self.nickName = [[UILabel alloc]initWithFrame:CGRectZero];
    self.nickName.textColor = [UIColor blackColor];
    self.nickName.font = [UIFont systemFontOfSize:SIZE_FONT_16];
    [self.contentView addSubview:self.nickName];
    self.nickName.sd_layout
    .centerYEqualToView(self.face)
    .leftSpaceToView(self.face,12)
    .heightIs(22);
    [self.nickName setSingleLineAutoResizeWithMaxWidth:150];
    
    self.levelImage = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.levelImage.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.levelImage];
    self.levelImage.sd_layout
    .centerYEqualToView(self.nickName)
    .leftSpaceToView(self.nickName,5)
    .widthIs(30)
    .heightIs(16);
    
}

@end
