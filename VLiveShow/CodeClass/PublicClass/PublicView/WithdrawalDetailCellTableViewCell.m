//
//  WithdrawalDetailCellTableViewCell.m
//  Withdrawal
//
//  Created by tom.zhu on 16/8/11.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import "WithdrawalDetailCellTableViewCell.h"
#import "WithdrawalDetailModel.h"
#import "VLSUtily.h"

@interface WithdrawalDetailCellTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end

@implementation WithdrawalDetailCellTableViewCell
- (void)setmodel:(WithdrawalDetailModel*)model {
    [self.titleLabel setText:model.operator_];
    [self.statusLabel setText: [model.status boolValue] ? LocalizedString(@"withdraw_success") : LocalizedString(@"APPLY_INREVIEW")];
    [self.statusLabel setTextColor:[model.status boolValue] ? RGB16(COLOR_FONT_FF1130) : RGB16(COLOR_FONT_C3A851)];
    [self.contentLabel setText:[LocalizedString(@"bankCard_num") stringByAppendingString:model.accountNumber]];
    long long time = [model.createTime longLongValue] / 1000;
    NSString *timeStr = [VLSUtily getTimeByTodayWithString:[@(time) stringValue]];
    [self.timeLabel setText:timeStr];
    [self.statusLabel setTextColor:(model.status.boolValue ? RGB16(COLOR_LINE_FF1130) : RGB16(COLOR_FONT_C3A851))];
}

@end
