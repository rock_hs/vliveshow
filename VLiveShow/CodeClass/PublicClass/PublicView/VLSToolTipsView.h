//
//  VLSToolTipsView.h
//  VLiveShow
//
//  Created by SuperGT on 16/9/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^tapBlock)();

@interface VLSToolTipsView : UIView

@property (nonatomic, strong) tapBlock block;

- (instancetype)initWithFrame:(CGRect)frame WithText:(NSString *)text andAnchorPoint:(CGFloat)X tapBlock:(tapBlock)block autoDismiss:(BOOL)dismiss;

@end
