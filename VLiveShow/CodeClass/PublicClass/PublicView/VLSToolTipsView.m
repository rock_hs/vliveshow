//
//  VLSToolTipsView.m
//  VLiveShow
//
//  Created by SuperGT on 16/9/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSToolTipsView.h"

#define selfHight 50

@interface VLSToolTipsView ()

@property (nonatomic, strong) UIView *BGview;

@property (nonatomic, strong) UILabel *textLabel;

@property (nonatomic, strong) UIImageView *indictor;

@property (nonatomic, assign) BOOL dismiss;

@end

@implementation VLSToolTipsView

- (instancetype)initWithFrame:(CGRect)frame WithText:(NSString *)text andAnchorPoint:(CGFloat)X tapBlock:(tapBlock)block autoDismiss:(BOOL)dismiss
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
        self.textLabel.text = text;
        CGFloat width = self.textLabel.frame.size.width + 28;
        [self countwithX:X andWidth:width];
        self.block = block;
        self.dismiss = dismiss;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tap];
        [self checkdismiss];
    }
    return self;
}

- (void)setup
{
    self.BGview = [[UIView alloc] initWithFrame:CGRectZero];
    self.BGview.backgroundColor = RGBA16(COLOR_BG_FF1130, .85f);
    self.BGview.layer.cornerRadius = 8;
    self.BGview.layer.masksToBounds = YES;
    [self addSubview:self.BGview];
    self.BGview.sd_layout
    .topSpaceToView(self,0)
    .leftSpaceToView(self,0)
    .rightSpaceToView(self,0)
    .heightIs(40);
    
    self.indictor = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self.indictor setImage:[UIImage imageNamed:@"ic_jiao"]];
    self.indictor.contentMode = UIViewContentModeScaleAspectFill;
    self.indictor.backgroundColor = [UIColor clearColor];
//    self.indictor.alpha = .85f;
    [self addSubview:self.indictor];
    [self insertSubview:self.indictor belowSubview:self.BGview];
    self.indictor.sd_layout
    .topSpaceToView(self.BGview,0)
    .heightIs(12)
    .widthIs(22);
    
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.textLabel.textColor = RGB16(COLOR_FONT_FFFFFF);
    self.textLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
//    self.textLabel.numberOfLines = 1;
    self.textLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.textLabel];
    self.textLabel.sd_layout
    .topSpaceToView(self,0)
    .centerXEqualToView(self)
    .heightIs(40);
    [self.textLabel setSingleLineAutoResizeWithMaxWidth:SCREEN_WIDTH];
    
}

- (void)countwithX:(CGFloat)X andWidth:(CGFloat)width
{
    CGFloat offset = (X + width/2);
    CGFloat leftoffset = (X - width/2);
    if (X > SCREEN_WIDTH/2) {
        if (offset > SCREEN_WIDTH) {
            CGRect frame = CGRectMake(X - width/2 - (offset - SCREEN_WIDTH) - 15, -selfHight, width, selfHight);
            self.frame = frame;
            self.indictor.sd_layout.rightSpaceToView(self,SCREEN_WIDTH - X - self.indictor.frame.size.width/2 - 15);
        }else{
            CGRect frame = CGRectMake(X - width/2, -selfHight, width, selfHight);
            self.frame = frame;
            self.indictor.sd_layout.rightSpaceToView(self,width/2 - self.indictor.frame.size.width/2);
        }
    }else{
        if (leftoffset < 0) {
            CGRect frame = CGRectMake(15, -selfHight, width, selfHight);
            self.frame = frame;
            self.indictor.sd_layout.leftSpaceToView(self,X - self.indictor.frame.size.width/2 -15);
        }else{
            CGRect frame = CGRectMake(X - width/2, -selfHight, width, selfHight);
            self.frame = frame;
            self.indictor.sd_layout.leftSpaceToView(self,width/2 - self.indictor.frame.size.width/2);
        }
    }
}

- (void)tapAction
{
    if (self.block) {
        self.block();
    }
}

- (void)checkdismiss
{
    if (self.dismiss) {
        [UIView animateWithDuration:1.0f delay:4.0f options:UIViewAnimationOptionCurveLinear animations:^{
            self.alpha = 0.0f;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }
}


@end
