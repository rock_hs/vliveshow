//
//  VLSTextField.h
//  VLiveShow
//
//  Created by Cavan on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VLSTextField : UITextField 

@property (nonatomic, strong)UILabel *lineLabel;
@property (nonatomic, copy)NSString *placeholderStr;


@end
