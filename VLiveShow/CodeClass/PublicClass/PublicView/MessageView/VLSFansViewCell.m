//
//  VLSFansViewCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSFansViewCell.h"

@implementation VLSFansViewCell

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext(); CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor); CGContextFillRect(context, rect); //上分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_FFFFFF).CGColor); CGContextStrokeRect(context, CGRectMake(5, -1, rect.size.width - 10, 1)); //下分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_E2E2E2).CGColor); CGContextStrokeRect(context, CGRectMake(75, rect.size.height, rect.size.width - 75, 1));
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
        
        [_focusBtn addTarget:self action:@selector(focusBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)initView{
    
    //头像frame
    _headerImage = [[UIImageView alloc] initWithImage:ICON_PLACEHOLDER];
    _headerImage.frame = CGRectMake(15, 8, 50, 50);
    _headerImage.layer.cornerRadius = 25;
    _headerImage.clipsToBounds = YES;
    _headerImage.backgroundColor = [UIColor grayColor];
    _headerImage.contentMode = UIViewContentModeScaleAspectFill;
    
    //用户名
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 14, SCREEN_WIDTH - 75 - 56 - 25, 22)];
    _nameLabel.font = [UIFont systemFontOfSize:16];
    _nameLabel.textColor = RGB16(COLOR_FONT_4A4A4A);
    _nameLabel.text = LocalizedString(@"MINE_DONOTSETNICKNAME");
    //发布时间
    _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    _timeLabel.font = [UIFont systemFontOfSize:8];
    _timeLabel.textColor = RGB16(COLOR_FONT_B2B2B2);
    
    //个性签名
    _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, CGRectGetMaxY(_nameLabel.frame), 150,20)];
    _contentLabel.font = [UIFont systemFontOfSize:14];
    _contentLabel.text = LocalizedString(@"HADFOUCEDYOU");
    _contentLabel.textColor = RGB16(COLOR_FONT_979797);
    
    _focusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _focusBtn.frame = CGRectMake(SCREEN_WIDTH - 71, 21, 56,28);
    [_focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flow"] forState:UIControlStateNormal];
    _focusBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_13];
    [_focusBtn setTitle:LocalizedString(@"MINE_CHECK") forState:UIControlStateNormal];
    [_focusBtn setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateNormal];
    
    [self.contentView addSubview:_headerImage];
    [self.contentView addSubview:_nameLabel];
    [self.contentView addSubview:_contentLabel];
    [self.contentView addSubview:_focusBtn];
}

//按钮事件
- (void)focusBtnClicked:(UIButton*)sender{

    sender.selected = !sender.selected;
    if ([_delegate respondsToSelector:@selector(focusBtnViewCellBtn:)]) {
        [_delegate focusBtnViewCellBtn:self];
    }
}

//model赋值
-(void)setModel:(VLSFansModel *)model{
    _model = model;

    //头像
   [self.headerImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/v1/users/%@/avatars.png",BASE_URL,model.UserID]] placeholderImage:ICON_PLACEHOLDER];
    
    //时间
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yy/MM/dd";
//    NSString *timeStr = [self compareCurrentTime:model.timestamp];
    NSString *timeStr = [NSString compareCurrentTime:model.timestamp];
    
    //昵称
    NSMutableAttributedString *nameAttri = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",model.nickName]];
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@",timeStr]];
    [nameAttri appendAttributedString:string];
    
    // 修改富文本中的不同文字的样式
    [nameAttri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:10] range:NSMakeRange(model.nickName.length + 2, timeStr.length)];
    // 设置数字为浅灰色
    [nameAttri addAttribute:NSForegroundColorAttributeName value:RGB16(COLOR_FONT_B2B2B2) range:NSMakeRange(model.nickName.length + 2, timeStr.length)];
//    _nameLabel.attributedText = nameAttri;
    _nameLabel.text = model.nickName;
    
//    _contentLabel.text = [NSString stringWithFormat:@"%@关注了你",model.nickName];
   
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
