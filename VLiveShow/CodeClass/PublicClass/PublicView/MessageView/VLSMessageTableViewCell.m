//
//  VLSMessageTableViewCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMessageTableViewCell.h"
#import "VLSLiveHttpManager.h"
@implementation VLSMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext(); CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor); CGContextFillRect(context, rect); //上分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_FFFFFF).CGColor); CGContextStrokeRect(context, CGRectMake(0, -1, rect.size.width, 1)); //下分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_E2E2E2).CGColor); CGContextStrokeRect(context, CGRectMake(5, rect.size.height, rect.size.width - 10, 1));
}

//在该方法中实现自定义cell上相关控件的创建
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSD_Layout];
//        self.onLiving.hidden = YES;
    }
    return self;
}

- (void)setSD_Layout{
    
    _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(9, 21, 50, 50)];
    _headImageView.contentMode = UIViewContentModeScaleAspectFill;
    _headImageView.layer.masksToBounds = YES;
    _headImageView.layer.cornerRadius = 25;
    _headImageView.image = ICON_PLACEHOLDER;
    _headImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    _titleLable = [[UILabel alloc] init];
    _titleLable.font = [UIFont systemFontOfSize:16];
    _titleLable.textColor = [UIColor blackColor];
    
    _onLiving = [[UILabel alloc] init];
    _onLiving.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    _onLiving.textColor = RGB16(COLOR_BG_9B9B9B);
    _onLiving.text = LocalizedString(@"ONLIVING");
    
    _timeLabel = [[UILabel alloc] init];
    _timeLabel.textColor = RGB16(COLOR_FONT_B2B2B2);
    _timeLabel.textAlignment = NSTextAlignmentRight;
    _timeLabel.font = [UIFont systemFontOfSize:12];
    
    _contentLabel = [[UILabel alloc] init];
    _contentLabel.font = [UIFont systemFontOfSize:14];
    _contentLabel.numberOfLines = 0;
    _contentLabel.textColor = RGB16(COLOR_FONT_999999);
    
    _contentImageView = [[UIImageView alloc] init];
    _contentImageView.backgroundColor = [UIColor lightGrayColor];
    
    _playImageView = [UIImageView new];
    _playImageView.image = [UIImage imageNamed:@"but_bc_news_del"];
    _playImageView.layer.cornerRadius = 10;
    _playImageView.clipsToBounds = YES;
    _playImageView.backgroundColor = [UIColor clearColor];
    
    _intoLabel = [UILabel new];
    _intoLabel.textColor = [UIColor whiteColor];
    _intoLabel.text = LocalizedString(@"GOTOLIVEROOM");
    _intoLabel.textAlignment = NSTextAlignmentCenter;
    _intoLabel.font = [UIFont boldSystemFontOfSize:10];
    
    [self.contentView addSubview:self.headImageView];
    [self.contentView addSubview:self.contentImageView];
    [self.contentView addSubview:self.titleLable];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.contentLabel];
//    [self.contentView addSubview:self.onLiving];
    [self.contentImageView addSubview:self.playImageView];
    [self.contentImageView addSubview:self.intoLabel];
    
    
    _headImageView.sd_layout
    .leftSpaceToView(self.contentView,9)
    .topSpaceToView(self.contentView,21)
    .heightIs(50)
    .widthIs(50);
    
    
    _timeLabel.sd_layout
    .rightSpaceToView(self.contentView,19)
    .topSpaceToView(self.contentView,23)
    .widthIs(80)
    .heightIs(20);
    
    _titleLable.sd_layout
    .leftSpaceToView(self.headImageView,9)
    .rightSpaceToView(self.timeLabel, 10)
    .topSpaceToView(self.contentView,23)
    .heightIs(20);
    
    _contentLabel.sd_layout
    .leftSpaceToView(self.headImageView,9)
    .rightSpaceToView(self.contentView,20)
    .autoHeightRatio(0)
    .topSpaceToView(self.titleLable,5);
    
    
    _contentImageView.sd_layout
    .leftSpaceToView(self.headImageView,9)
    .rightSpaceToView(self.contentView,19)
    .topSpaceToView(self.contentLabel,10)
    .heightIs(0);
    
    _playImageView.sd_layout
    .rightSpaceToView(self.contentImageView, 10)
    .topSpaceToView(self.contentImageView, 10)
    .heightIs(20)
    .widthIs(40);
    
    _intoLabel.sd_layout
    .centerXEqualToView(self.playImageView)
    .centerYEqualToView(self.playImageView)
    .widthIs(40)
    .heightIs(10);
    
    [self setupAutoHeightWithBottomViewsArray:@[self.contentImageView,self.contentLabel] bottomMargin:20];
}

#pragma mark - Model赋值实现
-(void)setModel:(VLSLiveRemindModel *)model{
    _model = model;
    //主播头像
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/v1/users/%@/avatars.png",BASE_URL,model.anchorID]] placeholderImage:ICON_PLACEHOLDER];
    self.titleLable.text = model.anchorName;//主播昵称
//    _titleLable.sd_layout
//    .leftSpaceToView(self.headImageView,18)
//    .topSpaceToView(self.contentView,23)
//    .widthIs([self getWidthOfNameLabel:model.anchorName withFont:14 withSize:20] + 4)
//    .heightIs(20);
    
    self.contentLabel.text = model.content;//内容
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yy/MM/dd";
//    self.timeLabel.text = [self compareCurrentTime:model.timestamp];
    self.timeLabel.text = [NSString compareCurrentTime:model.timestamp];

    
    //直播图片
    [_contentImageView sd_setImageWithURL:[NSURL URLWithString:model.imageURL] placeholderImage:nil];
    if (model.imageURL != nil) {
        CGFloat scale =  _contentImageView.frame.size.width;
        _contentImageView.sd_layout.heightIs(scale*8.f/16.f);
    } else {
        _contentImageView.sd_layout.heightIs(0);
    }
    [self setupAutoHeightWithBottomViewsArray:@[self.contentImageView,self.contentLabel,self.headImageView] bottomMargin:20];
    
//    [self getPlayerRoomid:model.anchorID withNickName:model.anchorName];
}

//根据内容计算按钮长度
//- (CGFloat)getWidthOfNameLabel:(NSString*)aString withFont:(CGFloat)fontSize withSize:(CGFloat)fontHeight{
//    CGSize titleSize = [aString sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(MAXFLOAT, fontHeight)];
//    return titleSize.width;
//}

- (void)getPlayerRoomid:(NSString*)userID withNickName:(NSString*)name
{
    ManagerCallBack *callback1 = [[ManagerCallBack alloc] init];
    callback1.updateBlock = ^(id result){
        
        if ([result isKindOfClass:[NSNull class]]) {
            self.onLiving.hidden = YES;
            
        }else
        {
            self.onLiving.hidden = NO;
            _onLiving.sd_layout
            .leftSpaceToView(self.titleLable,0)
            .topSpaceToView(self.contentView,23)
            .heightIs(20)
            .widthIs(64);
        }
    };
    callback1.errorBlock = ^(id result){
    };
    [VLSLiveHttpManager getRoomId:userID callback:callback1];
}

@end
