//
//  VLSVLiveShowTableViewCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSVLiveShowTableViewCell.h"

@implementation VLSVLiveShowTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext(); CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor); CGContextFillRect(context, rect); //上分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_FFFFFF).CGColor); CGContextStrokeRect(context, CGRectMake(0, -1, rect.size.width, 1)); //下分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_E2E2E2).CGColor); CGContextStrokeRect(context, CGRectMake(0, rect.size.height, rect.size.width, 1));
}

//在该方法中实现自定义cell上相关控件的创建
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self loadAllViews];
    }
    return self;
}

- (void)loadAllViews{
    [self.contentView addSubview:self.head];
    [self.contentView addSubview:self.content];
    [self.head addSubview:self.headImageView];
    [self.head addSubview:self.numImageView];
    [self.head addSubview:self.numLabel];
    [self.content addSubview:self.titleLable];
    [self.content addSubview:self.contentLabel];
}

- (UIView *)head
{
    if (_head == nil) {
        _head = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60 )];
    }
    return _head;
}

- (UIView *)content
{
    if (_content == nil) {
        _content = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.head.frame) + 10, 0, self.frame.size.width - CGRectGetMaxX(self.head.frame) - 10, 60)];
    }
    return _content;
}

//头像
-(UIImageView *)headImageView{
    
    if (_headImageView == nil) {
        _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        _headImageView.center = CGPointMake(CGRectGetMidX(self.head.frame)+8, CGRectGetMidY(self.head.frame));
        _headImageView.contentMode = UIViewContentModeScaleAspectFill;
        _headImageView.layer.masksToBounds = YES;
        _headImageView.layer.cornerRadius = 20;
    }
    return _headImageView;
}

-(UIImageView *)numImageView{
    
    if (!_numImageView) {
        _numImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.headImageView.frame) - 15, 10, 22, 15)];
        _numImageView.layer.cornerRadius = 7;
        _numImageView.contentMode = UIViewContentModeScaleAspectFit;
        _numImageView.clipsToBounds = YES;
    }
    return _numImageView;
}

//消息个数
-(UILabel *)numLabel{
    
    if (_numLabel == nil) {
        _numLabel = [[UILabel alloc] initWithFrame:self.numImageView.frame];
        _numLabel.textColor = [UIColor whiteColor];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.font = [UIFont systemFontOfSize:10];
        _numLabel.text = @"19";
        _numLabel.backgroundColor = [UIColor clearColor];
    }
    return _numLabel;
}

//标题
-(UILabel *)titleLable{
    
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 200, 20)];
        _titleLable.font = [UIFont systemFontOfSize:14];

    }
    return _titleLable;
}

//内容
-(UILabel *)contentLabel{
    
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH - 70 - 40, 20)];
        _contentLabel.font = [UIFont systemFontOfSize:12];
        _contentLabel.textColor = RGB16(COLOR_FONT_979797);
    }
    return _contentLabel;
}

-(void)setModel:(VLSVLiveViewModel *)model{

    _model = model;
    self.headImageView.image = [UIImage imageNamed:_model.imageName];
    self.titleLable.text = model.title;
    self.contentLabel.text = model.content;
    self.contentLabel.frame = CGRectMake(0, 30, CGRectGetWidth(self.frame) - 70 - 40, 20);
    if (model.content == nil) {
        _titleLable.frame =  CGRectMake(0, 20, 200, 20);
    }else{
    
        _titleLable.frame =  CGRectMake(0, 10, 200, 20);
    }
    self.numLabel.text = [NSString stringWithFormat:@"%ld",model.messageNumber];
    if (model.messageNumber == 0) {
        self.numLabel.hidden = YES;
        self.numImageView.hidden = YES;
    }else if (model.messageNumber > 0 && model.messageNumber < 10){

        self.numLabel.hidden = NO;
        self.numImageView.hidden = NO;
        self.numImageView.image = [UIImage imageNamed:@"broadcast-news-v_tv-notice-tip1-9.png"];
    }else if (model.messageNumber >= 10 && model.messageNumber < 100){

        self.numLabel.hidden = NO;
        self.numImageView.hidden = NO;
        self.numImageView.image = [UIImage imageNamed:@"broadcast-news-v_tv-notice-tip10-99.png"];
    }else{
        self.numLabel.hidden = YES;
        self.numImageView.image = [UIImage imageNamed:@"broadcast-news-v_tv-notice-tip_99.png"];
    }
}




@end
