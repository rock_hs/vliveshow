//
//  VTeamTableViewCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VTeamTableViewCell.h"

@implementation VTeamTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext(); CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor); CGContextFillRect(context, rect); //上分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_FFFFFF).CGColor); CGContextStrokeRect(context, CGRectMake(5, -1, rect.size.width - 10, 1)); //下分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_E2E2E2).CGColor); CGContextStrokeRect(context, CGRectMake(0, rect.size.height, rect.size.width, 1));
}


//在该方法中实现自定义cell上相关控件的创建
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setSD_Layout];
    }
    return self;
}

- (void)setSD_Layout{
    
    _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(18, 21, 34, 34)];
    _headImageView.contentMode = UIViewContentModeScaleAspectFill;
    _headImageView.layer.masksToBounds = YES;
    _headImageView.layer.cornerRadius = 17;
    _headImageView.image = ICON_PLACEHOLDER;
    
    _titleLable = [[UILabel alloc] init];
    _titleLable.font = [UIFont systemFontOfSize:16];
    _titleLable.textColor = [UIColor blackColor];
    
    _timeLabel = [[UILabel alloc] init];
    _timeLabel.textColor = RGB16(COLOR_FONT_B2B2B2);
    _timeLabel.textAlignment = NSTextAlignmentRight;
    _timeLabel.font = [UIFont systemFontOfSize:12];
    
    _contentLabel = [[UILabel alloc] init];
    _contentLabel.font = [UIFont systemFontOfSize:14];
    _contentLabel.numberOfLines = 0;
    _contentLabel.textColor = RGB16(COLOR_FONT_999999);
    
    _contentImageView = [[UIImageView alloc] init];
    _contentImageView.backgroundColor = [UIColor lightGrayColor];
    
    [self.contentView addSubview:self.headImageView];
    [self.contentView addSubview:self.contentImageView];
    [self.contentView addSubview:self.titleLable];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.contentLabel];
    
    _timeLabel.sd_layout
    .rightSpaceToView(self.contentView,21)
    .topSpaceToView(self.contentView,21)
    .widthIs(60)
    .heightIs(20);
    
    _titleLable.sd_layout
    .leftSpaceToView(self.headImageView,10)
    .rightSpaceToView(self.timeLabel,10)
    .topSpaceToView(self.contentView,21)
    .heightIs(20);
    
    _contentLabel.sd_layout
    .leftSpaceToView(self.headImageView,10)
    .rightSpaceToView(self.contentView,21)
    .topSpaceToView(self.titleLable,10)
    .autoHeightRatio(0);
    
    _contentImageView.sd_layout
    .leftEqualToView(self.contentLabel)
    .widthRatioToView(self.contentLabel,1)
    .topSpaceToView(self.contentLabel,15);
}

#pragma mark - Model赋值实现
- (void)setModel:(VLSTeamMessageModel*)model{
    
    _model = model;

    self.titleLable.text = model.title;
    self.contentLabel.text = model.content;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yy/MM/dd";
    self.timeLabel.text = [NSString compareCurrentTime:model.timestamp];
    
    if (model.imageURL != nil) {

        //直播图片
        [_contentImageView sd_setImageWithURL:[NSURL URLWithString:model.imageURL] placeholderImage:nil];
        CGFloat scale =  _contentImageView.frame.size.width;
        _contentImageView.sd_layout.heightIs(scale*8.f/16.f);
    } else {
        _contentImageView.sd_layout.heightIs(0);
        
    }

    [self setupAutoHeightWithBottomViewsArray:@[self.contentImageView,self.headImageView] bottomMargin:20];
}


@end
