//
//  VLSFansViewCell.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSFansModel.h"
#import "NSString+UserFile.h"
@class VLSFansViewCell;
@protocol VLSFansTableViewCellDelegate <NSObject>

- (void)focusBtnViewCellBtn:(VLSFansViewCell *)cell;

@end

@interface VLSFansViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIButton *focusBtn;
@property (nonatomic, weak) id<VLSFansTableViewCellDelegate>delegate;

@property (nonatomic, strong) VLSFansModel *model;
/**
 * 计算指定时间与当前的时间差
 * @param compareDate   某一指定时间
 * @return 多少(秒or分or天or月or年)+前 (比如，3天前、10分钟前)
 */
//-(NSString *) compareCurrentTime:(NSDate*) compareDate;
@end
