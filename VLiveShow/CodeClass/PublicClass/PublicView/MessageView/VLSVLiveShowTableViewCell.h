//
//  VLSVLiveShowTableViewCell.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSVLiveViewModel.h"
@interface VLSVLiveShowTableViewCell : UITableViewCell

@property (nonatomic, strong) UIView *head;
@property (nonatomic, strong) UIView *content;

//标题
@property (nonatomic, copy) UILabel *titleLable;
//内容
@property (nonatomic, copy) UILabel *contentLabel;
//头像
@property (nonatomic, copy) UIImageView *headImageView;

//消息个数
@property (nonatomic, copy) UILabel *numLabel;

//消息图片
@property (nonatomic, copy) UIImageView *numImageView;

@property (nonatomic, strong)VLSVLiveViewModel *model;

@end
