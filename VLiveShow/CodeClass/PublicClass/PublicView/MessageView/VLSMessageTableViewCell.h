//
//  VLSMessageTableViewCell.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/27.
//  Copyright © 2016年 vliveshow. All rights reserved.


#import <UIKit/UIKit.h>
#import "VLSLiveRemindModel.h"
#import "NSString+UserFile.h"

@interface VLSMessageTableViewCell : UITableViewCell

//标题
@property (nonatomic, strong) UILabel *titleLable;

//内容
@property (nonatomic, strong) UILabel *contentLabel;

//头像
@property (nonatomic, strong) UIImageView *headImageView;

//时间
@property (nonatomic, strong) UILabel *timeLabel;

//内容图片
@property (nonatomic, strong) UIImageView *contentImageView;

//播放图片
@property (nonatomic, strong) UIImageView *playImageView;

//进入直播间
@property (nonatomic, strong) UILabel *intoLabel;

@property (nonatomic ,strong) VLSLiveRemindModel *model;

@property (nonatomic, strong)UILabel *onLiving;

//Model赋值

//-(NSString *) compareCurrentTime:(NSDate*) compareDate;

@end
