//
//  VTeamTableViewCell.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSTeamMessageModel.h"
#import "NSString+UserFile.h"

@interface VTeamTableViewCell : UITableViewCell

//标题
@property (nonatomic, strong) UILabel *titleLable;

//内容
@property (nonatomic, strong) UILabel *contentLabel;

//头像
@property (nonatomic, strong) UIImageView *headImageView;

//时间
@property (nonatomic, strong) UILabel *timeLabel;

//内容图片
@property (nonatomic, strong) UIImageView *contentImageView;

@property (nonatomic, strong) VLSTeamMessageModel *model;

/**
 * 计算指定时间与当前的时间差
 * @param compareDate   某一指定时间
 * @return 多少(秒or分or天or月or年)+前 (比如，3天前、10分钟前)
 */
//-(NSString *) compareCurrentTime:(NSDate*) compareDate;
@end
