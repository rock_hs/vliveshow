//
//  VLSPhoneInputTextField.m
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSPhoneInputView.h"
#import "VLSCountryAreaManager.h"

@interface VLSPhoneInputView ()

@property (nonatomic, strong)UILabel *lineLabel;
/// 记录操作是删除还是输入
@property (nonatomic, copy)NSString *temStr;

@end

@implementation VLSPhoneInputView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doInit];
    }
    return self;
}

- (void)doInit {
    
    [self addSubview:self.phoneTF];
    [self.phoneTF addSubview:self.lineLabel];
    
    self.phoneTF.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0);
    
    self.lineLabel.sd_layout
    .leftSpaceToView(self.phoneTF, 0)
    .rightSpaceToView(self.phoneTF, 0)
    .bottomSpaceToView(self.phoneTF, 0)
    .heightIs(0.5);
    
}

- (void)inputWrongPhoneNumber {
    
    self.lineLabel.backgroundColor = [UIColor redColor];
    self.phoneTF.textColor = [UIColor redColor];
    
}

- (void)phoneTFAction {
    self.normalPhoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (self.phoneTF.textColor == [UIColor redColor]) {
        self.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        self.phoneTF.textColor = [UIColor blackColor];
        if ([self.delegate respondsToSelector:@selector(hidePhoneInputWarning)]) {
            [self.delegate hidePhoneInputWarning];
        }
    }
    
    if (self.phoneTF.text.length > 0 &&  self.phoneTF.text.length > self.temStr.length) {
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
            [self phoneFormatWithChinaMobile];
        }
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
            NSString *firstChar = [self.phoneTF.text substringToIndex:1];
            if ([firstChar isEqualToString:@"0"]) {
                if (self.phoneTF.text.length == 10) {
                    [self.phoneTF endEditing:YES];
                    self.normalPhoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self phoneExistsOnly];
                    [self performDelegateMethodWithEnable:YES];
                }
//                if (self.phoneTF.text.length > 10) {
//                    self.phoneTF.text = [self.phoneTF.text substringToIndex:10];
//                    [self.phoneTF endEditing:YES];
//                }
            } else {
                if (self.phoneTF.text.length == 9) {
                    [self.phoneTF endEditing:YES];
                    self.normalPhoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self phoneExistsOnly];
                    [self performDelegateMethodWithEnable:YES];
                }
//                if (self.phoneTF.text.length > 9) {
//                    [self.phoneTF endEditing:YES];
//                    self.phoneTF.text = [self.phoneTF.text substringToIndex:9];
//                }
            }
        }
        
        if (![self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]
            && ![self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
            [self performDelegateMethodWithEnable:YES];
        }
        
    } else {
        
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
            if (self.normalPhoneNum.length == 11) {
                [self performDelegateMethodWithEnable:YES];
            } else {
                [self performDelegateMethodWithEnable:NO];
            }
            
        } else if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
            
            if (self.phoneTF.text.length < 2) {
                [self performDelegateMethodWithEnable:NO];
                return;
            }
            NSString *firstChar = [self.phoneTF.text substringToIndex:1];
            if ([firstChar isEqualToString:@"0"]) {
                if (self.phoneTF.text.length == 10) {
                    [self phoneExistsOnly];
                    [self performDelegateMethodWithEnable:YES];
                } else {
                    [self performDelegateMethodWithEnable:NO];
                }
            } else {
                if (self.phoneTF.text.length == 9) {
                    [self phoneExistsOnly];
                    [self performDelegateMethodWithEnable:YES];
                } else {
                    [self performDelegateMethodWithEnable:NO];
                }
                
            }
            
            
        } else {
            [self performDelegateMethodWithEnable:YES];
        }
        
        
    }
    
    
    self.temStr = self.phoneTF.text;
    self.normalPhoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
}


#pragma mark - 手机号格式化

- (void)phoneFormatWithChinaMobile {
    if (self.phoneTF.text.length == 3) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@ ",self.phoneTF.text];
        
    }
    
    if (self.phoneTF.text.length == 8) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@ ",self.phoneTF.text];
        
    }
    
    if (self.phoneTF.text.length == 13) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@",self.phoneTF.text];
        [self.phoneTF endEditing:YES];
        [self phoneExistsOnly];
        
        [self performDelegateMethodWithEnable:YES];
    }
    if (self.phoneTF.text.length > 13) {
        
        self.phoneTF.text = [self.phoneTF.text substringToIndex:13];
        [self.phoneTF endEditing:YES];
        
    }
    
}


- (void)phoneExistsOnly {
    
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.normalPhoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if ([[result objectForKey:@"existPhone"] boolValue]) {
            
            if ([self.delegate respondsToSelector:@selector(phoneNumberExisted)]) {
                [self.delegate phoneNumberExisted];
            }
            
        }
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            
        }
    };
    
    [AccountManager phoneExists:model callback:callback];
}


- (void)performDelegateMethodWithEnable:(BOOL)enable {
    if ([self.delegate respondsToSelector:@selector(inputPhoneNumberWithNextBtnEnable:)]) {
        [self.delegate inputPhoneNumberWithNextBtnEnable:enable];
    }
}

#pragma getters

- (UILabel *)countryLabel{
    if (!_countryLabel) {
        
        self.countryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        self.countryLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
        self.countryLabel.text = [VLSCountryAreaManager shareManager].area;
        
    }
    return _countryLabel;
}

- (UILabel *)lineLabel {
    if (!_lineLabel) {
        self.lineLabel = [[UILabel alloc] init];
        self.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
    }
    return _lineLabel;
}

- (UITextField *)phoneTF {
    if (!_phoneTF) {
        
        self.phoneTF = [[UITextField alloc] init];
        
        self.phoneTF.tintColor = [UIColor grayColor];
        self.phoneTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.phoneTF.returnKeyType = UIReturnKeyDone;
        [self.phoneTF setCs_acceptEventIntervalFlag:YES];

        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
        self.phoneTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PHONE_NUMBER") attributes:attributes];
        self.phoneTF.keyboardType = UIKeyboardTypeNumberPad;
        self.phoneTF.leftView = self.countryLabel;
        self.phoneTF.leftViewMode = UITextFieldViewModeAlways;
        [self.phoneTF addTarget:self action:@selector(phoneTFAction) forControlEvents:UIControlEventEditingChanged];
        
    }
    return _phoneTF;
}


@end
