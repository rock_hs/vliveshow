//
//  VLSSearchGuestTableViewCell.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//
#import "VLSLevelToImage.h"
#import "VLSSearchGuestTableViewCell.h"

#define DefaultColor [UIColor colorWithRed:255/155.f green:17/255.f blue:48/255.f alpha:1]

@interface VLSSearchGuestTableViewCell ()

@property (nonatomic, strong) UIButton *inviteButton;

@property (nonatomic, strong) UIActivityIndicatorView *indecator;

@end

@implementation VLSSearchGuestTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setup];
    }
    return self;
}

- (void)setup
{

    self.inviteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.inviteButton.layer.cornerRadius = 2;
    self.inviteButton.layer.masksToBounds = YES;
    self.inviteButton.layer.borderWidth = 1;
    self.inviteButton.layer.borderColor = DefaultColor.CGColor;
    self.inviteButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
    [self.inviteButton setTitle:LocalizedString(@"INVITE_PEOPLE") forState:UIControlStateNormal];
    [self.inviteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.inviteButton setTitle:LocalizedString(@"INVITE_DID_PEOPLE") forState:UIControlStateSelected];
    [self.inviteButton setTitleColor:DefaultColor forState:UIControlStateSelected];
    [self.inviteButton addTarget:self action:@selector(requestInvite) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.inviteButton];
    self.indecator.hidden = YES;
    self.indecator.userInteractionEnabled = NO;
    
    self.inviteButton.sd_layout
    .rightSpaceToView(self.contentView,12)
    .centerYEqualToView(self.contentView)
    .widthIs(56)
    .heightIs(28);
    
    self.indecator.sd_layout
    .centerYEqualToView(self.inviteButton)
    .centerXEqualToView(self.inviteButton)
    .heightRatioToView(self.inviteButton,1)
    .widthEqualToHeight();
    
    [self setupAutoHeightWithBottomViewsArray:@[self.face] bottomMargin:10];

}


- (void)setModel:(VLSInviteGuestModel *)model
{
    _model = model;
    if (model) {
        [self.face sd_setImageWithURL:[NSURL URLWithString:model.faceURL] placeholderImage:ICON_PLACEHOLDER];
        self.nickName.text = model.nickName;
        
        if (model.isSelected) {
            self.inviteButton.selected = YES;
            self.inviteButton.userInteractionEnabled = NO;
            self.inviteButton.backgroundColor = [UIColor whiteColor];
        }else{
            self.inviteButton.selected = NO;
            self.inviteButton.userInteractionEnabled = YES;
            self.inviteButton.backgroundColor = DefaultColor;
        }
        
        if (model.isRequest) {
            self.indecator.hidden = NO;
            self.inviteButton.titleLabel.alpha = 0;
            [self.indecator startAnimating];
        }else{
            self.indecator.hidden = YES;
            self.inviteButton.titleLabel.alpha = 1;
        }
        if (model.level) {
            NSString *imageLevel = [NSString stringWithFormat:@"%@",model.level];
            UIImage *Levelimage = [VLSLevelToImage getLevelImageWidthLevel:imageLevel];
            self.levelImage.image = Levelimage;
        }else{
            self.levelImage.image = nil;
        }
    }
}



- (UIActivityIndicatorView *)indecator {
    if (_indecator == nil) {
        self.indecator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self.inviteButton addSubview:_indecator];
    }
    return _indecator;
}

- (void)requestInvite
{
    // 开始动画
    _model.isRequest = YES;
    self.indecator.hidden = NO;
    self.inviteButton.titleLabel.alpha = 0;
    [self.indecator startAnimating];
    
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self.indecator stopAnimating];
        _model.isSelected = YES;
        self.indecator.hidden = YES;
        self.inviteButton.titleLabel.alpha = 1;
        self.inviteButton.selected = YES;
        self.inviteButton.userInteractionEnabled = NO;
        self.inviteButton.backgroundColor = [UIColor whiteColor];
        _model.isRequest = NO;

    };
    callback.errorBlock = ^(id result){
        [self.indecator stopAnimating];
        self.inviteButton.titleLabel.alpha = 1;
        _model.isRequest = NO;

    };
    if (!_model.userId) {
        _model.userId = @"0";
    }
    [[VLSAVMultiManager sharedManager] inviteHonoredGuestWithInviteIds:@[_model.userId] callback:callback];
}
@end
