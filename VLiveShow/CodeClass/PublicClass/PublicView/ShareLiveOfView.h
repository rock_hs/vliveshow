//
//  ShareLiveOfView.h
//  VLiveShow
//
//  Created by sp on 16/7/6.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSLiveHttpManager.h"
#import "MBProgressHUD.h"

@interface ShareLiveOfView : UIView

@property (nonatomic,copy)NSString * userID;
@property (nonatomic,copy)NSString * seeNum;

@property (nonatomic,assign)BOOL isFocues;
- (void)reloadContent;
- (void)judgeIsFocues;

@end
