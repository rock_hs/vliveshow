//
//  VLSVerCodeInputTextField.m
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSVerCodeInputView.h"

@interface VLSVerCodeInputView ()

@property (nonatomic, strong)UILabel *lineLabel;
@property (nonatomic, strong)UILabel *leftLabel;
@property (nonatomic, strong)UIView *rightView;
@property (nonatomic, strong)UIImageView *phoneIcon;
@property (nonatomic, strong)UIButton *verCodeBtn;


@end

@implementation VLSVerCodeInputView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doInit];
    }
    return self;
}

- (void)doInit {
    
    [self addSubview:self.verCodeTF];
    [self.verCodeTF addSubview:self.lineLabel];
    [self.rightView addSubview:self.phoneIcon];
    [self.rightView addSubview:self.verCodeBtn];
    
    self.verCodeTF.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0);
    
    self.phoneIcon.sd_layout
    .leftSpaceToView(self.rightView, 0)
    .topSpaceToView(self.rightView, 10)
    .bottomSpaceToView(self.rightView, 10)
    .widthIs(10);
    
    self.verCodeBtn.sd_layout
    .leftSpaceToView(self.phoneIcon, 0)
    .rightSpaceToView(self.rightView, 0)
    .topSpaceToView(self.rightView, 0)
    .bottomSpaceToView(self.rightView, 0);
    
    self.lineLabel.sd_layout
    .leftSpaceToView(self.verCodeTF, 0)
    .rightSpaceToView(self.verCodeTF, 0)
    .bottomSpaceToView(self.verCodeTF, 0)
    .heightIs(0.5);
    
    
    
}

- (void)verCodeInputWrong {
    self.lineLabel.backgroundColor = [UIColor redColor];
    self.verCodeTF.textColor = [UIColor redColor];
}


- (void)verCodeBtnStateWithTimeRuning:(NSString *)strTime {
    [self.verCodeBtn setTitle:[NSString stringWithFormat:@"%@s%@",strTime, LocalizedString(@"VERCODE_SEND_LATER")] forState:UIControlStateNormal];
    [self.verCodeBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    self.phoneIcon.image = [UIImage imageNamed:@"phone_gray"];
    [UIView commitAnimations];
    self.verCodeBtn.userInteractionEnabled = NO;
}

- (void)verCodeBtnStateWithTimeFinished {
    [self.verCodeBtn setTitle:LocalizedString(@"VERCODE_SEND_AGAIN") forState:UIControlStateNormal];
    [self.verCodeBtn setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateNormal];
    self.verCodeBtn.userInteractionEnabled = YES;
    self.phoneIcon.image = [UIImage imageNamed:@"phone_red"];
}

- (void)verCodeAction:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(sendVerCode:)]) {
        [self.delegate sendVerCode:button];
    }
       
}
- (void)verCodeTFAction {
    if (self.lineLabel.backgroundColor == [UIColor redColor]) {
        self.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        self.verCodeTF.textColor = [UIColor blackColor];
        if ([self.delegate respondsToSelector:@selector(hideVerCodeInputWarning)]) {
            [self.delegate hideVerCodeInputWarning];
        }
    }
    
    if (self.verCodeTF.text.length == 4) {
        if ([self.delegate respondsToSelector:@selector(inputVerCode:)]) {
            [self.delegate inputVerCode:self.verCodeTF.text];
        }
    }
}
#pragma getters

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        
        self.leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        self.leftLabel.text = LocalizedString(@"LOGIN_LABEL_VERCODE");
        self.leftLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
    
    }
    return _leftLabel;
}


- (UIView *)rightView {
    if (!_rightView) {
       
        self.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        
        
    }
    return _rightView;
}
- (UIImageView *)phoneIcon {
    if (!_phoneIcon) {
       
        
        self.phoneIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone_gray"]];
        
    }
    return _phoneIcon;
}
- (UIButton *)verCodeBtn {
    if (!_verCodeBtn) {
       
        self.verCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.verCodeBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.verCodeBtn setTitle:LocalizedString(@"LOGIN_PHONE_SEND") forState: UIControlStateNormal];
        [self.verCodeBtn addTarget:self action:@selector(verCodeAction:) forControlEvents:UIControlEventTouchUpInside];
        self.verCodeBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        
    }
    return _verCodeBtn;
}

- (UITextField *)verCodeTF {
    if (!_verCodeTF) {
        self.verCodeTF = [[UITextField alloc] init];
        self.verCodeTF.tintColor = [UIColor grayColor];
        self.verCodeTF.keyboardType = UIKeyboardTypeNumberPad;
        self.verCodeTF.leftView = self.leftLabel;
        self.verCodeTF.rightView = self.rightView;
        self.verCodeTF.leftViewMode = UITextFieldViewModeAlways;
        self.verCodeTF.rightViewMode = UITextFieldViewModeAlways;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
        self.verCodeTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PAGE_INPUTVER") attributes:attributes];
        [self.verCodeTF addTarget:self action:@selector(verCodeTFAction) forControlEvents:UIControlEventEditingChanged];
  
    }
    return _verCodeTF;
}

- (UILabel *)lineLabel {
    if (!_lineLabel) {
        self.lineLabel = [[UILabel alloc] init];
        self.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
    }
    return _lineLabel;
}


@end
