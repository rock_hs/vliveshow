//
//  VLSNetworkBreakView.h
//  VLiveShow
//
//  Created by Cavan on 16/7/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSNetworkBreakViewDelegate <NSObject>

- (void)requestLiveListDataAgain;

@end

@interface VLSNetworkBreakView : UIView

@property (nonatomic, weak)id <VLSNetworkBreakViewDelegate> delegate;
/// 提示语
@property (nonatomic, strong)UILabel *titleLabel;


@end
