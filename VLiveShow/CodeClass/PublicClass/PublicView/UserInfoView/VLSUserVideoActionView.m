//
//  VLSUserVideoActionView.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSUserVideoActionView.h"

@implementation VLSUserVideoActionView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadView];
    }
    return self;
}
- (void)loadView
{
    
//    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, 10, 1, self.frame.size.height - 20)];
//    lineView1.backgroundColor = [UIColor lightGrayColor];
//    
//    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width-20, 1)];
//    lineView2.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:self.view1];
    [self addSubview:self.view2];
//    [self addSubview:lineView1];
//    [self addSubview:lineView2];

}
- (UIView*)view1
{
    if (_view1 == nil) {
        _view1 = [[UIView alloc] initWithFrame:CGRectMake(0,0 , self.frame.size.width/2, self.frame.size.height )];
        _view1.backgroundColor = [UIColor whiteColor];
        [_view1 addSubview:self.firstBtn];

    }
    return _view1;
}

- (UIView*)view2
{
    if (_view2 == nil) {
        _view2 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, 0, self.frame.size.width/2, self.frame.size.height )];
        _view2.backgroundColor = [UIColor whiteColor];
        [_view2 addSubview:self.secondBtn];
    }
    return _view2;
}


-(UIButton *)firstBtn
{
    if (_firstBtn == nil) {
        _firstBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _firstBtn.frame = CGRectMake(10, 10, self.view1.frame.size.width-15, 38);
        _firstBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_firstBtn setTitle:LocalizedString(@"ANCHOR_PROFILE_MUTE") forState:UIControlStateNormal];
        [_firstBtn setTitle:LocalizedString(@"ANCHOR_PROFILE_UNMUTE") forState:UIControlStateSelected];
        [_firstBtn setTitle:LocalizedString(@"ANCHOR_PROFILE_UNMUTE") forState:UIControlStateDisabled];
        [_firstBtn setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateNormal];
        [_firstBtn setTitleColor:RGB16(COLOR_FONT_FFFFFF) forState:UIControlStateSelected];

        [_firstBtn setBackgroundImage:[[UIImage imageNamed:@"but_bc_callin&mute_pre.png"] stretchableImageWithLeftCapWidth:40 topCapHeight:19] forState:UIControlStateNormal];
        [_firstBtn setBackgroundImage:[[UIImage imageNamed:@"but_bc_mute_nor.png"]stretchableImageWithLeftCapWidth:40 topCapHeight:19] forState:UIControlStateSelected];


    }
    return _firstBtn;
}

-(UIButton *)secondBtn
{
    if (_secondBtn == nil) {
        _secondBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _secondBtn.frame = CGRectMake(10, 10, self.view2.frame.size.width-15, 38);
        _secondBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_secondBtn setTitle:@" " forState:UIControlStateNormal];
        [_secondBtn setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateNormal];
        [_secondBtn setTitleColor:RGB16(COLOR_FONT_FFFFFF) forState:UIControlStateSelected];

        [_secondBtn setBackgroundImage:[[UIImage imageNamed:@"but_bc_callin&mute_pre.png"] stretchableImageWithLeftCapWidth:40 topCapHeight:19] forState:UIControlStateNormal];
        [_secondBtn setBackgroundImage:[[UIImage imageNamed:@"but_bc_mute_nor.png"]stretchableImageWithLeftCapWidth:40 topCapHeight:19] forState:UIControlStateSelected];

    }
    return _secondBtn;
    
}

@end
