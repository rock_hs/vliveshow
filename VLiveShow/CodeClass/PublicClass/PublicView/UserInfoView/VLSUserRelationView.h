//
//  BtnMiddleView.h
//  VLiveShow
//
//  Created by SXW on 16/5/31.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSUserRelationView : UIView

@property (nonatomic,strong)UIView *view1;
@property (nonatomic,strong)UIView *view2;
@property (nonatomic,strong)UIView *view3;

@property (nonatomic,strong)UILabel *focusNumberLab; //关注
@property (nonatomic,strong)UIButton *focusBtn;
@property (nonatomic,strong)UILabel *fansNumberLab;//粉丝
@property (nonatomic,strong)UIButton *fansBtn;
@property (nonatomic,strong)UILabel *friendNumberLab;//直播&送出
@property (nonatomic,strong)UIButton *friendBtn;


@end
