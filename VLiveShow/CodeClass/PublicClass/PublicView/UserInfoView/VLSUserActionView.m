//
//  BtnBottomView.m
//  VLS_BarrageViewDemo
//
//  Created by SXW on 16/5/31.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import "VLSUserActionView.h"
#import "HTTPFacade.h"
@implementation VLSUserActionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadView];
    }
    return self;
}
- (void)loadView
{
    self.backgroundColor = [UIColor whiteColor];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width-20, 1)];
    lineView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:self.view1];
//    [self addSubview:self.view2];
    [self addSubview:self.view3];
    [self addSubview:self.view4];
    [self addSubview:lineView];

}
- (UIView*)view1
{
    if (_view1 == nil) {
        _view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.frame.size.width/3, self.frame.size.height-20)];
        _view1.backgroundColor = [UIColor whiteColor];
        self.firstTopBut = [UIButton buttonWithType:UIButtonTypeSystem];
        self.firstTopBut.frame = CGRectMake(0, 0, 30, self.frame.size.height);
        self.firstTopBut.center = CGPointMake(_view1.frame.size.width/2, _view1.frame.size.height/2);
        self.firstTopBut.backgroundColor = [UIColor clearColor];
        [_view1 addSubview:self.znxBtn];
        [_view1 addSubview:self.znxLabel];
        [_view1 addSubview:self.firstTopBut];
        
    }
    return _view1;
}

//- (UIView*)view2
//{
//    if (_view2 == nil) {
//        _view2 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/4 * 1, 0, self.frame.size.width/4, self.frame.size.height )];
//        _view2.backgroundColor = [UIColor whiteColor];
//        [_view2 addSubview:self.jhyBtn];
//        [_view2 addSubview:self.jhyLabel];
//    }
//    return _view2;
//}
- (UIView*)view3

{
    if (_view3 == nil) {
        _view3 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/3 * 1, 10, self.frame.size.width/3, self.frame.size.height-20)];
        _view3.backgroundColor = [UIColor whiteColor];
        self.secondTopBut = [UIButton buttonWithType:UIButtonTypeSystem];
        self.secondTopBut.frame = CGRectMake(0, 0, 30, self.frame.size.height);
        self.secondTopBut.center = CGPointMake(_view3.frame.size.width/2, _view3.frame.size.height/2);
        self.secondTopBut.backgroundColor = [UIColor clearColor];

        
        [_view3 addSubview:self.zyBtn];
        [_view3 addSubview:self.zyLabel];
        [_view3 addSubview:self.secondTopBut];
    }
    return _view3;
}

- (UIView*)view4

{
    if (_view4 == nil) {
        _view4 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/3 * 2, 10, self.frame.size.width/3, self.frame.size.height-20)];
        _view4.backgroundColor = [UIColor whiteColor];
        self.thirdTopBut = [UIButton buttonWithType:UIButtonTypeSystem];
        self.thirdTopBut.frame = CGRectMake(0, 0, 30, self.frame.size.height);
        self.thirdTopBut.center = CGPointMake(_view4.frame.size.width/2, _view4.frame.size.height/2);
        self.thirdTopBut.backgroundColor = [UIColor clearColor];
        

        [_view4 addSubview:self.jgzBtn];
        [_view4 addSubview:self.jgzLabel];
        [_view4 addSubview:self.thirdTopBut];
       
    }
    return _view4;
}

-(UIButton *)znxBtn
{
    if (_znxBtn == nil) {
        _znxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _znxBtn.frame = CGRectMake(0 , 0, 20, 20);
        _znxBtn.center = CGPointMake(_view1.frame.size.width/2, _view1.frame.size.height/10*3);
        
        [_znxBtn setImage:[UIImage imageNamed:@"mine_blacklist"] forState:UIControlStateNormal];
    }
    return _znxBtn;
}

-(UILabel *)znxLabel
{
    if (_znxLabel == nil) {
        _znxLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view1.frame.size.height/5*3, self.view1.frame.size.width, self.view1.frame.size.height/5*2)];
        _znxLabel.textAlignment = NSTextAlignmentCenter;
        _znxLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        _znxLabel.font = [UIFont systemFontOfSize:13];
        _znxLabel.text = LocalizedString(@"ANCHOR_PROFILE_PULL_BLACK");
    }
    return _znxLabel;
    
}

-(UIButton *)jhyBtn
{
    if (_jhyBtn == nil) {
        
        _jhyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _jhyBtn.frame = CGRectMake(0 , 0, 20, 20);
        _jhyBtn.center = CGPointMake(_view1.frame.size.width/2, _view1.frame.size.height/10*3);
//        [_jhyBtn setBackgroundColor:[UIColor greenColor]];
        [_jhyBtn setImage:[UIImage imageNamed:LocalizedString(@"ANCHOR_PROFILE_ADD_FRIEND")] forState:UIControlStateNormal];
    }
    return _jhyBtn;
}

-(UILabel *)jhyLabel
{
    if (_jhyLabel == nil) {
        _jhyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view1.frame.size.height/5*3, self.view2.frame.size.width, self.view2.frame.size.height/5*2)];
        _jhyLabel.textAlignment = NSTextAlignmentCenter;
        _jhyLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        _jhyLabel.font = [UIFont systemFontOfSize:12];
        _jhyLabel.text = LocalizedString(@"ANCHOR_PROFILE_ADD_FRIEND");
    }
    return _jhyLabel;
    
}

-(UIButton *)zyBtn
{
    if (_zyBtn == nil) {
        _zyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _zyBtn.frame = CGRectMake(0 , 0, 20, 20);
        _zyBtn.center = CGPointMake(_view1.frame.size.width/2, _view1.frame.size.height/10*3);
        [_zyBtn setImage:[UIImage imageNamed:@"home_mine"] forState:UIControlStateNormal];
    }
    return _zyBtn;
    
}

-(UILabel *)zyLabel
{
    if (_zyLabel == nil) {
        _zyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view1.frame.size.height/5*3, self.view3.frame.size.width, self.view3.frame.size.height/5*2)];
        _zyLabel.textAlignment = NSTextAlignmentCenter;
        _zyLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        _zyLabel.font = [UIFont systemFontOfSize:12];
        _zyLabel.text = LocalizedString(@"ANCHOR_PROFILE_HOME");
    }
    return _zyLabel;
}

-(UIButton *)jgzBtn
{
    
    if (_jgzBtn == nil) {
        _jgzBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _jgzBtn.frame = CGRectMake(0 , 0, 20, 20);
        _jgzBtn.center = CGPointMake(_view1.frame.size.width/2, _view1.frame.size.height/10*3);
//                [_jgzBtn addTarget:self action:@selector(focusAction:) forControlEvents:UIControlEventTouchUpInside];
        [_jgzBtn setImage:[UIImage imageNamed:@"attention02"] forState:UIControlStateNormal];
        [_jgzBtn setImage:[UIImage imageNamed:@"attention02"] forState:UIControlStateSelected];
    }
    return _jgzBtn;
    
}

-(UILabel *)jgzLabel
{
    if (_jgzLabel == nil) {
        _jgzLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view1.frame.size.height/5*3, self.view4.frame.size.width, self.view4.frame.size.height/5*2)];
        _jgzLabel.textAlignment = NSTextAlignmentCenter;
        _jgzLabel.font = [UIFont systemFontOfSize:12];
        _jgzLabel.text = LocalizedString(@"MINE_FOCUS");
        _jgzLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    }
    return _jgzLabel;
}

//- (void)focusAction:(UIButton*)sender
//{
//    sender.selected = !sender.selected;
//    if ([self.delegate respondsToSelector:@selector(getFocuse:)]) {
//        [self.delegate getFocuse:sender];
//    }
//
//}


@end
