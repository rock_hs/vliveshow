//
//  VLSUserInviteView.h
//  VLiveShow
//
//  Created by LK on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSUserInviteView : UIView

@property (nonatomic, strong) UIButton *invitBtn;

@end
