//
//  VLSUserInviteView.m
//  VLiveShow
//
//  Created by LK on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSUserInviteView.h"

@implementation VLSUserInviteView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadView];
    }
    return self;
}
- (void)loadView
{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.invitBtn];
    
}

-(UIButton *)invitBtn
{
    
    if (_invitBtn == nil) {
        _invitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _invitBtn.frame = CGRectMake(20 , 11, self.bounds.size.width-40, 38);
        _invitBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        [_invitBtn setBackgroundColor:[UIColor whiteColor]];
        [_invitBtn setBackgroundImage:[[UIImage imageNamed:@"but_bc_mute_nor"] stretchableImageWithLeftCapWidth:30 topCapHeight:19] forState:UIControlStateNormal];
        [_invitBtn setImage:[UIImage imageNamed:@"ic_bc_callin.png"] forState:UIControlStateNormal];
        [_invitBtn setTitle:LocalizedString(@"INVITE_CONNECT") forState:UIControlStateNormal];
        [_invitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    }
    return _invitBtn;
    
}

@end
