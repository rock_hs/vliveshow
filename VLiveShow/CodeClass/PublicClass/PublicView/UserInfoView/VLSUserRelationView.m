//
//  BtnMiddleView.m
//  VLiveShow
//
//  Created by SXW on 16/5/31.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSUserRelationView.h"

@implementation VLSUserRelationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadView];
    }
    return self;
}
- (void)loadView
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, 15, 1, self.frame.size.height - 30)];
    lineView.backgroundColor = [UIColor lightGrayColor];
    
    [self addSubview:self.view1];
    [self addSubview:self.view2];
    [self addSubview:lineView];
    
}
- (UIView*)view1
{
    if (_view1 == nil) {
        _view1 = [[UIView alloc] initWithFrame:CGRectMake(0,0 , self.frame.size.width/2, self.frame.size.height )];
        _view1.backgroundColor = [UIColor whiteColor];
        [_view1 addSubview:self.focusNumberLab];
//        [_view1 addSubview:self.focusBtn];
    }
    return _view1;
}

- (UIView*)view2
{
    if (_view2 == nil) {
        _view2 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, 0, self.frame.size.width/2, self.frame.size.height )];
        _view2.backgroundColor = [UIColor whiteColor];
//        [_view2 addSubview:self.fansBtn];
        [_view2 addSubview:self.fansNumberLab];
    }
    return _view2;
}
//- (UIView*)view3
//
//{
//    if (_view3 == nil) {
//        _view3 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/3*2, 0, self.frame.size.width/3, self.frame.size.height)];
//        _view3.backgroundColor = [UIColor whiteColor];
//        [_view3 addSubview:self.friendNumberLab];
//        [_view3 addSubview:self.friendBtn];
//    }
//    return _view3;
//}
- (UILabel*)focusNumberLab
{
    if (_focusNumberLab == nil) {
        _focusNumberLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, self.view1.frame.size.width- 20, 30)];
       
        _focusNumberLab.textAlignment = NSTextAlignmentRight;
        _focusNumberLab.font = [UIFont systemFontOfSize:12];
//        _focusNumberLab.text = @"0";
        _focusNumberLab.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        _focusNumberLab.backgroundColor = [UIColor clearColor];
    }
    return _focusNumberLab;
}
-(UIButton *)focusBtn
{
    if (_focusBtn == nil) {
        _focusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _focusBtn.frame = CGRectMake(self.view1.frame.size.width -85, 5, 40, 30);
       
        _focusBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _focusBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_focusBtn setTitle:LocalizedString(@"MINE_FOCUS") forState:UIControlStateNormal];
        [_focusBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_focusBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    return _focusBtn;
}

- (UILabel*)fansNumberLab
{
    if (_fansNumberLab == nil) {
        _fansNumberLab = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, self.view2.frame.size.width-20, 30)];
        _fansNumberLab.textAlignment = NSTextAlignmentLeft;
        _fansNumberLab.font = [UIFont systemFontOfSize:12];
//        _fansNumberLab.text = @"0";
        _fansNumberLab.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        _fansNumberLab.backgroundColor = [UIColor clearColor];
    }
    return _fansNumberLab;
    
}
-(UIButton *)fansBtn
{
    if (_fansBtn == nil) {
        _fansBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _fansBtn.frame = CGRectMake(20, 5, 40, 30);
        
        _fansBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _fansBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        [_fansBtn setTitleColor:[UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        _fansBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_fansBtn
         setTitle:LocalizedString(@"MINE_FANS") forState:UIControlStateNormal];
    }
    return _fansBtn;
    
}
-(UILabel *)friendNumberLab
{
    if (_friendNumberLab == nil) {
        _friendNumberLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view3.frame.size.width, self.view3.frame.size.height/5*3)];
        _friendNumberLab.textAlignment = NSTextAlignmentCenter;
        _friendNumberLab.font = [UIFont systemFontOfSize:16];
        _friendNumberLab.text = @"0";
    }
    return _friendNumberLab;
    
    
}
-(UIButton *)friendBtn
{
    if (_friendBtn == nil) {
        _friendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _friendBtn.frame = CGRectMake(0, self.view1.frame.size.height/5*3, self.view1.frame.size.width, self.view1.frame.size.height/5*2);
        [_friendBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _friendBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        [_friendBtn setTitle:LocalizedString(@"MINE_FRIENDS") forState:UIControlStateNormal];
    }
    return _friendBtn;
    
}

@end
