//
//  VLSUserVideoActionView.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSUserVideoActionView : UIView

@property (nonatomic,strong)UIView *view1;
@property (nonatomic,strong)UIView *view2;

@property (nonatomic,strong)UIButton *firstBtn;
@property (nonatomic,strong)UIButton *secondBtn;

@end
