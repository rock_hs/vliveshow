//
//  BtnBottomView.h
//  VLS_BarrageViewDemo
//
//  Created by SXW on 16/5/31.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VLSUserActionView : UIView

@property (nonatomic,strong)UIView *view1;
@property (nonatomic,strong)UIView *view2;
@property (nonatomic,strong)UIView *view3;
@property (nonatomic,strong)UIView *view4;


@property (nonatomic,strong)UIButton *firstTopBut;
@property (nonatomic,strong)UIButton *secondTopBut;
@property (nonatomic,strong)UIButton *thirdTopBut;

@property (nonatomic,strong)UIButton *znxBtn;
@property (nonatomic,strong)UILabel *znxLabel; //站内信

@property (nonatomic,strong)UIButton *jhyBtn;
@property (nonatomic,strong)UILabel *jhyLabel;//加好友

@property (nonatomic,strong)UIButton *zyBtn;
@property (nonatomic,strong)UILabel *zyLabel;//主页
@property (nonatomic,strong)UIButton *jgzBtn;
@property (nonatomic,strong)UILabel *jgzLabel;//加关注
@property (nonatomic,strong)VLSUserProfile* vlModel;



@end
