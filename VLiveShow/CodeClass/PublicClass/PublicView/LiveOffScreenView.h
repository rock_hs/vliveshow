//
//  LiveOffScreenView.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeaveOffViewController.h"
typedef NS_ENUM(NSInteger, offType) {
    AnchorType = 1,
    Audience
};

@protocol LiveOffClick <NSObject>

- (void)followAnchorClick;

- (void)backBtnClick;

- (void)clickShareBtnIndex:(NSInteger)index;

- (NSString *)getLookNumFromMulitVC;

@end

@interface LiveOffScreenView : UIView

@property (nonatomic, assign) offType liveOffType;

@property (nonatomic, weak) id<LiveOffClick> delegate;

- (instancetype)initWithFrame:(CGRect)frame RoomID:(NSString *)roomid;

@end
