//
//  InviteGuestBaseTableViewCell.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSInviteGuestModel.h"

@interface InviteGuestBaseTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *face;

@property (nonatomic, strong) UILabel *nickName;

@property (nonatomic, strong) UILabel *lineCountLabel;

@property (nonatomic, strong) UIImageView *levelImage;

@end
