//
//  VLSVerCodeInputTextField.h
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSVerCodeInputViewDelegate <NSObject>

- (void)inputVerCode:(NSString *)verCodeStr;
- (void)sendVerCode:(UIButton *)button;
- (void)hideVerCodeInputWarning;

@end
@interface VLSVerCodeInputView : UIView

@property (nonatomic, strong)UITextField *verCodeTF;
@property (nonatomic, weak)id <VLSVerCodeInputViewDelegate> delegate;
- (void)verCodeInputWrong;
- (void)verCodeBtnStateWithTimeFinished;
- (void)verCodeBtnStateWithTimeRuning:(NSString *)strTime;

@end
