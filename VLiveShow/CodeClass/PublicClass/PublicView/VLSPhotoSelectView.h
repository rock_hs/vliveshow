//
//  VLSPhotoSelectView.h
//  VLiveShow
//
//  Created by Cavan on 16/6/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSPhotoSelectViewDelegate <NSObject>

/** 跳转到系统相册，从相册选取图片 */
- (void)jumpToSystemPhotoAlbum;
/** 跳转到系统相机，使用相机拍照 */
- (void)jumpToSystemCamera;
/** 关闭相册选取通用组件 */
- (void)closePhotoSelectView;

@end

@interface VLSPhotoSelectView : UIView

/** 相册选取通用组件的标题 */
@property (nonatomic, strong) UILabel *photoLabel;

@property (nonatomic, weak) id <VLSPhotoSelectViewDelegate> delegate;

- (void)showPhotoView:(BOOL)show animation:(BOOL)animation;


@end
