//
//  WithdrawConfirmCellTableViewCell.m
//  Withdrawal
//
//  Created by tom.zhu on 16/8/12.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import "WithdrawConfirmCellTableViewCell.h"

NSString *FONT = @"PingFang SC";

@interface WithdrawConfirmCellTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *label;
@end

@implementation WithdrawConfirmCellTableViewCell
- (void)setMaxAvailable:(NSString *)maxAvailable {
    _maxAvailable = maxAvailable;
    NSString *max = [@"￥" stringByAppendingString:maxAvailable];
    [self.label setText:max];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    UILabel *label = UILabel.new;
    label.font = [UIFont fontWithName:FONT size:30];
    label.text = @"￥";
    label.textColor = RGB16(COLOR_FONT_4A4A4A);
    [label sizeToFit];
}

@end
