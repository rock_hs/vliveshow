//
//  coverImage.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ANCHOR_MESSAGE_TYPE) {
    ANCHOR_LEAVE,
    ANCHOR_BACK
};

@interface coverImage : UIView

@property (nonatomic, strong) UIImageView *coverbg;

@property (nonatomic, assign) ANCHOR_MESSAGE_TYPE type;

- (instancetype)initWithFrame:(CGRect)frame WithURL:(NSString *)URL;

@end
