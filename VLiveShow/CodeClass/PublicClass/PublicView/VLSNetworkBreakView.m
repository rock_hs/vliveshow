//
//  VLSNetworkBreakView.m
//  VLiveShow
//
//  Created by Cavan on 16/7/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSNetworkBreakView.h"

@interface VLSNetworkBreakView ()

/// 点击提示图片
@property (nonatomic, strong)UIImageView *tapImageView;

@end

@implementation VLSNetworkBreakView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        
        [self addSubview:self.tapImageView];
        self.tapImageView.sd_layout
        .centerXEqualToView(self)
        .topSpaceToView(self, SCREEN_HEIGHT / 2 - 150)
        .widthIs(60)
        .heightIs(80);
        
        [self addSubview:self.titleLabel];
        self.titleLabel.sd_layout
        .leftSpaceToView(self, SCREEN_WIDTH / 2 - 55)
        .topSpaceToView(self.tapImageView, 10)
        .widthIs(100)
        .heightIs(40);
    }
    return self;
}


#pragma mark - getters


- (UIImageView *)tapImageView {
    if (!_tapImageView) {
        self.tapImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_click"]];
    }
    return _tapImageView;
}


- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.text = [NSString stringWithFormat:@"%@\n%@", LocalizedString(@"NETWORK_NOT_BEST"), LocalizedString(@"NETWORK_CLICK_LOAD")];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    }
    return _titleLabel;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    self.titleLabel.text = LocalizedString(@"PLEASE_TRY_AGARN");
    if ([self.delegate respondsToSelector:@selector(requestLiveListDataAgain)]) {
        [self.delegate requestLiveListDataAgain];
    }
}

@end
