//
//  VLSInviteGuestCell.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSInviteGuestCell.h"
#import "VLSLevelToImage.h"

@interface VLSInviteGuestCell ()

@property (nonatomic, strong) UIButton *button;

@end

@implementation VLSInviteGuestCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.button setImage:[UIImage imageNamed:@"but_bc_uncheck"] forState:UIControlStateNormal];
    [self.button setImage:[UIImage imageNamed:@"but_bc_check"] forState:UIControlStateSelected];
    self.button.userInteractionEnabled = NO;
    [self.contentView addSubview:self.button];
    // button约束
    self.button.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(self.contentView,26)
    .heightIs(20)
    .widthIs(20);
    
    [self setupAutoHeightWithBottomViewsArray:@[self.face] bottomMargin:10];
}
- (void)seletedStatus
{
    if (_model.isSelected) {
        self.button.selected = NO;
        _model.isSelected = NO;
    }else{
        self.button.selected = YES;
        _model.isSelected = YES;
    }
}

- (void)setModel:(VLSInviteGuestModel *)model
{
    _model = model;
    if (model) {
        [self.face sd_setImageWithURL:[NSURL URLWithString:model.faceURL] placeholderImage:ICON_PLACEHOLDER];
        self.nickName.text = model.nickName;
        if (model.isSelected) {
            self.button.selected = YES;
        }else{
            self.button.selected = NO;
        }
        if (model.level) {
            NSString *imageLevel = [NSString stringWithFormat:@"%@",model.level];
            UIImage *Levelimage = [VLSLevelToImage getLevelImageWidthLevel:imageLevel];
            self.levelImage.image = Levelimage;
        }else{
            self.levelImage.image = nil;
        }
    }
}

@end
