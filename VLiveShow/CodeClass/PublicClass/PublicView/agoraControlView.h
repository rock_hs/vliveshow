//
//  agoraControlView.h
//  Testdemo
//
//  Created by SuperGT on 16/6/16.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSVideoViewModel.h"

@interface agoraControlView : UIView

@property (nonatomic, strong) VLSVideoViewModel *model;


@end
