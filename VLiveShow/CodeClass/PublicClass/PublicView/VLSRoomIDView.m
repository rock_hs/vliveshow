//
//  VLSRoomIDView.m
//  VLiveShow
//
//  Created by SuperGT on 16/7/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSRoomIDView.h"


@interface VLSRoomIDView (){

    UILabel *roomLabel;
    UILabel *dateLabel;
}

@end

@implementation VLSRoomIDView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    if (self = [super init]) {
        
        // 给出初始frame
        self.frame = CGRectMake(SCREEN_WIDTH - 200 - 10, 98, 200, 30);
        
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    roomLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    roomLabel.textColor = [UIColor whiteColor];
    roomLabel.font = [UIFont systemFontOfSize:SIZE_FONT_10];
    roomLabel.textAlignment = NSTextAlignmentRight;
    
    roomLabel.shadowColor = [UIColor colorWithWhite:0.8f alpha:0.2f];    //设置文本的阴影色彩和透明度。
    roomLabel.shadowOffset = CGSizeMake(0.0f, 2.0f);
    
    [self addSubview:roomLabel];
    
    dateLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    dateLabel.textColor = [UIColor whiteColor];
    dateLabel.font = [UIFont systemFontOfSize:SIZE_FONT_10];
    dateLabel.textAlignment = NSTextAlignmentRight;
    
    dateLabel.shadowColor = [UIColor colorWithWhite:0.8f alpha:0.2f];    //设置文本的阴影色彩和透明度。
    dateLabel.shadowOffset = CGSizeMake(0.0f, 2.0f);
    
    [self addSubview:dateLabel];
    
    self.backgroundColor = [UIColor clearColor];
    
    [self layoutWholeView];
}

/**
 *  布局约束
 */

- (void)layoutWholeView
{
    roomLabel.sd_layout
    .topSpaceToView(self,0)
    .leftSpaceToView(self,0)
    .rightSpaceToView(self,0)
    .heightIs(14);
    
    dateLabel.sd_layout
    .topSpaceToView(roomLabel,0)
    .leftSpaceToView(self,0)
    .rightSpaceToView(self,0)
    .heightIs(14);
    
}
/**
 *  当前时间转化成string
 *
 *
 *
 *  @return 时间字符串
 */
- (NSString *)nowDateFormatter
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    dateformatter.dateFormat = [NSString stringWithFormat:@"yyyy/MM/dd"];
    return [dateformatter stringFromDate:date];
}

- (void)setRoomID:(NSString *)RoomID
{
    _RoomID = RoomID;
    roomLabel.text = [NSString stringWithFormat:@"%@%@",LocalizedString(@"INVITE_ROOMID"),_RoomID];
    dateLabel.text = [self nowDateFormatter];
}

- (void)layoutIsRight:(BOOL)isRight
{
    if (isRight) {
        roomLabel.textAlignment = NSTextAlignmentRight;
        dateLabel.textAlignment = NSTextAlignmentRight;
        self.frame = CGRectMake(SCREEN_WIDTH - 200 - 10, 98, 200, 30);
    }else{
        roomLabel.textAlignment = NSTextAlignmentLeft;
        dateLabel.textAlignment = NSTextAlignmentLeft;
        self.frame = CGRectMake(10, 26, 200, 30);
    }
}
@end
