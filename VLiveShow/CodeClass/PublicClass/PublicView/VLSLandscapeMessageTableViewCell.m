//
//  VLSLandscapeMessageTableViewCell.m
//  VLiveShow
//
//  Created by tom.zhu on 2016/12/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLandscapeMessageTableViewCell.h"
#import "VLSLiveRemindModel.h"
#import "NSString+UserFile.h"
#import "UIImageView+WebCache.h"

@interface VLSLandscapeMessageTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end

@implementation VLSLandscapeMessageTableViewCell
- (void)setModel:(VLSLiveRemindModel *)model {
    _model = model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/v1/users/%@/avatars.png",BASE_URL,model.anchorID]] placeholderImage:ICON_PLACEHOLDER];
    
    NSString *name = model.anchorName;
    NSString *nameAppendStr = [@" " stringByAppendingString:LocalizedString(@"ONLIVING")];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:[name stringByAppendingString:nameAppendStr]];
    [att addAttribute:NSForegroundColorAttributeName value:RGB16(COLOR_FONT_4A4A4A) range:NSMakeRange(0, name.length)];
    [att addAttribute:NSForegroundColorAttributeName value:RGB16(COLOR_FONT_9B9B9B) range:NSMakeRange(name.length, nameAppendStr.length)];
    [self.nameLabel setAttributedText:att];
    
    self.titleLabel.text = model.title;
    
    self.timeLabel.text = [NSString compareCurrentTime:model.timestamp];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
