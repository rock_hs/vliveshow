//
//  LeaveOffContentView.m
//  VLiveShow
//
//  Created by sp on 16/8/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "LeaveOffContentView.h"

@implementation LeaveOffContentView

- (id)initWithFrame:(CGRect)frame
{

    self = [super initWithFrame:frame];
    if (self) {
        [self loadAllView];
    }
    return self;
}
- (void)loadAllView
{
    [self addSubview:self.nameLabel];
    [self addSubview:self.contentLabel];
    [self addSubview:self.markLabel];
    [self addSubview:self.leftView];
    [self addSubview:self.rightView];
    [self.leftView addSubview:self.vTicketName];
    [self.leftView addSubview:self.vTicketNum];
    [self.rightView addSubview:self.watchName];
    [self.rightView addSubview:self.watchNum];
    
}
- (UILabel*)nameLabel
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, 30)];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont boldSystemFontOfSize:22];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _nameLabel;
}
- (UILabel*)contentLabel
{
    if (_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 35, SC_WIDTH, 25)];
        _contentLabel.textColor = [UIColor whiteColor];
        _contentLabel.textAlignment = NSTextAlignmentCenter;
        _contentLabel.font = [UIFont boldSystemFontOfSize:16];
    }
    return _contentLabel;
}
- (UILabel*)markLabel
{
    if (_markLabel == nil) {
        _markLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        _markLabel.font = [UIFont systemFontOfSize:12];
        _markLabel.textColor = RGB16(COLOR_FONT_C3A851);
        _markLabel.backgroundColor = [UIColor clearColor];
        _markLabel.layer.borderColor = RGB16(COLOR_FONT_C3A851).CGColor;
        _markLabel.layer.borderWidth = 1;
        _markLabel.layer.cornerRadius = 10;
        _markLabel.clipsToBounds = YES;
        _markLabel.textAlignment = NSTextAlignmentCenter;

    }
    return _markLabel;

}
- (UIView*)leftView
{
    if (_leftView == nil) {
        _leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 70, SC_WIDTH/2, 40)];
        UIView*lineview = [[UIView alloc] initWithFrame:CGRectMake(SC_WIDTH/2, 5, 1, 13)];
        lineview.backgroundColor = RGB16(COLOR_FONT_FFFFFF);
        [_leftView addSubview:lineview];
        _leftView.backgroundColor = [UIColor clearColor];
    }
    return _leftView;
}
- (UILabel*)vTicketNum
{
    if (_vTicketNum == nil) {
        _vTicketNum = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH/2, 20)];
        _vTicketNum.textColor = RGB16(COLOR_FONT_C3A851);
        _vTicketNum.font = [UIFont systemFontOfSize:15];
        _vTicketNum.textAlignment = NSTextAlignmentCenter;
        _vTicketNum.backgroundColor = [UIColor clearColor];
    }
    return _vTicketNum;
}
- (UILabel*)vTicketName
{
    if (_vTicketName == nil) {
        _vTicketName = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, SC_WIDTH/2, 20)];
        _vTicketName.text = LocalizedString(@"LIVE_GETVTICKET");
        _vTicketName.textColor = [UIColor whiteColor];
        _vTicketName.font = [UIFont boldSystemFontOfSize:14];
        _vTicketName.textAlignment = NSTextAlignmentCenter;
    }
    return _vTicketName;
}
- (UIView*)rightView
{
    if (_rightView == nil) {
        _rightView = [[UIView alloc] initWithFrame:CGRectMake(SC_WIDTH/2, 70, SC_WIDTH/2, 40)];
        _rightView.backgroundColor = [UIColor clearColor];
    }
    return _rightView;
}
- (UILabel*)watchNum
{
    if (_watchNum == nil) {
        _watchNum = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH/2, 20)];
        _watchNum.textColor = RGB16(COLOR_FONT_C3A851);
        _watchNum.textAlignment = NSTextAlignmentCenter;
        _watchNum.font = [UIFont systemFontOfSize:15];
    }
    return _watchNum;
}
- (UILabel*)watchName
{
    if (_watchName == nil) {
        _watchName = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, SC_WIDTH/2, 20)];
        _watchName.textColor = [UIColor whiteColor];
        _watchName.text = LocalizedString(@"LIVE_SEENUMBER");
        _watchName.textAlignment = NSTextAlignmentCenter;
        _watchName.font = [UIFont boldSystemFontOfSize:14];
    }

    return _watchName;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
