//
//  VLSPhoneInputTextField.h
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol VLSPhoneInputViewDelegate <NSObject>

- (void)hidePhoneInputWarning;
- (void)inputPhoneNumberWithNextBtnEnable:(BOOL)enable;
- (void)phoneNumberExisted;

@end
@interface VLSPhoneInputView : UIView

@property (nonatomic, strong)UILabel *countryLabel;
@property (nonatomic, strong)UITextField *phoneTF;
@property (nonatomic, copy)NSString *normalPhoneNum;

@property (nonatomic, weak)id <VLSPhoneInputViewDelegate> delegate;
- (void)inputWrongPhoneNumber;

@end
