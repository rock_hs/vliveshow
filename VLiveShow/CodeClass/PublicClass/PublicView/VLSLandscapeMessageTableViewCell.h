//
//  VLSLandscapeMessageTableViewCell.h
//  VLiveShow
//
//  Created by tom.zhu on 2016/12/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VLSLiveRemindModel;

static NSString *LandscapeMessageTableViewCellIdentifier = @"LandscapeMessageTableViewCellIdentifier";
@interface VLSLandscapeMessageTableViewCell : UITableViewCell
@property (nonatomic, strong) VLSLiveRemindModel *model;
@end
