//
//  VLSItemCoverView.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSItemCoverView.h"

@implementation VLSItemCoverView

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        
        self.lightLayer = [[CAShapeLayer alloc] init];
        self.lightLayer.strokeColor = [UIColor whiteColor].CGColor;
        self.lightLayer.fillColor = [UIColor clearColor].CGColor;
        self.lightLayer.lineWidth = 2;
        self.lightLayer.hidden = YES;
        [self.layer addSublayer:self.lightLayer];
        
    }
    return self;
}
- (void)layoutSubviews
{
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(width/30, height/30, width-width/15, height-height/15)];
    self.lightLayer.path = path.CGPath;
    
    
}


@end
