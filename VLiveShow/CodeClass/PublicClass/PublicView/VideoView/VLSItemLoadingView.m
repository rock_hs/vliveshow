//
//  VLSItemLoadingView.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSItemLoadingView.h"

@implementation VLSItemLoadingView
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.backImageView];
        [self addSubview:self.iconImageView];
        [self addSubview:self.tipsLabel];
        [self.backImageView addSubview:self.effectView];
    }
    return self;
}

- (void)setCoverUrl:(NSString *)coverUrl
{
    [self.backImageView sd_setImageWithURL:[NSURL URLWithString:coverUrl] placeholderImage:[UIImage imageNamed:@"broadcast- Host view-par viewport－bg.png"]];
}

- (void)layoutSubviews
{
    self.backImageView.frame = self.bounds;
    self.effectView.frame = self.bounds;
    self.iconImageView.center = CGPointMake(self.center.x, self.center.y-10);
    self.tipsLabel.frame = CGRectMake(0, 0, self.frame.size.width, 40);
    self.tipsLabel.center = CGPointMake(self.center.x, self.center.y+30);
    
}

- (UIImageView *)backImageView
{
    if (_backImageView == nil) {
        _backImageView = [[UIImageView alloc] initWithFrame:self.bounds];

        _backImageView.backgroundColor = [UIColor clearColor];
    }
    return _backImageView;
}

- (UIVisualEffectView *)effectView
{
    if (_effectView == nil) {
        _effectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
        _effectView.frame = self.bounds;
        _effectView.alpha = 1.0;
    }
    return _effectView;
}

- (UIImageView *)iconImageView
{
    if (_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 31, 37)];
        _iconImageView.image = [UIImage imageNamed:@"broadcast_Host_view_par_viewport.png"];
    }
    return _iconImageView;
}

- (UILabel *)tipsLabel {
    if (!_tipsLabel) {
        _tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
        _tipsLabel.text = [NSString stringWithFormat:@"%@ %@",LocalizedString(@"INVITE_GUEST"),LocalizedString(@"ON_CONNECTION")];
        _tipsLabel.textAlignment = NSTextAlignmentCenter;
        _tipsLabel.textColor = [UIColor whiteColor];
        _tipsLabel.font = [UIFont systemFontOfSize:13];
        _tipsLabel.numberOfLines = 0;
    }
    return _tipsLabel;
}

@end
