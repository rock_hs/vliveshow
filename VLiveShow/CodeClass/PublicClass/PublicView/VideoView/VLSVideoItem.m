//
//  VLSVideoItem.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//


#import "VLSVideoItem.h"
#import "VLSMessageManager.h"
@implementation VLSVideoItem
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.originalFrame = frame;
        self.backgroundColor = [UIColor blackColor];
        self.videoConten = [[UIView alloc] initWithFrame:self.bounds];
        [self addSubview:self.videoConten];
        [self addSubview:self.anchorLabelBg];
        [self addSubview:self.anchorLabel];
        [self addSubview: self.guestLabel];
        [self addSubview:self.muteButton];
        [self addSubview:self.loadingView];
        [self addSubview:self.covewView];
        
        [self showAnchorLabelBG:NO];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(taptap:)];
//        [self addGestureRecognizer:tap];

    }
    return self;
}

- (void)showBorderWidthColor:(UIColor *)color;
{
    self.layer.borderWidth = 2;
    self.layer.borderColor = color.CGColor;
    
}

#pragma mark -actions
- (void)muteClick
{
    
}

- (void)showAnchorLabelBG:(BOOL)show
{
    self.anchorLabel.hidden = !show;
    self.anchorLabelBg.hidden = !show;
    if (show)
    {
        self.guestLabel.hidden = YES;
    }
}

- (void)setModel:(VLSVideoViewModel *)model
{
    _model = model;
    if (_model.muteVoice) {
        self.muteButton.hidden = NO;
    } else {
        self.muteButton.hidden = YES;
    }
    if (_model) {
        if (![_userProfile.userId isEqualToString: model.userId])
        {
            _userProfile = nil;
        }
        if (!_userProfile) {
            [self loadUserInfo];
        }
    }
    [self refreshGuestInfo];
}

- (void)loadUserInfo
{
    WS(ws)
    [[VLSMessageManager sharedManager] getUserProfile:_model.userId succ:^(VLSUserProfile *profile) {
        dispatch_async(dispatch_get_main_queue(), ^{
            ws.userProfile = profile;
            ws.loadingView.tipsLabel.text = [NSString stringWithFormat:@"%@%@%@",LocalizedString(@"INVITE_GUEST"),self.userProfile.userName,LocalizedString(@"ON_CONNECTION")];
            [ws.loadingView setCoverUrl: ws.userProfile.userFaceURL];
            [ws refreshGuestInfo];
        });
    }];
}

- (void)taptap:(UITapGestureRecognizer* )tap
{
    
}
//
//- (void)longPress:(UILongPressGestureRecognizer *)longPress
//{
//    if (longPress.state == UIGestureRecognizerStateBegan){
//        
//        point = [longPress locationInView:self];
//        point2 = [longPress locationInView:self.superview];
//        [self.superview exchangeSubviewAtIndex:[self.superview.subviews indexOfObject:self] withSubviewAtIndex:[[self.superview subviews] count] - 1];
//        if ([_delegate respondsToSelector:@selector(moveBeganWidthItem:)]) {
//            [self.delegate moveBeganWidthItem:self];
//        }
//    }else if (longPress.state == UIGestureRecognizerStateChanged){
//        
//        CGPoint movePoint = [longPress locationInView:self.superview];
//        [self setFrame:CGRectMake( movePoint.x - point.x, movePoint.y - point.y, self.frame.size.width, self.frame.size.height)];
//        if ([_delegate respondsToSelector:@selector(moveChangeWidthItem:)]) {
//            [self.delegate moveChangeWidthItem:self];
//        }
//    }else if (longPress.state == UIGestureRecognizerStateEnded){
//        
//        CGPoint endPoint = [longPress locationInView:self.superview];
//        CGFloat newX = endPoint.x - point.x ;
//        CGFloat newY = endPoint.y - point.y ;
//        
//        if ([_delegate respondsToSelector:@selector(moveEndWidthItem:newPoint:)]) {
//            [self.delegate moveEndWidthItem:self newPoint:CGPointMake(newX, newY)];
//        }
//
//    }
//}

- (void)layoutSubviews
{
    self.covewView.frame = self.bounds;
    self.loadingView.frame = self.bounds;
    self.videoConten.frame = self.bounds;
    if (self.frame.size.width == SCREEN_WIDTH) {
        UIImage* bgImage = [UIImage imageNamed:@"muted_zhubo"];
        
        self.muteButton.frame = CGRectMake(0, 0, bgImage.size.width, bgImage.size.height);
        [_muteButton setBackgroundImage: bgImage forState:UIControlStateNormal];
        self.muteButton.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    }else{
        UIImage* bgImage = [UIImage imageNamed:@"muted_jiabin"];
        self.muteButton.frame = CGRectMake(self.frame.size.width - bgImage.size.width - self.layer.borderWidth, self.layer.borderWidth, bgImage.size.width, bgImage.size.height);
        [_muteButton setBackgroundImage: bgImage forState:UIControlStateNormal];
    }
    [self refreshGuestInfo];
}
#pragma mark -getters

- (UIButton *)muteButton
{
    if (_muteButton == nil) {
        _muteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _muteButton.selected = NO;
        _muteButton.hidden = YES;
        [_muteButton addTarget:self action:@selector(muteClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _muteButton;
}

- (VLSItemLoadingView *)loadingView
{
    if (_loadingView == nil) {
        _loadingView = [[VLSItemLoadingView alloc] initWithFrame:self.bounds];
        _loadingView.hidden = NO;
    }
    return _loadingView;
}

- (VLSItemCoverView *)covewView
{
    if (_covewView == nil) {
        _covewView = [[VLSItemCoverView alloc] initWithFrame:self.bounds];
        _covewView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.4];
        _covewView.hidden = YES;
    }
    return _covewView;
}

- (UIImageView *)anchorLabelBg
{
    if (_anchorLabelBg == nil) {
        _anchorLabelBg = [[UIImageView alloc] initWithFrame:CGRectMake(6, 6, 29, 16)];
        _anchorLabelBg.image = [UIImage imageNamed:@"broadcast_Host_view_exscreen_host_bg.png"];
    }
    return _anchorLabelBg;
}

- (UILabel *)anchorLabel
{
    if (!_anchorLabel) {
        _anchorLabel = [[UILabel alloc] initWithFrame:CGRectMake(6, 6, 29, 16)];
        _anchorLabel.text = @"主播";
        _anchorLabel.textAlignment = NSTextAlignmentCenter;
        _anchorLabel.textColor = [UIColor whiteColor];
        _anchorLabel.font = [UIFont boldSystemFontOfSize:11];
    }
    return _anchorLabel;
}

- (UIExtLabel*)guestLabel
{
    if (_guestLabel == nil)
    {
        _guestLabel = [[UIExtLabel alloc] initWithFrame: CGRectMake(0, CGRectGetHeight(self.frame) - 16, 29, 16)];
        _guestLabel.textAlignment = NSTextAlignmentCenter;
        _guestLabel.textColor = [UIColor whiteColor];
        _guestLabel.backgroundColor = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 0.5];
        _guestLabel.font = [UIFont boldSystemFontOfSize:11];
        _guestLabel.hidden = YES;
        _guestLabel.textLeftInsect = 8;
        _guestLabel.textRightInsect = 8;
        _guestLabel.layer.masksToBounds = YES;
        _guestLabel.layer.cornerRadius = CGRectGetHeight(_guestLabel.frame) / 2;
    }
    return _guestLabel;
}

- (void)setGuestUserName: (NSString*)userName
{
    if ([userName length] == 0)
    {
        return;
    }
    self.guestLabel.text = userName;
    CGFloat width =  ceil( MIN(self.guestLabel.intrinsicContentSize.width, CGRectGetWidth(self.frame) / 2));
    self.guestLabel.frame = CGRectMake(2, CGRectGetHeight(self.frame) - 18, width, 16);
    
}

- (void)refreshGuestInfo
{
    NSString* userName = self.userProfile.userName;
    self.guestLabel.hidden = [userName length] == 0 || !self.anchorLabel.hidden || self.model.userType == UserTypeHost || (self.model && self.model.isMainScreen);
    [self setGuestUserName: userName];
}
@end
