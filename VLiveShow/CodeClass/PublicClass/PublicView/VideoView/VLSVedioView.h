//
//  VLSVedioView.h
//  VLiveShow
//
//  Created by Cavan on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSVideoItem.h"

static const ITEMHIGHT = 83;
#define ITEMWIDTH(count) count >= 4 ? SCREEN_WIDTH / 6 : 111

typedef NS_ENUM(NSInteger, Orientation) {
    Vertical,
    horizontal,
};

@protocol VLSVideoViewDelegate <NSObject>
@optional
- (void)videoViewReplaceUser:(VLSVideoViewModel *)fromModel toUser:(VLSVideoViewModel *)toModel;
- (void)videoViewUpdateNeedChangeBottomHeight:(CGFloat)height oneGuestHight:(CGFloat)oneGuestHight;
- (void)videoViewUpdateAnchorView:(VLSVideoViewModel*)model;

@end

@interface VLSVedioView : UIView

@property (nonatomic, weak) id <VLSVideoViewDelegate> delegate;
// 新接收的嘉宾数组
@property (nonatomic, strong) NSMutableArray *NewGuestInfos;
//标志当前直播间是横屏竖屏
@property (nonatomic, assign) KLiveRoomOrientation orientation;

@property (nonatomic, assign) ParticipantStatus participantStatus;

- (void)guestOffLine:(BOOL)off usrid:(NSString *)uid;

/// 布局嘉宾视图
- (void)layoutLivesViewWithGuests:(NSMutableArray *)NewGuestInfos;

- (void)moveBeganWidthItem:(VLSVideoItem *)item;
- (void)moveChangeWidthItem:(VLSVideoItem *)item;
- (void)moveEndWidthItem:(VLSVideoItem *)item newPoint:(CGPoint)newPoint;

- (void)rotate:(UIDeviceOrientation)orientation superView:(UIView*)view;
- (void)orientation:(Orientation)orientation;
@end
