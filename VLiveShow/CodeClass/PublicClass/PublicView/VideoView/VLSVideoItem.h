//
//  VLSVideoItem.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSItemCoverView.h"
#import "VLSItemLoadingView.h"
#import "VLSUserProfile.h"
@protocol VLSVideoItemDelegate;
@class UIExtLabel;

@interface VLSVideoItem : UIView
{
    CGPoint point;
    CGPoint point2;
    BOOL enableMove;
}
@property (nonatomic ,strong) UIView *videoConten;
@property (nonatomic, strong) UIButton *muteButton;
@property (nonatomic, assign) CGRect originalFrame;
@property (nonatomic, strong) UILabel *anchorLabel;
@property (nonatomic, strong) UIImageView *anchorLabelBg;

@property (nonatomic, strong) VLSItemCoverView *covewView;     //长按时候覆盖
@property (nonatomic, strong) VLSItemLoadingView *loadingView; //显示加载中
@property (nonatomic, assign) id<VLSVideoItemDelegate> delegate;
@property (nonatomic, strong) VLSUserProfile *userProfile;
@property (nonatomic, strong) VLSVideoViewModel *model;

@property (nonatomic, strong) UIExtLabel* guestLabel;

- (void)showBorderWidthColor:(UIColor *)color;
- (void)showAnchorLabelBG:(BOOL)show;

- (void)setGuestUserName: (NSString*)userName;
@end


@protocol VLSVideoItemDelegate <NSObject>

- (void)moveBeganWidthItem:(VLSVideoItem *)item;
- (void)moveChangeWidthItem:(VLSVideoItem *)item;
- (void)moveEndWidthItem:(VLSVideoItem *)item newPoint:(CGPoint)newPoint;
@end
