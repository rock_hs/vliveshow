//
//  VLSVedioView.m
//  VLiveShow
//
//  Created by Cavan on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSVedioView.h"
#import "VLSVideoViewModel.h"
#import <AgoraRtcEngineKit/AgoraRtcEngineKit.h>
#import "VLSAgoraVideoService.h"
#import "VLSMessageManager.h"
#import "VLSVideoViewModel.h"
#import "VLSAgoraVideoService.h"
#import "VLSLiveContentView.h"

//#define GUESTVIEW_WIDTH (SCREEN_WIDTH / (self.NewGuestInfos.count - 1))
#define GUESTVIEW_WIDTH (SCREEN_WIDTH / (self.NewGuestInfos.count < 6 ? (self.NewGuestInfos.count - 1) : 3))
typedef enum : NSUInteger {
    GuestsCountOne = 1, // 一位嘉宾
    GuestsCountTwo,
    GuestsCountThree,
    GuestsCountFour,
    GuestsCountFive,
    GuestsCountSix,
} GuestsCount;

@interface VLSVedioView ()<AgoraRtcEngineDelegate>
{
    NSInteger shouldMoveToTag;
}
// agora服务
@property (nonatomic, strong) VLSAgoraVideoService *agora;
/// 要显示的视频数组
@property (nonatomic, strong) NSMutableArray *vedioArray;
// 要显示的嘉宾Frame数组
@property (nonatomic, strong) NSMutableArray *frames;
// 当前保存的嘉宾数组
@property (nonatomic, strong) NSMutableArray *guestInfos;
// 当前保存的view数组
@property (nonatomic, strong) NSMutableArray *videoViews;
// 记录匹配数组的相同的索引号
//@property (nonatomic, strong) NSMutableArray *existindex;
//@property (nonatomic, strong) NSMutableArray *newexistindex;


@end

@implementation VLSVedioView
- (void)setOrientation:(KLiveRoomOrientation)orientation {
    if (_orientation == orientation) {
        return;
    }
    _orientation = orientation;
    if (orientation == KLiveRoomOrientation_LandscapeRight || orientation == KLiveRoomOrientation_LandscapeLeft) {
        [self layoutLivesViewWithGuests:self.NewGuestInfos];
    }
    [[VLSAgoraVideoService sharedInstance] setOrientation:orientation];
}

- (void)setParticipantStatus:(ParticipantStatus)participantStatus {
    _participantStatus = participantStatus;
    float height = MIN(SCREEN_HEIGHT, SCREEN_WIDTH);
    [self.frames enumerateObjectsUsingBlock:^(NSValue *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) {
            //大屏还是大屏
        }else {
            CGRect rect = [obj CGRectValue];
            if (participantStatus == LiveContentView_Participant_Landscape_Show) {
                rect.origin.y = height - ITEMHIGHT;
            }else {
                rect.origin.y += ITEMHIGHT;
            }
            obj = [NSValue valueWithCGRect:rect];
            [self.frames replaceObjectAtIndex:idx withObject:obj];
            
            //改变item
            VLSVideoViewModel *model = [self.NewGuestInfos objectAtIndex:idx];
            VLSVideoItem *item;
            if ([[VLSMessageManager sharedManager] checkUserIsSelf:model.userId]) {
                item = [[VLSAgoraVideoService sharedInstance] createLocalVideo];
            }else{
                item = [[VLSAgoraVideoService sharedInstance] createRemoteVideoOfUid:model.userId.integerValue];
            }
            item.model = model;
            [UIView animateWithDuration:ANIMATION_DURATION animations:^{
                item.frame = rect;
            }];
        }
    }];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {        
    }
    return self;
}

- (void)layoutLivesViewWithGuests:(NSMutableArray *)NewGuestInfos {
    self.NewGuestInfos = NewGuestInfos;
    NSArray* subviews = [self.subviews copy];
//    // 清除布局
    for (UIView *view in subviews) {
        if ([view isKindOfClass:[VLSVideoItem class]]) {
            [view removeFromSuperview];
        }
    }
    //四路兼容六路视频
    if (self.NewGuestInfos.count>7) {
        NSRange range = NSMakeRange(0, 7);
        NSArray *array = [self.NewGuestInfos subarrayWithRange:range];
        self.NewGuestInfos = [[NSMutableArray alloc] initWithArray:array];
    }
    
    [self framesByCount:self.NewGuestInfos.count];
    
    NSLog(@"开始布局");

    // 进入房间时 创建嘉宾布局
    for (int j = 0; j < self.NewGuestInfos.count; j++) {
        NSValue *value = self.frames[j];
        CGRect rect = [value CGRectValue];
        VLSVideoViewModel *model = [self.NewGuestInfos objectAtIndex:j];
        VLSVideoItem *item;
        
        if ([[VLSMessageManager sharedManager] checkUserIsSelf:model.userId]) {
            item = [[VLSAgoraVideoService sharedInstance] createLocalVideo];
            item.loadingView.hidden = YES;
        }else{
            item = [[VLSAgoraVideoService sharedInstance] createRemoteVideoOfUid:model.userId.integerValue];
            if (model.userType == UserTypeHost) {
                item.loadingView.hidden = YES;
            }
        }
        item.tag = 200+j;
        item.model.isMainScreen = (rect.size.width == self.frame.size.width);
        item.model = model;
        item.frame = rect;
        item.originalFrame = rect;
        [self addSubview:item];
        
        if (item.frame.size.width == self.frame.size.width) {
            if (item.model.userType != UserTypeHost) {
                if ([_delegate respondsToSelector:@selector(videoViewUpdateAnchorView:)]) {
                    [_delegate videoViewUpdateAnchorView:model];
                }
            }
            item.model.isMainScreen = YES;
            [item showBorderWidthColor:[UIColor clearColor]];
            [item showAnchorLabelBG:NO];
            [self sendSubviewToBack:item];
            [[VLSAgoraVideoService sharedInstance] setFullScreenRemoteVideoTypeOfUid:model.userId.integerValue];
        
        }else{
            if (model.userType == UserTypeHost) {
                [item showBorderWidthColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"button04.png"]]];
                [item showAnchorLabelBG:YES];
            }else{
                [item showBorderWidthColor:[UIColor whiteColor]];
                [item showAnchorLabelBG:NO];
            }
            [[VLSAgoraVideoService sharedInstance] setSmallScreenRemoteVideoTypeOfUid:model.userId.integerValue];
        }
    }
    
    if (self.NewGuestInfos.count>2) {
        CGRect frame = [[self.frames objectAtIndex:2] CGRectValue];
        if ([_delegate respondsToSelector:@selector(videoViewUpdateNeedChangeBottomHeight:oneGuestHight:)]) {
            float height = 0;
            if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
                height = frame.size.height;
            }else {
                height = self.frames.count > 5 ? frame.size.height * 2 : frame.size.height;
            }
            [_delegate videoViewUpdateNeedChangeBottomHeight:height oneGuestHight:0.0f];
        }
    }else if(self.NewGuestInfos.count == 2){
        CGRect oneGuestFrame = [[self.frames objectAtIndex:1] CGRectValue];
        if ([_delegate respondsToSelector:@selector(videoViewUpdateNeedChangeBottomHeight:oneGuestHight:)]) {
            [_delegate videoViewUpdateNeedChangeBottomHeight:0.0f oneGuestHight:oneGuestFrame.size.height];
        }
    }else{
        if ([_delegate respondsToSelector:@selector(videoViewUpdateNeedChangeBottomHeight:oneGuestHight:)]) {
            [_delegate videoViewUpdateNeedChangeBottomHeight:0.0f oneGuestHight:0.0f];
        }
    }
}

- (void)guestOffLine:(BOOL)off usrid:(NSString *)uid;
{
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[VLSVideoItem class]]) {
            VLSVideoItem *item = (VLSVideoItem*)view;
            if ([item.model.userId isEqualToString:uid]) {
//                item.loadingView.hidden = !off;
//                item.loadingView.tipsLabel.text = [NSString stringWithFormat:@"嘉宾%@暂时离开...",item.userProfile.userName];
            }
        }
    }
}

- (VLSVideoItem *)getItem:(NSString *)uid
{
    for (int i= 0; i<self.subviews.count; i++) {
        UIView *view = [self.subviews objectAtIndex:i];
        if ([view isKindOfClass:[VLSVideoItem class]]) {
            VLSVideoItem *item = (VLSVideoItem*)view;
            if ([item.model.userId isEqualToString:uid]) {
                return item;
            }
        }
    }
    return nil;
}

- (void)moveBeganWidthItem:(VLSVideoItem *)item
{
    shouldMoveToTag = item.tag;
    [self videoItemShowCover:YES withoutView:item];
}

- (void)moveChangeWidthItem:(VLSVideoItem *)item
{
    [self checkItemViewContainPoint:item.center withoutView:item];
}

- (void)moveEndWidthItem:(VLSVideoItem *)item newPoint:(CGPoint)newPoint
{
    [self videoItemShowCover:NO withoutView:item];
    [self changeItemViewWidthItemView:item];
}


#pragma mark -actions

#pragma mark -priviate methods

- (void)framesByCount:(NSInteger)count
{
    self.frames = [NSMutableArray arrayWithCapacity:5];
    CGFloat width = GUESTVIEW_WIDTH;
    
    CGFloat min = MIN(SCREEN_WIDTH, SCREEN_HEIGHT);
    CGFloat max = MAX(SCREEN_WIDTH, SCREEN_HEIGHT);
    
    float screen_width = 0, screen_height = 0;
    if (self.orientation == KLiveRoomOrientation_Portrait || self.orientation == KLiveRoomOrientation_Unknown) {
        screen_width = min;
        screen_height = max;
    }else {
        screen_width = max;
        screen_height = min;
    }
    
    //三位嘉宾 138
    /*
    if (self.orientation == KLiveRoomOrientation_Portrait || self.orientation == KLiveRoomOrientation_Unknown) {
        switch (count) {
            case 1:
            {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
            }
                break;
            case 2:
            {
                
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(SCREEN_WIDTH - 1.2*SCREEN_WIDTH/3 - 15, SCREEN_HEIGHT - 1.2*1.1*SCREEN_WIDTH/3 -50, 1.2*SCREEN_WIDTH/3, 1.2*1.1*SCREEN_WIDTH/3);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
            }
                break;
            case 3:
            {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(0, SCREEN_HEIGHT - GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(SCREEN_WIDTH - GUESTVIEW_WIDTH, SCREEN_HEIGHT - GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
            }
                break;
            case 4:
            {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(0, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(GUESTVIEW_WIDTH, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 = CGRectMake(SCREEN_WIDTH - GUESTVIEW_WIDTH, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
            }
                break;
            case 5:
            {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(0, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(GUESTVIEW_WIDTH, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 = CGRectMake(2 * GUESTVIEW_WIDTH, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 = CGRectMake(SCREEN_WIDTH - GUESTVIEW_WIDTH, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
            }
                break;
            case 6: {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(GUESTVIEW_WIDTH, SCREEN_HEIGHT - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(2 * GUESTVIEW_WIDTH, SCREEN_HEIGHT - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 = CGRectMake(0, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 = CGRectMake(GUESTVIEW_WIDTH, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
                
                CGRect rect5 = CGRectMake(SCREEN_WIDTH - GUESTVIEW_WIDTH, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value5 = [NSValue valueWithCGRect:rect5];
                [self.frames addObject:value5];
            }
                break;
            case 7: {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(0, SCREEN_HEIGHT - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(GUESTVIEW_WIDTH, SCREEN_HEIGHT - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 = CGRectMake(2 * GUESTVIEW_WIDTH, SCREEN_HEIGHT - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 = CGRectMake(0, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
                
                CGRect rect5 = CGRectMake(GUESTVIEW_WIDTH, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value5 = [NSValue valueWithCGRect:rect5];
                [self.frames addObject:value5];
                
                CGRect rect6 = CGRectMake(SCREEN_WIDTH - GUESTVIEW_WIDTH, SCREEN_HEIGHT - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value6 = [NSValue valueWithCGRect:rect6];
                [self.frames addObject:value6];
            }
                break;
            default:
                break;
        }
    }else {
        int itemWidth = count >= 4 ? SCREEN_WIDTH / 6 : 111;
        int itemHight = ITEMHIGHT;
        int itemBottomMargin = self.participantStatus == LiveContentView_Participant_Landscape_Show ? 0 : -ITEMHIGHT;
        switch (count) {
            case 1:
            {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
            }
                break;
            case 2: {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(SCREEN_WIDTH - itemWidth, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
            }
                break;
            case 3: {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(SCREEN_WIDTH - itemWidth * 2, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(SCREEN_WIDTH - itemWidth, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
            }
                break;
            case 4: {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(SCREEN_WIDTH - itemWidth * 3, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(SCREEN_WIDTH - itemWidth * 2, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 =  CGRectMake(SCREEN_WIDTH - itemWidth, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
            }
                break;
            case 5: {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(SCREEN_WIDTH - itemWidth * 4, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(SCREEN_WIDTH - itemWidth * 3, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 =  CGRectMake(SCREEN_WIDTH - itemWidth * 2, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 =  CGRectMake(SCREEN_WIDTH - itemWidth, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
            }
                break;
            case 6: {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(SCREEN_WIDTH - itemWidth * 5, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(SCREEN_WIDTH - itemWidth * 4, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 =  CGRectMake(SCREEN_WIDTH - itemWidth * 3, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 =  CGRectMake(SCREEN_WIDTH - itemWidth * 2, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
                
                CGRect rect5 =  CGRectMake(SCREEN_WIDTH - itemWidth, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value5 = [NSValue valueWithCGRect:rect5];
                [self.frames addObject:value5];
            }
                break;
            case 7: {
                CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(SCREEN_WIDTH - itemWidth * 6, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(SCREEN_WIDTH - itemWidth * 5, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 =  CGRectMake(SCREEN_WIDTH - itemWidth * 4, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 =  CGRectMake(SCREEN_WIDTH - itemWidth * 3, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
                
                CGRect rect5 =  CGRectMake(SCREEN_WIDTH - itemWidth * 2, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value5 = [NSValue valueWithCGRect:rect5];
                [self.frames addObject:value5];
                
                CGRect rect6 =  CGRectMake(SCREEN_WIDTH - itemWidth, SCREEN_HEIGHT - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value6 = [NSValue valueWithCGRect:rect6];
                [self.frames addObject:value6];
            }
                break;
                
            default:
                break;
        }
    }
    */
    if (self.orientation == KLiveRoomOrientation_Portrait || self.orientation == KLiveRoomOrientation_Unknown) {
        switch (count) {
            case 1:
            {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
            }
                break;
            case 2:
            {
                
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(screen_width - 1.2*screen_width/3 - 15, screen_height - 1.2*1.1*screen_width/3 -50, 1.2*screen_width/3, 1.2*1.1*screen_width/3);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
            }
                break;
            case 3:
            {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(0, screen_height - GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(screen_width - GUESTVIEW_WIDTH, screen_height - GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
            }
                break;
            case 4:
            {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(0, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(GUESTVIEW_WIDTH, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 = CGRectMake(screen_width - GUESTVIEW_WIDTH, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
            }
                break;
            case 5:
            {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(0, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(GUESTVIEW_WIDTH, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 = CGRectMake(2 * GUESTVIEW_WIDTH, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 = CGRectMake(screen_width - GUESTVIEW_WIDTH, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
            }
                break;
            case 6: {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(GUESTVIEW_WIDTH, screen_height - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(2 * GUESTVIEW_WIDTH, screen_height - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 = CGRectMake(0, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 = CGRectMake(GUESTVIEW_WIDTH, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
                
                CGRect rect5 = CGRectMake(screen_width - GUESTVIEW_WIDTH, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value5 = [NSValue valueWithCGRect:rect5];
                [self.frames addObject:value5];
            }
                break;
            case 7: {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(0, screen_height - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(GUESTVIEW_WIDTH, screen_height - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 = CGRectMake(2 * GUESTVIEW_WIDTH, screen_height - 2 * 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 = CGRectMake(0, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
                
                CGRect rect5 = CGRectMake(GUESTVIEW_WIDTH, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value5 = [NSValue valueWithCGRect:rect5];
                [self.frames addObject:value5];
                
                CGRect rect6 = CGRectMake(screen_width - GUESTVIEW_WIDTH, screen_height - 1.1*GUESTVIEW_WIDTH, GUESTVIEW_WIDTH, 1.1*GUESTVIEW_WIDTH);
                NSValue *value6 = [NSValue valueWithCGRect:rect6];
                [self.frames addObject:value6];
            }
                break;
            default:
                break;
        }
    }else {
        int itemWidth = count >= 4 ? screen_width / 6 : 111;
        int itemHight = ITEMHIGHT;
        int itemBottomMargin = self.participantStatus == LiveContentView_Participant_Landscape_Show ? 0 : -ITEMHIGHT;
        switch (count) {
            case 1:
            {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
            }
                break;
            case 2: {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(screen_width - itemWidth, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
            }
                break;
            case 3: {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(screen_width - itemWidth * 2, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(screen_width - itemWidth, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
            }
                break;
            case 4: {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(screen_width - itemWidth * 3, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(screen_width - itemWidth * 2, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 =  CGRectMake(screen_width - itemWidth, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
            }
                break;
            case 5: {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(screen_width - itemWidth * 4, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(screen_width - itemWidth * 3, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 =  CGRectMake(screen_width - itemWidth * 2, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 =  CGRectMake(screen_width - itemWidth, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
            }
                break;
            case 6: {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(screen_width - itemWidth * 5, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(screen_width - itemWidth * 4, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 =  CGRectMake(screen_width - itemWidth * 3, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 =  CGRectMake(screen_width - itemWidth * 2, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
                
                CGRect rect5 =  CGRectMake(screen_width - itemWidth, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value5 = [NSValue valueWithCGRect:rect5];
                [self.frames addObject:value5];
            }
                break;
            case 7: {
                CGRect rect = CGRectMake(0, 0, screen_width, screen_height);
                NSValue *value = [NSValue valueWithCGRect:rect];
                [self.frames addObject:value];
                
                CGRect rect1 = CGRectMake(screen_width - itemWidth * 6, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value1 = [NSValue valueWithCGRect:rect1];
                [self.frames addObject:value1];
                
                CGRect rect2 = CGRectMake(screen_width - itemWidth * 5, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value2 = [NSValue valueWithCGRect:rect2];
                [self.frames addObject:value2];
                
                CGRect rect3 =  CGRectMake(screen_width - itemWidth * 4, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value3 = [NSValue valueWithCGRect:rect3];
                [self.frames addObject:value3];
                
                CGRect rect4 =  CGRectMake(screen_width - itemWidth * 3, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value4 = [NSValue valueWithCGRect:rect4];
                [self.frames addObject:value4];
                
                CGRect rect5 =  CGRectMake(screen_width - itemWidth * 2, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value5 = [NSValue valueWithCGRect:rect5];
                [self.frames addObject:value5];
                
                CGRect rect6 =  CGRectMake(screen_width - itemWidth, screen_height - itemBottomMargin - itemHight, itemWidth, itemHight);
                NSValue *value6 = [NSValue valueWithCGRect:rect6];
                [self.frames addObject:value6];
            }
                break;
                
            default:
                break;
        }
    }
}
#pragma mark -actions

#pragma mark -priviate methods
//显示、隐藏遮罩
- (void)videoItemShowCover:(BOOL)show withoutView:(VLSVideoItem *)item
{
    for (VLSVideoItem * view in self.subviews) {
        if (show) {
            if (view.tag != item.tag) {
                view.covewView.hidden = NO;
            }
        }else{
            view.covewView.hidden = YES;
        }
    }
}
//显示高亮的线框、提示松开即进入该view
- (void)checkItemViewContainPoint:(CGPoint)point withoutView:(VLSVideoItem *)item
{
    
    CGFloat w = item.frame.size.width;
    CGFloat h = item.frame.size.height;
    CGFloat ocx = item.originalFrame.size.width/2+item.originalFrame.origin.x;
    CGFloat ocy = item.originalFrame.size.height/2+item.originalFrame.origin.y;
    
    //当移动的该view的中心点x距离原始中心点大于宽度1/3；并且x__>y__
    //活着中心点y
    NSLog(@"%f\n%f\n%f\n%f",fabs(point.x - ocx),(ocy - point.y),w,h);
    if ((fabs(point.x - ocx)>w/3&&fabs(point.x - ocx)>(ocy - point.y)) || ((ocy - point.y)>h/3&&(ocy - point.y)>fabs(point.x - ocx))) {
        for (VLSVideoItem * view in self.subviews) {
            
            if (view != item) {
                
                if (CGRectContainsPoint(view.frame, point)) {
                    if (shouldMoveToTag != view.tag) {
                        
                        VLSVideoItem * oldView = [self viewWithTag:shouldMoveToTag];
                        oldView.covewView.lightLayer.hidden = YES;
                        view.covewView.lightLayer.hidden = NO;
                        shouldMoveToTag = view.tag;
                    }
                }
            }
        }
        
    }
}
//交换或者回到原位
- (void)changeItemViewWidthItemView:(VLSVideoItem *)item
{
    VLSVideoItem *toView = [self viewWithTag:shouldMoveToTag];
    toView.covewView.lightLayer.hidden = YES;
    
    if (shouldMoveToTag == item.tag) {
        item.frame = item.originalFrame;
    }else{
        
        if (toView.model.isMainScreen) {
            item.model.isMainScreen = YES;
            toView.model.isMainScreen = NO;
        }
        item.frame = toView.frame;
        toView.frame = item.originalFrame;
        toView.originalFrame = toView.frame;
        item.originalFrame = item.frame;
        
        [self exchangeSubviewAtIndex:[self.subviews indexOfObject:toView] withSubviewAtIndex:self.subviews.count-1];
        if ([_delegate respondsToSelector:@selector(videoViewReplaceUser:toUser:)]) {
            [_delegate videoViewReplaceUser:item.model toUser:toView.model];
        }
    }
}

- (void)rotate:(UIDeviceOrientation)orientation superView:(UIView*)view {
//    [self.frames enumerateObjectsUsingBlock:^(NSValue *value, NSUInteger idx, BOOL * _Nonnull stop) {
//        NSLog(@"value == %@", NSStringFromCGRect([value CGRectValue]));
//    }];
    if (self.NewGuestInfos.count == 0) {
        //直播间关闭以后取消
        return;
    }
    VLSVideoViewModel *model = [self.NewGuestInfos objectAtIndex:0];
    VLSVideoItem *item;
    
    if ([[VLSMessageManager sharedManager] checkUserIsSelf:model.userId]) {
        item = [[VLSAgoraVideoService sharedInstance] createLocalVideo];
    }else{
        item = [[VLSAgoraVideoService sharedInstance] createRemoteVideoOfUid:model.userId.integerValue];
    }
    item.model = model;
    view.layer.anchorPoint = CGPointMake(0, 0);
    CGAffineTransform transform = item.transform;
    CGAffineTransform temTransform = CGAffineTransformIdentity;
    switch (orientation) {
        case UIDeviceOrientationPortrait:
        {
            temTransform = CGAffineTransformRotate(CGAffineTransformMakeScale(SCREEN_WIDTH / item.bounds.size.width, SCREEN_HEIGHT / item.bounds.size.height), 0);
            [UIView animateWithDuration:0.25f animations:^{
//                if (![NSStringFromCGAffineTransform(temTransform) isEqualToString:NSStringFromCGAffineTransform(transform)]) {
////                    item.transform = temTransform;
//                    view.transform = temTransform;
//                }
                
                //没有连线嘉宾，改变frams的firstObject
                NSValue *value = [NSValue valueWithCGRect:[[UIScreen mainScreen] bounds]];//[NSValue valueWithCGRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                [self.frames replaceObjectAtIndex:0 withObject:value];
            }];
        }
            break;
        case UIDeviceOrientationLandscapeLeft:
        {
//            temTransform = CGAffineTransformRotate(CGAffineTransformMakeScale(SCREEN_HEIGHT / SCREEN_WIDTH, SCREEN_HEIGHT / SCREEN_WIDTH), M_PI / 2);
            temTransform = CGAffineTransformScale(CGAffineTransformIdentity, SCREEN_WIDTH / item.bounds.size.width, SCREEN_HEIGHT / item.bounds.size.height);
            [UIView animateWithDuration:0.25f animations:^{
                if (![NSStringFromCGAffineTransform(temTransform) isEqualToString:NSStringFromCGAffineTransform(transform)]) {
//                    item.transform = temTransform;
                    
                    //没有连线嘉宾，改变frams的firstObject
                    NSValue *value = [NSValue valueWithCGRect:[[UIScreen mainScreen] bounds]];//[NSValue valueWithCGRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                    [self.frames replaceObjectAtIndex:0 withObject:value];
                }
            }];
        }
            break;
        case UIDeviceOrientationLandscapeRight:
        {
//            temTransform = CGAffineTransformRotate(CGAffineTransformMakeScale(SCREEN_HEIGHT / SCREEN_WIDTH, SCREEN_HEIGHT / SCREEN_WIDTH), -M_PI / 2);
            temTransform = CGAffineTransformScale(CGAffineTransformIdentity, SCREEN_WIDTH / item.bounds.size.width, SCREEN_HEIGHT / item.bounds.size.height);
            [UIView animateWithDuration:0.25f animations:^{
                if (![NSStringFromCGAffineTransform(temTransform) isEqualToString:NSStringFromCGAffineTransform(transform)]) {
//                    item.transform = temTransform;

                    //没有连线嘉宾，改变frams的firstObject
                    NSValue *value = [NSValue valueWithCGRect:[[UIScreen mainScreen] bounds]];//[NSValue valueWithCGRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                    [self.frames replaceObjectAtIndex:0 withObject:value];
                }
            }];
        }
            break;
        case UIDeviceOrientationPortraitUpsideDown:
        {
            
        }
            break;
            
        default:
            break;
    }
    
    view.frame = [[UIScreen mainScreen] bounds];
    [view layoutSubviews];
    [view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[VLSLiveContentView class]]) {
            [obj layoutSubviews];
        }
    }];
    
    self.frame = [[UIScreen mainScreen] bounds];
    [self layoutSubviews];
    
    NSValue *value = [self.frames firstObject];
    CGRect rect = [value CGRectValue];
    
    item.frame = rect;
    item.originalFrame = rect;
}

- (void)orientation:(Orientation)orientation {
    switch (orientation) {
        case Vertical:
        {
            
        }
            break;
        case horizontal:
        {
            //agora实例变量并没有被初始化，设置无效。。。。。。
//            VLSVideoViewModel *model = [self.NewGuestInfos objectAtIndex:0];
//            //设置远程视频流为fit模式
//            [self.agora setRemoteRenderMode:[model.userId integerValue] mode:AgoraRtc_Render_Fit];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - getters




@end
