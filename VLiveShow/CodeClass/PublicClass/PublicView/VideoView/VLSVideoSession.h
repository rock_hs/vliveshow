//
//  VLSVideoSession.h
//  AGLiveDemo
//
//  Created by 李雷凯 on 16/6/20.
//  Copyright © 2016年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AgoraRtcEngineKit/AgoraRtcEngineKit.h>
#import "VLSVideoItem.h"
#import <UIKit/UIKit.h>

@interface VLSVideoSession : NSObject

@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, assign) CGRect frame;
@property (nonatomic, strong) VLSVideoItem *videoView;
@property (nonatomic, strong) AgoraRtcVideoCanvas *canvas;

- (instancetype)initSessionUid:(NSInteger)uid;

@end

@interface VLSVideoSession ()

- (VLSVideoSession *)initLocalSeeeion;

@end

