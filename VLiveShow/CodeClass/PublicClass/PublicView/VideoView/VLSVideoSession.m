//
//  VLSVideoSession.m
//  AGLiveDemo
//
//  Created by 李雷凯 on 16/6/20.
//  Copyright © 2016年 LK. All rights reserved.
//

#import "VLSVideoSession.h"

@implementation VLSVideoSession

- (instancetype)initSessionUid:(NSInteger )uid;
{
    if (self = [super init]) {
        self.uid = uid;
        self.videoView = [[VLSVideoItem alloc] initWithFrame:CGRectMake(0, 0, 300, 300)];
        self.videoView.backgroundColor = [UIColor lightGrayColor];
        self.canvas = [[AgoraRtcVideoCanvas alloc] init];
        self.canvas.uid = uid;
        self.canvas.view = self.videoView.videoConten;
        self.canvas.renderMode = AgoraRtc_Render_Hidden;
    }
    return self;
}

- (VLSVideoSession *)initLocalSeeeion;
{
    self.uid = 0;
    return [[VLSVideoSession alloc] initSessionUid:0];
}

@end



