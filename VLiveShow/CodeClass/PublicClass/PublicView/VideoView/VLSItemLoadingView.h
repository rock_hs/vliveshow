//
//  VLSItemLoadingView.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSItemLoadingView : UIView
@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIVisualEffectView *effectView;

@property (nonatomic, strong) UILabel *tipsLabel;

- (void)setCoverUrl:(NSString *)coverUrl;
@end
