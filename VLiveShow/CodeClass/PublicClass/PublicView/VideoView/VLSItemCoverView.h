//
//  VLSItemCoverView.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSItemCoverView : UIView
@property (nonatomic, strong)CAShapeLayer *lightLayer;
@end
