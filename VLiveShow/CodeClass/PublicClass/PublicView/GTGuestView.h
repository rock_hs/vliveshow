//
//  GTGuestView.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GTGuestView;

@protocol GTGuestViewDelegate<NSObject>

//消息按钮点击
- (void)guestViewClicked:(GTGuestView *)guestView;
@end

@interface GTGuestView : UIView

@property (nonatomic, copy) NSString *GuestID;
@property (nonatomic, strong) VLSUserProfile *model;
@property (nonatomic, assign) id<GTGuestViewDelegate>delegate;

-(void)setGuestID:(NSString *)GuestID;

@end
