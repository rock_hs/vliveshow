//
//  VLSPhotoSelectView.m
//  VLiveShow
//
//  Created by Cavan on 16/6/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSPhotoSelectView.h"

@interface VLSPhotoSelectView ()

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIButton *selectPhotoBtn;
@property (nonatomic, strong) UIButton *camaraBtn;
@property (nonatomic, strong) UIButton *closeBtn;


@end
@implementation VLSPhotoSelectView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.3];
        
        [self addSubview:self.contentView];
        self.contentView.sd_layout
        .leftSpaceToView(self, 0)
        .rightSpaceToView(self, 0)
        .bottomSpaceToView(self, 0)
        .heightIs(228.0f / 667 * SCREEN_HEIGHT);
        
        [self.contentView addSubview:self.camaraBtn];
        self.camaraBtn.sd_layout
        .leftSpaceToView(self.contentView, 38)
        .rightSpaceToView(self.contentView, 38)
        .bottomSpaceToView(self.contentView, 50)
        .heightIs(40);
        
        [self.contentView addSubview:self.selectPhotoBtn];
        self.selectPhotoBtn.sd_layout
        .leftSpaceToView(self.contentView, 38)
        .rightSpaceToView(self.contentView, 38)
        .bottomSpaceToView(self.camaraBtn, 20)
        .heightIs(40);
        
        [self.contentView addSubview:self.photoLabel];
        self.photoLabel.sd_layout
        .topSpaceToView(self.contentView, 15)
        .leftSpaceToView(self.contentView, 30)
        .rightSpaceToView(self.contentView, 30)
        .heightIs(20);
        
        [self.contentView addSubview:self.closeBtn];
        self.closeBtn.sd_layout
        .rightSpaceToView(self.contentView, 0)
        .centerYEqualToView(self.photoLabel)
        .widthIs(50)
        .heightIs(50);
        
       

        
    }
    return self;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self showPhotoView:NO animation:YES];
}

- (void)showPhotoView:(BOOL)show animation:(BOOL)animation
{
    if (animation) {
        [UIView animateWithDuration:0.42 animations:^{
            if (show) {
                self.alpha = 1;
            }else{
                self.alpha = 0;
            }
        }];
    }else{
        if (show) {
            self.alpha = 1;
        }else{
            self.alpha = 0;
        }
    }
}

- (UIView *)contentView
{
    if (_contentView == nil) {
        _contentView = [[UIView alloc] initWithFrame:CGRectZero];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

-(UIButton *)selectPhotoBtn{
    
    if (!_selectPhotoBtn) {
        _selectPhotoBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        _selectPhotoBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        [_selectPhotoBtn setBackgroundImage:[UIImage imageNamed:@"button_grad"] forState:UIControlStateNormal];
        [_selectPhotoBtn setTitle:LocalizedString(@"PHOTO_SELECT_FROM_ALBUM") forState:UIControlStateNormal];
        [_selectPhotoBtn setTintColor:[UIColor colorWithWhite:0.290 alpha:1.000]];
        [_selectPhotoBtn addTarget:self action:@selector(photoBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectPhotoBtn;
}

-(UIButton *)camaraBtn{
    
    if (!_camaraBtn) {
        _camaraBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        _camaraBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        [_camaraBtn setBackgroundImage:[UIImage imageNamed:@"button_grad"] forState:UIControlStateNormal];
        [_camaraBtn setTitle:LocalizedString(@"PHOTO_TAKE_PHOTO") forState:UIControlStateNormal];
        [_camaraBtn setTintColor:[UIColor colorWithWhite:0.290 alpha:1.000]];
        [_camaraBtn addTarget:self action:@selector(camaraBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _camaraBtn;
}

- (UIButton *)closeBtn {
    if (!_closeBtn) {
        self.closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeBtn setImage:[UIImage imageNamed:@"livebefore_close2"] forState:UIControlStateNormal];
        [self.closeBtn addTarget:self action:@selector(closeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}

- (UILabel *)photoLabel {
    if (!_photoLabel) {
        self.photoLabel = [[UILabel alloc] init];
        self.photoLabel.text = LocalizedString(@"PHOTO_CHANGE_COVER");
        self.photoLabel.textAlignment = NSTextAlignmentCenter;
        self.photoLabel.textColor = [UIColor colorWithWhite:0.608 alpha:1.000];
        self.photoLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    }
    return _photoLabel;
}
#pragma mark - 按钮事件

- (void)photoBtnClick:(UIButton*)sender{
    
    if ([self.delegate respondsToSelector:@selector(jumpToSystemPhotoAlbum)]) {
        [self.delegate jumpToSystemPhotoAlbum];
    }
}
- (void)camaraBtnClick:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(jumpToSystemCamera)]) {
        [self.delegate jumpToSystemCamera];
    }
}
- (void)closeBtnClick:(id)sender {
    [self showPhotoView:NO animation:YES];
    if ([self.delegate respondsToSelector:@selector(closePhotoSelectView)]) {
        [self.delegate closePhotoSelectView];
    }
}


@end
