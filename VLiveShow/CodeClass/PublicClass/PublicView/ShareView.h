//
//  ShareView.h
//  VLiveShow
//
//  Created by sp on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShareViewDelegate <NSObject>

- (void)clickShareBtnIndex:(NSInteger)index;

@end

@interface ShareView : UIView
/// 微信分享按钮
@property (nonatomic, strong)UIButton *weChatButton;
/// QQ 空间分享按钮
@property (nonatomic, strong)UIButton *zoneButton;
/// 朋友圈分享按钮
@property (nonatomic, strong)UIButton *friendShipButton;
/// QQ 分享按钮
@property (nonatomic, strong)UIButton *qqButton;
/// 微博分享按钮
@property (nonatomic, strong)UIButton *sinaButton;

@property (nonatomic,strong)UILabel *shareLabel;
@property (nonatomic, weak) id<ShareViewDelegate> delegate;

@end
