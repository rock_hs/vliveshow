//
//  VLSPwdInputView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSPwdInputViewDelegate <NSObject>

- (void)hidePwdInputWaring;
- (void)nextActionEnable:(BOOL)enable;

@end
@interface VLSPwdInputView : UIView

@property (nonatomic, copy)NSString *placeholder;
@property (nonatomic, strong)UITextField *pwdTF;
@property (nonatomic, weak)id <VLSPwdInputViewDelegate> delegate;
- (void)showWarningWithWrongPwd;

@end
