//
//  VLSTextField.m
//  VLiveShow
//
//  Created by Cavan on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSTextField.h"

@interface VLSTextField ()

@end

@implementation VLSTextField 


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tintColor = [UIColor grayColor];
        self.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.returnKeyType = UIReturnKeyDone;
        [self setCs_acceptEventIntervalFlag:YES];
        [self addSubview:self.lineLabel];
        self.lineLabel.sd_layout
        .leftSpaceToView(self, 0)
        .rightSpaceToView(self, 0)
        .bottomSpaceToView(self, 0)
        .heightIs(0.5);
        
    }
    return self;
}


- (UILabel *)lineLabel {
    if (!_lineLabel) {
        self.lineLabel = [[UILabel alloc] init];
        self.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
    }
    return _lineLabel;
}





@end
