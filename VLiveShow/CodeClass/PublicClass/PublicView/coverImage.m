//
//  coverImage.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "coverImage.h"

@interface coverImage ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation coverImage

- (instancetype)initWithFrame:(CGRect)frame WithURL:(NSString *)URL
{
    if (self = [super initWithFrame:frame]) {
        self.coverbg = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.coverbg.contentMode = UIViewContentModeScaleAspectFill;
        self.coverbg.clipsToBounds = YES;
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
        effectView.alpha = 0.5f;
        self.coverbg.backgroundColor = [UIColor clearColor];
        [self.coverbg addSubview:effectView];
        [self addSubview:self.coverbg];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectZero];
        self.label.textColor = [UIColor whiteColor];
        self.label.font = [UIFont boldSystemFontOfSize:SIZE_FONT_20];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.numberOfLines = 0;
        [self addSubview:self.label];
        
        effectView.sd_layout
        .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
        
        self.coverbg.sd_layout
        .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
        
        self.label.sd_layout
        .centerXEqualToView(self)
        .centerYEqualToView(self)
        .leftSpaceToView(self,0)
        .rightSpaceToView(self,0)
        .autoHeightRatio(0);
        
        [self.coverbg sd_setImageWithURL:[NSURL URLWithString:URL] placeholderImage:[UIImage imageNamed:@"Broadcast"]];
    }
    return self;
}

- (void)setType:(ANCHOR_MESSAGE_TYPE)type
{
    if (type == ANCHOR_LEAVE) {
        self.label.text = LocalizedString(@"ANCHOR_LEAVE");
    }else{
        self.label.text = nil;
    }
}

@end
