//
//  ImageViewInspect.h
//  AntsSportApp
//
//  Created by zidane on 16/3/10.
//  Copyright © 2016年 Xiaoyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewInspect : UIScrollView
- (instancetype)initWithFrame:(CGRect)frame andImage:(UIImage *)image;
- (instancetype)initWithFrame:(CGRect)frame andUrlstr:(NSString *)urlStr width:(float)width height:(float)height;
@end
