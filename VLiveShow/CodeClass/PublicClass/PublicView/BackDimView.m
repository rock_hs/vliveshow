//
//  BackDimView.m
//  VLiveShow
//
//  Created by sp on 16/8/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "BackDimView.h"

@implementation BackDimView
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.topImageView];
        [self addSubview:self.bottomImageView];
        self.bottomImageView.sd_layout
        .bottomSpaceToView(self, 0);
    }
    return self;
}

- (UIImageView*)topImageView
{
    if (_topImageView == nil) {
        _topImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, SC_HEIGHT/650*212)];
        _topImageView.backgroundColor = [UIColor grayColor];
        UIView *shdowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, SC_HEIGHT/650*212)];
        shdowView.alpha = 0.4;
        shdowView.backgroundColor = [UIColor blackColor];
        [_topImageView addSubview:shdowView];
    }

    return _topImageView;
}
- (UIImageView*)bottomImageView
{
    if (_bottomImageView == nil) {
        _bottomImageView= [[UIImageView alloc] initWithFrame:CGRectMake(0,  self.topImageView.frame.size.height, SC_WIDTH, SC_HEIGHT -self.topImageView.frame.size.height)];
        UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:effect];
        [_bottomImageView addSubview:effectView];
        effectView.sd_layout
        .spaceToSuperView(UIEdgeInsetsMake(0, 0, -20, 0));
    }
    return _bottomImageView;

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
