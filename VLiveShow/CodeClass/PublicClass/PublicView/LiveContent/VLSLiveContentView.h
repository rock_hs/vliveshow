//
//  VLSLiveContentView.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiftView.h"
#import "InkeTicket.h"
#import "GTGuestView.h"
#import "VLSGiftShow.h"
#import "VLSPriseView.h"
#import "VLSAnchorView.h"
#import "VLSTicketsView.h"

#import "VLSBottomView.h"
#import "VLSMessageBoard.h"
#import "BulletGroupView.h"
#import "VLSAudiencesView.h"
#import "ShareActionSheet.h"
#import "VLSHeadAlertView.h"
#import "HYLiveGiftBarrage.h"

@protocol VLSLiveContentViewDelegate <NSObject>

- (void)sendGift:(GiftModel * _Nonnull)model;
- (void)chargeVDiamond;

@end

@interface VLSLiveContentView : UIView

@property (nonatomic, weak) id<VLSLiveContentViewDelegate>delegate;

@property (nonatomic, assign) id tempDelegate;
//秀票view
@property (nonatomic, strong) VLSTicketsView *ticket;
//礼物view
@property (nonatomic, strong) GiftView *giftView;

// 礼物View
@property (nonatomic, strong) VLSGiftShow *giftShow;
// 主播view
@property (nonatomic, strong) VLSAnchorView *anchor;
// 嘉宾view
@property (nonatomic, strong) GTGuestView *guestView;
// 点赞所在的view层
@property (nonatomic, strong) VLSPriseView *priseView;
//透明view
@property (nonatomic, strong) UIView *halfAlphaView;
// 信息view
@property (nonatomic, strong) VLSMessageBoard *msgBoard;
// 底部控制按钮
@property (nonatomic, strong) VLSBottomView *bottomView;
// 弹幕
@property (nonatomic, strong) BulletGroupView *bulletView;
//分享
@property (nonatomic, strong) ShareActionSheet * shareAction;
// 观众view
@property (nonatomic, strong) VLSAudiencesView *audiencesView;
@property (nonatomic, assign) ActionStatus actionStatus;

@property (nonatomic, assign) KLiveRoomOrientation oriention;

@property (nonatomic, assign) int guestViewHeight;

@property (nonatomic, assign) ParticipantStatus participantStatus;
@property (nonatomic, strong) UIView* giftContainerView;

//@property (nonatomic, strong) UIView *BGview;

/* 礼物 */
@property (nonatomic,strong) HYLiveGiftBarrage *giftService;
- (instancetype)initWithFrame:(CGRect)frame actionStatus:(ActionStatus)actionStatus;
/* 显示礼物视图 */
- (void)showGiftListView;

//横屏模式，连线嘉宾视窗隐藏显示
- (void)guestViewHiddenAnimation:(void(^)(BOOL isGuestViewHidden))isGuestViewHiddenHandel;

/**
 *  设置标签内容
 *
 *  @param content 标签内容
 */
- (void)setTagContent:(NSString *)content;

- (void)hiddenGiftView;

- (void)giftlistivewReload;

//显示聊天框
- (void)showMessageBoardView;

@end
