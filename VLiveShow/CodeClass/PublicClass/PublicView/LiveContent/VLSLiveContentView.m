
//
//  VLSLiveContentView.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveContentView.h"
#import "UIView+LYAdd.h"
#import "VLSVedioView.h"
#import "VLiveShow-Swift.h"

#define anchorHight 35
#define audiencesHight 30
#define anchorY 26

@interface VLSLiveContentView ()<VLSMessageBoardViewDelegate, VLSGiftViewControllerDelegate>

@property (nonatomic, assign) BOOL isAnimationEnd;
@property (nonatomic, strong) VLSMessageBoardView* messageBoardView;
@property (nonatomic, strong) VLSGiftViewController* giftVC;

@end

@implementation VLSLiveContentView
- (void)setOriention:(KLiveRoomOrientation)oriention {
    _oriention = oriention;
    CGRect rect = (oriention == KLiveRoomOrientation_LandscapeRight || oriention == KLiveRoomOrientation_LandscapeLeft) ? CGRectMake(0, 0, MAX(SCREEN_HEIGHT, SCREEN_WIDTH), MIN(SCREEN_HEIGHT, SCREEN_WIDTH)) : [[UIScreen mainScreen] bounds];
    self.frame = rect;
    self.audiencesView.oriention = self.oriention;
    
    rect = self.anchor.frame;
    rect.origin.y = (oriention == KLiveRoomOrientation_LandscapeRight || oriention == KLiveRoomOrientation_LandscapeLeft) ? 5 : anchorY;
    self.anchor.frame = rect;
    
}

- (instancetype)initWithFrame:(CGRect)frame actionStatus:(ActionStatus)actionStatus
{
    if (self = [super initWithFrame:frame]) {
        self.isAnimationEnd = YES;
        self.actionStatus = actionStatus;
        
        [self addSubview: self.giftContainerView];
        [self addSubview:self.priseView];
        [self addSubview:self.anchor];
        [self addSubview:self.ticket];
        [self addSubview:self.audiencesView];
        [self addSubview:self.guestView];
        //  [self.contentView addSubview:self.bulletView];
        [self addSubview:self.msgBoard];

        [self addSubview:self.bottomView];
        
        //赞
        self.priseView.sd_layout
        .leftSpaceToView(self, 0)
        .rightSpaceToView(self, 0)
        .topSpaceToView(self, 0)
        .bottomSpaceToView(self, 0);
        
        // 观众列表view约束
        self.audiencesView.sd_layout
        .leftSpaceToView(self.anchor,7)
        .centerYEqualToView(self.anchor)
        .rightSpaceToView(self,55)
        .heightIs(audiencesHight);
        // V票
        self.ticket.sd_layout
        .topSpaceToView(self.anchor,10)
        .leftEqualToView(self.anchor)
        .heightIs(20);
        [self.ticket layoutTickets];
        
        self.guestView.sd_layout
        .topSpaceToView(self.ticket,10)
        .leftEqualToView(self.anchor)
        .heightIs(anchorHight);
        
      self.giftContainerView.sd_layout
        .leftSpaceToView(self, 0)
        .rightSpaceToView(self, 0)
        .topSpaceToView(self, 0)
        .bottomSpaceToView(self, 0);
        
         _giftService = [[HYLiveGiftBarrage alloc] initBarrageToView:self giftToView:self.giftContainerView];
       
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self reconfigUI];
}

- (void)reconfigUI {
    self.priseView.frame = self.bounds;
    float screenWidth = MIN(SCREEN_WIDTH, SCREEN_HEIGHT);
    if (self.oriention == KLiveRoomOrientation_Portrait || self.oriention == KLiveRoomOrientation_Unknown) {
        self.msgBoard.frame = CGRectMake(5, SCREEN_HEIGHT - 150 - 50 - self.guestViewHeight /*+ (self.guestViewHeight == 0 ? 0 : 50)*/, 2*screenWidth/3, 150);
    }else {
        self.msgBoard.frame = CGRectMake(5, SCREEN_HEIGHT - 150 - self.guestViewHeight, 2*screenWidth/3, 150);
    }
    
    self.audiencesView.sd_layout.rightSpaceToView(self,55);
    
    if (self.oriention == KLiveRoomOrientation_Portrait || self.oriention == KLiveRoomOrientation_Unknown) {
        return;
    }
    int y = 0;
    if (self.participantStatus == LiveContentView_Participant_Landscape_Show) {
        y = SCREEN_HEIGHT - 50 - ITEMHIGHT;
    }else {
        y = SCREEN_HEIGHT - 50;
    }
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        self.bottomView.frame = CGRectMake(0, y, SCREEN_WIDTH, 50);
    }];
}


- (void)guestViewHiddenAnimation:(void(^)(BOOL isGuestViewHidden))isGuestViewHiddenHandel {
    if (!self.isAnimationEnd) {
        return;
    }
    int y = 0;
    if (self.participantStatus == LiveContentView_Participant_Landscape_Show) {
        y = SCREEN_HEIGHT - 50;
    }else {
        y = SCREEN_HEIGHT - 50 - ITEMHIGHT;
    }
    self.isAnimationEnd = NO;
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        self.bottomView.frame = CGRectMake(0, y, SCREEN_WIDTH, 50);
        self.isAnimationEnd = YES;
        if (isGuestViewHiddenHandel) {
            isGuestViewHiddenHandel(y == SCREEN_HEIGHT - 50);
        }
    }];
}

- (void)showGiftListView{
    if (!_giftVC) {
        VLSGiftViewController* giftVC = [[VLSGiftViewController alloc] init];
        giftVC.delegate = self;
        giftVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        _giftVC = giftVC;
    }
    [[UIApplication sharedApplication].keyWindow.getCurrentVC presentViewController:_giftVC animated:YES completion:nil];
}

- (void)giftlistivewReload {
    [_giftVC reloadView];
}

#pragma mark - UIGestureRecognizer

- (void)handleDismissGiftTapGesture:(UITapGestureRecognizer *)tapGesture {
  [self hiddenGiftView];
}

- (void)handleDismissGiftPanGesture:(UIPanGestureRecognizer *)panGesture {
  [self hiddenGiftView];
}

- (VLSPriseView *)priseView
{
    if (_priseView == nil) {
        _priseView = [[VLSPriseView alloc] initWithFrame:self.bounds];
        _priseView.isHost = NO;
    }
    return _priseView;
}

- (VLSAnchorView *)anchor
{
    if (_anchor == nil) {
        _anchor = [[VLSAnchorView alloc]initWithFrame:CGRectMake(14, anchorY, 120, anchorHight)];
//        _anchor.layoutDelegate = self;
    }
    return _anchor;
}


- (GTGuestView *)guestView
{
    if (_guestView == nil) {
        _guestView = [[GTGuestView alloc]initWithFrame:CGRectMake(12, 67, 100, anchorHight)];
        _guestView.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.43f];
        _guestView.layer.cornerRadius = self.anchor.frame.size.height/2;
        _guestView.layer.masksToBounds = YES;
        _guestView.hidden = YES;

    }
    return _guestView;
}

- (VLSAudiencesView *)audiencesView
{
    if (_audiencesView == nil) {
//        CGRect frame = self.anchor.frame;
        _audiencesView = [[VLSAudiencesView alloc] initWithFrame:CGRectMake(100, 100, 100, 30)];
        _audiencesView.backgroundColor = [UIColor clearColor];
        _audiencesView.userInteractionEnabled = YES;
    }
    return _audiencesView;
}

- (VLSTicketsView *)ticket
{
    if (_ticket == nil) {
        _ticket = [[VLSTicketsView alloc]initWithFrame:CGRectMake(0, 100, 100, 20)];
        _ticket.backgroundColor = [UIColor clearColor];
    }
    return _ticket;
}
- (VLSGiftShow *)giftShow
{
    if (_giftShow == nil) {
        _giftShow = [[VLSGiftShow alloc] init];
//        [_giftShow setsetItemImages:self.images];
    }
    
    return _giftShow;
}

- (VLSBottomView *)bottomView
{
    if (_bottomView == nil) {
        int width = (self.oriention == KLiveRoomOrientation_LandscapeRight || self.oriention == KLiveRoomOrientation_LandscapeLeft) ? MAX(SCREEN_WIDTH, SCREEN_HEIGHT) : MIN(SCREEN_WIDTH, SCREEN_HEIGHT);
        _bottomView = [[VLSBottomView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT- 50, width, 50) actionStatus:self.actionStatus];
//        _bottomView.actionType = ActionTypeLive;
    }
    return _bottomView;
}

-(UIView *)halfAlphaView{

    if (_halfAlphaView == nil) {
        _halfAlphaView = [[UIView alloc] initWithFrame:CGRectMake(5, SCREEN_HEIGHT - 200, 2*SCREEN_WIDTH/3 - 20, 20)];
        _halfAlphaView.backgroundColor = [UIColor clearColor];
        _halfAlphaView.alpha = 0.8;
    }
    return _halfAlphaView;
}

- (VLSMessageBoard *)msgBoard
{
    
    if (_msgBoard == nil) {
        float screenWidth = MIN(SCREEN_WIDTH, SCREEN_HEIGHT);
        _msgBoard = [[VLSMessageBoard alloc] initWithFrame:CGRectMake(5, SCREEN_HEIGHT - 200, 2*screenWidth/3, 150)];
    }
    return _msgBoard;
}

- (ShareActionSheet *)shareAction
{
    if (_shareAction == nil) {
        NSArray * images = @[@"broadcast-share-qq",@"broadcast-share-zone",@"broadcast-share-weixin",@"share_moments",@"weibo01"];
        _shareAction = [[ShareActionSheet alloc] initWithFrame:CGRectMake(0, 0 , SCREEN_WIDTH, 258) delegate:_tempDelegate images:images];
    }
    return _shareAction;
}

- (BulletGroupView *)bulletView
{
    if (_bulletView == nil) {
        _bulletView = [[BulletGroupView alloc] initWithOriginY:[UIScreen mainScreen].bounds.size.height - 332 trajectoryHeight:200 trajectoryNum:3];
        BulletSettingDic *settingDic = [[BulletSettingDic alloc] init];
        [settingDic setBulletBackgroundColor:[UIColor colorWithWhite:0.4 alpha:0.5]];
        [settingDic setBulletTextColor:[UIColor redColor]];
        [settingDic setBulletTextFont:[UIFont systemFontOfSize:12]];
        [settingDic setBulletAnimationDuration:4.0];//间隔时间
        [settingDic setBulletHeight:30];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"suolong"]];
        [settingDic setbulletImageView:imageView];
        [settingDic setbulletName:@"小轩随风"];
        [_bulletView setBulletDic:settingDic];
        _bulletView.dataSource = @[@"我去"];
    }
    return _bulletView;
}

- (UIView*)giftContainerView
{
    if (_giftContainerView == nil)
    {
        _giftContainerView = [[UIView alloc] initWithFrame: self.bounds];
        _giftContainerView.backgroundColor = [UIColor clearColor];
        _giftContainerView.userInteractionEnabled = NO;
    }
    return _giftContainerView;
}

#pragma mark - 后面需要单独弄出来
//根据文字计算宽度
-(CGFloat)widthOfString:(NSString *)string font:(UIFont *)font height:(CGFloat)height
{
    NSDictionary * dict=[NSDictionary dictionaryWithObject: font forKey:NSFontAttributeName];
    CGRect rect=[string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return rect.size.width;
}

- (void)showMessageBoardView
{
    if (self.messageBoardView == nil) {
        VLSMessageBoardView* messageBoardView = [[VLSMessageBoardView alloc] init];
        messageBoardView.delegate = self;
        [self addSubview:messageBoardView];
        self.messageBoardView = messageBoardView;
    }
    [self.messageBoardView showWithY: self.bottomView.origin.y];
}

- (void)send:(NSString*)text
{
    //发送消息
    VLSSendGroupMsgModel *msg = [[VLSSendGroupMsgModel alloc] init];
    msg.message = text;
    [[VLSMessageManager sharedManager] sendGroupMessage:msg success:^(id result) {
        
    } error:^(int code, NSString *msg) {
        if (code == [@"被禁言" intValue]) {
            [VLSUtily toastWithText:LocalizedString(@"forbidSendMsg_toast") addtoview:self animation:YES duration:2];
        }
    }];
}

#pragma mark - VLSGiftViewControllerDelegate
- (void)sendGift:(GiftModel * _Nonnull)model
{
    if ([_delegate respondsToSelector:@selector(sendGift:)]) {
        [_delegate sendGift:model];
    }
}

- (void)chargeVDiamond
{
    if ([_delegate respondsToSelector:@selector(chargeVDiamond)]) {
        [_delegate chargeVDiamond];
    }
}

@end
