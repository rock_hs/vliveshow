//
//  VLSInviteGuestCell.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "InviteGuestBaseTableViewCell.h"

@interface VLSInviteGuestCell : InviteGuestBaseTableViewCell

@property (nonatomic, strong) VLSInviteGuestModel *model;

- (void)seletedStatus;
@end
