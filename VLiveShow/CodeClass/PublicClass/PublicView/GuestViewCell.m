//
//  GuestViewCell.m
//  VLiveShow
//
//  Created by sp on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "GuestViewCell.h"

@implementation GuestViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.headImageView];
        [self addSubview:self.nameLabel];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
- (UIImageView*)headImageView
{
    if (_headImageView == nil) {
        _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        _headImageView.image = [UIImage imageNamed:@"180_iPhone6Plus_red"];
        _headImageView.layer.cornerRadius = 25;
        _headImageView.layer.borderWidth = 2;
        _headImageView.layer.borderColor = [UIColor grayColor].CGColor;
        _headImageView.clipsToBounds = YES;
    }
    return _headImageView;
}
- (UILabel*)nameLabel
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, self.frame.size.width, 20)];
        _nameLabel.text = @"Jim";
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont systemFontOfSize:11];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _nameLabel;

}
- (void)setGuestModel:(VLSGuestModel *)guestModel
{
    self.nameLabel.text = guestModel.nickName;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:guestModel.avatars] placeholderImage:ICON_PLACEHOLDER];

}

@end
