//
//  LeaveOffContentView.h
//  VLiveShow
//
//  Created by sp on 16/8/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaveOffContentView : UIView
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *contentLabel;
@property (nonatomic,strong)UIView *leftView;
@property (nonatomic,strong)UIView *rightView;
@property (nonatomic,strong)UILabel *vTicketNum;
@property (nonatomic,strong)UILabel *vTicketName;
@property (nonatomic,strong)UILabel *watchNum;
@property (nonatomic,strong)UILabel *watchName;
@property (nonatomic,strong)UILabel *markLabel;
@end
