//
//  VLSCountryView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/20.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSCountryView.h"
#import "VLSCountryAreaManager.h"

@interface VLSCountryView ()

@property (nonatomic, strong)UILabel *lineLabel;
@property (nonatomic, strong)UILabel *leftLabel;
@property (nonatomic, strong)UIImageView *rightImgView;

@end

@implementation VLSCountryView


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doInit];
    }
    return self;
}

- (void)doInit {
    self.backgroundColor = RGB16(COLOR_BG_FFFFFF);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self addGestureRecognizer:tap];
    
    [self addSubview:self.leftLabel];
    [self addSubview:self.countryLabel];
    [self addSubview:self.rightImgView];
    
    self.leftLabel.sd_layout
    .leftSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0)
    .widthIs(80);
    
    self.countryLabel.sd_layout
    .leftSpaceToView(self.leftLabel, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0)
    .widthIs(100);
    
    self.rightImgView.sd_layout
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 10)
    .bottomSpaceToView(self, 10)
    .widthIs(20);
    
    [self addSubview:self.lineLabel];
    self.lineLabel.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .bottomSpaceToView(self, 0)
    .heightIs(0.5);
    
}

- (void)tapAction {
    if ([self.delegate respondsToSelector:@selector(selectCountryArea)]) {
        [self.delegate selectCountryArea];
    }
}

#pragma mark - getters

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        self.leftLabel = [[UILabel alloc] init];
        self.leftLabel.text = LocalizedString(@"LOGIN_COUNTRY_AREA");
        self.leftLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
        self.leftLabel.userInteractionEnabled = YES;
    }
    return _leftLabel;
}

- (UILabel *)countryLabel {
    if (!_countryLabel) {
        self.countryLabel = [[UILabel alloc] init];
        self.countryLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
        self.countryLabel.text = [VLSCountryAreaManager shareManager].name;
        self.countryLabel.userInteractionEnabled = YES;
    }
    return _countryLabel;
}

- (UIImageView *)rightImgView {
    if (!_rightImgView) {
        self.rightImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_goto"]];
    }
    return _rightImgView;
}

- (UILabel *)lineLabel {
    if (!_lineLabel) {
        self.lineLabel = [[UILabel alloc] init];
        self.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
    }
    return _lineLabel;
}

@end
