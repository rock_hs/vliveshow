//
//  VLSRoomIDView.h
//  VLiveShow
//
//  Created by SuperGT on 16/7/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSRoomIDView : UIView

@property (nonatomic, copy) NSString *RoomID;

- (void)layoutIsRight:(BOOL)isRight;

@end
