//
//  ShareView.m
//  VLiveShow
//
//  Created by sp on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "ShareView.h"

@implementation ShareView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadAllview];
    }
    return self;
}

- (void)loadAllview
{
    [self addSubview:self.shareLabel];
    
    
    [self addSubview:self.qqButton];
    self.qqButton.sd_layout
    .leftSpaceToView(self, SC_WIDTH/375*53 )
    .bottomSpaceToView(self,0)
    .widthIs(20)
    .heightIs(20);

    [self addSubview:self.zoneButton];
    self.zoneButton.sd_layout
    .leftSpaceToView(self.qqButton, (SC_WIDTH-SC_WIDTH/375*53*2-100)/4)
    .bottomSpaceToView(self,0)
    .widthIs(20)
    .heightIs(20);
    
    [self addSubview:self.weChatButton];
    self.weChatButton.sd_layout
    .leftSpaceToView(self.zoneButton,(SC_WIDTH-SC_WIDTH/375*53*2-100)/4)
    .bottomSpaceToView(self,0)
    .widthIs(20)
    .heightIs(20);

    [self addSubview:self.friendShipButton];
    self.friendShipButton.sd_layout
    .leftSpaceToView(self.weChatButton, (SC_WIDTH-SC_WIDTH/375*53*2-100)/4)
    .bottomSpaceToView(self,0)
    .widthIs(20)
    .heightIs(20);
    
    
    [self addSubview:self.sinaButton];
    self.sinaButton.sd_layout
    .leftSpaceToView(self.friendShipButton, (SC_WIDTH-SC_WIDTH/375*53*2-100)/4)
    .bottomSpaceToView(self,0)
    .heightIs(20)
    .widthIs(20);



}
- (UIButton *)weChatButton {
    if (!_weChatButton) {
        self.weChatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.weChatButton setImage:[UIImage imageNamed:@"wechat"] forState:UIControlStateNormal];
        [self.weChatButton setImage:[UIImage imageNamed:@"wechat_choose"] forState:UIControlStateSelected];
        [self.weChatButton addTarget:self action:@selector(weChatAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _weChatButton;
}

- (UIButton *)zoneButton {
    if (!_zoneButton) {
        self.zoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.zoneButton setImage:[UIImage imageNamed:@"QQzone"] forState:UIControlStateNormal];
        [self.zoneButton setImage:[UIImage imageNamed:@"QQzone_choose"] forState:UIControlStateSelected];
        [self.zoneButton addTarget:self action:@selector(zoneAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _zoneButton;
}

- (UIButton *)friendShipButton {
    if (!_friendShipButton) {
        self.friendShipButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.friendShipButton setImage:[UIImage imageNamed:@"friends-choose"] forState:UIControlStateNormal];
        [self.friendShipButton setImage:[UIImage imageNamed:@"friends_choose"] forState:UIControlStateSelected];
        [self.friendShipButton addTarget:self action:@selector(friendShipAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _friendShipButton;
}

- (UIButton *)qqButton {
    if (!_qqButton) {
        self.qqButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.qqButton setImage:[UIImage imageNamed:@"QQ"] forState:UIControlStateNormal];
        [self.qqButton setImage:[UIImage imageNamed:@"QQ_choose"] forState:UIControlStateSelected];
        [self.qqButton addTarget:self action:@selector(qqAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _qqButton;
}

- (UIButton *)sinaButton {
    if (!_sinaButton) {
        self.sinaButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.sinaButton setImage:[UIImage imageNamed:@"sina"] forState:UIControlStateNormal];
        [self.sinaButton setImage:[UIImage imageNamed:@"sina_choose"] forState:UIControlStateSelected];
        [self.sinaButton addTarget:self action:@selector(sinaAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sinaButton;
}
- (UILabel*)shareLabel
{
    if (_shareLabel == nil) {
        _shareLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, 25)];
        _shareLabel.text = LocalizedString(@"LIVE_SHARE");
        _shareLabel.font = [UIFont boldSystemFontOfSize:14];
        _shareLabel.textColor = [UIColor colorFromHexRGB:@"C3A851"];
        _shareLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _shareLabel;
}
#pragma mark - 微信分享按钮

- (void)weChatAction:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
        [self.delegate clickShareBtnIndex:2];
    }
    
}

#pragma mark - qq 空间分享按钮

- (void)zoneAction:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
        [self.delegate clickShareBtnIndex:1];
    }
    
}
#pragma mark - 朋友圈分享按钮

- (void)friendShipAction:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
        [self.delegate clickShareBtnIndex:3];
    }
}

#pragma mark - QQ 分享按钮

- (void)qqAction:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
        [self.delegate clickShareBtnIndex:0];
    }
    
}
#pragma mark - 微博分享按钮

- (void)sinaAction:(UIButton *)button {
    
    
    if ([self.delegate respondsToSelector:@selector(clickShareBtnIndex:)]) {
        [self.delegate clickShareBtnIndex:4];
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
