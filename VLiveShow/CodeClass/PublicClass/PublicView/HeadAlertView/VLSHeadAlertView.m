//
//  VLSHeadAlertView.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSHeadAlertView.h"
@interface VLSHeadAlertView ()
@property (nonatomic, strong)UIImageView *backImage;
@property (nonatomic, strong)UILabel *titleLabel;
@property (nonatomic, strong)UIImageView *leftImgView;

@end

@implementation VLSHeadAlertView
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
//        [self addSubview:self.backImage];
//        [self addSubview:self.titleLabel];
//        [self addSubview:self.leftImgView];
//        self.hidden = YES;
        [self doInit];
    }
    return self;
}

- (void)doInit {
    [self addSubview:self.backImage];
    [self addSubview:self.titleLabel];
    [self addSubview:self.leftImgView];
    self.hidden = YES;
    
    self.backImage.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0);
    
    self.titleLabel.sd_layout
    .topSpaceToView(self, 25)
    .centerXEqualToView(self)
    .heightIs(20);
    
    self.leftImgView.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(self, 25)
    .rightSpaceToView(self.titleLabel, 10)
    .widthIs(20);
    
}
- (void)setMessage:(NSString *)message
{
    _message = message;
    self.titleLabel.text = _message;
}

#pragma mark actions
- (void)showHeadAlert
{
    self.hidden = NO;
    CGRect frame = self.frame;
    frame.origin.y = 0;
    [UIView animateWithDuration:0.3f animations:^{
        self.frame = frame;
        [self setNeedsLayout];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)showHeadAlertThenHidden
{
    [self showHeadAlert];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        [self hiddenHeadAlert];
    });

}

- (void)showHeadAlertAndHiddenAfterDuration:(NSTimeInterval)duration
{
    [self showHeadAlert];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        [self hiddenHeadAlert];
    });
}

- (void)hiddenHeadAlert
{
    CGRect frame = self.frame;
    frame.origin.y = -frame.size.height;
    [UIView animateWithDuration:0.3f animations:^{
        self.frame = frame;
        [self setNeedsLayout];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (void)showLeftImage:(UIImage *)image WithTitle:(NSString *)title {
    
    self.leftImgView.hidden = NO;
    self.leftImgView.image = image;
    self.titleLabel.text = title;
    [self showHeadAlertThenHidden];
}
- (void)showTitleWithString:(NSString *)titleStr {
    
    self.leftImgView.hidden = YES;
    self.titleLabel.text = titleStr;
    [self showHeadAlertThenHidden];
}

- (void)showTitleWithString:(NSString *)titleStr withDuration: (NSTimeInterval)duration
{
    self.leftImgView.hidden = YES;
    self.titleLabel.text = titleStr;
    [self showHeadAlertAndHiddenAfterDuration: 4];
}

#pragma mark getters
- (UIImageView *)backImage
{
    if (_backImage == nil) {
//        _backImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _backImage = [[UIImageView alloc] init];
        _backImage.backgroundColor = [UIColor redColor];
//        _backImage.image = [UIImage imageNamed:@"Bar_6plus.png"];
    }
    return _backImage;
}

- (UILabel *)titleLabel
{
    if (_titleLabel == nil) {
//        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.frame.size.width, self.frame.size.height-20)];
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_16];
        _titleLabel.textColor = [UIColor whiteColor];
        [self.titleLabel setSingleLineAutoResizeWithMaxWidth:SCREEN_WIDTH];
    }
    return _titleLabel;
}

- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        self.leftImgView = [[UIImageView alloc] init];
    }
    return _leftImgView;
}

@end
