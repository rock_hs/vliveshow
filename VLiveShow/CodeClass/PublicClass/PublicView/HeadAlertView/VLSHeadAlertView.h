//
//  VLSHeadAlertView.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSHeadAlertView : UIView
@property (nonatomic, strong)NSString *message;

- (void)showHeadAlert;
- (void)showHeadAlertThenHidden;
- (void)showHeadAlertAndHiddenAfterDuration:(NSTimeInterval)duration;
- (void)hiddenHeadAlert;
- (void)showLeftImage:(UIImage *)image WithTitle:(NSString *)title;
- (void)showTitleWithString:(NSString *)titleStr;
- (void)showTitleWithString:(NSString *)titleStr withDuration: (NSTimeInterval)duration;
@end
