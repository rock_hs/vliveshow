//
//  GTGuestView.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "GTGuestView.h"
#import "VLSMessageManager.h"
#import "VLiveShow-Swift.h"

#define imageH (self.frame.size.height-4)

@interface GTGuestView ()

@property (nonnull,nonatomic,strong) UIImageView *AnchorIcon;

@property (nonnull, nonatomic, strong) RollLabel* labelScorll;

@property (nonnull, nonatomic, strong) UILabel *Nothinglabel;

@end

@implementation GTGuestView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.AnchorIcon = [[UIImageView alloc]initWithFrame:CGRectMake(2, 2, imageH, imageH)];
        self.AnchorIcon.layer.masksToBounds = YES;
        self.AnchorIcon.layer.cornerRadius = imageH / 2;
        //        self.AnchorIcon.layer.shadowColor = [UIColor whiteColor].CGColor;
        self.AnchorIcon.backgroundColor = [UIColor clearColor];
        [self addSubview:self.AnchorIcon];
        self.AnchorIcon.sd_layout
        .topSpaceToView(self,2)
        .leftSpaceToView(self,2)
        .widthIs(imageH)
        .heightIs(imageH);
        
        self.Nothinglabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.Nothinglabel.font = [UIFont systemFontOfSize:SIZE_FONT_10];
        self.Nothinglabel.textColor = [UIColor whiteColor];
        [self addSubview:self.Nothinglabel];
        
        self.Nothinglabel.sd_layout
        .leftSpaceToView(self.AnchorIcon,5)
        .topSpaceToView(self,3)
        .heightIs(14);
        
        [self.Nothinglabel setSingleLineAutoResizeWithMaxWidth:100];
        
        self.labelScorll = [[RollLabel alloc] initWithFrame:CGRectMake(0, 0, 60, 15)];
        [self addSubview:self.labelScorll];
        
        self.labelScorll.sd_layout
        .leftSpaceToView(self.AnchorIcon,5)
        .bottomSpaceToView(self,3)
        .heightIs(15);
        
        self.Nothinglabel.text = LocalizedString(@"INVITE_GUEST");
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gusetTap)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)gusetTap
{
    if ([_delegate respondsToSelector:@selector(guestViewClicked:)]) {
        [_delegate guestViewClicked:self];
    }
}

-(void)setGuestID:(NSString *)GuestID
{
    _GuestID = GuestID;

    [[VLSMessageManager sharedManager] getUserProfile:GuestID succ:^(VLSUserProfile *profile) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.labelScorll setText: profile.userName font: SIZE_FONT_10];
            [_AnchorIcon sd_setImageWithURL:[NSURL URLWithString:profile.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
            [self layoutWholeView];
        });
        self.model = profile;
    }];
}

#pragma mark 自适应方法

- (void)layoutWholeView
{
    
    // 设置self宽度
    
    [UIView animateWithDuration:1.0f animations:^{
        //        [self updateLayout];
        self.sd_layout.widthIs([self longerLabelwidths] + 15);
        [self updateLayout];
    } completion:^(BOOL finished) {
        
    }];
    
}


- (CGFloat)longerLabelwidths
{
    [self.labelScorll updateLayout];    
    
    CGFloat Nothingwidth = self.labelScorll.frame.size.width;
    
    CGFloat NothinglabelX = self.labelScorll.frame.origin.x + Nothingwidth;
    CGFloat onLineNumberX = self.Nothinglabel.frame.origin.x + self.Nothinglabel.frame.size.width;
    return (NothinglabelX >= onLineNumberX)?NothinglabelX:onLineNumberX;
}

- (UIView *)longerLabel
{
    [self.labelScorll updateLayout];
    [self.Nothinglabel updateLayout];
    CGFloat Nothingwidth = self.labelScorll.frame.size.width;
    
    CGFloat NothinglabelX = self.labelScorll.frame.origin.x + Nothingwidth;
    CGFloat onLineNumberX = self.Nothinglabel.frame.origin.x + self.Nothinglabel.frame.size.width;
    return (NothinglabelX >= onLineNumberX)?self.labelScorll:self.Nothinglabel;
    
}

@end
