//
//  ShareLiveOfView.m
//  VLiveShow
//
//  Created by sp on 16/7/6.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "ShareLiveOfView.h"
#import "AppDelegate.h"
#import <UMSocialCore/UMSocialCore.h>
@interface ShareLiveOfView ()

@property (nonatomic, strong) UIImageView *BGview;


@property (nonatomic,strong)UIImageView *logoImageV;

@property (nonatomic,strong)UIImageView *iconImageV;

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *numberLabel;

@property (nonatomic, strong) UILabel *endLabel;

@property (nonatomic, strong) UILabel *LookNum;

@property (nonatomic, strong) UILabel *LookEd;

@property (nonatomic, strong) UILabel *textLabel;

@property (nonatomic, strong) UIButton *followBtn;

@property (nonatomic, strong) UIButton *backBtn;


@end


@implementation ShareLiveOfView
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.BGview = [[UIImageView alloc]initWithFrame:CGRectZero];
        [self.BGview setImage:[UIImage imageNamed:@"Broadcast"]];
        [self addSubview:self.BGview];
        /**
         *  ios8自带毛玻璃效果
         */
        
        UIBlurEffect *beffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        UIVisualEffectView *view = [[UIVisualEffectView alloc]initWithEffect:beffect];
        
        view.frame = self.bounds;
        
        [self addSubview:view];
        
        self.logoImageV = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 120, 40, 76, 34)];
        self.logoImageV.image = [UIImage imageNamed:@"VliveLogo"];
        [self addSubview:self.logoImageV];
        self.iconImageV = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.iconImageV.layer.borderColor = [UIColor grayColor].CGColor;
        self.iconImageV.clipsToBounds = YES;
        self.iconImageV.layer.borderWidth = 4;
        self.iconImageV.layer.cornerRadius = 46;
        [self addSubview:self.iconImageV];

        self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 185, SCREEN_WIDTH - 40, 10)];
        self.nameLabel.font = [UIFont systemFontOfSize:10];
        self.nameLabel.textColor = [UIColor whiteColor];
        self.nameLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.nameLabel];
        self.numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 195, SCREEN_WIDTH - 40, 10)];
        self.numberLabel.font = [UIFont systemFontOfSize:5];
        self.numberLabel.textColor = [UIColor whiteColor];
        self.numberLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.numberLabel];
        self.endLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.endLabel.font = [UIFont systemFontOfSize:34];
        self.endLabel.textColor = [UIColor whiteColor];
        self.endLabel.textAlignment = NSTextAlignmentCenter;
        self.endLabel.text = LocalizedString(@"LIVE_OVER");
        [self addSubview:self.endLabel];
        [self addSubview:self.LookNum];
        [self addSubview:self.LookEd];
        self.followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.followBtn setBackgroundImage:[UIImage imageNamed:@"button03"] forState:UIControlStateNormal];
        [self.followBtn addTarget:self action:@selector(followAction) forControlEvents:UIControlEventTouchUpInside];
        [self.followBtn setTitle:LocalizedString(@"FOLLOW_ANCHOR") forState:UIControlStateNormal];
        self.followBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.followBtn setTitleColor:RGB16(COLOR_FONT_FF784A) forState:UIControlStateNormal];
        [self addSubview:self.followBtn];
        self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.backBtn setBackgroundImage:[UIImage imageNamed:@"button03"] forState:UIControlStateNormal];
        [self.backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [self.backBtn setTitle:LocalizedString(@"BACK_INHOME") forState:UIControlStateNormal];
        [self.backBtn setTitleColor:RGB16(COLOR_FONT_FF784A) forState:UIControlStateNormal];
        self.backBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:self.backBtn];
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI
{
    self.BGview.sd_layout
    .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
    self.iconImageV.sd_layout
    .topSpaceToView(self,75)
    .centerXEqualToView(self)
    .heightIs(92)
    .widthIs(92)
    .centerXEqualToView(self);
    self.endLabel.sd_layout
    .topSpaceToView(self,205)
    .centerXEqualToView(self)
    .heightIs(40);
    [self.endLabel setSingleLineAutoResizeWithMaxWidth:300];
    
    self.LookNum.sd_layout
    .topSpaceToView(self.endLabel,0)
    .leftSpaceToView(self,0)
    .widthIs(SCREEN_WIDTH/2-15)
    .heightIs(30);
    
    self.LookEd.sd_layout
    .topSpaceToView(self.endLabel,0)
    .rightSpaceToView(self,0)
    .widthIs(SCREEN_WIDTH/2+15)
    .heightIs(30);
    
    self.backBtn.sd_layout
    .leftSpaceToView(self,30)
    .rightSpaceToView(self,30)
    .bottomSpaceToView(self,140)
    .heightIs(40);
    
    self.followBtn.sd_layout
    .leftSpaceToView(self,30)
    .rightSpaceToView(self,30)
    .bottomSpaceToView(self.backBtn,20)
    .heightIs(40);

}
#pragma mark-判断有没有关注过
- (void)judgeIsFocues
{

    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        NSString *isfocues = [result objectForKey:@"is_attended"];
        if ([isfocues isEqualToString:@"N"]) {
            self.isFocues = NO;
        }else
        {
            self.isFocues = YES;
        }
        [self getFollow];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
        }
    };
    [VLSLiveHttpManager getrelationShip:self.userID callBack:callback];
}



/**
 *  关注主播按钮
 *
 *  @return void
 */

- (void)followAction
{
    [self judgeIsFocues];
}

- (void)getFollow
{
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self success:LocalizedString(@"ALREADY_CONCERONED")];
    };
    callback.errorBlock = ^(id result){
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            
        }
    };
    if (self.isFocues == YES) {
        [self success:LocalizedString(@"MINE_USER_INBFOCUES")];
    }else
    {
        [VLSLiveHttpManager focusToUser:self.userID callback:callback];
    }
}
- (void)backAction
{
  AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [self removeFromSuperview];
    [app showTabBarController];
    
}

- (void)reloadContent
{
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        VLSUserInfoViewModel *model = result;
        self.nameLabel.text = model.userName;
        self.LookNum.text = [NSString stringWithFormat:@"%@",self.seeNum];
        self.numberLabel.text = self.userID;
         [self.iconImageV sd_setImageWithURL:[NSURL URLWithString:model.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
        }
    };
    [VLSLiveHttpManager getUserInfo:self.userID callback:callback];
}

-(void)success:(NSString*)title
{
            MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = title;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
}

- (UILabel*)LookNum
{
    
    if (_LookNum == nil) {
        _LookNum = [[UILabel alloc]initWithFrame:CGRectZero];
        _LookNum.font = [UIFont systemFontOfSize:24];
        _LookNum.textAlignment = NSTextAlignmentRight;
        _LookNum.textColor = RGB16(COLOR_FONT_FF784A);
        
    }
    return _LookNum;
}
- (UILabel*)LookEd
{
    
    if (_LookEd == nil) {
        _LookEd = [[UILabel alloc]initWithFrame:CGRectZero];
        _LookEd.font = [UIFont systemFontOfSize:20];
        _LookEd.textAlignment = NSTextAlignmentLeft;
        _LookEd.textColor = [UIColor whiteColor];
        _LookEd.text = LocalizedString(@"LOOK_NUMBER");
        
    }
    return _LookEd;
}


@end
