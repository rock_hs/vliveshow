//
//  VLSCountryView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/20.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSCountryViewDelegate <NSObject>

- (void)selectCountryArea;

@end
@interface VLSCountryView : UIView

@property (nonatomic, strong)UILabel *countryLabel;
@property (nonatomic, weak)id <VLSCountryViewDelegate> delegate;

@end
