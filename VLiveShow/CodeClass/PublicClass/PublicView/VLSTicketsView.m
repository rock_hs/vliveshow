//
//  VLSTicketsView.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSTicketsView.h"
#import "NSString+UserFile.h"

@interface VLSTicketsView ()

@property (nonatomic, strong) UIView *BGview;

@property (nonatomic, strong) UIImageView *leftImageview;

@property (nonatomic, strong) UILabel *textLabel;

@property (nonatomic, strong) UIImageView *rightImage;

@property (nonatomic, strong) dispatch_source_t timer;

@end

@implementation VLSTicketsView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)setupUI
{
    self.BGview = [[UIView alloc] init];
    self.BGview.backgroundColor = [UIColor colorWithRed:0.f green:0.f blue:0.f alpha:.4f];
    [self addSubview:self.BGview];
    self.BGview.sd_layout
    .topSpaceToView(self,0)
    .leftSpaceToView(self,0)
    .bottomSpaceToView(self,0)
    .rightSpaceToView(self,0);
    self.BGview.sd_cornerRadiusFromHeightRatio = @0.5f;
    
    self.leftImageview = [[UIImageView alloc] init];
    [self.leftImageview setImage:[UIImage imageNamed:@"ic_vpiao"]];
    self.leftImageview.userInteractionEnabled = YES;
    self.leftImageview.backgroundColor = [UIColor clearColor];
    self.leftImageview.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.leftImageview];
    self.leftImageview.sd_layout
    .leftSpaceToView(self,4)
    .centerYEqualToView(self)
    .widthIs(12)
    .heightIs(12);
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.textColor = [UIColor whiteColor];
    self.textLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
    self.textLabel.backgroundColor = [UIColor clearColor];
    self.textLabel.text = LocalizedString(@"V_TICKETS_RANKING");
    [self.textLabel sizeToFit];
    [self addSubview:self.textLabel];
    self.textLabel.sd_layout
    .leftSpaceToView(self.leftImageview,2)
    .centerYEqualToView(self)
    .heightIs(16);
    [self.textLabel setSingleLineAutoResizeWithMaxWidth:200];
    
    self.rightImage = [[UIImageView alloc]init];
    self.rightImage.backgroundColor = [UIColor clearColor];
    self.rightImage.userInteractionEnabled = YES;
    [self.rightImage setImage:[UIImage imageNamed:@"ic_into"]];
    self.rightImage.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.rightImage];
    self.rightImage.sd_layout
    .leftSpaceToView(self.textLabel,5)
    .centerYEqualToView(self)
    .heightIs(10)
    .widthIs(5);
    
}

- (void)tapGestureAction
{
    if ([_delegate respondsToSelector:@selector(VLSTicketsViewTap)]) {
        [_delegate VLSTicketsViewTap];
    }
}

- (void)layoutTickets
{
    [self.textLabel updateLayout];
    CGFloat numWidth = self.textLabel.frame.size.width;
    if (numWidth >= 200) {
        numWidth = 200;
    }
    CGFloat wholeW = self.textLabel.frame.origin.x + numWidth + 15;
    self.sd_layout.widthIs(wholeW);
}

// 因为当前要显示的值在千分逗号以后，解析之后是一，所以需要有一个保存当前显示的值
- (void)setLookNum:(NSString *)lookNum
{
    _lookNum = lookNum;
    self.textLabel.text = [LocalizedString(@"V_TICKETS_RANKING") stringByAppendingFormat: @" %@", [NSString thousandpointsNum:[_lookNum integerValue]]] ;
    NSLog(@"当前显示的值%@",_lookNum);
    [self layoutTickets];
}

@end
