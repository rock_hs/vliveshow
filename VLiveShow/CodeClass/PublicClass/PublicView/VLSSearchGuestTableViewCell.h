//
//  VLSSearchGuestTableViewCell.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "InviteGuestBaseTableViewCell.h"

@interface VLSSearchGuestTableViewCell : InviteGuestBaseTableViewCell

@property (nonatomic, strong) VLSInviteGuestModel *model;

@end
