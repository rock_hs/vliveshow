//
//  BackDimView.h
//  VLiveShow
//
//  Created by sp on 16/8/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackDimView : UIView
@property (nonatomic,strong)UIImageView * topImageView;
@property (nonatomic,strong)UIImageView * bottomImageView;
@end
