//提取V票的网络请求

#import "HTTPFacade.h"
//TODO:闭包暂时放这里，以后放在全局定义中
typedef void(^NetworkSucBlock)(id object);
typedef void(^NetworkErrorBlock)(NSError* error);

@interface HTTPFacade (withdraw)
/**
 *  V票可提现金额
 */
+ (void)cashoutUsable:(NetworkSucBlock)sucBlock errBlock:(NetworkErrorBlock)errBlock;
/**
 * V票余额
 */
+ (void)cashoutReamin:(NetworkSucBlock)sucBlock errBlock:(NetworkErrorBlock)errBlock;
@end
