//
//  VLSLiveViewController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMultiLiveViewController.h"
#import "VLSOthersViewController.h"
#import "VLSUserProfileController.h"
#import "VLSLiveNoViewController.h"
#import "VLSLittleNavigationController.h"
#import "closeRoomActionViewController.h"
#import "VLSLiveHttpManager.h"
#import "NSString+UserFile.h"
#import "AppDelegate.h"
#import "ApplyBGView.h"
#import "VLSMessageBoard.h"
#import "UIView+LYAdd.h"
#import "VLSUserTrackingManager.h"
#import "VLiveShow-Swift.h"
#import "VLSHeartManager.h"
#import "UIView+MJExtension.h"
#import "VLSMessageViewController.h"
#import "VLSNavigationController.h"

@interface VLSMultiLiveViewController()<UITextFieldDelegate,VLSBottomViewDelegate,VLSAudiencesViewDelegate,VLSMessageBoardDelegate,VLSPriseViewDelegate,VLSUserProfileControllerDelelegate,VLSMessageManagerDelegate,ShareActionSheetDelegate,ShareViewDelegate,LeaveOffViewDelegate,VLSAnchorViewDelegate,VLSVideoViewDelegate,GTGuestViewDelegate,UIGestureRecognizerDelegate,closeRoomDelegate,VLSMessageBoardDelegate,VLSPriseManagerDelegate,VLSTicketsViewDelegate, VLSLiveContentViewDelegate>
{
    CGFloat currentTranslate;   //用来记录清屏移动的位置
    NSInteger likeCount;        //自己点赞数量
    BOOL contentShow;           //用来是否清屏
    BOOL anchorLeave;            //主播暂时离开
    
    
    VLSLittleNavigationController *nav;
    VLSLiveNoViewController *vmc;
}
@property(nonatomic, assign)  BOOL isBlockingUpdatingTickets;
@property (nonatomic, assign) BOOL isLandscapeParticipantViewShown;      //横屏连线嘉宾是否显示,只要有连线嘉宾，默认显示
@property (nonatomic, strong) RankingListViewController *ranklistVC;
@property(nonatomic, strong)UILabel *tagLab;
@property (nonatomic, copy) void (^configUIBlock)(KLiveRoomOrientation orientation);    //在进入房间，获取到liveinfo后再次做横竖屏布局
@property (nonatomic, assign) UIInterfaceOrientation currentOrientation;
@end

@implementation VLSMultiLiveViewController
- (void)willMoveToParentViewController:(UIViewController *)parent {
    if (parent == nil) {
        self.configUIBlock = nil;
    }
}

- (void)setActionType:(ActionType)actionType {
    [super setActionType:actionType];
    if (_contentView) {
        if (actionType == ActionTypeLessonAttendance || actionType == ActionTypeLessonAudience) {
            return;
        }
        [self.contentView.bottomView setActionType:actionType];
        self.configUIBlock(KLiveRoomOrientation_LandscapeRight);
        self.configUIBlock = nil;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [self viewdisappearOrientationConfig];
    [self forcePortrait];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    nav.view.frame = CGRectMake(0, SCREEN_HEIGHT - 275, SCREEN_WIDTH, 275);
    vmc.view.frame = CGRectMake(0, SCREEN_HEIGHT - 275, SCREEN_WIDTH, 275);
    if ([VLSMessageManager sharedManager].newMail == YES) {
        [self.contentView.bottomView changeMailIcon:YES];
    }else{
        
        [self.contentView.bottomView changeMailIcon:NO];
    }
  
    [self getCurrentBlance];
  
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        if (self.currentOrientation != UIInterfaceOrientationLandscapeRight || self.currentOrientation != UIInterfaceOrientationLandscapeLeft) {
            [self forceLandscape];
        }
    }else {
        [self configOriention];
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:ENABLEENTERLIVEROOM object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isLandscapeParticipantViewShown = YES;
    self.currentOrientation = UIInterfaceOrientationPortrait;
    
    likeCount = 0;
    contentShow = YES;
    currentTranslate = 0.0f;
    
    [VLSAgoraVideoService sharedInstance].messagedelegate = self;
    
    [self.view addSubview:self.videoView];
    [self.view addSubview:self.contentView];
    [self.view addSubview:self.tagLab];
    [self.view addSubview:self.closeViewButton];
    [self.view addSubview:self.roomIDview];
    [self.view addSubview:self.headAlert];
    
//    CGRectMake(SCREEN_WIDTH - 200 - 10, 98, 200, 30);
    self.roomIDview.sd_layout
    .rightSpaceToView(self.view, 10);
    
    int y = (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) ? 5 : 28;
    self.closeViewButton.sd_layout
    .rightSpaceToView(self.view, 12)
    .topSpaceToView(self.view, y)
    .widthIs(30)
    .heightIs(30);
    
    if ([AccountManager sharedAccountManager].account.isSuperUser) {
        self.contentView.bottomView.actionType = ActionTypeAdmin;
        if (self.selfIsAnchor) {
            self.isAudiences = YES;
            if (self.anchorTickets) {
                [self receiveUpdateTicket:self.anchorTickets];
            }
        }
    }else{
        if (self.selfIsAnchor) {
            self.contentView.bottomView.actionType = ActionTypeAnchor;
            self.isAudiences = YES;
            if (self.anchorTickets) {
                [self receiveUpdateTicket:self.anchorTickets];
            }

        }else{
            [self.videoView addSubview:self.videoCoverView];
            self.contentView.bottomView.actionType = ActionTypeLive;
        }
    }
    
    self.liveInfo = [[VLSLiveInfoViewModel alloc]init];
    self.liveInfo.watchNum = @"0";
    self.liveInfo.coverUrl = self.coverUrl;
/*****************************点赞管理************************/
    self.priseManager = [[VLSPriseManager alloc] init];
    self.priseManager.delegate = self;
/*****************************第一次安装***********************/
    [self checkIsFirst];
    
/********************************************************/
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(willEnterIntoBackground:) name: UIApplicationDidEnterBackgroundNotification object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(willTerminate:) name: UIApplicationWillTerminateNotification object: nil];
    
    //增加逻辑判断：如果当前用户正在直播或者正在连线，则被踢出时需同时关闭直播间
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginouted:) name:NOTIFICATION_LOGINOUTED object:nil];
  
    UIPanGestureRecognizer *panGestureReconginzer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panInContentView:)];
    panGestureReconginzer.delegate = self;
    [self.view addGestureRecognizer:panGestureReconginzer];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(priseTap:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    NSLog(@"进入直播间");
    
    NSString *language = [[NSLocale preferredLanguages] firstObject];
    NSArray *languageArray = [VLSHeartManager shareHeartManager].heartModel.liveAnnouncement;
    if ([language containsString:@"Hant"] || [language containsString:@"HK"] || [language containsString:@"TW"]) {
        VLSMessageViewModel *model = [[VLSMessageViewModel alloc]initWidthUser:nil message:languageArray[1][@"tw"] types:10016];
        [self.contentView.msgBoard addDataSource:@[model]];
    } else if([language containsString:@"EN"]){
        VLSMessageViewModel *model = [[VLSMessageViewModel alloc]initWidthUser:nil message:languageArray[2][@"en"] types:10016];
        [self.contentView.msgBoard addDataSource:@[model]];
    }else{
        VLSMessageViewModel *model = [[VLSMessageViewModel alloc]initWidthUser:nil message:languageArray[0][@"cn"] types:10016];
        [self.contentView.msgBoard addDataSource:@[model]];
 }
    WS(ws)
    self.configUIBlock = ^(KLiveRoomOrientation orientation){
        ws.orientation = orientation;
        ws.videoView.orientation = orientation;
        if (orientation == KLiveRoomOrientation_LandscapeLeft || orientation == KLiveRoomOrientation_LandscapeRight) {
            [ws configOriention];
        }
    };
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{

    if ([NSStringFromClass([touch.view class]) isEqualToString:@"VLSPriseView"] || [NSStringFromClass([touch.view class]) isEqualToString:@"UIViewControllerWrapperView"] || [NSStringFromClass([touch.view class]) isEqualToString:@"AgoraVideoRenderIosView"] || [NSStringFromClass([touch.view class]) isEqualToString:@"VLSLiveContentView"]) {
        return YES;
    }
    return NO;

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    
    if ([[otherGestureRecognizer.view class] isSubclassOfClass:[UITableView class]]) {
        return NO;
    }
    
    if( [[otherGestureRecognizer.view class] isSubclassOfClass:[UITableViewCell class]] ||
       [NSStringFromClass([otherGestureRecognizer.view class]) isEqualToString:@"UITableViewCellScrollView"] ||
       [NSStringFromClass([otherGestureRecognizer.view class]) isEqualToString:@"UITableViewWrapperView"]) {
        
        return YES;
    }
    return YES;
}
/*
- (void)configOriention {
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        if (orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft) {
            return;
        }
        
        if (SYSTEM_VERSION_GREATER_THAN(@"10.0")) {
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:YES];
        }else {
            NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
            [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        }
        
        self.contentView.oriention = UIDeviceOrientationLandscapeRight;
    }else if (self.orientation == KLiveRoomOrientation_Portrait || self.orientation == KLiveRoomOrientation_Unknown) {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        if (orientation == UIInterfaceOrientationPortrait) {
            return;
        }
        
        if (SYSTEM_VERSION_GREATER_THAN(@"10.0")) {
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];
        }else {
            NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
            [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        }
        
        self.contentView.oriention = KLiveRoomOrientation_Portrait;
    }
    UIViewController *v = [UIViewController new];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:v animated:NO completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [v dismissViewControllerAnimated:NO completion:^{
                //这么做是因为在push动画过程中present控制有几率导致dismiss无法完成
                    [v.view removeFromSuperview];
                    [v removeFromParentViewController];
                }];
            });
        }];
    });
}
*/
- (void)configOriention {
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        if (self.currentOrientation != UIInterfaceOrientationPortrait) {
            return;
        }
        if (self.orientation != KLiveRoomOrientation_LandscapeRight || self.orientation != KLiveRoomOrientation_LandscapeLeft) {
            self.contentView.oriention = self.orientation;
            [self reconfigUI:self.orientation];
        }
        [self forceLandscape];
        self.contentView.oriention = UIDeviceOrientationLandscapeRight;
    }else if(self.orientation == KLiveRoomOrientation_Portrait || self.orientation == KLiveRoomOrientation_Unknown) {
        if (self.currentOrientation == UIInterfaceOrientationPortrait) {
            return;
        }
        if (self.orientation != KLiveRoomOrientation_Portrait || self.orientation != KLiveRoomOrientation_Unknown) {
            self.contentView.oriention = self.orientation;
            [self reconfigUI:self.orientation];
        }
        [self forcePortrait];
        self.contentView.oriention = KLiveRoomOrientation_Portrait;
    }
}

- (void)reconfigUI:(KLiveRoomOrientation)orientation {
    if (self.contentView.oriention == orientation) {
        return;
    }
    self.videoView = nil;
    self.contentView = nil;
    self.closeViewButton = nil;
    [self.view addSubview:self.videoView];
    [self.view addSubview:self.contentView];
    [self.view addSubview:self.closeViewButton];
    int y = (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) ? 5 : 28;
    self.closeViewButton.sd_layout
    .rightSpaceToView(self.view, 12)
    .topSpaceToView(self.view, y)
    .widthIs(30)
    .heightIs(30);
}

- (void)forceLandscape {
    VLSNavigationController *navi = (VLSNavigationController*)self.navigationController;
    [navi changeSupportedInterfaceOrientations:UIInterfaceOrientationMaskLandscapeRight];
    
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = UIInterfaceOrientationLandscapeRight;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
        self.currentOrientation = UIInterfaceOrientationLandscapeRight;
    }
}

- (void)forcePortrait {
    VLSNavigationController *navi = (VLSNavigationController*)self.navigationController;
    [navi changeSupportedInterfaceOrientations:UIInterfaceOrientationMaskPortrait];
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = UIInterfaceOrientationPortrait;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
        self.currentOrientation = UIInterfaceOrientationPortrait;
    }
}
/*
- (void)viewdisappearOrientationConfig {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait) {
        return;
    }
    
    if (SYSTEM_VERSION_GREATER_THAN(@"10.0")) {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];
    }else {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    }
    
    UIViewController *v = [UIViewController new];
    [self presentViewController:v animated:NO completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [v dismissViewControllerAnimated:NO completion:nil];
        });
    }];
}
*/

#pragma mark 检查是否是第一次做主播或者看直播
- (void)checkIsFirst
{
    if (self.selfIsAnchor) {
        [self.contentView.bottomView showisFirstShow];
    }else{
        [self.contentView.bottomView showisFirstWatch];
    }
}

#pragma mark - 继承父类方法

- (void)reloadAnchorInfo
{
    [self.contentView.anchor setPmodel:self.anchorProfile];
}

- (void)reloadMemberList
{
    [self.contentView.audiencesView setDataArray:self.menberList];
}


- (void)reloadRoomInfo
{
    self.roomIDview.RoomID = self.anchorId;
    // v票信息
    [self receiveUpdateTicket:self.liveInfo.totalGainTicket];
    [self.videoCoverView.coverbg sd_setImageWithURL:[NSURL URLWithString:self.liveInfo.coverUrl] placeholderImage:[UIImage imageNamed:@"Broadcast"]];
    
    if (self.liveInfo.topics && self.liveInfo.topics.count>1) {
//        [self.contentView setTagContent:self.liveInfo.topics[1]];
        [self setTagContent:self.liveInfo.topics[1]];
    }
    //直播房间名称传值
    self.contentView.anchor.roomName = self.liveInfo.title;
}
//没有关注主播
- (void)notAttendAnchor
{
    //    [self.contentView.anchor setType:AnchorOnScreen];
    [self.contentView.anchor notConcerned];
}

- (void)guestCancelLinkSuccess
{
    [super guestCancelLinkSuccess];
    if (anchorLeave) {
        //当主播暂时离开时候， 自己获取发送布局的权限，自己离开时候要发送当前布局给后台
        [[VLSAVMultiManager sharedManager] guestLeaveWhenAnchorLeaveMoment];
    }
    //**********改变底部button状态*****************/
    [self.contentView.bottomView changefriendStatus:NO];
}

- (void)reloadVideoView
{
    [super reloadVideoView];
    
#pragma mark - 嘉宾恢复视屏框
    if ([[VLSMessageManager sharedManager] checkUserIsGuests:[VLSMessageManager sharedManager].host.userId]){
        [VLSAVMultiManager sharedManager].linkStatus = UserLinkStatusOnCamera;
        [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:NO];
        
        self.contentView.bottomView.actionType = ActionTypeGuest;
    }else if (self.liveInfo.layout.anchors.count > 1){
        if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
            self.contentView.bottomView.actionType = self.isSelfAttendance ? ActionTypeConnectedLandscape_Attendance : ActionTypeConnectedLandscape_Audience;
        }else if (self.liveInfo.layout.anchors.count == 1) {
            self.contentView.bottomView.actionType = ActionTypeNormalLandscape;
        }
    }
    
    for (VLSVideoAnchorModel *model in self.liveInfo.layout.anchors) {
        VLSVideoViewModel *vmodel = [[VLSVideoViewModel alloc] initViewModelWidthVideoAnchorModel:model];
        BOOL userIsSelf = [[VLSMessageManager sharedManager] checkUserIsSelf:vmodel.userId];
        if (userIsSelf && vmodel.muteVoice) {
            BOOL userIsGuests = [[VLSMessageManager sharedManager] checkUserIsGuests:vmodel.userId];
            [self receiveGuestMuteSelf:vmodel.muteVoice];
            if (userIsGuests) {
                if (!vmodel.muteVoiceBySelf) {
                    [self.contentView.bottomView voiceEnable:!vmodel.muteVoice];
                }
            }
        }
    }
}

- (void)anchorOffLine
{
    anchorLeave = YES;
    self.videoCoverView.hidden = NO;
    [self.videoCoverView setType:ANCHOR_LEAVE];
}

- (void)anchorIsBack
{
    anchorLeave = NO;
    self.videoCoverView.hidden = YES;
    [self.videoCoverView setType:ANCHOR_BACK];
}

- (void)guestIsBackUid:(NSString *)uid
{
    [self.videoView guestOffLine:NO usrid:uid];
}

- (void)guestOffLineUid:(NSString *)uid
{
    [self.videoView guestOffLine:YES usrid:uid];
}

- (void)touchOnLiveBaseView:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    // 判断是否在嘉宾的view上
    [super touchOnLiveBaseView:touches withEvent:event];
}

- (void)setTagContent:(NSString *)content {
    CGFloat width = [VLSUtily widthOfString:content font:[UIFont systemFontOfSize:12] height:20];
    int y = (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeRight) ? 70 - 23 : 70;
    self.tagLab.sd_layout.topSpaceToView(self.view,y).rightSpaceToView(self.view,10).widthIs(width+18).heightIs(20.0);
    self.tagLab.text = content;
}

- (UILabel *)tagLab
{
    if (!_tagLab) {
        
        _tagLab = [[UILabel alloc]init];
        _tagLab.layer.cornerRadius = 10;
        _tagLab.layer.borderWidth = 1;
        _tagLab.layer.borderColor = RGB16(COLOR_FONT_C3A851).CGColor;
        _tagLab.backgroundColor = [UIColor clearColor];
        _tagLab.textColor = RGB16(COLOR_FONT_C3A851);
        _tagLab.font = [UIFont systemFontOfSize:12];
        _tagLab.textAlignment = NSTextAlignmentCenter;
    }
    
    return _tagLab;
}

#pragma mark - VLSMessageManagerDelegate
//收到常规的消息

- (void)receiveNormalMessage:(NSMutableArray *)messageList
{
    [self.contentView.msgBoard addDataSourceWithArray: messageList];
}
/**
 *  收到常规的礼物
 */
- (void)receiveNormalGiftMessage:(GiftModel *)giftModel{

  if (giftModel.giftType == 1) {
    [_contentView.giftService addGiftEnum:giftModel];
  }else if (giftModel.giftType == 0){
    giftModel.giftImage = giftModel.giftImage;
  }
  giftModel.currentCount = [GiftCountModel getSameCount:[NSString stringWithFormat:@"%@%@%@",giftModel.giftID,giftModel.userID,giftModel.timerStamp]];
  
  [_contentView.giftService addBarrageModel:giftModel];
    
    NSString *userid = [NSString stringWithFormat:@"%ld",[AccountManager sharedAccountManager].account.uid];
    BOOL isAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:userid];
    if (isAnchor)
    {
        [self sendUpdatedTickets];
    }
  
}

//收到自定义的礼物
- (void)receiveCustomGiftMessage:(NSMutableArray *)giftList
{
    [self.contentView.msgBoard addDataSource:giftList];
}

//收到弹幕消息
- (void)receiveBarrageMessage:(NSMutableArray *)barrageList
{
    
}

//收到赞消息
- (void)receivePariseMessage:(NSInteger)count
{
    [self.priseManager excuteOperation];
}

- (void)showPriseAnimation
{
    [self.contentView.priseView showAnimation];
}

//新增群成员
- (void)receiveAddMembers:(NSArray *)userList isRoot:(BOOL)root
{
    if (!root) {
        // 真实的人
        self.cNum++;
    }
    

    // 加入观众列表数组
    [self.contentView.audiencesView insertDataArray:userList];
    
}

//删除群成员
- (void)receiveDeleteMembers:(NSArray *)userList
{
    if (self.cNum > 0) {
        self.cNum--;
    }
}

//更新群成员
- (void)receiveUpdataMembers:(NSArray *)userList
{
    
}

// 更新秀票
- (void)receiveUpdateTicket:(NSString *)count
{
    if (count == nil)
    {
        return;
    }
    self.contentView.ticket.lookNum = count;
    // 基数判断
//    if (!self.tureTickets) {
//        
//        self.contentView.ticket.lookNum = count;
//        
//    }else{
//        [self.contentView.ticket compareLabelTureNum:count];
//    }
//    self.tureTickets = count;

}

// 直播间人数更新
- (void)receiveUpdateMemberCount:(NSString *)count
{
    if (!self.isAudiences) {
        return;
    }
        
    // 基数判断
    if (!self.tureWatch) {
        
        if ([self.contentView.audiencesView checkisFullList]) {
            // 如果人数已满
            NSInteger countNum = [count integerValue];
            /*
            if (countNum >= self.contentView.audiencesView.dataArray.count) {
                self.tureWatch = count;
            }else{
                self.tureWatch = [NSString stringWithFormat:@"%lu",(unsigned long)self.contentView.audiencesView.dataArray.count];
            }
            */
            NSInteger maxNum = MAX(countNum, self.contentView.audiencesView.dataArray.count);
            self.tureWatch = [@(maxNum) stringValue];
        }else{
            self.tureWatch = [NSString stringWithFormat:@"%lu",(unsigned long)self.contentView.audiencesView.dataArray.count];
        }
        self.contentView.anchor.lookNum = self.tureWatch;
        [self.contentView.anchor layoutWholeView];
        
    }else{
    
        if ([self.contentView.audiencesView checkisFullList]) {
            NSInteger countNum = [count integerValue];
            /*
            if (countNum >= self.contentView.audiencesView.dataArray.count) {
                self.tureWatch = count;
            }else{
                self.tureWatch = [NSString stringWithFormat:@"%lu",(unsigned long)self.contentView.audiencesView.dataArray.count];
            }
             */
            NSInteger maxNum = MAX(countNum, self.contentView.audiencesView.dataArray.count);
            self.tureWatch = [@(maxNum) stringValue];
        }else{
            self.tureWatch = [NSString stringWithFormat:@"%lu",(unsigned long)self.contentView.audiencesView.dataArray.count];
        }
        [self.contentView.anchor compareLabelTureNum:self.tureWatch];
    }
}


//被主播拉黑
- (void)receiveSendToBlacklist
{
    
    VLSAVMultiManager *manager = [VLSAVMultiManager sharedManager];
    if(manager.linkStatus == UserLinkStatusApplySuccess || manager.linkStatus == UserLinkStatusOnCamera){
        [self guestQuiteRoom];
    }else{
        //观众关闭直播
        if (manager.linkStatus == UserLinkStatusApplying) {
            [self guestCancelLinkRequest];
            [self audienceQuiteRoom];
        }else{
            [self audienceQuiteRoom];
        }
    }
    
    [self showSendBlackAlert];
    [self isShowMsgStationBoard:NO];
    
    [[VLSUserTrackingManager shareManager] trackingQuitroom:@"bePullBlack" context: self.roomTrackContext];
}
//收到解散群的群的消息
- (void)receiveDeleteGroup
{
    [[VLSMessageManager sharedManager].liveAnchors removeAllObjects];
    [[VLSAgoraVideoService sharedInstance] leaveChannel];
    [VLSAgoraVideoService sharedInstance].channelKey = nil;
    //群解散的时候把弹出来的控制器都dismiss
    [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    [self isShowMsgStationBoard:NO];
    
    if (self.contentView.shareAction) {
        [self.contentView.shareAction dismiss];
    }
    if (self.contentView.msgBoard) {
        [[UIApplication sharedApplication].keyWindow endEditing:YES];
    }
    
    LeaveOffViewController *lfVC = [LeaveOffViewController new];
    lfVC.shareView.delegate = self;
    lfVC.delegate = self;
    lfVC.contentView.nameLabel.text = self.anchorProfile.userName;
    lfVC.contentView.contentLabel.text = self.liveInfo.title;
    lfVC.isAnchor = self.selfIsAnchor;
    lfVC.userID = self.anchorProfile.userId;
    lfVC.anchorProfile = self.anchorProfile;
    lfVC.roomId = self.liveInfo.roomId;
    lfVC.contentView.markLabel.text = self.liveInfo.roomMark;
    lfVC.lessonId = @([[self valueForKey:@"lessonId"] integerValue]);
    [lfVC.headImageView sd_setImageWithURL:[NSURL URLWithString:self.anchorProfile.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
    [lfVC.backView.bottomImageView sd_setImageWithURL:[NSURL URLWithString:self.anchorProfile.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
    [lfVC.backView.topImageView sd_setImageWithURL:[NSURL URLWithString:self.liveInfo.coverUrl] placeholderImage:ICON_PLACEHOLDER];

    lfVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:lfVC animated:NO];
    [[VLSUserTrackingManager shareManager] trackingQuitroom:@"liveend" context: self.roomTrackContext];
}

- (void)receiveUpdateLiveLayout:(NSMutableArray *)liveAnchors
{
    [self.videoView layoutLivesViewWithGuests:liveAnchors];
    
    self.isSelfAttendance = NO;
    [liveAnchors enumerateObjectsUsingBlock:^(VLSVideoViewModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx != 0) {
            if ([obj.userId integerValue] == [[[AccountManager sharedAccountManager] account] uid]) {
                self.isSelfAttendance = YES;
            }
        }
    }];
    
    ParticipantStatus status = 0;
    ActionType actionType = 0;
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        if (liveAnchors.count > 1) {
            status = self.isLandscapeParticipantViewShown ? LiveContentView_Participant_Landscape_Show : LiveContentView_Participant_Landscape_Hide;
            actionType = self.isSelfAttendance ? ActionTypeConnectedLandscape_Attendance : ActionTypeConnectedLandscape_Audience;
        }else {
            status = LiveContentView_Participant_Landscape_Normal;
            [self.contentView.bottomView setMj_y:(SCREEN_HEIGHT - self.contentView.bottomView.height)];
            actionType = ActionTypeNormalLandscape;
        }
        
        self.contentView.participantStatus = status;
        self.videoView.participantStatus = status;
        self.contentView.bottomView.actionType = actionType;
    }
    if (self.selfIsAnchor) {
        [self sendLayoutToBack];
    }
}

- (void)getCurrentBlance{

  __weak typeof(self) ws = self;
  
  [[VLSIAPManager sharedInstance] loadCurrentBlance:nil];
  
}

//禁言
- (void)receiveForbidSendMsg:(NSArray *)userList {
    WS(ws)
    [userList enumerateObjectsUsingBlock:^(VLSMessageViewModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.user.userId integerValue] == [[[AccountManager sharedAccountManager] account] uid]) {
            [VLSUtily toastWithText:LocalizedString(@"forbidSendMsg_toast") addtoview:self.view animation:YES duration:2];
            dispatch_async(dispatch_get_main_queue(), ^{
                [ws.contentView.bottomView setForbidSendMsg:true];
                [ws isShowMsgStationBoard:NO];
                [[[[UIApplication sharedApplication] delegate] window] endEditing:YES];
            });
        }
    }];
}

//取消禁言
- (void)receiveForbidSendMsgCancel:(NSArray *)userList {
    if (userList.count >= 3 && [userList objectAtIndex:2]) {
        NSString *userId = [userList objectAtIndex:2];
        if ([[[AccountManager sharedAccountManager] account] uid] == [userId integerValue]) {
            UIAlertView *al = [[UIAlertView alloc] initWithTitle:LocalizedString(@"forbidSendMsg") message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:LocalizedString(@"LIVEBEFORE_I_KONWEN"), nil];
            [al show];
            [self.contentView.bottomView setForbidSendMsg:false];
        }
    }
}

#pragma mark - 加载房间信息
- (void)loadRoomInfo
{
    WS(ws)
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if (result) {
            VLSLiveInfoViewModel *model = (VLSLiveInfoViewModel*)result;
            int orientation = [model.orientation isEqualToNumber:@2] ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
            if (ws.configUIBlock) {
                ws.configUIBlock(orientation);
            }
            
            //watchNum真实观看人数

            long long watchNum = [self.lessonId intValue] ? [[result onlineNum] longLongValue] : [[result scNum] longLongValue];
//            NSString *numStr = 0;
//            if ([self.contentView.audiencesView checkisFullList]) {
//                // 如果人数已满
//                NSInteger maxNum = MAX(watchNum, self.contentView.audiencesView.dataArray.count);
//                numStr = [@(maxNum) stringValue];
//            }else{
//                numStr = [NSString stringWithFormat:@"%lu",(unsigned long)self.contentView.audiencesView.dataArray.count];
//            }

            self.contentView.anchor.lookNum = [@(watchNum) stringValue];
            
            self.liveInfo = result;
            
            [self reloadRoomInfo];
            if (self.liveInfo.layout) {
                [self reloadVideoView];
            }
            [self.contentView.bottomView enableRatingButton: !model.rated];
        }
        if (!self.liveInfo.living) {
            [self receiveDeleteGroup];
        }
    };
    callback.errorBlock = ^(id result){
        
    };
    [VLSLiveHttpManager getLiveInfo:self.roomId callback:callback];
}


#pragma mark -VLSAgoraVideoServiceDelegate

- (void)agoraListenerFirstRemoteVideoDecodedOfUid:(NSUInteger)uid
{
//    [self.videoCoverView removeFromSuperview];
    self.videoCoverView.hidden = YES;
}

#pragma mark - VLSVideoViewDelegate
//有嘉宾的时候变换bottomView高度
- (void)videoViewUpdateNeedChangeBottomHeight:(CGFloat)height oneGuestHight:(CGFloat)oneGuestHight
{
    CGRect priseFrame = self.contentView.priseView.frame;
    if (height == 0.0f) {
        if (oneGuestHight != 0.0f) {
            priseFrame.size.height = SCREEN_HEIGHT - oneGuestHight;
        }else{
            priseFrame.size.height = SCREEN_HEIGHT;
        }
    }else{
        priseFrame.size.height = SCREEN_HEIGHT - height;
    }
    self.contentView.priseView.frame = priseFrame;
    
    CGRect bottomFrame = self.contentView.bottomView.frame;
    bottomFrame.origin.y = SCREEN_HEIGHT - 50 - height;
    self.contentView.bottomView.frame = bottomFrame;
    
    CGRect msgFrame = self.contentView.msgBoard.frame;
    msgFrame.origin.y = SCREEN_HEIGHT - 200 - height + (height == 0 ? 0 : 50);
    self.contentView.msgBoard.frame = msgFrame;
//    self.contentView.guestViewHeight = height;
    
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        NSInteger guestCount = self.videoView.NewGuestInfos.count - 1;
        
        float itemWidth = ITEMWIDTH(guestCount);
        float currentWidth = guestCount * itemWidth + CGRectGetWidth(self.contentView.msgBoard.frame);
        float screenWidth = MAX(SCREEN_WIDTH, SCREEN_HEIGHT);
        if (currentWidth > screenWidth) {
            self.contentView.guestViewHeight = height;
        }else {
            self.contentView.guestViewHeight = 0;
        }
    }else {
        self.contentView.guestViewHeight = height;
    }
}
#pragma mark - 主播、嘉宾点击
//点击主播头像
- (void)anchoViewClicked:(VLSAnchorView *)anchorView{
    
    self.userID = self.anchorId;
    if (self.anchorProfile == nil) {
        VLSUserProfile *userpf = [VLSUserProfile new];
        userpf.userId = self.anchorId;
        [self showUserProfile:userpf];
    }else
    {
        [self showUserProfile:self.anchorProfile];
    }
}
// 点击关注按钮
- (void)focusAnchor
{
    ManagerCallBack  *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        NSLog(@"关注主播成功");
    };
    callback.errorBlock = ^(id result)
    {
        //        [self error:result];
        NSLog(@"关注主播失败");
    };
    [VLSLiveHttpManager focusToUser:self.anchorId callback:callback];
}

// 点击嘉宾头像
- (void)guestViewClicked:(GTGuestView *)guestView{
    
    self.userID = guestView.GuestID;
    [self showUserProfile:guestView.model];
}

- (void)showUserProfile:(VLSUserProfile *)user
{
    [self isShowMsgStationBoard:NO];
    VLSUserProfileController *userController = [[VLSUserProfileController alloc] init];
    userController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    userController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    userController.actionType = [self.lessonId intValue] ? ActionTypeLessonTeacher : ActionTypeLive;
    //slave模式需要改变userID
    if (user.userId.length == 9) {
        user.userId = [user.userId substringToIndex:7];
    }
    userController.userProfile = user;
    userController.roomId = self.roomId;
    userController.delegate = self;
    [self presentViewController:userController animated:YES completion:^{
    }];
}

- (void)showRating
{
    WS(ws)
    CourseRateViewController* ratingVC = [[UIStoryboard storyboardWithName: @"CourseRate" bundle: nil] instantiateInitialViewController];
    ratingVC.liveShowModel = self.liveInfo;
    ratingVC.anchorProfile = self.anchorProfile;
    ratingVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    ratingVC.finishBlock = ^(BOOL result)
    {
        if (result)
        {
            ws.liveInfo.rated = YES;
            [ws.contentView.bottomView enableRatingButton: NO];
        }
    };
    
    [self presentViewController: ratingVC animated: NO completion: nil];
}

#pragma mark - VLSAudiencesViewDelegate 观众点击
- (void)audiencesViewSelect:(VLSUserProfile *)user
{
    self.userID = user.userId;
    [self showUserProfile:user];
}

#pragma mark - v票的点击事件
- (void)VLSTicketsViewTap
{
    if (self.orientation == KLiveRoomOrientation_Portrait || self.orientation == KLiveRoomOrientation_Unknown) {
        RankingListViewController *ranklist = [RankingListViewController  new];
        ranklist.hidesBottomBarWhenPushed = YES;
        ranklist.userId = self.anchorProfile.userId;
        [self.navigationController pushViewController:ranklist animated:YES];
    }else {
        [self showRankingVC:YES];
    }
}

#pragma mark - VLSUserProfileControllerDelelegate 观众个人信息页面的回调
- (void)fromUserProfileAnchorClose
{
    [self closeLiveRoomFrom:NO];
}
- (void)fromUserProfileGuestCancelLink
{
    [self guestEndLink];
}


- (void)touchMain
{
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    [[VLSUserTrackingManager shareManager] trackingViewprofile:self.userID];
    vlsVC.userId =self.userID;
    
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vlsVC animated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
- (void)getBlack
{
    if (self.userID == self.anchorId)
    {
        
        //判断观众是否是嘉宾
        VLSAVMultiManager *manager = [VLSAVMultiManager sharedManager];
        if(manager.linkStatus == UserLinkStatusApplySuccess || manager.linkStatus == UserLinkStatusOnCamera){
            [self quitRoomAction];
        }else{
            //观众关闭直播
            if (manager.linkStatus == UserLinkStatusApplying) {
                [self guestCancelLinkRequest];
                [self audienceQuiteRoom];
            }else{
                [self audienceQuiteRoom];
            }
        }
        [self.contentView.giftService stopAllOperation];
    }
}
//关注回调
- (void)followClick:(BOOL)isAttent
{
    [self.contentView.anchor updateStatusFocusBt:isAttent];
}
#pragma mark - VLSMessageBoardDelegate 信息cell点击的代理
- (void)messageBoardSelectUser:(VLSUserProfile *)user
{
    self.userID = user.userId;
    [self showUserProfile:user];
    [[[[UIApplication sharedApplication] delegate] window] endEditing:YES];
}
#pragma mark -  点赞
- (void)priseViewTouch
{
    [self.priseManager excuteOperation];
    
    if (likeCount == 0) {
        likeCount +=1;
        VLSSendGroupMsgModel *msg = [[VLSSendGroupMsgModel alloc] init];
        msg.message = LocalizedString(@"LIVE_ANCHOR_SEND_PRAISE");
        [[VLSMessageManager sharedManager] sendPraiseMessage:1 sendModel:msg];
        //点赞动画只发一次
        [[VLSMessageManager sharedManager] sendPraiseMessage:1 success:^{
            
        } fail:^(int code, NSString *msg) {
            if (code == 10017) {
                [VLSUtily toastWithText:LocalizedString(@"forbidSendMsg_toast") addtoview:self.view animation:YES duration:2];
            }
        }];
    }
}


#pragma mark - VLSBottomViewDelegate 底部按钮事件代理

//消息按钮点击
- (void)bottomMessageClicked
{
    [self.contentView showMessageBoardView];
}

- (void)showRankingVC:(BOOL)toShow {
    CGRect __block rect = self.ranklistVC.view.frame;
    if (toShow) {
        if (self.ranklistVC) {
            return;
        }
        self.ranklistVC = [RankingListViewController  new];
        self.ranklistVC.orientation = self.orientation;
        self.ranklistVC.hidesBottomBarWhenPushed = YES;
        self.ranklistVC.userId = self.anchorProfile.userId;
        [self.view addSubview:self.ranklistVC.view];
        [self addChildViewController:self.ranklistVC];
        rect.origin.x = -K_LANDSCAPEVIEWWIDTH;
        rect.size.width = K_LANDSCAPEVIEWWIDTH;
        rect.size.height = SCREEN_HEIGHT;
        self.ranklistVC.view.frame = rect;
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            rect.origin.x = 0;
            self.ranklistVC.view.frame = rect;
        }];
    }else {
        rect.origin.x = -K_LANDSCAPEVIEWWIDTH;
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            self.ranklistVC.view.frame = rect;
        } completion:^(BOOL finished) {
            [self.ranklistVC.view removeFromSuperview];
            [self.ranklistVC removeFromParentViewController];
            self.ranklistVC = nil;
        }];
    }
}

- (void)isShowMsgStationBoard:(BOOL)isShow{

    if (isShow) {
        if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
            vmc = [[VLSLiveNoViewController alloc] init];
            vmc.orientation = self.orientation;
            nav = [[VLSLittleNavigationController alloc] initWithRootViewController:vmc];
            nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            nav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            nav.fatherVC = self;
            [nav.view setMj_x:SCREEN_WIDTH];
            [nav.view setMj_w: K_LANDSCAPEVIEWWIDTH];
            UIWindow *window = [[UIApplication sharedApplication] keyWindow];
            [window addSubview:nav.view];
            [UIView animateWithDuration:.3f animations:^{
                [nav.view setMj_x:SCREEN_WIDTH - K_LANDSCAPEVIEWWIDTH];
            }];
        }else {
            vmc = [[VLSLiveNoViewController alloc] init];
            nav = [[VLSLittleNavigationController alloc] initWithRootViewController:vmc];
            nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            nav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            nav.fatherVC = self;
            UIWindow *window = [[UIApplication sharedApplication] keyWindow];
            [window addSubview:nav.view];
        }
    }else{
        if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
            [UIView animateWithDuration:.3f animations:^{
                [nav.view setMj_x:SCREEN_WIDTH];
            } completion:^(BOOL finished) {
                [nav.view removeFromSuperview];
                nav = nil;
            }];
        }else {
            if (nav) {
                
                [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
                    nav.view.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 275 );
                } completion:^(BOOL finished) {
                    [nav.view removeFromSuperview];
                    nav = nil;
                }];
                return;
            }
        }
    }
}

-(void)hideMsgStation{

    [self isShowMsgStationBoard:NO];
}

-(void)bottomGuestViewHiddenClicked:(UIButton*)btn {
//    [btn setSelected:!btn.selected];
//    self.isLandscapeParticipantViewShown = !btn.selected;
//    //bottomview layout
//    self.contentView.participantStatus = self.isLandscapeParticipantViewShown ? LiveContentView_Participant_Landscape_Show : LiveContentView_Participant_Landscape_Hide;
//    [self.contentView layoutSubviews];
//    //videoView layout
//    [self.videoView setParticipantStatus:(self.isLandscapeParticipantViewShown ? LiveContentView_Participant_Landscape_Show : LiveContentView_Participant_Landscape_Hide)];

    
    WS(ws)
    [self.contentView guestViewHiddenAnimation:^(BOOL isGuestViewHidden) {
        [btn setSelected:!btn.selected];
        ws.isLandscapeParticipantViewShown = !btn.selected;
        
        //bottomview layout
        ws.contentView.participantStatus = ws.isLandscapeParticipantViewShown ? LiveContentView_Participant_Landscape_Show : LiveContentView_Participant_Landscape_Hide;
        //videoView layout
        [ws.videoView setParticipantStatus:(ws.isLandscapeParticipantViewShown ? LiveContentView_Participant_Landscape_Show : LiveContentView_Participant_Landscape_Hide)];
    }];
}

//站内信消息点击事件
-(void)bottomMailClicked
{
    [self.contentView.bottomView changeMailIcon:NO];
    [VLSMessageManager sharedManager].newMail = NO;
    
    [self isShowMsgStationBoard:YES];

}
//切换相机
- (void)bottomChangeCamera
{
    [[VLSAgoraVideoService sharedInstance] switchCamera];
}

//分享点击
- (void)bottomShareClicked
{
    [self.contentView.shareAction showInView:self.view];
}

- (void)bottomRateClicked
{
    [self showRating];
}

#pragma mark -liveoff 代理
- (void)followAnchorClick
{
    // 网络请求关注主播
    [self showProgressHUD];
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        [self hideProgressHUDAfterDelay:0];
        ManagerEvent *event = [ManagerEvent toast:LocalizedString(@"ALREADY_CONCERONED") title:nil];
        [self error:event];
    };
    callback.errorBlock = ^(id result)
    {
        [self hideProgressHUDAfterDelay:0];
        [self error:result];
    };
    [VLSLiveHttpManager focusToUser:self.anchorId callback:callback];
}
- (void)backBtnClick
{
    
    [[VLSAgoraVideoService sharedInstance] leaveChannel];
    
    if (_isFromMail) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        if (self.selfIsAnchor) {
            if (self.isReconnection) {
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }else{
                self.presentingViewController.view.alpha = 0;
                [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            
        }else{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}
//礼物点击
- (void)bottomGiftClicked
{
  [self getGiftList];
  [self.contentView showGiftListView];
  [self getCurrentBlance];
  
}

- (void)getGiftList{
  WS(ws)
  ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
  callback.updateBlock = ^(NSDictionary* result)
  {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"GIFTVERSION"];
    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"version"] forKey:@"GIFTVERSION"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
      GiftModel * model = [[GiftModel alloc] init];
      model.saveGiftdataCompletionBlock = ^{
          [ws.contentView giftlistivewReload];
      };
      [model setGiftPlist:[result objectForKey:@"list"]];
      
    NSArray * deletedList = [result objectForKey:@"deletedList"];
    if (deletedList.count > 0) {
      [model deletedGiftImg:[result objectForKey:@"deletedList"]];
    }
      
  };
  callback.errorBlock = ^(id result)
  {
    NSLog(@"result -- %@",result);
  };
  
  [VLSLiveHttpManager getGiftList:callback];
  
}

- (void)clickShareBtnIndex:(NSInteger)index
{
    if ([ShareActionSheet isInstallation:index]) {
        
        NSString *coverImage = self.liveInfo.coverUrl;
        NSString *userName = self.anchorProfile.userName;
        NSString *content = [NSString stringWithFormat:@"%@%@%@", LocalizedString(@"SHARE_HTML_URL"), self.anchorProfile.userName, LocalizedString(@"SHARE_HTML_URL_OPEN_LIVE")];
        NSString *currentUserID = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
        NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                               coverImage,      @"coverImageURL",
                               userName,        @"userName",
                               self.roomId,              @"roomID",
                               content,           @"content",
                               currentUserID,   @"userID",
                               @YES,         @"overrideTitleInWXTimeline",
                                      nil];
        [self shareToPlatform:index messageObject:dict completion:^(NSDictionary *item) {
            [self shareTrackingLiveAfter:self.roomId Index:index];
        }];
    }else{
        [self showAlterviewController:LocalizedString(@"SHARE_HTML_NO_INSTALL_APP")];
    }
}

- (NSString *)getLookNumFromMulitVC
{
    if (self.tureWatch) {
        return self.tureWatch;
    }else{
        return nil;
    }
}

#pragma mark -- ShareActionSheetDelegate

- (void)xs_actionSheet:(ShareActionSheet *)actionSheet clickedButtonIndex:(NSInteger)index
{
    [actionSheet dismiss];
    if (index == 403) {
        [self showAlterviewController:LocalizedString(@"SHARE_HTML_NO_INSTALL_APP")];
        return;
    }
    NSString *coverImage = self.liveInfo.coverUrl;
    NSString *userName = self.anchorProfile.userName;
//    NSString *content = [NSString stringWithFormat:@"%@%@%@", LocalizedString(@"SHARE_HTML_URL"), self.anchorProfile.userName, LocalizedString(@"SHARE_HTML_URL_OPEN_LIVE")];
    NSString *currentUserID = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
    
    NSString *title = [NSString stringWithFormat:@"%@", LocalizedString(@"SHARE_HTML_URL")];
    NSString *content = [NSString stringWithFormat:@"%@%@", self.anchorProfile.userName, LocalizedString(@"SHARE_HTML_URL_OPEN_LIVE")];
    
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  coverImage,      @"coverImageURL",
                                  userName,        @"userName",
                                  self.roomId,              @"roomID",
                                  content,           @"content",
                                  currentUserID,   @"userID",
                                  @NO,            @"overrideTitleInWXTimeline",
                                  title, @"title",
                                  nil];
    [self shareToPlatform:index messageObject:dict completion:^(NSDictionary *item) {
        [self shareTrackingLiveProcess:self.roomId Index:index];
    }];
}

#pragma mark -closeRoomDelegate 关闭直播间代理

// 结束连线
- (void)cancleGuestAction
{
    /**
     *
     */
    UIView *view = [self.contentView.subviews objectAtIndex:0];
    if ([view isMemberOfClass:[CountDownView class]]) {
        CountDownView *countview = (CountDownView *)view;
        countview.delegate = nil;
        [countview removeFromSuperview];
    }
    //结束连线
    [self guestEndLink];
}
// 退出房间
- (void)quitRoomAction
{
    /**
     *  嘉宾倒计时退出时，崩溃，代理被销毁且不为空，所以退出时遍历代理设置为空
     */
    UIView *view = [self.contentView.subviews objectAtIndex:0];
    if ([view isMemberOfClass:[CountDownView class]]) {
        CountDownView *countview = (CountDownView *)view;
        countview.delegate = nil;
    }
    [self guestQuiteRoom];
}

#pragma mark - VLSLiveContentViewDelegate
- (void)sendGift:(GiftModel * _Nonnull)model
{
    model.userID = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
    model.name = [AccountManager sharedAccountManager].account.nickName;
    model.roomID = self.roomId;
    
    NSString * giftName;
    NSString * giftUnit;
    if ([NSLocalizedString(@"LANGUAGE_LOGO", nil) isEqualToString:@"香港"] || [NSLocalizedString(@"LANGUAGE_LOGO", nil) isEqualToString:@"台湾"]) {
        for (NSDictionary * dict in model.attrs) {
            if ([[dict objectForKey:@"i18n"] isEqualToString:@"tw"]) {
                giftName = [dict objectForKey:@"name"];
                giftUnit = [dict objectForKey:@"unit"];
            }
        }
    }else if ([NSLocalizedString(@"LANGUAGE_LOGO", nil) isEqualToString:@"ENGLISH"]){
        for (NSDictionary * dict in model.attrs) {
            if ([[dict objectForKey:@"i18n"] isEqualToString:@"en"]) {
                giftName = [dict objectForKey:@"name"];
                giftUnit = [dict objectForKey:@"unit"];
            }
        }
    }else{
        for (NSDictionary * dict in model.attrs) {
            if ([[dict objectForKey:@"i18n"] isEqualToString:@"cn"]) {
                giftName = [dict objectForKey:@"name"];
                giftUnit = [dict objectForKey:@"unit"];
            }
        }
    }
    model.giftName = [NSString stringWithFormat:@"%@%@%@",LocalizedString(@"SENDONE"),giftUnit,giftName];
    [[VLSGiftManager sharedManager] sendGiftMessage:model];
}

- (void)chargeVDiamond
{
    UIViewController* vc = [[UIStoryboard storyboardWithName: @"Payment" bundle: nil] instantiateInitialViewController];
    [self.navigationController pushViewController: vc animated: YES];
}

#pragma mark - priviate methods

#pragma mark - x
- (void)closeLiveRoomFrom:(BOOL)fromBtn
{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"text1"];
    [self isShowMsgStationBoard:NO];
    BOOL selfIsSuperUser = [AccountManager sharedAccountManager].account.isSuperUser;
    if (!fromBtn&&selfIsSuperUser) {
        [self anchorCloseLiveRoomWidthAlert];
        
    }else{
        if (self.selfIsAnchor) {
            [self anchorCloseLiveRoomWidthAlert];
        }else{
            //判断观众是否是嘉宾
            VLSAVMultiManager *manager = [VLSAVMultiManager sharedManager];
            if(manager.linkStatus == UserLinkStatusApplySuccess || manager.linkStatus == UserLinkStatusOnCamera){
                [self guestCloseLiveRoomWidthAlert];
            }else{
                //观众关闭直播
                if (manager.linkStatus == UserLinkStatusApplying) {
                    [self guestCancelLinkRequest];
                    [self audienceQuiteRoom];
                }else{
                    [self audienceQuiteRoom];
                }
            }
            [self.contentView.giftService stopAllOperation];
        }
    }
    [[VLSUserTrackingManager shareManager] trackingQuitroom:@"selfLeave" context: self.roomTrackContext];
  
}

- (void)closeLiveRoom{
    [self closeLiveRoomFrom:YES];
}

#pragma mark - customActions
- (void)anchorCloseLiveRoomWidthAlert
{
    //主播关闭直播
    [self showAnchorQuiteRoomAlertTapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if ((buttonIndex -2)==1) {
            //关闭主播间
            [self anchorQuiteRoom];
            [self receiveDeleteGroup];
            [self.contentView.giftService stopAllOperation];
          
        }
    }];
}

- (void)guestCloseLiveRoomWidthAlert
{
    closeRoomActionViewController *action = [[closeRoomActionViewController alloc]init];
    action.delegate = self;
    action.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    action.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:action animated:YES completion:nil];
    
}

#pragma mark 点击发赞，或者弹出嘉宾个人信息
- (void)priseTap:(UITouch *)tap
{
    
    CGPoint point = [tap locationInView:self.videoView];
    VLSVideoItem *item = [self chectPoint:point];
    if (item) {
        
        if (item.userProfile) {
            self.userID = item.userProfile.userId;
            
            [self showUserProfile:item.userProfile];
        }else{
            VLSUserProfile *userPro = [[VLSUserProfile alloc]init];
            userPro.userId = item.model.userId;
//            self.userID = item.userProfile.userId;
            self.userID = item.model.userId;
            
            [self showUserProfile:userPro];
        }
    }else  if([self checkInAnchorView:point]){
        
    }else{
        
        for (UIView *view in self.view.subviews) {
            if ([view isKindOfClass:[ApplyBGView class]]) {
                if (!view.hidden) {
                    view.hidden = YES;
                    return;
                }
            }
        }
        [self showRankingVC:NO];
        [self isShowMsgStationBoard:NO];
        [self priseViewTouch];
    }
    
}


#pragma mark - priviate method
//清屏
- (void)panInContentView:(UIPanGestureRecognizer *)panGestureReconginzer
{
    //避免拖拽嘉宾位时长按拖动手势和拖拽清屏手势冲突
    self.contentView.hidden = NO;
    if (!self.isLongPressGesture) {
        if (panGestureReconginzer.state == UIGestureRecognizerStateChanged) {
            CGFloat translation = [panGestureReconginzer translationInView:self.view].x;
            if (translation + currentTranslate > 0 && translation <=SCREEN_WIDTH) {
//                self.contentView.transform = CGAffineTransformMakeTranslation(translation +currentTranslate, 0);
                self.contentView.x = currentTranslate + translation;
            }
        }
        else if (panGestureReconginzer.state == UIGestureRecognizerStateEnded) {
//            currentTranslate = self.contentView.transform.tx;
            currentTranslate = self.contentView.x;
            if (contentShow) {
                if (fabs(currentTranslate)<50) {
                    currentTranslate = 0;
                    [self panAnimation];
                }else if(currentTranslate>50){
                    contentShow = NO;
                    currentTranslate = SCREEN_WIDTH;
                    [self panAnimation];
                }
            }else {
                if (fabs(currentTranslate)<SCREEN_WIDTH-50) {
                    contentShow = YES;
                    currentTranslate = 0;
                    
                    [self panAnimation];
                }else if(currentTranslate>SCREEN_WIDTH-50){
                    currentTranslate = SCREEN_WIDTH;
                    [self panAnimation];
                }
            }
        }
    }
}
- (void)panAnimation
{
    [UIView animateWithDuration:0.3 animations:^{
        if (contentShow) {
//            self.contentView.transform  = CGAffineTransformMakeTranslation(0, 0);
//            [self.roomIDview layoutIsRight:YES];
            self.contentView.x = 0;
        }else{
//            self.contentView.transform  = CGAffineTransformMakeTranslation(SCREEN_WIDTH, 0);
//            [self.roomIDview layoutIsRight:NO];
            self.contentView.x = SCREEN_WIDTH;
        }
    } completion:^(BOOL finished) {
        if (contentShow) {
          self.contentView.hidden = NO;
        }else{
          self.contentView.hidden = YES;
        }
    }];
}

#pragma mark - getters

- (VLSLiveContentView *)contentView
{
    if (_contentView == nil) {
//        _contentView = [[VLSLiveContentView alloc] initWithFrame:self.view.bounds actionStatus:!([self.orientation integerValue] == 2) ?: ConnectionHidden];
        CGRect rect = (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) ? CGRectMake(0, 0, SCREEN_HEIGHT, SCREEN_WIDTH) : self.view.bounds;
        _contentView = [[VLSLiveContentView alloc] initWithFrame:rect actionStatus:!(self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) ?: ConnectionHidden];
        _contentView.backgroundColor = [UIColor clearColor];
        _contentView.userInteractionEnabled = YES;
        _contentView.delegate = self;
        _contentView.bottomView.delegate = self;
        _contentView.msgBoard.delegate = self;
        _contentView.audiencesView.delegate = self;
        _contentView.priseView.delegagte = self;
        _contentView.shareAction.delegate = self;
        _contentView.anchor.delegate = self;
        _contentView.guestView.delegate = self;
        _contentView.ticket.delegate = self;
        _contentView.oriention = self.orientation;
    }
    return _contentView;
}

- (VLSVedioView *)videoView
{
    if (_videoView == nil) {
        CGRect rect = (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) ? CGRectMake(0, 0, SCREEN_HEIGHT, SCREEN_WIDTH) : self.view.bounds;
        _videoView =[[VLSVedioView alloc] initWithFrame:rect];
        _videoView.backgroundColor = [UIColor clearColor];
        _videoView.delegate = self;
        _videoView.orientation = self.orientation;
    }
    return _videoView;
}

- (coverImage *)videoCoverView
{
    if (_videoCoverView == nil) {
        CGRect rect = (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) ? CGRectMake(0, 0, SCREEN_HEIGHT, SCREEN_WIDTH) : self.view.bounds;
        _videoCoverView = [[coverImage alloc] initWithFrame:rect     WithURL:self.coverUrl];
    }
    return _videoCoverView;
}
- (VLSHeadAlertView *)headAlert
{
    if (_headAlert == nil) {
        _headAlert = [[VLSHeadAlertView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    }
    return _headAlert;
}
-(UIButton *)closeViewButton{
    
    if (_closeViewButton == nil) {
        _closeViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _closeViewButton.frame = CGRectMake(SCREEN_WIDTH - 42, 28, 30, 30);
        _closeViewButton.frame = CGRectMake(SCREEN_WIDTH - 42, 24, 30, 30);
        [_closeViewButton setImage:[UIImage imageNamed:@"host-close.png"] forState:UIControlStateNormal];
        [_closeViewButton addTarget:self action:@selector(closeLiveRoom) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeViewButton;
}


- (VLSRoomIDView *)roomIDview
{
    if (!_roomIDview) {
        _roomIDview = [[VLSRoomIDView alloc]init];
        self.roomIDview.RoomID = self.anchorId;
    }
    return _roomIDview;
}

//判断点击坐标是否在嘉宾view上
- (VLSVideoItem *)chectPoint:(CGPoint)checkPoint
{
    for (VLSVideoItem * view in self.videoView.subviews) {
        if (view.frame.size.width != self.view.frame.size.width) {
            if (CGRectContainsPoint(view.frame, checkPoint)) {
                if (![view isKindOfClass:[VLSVideoItem class]]) {
                    return nil;
                }
                return view;
            }
        }
    }
    return nil;
}


- (BOOL)checkInAnchorView:(CGPoint)checkPoint
{
    if (CGRectContainsPoint(self.contentView.anchor.frame, checkPoint)) {
        return YES;
    }
    return NO;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[GiftCountModel sharedGiftCountModel].GiftArr removeAllObjects];
}

// Override this selector to track user quiting room
- (void)reloadLiveWidthRoomId:(NSString *)roomId anchorId:(NSString *)anchorId
{
    if ([self.roomId length] > 0)
    {
        [[VLSUserTrackingManager shareManager] trackingQuitroom:@"jump" context: self.roomTrackContext];
    }
    [super reloadLiveWidthRoomId: roomId anchorId: anchorId];
}

- (void)sendUpdatedTickets
{
    if (!self.isBlockingUpdatingTickets)
    {
        self.isBlockingUpdatingTickets = YES;
        __weak __typeof(self) weakSelf = self;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            ManagerCallBack *callback = [[ManagerCallBack alloc] init];
            callback.updateBlock = ^(id result){
                if (result) {
                    
                    VLSLiveInfoViewModel* liveInfo = result;
                    [[VLSMessageManager sharedManager] sendUpdateVTickets: liveInfo.totalGainTicket
                                                                     succ:^{
                                                                         weakSelf.isBlockingUpdatingTickets = NO;
                                                                         
                                                                     } fail:^(int code, NSString *msg) {
                                                                         weakSelf.isBlockingUpdatingTickets = NO;
                                                                     }];
                }
            };
            callback.errorBlock = ^(id result){
                weakSelf.isBlockingUpdatingTickets = NO;
            };
            [VLSLiveHttpManager getLiveInfo: self.roomId callback:callback];
        });
    }
}

- (void)willEnterIntoBackground: (NSNotification*)notification
{
    [[VLSUserTrackingManager shareManager] trackingRoomInterruption: @"enterbackground" context: self.roomTrackContext];
}


- (void)willTerminate: (NSNotification*)notification
{
    [[VLSUserTrackingManager shareManager] trackingRoomInterruption: @"terminate" context: self.roomTrackContext];
}

//账号被踢出通知
- (void)loginouted:(NSNotification*)notification
{
    if (self.selfIsAnchor) {
        //关闭主播间
        [self anchorQuiteRoom];
        [self.contentView.giftService stopAllOperation];
    }else{
        //判断观众是否是嘉宾
        VLSAVMultiManager *manager = [VLSAVMultiManager sharedManager];
        if(manager.linkStatus == UserLinkStatusApplySuccess || manager.linkStatus == UserLinkStatusOnCamera){
            [self quitRoomAction];
        }else{
            //观众关闭直播
            if (manager.linkStatus == UserLinkStatusApplying) {
                [self guestCancelLinkRequest];
                [self audienceQuiteRoom];
            }else{
                [self audienceQuiteRoom];
            }
        }
        [self.contentView.giftService stopAllOperation];
    }
    [[VLSUserTrackingManager shareManager] trackingQuitroom:@"selfLeave" context: self.roomTrackContext];
}

- (BOOL)stopWatchingWithCompleteBlock: (void (^)(BOOL)) completeBlock
{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"text1"];
    [self isShowMsgStationBoard:NO];
    BOOL selfIsSuperUser = [AccountManager sharedAccountManager].account.isSuperUser;
    if (selfIsSuperUser)
    {
        return NO;
    }else{
        if (self.selfIsAnchor)
        {
            return NO;
        }else
        {
            //判断观众是否是嘉宾
            VLSAVMultiManager *manager = [VLSAVMultiManager sharedManager];
            if(manager.linkStatus == UserLinkStatusApplySuccess || manager.linkStatus == UserLinkStatusOnCamera){
                [self guestCloseLiveRoomWidthAlert];
                return NO;
            }else{
                //观众关闭直播
                if (manager.linkStatus == UserLinkStatusApplying) {
                    [self guestCancelLinkRequest];
                    [self audienceQuiteRoom: NO completeBlock: completeBlock];
                }else{
                    [self audienceQuiteRoom: NO completeBlock: completeBlock];
                }
            }
            [self.contentView.giftService stopAllOperation];
        }
    }
    [[VLSUserTrackingManager shareManager] trackingQuitroom:@"selfLeave" context: self.roomTrackContext];
    return YES;
}
@end
