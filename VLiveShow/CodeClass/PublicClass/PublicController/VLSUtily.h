//
//  VLSUtily.h
//  VLiveShow
//
//  Created by tom.zhu on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SINGLETON.h"

typedef enum : NSUInteger {
    ActionTypeLive,         //直播
    ActionTypeAnchor,       //主播
    ActionTypeGuest,        //嘉宾
    ActionTypeAdmin,        //管理员
    
    ActionTypeLessonTeacher,        //课程讲师
    ActionTypeLessonAttendance,     //课程预约连线嘉宾
    ActionTypeLessonAudience,       //课程预约观众
    ActionTypeLessonVIP ,
    
    ActionTypeLessonLandscape,        //课程横屏模式
    ActionTypeNormalLandscape,        //PC横屏模式
    ActionTypeConnectedLandscape_Attendance,                 //横屏嘉宾连线模式
    ActionTypeConnectedLandscape_Audience,       //横屏观众连线模式
} ActionType;

typedef NS_ENUM(NSInteger, ParticipantStatus) {
    LiveContentView_Participant_Landscape_Show = 1,      //横屏，显示连线嘉宾
    LiveContentView_Participant_Landscape_Hide,    //横屏，不显示连线嘉宾
    LiveContentView_Participant_Landscape_Normal,   //横屏，没有连线嘉宾
};

typedef enum : NSInteger {
    KLiveRoomOrientation_Unknown,
    KLiveRoomOrientation_Portrait,
    KLiveRoomOrientation_LandscapeLeft = 3,
    KLiveRoomOrientation_LandscapeRight = 4,
}KLiveRoomOrientation;        //直播间屏幕状态

#define ANIMATION_DURATION 0.3f

//NOTIFICATIONCENTER
static NSString *ENABLEENTERLIVEROOM = @"ENABLEENTERLIVEROOM";

@interface VLSUtily : NSObject
//SINGLETONH(VLSUtily)
+ (NSDateComponents *)getShanghaiNowDateFromat:(NSDate *)AnDate;
//MARK: TOAST
+ (void)toastWithText:(NSString*)str addtoview:(UIView*)view animation:(BOOL)animation duration:(float)duration;
+ (void)toastWithText:(NSString*)str addtoview:(UIView*)view animation:(BOOL)animation duration:(NSInteger)duration center:(BOOL)center;
+ (void)networkWithProgressTotal:(int64_t)expected written:(int64_t)witten addtoview:(UIView*)view;
+ (NSString*) getTimeByTodayWithString:(NSString*)dateString;
//去除小数点后position位，不四舍五入
+ (NSString *)stringByNotRounding:(double)price afterPoint:(int)position;
//用户是否购买课程
+ (void)isSubscription:(NSString*)courseId handle:(void(^)(id result, ManagerEvent *error))handle;
//十位转二进制 获取二级制真位
+ (NSArray *)toBinarySystemWithDecimalSystem:(int)decimal;
//获取两个数值范围内的随机数
+ (int)randomFrom:(int)from to:(int)to;
//根据文字计算宽度
+ (CGFloat)widthOfString:(NSString *)string font:(UIFont *)font height:(CGFloat)height;
//获取系统语言 (zh-hans为简体中文,zh-hant为繁体中文)
+ (NSString*)currentLanguageString;
@end
