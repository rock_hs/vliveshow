//
//  LeaveOffViewController.m
//  VLiveShow
//
//  Created by sp on 16/8/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "LeaveOffViewController.h"
#import "NSString+UserFile.h"
#import "VLiveShow-Swift.h"
#define SEPARATELINE (SC_WIDTH - 300)/5
#define CELL_WIDTH 50
#define CELL_HEIGHT 70
@interface LeaveOffViewController ()

@end

@implementation LeaveOffViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.lessonId.integerValue != 0 && !self.isAnchor)
    {
        CourseRateViewController* vc = [[UIStoryboard storyboardWithName: @"CourseRate" bundle: nil] instantiateInitialViewController];
        vc.lessonId = self.lessonId;
        vc.anchorProfile = self.anchorProfile;
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        if (self.backView.topImageView.image)
        {
            vc.coverImage = self.backView.topImageView.image;
        }
        else if ( self.backView.topImageView.sd_imageURL)
        {
            vc.coverImageURL = self.backView.topImageView.sd_imageURL;
        }
        [self presentViewController: vc animated: NO completion: nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
     [UIView setAnimationsEnabled:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadNameLayout];
    
    self.navigationController.navigationBar.hidden = YES;
//    [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    [self.guestCollectionView registerClass:[GuestViewCell class] forCellWithReuseIdentifier:@"guestcell"];

    self.view.backgroundColor = [UIColor blackColor];
    [self loadAllView];
    [self loadDate];
    // Do any additional setup after loading the view.
    [self configOriention];
}

- (void)configOriention {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait) {
        return;
    }
    
    if (SYSTEM_VERSION_GREATER_THAN(@"10.0")) {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];
    }else {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    }
    
    UIViewController *v = [UIViewController new];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:v animated:NO completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [v dismissViewControllerAnimated:NO completion:^{
                    //这么做是因为在push动画过程中present控制有几率导致dismiss无法完成
                    [v.view removeFromSuperview];
                    [v removeFromParentViewController];
                }];
            });
        }];
    });
    
}

- (void)loadAllView
{
    [self.view addSubview:self.backView];
    [self.view addSubview:self.contentView];
    [self.view addSubview:self.backButton];
    [self.view addSubview:self.headImageView];
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.shareView];
    
    self.backView.sd_layout
    .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));

}
- (void)loadDate
{
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        self.guestArray = [VLSGuestModel mj_objectArrayWithKeyValuesArray:[result objectForKey:@"guests"]];
        
        NSInteger count = self.guestArray.count;
        _guestCollectionView.frame = CGRectMake(MAX(0, (SC_WIDTH - count * (CELL_WIDTH + SEPARATELINE) + SEPARATELINE) / 2), SC_HEIGHT/667*110-CELL_HEIGHT, MIN(SC_WIDTH, count * (CELL_WIDTH + SEPARATELINE) - SEPARATELINE), CELL_HEIGHT);
        
        NSInteger  watchN = [[result objectForKey:@"tscNum"] integerValue];
        NSInteger gainT = [[result objectForKey:@"gainTicket"] integerValue];
        self.contentView.vTicketNum.text = [NSString thousandpointsNum:gainT];
        self.contentView.watchNum.text = [NSString thousandpointsNum:watchN];
        
        
        if (self.guestArray.count >0)
        {
            [self.view addSubview:self.guestView];
            self.shareView.frame = CGRectMake(0, self.contentView.frame.origin.y +SC_HEIGHT/667*245, SC_WIDTH, 60);
        }
        [self.guestCollectionView reloadData];
    };
    callback.errorBlock = ^(id result){
        
        
     };
    if (self.roomId!= nil) {
        [VLSLiveHttpManager getEndPageInfo:self.roomId callback:callback];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-uicollectionDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.guestArray.count;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GuestViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"guestcell" forIndexPath:indexPath];
    VLSGuestModel *model = [self.guestArray objectAtIndex:indexPath.row];//
    
    cell.guestModel = model;
    return cell;
}
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    
//    if (self.guestArray.count <= 1) {
//        return UIEdgeInsetsMake(0, SC_WIDTH/2-25, 0, SC_WIDTH/2-25);
//    }else if (self.guestArray.count == 2)
//    {
//     return UIEdgeInsetsMake(0, (SC_WIDTH -100 - SEPARATELINE)/2, 0, (SC_WIDTH -100 - SEPARATELINE)/2);
//    }else if (self.guestArray.count == 3)
//    {
//        return UIEdgeInsetsMake(0, (SC_WIDTH -150 - SEPARATELINE*2)/2, 0, (SC_WIDTH -150 - SEPARATELINE*2)/2);
//    }else if (self.guestArray.count == 4)
//    {
//        return UIEdgeInsetsMake(0, (SC_WIDTH -200 - SEPARATELINE*3)/2, 0, (SC_WIDTH -200 - SEPARATELINE*3)/2);
//    }else if (self.guestArray.count == 5)
//    {
//        return UIEdgeInsetsMake(0, (SC_WIDTH -250 - SEPARATELINE*4)/2, 0, (SC_WIDTH -250 - SEPARATELINE*4)/2);
//    }else
//    {
//        return UIEdgeInsetsMake(0, SEPARATELINE, 0, SEPARATELINE);
//    }
//
//}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    for (int i =  0; i < self.guestArray.count; i++) {
        VLSGuestModel *model = [self.guestArray objectAtIndex:indexPath.row];
        vlsVC.userId =model.userId;

    }
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vlsVC animated:YES];

}
#pragma mark-get
- (UIView*)guestView
{
    if (_guestView == nil) {
        _guestView = [[UIView alloc] initWithFrame:CGRectMake(0, self.contentView.frame.origin.y +SC_HEIGHT/667*120, SC_WIDTH, SC_HEIGHT/667*110)];
        _guestView.backgroundColor = [UIColor clearColor];
        UILabel *namelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, 25)];
        namelabel.textAlignment = NSTextAlignmentCenter;
        namelabel.font = [UIFont boldSystemFontOfSize:14];
        namelabel.textColor = [UIColor colorFromHexRGB:@"C3A851"];
        namelabel.text = LocalizedString(@"LIVE_INTOPEOPLE");
        [_guestView addSubview:namelabel];
        [_guestView addSubview:self.guestCollectionView];
    }
    return _guestView;
}
- (UICollectionView*)guestCollectionView
{
    if (_guestCollectionView == nil) {
             //2.初始化collectionView
        _guestCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,SC_HEIGHT/667*110-70, SC_WIDTH, 70) collectionViewLayout:self.layout];
        _guestCollectionView.delegate = self;
        _guestCollectionView.dataSource = self;
        _guestCollectionView.backgroundColor = [UIColor clearColor];
        _guestCollectionView.showsHorizontalScrollIndicator = NO;
    }
    return _guestCollectionView;
}
- (UIButton*)backButton
{
    if (_backButton == nil) {
        _backButton = [UIButton buttonWithType:0];
        _backButton.frame = CGRectMake(25, SC_HEIGHT - 30-44, SC_WIDTH - 50, 40);
        _backButton.layer.cornerRadius = 20;
        _backButton.backgroundColor = [UIColor colorFromHexRGB:@"FF1130"];
        [_backButton setTitle:LocalizedString(@"BACK_HOME") forState:UIControlStateNormal];
        _backButton.titleLabel.font = [UIFont systemFontOfSize:16];
        _backButton.titleLabel.textColor = [UIColor whiteColor];
        [_backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _backButton;
}
- (UIImageView*)headImageView
{
    if (_headImageView == nil) {
        _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SC_WIDTH/2 -50, SC_HEIGHT/650*212-70, 100, 100)];
        _headImageView.layer.borderWidth = 6;
        _headImageView.layer.borderColor = [UIColor colorWithWhite:0.3 alpha:0.5].CGColor;
        _headImageView.layer.cornerRadius = 50;
        _headImageView.clipsToBounds = YES;
        
        _headImageView.userInteractionEnabled = YES;
        UIButton * headBut = [UIButton buttonWithType:1];
        headBut.frame = CGRectMake(0, 0, 100, 100);
        [_headImageView addSubview:headBut];
        [headBut addTarget:self action:@selector(headButAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _headImageView;
}
- (void)headButAction:(UIButton*)sender
{
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    vlsVC.userId = self.userID;
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vlsVC animated:YES];

}
- (LeaveOffContentView*)contentView
{
    if (_contentView == nil) {
        _contentView = [[LeaveOffContentView alloc] initWithFrame:CGRectMake(0, self.headImageView.frame.origin.y +100+5, SC_WIDTH, SC_HEIGHT/667*110)];
    }
    return _contentView;
}
- (ShareView*)shareView
{
    if (_shareView == nil) {
        _shareView = [[ShareView alloc] initWithFrame:CGRectMake(0, self.contentView.frame.origin.y +SC_HEIGHT/667*120, SC_WIDTH, SC_HEIGHT/667*60)];
    }
    return _shareView;
}
- (UILabel*)titleLabel
{
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, SC_HEIGHT/650*66, SC_WIDTH, 30)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:28];
        _titleLabel.text = LocalizedString(@"LIVE_OVER");
    }
    return _titleLabel;
}
- (UICollectionViewFlowLayout*)layout
{
    if (_layout == nil) {
        _layout = [[UICollectionViewFlowLayout alloc] init];
        
        //该方法也可以设置itemSize
//        _layout.itemSize = CGSizeMake(50, 70);
        _layout.itemSize = CGSizeMake(CELL_WIDTH, CELL_HEIGHT);
        //设置边距
//        _layout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 20);
        [_layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        _layout.minimumLineSpacing = SEPARATELINE;

    }
    
    return _layout;
}
- (BackDimView*)backView
{
    if (_backView == nil) {
        _backView = [[BackDimView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, SC_HEIGHT)];
    }
    return _backView;
}

#pragma mark-Aciton
- (void)backAction:(UIButton*)sender
{
    if ([_delegate respondsToSelector:@selector(backBtnClick)]) {
        [_delegate backBtnClick];
    }

}
- (NSString*)getModleStrWith:(NSString*)number
{
    NSMutableString * str = [[NSMutableString alloc] initWithString:number];
    if (str.length < 4) {
        return str;
    }else
    {
        for (int i = 3; i < number.length; i++)
        {
            
            if (i%3 == 0)
            {
                
                [str insertString:@"," atIndex:i+ i/3-1];
            }
        }
        return str;
    }

 
}

- (void)reloadNameLayout
{
    if (self.contentView.markLabel.text !=nil) {
        CGSize marksize = [self.contentView.markLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:12],NSFontAttributeName, nil]];
        CGSize contentsize = [self.contentView.contentLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:16],NSFontAttributeName, nil]];
        CGFloat contentlength = contentsize.width;
        CGFloat marklength = marksize.width+10;
        if (contentlength +marklength+10 >=SC_WIDTH) {
            self.contentView.markLabel.frame = CGRectMake(0, 37.5, marklength, 20);
            self.contentView.contentLabel.frame = CGRectMake(marklength+10, 35, SC_WIDTH-marklength-10, 25);
        }else
        {
        
            self.contentView.markLabel.frame = CGRectMake((SC_WIDTH-marklength-contentlength-10)/2, 37.5, marklength, 20);
            self.contentView.contentLabel.frame = CGRectMake((SC_WIDTH-marklength-contentlength-10)/2+marklength+10, 35, contentlength, 25);

        }

    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
