//
//  WithdrawalVC.m
//  Withdrawal
//
//  Created by tom.zhu on 16/8/10.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import "WithdrawalVC.h"
#import "WithdrawalDetailViewController.h"
#import "WithdrawConfirmVC.h"
#import "HTTPFacade+withdraw.h"
#import "HTTPFacade+get.h"
#import "WithdrawModel.h"   
#import "VLSUtily.h"
#import "BindingCardViewController.h"
#if DEBUG
#import <FLEX/FLEX.h>
#endif

static NSString *cellIdentity_withdrawal = @"cellIdentity_withdrawal";
static CGFloat K_CELL_HEIGHT             = 140;
static CGFloat K_CELL_SEPARATORINSET     = 18;
static CGFloat K_NUMLABEL_FONT           = 30, K_DETAILLABEL_FONT = 14;
static CGFloat K_NUMLABEL_TOPMARGIN      = 34, K_DETAILLABEL_BOTTOMMARGIN = 34;

@interface WithdrawalTableviewCell : UITableViewCell
@property (nonatomic ,strong) UILabel *numLabel;//数字显示
@property (nonatomic ,strong) UILabel *detailLabel;//数字含义显示
/**设置内容，修改颜色*/
- (void)setdata:(WithdrawModel*)data indexPath:(NSIndexPath*)indexPath;
@end

@implementation WithdrawalTableviewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle        = UITableViewCellSelectionStyleNone;
        self.separatorInset = UIEdgeInsetsMake(0, K_CELL_SEPARATORINSET, 0, K_CELL_SEPARATORINSET);
        self.numLabel              = UILabel.new;
        self.detailLabel           = UILabel.new;
        self.detailLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        self.numLabel.font         = [UIFont systemFontOfSize:K_NUMLABEL_FONT];
        self.detailLabel.font      = [UIFont systemFontOfSize:K_DETAILLABEL_FONT];
        [self.contentView addSubview:self.numLabel];
        [self.contentView addSubview:self.detailLabel];
        self.numLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addConstraints:@[
                                           [NSLayoutConstraint constraintWithItem:self.numLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0],
                                           [NSLayoutConstraint constraintWithItem:self.numLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:K_NUMLABEL_TOPMARGIN],
                                           ]];
        [self.contentView addConstraints:@[
                                           [NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0],
                                           [NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeBaseline relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-K_DETAILLABEL_BOTTOMMARGIN]
                                           ]];
    }
    return self;
}

- (void)setdata:(WithdrawModel*)data indexPath:(NSIndexPath*)indexPath {
    self.numLabel.textColor = indexPath.row ? RGB16(COLOR_FONT_FF1130) : RGB16(COLOR_FONT_4A4A4A);
    self.numLabel.text         = [data.money stringValue];
    self.detailLabel.text      = data.name;
    [self.numLabel sizeToFit];
    [self.detailLabel sizeToFit];
    [self.contentView layoutSubviews];
    [self.contentView layoutIfNeeded];
}

@end

static CGFloat K_FOOTER_WIDTH = 300, K_FOOTER_HEIGHT = 40;
static CGFloat K_DRAWBUTTON_BOTTOMMARGIN = 157, K_DETAILLABEL_BOTTOMMARGIN_FOOTER = 117;
static CGFloat K_DRAWBUTTON_FONT = 16, K_DETAILLABEL_FONT_FOOTER = 14;

@interface WithdrawalTableviewFooter : UIView
@property (nonatomic, copy  ) void (^drawButtonBlock)();
@property (nonatomic, strong) UIButton        *drawButton;//全部提取按钮
@property (nonatomic, strong) UILabel         *detailLabel;//提示语显示
- (void)checkStatus:(BOOL)enable;
@end

@implementation WithdrawalTableviewFooter

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = RGB16(0xF8F8F8);
        self.drawButton = UIButton.new;
        self.detailLabel = UILabel.new;
        [self.drawButton.titleLabel setFont:[UIFont systemFontOfSize:K_DRAWBUTTON_FONT]];
        [self.detailLabel setFont:[UIFont systemFontOfSize:K_DETAILLABEL_FONT_FOOTER]];
        self.drawButton.layer.borderWidth = 1;
        self.drawButton.layer.borderColor = RGB16(COLOR_FONT_9B9B9B).CGColor;
        self.drawButton.layer.cornerRadius = K_FOOTER_HEIGHT / 2;
        self.detailLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        //TODO:国际化
        [self.drawButton setTitle:LocalizedString(@"bind_card") forState:UIControlStateNormal];
        [self.detailLabel setText:[[LocalizedString(@"pay_time") stringByAppendingString:@"，"] stringByAppendingString:LocalizedString(@"bind_cardfirst")]];
        [self.drawButton addTarget:self action:@selector(drawButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.drawButton setTitleColor:RGB16(COLOR_FONT_9B9B9B) forState:UIControlStateNormal];
        [self.drawButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self addSubview:self.drawButton];
        [self addSubview:self.detailLabel];
        [self checkStatus:YES];
        
        self.drawButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:@[
                               [NSLayoutConstraint constraintWithItem:self.drawButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:0 constant:K_FOOTER_WIDTH],
                               [NSLayoutConstraint constraintWithItem:self.drawButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:0 constant:K_FOOTER_HEIGHT],
                               [NSLayoutConstraint constraintWithItem:self.drawButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:-K_DRAWBUTTON_BOTTOMMARGIN],
                               [NSLayoutConstraint constraintWithItem:self.drawButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0],
                               ]];
        [self addConstraints:@[
                               [NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0],
                               [NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:-K_DETAILLABEL_BOTTOMMARGIN_FOOTER],
                               ]];
    }
    return self;
}

- (void)checkStatus:(BOOL)enable {
    self.drawButton.selected = enable;
    self.drawButton.layer.borderColor = enable ? [UIColor clearColor].CGColor : RGB16(COLOR_FONT_9B9B9B).CGColor;
    self.drawButton.backgroundColor = enable ? RGB16(COLOR_FONT_FF1130) : [UIColor clearColor];
}

- (void)drawButtonClicked:(UIButton*)btn {
    if (!btn.selected) {
        return;
    }
    btn.layer.borderColor = btn.selected ? [UIColor clearColor].CGColor : RGB16(COLOR_FONT_9B9B9B).CGColor;
    btn.backgroundColor = btn.selected ? RGB16(COLOR_FONT_FF1130) : [UIColor clearColor];
    if (self.drawButtonBlock) {
        self.drawButtonBlock();
    }
}

@end

@interface WithdrawalVC () <UITableViewDataSource, UITableViewDelegate, WithdrawConfirmDelegate>
@property (nonatomic, strong) MoneybankModel *bankModel;
@property (nonatomic, strong) UITableView    *tableview;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation WithdrawalVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:self.navigationController.viewControllers.count == 1 animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //TODO:国际化
    WithdrawModel *model = [[WithdrawModel alloc] initWithDic:@{
                                                                @"money" : @0,
                                                                @"name"   : LocalizedString(@"ticket_remain"),
                                                                }];
    WithdrawModel *model2 = [[WithdrawModel alloc] initWithDic:@{
                                                                @"money" : @0,
                                                                @"name"   : LocalizedString(@"available_cash"),
                                                                }];
    self.dataArray = [@[
                       model, model2
                       ] mutableCopy];
    [self naviconfig];
    [self uiconfig];
    [self requestData];
    [self.tableview reloadData];
}

- (void)naviconfig {
    //TODK:国际化
    self.title = LocalizedString(@"mine_ticket");
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"detail") style:UIBarButtonItemStylePlain target:self action:@selector(detailClicked)];
}

- (void)uiconfig {
    self.edgesForExtendedLayout       = UIRectEdgeNone;
    self.tableview                    = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStylePlain];
    [self.view addSubview:self.tableview];
    WithdrawalTableviewFooter *footer = [[WithdrawalTableviewFooter alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 2 * K_CELL_HEIGHT)];
//    self.tableview.tableFooterView    = footer;
    self.tableview.tableFooterView = UIView.new;
    self.tableview.bounces            = NO;
    [self.tableview registerClass:[WithdrawalTableviewCell class] forCellReuseIdentifier:cellIdentity_withdrawal];
    self.tableview.dataSource         = self;
    self.tableview.delegate           = self;
    footer.drawButtonBlock            = ^{
//        WithdrawConfirmVC *vc = [[WithdrawConfirmVC alloc] initWithNibName:@"WithdrawConfirmVC" bundle:nil];
//        WithdrawModel *model = (WithdrawModel*)[self.dataArray lastObject];
////        vc.maxAvailable = [NSString stringWithFormat:@"%.1f", [model.money doubleValue]];
//        
//        NSString *temStr = [model.money stringValue];
//        NSRange range = [temStr rangeOfString:@"."];
//        if (range.location != NSNotFound) {
//         temStr = [temStr substringWithRange:NSMakeRange(0, range.location + 2)];   
//        }
//        
//        vc.maxAvailable = temStr;
//        vc.delegate_ = self;
//        [self.navigationController pushViewController:vc animated:YES];
        
        BindingCardViewController *vc = [BindingCardViewController new];
        vc.accountName = _bankModel.account;
        vc.accountNumber = _bankModel.accountNumber;
        vc.model = _bankModel;
        [self.navigationController pushViewController:vc animated:YES];
    };
#if DEBUG
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(100, 300, 50, 50);
    btn.backgroundColor = [UIColor redColor];
    [self.view addSubview:btn];
    [btn addTarget:self action:@selector(flexshow) forControlEvents:UIControlEventTouchUpInside];
#endif
}

#if DEBUG
- (void)flexshow {
    [[FLEXManager sharedManager] showExplorer];
}
#endif

- (void)detailClicked {
    WithdrawalDetailViewController *vc = [[WithdrawalDetailViewController alloc] initWithNibName:@"WithdrawalDetailViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)requestData {
    __weak typeof(self) ws = self;
    BOOL __block res_ticket = NO, res_Money = NO;
    WithdrawalTableviewFooter *footer = (WithdrawalTableviewFooter*)self.tableview.tableFooterView;
    void (^updateStatusBlock)() = ^{
        if (res_ticket && res_Money) {
            //日期不能在每月1至10号
            NSDateComponents *cmp = [VLSUtily getShanghaiNowDateFromat:[NSDate date]];
            NSInteger day = cmp.day;
            if (day > 1 && day < 10) {
                [footer checkStatus:NO];
            }else {
                [footer checkStatus:YES];
            }
        }else {
            [footer checkStatus:NO];
        }
    };
    //V票余额
    [HTTPFacade cashoutReamin:^(id object) {
//        diamond（实时的V钻），earning （实时的V票），money （可提现金额），rate （V票转现金比率）， usableBalance.earning (可用V票)，usableBalance.diamond (可用V钻)，usableBalance.frozenEarning (冻结V票), usableBalance.totalEarning （累计V票）
        WithdrawModel *model = [ws.dataArray firstObject];
        NSNumber *money = [object objectForKey:@"earning"];
        //V票余额只能是整数
        money = @([money intValue]);
        [model setMoney:money];
        res_Money = YES;
        
        //V票可提现金额
        WithdrawModel *model2 = [ws.dataArray lastObject];
        
        //保留一位小数
        NSString *temStr = object[@"money"];
        NSRange range = [temStr rangeOfString:@"."];
        if (range.location != NSNotFound) {
            temStr = [temStr substringWithRange:NSMakeRange(0, range.location + 2)];
        }
        
        [model2 setMoney:@([temStr doubleValue])];
//        [ws.tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        res_ticket = YES;
        [ws.tableview reloadData];

//        updateStatusBlock();
    } errBlock:^(NSError *error) {
        
    }];
    
    //绑定银行卡信息
    NSString *path = @"/money/bank";
//    [HTTPFacade getPath:path param:@{} successBlock:^(id  _Nonnull object) {
////        MoneybankModel *model = [[MoneybankModel alloc] initWithDic:[object objectForKey:@"bank"]];
////        _bankModel = model;
////        reloadData();
//    } errorBlcok:^(ManagerEvent * _Nonnull error) {
//        [VLSUtily toastWithText:error.title addtoview:self.view animation:YES duration:1];
//    }];
    

    [HTTPFacade getPath:path param:@{} successBlock:^(id  _Nonnull object) {
        MoneybankModel *model = [[MoneybankModel alloc] initWithDic:[object objectForKey:@"bank"]];
        ws.bankModel = model;
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        
    }];
}

#pragma mark - WithdrawConfirmDelegate
- (void)withdrawConfirmDidSuccess:(NSString*)maxAvailable {
    WithdrawModel *model = [self.dataArray lastObject];
    model.money = @([maxAvailable longLongValue]);
    [self.tableview reloadData];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return K_CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WithdrawalTableviewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentity_withdrawal];
    [cell setdata:self.dataArray[indexPath.row] indexPath:indexPath];
    if (indexPath.row == 1) {
        cell.separatorInset = UIEdgeInsetsMake(0, SCREEN_WIDTH, 0, 0);
    }
    return cell;
}

@end
