//
//  UIView+gradient.m
//  VLiveShow
//
//  Created by tom.zhu on 16/9/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "UIView+gradient.h"
#import <objc/runtime.h>
static void *GRADIENTLAYER = @"GRADIENTLAYER";

NSString * const GRADIENT_AXIS_HORIZON = @"GRADIENT_AXIS_HORIZON";
NSString * const GRADIENT_AXIS_VERTICAL = @"GRADIENT_AXIS_VERTICAL";

@implementation UIView (gradient)
- (void)viewGradientWithStartPosition:(double)startPosition
                           endPositon:(double)endPosition
                             position:(NSString*)position
                      backgroundColor:(UIColor*)backgroundColor
                            maskColor:(UIColor*)maskColor
                             duration:(float)time
                             progress:(float)progress {
    CAGradientLayer *_gradientLayer = nil;
    _gradientLayer = objc_getAssociatedObject(self, GRADIENTLAYER);
    if (nil == _gradientLayer) {
        _gradientLayer = [CAGradientLayer layer];
        _gradientLayer.frame = self.bounds;
        [self.layer addSublayer:_gradientLayer];
        
        CGPoint point = CGPointMake(0, 1);
        if ([position isEqualToString:@"GRADIENT_AXIS_HORIZON"]) point = CGPointMake(1, 0);
        else point = CGPointMake(0, 1);
        _gradientLayer.startPoint = CGPointZero;
        _gradientLayer.endPoint = point;
        
        _gradientLayer.colors = @[(__bridge id)backgroundColor.CGColor,
                                  (__bridge id)maskColor.CGColor];
        
        _gradientLayer.locations = @[@(startPosition), @(endPosition ?: 1)];
        
        objc_setAssociatedObject(self, GRADIENTLAYER, _gradientLayer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    NSMutableArray *ary = [_gradientLayer.locations mutableCopy];
    NSNumber *firLoc = [ary firstObject];
    float firFlo = [firLoc floatValue];
    firFlo = progress;
    firLoc = @(firFlo);
    [ary replaceObjectAtIndex:0 withObject:firLoc];
    _gradientLayer.locations = ary;
    NSLog(@"progress ==== %f", progress);
}
@end
