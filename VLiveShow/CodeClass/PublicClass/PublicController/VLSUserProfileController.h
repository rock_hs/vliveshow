//
//  VLSUserProfileController.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "VLSUserProfile.h"
#import "VLSUserActionView.h"
#import "VLSBlackModel.h"
#import "VLSMuteModel.h"
#import "VLSUserTrackingManager.h"
@protocol VLSUserProfileControllerDelelegate<NSObject>

@optional
- (void)fromUserProfileAnchorClose;
- (void)fromUserProfileGuestCancelLink;
- (void)touchMain;
- (void)getBlack;
- (void)followClick:(BOOL)isAttent;
@end

@interface VLSUserProfileController : VLSBaseViewController
@property (nonatomic, assign) id <VLSUserProfileControllerDelelegate>delegate;
@property (nonatomic, strong) VLSUserInfoViewModel *model;
@property (nonatomic, strong) VLSUserProfile *userProfile;
@property (nonatomic, assign) ActionType actionType;
@property (nonatomic, strong) NSString *roomId;
@end
