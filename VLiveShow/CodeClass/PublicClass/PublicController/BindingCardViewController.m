//
//  BindingCardViewController.m
//  VLiveShow
//
//  Created by tom.zhu on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "BindingCardViewController.h"
#import "HTTPFacade+get.h"
#import "VLSUtily.h"    

static float LABEL_WIDTH = 78;
static float SEPARAOR_LEFTMARGIN = 18;
static float CELL_HEIGHT = 55;
static float BUTTON_TOPMARGIN = 50;
static float BUTTON_WIDTH = 300;
static NSString *KEYPATH = @"UITextFieldTextDidChangeNotification";

@interface BindingCardViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UITextField *tf_name;
@property (nonatomic, strong) UITextField *tf_account;
@property (nonatomic, strong) UIButton *confirmButton;
@end

@implementation BindingCardViewController
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = LocalizedString(@"binding_bankcard");
    self.view.backgroundColor = RGB16(COLOR_BG_F8F8F8);
    _tableView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
    _tableView.backgroundColor = RGB16(COLOR_BG_F8F8F8);
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.scrollEnabled = NO;
    [self.view addSubview:_tableView];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self checkStatus];
    });
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkStatus) name:KEYPATH object:nil];
}

- (void)checkStatus {
    BOOL res = NO;
    if (_tf_name.text.length && _tf_account.text.length) {
        _confirmButton.selected = YES;
        res = YES;
    }else {
        _confirmButton.selected = NO;
        res = NO;
    }
    _confirmButton.selected = res;
    _confirmButton.layer.borderColor = res ? [UIColor clearColor].CGColor : RGB16(COLOR_FONT_9B9B9B).CGColor;
    _confirmButton.backgroundColor = res ? RGB16(COLOR_FONT_FF1130) : [UIColor clearColor];
}

- (void)confirmClicked:(UIButton*)btn {
    if (_confirmButton.selected == NO) {
        return;
    }
    __weak typeof(self) ws = self;
    NSDictionary *param = @{
                            @"account" : _tf_name.text,
                            @"accountNumber" : _tf_account.text,
                            @"accountCity" : @"",
                            @"accountBranch" : @"",
                            @"accountType" : @"ABC",
                            };
    NSString *path = [@"/money/bank/bind?token=" stringByAppendingString:[AccountManager sharedAccountManager].account.token];
    [HTTPFacade postPath:path param:param body:nil successBlock:^(id  _Nonnull object) {
        [VLSUtily toastWithText:LocalizedString(@"success_bankcard") addtoview:self.view animation:YES duration:2 center:YES];
        ws.model.accountNumber = _tf_account.text;
        if ([ws.delegate respondsToSelector:@selector(bindingCardDidSuccess:)]) {
            [ws.delegate bindingCardDidSuccess:ws.model];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [ws.navigationController popViewControllerAnimated:YES];
        });
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        
    }];
}

#pragma mark - UITableViewDataSource,UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
    if (indexPath.row != 2) {
        UILabel *label = [UILabel new];
        //TODO:国际化
        label.text = indexPath.row ? LocalizedString(@"card_num") : LocalizedString(@"account");
        label.font = [UIFont systemFontOfSize:16];
        [label sizeToFit];
        [label setCenterY_sd:CELL_HEIGHT / 2];
        [label setLeft_sd:SEPARAOR_LEFTMARGIN];
        label.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:label];
        UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - LABEL_WIDTH, CELL_HEIGHT)];
        if (indexPath.row == 0) {
            _tf_name = tf;
        }else {
            _tf_account = tf;
        }
        [tf setLeft_sd:LABEL_WIDTH];
        tf.keyboardType = indexPath.row ? UIKeyboardTypeNumberPad : UIKeyboardTypeDefault;
        tf.textColor = RGB16(COLOR_FONT_4A4A4A);
        tf.clearButtonMode = UITextFieldViewModeWhileEditing;
        //TODO:国际化
        NSString *num = self.accountNumber ?: nil;
        NSString *name = self.accountName ?: nil;
        tf.text = indexPath.row ? num : name;
        tf.placeholder = indexPath.row ? LocalizedString(@"binding_warning") : nil;
        cell.separatorInset = indexPath.row == 1 ? UIEdgeInsetsMake(0, 0, 0, 0) : UIEdgeInsetsMake(0, SEPARAOR_LEFTMARGIN, 0, SEPARAOR_LEFTMARGIN);
        [cell.contentView addSubview:tf];
        if (indexPath.row == 0) {
            [tf becomeFirstResponder];
        }
    }else {
        _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - BUTTON_WIDTH) / 2, BUTTON_TOPMARGIN, BUTTON_WIDTH, 40)];
        //TODO:国际化
        [_confirmButton setTitle:LocalizedString(@"bind") forState:UIControlStateNormal];
        [_confirmButton setTitleColor:RGB16(COLOR_FONT_9B9B9B) forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        _confirmButton.layer.borderWidth = 1;
        _confirmButton.layer.borderColor = RGB16(COLOR_FONT_9B9B9B).CGColor;
        _confirmButton.layer.cornerRadius = 20;
        [_confirmButton addTarget:self action:@selector(confirmClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:_confirmButton];
    }
    cell.contentView.backgroundColor = indexPath.row == 2 ? RGB16(COLOR_BG_F8F8F8) : [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row == 2 ? 600 : CELL_HEIGHT;
}

@end
