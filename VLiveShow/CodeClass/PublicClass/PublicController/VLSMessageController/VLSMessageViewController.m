//
//  VLSMessageViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMessageViewController.h"

@interface VLSMessageViewController ()


@end

@implementation VLSMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = LocalizedString(@"STANDINSIDELETTER");
    [self loadViews];
//    [self dispathgroupOperation];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self dispathgroupOperation];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar useRedTheme];
}

- (void)rightbarClicked {
    if ([self.delegate respondsToSelector:@selector(isShowMsgStationBoard:)]) {
        [self.delegate isShowMsgStationBoard:NO];
    }
}

- (void)dispathgroupOperation
{
    [[VLSDBHelper defaultHelper] getAllMessageArrFromDB:[self reloadWhenComplete]];
}

//- (void)createData
//{
//    _messageArray = [[NSMutableArray alloc] initWithCapacity:0];
//
//}

- (complete)reloadWhenComplete
{
    complete com = ^(NSMutableArray *arr){
        self.messageArray = arr;
        [self.tableView reloadData];
    };
    return com;
}

#pragma mark - 加载视图

- (void)loadViews{

    [self.view addSubview:self.tableView];

    VLSVLiveViewModel *model1 = [[VLSVLiveViewModel alloc] init];
    model1.title = LocalizedString(@"VTEAM");
    model1.imageName = @"ic_official";

    VLSVLiveViewModel *model2 = [[VLSVLiveViewModel alloc] init];
    model2.title = LocalizedString(@"LIVENOTICE");
    model2.imageName = @"ic_kaibotixing";

    VLSVLiveViewModel *model3= [[VLSVLiveViewModel alloc] init];
    model3.title = LocalizedString(@"FANS");
    model3.imageName = @"ic_fans";
    [self.messageArray addObject:model1];
    [self.messageArray addObject:model2];
    [self.messageArray addObject:model3];
    
    [self.tableView reloadData];

}

#pragma mark - UITableviewDelegate 和 UITableviewDataSource 代理方法

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.messageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VLSVLiveShowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    VLSVLiveViewModel *model = [self.messageArray objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        VLSVTeamViewController *teamVC = [[VLSVTeamViewController alloc] init];
        teamVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:teamVC animated:YES];
        [VLSDBHelper UpdateTeamDBandReload:[self reloadWhenComplete]];
        
    }else if (indexPath.row == 1){
    
        VLSLiveNoticeViewController *noticeVC = [[VLSLiveNoticeViewController alloc] init];
        noticeVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:noticeVC animated:YES];
        [VLSDBHelper UpdateLiveDBandReload:[self reloadWhenComplete]];
    }else{
    
        VLSFansViewController *fansVC = [[VLSFansViewController alloc] init];
        fansVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:fansVC animated:YES];
        [VLSDBHelper UpdateFansDBandReload:[self reloadWhenComplete]];
    }
    
//    [self dispathgroupOperation];
}

#pragma mark - 初始化控件

-(UITableView *)tableView{

    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //注册
        [_tableView registerClass:[VLSVLiveShowTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

@end
