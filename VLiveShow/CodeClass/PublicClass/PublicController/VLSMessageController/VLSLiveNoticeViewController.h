//
//  VLSLiveNoticeViewController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "VLSLiveRemindModel.h"
#import "VLSMessageTableViewCell.h"
#import "VLSMyMessageViewModel.h"
#import "VLSLiveListModel.h"
#import "VLSShowViewController.h"
#import "VLSDBHelper.h"

@interface VLSLiveNoticeViewController : VLSBaseViewController<UITableViewDelegate,UITableViewDataSource>{
    
    NSInteger page;
    NSInteger pageSize;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *liveArray;

@end
