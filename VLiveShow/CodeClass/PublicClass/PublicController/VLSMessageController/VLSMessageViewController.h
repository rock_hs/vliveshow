//
//  VLSMessageViewController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "VLSVLiveShowTableViewCell.h"
#import "VLSFansViewController.h"
#import "VLSVTeamViewController.h"
#import "VLSLiveNoticeViewController.h"
#import "VLSTeamMessageModel.h"
#import "VLSLiveRemindModel.h"
#import "VLSFansModel.h"
#import "VLSVLiveViewModel.h"
#import "LKDBHelper.h"
#import "VLSDBHelper.h"

@interface VLSMessageViewController : VLSBaseViewController<UITableViewDelegate, UITableViewDataSource>{
    
    NSInteger countV;
    NSInteger countR;
    NSInteger countF;
    
    VLSTeamMessageModel *vTeamModel;
    VLSLiveRemindModel *vLiveModel;
    VLSFansModel *vFansModel;
    
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *messageArray;
@property (nonatomic, weak) id delegate;
@end
