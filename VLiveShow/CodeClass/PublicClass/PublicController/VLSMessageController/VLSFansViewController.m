//
//  VLSFansViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSFansViewController.h"

@interface VLSFansViewController ()
@end

@implementation VLSFansViewController

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar useRedTheme];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    self.title = LocalizedString(@"FANS");
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        // 耗时的操作
//        [self getModelFromDB];
//    });
    [self createTableView];
    [self configer];
    [self loadMoreData];

    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_delete"] style:UIBarButtonItemStylePlain target:self action:@selector(rightItemAction:)];
    self.navigationItem.rightBarButtonItem = rightItem;
}
- (void)configer
{
    pageSize = 20;
    page = 0;
    
    self.fansArray = [NSMutableArray arrayWithCapacity:0];
}
- (void)loadMoreData
{
    [VLSDBHelper fansSearchWithPage:page pageSize:pageSize completeHandler:^(NSMutableArray *arr) {
        [self.fansArray addObjectsFromArray:arr];
        page ++;
        if (arr.count < 20 && self.fansArray.count > 20) {
            // 已经是最后一页
            return ;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
    
}
/*

 */

//表格创建
- (void)createTableView{
 
    [self.view addSubview:self.tableView];
}

#pragma mark - UITableViewDelegate 和 UITableviewDataSource代理方法

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.fansArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VLSFansViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    VLSFansModel *model = [self.fansArray objectAtIndex:indexPath.row];
    cell.model = model;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    VLSFansModel *model = [self.fansArray objectAtIndex:indexPath.row];
    [[VLSUserTrackingManager shareManager] trackingViewprofile:model.UserID];
    vlsVC.userId = model.UserID;
    [self.navigationController pushViewController:vlsVC animated:YES];

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.fansArray.count >= 20 && (self.fansArray.count - indexPath.row) == 5) {
        [self loadMoreData];
    }
}



-(UITableView *)tableView{

    if (_tableView == nil) {
        int hight = 0;
//        if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
//            hight = SCREEN_HEIGHT;
//        }else {
            hight = SCREEN_HEIGHT - 64;
//        }
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, hight) style:UITableViewStylePlain];
        
        //数据源代理
        _tableView.dataSource = self;
        //tableview代理
        _tableView.delegate = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        [_tableView registerClass:[VLSFansViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.separatorColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];

    }
    return _tableView;
}


-(NSMutableArray *)fansArray{
    
    if (!_fansArray) {
        _fansArray = [[NSMutableArray alloc] init];
    }
    return _fansArray;
}

#pragma mark - cell代理事件
-(void)focusBtnViewCellBtn:(VLSFansViewCell *)cell{
    
    NSIndexPath *indexpath = [self.tableView indexPathForCell:cell];
    
    VLSFansModel *model = [self.fansArray objectAtIndex:indexpath.row];
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    [[VLSUserTrackingManager shareManager] trackingViewprofile:model.UserID];
    vlsVC.userId = model.UserID;
    [self.navigationController pushViewController:vlsVC animated:YES];
    
}

- (void)rightItemAction:(id)sender{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"BESUERTO_CLEAR_ALLMESSAGE") preferredStyle:UIAlertControllerStyleAlert];
    // 确定按钮
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [VLSFansModel deleteWithWhere:nil];
        self.fansArray = nil;
        [self.tableView reloadData];
    }];
    // 取消按钮
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [alertController addAction:cancleAction];
    
    [okAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    [okAction setValue:[UIFont systemFontOfSize:SIZE_FONT_14] forKey:@"titleFont"];
    
    [cancleAction setValue:RGB16(COLOR_FONT_4A4A4A) forKey:@"titleTextColor"];
    [cancleAction setValue:[UIFont systemFontOfSize:SIZE_FONT_14] forKey:@"titleFont"];
    [self presentViewController:alertController animated:YES completion:nil];
}











/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
