//
//  VLSVTeamViewController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"

@interface VLSVTeamViewController : VLSBaseViewController<UITableViewDelegate,UITableViewDataSource>{
    
    NSInteger page;
    NSInteger pageSize;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *vTeamArray;


@end
