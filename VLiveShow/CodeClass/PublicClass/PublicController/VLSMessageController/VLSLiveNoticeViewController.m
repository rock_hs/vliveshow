//
//  VLSLiveNoticeViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveNoticeViewController.h"
#import "VLSLandscapeMessageTableViewCell.h"

@interface VLSLiveNoticeViewController ()

@end

@implementation VLSLiveNoticeViewController

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar useRedTheme];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    self.title = LocalizedString(@"LIVENOTICE");
    [self loadAllView];
    [self configer];
    [self loadMoreData];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_delete"] style:UIBarButtonItemStylePlain target:self action:@selector(rightItemAction:)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)configer
{
    pageSize = 20;
    page = 0;
    
    self.liveArray = [NSMutableArray arrayWithCapacity:0];
}
- (void)loadMoreData
{
    [VLSDBHelper LiveSearchWithPage:page pageSize:pageSize completeHandler:^(NSMutableArray *arr) {
        [self.liveArray addObjectsFromArray:arr];
        page ++;
        if (arr.count < 20 && self.liveArray.count > 20) {
            // 已经是最后一页
            return ;
        }
        
        [self.tableView reloadData];
    }];
}

- (void)loadAllView{

    [self.view addSubview:self.tableView];
}

#pragma mark - UITableviewDelegate 和 UITableviewDataSource 代理方法

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.liveArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
//    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
//        VLSLandscapeMessageTableViewCell *cell_ = [tableView dequeueReusableCellWithIdentifier:LandscapeMessageTableViewCellIdentifier forIndexPath:indexPath];
//        if (self.liveArray.count > 0) {
//            VLSLiveRemindModel *model = [self.liveArray objectAtIndex:indexPath.row];
//            cell_.model = model;
//        }
//        cell = cell_;
//    }else {
        VLSMessageTableViewCell *cell_ = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell_.selectionStyle = UITableViewCellSelectionStyleNone;
        cell_.accessoryType = UITableViewCellAccessoryNone;
        if (self.liveArray.count > 0) {
            VLSLiveRemindModel *model = [self.liveArray objectAtIndex:indexPath.row];
            cell_.model = model;
        }
        cell = cell_;
//    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (self.orientation == KLiveRoomOrientation_LandscapeLeft || self.orientation == KLiveRoomOrientation_LandscapeRight) {
//        return 89;
//    }
    return [self.tableView cellHeightForIndexPath:indexPath model:self.liveArray[indexPath.row] keyPath:@"model" cellClass:[VLSMessageTableViewCell class] contentViewWidth:[self cellContentViewWith]];
    
}

- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    // 适配ios7
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
        width = [UIScreen mainScreen].bounds.size.height;
    }
    return width;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //没有预约的用户无法收到课程直播的通知，本处不用判断课程和用户的预约关系
    VLSShowViewController *vshow = [[VLSShowViewController alloc] init];
    VLSLiveRemindModel *modelR = [self.liveArray objectAtIndex:indexPath.row];
    
    vshow.anchorId = modelR.anchorID;
    vshow.roomId = modelR.RoomID;
    vshow.isFromMail = YES;
    
    VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
    joinRoom.roomID = modelR.RoomID;
    joinRoom.joinType = @"notice";
    vshow.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];
    
    [self.navigationController pushViewController:vshow animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.liveArray.count >= 20 && (self.liveArray.count - indexPath.row) == 5) {
        [self loadMoreData];
    }
}


#pragma mark - 控件初始化

-(UITableView *)tableView{
    
    if (_tableView == nil) {
        int hight = 0;
//        if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
//            hight = SCREEN_HEIGHT;
//        }else {
            hight = SCREEN_HEIGHT - 64;
//        }
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, hight) style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //注册
        [_tableView registerClass:[VLSMessageTableViewCell class] forCellReuseIdentifier:@"cell"];
        [_tableView registerNib:[UINib nibWithNibName:@"VLSLandscapeMessageTableViewCell" bundle:nil] forCellReuseIdentifier:LandscapeMessageTableViewCellIdentifier];
        //设置cell的估计高度
        _tableView.estimatedRowHeight = 200;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

-(NSMutableArray *)liveArray{

    if (!_liveArray) {
        _liveArray = [NSMutableArray new];
    }
    return _liveArray;
}

- (void)rightItemAction:(id)sender{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"BESUERTO_CLEAR_ALLMESSAGE") preferredStyle:UIAlertControllerStyleAlert];
    // 确定按钮
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [VLSLiveRemindModel deleteWithWhere:nil];
        self.liveArray = nil;
        [self.tableView reloadData];
    }];
    // 取消按钮
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [alertController addAction:cancleAction];
    
    [okAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    [okAction setValue:[UIFont systemFontOfSize:SIZE_FONT_14] forKey:@"titleFont"];
    
    [cancleAction setValue:RGB16(COLOR_FONT_4A4A4A) forKey:@"titleTextColor"];
    [cancleAction setValue:[UIFont systemFontOfSize:SIZE_FONT_14] forKey:@"titleFont"];
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
