//
//  VLSVTeamViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSVTeamViewController.h"
#import "VLSShowViewController.h"
#import "VLSTeamMessageModel.h"
#import "VTeamTableViewCell.h"
#import "VLSDBHelper.h"
#import "TiePhoneNumberController.h"

@interface VLSVTeamViewController ()
@end

@implementation VLSVTeamViewController

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar useRedTheme];
    self.tableView.frame = self.view.bounds;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = LocalizedString(@"VTEAM");
    [self loadAllView];
    

    [self configer];
    [self loadMoreData];

    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_delete"] style:UIBarButtonItemStylePlain target:self action:@selector(rightItemAction:)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)configer
{
    pageSize = 20;
    page = 0;
    
    self.vTeamArray = [NSMutableArray arrayWithCapacity:0];
}
- (void)loadMoreData
{

    [VLSDBHelper teamSearchWithPage:page pageSize:pageSize completeHandler:^(NSMutableArray *arr) {
        [self.vTeamArray addObjectsFromArray:arr];
        page ++;
        if (arr.count < 20 && self.vTeamArray.count > 20) {
            // 已经是最后一页
            return ;
        }
        
        [self.tableView reloadData];
    }];
    
}



- (void)loadAllView{
    
    [self.view addSubview:self.tableView];
}



#pragma mark - UITableviewDelegate 和 UITableviewDataSource 代理方法

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.vTeamArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VTeamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    if (_vTeamArray.count > 0) {
        
        VLSTeamMessageModel *model = [self.vTeamArray objectAtIndex:indexPath.row];
        cell.model = model;
    }
   
    cell.headImageView.image = [UIImage imageNamed:@"ic_official"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    VLSTeamMessageModel *model = [self.vTeamArray objectAtIndex:indexPath.row];
    if (model.roomID == nil) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }else{
        if ([AccountManager sharedAccountManager].account.mobilePhone != nil && ![[AccountManager sharedAccountManager].account.mobilePhone isEqualToString:@""]) {//邀请上线必须绑定手机号码
            NSString *userid = model.anchorID;
            VLSShowViewController *show = [[VLSShowViewController alloc] init];
            show.anchorId = userid;
            show.roomId = model.roomID;
            show.isFromMailInvite = YES;
            
            VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
            joinRoom.roomID = model.roomID;
            joinRoom.joinType = @"invited";
            
            show.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];
            [[VLSUserTrackingManager shareManager] trackingAgreeGotoRoom:model.roomID anchorid:userid fromWhere:@"noticeInvite"];
            
            show.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:show animated:YES];
        } else {
            TiePhoneNumberController* acc = [[TiePhoneNumberController alloc] init];
            [self.navigationController pushViewController:acc animated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [self.tableView cellHeightForIndexPath:indexPath model:[self.vTeamArray objectAtIndex:indexPath.row] keyPath:@"model" cellClass:[VTeamTableViewCell class] contentViewWidth:[self cellContentViewWith]];
}

- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    // 适配ios7
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
        width = [UIScreen mainScreen].bounds.size.height;
    }
    return width;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.vTeamArray.count >= 20 && (self.vTeamArray.count - indexPath.row) == 5) {
        [self loadMoreData];
    }
}


#pragma mark - 控件初始化

-(UITableView *)tableView{
    
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //注册
        [_tableView registerClass:[VTeamTableViewCell class] forCellReuseIdentifier:@"cell"];
        //设置cell的估计高度
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

-(NSMutableArray *)vTeamArray{

    if (!_vTeamArray) {
        _vTeamArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _vTeamArray;
}

- (void)rightItemAction:(id)sender{

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"BESUERTO_CLEAR_ALLMESSAGE") preferredStyle:UIAlertControllerStyleAlert];
    // 确定按钮
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [VLSTeamMessageModel deleteWithWhere:nil];
        self.vTeamArray = nil;
        [self.tableView reloadData];
    }];
    // 取消按钮
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [alertController addAction:cancleAction];
    
    [okAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    [okAction setValue:[UIFont systemFontOfSize:SIZE_FONT_14] forKey:@"titleFont"];
    
    [cancleAction setValue:RGB16(COLOR_FONT_4A4A4A) forKey:@"titleTextColor"];
    [cancleAction setValue:[UIFont systemFontOfSize:SIZE_FONT_14] forKey:@"titleFont"];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
