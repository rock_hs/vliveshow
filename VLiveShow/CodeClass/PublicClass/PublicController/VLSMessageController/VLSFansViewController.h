//
//  VLSFansViewController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "VLSFansViewCell.h"
#import "ManagerCallBack.h"
#import "VLSLiveHttpManager.h"
#import "HTTPFacade.h"
#import "VLSMyMessageViewModel.h"
#import "VLSOthersViewController.h"
#import "VLSFansModel.h"
#import "VLSDBHelper.h"

@interface VLSFansViewController : VLSBaseViewController<UITableViewDelegate,UITableViewDataSource,VLSFansTableViewCellDelegate>{
    
    NSInteger page;
    NSInteger pageSize;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *cellLine;
@property (nonatomic, strong) NSMutableArray *fansArray;


@end
