//
//  HTTPFacade+get.h
//  VLiveShow
//
//  Created by tom.zhu on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HTTPFacade.h"
#import "ManagerEvent.h"
#import "PostFileModel.h"

typedef void(^NetworkSucBlock)(_Nonnull id object);
typedef void(^NetworkProcessBlock)(unsigned long long size, unsigned long long total);
typedef void(^NetworkErrBlock)(ManagerEvent * _Nonnull error);

static NSString * _Nonnull HOST_VERSION = @"v1";

@interface HTTPFacade (get)
+ (void)getPath:(nonnull NSString*)path
          param:(nonnull NSDictionary*)param
   successBlock:(nonnull NetworkSucBlock)succBlock
     errorBlcok:(nonnull NetworkErrBlock)errorBlock;

/**
 *  post 请求
 *
 *  @param path       path
 *  @param param      参数
 *  @param bodys      表单数据 默认为nil，非空则表示有表单内容
 *  @param succBlock  成功回调
 *  @param errorBlock 失败回调
 */
+ (void)postPath:(nonnull NSString*)path
           param:(nonnull id)param
            body:(nullable NSDictionary*)bodys
    successBlock:(nonnull NetworkSucBlock)succBlock
      errorBlcok:(nonnull NetworkErrBlock)errorBlock;

+ (void)uploadPath:(nonnull NSString*)path
              file:(nullable PostFileModel *)postFile
             param:(nonnull NSDictionary*)param
      successBlock:(nonnull NetworkSucBlock)succBlock
      processBlock:(nonnull NetworkProcessBlock)processBlock
        errorBlcok:(nonnull NetworkErrBlock)errorBlock;

+ (void)delePath:(nonnull NSString*)path
           param:(nonnull NSDictionary*)param
    successBlock:(nonnull NetworkSucBlock)succBlock
      errorBlcok:(nonnull NetworkErrBlock)errorBlock;

+ (void)putPath:(nonnull NSString*)path
          param:(nonnull NSDictionary*)param
   successBlock:(nonnull NetworkSucBlock)succBlock
     errorBlcok:(nonnull NetworkErrBlock)errorBlock;
@end
