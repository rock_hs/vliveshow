//
//  HTTPFacade+get.m
//  VLiveShow
//
//  Created by tom.zhu on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HTTPFacade+get.h"
#import "ManagerUtil.h"
#import "VLSUtily.h"
#import "AFNetworkReachabilityManager.h"

@interface HTTPFacade ()
/**
 *  get
 */
+(NSUInteger)get:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback;
/**
 *  上传文件
 */
+ (NSInteger)uploadFile:(NSString*)url file:(PostFileModel *)postFile parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback;
/**
 *  delete
 */
+(NSUInteger)delete:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback;
@end

@implementation HTTPFacade (get)
+ (BOOL)isNetReachable {
    return [[AFNetworkReachabilityManager sharedManager] isReachable];
}

+(NSString*)getJSON:(NSObject*)info {
    if ([NSJSONSerialization isValidJSONObject:info]) {
        NSError* error;
        NSData* registerData = [NSJSONSerialization dataWithJSONObject:info options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:registerData encoding:NSUTF8StringEncoding];
        //去掉空格和换行符
        NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
        
        NSRange range = {0,jsonString.length};
        
        [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
        
        NSRange range2 = {0,mutStr.length};
        
        [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
        return mutStr;
    }
    else {
        return @"";
    }
}

+(NSURLSessionDataTask *)post:(NSString*)url parameter:(id)parameter body:(NSDictionary*)bodys requestSerializer:(id)ser callback:(HttpCallBack *)callback
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 30.0f;
    
    if (!ser) {
        manager.requestSerializer =[AFJSONRequestSerializer serializer];
    }
    
    NSMutableDictionary* para = [NSMutableDictionary dictionary];
    [para addEntriesFromDictionary:[self defaultParameter]];
    if (parameter) {
        [para addEntriesFromDictionary:parameter];
    }
    NSLog(@"%@\n%@",url,para);
    NSURLSessionDataTask *task = nil;
    
    if (bodys) {
        task = [manager POST:url parameters:para constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            if (bodys) {
                NSString *str = [HTTPFacade getJSON:bodys];
                NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                
                [formData appendPartWithFileData:data name:@"1" fileName:@"1" mimeType:@"application/json"];
            }
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            callback.doneBlock(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            callback.errorBlock(error);
        }];
    }else {
    task = [manager POST:url parameters:para progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        callback.doneBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback.errorBlock(error);
    }];
    }
    
    return task;
}

+ (HttpCallBack*)callback:(NetworkSucBlock)sucBlock
             processBlock:(NetworkProcessBlock)processBlcok
                 errBlock:(NetworkErrBlock)errBlock {
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if (sucBlock) {
            sucBlock(result);
        }
    };
    callback.errorBlock = ^(id result){
        if (errBlock) {
            errBlock(result);
        }
    };
//    callback.ProcessBlock = ^ (unsigned long long size, unsigned long long total){
//        if (processBlcok) {
//            processBlcok(size,total);
//        }
//    };
    callback.loadBlock = ^(id result){
        NSDictionary *dic = (NSDictionary*)result;
        float size = [dic[@"size"] floatValue];
        float total = [dic[@"total"] floatValue];
        if (processBlcok) {
            processBlcok(total, size);
        }
    };
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    return http;
}

+ (void)getPath:(NSString*)path
          param:(NSDictionary*)param
   successBlock:(NetworkSucBlock)succBlock
     errorBlcok:(NetworkErrBlock)errorBlock {
//    if (![HTTPFacade isNetReachable]) {
//        [VLSUtily toastWithText:LocalizedString(@"check_network") addtoview:[[[UIApplication sharedApplication] delegate] window] animation:YES duration:1 center:YES];
//        return;
//    }
    HttpCallBack* callback = [HTTPFacade callback:succBlock processBlock:nil errBlock:errorBlock];
    Method originalMethod = class_getClassMethod(objc_getClass("HTTPFacade"), @selector(get:parameter:callback:));
    NSString *hostUrl = [[BASE_URL stringByAppendingString:@"/"] stringByAppendingString:HOST_VERSION];
    NSString *fullUrl = [hostUrl stringByAppendingString:path];
    if (originalMethod) {
        [HTTPFacade get:fullUrl parameter:param callback:callback];
    }
}

+ (void)postPath:(nonnull NSString*)path
           param:(nonnull id)param
            body:(nullable NSArray*)bodys
    successBlock:(nonnull NetworkSucBlock)succBlock
      errorBlcok:(nonnull NetworkErrBlock)errorBlock {
//    if (![HTTPFacade isNetReachable]) {
//        [VLSUtily toastWithText:LocalizedString(@"check_network") addtoview:[[[UIApplication sharedApplication] delegate] window] animation:YES duration:1 center:YES];
//        return;
//    }
    HttpCallBack* callback = [HTTPFacade callback:succBlock processBlock:nil errBlock:errorBlock];
    Method originalMethod = class_getClassMethod(objc_getClass("HTTPFacade"), @selector(post:parameter:body:requestSerializer:callback:));
    NSString *hostUrl = [[BASE_URL stringByAppendingString:@"/"] stringByAppendingString:HOST_VERSION];
    NSString *fullUrl = [hostUrl stringByAppendingString:path];
    if (originalMethod) {
        [HTTPFacade post:fullUrl parameter:param body:bodys requestSerializer:nil callback:callback];
    }
}

+ (void)uploadPath:(nonnull NSString*)path
              file:(nullable PostFileModel *)postFile
             param:(nonnull NSDictionary*)param
      successBlock:(nonnull NetworkSucBlock)succBlock
      processBlock:(nonnull NetworkProcessBlock)processBlock
        errorBlcok:(nonnull NetworkErrBlock)errorBlock {
//    if (![HTTPFacade isNetReachable]) {
//        [VLSUtily toastWithText:LocalizedString(@"check_network") addtoview:[[[UIApplication sharedApplication] delegate] window] animation:YES duration:1 center:YES];
//        return;
//    }
    HttpCallBack* callback = [HTTPFacade callback:succBlock processBlock:processBlock errBlock:errorBlock];
    Method originalMethod = class_getClassMethod(objc_getClass("HTTPFacade"), @selector(uploadFile:file:parameter:callback:));
    NSString *hostUrl = [[BASE_URL stringByAppendingString:@"/"] stringByAppendingString:HOST_VERSION];
    NSString *fullUrl = [hostUrl stringByAppendingString:path];
    if (originalMethod) {
        if (postFile) {
            [HTTPFacade uploadFile:fullUrl file:postFile parameter:param callback:callback];
        }
    }
}

+ (void)delePath:(NSString*)path
           param:(NSDictionary*)param
    successBlock:(NetworkSucBlock)succBlock
      errorBlcok:(NetworkErrBlock)errorBlock {
//    if (![HTTPFacade isNetReachable]) {
//        [VLSUtily toastWithText:LocalizedString(@"check_network") addtoview:[[[UIApplication sharedApplication] delegate] window] animation:YES duration:1 center:YES];
//        return;
//    }
    HttpCallBack* callback = [HTTPFacade callback:succBlock processBlock:nil errBlock:errorBlock];
    Method originalMethod = class_getClassMethod(objc_getClass("HTTPFacade"), @selector(delete:parameter:callback:));
    NSString *hostUrl = [[BASE_URL stringByAppendingString:@"/"] stringByAppendingString:HOST_VERSION];
    NSString *fullUrl = [hostUrl stringByAppendingString:path];
    if (originalMethod) {
        [HTTPFacade delete:fullUrl parameter:param callback:callback];
    }
}

+ (void)putPath:(NSString*)path
          param:(NSDictionary*)param
   successBlock:(NetworkSucBlock)succBlock
     errorBlcok:(NetworkErrBlock)errorBlock {
//    if (![HTTPFacade isNetReachable]) {
//        [VLSUtily toastWithText:LocalizedString(@"check_network") addtoview:[[[UIApplication sharedApplication] delegate] window] animation:YES duration:1 center:YES];
//        return;
//    }
}

@end
