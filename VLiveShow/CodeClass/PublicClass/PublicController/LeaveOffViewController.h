//
//  LeaveOffViewController.h
//  VLiveShow
//
//  Created by sp on 16/8/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSUserProfile.h"
#import "VLSGuestModel.h"
#import "VLSLiveHttpManager.h"
#import "LeaveOffContentView.h"
#import "ShareView.h"
#import "GuestViewCell.h"
#import "BackDimView.h"
#import "VLSOthersViewController.h"
@protocol LeaveOffViewDelegate <NSObject>

- (void)backBtnClick;

@end

@interface LeaveOffViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)UIView *guestView;//嘉宾显示块
@property (nonatomic,strong)UICollectionView * guestCollectionView;
@property (nonatomic,strong)UIButton * backButton;
@property (nonatomic,strong)BackDimView *backView;
@property (nonatomic,strong)ShareView *shareView;//分享块
@property (nonatomic,strong)LeaveOffContentView *contentView;//上方显示信息块
@property (nonatomic,strong)UIImageView * headImageView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UICollectionViewFlowLayout *layout;
@property (nonatomic,copy)NSString *userID;
@property (nonatomic,copy)NSString *roomId;
@property (nonatomic,weak)id<LeaveOffViewDelegate> delegate;
@property (nonatomic,strong)NSMutableArray *guestArray;

@property (nonatomic, strong) NSNumber* lessonId;
@property (nonatomic, assign) BOOL isAnchor;
@property (nonatomic, strong) VLSUserProfile *anchorProfile;
@end
