//
//  WithdrawalDetailViewController.m
//  Withdrawal
//
//  Created by tom.zhu on 16/8/11.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import "WithdrawalDetailViewController.h"
#import "WithdrawalDetailCellTableViewCell.h"
#import "HTTPFacade+get.h"
#import "WithdrawalDetailModel.h"
#import <objc/runtime.h>
static NSUInteger PAGE_NUM = 8;
@interface WithdrawalDetailViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView    *tableview;
@property (nonnull, strong) NSMutableArray *dataArray;
@end

@implementation WithdrawalDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = @[].mutableCopy;
    [self requestData:YES];
    [self uiconfig];
}

- (void)uiconfig {
    [self.tableview registerNib:[UINib nibWithNibName:@"WithdrawalDetailCellTableViewCell" bundle:nil] forCellReuseIdentifier:CELLIDENTITY_WITHDRAWDETAIL];
    self.tableview.tableFooterView = UIView.new;
}

- (void)requestData:(BOOL)pull {
    NSUInteger pageSize = PAGE_NUM, pageNum = 1;
    if (pull) {
        pageNum = 1;
    }else {
        pageNum = (self.dataArray.count + pageNum - 1) / pageNum;
    }
    __weak typeof(self) ws = self;
    NSString *path = [NSString stringWithFormat:@"/money/cashout/list?pageSize=%ld&pageNum=%ld", pageSize, pageNum];
    [HTTPFacade getPath:path param:@{@"token" : [AccountManager sharedAccountManager].account.token} successBlock:^(id  _Nonnull object) {
        if (pull) {
            [ws.dataArray removeAllObjects];
        }
        NSArray *ary = [object objectForKey:@"list"];
        [ary enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            WithdrawalDetailModel *model = [[WithdrawalDetailModel alloc] initWithDic:obj];
            [ws.dataArray addObject:model];
        }];
        [ws.tableview reloadData];
        [ws checkData];
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        
    }];
    [self.tableview reloadData];
}

- (void)checkData {
    if (self.dataArray.count == 0) {
        UIView *backView = [UIView new];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_tixian_quesheng"]];
        UILabel *label = [UILabel new];
        label.text = LocalizedString(@"no_withdraw");
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = RGB16(COLOR_FONT_9B9B9B);
        [label sizeToFit];
        [backView addSubview:imageView];
        [backView addSubview:label];
        
        float width = 0, heigth = 0;
        CGRect rect = imageView.frame;
        heigth += rect.size.width;
        heigth += 14;
        heigth += label.frame.size.height;
        width = MAX(imageView.frame.size.width, label.frame.size.width);
        rect.size.width = width;
        rect.origin.x = (SCREEN_WIDTH - width) / 2;
        rect.origin.y = 166;
        backView.frame = rect;
        
        CGRect temRect = label.frame;
        temRect.origin.y = CGRectGetHeight(imageView.frame) + 14;
        label.frame = temRect;
        temRect = imageView.frame;
        temRect.origin.x = (width - imageView.frame.size.width) / 2;
        imageView.frame = temRect;
        
        [self.view addSubview:backView];
    }
}

- (CGFloat)cellheight:(UITableViewCell*)cell configuration:(void(^)(WithdrawalDetailCellTableViewCell *cell))configuration {
    [cell prepareForReuse];
    if (configuration) {
        configuration((WithdrawalDetailCellTableViewCell*)cell);
    }
    CGFloat fittingHeight = 0;
    NSLayoutConstraint *widthFenceConstraint = [NSLayoutConstraint constraintWithItem:cell.contentView
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:nil
                                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                                           multiplier:1.0
                                                                             constant:[UIScreen mainScreen].bounds.size.width];
    [cell.contentView addConstraint:widthFenceConstraint];
    fittingHeight = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    [cell.contentView removeConstraint:widthFenceConstraint];
    return fittingHeight;
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WithdrawalDetailCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELLIDENTITY_WITHDRAWDETAIL forIndexPath:indexPath];
    [cell setmodel:[self.dataArray objectAtIndex:indexPath.row]];
    return cell;
}

@end
