//
//  VLSMultiLiveViewController.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMultiLiveBaseViewController.h"
#import "VLSLiveContentView.h"
#import "VLSVedioView.h"
#import "VLSRoomIDView.h"
#import "LeaveOffViewController.h"
#import "VLSPriseManager.h"
#import "coverImage.h"
#import "CountDownView.h"
#import "RankingListViewController.h"
@interface VLSMultiLiveViewController : VLSMultiLiveBaseViewController
// 视频 View
@property (nonatomic, strong) VLSVedioView *videoView;
@property (nonatomic, strong) coverImage *videoCoverView;
// 顶部弹窗
@property (nonatomic, strong) VLSHeadAlertView *headAlert;
// contentView
@property (nonatomic, strong) VLSLiveContentView *contentView;
// 关闭按钮
@property (nonatomic, strong) UIButton *closeViewButton;
@property (nonatomic,strong)  NSString *userID;

@property (nonatomic, copy)   NSString *coverUrl;
// 点赞管路
@property (nonatomic, strong) VLSPriseManager *priseManager;

//判断长按手势在触发中
@property (nonatomic, assign) BOOL isLongPressGesture;

// V来秀
@property (nonatomic, strong) VLSRoomIDView *roomIDview;

@property (nonatomic, assign) BOOL isFromMail;
@property (nonatomic, assign) BOOL isFromMailInvite;

@property(nonatomic, nullable, strong) VLSUserTrackContext* roomTrackContext;

@property (nonatomic, strong) UIViewController *pushToViewController;       //将要跳转到的控制器（横屏直播间下跳转横屏直播间、横屏直播间跳转竖屏直播间、竖屏直播间跳转横屏直播间）

- (VLSVideoItem *)chectPoint:(CGPoint)checkPoint;

- (void)priseViewTouch;
- (void)isShowMsgStationBoard:(BOOL)isShow;

- (void)stopAllOperation;
- (BOOL)stopWatchingWithCompleteBlock: (void (^)(BOOL)) completeBlock;

@end
