//
//  HTTPFacade+withdraw.m
//  VLiveShow
//
//  Created by tom.zhu on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HTTPFacade+withdraw.h"
#import "ManagerUtil.h"

@interface HTTPFacade ()
+(NSUInteger)get:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback;
@end

@implementation HTTPFacade (withdraw)
+ (HttpCallBack*)callback:(NetworkSucBlock)sucBlock errBlock:(NetworkErrorBlock)errBlock {
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if (sucBlock) {
            sucBlock(result);
        }
    };
    callback.errorBlock = ^(id result){
        if (errBlock) {
            errBlock(result);
        }
    };
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    return http;
}

+ (void)cashoutUsable:(NetworkSucBlock)sucBlock errBlock:(NetworkErrorBlock)errBlock {
    HttpCallBack* callback = [ HTTPFacade callback:sucBlock errBlock:errBlock];
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSDictionary *param = @{
                            @"token" : token,
                            };
    Method originalMethod = class_getClassMethod(objc_getClass("HTTPFacade"), @selector(get:parameter:callback:));
    if (originalMethod) {
        [HTTPFacade get:@"http://124.172.174.187:8888/vliveshow-api-app/v1/money/cashout/usable" parameter:param callback:callback];
    }
}

+ (void)cashoutReamin:(NetworkSucBlock)sucBlock errBlock:(NetworkErrorBlock)errBlock {
    HttpCallBack* callback = [ HTTPFacade callback:sucBlock errBlock:errBlock];
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSDictionary *param = @{
                            @"token" : token,
                            };
    Method originalMethod = class_getClassMethod(objc_getClass("HTTPFacade"), @selector(get:parameter:callback:));
    if (originalMethod) {
        NSString *path = [BASE_URL stringByAppendingString:@"/v1/money/user/balance"];
//        [HTTPFacade get:@"http://124.172.174.187:8888/vliveshow-api-app/v1/money/user/balance" parameter:param callback:callback];
        [HTTPFacade get:path parameter:param callback:callback];
    }
}
@end
