//
//  BindingCardViewController.h
//  VLiveShow
//
//  Created by tom.zhu on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoneybankModel.h"

@protocol BindingCardViewControllerDelegate <NSObject>
- (void)bindingCardDidSuccess:(MoneybankModel*)model;
@end

@interface BindingCardViewController : UIViewController
@property (nonatomic, assign) id<BindingCardViewControllerDelegate> delegate;
@property (nonatomic, strong) NSString *accountNumber;
@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) MoneybankModel *model;
@end
