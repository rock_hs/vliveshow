//
//  VLSUtily.m
//  VLiveShow
//
//  Created by tom.zhu on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSUtily.h"
#import "MBProgressHUD.h"
#import "HTTPFacade+get.h"  

@implementation VLSUtily
//SINGLETONM(VLSUtily)
#pragma mark - 日历获取在9.x之后的系统使用currentCalendar会出异常。在8.0之后使用系统新API。
+ (NSCalendar *)currentCalendar {
    if ([NSCalendar respondsToSelector:@selector(calendarWithIdentifier:)]) {
        return [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    }
    return [NSCalendar currentCalendar];
}

+ (NSDate *)getNowDateFromatAnDate:(NSDate *)anyDate {
    //设置源日期时区
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];//或UTC
    //设置转换后的目标日期时区
    NSTimeZone* destinationTimeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    //得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:anyDate];
    //目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:anyDate];
    //得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    //转为现在时间
    NSDate* destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:anyDate];
    return destinationDateNow;
}

+ (NSDateComponents *)getShanghaiNowDateFromat:(NSDate *)AnDate {
    NSDate *Anydate = [VLSUtily getNowDateFromatAnDate:AnDate];
    NSCalendar *calendar = [VLSUtily currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay |NSCalendarUnitHour |NSCalendarUnitMinute;
    NSDateComponents *cmp = [calendar components:unitFlags fromDate:Anydate];
    return cmp;
}

+ (void)toastWithText:(NSString*)str addtoview:(UIView*)view animation:(BOOL)animation duration:(float)duration {
    [VLSUtily toastWithText:str addtoview:view animation:animation duration:duration yOffset:100 userEnable:NO];
}

+ (void)toastWithText:(NSString*)str addtoview:(UIView*)view animation:(BOOL)animation duration:(NSInteger)duration center:(BOOL)center {
    NSInteger n = duration ? 0 : 100;
    [VLSUtily toastWithText:str addtoview:view animation:animation duration:duration yOffset:n userEnable:NO];
}

+ (void)toastWithText:(NSString*)str addtoview:(UIView*)view animation:(BOOL)animation duration:(float)duration yOffset:(NSInteger)offset userEnable:(BOOL)enable {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:animation];
    hud.mode = MBProgressHUDModeText;
    hud.userInteractionEnabled = enable;
    hud.labelText = str;
    hud.margin = 10;
    hud.yOffset = offset;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:duration];
}

+ (void)networkWithProgressTotal:(int64_t)expected written:(int64_t)witten addtoview:(UIView*)view {
    dispatch_async(dispatch_get_main_queue(), ^{
        float progress = witten / expected;
        NSLog(@"progress == %f", progress);
        MBProgressHUD __block *hud = [MBProgressHUD HUDForView:view];
        if (!hud) {
            hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
            hud.mode = MBProgressHUDModeAnnularDeterminate;
        }
        if (progress < 1) {
            hud.progress = progress;
        }else {
            UIImage *image = [[UIImage imageNamed:@"Checkmark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            hud.customView = imageView;
            hud.mode = MBProgressHUDModeCustomView;
            [hud hide:YES afterDelay:0];
        }
    });
}

+ (NSString*) getTimeByTodayWithString:(NSString*)dateString {
    double temDouble = dateString.doubleValue;
    NSDate *time = [NSDate dateWithTimeIntervalSince1970:temDouble];
    NSString *timeStr = [VLSUtily getTimeByToday:time];
    return timeStr;
}

+ (NSString*) getTimeByToday:(NSDate*)time{
    NSDate *nowDate = [NSDate date];
    NSTimeInterval intervalSec = [nowDate timeIntervalSinceDate:time];
    NSString * str = nil;
    double day = intervalSec/(24*60*60);
    double hour = 0.0;
    int dayInt = 0;
    int hourInt = 0;
    int minuteInt = 0;
    if(day < 1.0)
    {
        hour = intervalSec/(60*60);
        if(hour < 1.0)
        {
            minuteInt =intervalSec/60;
            if (minuteInt == 0) {
                str = NSLocalizedString(@"sns_justNow", nil);
            }else{
                str = [NSString stringWithFormat:@"%d %@",minuteInt, NSLocalizedString(@"sns_minutes_ago", nil)];
            }
        }
        else
        {
            hourInt = (int) hour;
            str = [NSString stringWithFormat:@"%d %@",hourInt, NSLocalizedString(@"sns_hours_ago", nil)];
        }
    }
    else
    {
        dayInt = (int) day;
        str = [NSString stringWithFormat:@"%d %@",dayInt, NSLocalizedString(@"sns_days_ago", nil)];
    }
    return  str;
}

+ (NSString *)stringByNotRounding:(double)price afterPoint:(int)position {
    long long index = pow(10, position);
    price *= index;
    price = floor(price);
    price /= index;
    return [@(price) stringValue];
}

+ (void)isSubscription:(NSString*)courseId handle:(void(^)(id result, ManagerEvent *error))handle {
    NSString *path = [NSString stringWithFormat:@"/course/%@/subscription", courseId];
    [HTTPFacade getPath:path param:@{} successBlock:^(id  _Nonnull object) {
        if (handle) {
            handle(object, nil);
        }
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        if (handle) {
            handle(nil, error);
        }
    }];
}

+ (NSArray *)toBinarySystemWithDecimalSystem:(int)decimal {
    int num = decimal;
    int remainder = 0;      //余数
    int divisor = 0;        //除数
    
    NSString * prepare = [NSString string];
    while (true) {
        remainder = num % 2;
        divisor = num / 2;
        num = divisor;
        prepare = [prepare stringByAppendingFormat:@"%d",remainder];
        
        if (divisor == 0) {
            break;
        }
    }
    NSMutableArray *result = [NSMutableArray array];
    for (int i = 0; i < prepare.length; i++) {
        if ([[prepare substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"1"]) {
            [result addObject:@(i)];
        }
    }
    return result;
}

+ (int)randomFrom:(int)from to:(int)to {
    return from + arc4random() % abs(to - from) + 1;
}

//根据文字计算宽度
+ (CGFloat)widthOfString:(NSString *)string font:(UIFont *)font height:(CGFloat)height
{
    NSDictionary * dict=[NSDictionary dictionaryWithObject: font forKey:NSFontAttributeName];
    CGRect rect=[string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return rect.size.width;
}

+ (NSString*)currentLanguageString {
    //zh-hans为简体中文,zh-hant为繁体中文
    NSString* lan = [[[NSLocale preferredLanguages] firstObject] lowercaseString];
    if ([lan hasPrefix: @"zh-hans"])
    {
        return @"zh-hans";
    }
    else if ([lan hasPrefix: @"zh-hant"]
             || [lan containsString: @"zh-hk"]
             || [lan containsString: @"zh-tw"]
             )
    {
        return @"zh-hant";
    }
    else if ([lan hasPrefix: @"en"])
    {
        return @"en";
    }
    
    return lan;
}

@end
