//
//  WithdrawConfirmVC.m
//  Withdrawal
//
//  Created by tom.zhu on 16/8/12.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import "WithdrawConfirmVC.h"
#import "WithdrawConfirmCellTableViewCell.h"
#import "VLSUtily.h"
#import "BindingCardViewController.h"
#import "HTTPFacade+get.h"  
#import "CashoutconfigModel.h"
#import "MoneybankModel.h"

@interface WithdrawConfirmVC () <BindingCardViewControllerDelegate>

@end

@implementation WithdrawConfirmVC
{
    UIButton *_confirmButton;
    CashoutconfigModel *_configModel;
    MoneybankModel *_bankModel;
    double _taxRate;    //税率
}
- (NSString *)maxAvailable {
    if (_taxRate == 0) {
        return @"0";
    }else {
        if (_taxRate > 1 || _taxRate < 0) {
            return @"0";
        }else{
            double res = [_maxAvailable doubleValue] * (1 - _taxRate);
            
            NSString *temStr = [@(res) stringValue];
            NSRange range = [temStr rangeOfString:@"."];
            if (range.location != NSNotFound) {
                temStr = [temStr substringWithRange:NSMakeRange(0, range.location + 2)];
            }
            
            return temStr;
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _taxRate = 0;
    [self.tableView registerNib:[UINib nibWithNibName:@"WithdrawConfirmCellTableViewCell" bundle:nil] forCellReuseIdentifier:WithdrawConfirmCellTableViewCell_ID];
    self.title = LocalizedString(@"withdraw_title");
    _confirmButton = UIButton.new;
    //TODO:国际化
    [_confirmButton setTitle:LocalizedString(@"confirm") forState:UIControlStateNormal];
    [_confirmButton setTitleColor:RGB16(COLOR_FONT_9B9B9B) forState:UIControlStateNormal];
    [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    _confirmButton.layer.borderWidth = 1;
    _confirmButton.layer.borderColor = RGB16(COLOR_FONT_9B9B9B).CGColor;
    _confirmButton.layer.cornerRadius = 20;
    [_confirmButton addTarget:self action:@selector(confirmClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_confirmButton];
    _confirmButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:@[
                                     [NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:0 constant:300],
                                     [NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:0 constant:40],
                                     [NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1 constant:0],
                                     [NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant: ([UIScreen mainScreen].bounds.size.height - 157)],
                                     ]];
    [self requestData];
}

- (void)requestData {
    void (^reloadData)() = ^{
        [self.tableView reloadData];
        [self checkStatus];
    };
    NSString *path = @"/money/cashout/config";
    [HTTPFacade getPath:path param:@{} successBlock:^(id  _Nonnull object) {
        CashoutconfigModel *model = [[CashoutconfigModel alloc] initWithDic:[object objectForKey:@"config"]];
        _configModel = model;
        reloadData();
        NSString *tax = [NSString stringWithFormat:@"%.2f", [[object[@"config"] objectForKey:@"taxRate"] doubleValue]];
        _taxRate = [tax floatValue];
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        
    }];
    
    path = @"/money/bank";
    [HTTPFacade getPath:path param:@{} successBlock:^(id  _Nonnull object) {
        MoneybankModel *model = [[MoneybankModel alloc] initWithDic:[object objectForKey:@"bank"]];
        _bankModel = model;
        reloadData();
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        [VLSUtily toastWithText:error.title addtoview:self.view animation:YES duration:1];
    }];
}

- (void)checkStatus {
    NSDateComponents *cmp = [VLSUtily getShanghaiNowDateFromat:[NSDate date]];
    NSInteger day = cmp.day;
    BOOL res = NO;
    if (day > 1 && day < 10) {
        res = NO;
    }else {
        res = YES;
    }
    if (_bankModel.accountNumber && _bankModel.account) {
        res = YES;
    }else {
        res = NO;
    }
    _confirmButton.selected = res;
    _confirmButton.layer.borderColor = res ? [UIColor clearColor].CGColor : RGB16(COLOR_FONT_9B9B9B).CGColor;
    _confirmButton.backgroundColor = res ? RGB16(COLOR_FONT_FF1130) : [UIColor clearColor];
}

- (void)confirmClicked:(UIButton*)btn {
    __weak typeof(self) ws = self;
    NSDictionary *param = @{
                            @"earning":self.maxAvailable,
                            };
    NSString *path = [@"/money/cashout/request?token=" stringByAppendingString:[AccountManager sharedAccountManager].account.token];
    [HTTPFacade postPath:path param:param body:nil successBlock:^(id  _Nonnull object) {
        [VLSUtily toastWithText:LocalizedString(@"load_completely") addtoview:self.view animation:YES duration:2];
        ws.maxAvailable = @"0";
        [ws.tableView reloadData];
        if ([ws.delegate_ respondsToSelector:@selector(withdrawConfirmDidSuccess:)]) {
            [ws.delegate_ withdrawConfirmDidSuccess:ws.maxAvailable];
        }
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        [VLSUtily toastWithText:error.info addtoview:self.view animation:YES duration:2];
    }];
}

#pragma mark - BindingCardViewControllerDelegate
- (void)bindingCardDidSuccess:(MoneybankModel*)model {
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [self checkStatus];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CGFloat height = 0;
    if (indexPath.section == 0) {
        height = 56;
    }else {
        if (indexPath.row == 0) {
            height = 100;
        }else {
            height = 56;
        }
    }
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    if (indexPath.section == 0) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cellid"];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:16];
        cell.detailTextLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        //TODO:国际化
        cell.textLabel.text = LocalizedString(@"to_card");
        cell.detailTextLabel.text = _bankModel.accountNumber ?: @"~";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            WithdrawConfirmCellTableViewCell *cell_ = (WithdrawConfirmCellTableViewCell*)[tableView dequeueReusableCellWithIdentifier:WithdrawConfirmCellTableViewCell_ID forIndexPath:indexPath];
            NSString *max = _taxRate == 0 ? @"~" : self.maxAvailable;
            cell_.maxAvailable = max;
            cell = cell_;
        }else {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellid"];
            cell.textLabel.font = [UIFont systemFontOfSize:14];
            cell.textLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
            cell.textLabel.text = [LocalizedString(@"cash_rule") stringByAppendingString:[_configModel.taxRate stringValue] ?: @"~"];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        BindingCardViewController *vc = [BindingCardViewController new];
        vc.delegate = self;
        vc.accountName = _bankModel.account;
        vc.accountNumber = _bankModel.accountNumber;
        vc.model = _bankModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
