//
//  UIView+gradient.h
//  VLiveShow
//
//  Created by tom.zhu on 16/9/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const GRADIENT_AXIS_HORIZON;
extern NSString * const GRADIENT_AXIS_VERTICAL;

@interface UIView (gradient)
/**
 *  View adding the Gradient layer
 *
 *  @param startPosition the position of beginning, default is 0
 *  @param endPosition   the position of end point, default is 1
 *  @param position      GRADIENT_AXIS_HORIZON or GRADIENT_AXIS_VERTICAL
 *  @param color         background color
 *  @param color         gradient layer color which has alphe channel
 *  @param time          animation duration
 *  @param progress      current position of boundary, from 0 ~ 1
 */
- (void)viewGradientWithStartPosition:(double)startPosition
                           endPositon:(double)endPosition
                             position:(NSString*)position
                      backgroundColor:(UIColor*)color
                            maskColor:(UIColor*)color
                             duration:(float)time
                             progress:(float)progress;
@end
