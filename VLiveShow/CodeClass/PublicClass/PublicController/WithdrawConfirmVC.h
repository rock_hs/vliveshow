//
//  WithdrawConfirmVC.h
//  Withdrawal
//
//  Created by tom.zhu on 16/8/12.
//  Copyright © 2016年 tom.zhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WithdrawConfirmDelegate <NSObject>
- (void)withdrawConfirmDidSuccess:(NSString*)maxAvailable;
@end

@interface WithdrawConfirmVC : UITableViewController
@property (nonatomic, assign) id<WithdrawConfirmDelegate> delegate_;
@property (nonatomic, strong) NSString *maxAvailable;//可提现的金额
@end
