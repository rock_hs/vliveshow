//
//  VLSUserProfileController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSUserProfileController.h"
#import "VLSUserRelationView.h"
#import "VLSMessageManager.h"
#import "VLSLiveHttpManager.h"
#import "VLSUserVideoActionView.h"
#import "HTTPFacade.h"
#import "AccountManager.h"
#import "VLSAVMultiManager.h"
#import "VLSAgoraVideoService.h"
#import "VLSUserInviteView.h"
#import "VLSAnchorViewController.h"

#define SC_WIDTH MIN(SCREEN_WIDTH, SCREEN_HEIGHT)
#define SC_HIGHT MAX(SCREEN_WIDTH, SCREEN_HEIGHT)

@interface VLSUserProfileController() <UIAlertViewDelegate>
{
    VLSMuteModel *muteModel;
}
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *infoContentView;
//自定义
@property (nonatomic, strong) UIButton *juBaoBtn;
@property (nonatomic, strong) UIButton *topJubaoBtn;

@property (nonatomic, strong) UILabel *laheiLabel;
@property (nonatomic, strong) UIButton *closeViewBtn;

@property (nonatomic, strong) UIButton *dumbButton;
@property (nonatomic, assign) BOOL isDumbd;         //是否被禁言

@property (nonatomic, strong) UIView *headImageBG;
@property (nonatomic, strong) UIImageView *headImageV;//头像
@property (nonatomic, strong) UIButton *topHeadbutton;

@property (nonatomic, strong) UILabel *userName;
@property (nonatomic, strong) UIImageView *sexImageView;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *locationLabel;
@property (nonatomic, strong) VLSUserRelationView *bottomView;
@property (nonatomic, strong) VLSUserActionView *btnBottomView;
@property (nonatomic, strong) VLSUserVideoActionView *videoAction;
@property (nonatomic, strong) VLSUserInviteView *inviteView;



@end

@implementation VLSUserProfileController
- (void)setIsDumbd:(BOOL)isDumbd {
    _isDumbd = isDumbd;
    if (isDumbd) {
        [self.dumbButton setTitle:LocalizedString(@"dumbd") forState:UIControlStateNormal];
        [self.dumbButton setTitle:LocalizedString(@"dumbd") forState:UIControlStateHighlighted];
        [self.dumbButton setImage:[UIImage imageNamed:@"ic_yijinyan"] forState:UIControlStateNormal];
        [self.dumbButton setImage:[UIImage imageNamed:@"ic_yijinyan"] forState:UIControlStateHighlighted];
    }else {
        [self.dumbButton setTitle:LocalizedString(@"dumb") forState:UIControlStateNormal];
        [self.dumbButton setTitle:LocalizedString(@"dumb") forState:UIControlStateHighlighted];
        [self.dumbButton setImage:[UIImage imageNamed:@"ic_jinyan"] forState:UIControlStateNormal];
        [self.dumbButton setImage:[UIImage imageNamed:@"ic_jinyan"] forState:UIControlStateHighlighted];
    }
    CGSize size = [self.dumbButton sizeThatFits:CGSizeMake(100, 50)];
    [self.dumbButton setSize_sd:size];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.view addSubview:self.contentView];
    [self.contentView addSubview:self.infoContentView];
    [self.infoContentView addSubview:self.headImageBG];
    [self.headImageBG addSubview:self.headImageV];

    //    [self.infoContentView addSubview:self.sexImageView];
    [self.infoContentView addSubview:self.juBaoBtn];

    [self.infoContentView addSubview:self.closeViewBtn];
    [self.infoContentView addSubview:self.laheiLabel];
    [self.infoContentView addSubview:self.dumbButton];
    [self.infoContentView addSubview:self.contentLabel];
    [self.infoContentView addSubview:self.locationLabel];
    [self.infoContentView addSubview:self.topJubaoBtn];

    [self.contentView addSubview:self.bottomView];
    [self.contentView addSubview:self.btnBottomView];
    [self.contentView addSubview:self.videoAction];
    [self.contentView addSubview:self.inviteView];
    [self.view addSubview:self.topHeadbutton];

    //     self.userName = [[UILabel alloc] initWithFrame:CGRectMake(20, self.contentView.frame.size.width/6 + 5, self.contentView.frame.size.width - 20 * 2, 30)];
    //    self.userName.font = [UIFont boldSystemFontOfSize:18];
    //    self.userName.textAlignment = NSTextAlignmentCenter;
    [self.infoContentView addSubview:self.userName];
    [self configur];
    [self loadData];
    
}

- (void)configur
{
    self.userName.text = self.userProfile.userName;
    
    [self.headImageV sd_setImageWithURL:[NSURL URLWithString:self.userProfile.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
    
    muteModel = [[VLSMuteModel alloc] init];
    muteModel.userId = self.userProfile.userId;
    
#pragma mark - 判断显示不同的控制按钮
    BOOL userIsSelf = [[VLSMessageManager sharedManager] checkUserIsSelf:self.userProfile.userId];
    BOOL userIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:self.userProfile.userId];
    BOOL userIsAudience = [[VLSMessageManager sharedManager] checkUserIsAudience:self.userProfile.userId];
    BOOL userIsGuests = [[VLSMessageManager sharedManager] checkUserIsGuests:self.userProfile.userId];
    BOOL selfIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:[VLSMessageManager sharedManager].host.userId];
    BOOL selfIsSuperUser = [AccountManager sharedAccountManager].account.isSuperUser;
    BOOL userMuteVoice = [[VLSMessageManager sharedManager] checkUserIsVoiceMute:self.userProfile.userId];
    BOOL userMuteVoiceSelf = [[VLSMessageManager sharedManager] checkUserIsVoiceMuteBySelf:self.userProfile.userId];
    
    if (userIsSelf) {
        self.juBaoBtn.hidden = YES;
        self.topJubaoBtn.hidden = YES;
        self.laheiLabel.hidden = YES;
        self.btnBottomView.hidden = YES;
        self.dumbButton.hidden = YES;
        //查看的是自己的信息
        if (userIsAnchor) {
            CGRect frame = self.videoAction.frame;
            frame.origin.y = frame.origin.y -60;
            self.videoAction.frame = frame;
            
            [self updateMuteBtStatus:userMuteVoice userMuteVoiceSelf:userMuteVoiceSelf];
            
            [self.videoAction.secondBtn setTitle:LocalizedString(@"ANCHOR_CLOSE") forState:UIControlStateNormal];
            
        }else if (userIsGuests){
            CGRect frame = self.videoAction.frame;
            frame.origin.y = frame.origin.y - 60;
            self.videoAction.frame = frame;
            [self updateMuteBtStatus:userMuteVoice userMuteVoiceSelf:userMuteVoiceSelf];
            [self.videoAction.secondBtn setTitle:LocalizedString(@"INVITE_GOTOCONNECT_END") forState:UIControlStateNormal];
        }else if (userIsAudience){
            self.videoAction.hidden= YES;
            self.bottomView.frame = CGRectMake(0, self.contentView.frame.size.height-180, self.contentView.frame.size.width, 60);
            self.bottomView.backgroundColor = [UIColor whiteColor];
        }

    }else{
        if (selfIsAnchor||selfIsSuperUser) {
            if (userIsAnchor && selfIsSuperUser) {
                //自己是超级用户看别人的信息
                [self updateMuteBtStatus:userMuteVoice userMuteVoiceSelf:userMuteVoiceSelf];
                [self.videoAction.secondBtn setTitle:LocalizedString(@"ANCHOR_CLOSE") forState:UIControlStateNormal];

            }else if (userIsGuests) {
                [self updateMuteBtStatus:userMuteVoice userMuteVoiceSelf:userMuteVoiceSelf];
                [self.videoAction.secondBtn setTitle:LocalizedString(@"INVITE_GOTOCONNECT_END") forState:UIControlStateNormal];

            }else{
                self.videoAction.hidden = YES;
                self.bottomView.hidden = NO;
//                self.inviteView.hidden = NO;
//                CGRect frame = self.inviteView.frame;
//                frame.origin.y = frame.origin.y;
//                self.inviteView.frame = frame;
                if (self.actionType == ActionTypeLessonTeacher) {
                    
                }else {
                    self.inviteView.hidden = NO;
                    CGRect frame = self.inviteView.frame;
                    frame.origin.y = frame.origin.y;
                    self.inviteView.frame = frame;
                }
            }
        }else{
            self.videoAction.hidden = YES;
        }
    }
}

//更新静音按钮状态
- (void)updateMuteBtStatus:(BOOL)userMuteVoice userMuteVoiceSelf:(BOOL)userMuteVoiceSelf
{
    BOOL selfIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:[VLSMessageManager sharedManager].host.userId];
    BOOL selfIsSuperUser = [AccountManager sharedAccountManager].account.isSuperUser;
    if (selfIsAnchor || selfIsSuperUser) {
        if (userMuteVoice) {//已静音
            if (!userMuteVoiceSelf) {//被自己静音
                self.videoAction.firstBtn.selected = YES;
            } else {//否则不让取消静音
                self.videoAction.firstBtn.enabled = NO;
            }
        }
    } else {
        if (userMuteVoice) {//已静音
            if (userMuteVoiceSelf) {//被自己静音
                self.videoAction.firstBtn.selected = YES;
            } else {//否则不让取消静音
                self.videoAction.firstBtn.enabled = NO;
            }
        }
    }
}

#pragma mark - 嘉宾静音
- (void)guestMuteVoice:(BOOL)mute
{
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:mute];

    //禁麦
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    callback.errorBlock = ^(id result){
        
    };
    [[VLSAVMultiManager sharedManager] geneusMuteSelf:mute callback:callback];
    
}

#pragma mark - 主播静音
- (void)muteVoice:(BOOL)mute
{
    //禁麦
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    callback.errorBlock = ^(id result){
        
    };
    [[VLSAVMultiManager sharedManager] anchorMuteModels:@[muteModel] callback:callback];
    
}

- (void)loadData
{
    
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        self.model = result;
        
        BOOL isattend = [self.model.isAttent boolValue] ;
        
        if (isattend == YES) {
            
            self.btnBottomView.jgzBtn.selected = YES;
            self.btnBottomView.jgzLabel.text = LocalizedString(@"MINE_USER_CANCELFOCURE");
        }else
        {
            self.btnBottomView.jgzBtn.selected = NO;
            self.btnBottomView.jgzLabel.text = LocalizedString(@"MINE_USER_FOCURE");
            
        }
        
        
        if (self.model.userContent == nil||[self.model.userContent isEqualToString:@" "])
        {
            self.contentLabel.text = LocalizedString(@"MINE_USER_PROFILE_LAZY_NOTHING");
        }else
        {
            self.contentLabel.text = self.model.userContent;

        }
        if (self.model.userLocation == nil) {
            self.locationLabel.text = [NSString stringWithFormat:@"ID:%@",self.model.userId];
            
        }else
        {
            self.locationLabel.text = [NSString stringWithFormat:@"%@  ID%@",self.model.userLocation,self.model.userId];
        }
        self.sexImageView.image = [UIImage imageNamed:self.model.sexImageName];
        
        NSMutableAttributedString *focuesNum = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %ld",LocalizedString(@"MINE_USER_FOCURE"),self.model.followNumber]];
        // 修改富文本中的不同文字的样式
        [focuesNum addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, 2)];
        NSString * sstr = [NSString stringWithFormat:@"%ld",self.model.followNumber];
        [focuesNum addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(2, sstr.length+1)];
        
        
        NSMutableAttributedString *fansNum = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %ld",LocalizedString(@"MINE_USER_FANCE"),self.model.fansNumber]];
        // 修改富文本中的不同文字的样式
        [fansNum addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, 2)];
        NSString * sstr1 = [NSString stringWithFormat:@"%ld",self.model.fansNumber];
        [fansNum addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(2, sstr1.length+1)];
        
        
        self.bottomView.focusNumberLab.attributedText = focuesNum;
        self.bottomView.fansNumberLab.attributedText = fansNum;
        
//        self.bottomView.friendNumberLab.text = [NSString stringWithFormat:@"%ld",(long)self.model.friendNumber];
        //        self.userName.text = viewModel.userName;
        
        NSMutableAttributedString *attri =     [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",self.model.userName]];
        // 添加表情
        NSTextAttachment* attch = [[NSTextAttachment alloc] init];
        // 设置图片大小
        // 表情图片
        if ([self.model.sexImageName isEqualToString:@"MALE"]) {
            attch.image = [UIImage imageNamed:@"mine_male_blue"];
            attch.bounds = CGRectMake(0, 0, 12, 12);

        }else if ([self.model.sexImageName isEqualToString:@"FEMALE"]){
            attch.image = [UIImage imageNamed:@"mine_female_red"];
            attch.bounds = CGRectMake(0, 0, 9, 12);

        }else{
            
            attch.image = [UIImage imageNamed:@""];
            attch.bounds = CGRectMake(0, 0, 0, 0);
        }
        
        // 创建带有图片的富文本
        NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:attch];
        [attri appendAttributedString:string];
        // 用label的attributedText属性来使用富文本
        self.userName.textAlignment = NSTextAlignmentCenter;
        self.userName.attributedText = attri;
        
    };
    callback.errorBlock = ^(id result)
    {
        [self error:result];
    };
    [VLSLiveHttpManager getUserInfo:[NSString stringWithFormat:@"%@",_userProfile.userId] callback:callback];
    
    BOOL selfIsSuperUser = [AccountManager sharedAccountManager].account.isSuperUser;
    if ([self.delegate isKindOfClass:[VLSAnchorViewController class]] || selfIsSuperUser) {
        WS(ws)
        NSDictionary *param = @{
                                @"token" : [AccountManager sharedAccountManager].account.token,
                                @"userId" : self.userProfile.userId,
                                @"roomId" : self.roomId,
                                };
        NSString *path = @"/liveshow/userInfo";
        [HTTPFacade getPath:path param:param successBlock:^(id  _Nonnull object) {
            if ([[object objectForKey:@"isForbidden"] boolValue]) {
                [ws setIsDumbd:true];
            }else {
                [ws setIsDumbd:false];
            }
            [ws.dumbButton setEnabled:true];
        } errorBlcok:^(ManagerEvent * _Nonnull error) {
            
        }];
    }
}

#pragma mark - 按钮事件

//关闭
- (void)closeClick:(UIButton *)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    //    [self hidden];
}

//举报用户
- (void)juBaoClick:(UIButton *)sender{
    
    
}
#pragma mark-关注
//关注事件
- (void)followClick:(UIButton *)sender{
    
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        [self loadData];
        BOOL userIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:self.userProfile.userId];
        if (userIsAnchor) {
            //回调到其他页面更新数据
            if ([self.delegate respondsToSelector:@selector(followClick:)]) {
                [self.delegate followClick:self.model.isAttent.boolValue];
            }
        }
    };
    callback.errorBlock = ^(id result)
    {
        [self error:result];
    };
    
    if ([self.model.isBlack boolValue] == YES)
    {
        ManagerEvent *event = [ManagerEvent new];
        event.title = LocalizedString(@"MINE_USER_CANTBEFOCUE");
        [self success:event];
        
    }else
    {
        
        if ([self.model.isAttent boolValue] == NO) {
            
            [[VLSUserTrackingManager shareManager] trackingFoucswithBeFocusedUserid:self.userProfile.userId action:@"focus_on"];
            
            [VLSLiveHttpManager focusToUser:self.userProfile.userId callback:callback];
        }
        else
        {
            [[VLSUserTrackingManager shareManager] trackingFoucswithBeFocusedUserid:self.userProfile.userId action:@"focus_off"];

            [VLSLiveHttpManager cancelFocusToUser:self.userProfile.userId callback:callback];
            
        }
    }
}
#pragma mark-主页
- (void)mainAction:(UIButton*)sender
{
    
    
    if ([self.delegate respondsToSelector:@selector(touchMain)]) {
        [self.delegate touchMain];
    }
    
}
#pragma mark- 拉黑
- (void)getBlack:(UIButton*)sener
{
    
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        BOOL selfIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:[VLSMessageManager sharedManager].host.userId];
        BOOL selfIsSuperUser = [AccountManager sharedAccountManager].account.isSuperUser;
        
        if (selfIsAnchor || selfIsSuperUser) {
            //                self.contentView.hidden = YES;
            [self dismissViewControllerAnimated:YES completion:nil];
            ManagerEvent *event = [ManagerEvent new];
            event.info = LocalizedString(@"ANCHORGETBLACK_SUCCESS");
            event.maxSize = CGSizeMake(200, 120);
            event.margin = 30;
            event.textfont = 16;
            MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.detailsLabelText = event.info;
            hud.minSize = event.maxSize;
            hud.margin = event.margin;
            hud.detailsLabelFont = [UIFont systemFontOfSize:event.textfont];
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
            
            [[VLSMessageManager sharedManager] sendBlack:YES toUser:self.userProfile.userId];
            
            [[VLSUserTrackingManager shareManager] trackingFoucswithBeFocusedUserid:self.userProfile.userId action:@"blacklist_on"];
        }
        
        [[VLSUserTrackingManager shareManager] trackingFoucswithBeFocusedUserid:self.userProfile.userId action:@"blacklist_on"];
        
        if ([self.delegate respondsToSelector:@selector(getBlack)]) {
            [self.delegate getBlack];
        }
        [self loadData];
        
    };
    callback.errorBlock = ^(id result)
    {
        [self error:result];
    };
    if ([self.model.isBlack boolValue] == NO) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"MINE_BLACKFRIENDS") message:LocalizedString(@"MINE_USER_PROFILE_ALERT_MESSAGE") preferredStyle:UIAlertControllerStyleAlert];
        // 确定按钮
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"MINE_USER_GETBLACK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {


        [VLSLiveHttpManager addBlack:self.userProfile.userId callback:callback];
        }];
        // 取消按钮
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"INVITE_CANCEL") style:UIAlertActionStyleCancel handler: nil];
        [alertController addAction:cancelAction];
        
        [alertController addAction:okAction];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else
    {
        ManagerEvent *event = [ManagerEvent new];
        event.title = LocalizedString(@"MINE_USER_INBLACK");
        [self success:event];
    }
}

- (void)firstClick:(UIButton *)sender
{
    
//    BOOL userIsSelf = [[VLSMessageManager sharedManager] checkUserIsSelf:self.userProfile.userId];
//    BOOL userIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:self.userProfile.userId];
//    BOOL userIsGuests = [[VLSMessageManager sharedManager] checkUserIsGuests:self.userProfile.userId];
    BOOL selfIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:[VLSMessageManager sharedManager].host.userId];
    BOOL selfIsSuperUser = [AccountManager sharedAccountManager].account.isSuperUser;
    BOOL userMuteVoice = [[VLSMessageManager sharedManager] checkUserIsVoiceMute:self.userProfile.userId];
//    BOOL userMuteVoiceSelf = [[VLSMessageManager sharedManager] checkUserIsVoiceMuteBySelf:self.userProfile.userId];
    self.videoAction.firstBtn.selected = !sender.selected;
    muteModel.muteVoice = !userMuteVoice;
    
    if (selfIsAnchor || selfIsSuperUser) {
        [self muteVoice:!sender.selected];
    } else {
        [self guestMuteVoice:muteModel.muteVoice];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)secondClick:(UIButton *)sender
{
    BOOL userIsSelf = [[VLSMessageManager sharedManager] checkUserIsSelf:self.userProfile.userId];
    BOOL userIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:self.userProfile.userId];
    BOOL userIsAudience = [[VLSMessageManager sharedManager] checkUserIsAudience:self.userProfile.userId];
    BOOL userIsGuests = [[VLSMessageManager sharedManager] checkUserIsGuests:self.userProfile.userId];
    BOOL selfIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:[VLSMessageManager sharedManager].host.userId];
    BOOL selfIsSuperUser = [AccountManager sharedAccountManager].account.isSuperUser;

    
    if (userIsSelf) {
        //查看的是自己的信息
        if (userIsAnchor) {
            [self anchorClose];
            //关闭直播
            //            [self.videoAction.secondBtn setTitle:@"关闭直播" forState:UIControlStateNormal];
            
        }else if (userIsGuests){
            [self guestCancelLink];
            //结束连线
            //            [self.videoAction.secondBtn setTitle:@"结束连线" forState:UIControlStateNormal];
        }else if (userIsAudience){
            
        }
    }else{
        if (selfIsAnchor||selfIsSuperUser) {
            if (userIsAnchor && selfIsSuperUser) {
                [self anchorClose];
            }else if (userIsGuests) {
                //自己是主播看别人的信息
                //主播取消嘉宾连线
                [self anchorCancelLink];
                //[self.videoAction.secondBtn setTitle:@"取消连线" forState:UIControlStateNormal];
            }
        }
    }
}

//主播邀请嘉宾
- (void)inviteAudience:(UIButton *)sender
{
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
    };
    callback.errorBlock = ^(id result){
        
    };
    [[VLSAVMultiManager sharedManager] inviteHonoredGuestWithInviteIds:@[self.userProfile.userId] callback:callback];
    [self dismissViewControllerAnimated:YES completion:nil];

}

//主播取消嘉宾连线
- (void)anchorCancelLink
{
    ManagerCallBack *callback1 = [[ManagerCallBack alloc] init];
    callback1.updateBlock = ^(id result){
        [self dismissViewControllerAnimated:YES completion:nil];
    };
    callback1.errorBlock = ^(id result){
        
    };
    if (_model != nil && _model.userId != nil) {
        [[VLSAVMultiManager sharedManager] anchorCancelLink:@[_model.userId] callback:callback1];
    }
}
//主播关闭直播
- (void)anchorClose
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([_delegate respondsToSelector:@selector(fromUserProfileAnchorClose)]) {
        [_delegate fromUserProfileAnchorClose];
    }
}
//嘉宾结束连线
- (void)guestCancelLink
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"INVITE_GOTOCONNECT_END") message:LocalizedString(@"INVITE_GOTOCONNECT_SURE_END") preferredStyle:UIAlertControllerStyleAlert];
    // 取消
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    // 确定
    UIAlertAction *sure = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        if ([_delegate respondsToSelector:@selector(fromUserProfileGuestCancelLink)]) {
            [_delegate fromUserProfileGuestCancelLink];
        }
    }];
    
    [alertController addAction:cancel];
    [alertController addAction:sure];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - getters

- (UIView *)contentView
{
    if (_contentView == nil) {
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH - 60 * 2, (SC_WIDTH - 50 * 2)/6+5+30+30+15+160)];
        _contentView.center = CGPointMake(self.view.center.x, self.view.center.y+35);
        _contentView.backgroundColor = [UIColor clearColor];
        //        _contentView.userInteractionEnabled = YES;
        
    }
    return _contentView;
}

- (UIView *)infoContentView
{
    if (_infoContentView == nil) {
        _infoContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, (SC_WIDTH - 50 * 2)/6+5+30+30+15)];
        _infoContentView.backgroundColor= [UIColor whiteColor];
    }
    return _infoContentView;
}

- (UIButton *)juBaoBtn
{
    if (_juBaoBtn == nil) {
        _juBaoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _juBaoBtn.frame = CGRectMake(10, 10, 20, 20);
        [_juBaoBtn setImage:[UIImage imageNamed:@"broadcast_public_brief_profile_report"] forState:UIControlStateNormal];
        [_juBaoBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
       
        _juBaoBtn.userInteractionEnabled = NO;
        
    }
    return _juBaoBtn;
}
- (UIButton*)topJubaoBtn
{
    if (_topJubaoBtn == nil) {
        _topJubaoBtn = [UIButton buttonWithType:0];
        _topJubaoBtn.backgroundColor = [UIColor clearColor];
        [_topJubaoBtn addTarget:self action:@selector(taptopjubaoAction) forControlEvents:UIControlEventTouchUpInside];
        _topJubaoBtn.frame = CGRectMake(0, 0, 60, 35);

        
    }
    return _topJubaoBtn;
}
- (void)taptopjubaoAction{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ANCHOR_PROFILE_REPORT") message:LocalizedString(@"ANCHOR_PROFILE_SURE_REPORT") preferredStyle:UIAlertControllerStyleAlert];
    //创建需要的按钮
    UIAlertAction *alertAction1 = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    //将UIAlertAction对象添加到UIAlertController上
    [alertController addAction:alertAction1];
    UIAlertAction *alertAction2 = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
        callback.updateBlock = ^(id result)
        {
            self.laheiLabel.text = LocalizedString(@"ANCHOR_PROFILE_HADREPORT");
            self.juBaoBtn.userInteractionEnabled = NO;
            self.topJubaoBtn.userInteractionEnabled = NO;
        };
        callback.errorBlock = ^(id result)
        {
            if ([result isKindOfClass:[ManagerEvent class]]) {
                [self error:result];
            }
        };
        
        [[VLSUserTrackingManager shareManager] trackingFoucswithBeFocusedUserid:self.userProfile.userId action:@"report"];
        
        [VLSLiveHttpManager postReport:self.userProfile.userId callback:callback type:nil content:nil remark:nil];
        //获取iOS的版本
        //        float version = [[[UIDevice currentDevice] systemVersion] floatValue];
        //        NSLog(@"%f", version);
    }];
    [alertController addAction:alertAction2];
    [self presentViewController:alertController animated:NO completion:nil];


}


- (UILabel *)laheiLabel
{
    if (_laheiLabel ==nil) {
        _laheiLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_juBaoBtn.frame) + 2, 10, 50, 20)];
        _laheiLabel.text = LocalizedString(@"ANCHOR_PROFILE_REPORT");
        _laheiLabel.textColor = [UIColor blackColor];
        _laheiLabel.font = [UIFont systemFontOfSize:13];
        _laheiLabel.userInteractionEnabled = YES;
    }
    return _laheiLabel;
}

- (UIButton *)dumbButton {
    if (nil == _dumbButton) {
        _dumbButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _dumbButton.adjustsImageWhenHighlighted = false;
        _dumbButton.frame = CGRectMake(_headImageBG.right_sd + 18, 7, 100, 30);
        [_dumbButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
        [_dumbButton setTitleColor:RGB16(COLOR_FONT_9B9B9B) forState:UIControlStateNormal];
        _dumbButton.backgroundColor = [UIColor clearColor];
        _dumbButton.enabled = false;
        [_dumbButton addTarget:self action:@selector(dumbButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _dumbButton;
}

- (void)dumbButtonClicked:(UIButton*)button {
    NSString *title = [self.dumbButton.titleLabel.text isEqualToString:LocalizedString(@"dumbd")] ? LocalizedString(@"forbidSendMsg_cancel_confirm") : LocalizedString(@"forbidSendMsg_confirm");
    NSString *cancel = [self.dumbButton.titleLabel.text isEqualToString:LocalizedString(@"dumbd")] ? LocalizedString(@"forbidSendMsg_cancel") : LocalizedString(@"YES");
    NSString *other = [self.dumbButton.titleLabel.text isEqualToString:LocalizedString(@"dumbd")] ? LocalizedString(@"miss_tap") : LocalizedString(@"VOTE_CONFIRM_CANCEL");
    UIAlertView *al = [[UIAlertView alloc] initWithTitle:title message:nil delegate:nil cancelButtonTitle:other otherButtonTitles:cancel, nil];
    al.delegate = self;
    [al show];
}

- (UIButton *)closeViewBtn
{
    if (_closeViewBtn == nil) {
        //关闭按钮
        _closeViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeViewBtn.frame = CGRectMake(self.contentView.frame.size.width - 39, -62, 39, 65);
        [_closeViewBtn setBackgroundImage:[UIImage imageNamed:@"but_bc_profile_close"] forState:UIControlStateNormal];
        [_closeViewBtn addTarget:self action:@selector(closeClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeViewBtn;
}

- (UIView *)headImageBG
{
    if (_headImageBG == nil) {
        _headImageBG = [[UIView alloc] initWithFrame:CGRectMake(3*self.contentView.frame.size.width / 10, -self.contentView.frame.size.width / 5,self.contentView.frame.size.width / 2.5, self.contentView.frame.size.width / 2.5)];
        _headImageBG.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:0.4];
        _headImageBG.layer.cornerRadius = self.contentView.frame.size.width / 5;
    }
    return _headImageBG;
}

- (UIImageView *)headImageV
{
    if (_headImageV == nil) {
        _headImageV = [[UIImageView alloc] init];
        _headImageV.frame = CGRectMake(4,4,(self.contentView.frame.size.width / 2.5)-8 ,(self.contentView.frame.size.width / 2.5) - 8);
        _headImageV.layer.cornerRadius = ((self.contentView.frame.size.width / 5)-4);
        _headImageV.contentMode = UIViewContentModeScaleAspectFill;
        _headImageV.clipsToBounds = YES;
       //        _headImageV.backgroundColor = [UIColor clearColor];
    }
    return _headImageV;
}
- (UIButton*)topHeadbutton
{
    if (_topHeadbutton == nil) {
        _topHeadbutton = [UIButton buttonWithType:0];
        _topHeadbutton.backgroundColor = [UIColor clearColor];
        _topHeadbutton.frame = CGRectMake(self.contentView.frame.size.width / 3+self.contentView.frame.origin.x, self.contentView.frame.origin.y-self.contentView.frame.size.width / 6,self.contentView.frame.size.width / 3, self.contentView.frame.size.width / 3);
        [_topHeadbutton addTarget:self action:@selector(tapAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _topHeadbutton;
}


- (void)tapAction
{
    
    if ([self.delegate respondsToSelector:@selector(touchMain)]) {
        [self.delegate touchMain];
    }

}

- (UILabel *)userName
{
    if (_userName == nil) {
        _userName = [[UILabel alloc] initWithFrame:CGRectMake(20, self.contentView.frame.size.width/5 + 5, self.contentView.frame.size.width - 20 * 2, 30)];
        _userName.font = [UIFont boldSystemFontOfSize:20];
//                _userName.backgroundColor = [UIColor redColor];
        _userName.textAlignment = NSTextAlignmentCenter;
    }
    return _userName;
}

- (UIImageView *)sexImageView
{
    if (_sexImageView == nil) {
        _sexImageView = [[UIImageView alloc] initWithFrame:CGRectMake( self.contentView.frame.size.width - 50, self.contentView.frame.size.width/5 + 8, 20, 20)];
        _sexImageView.image = [UIImage imageNamed:LocalizedString(@"MINE_PERSONAL_MALE")];
//        _sexImageView.backgroundColor = [UIColor lightGrayColor];
    }
    return _sexImageView;
}

- (UILabel *)contentLabel
{
    if (_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, self.contentView.frame.size.width/5 + 5+30+25 , self.contentView.frame.size.width - 20, 15)];
        _contentLabel.font = [UIFont systemFontOfSize:13];
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        _contentLabel.textAlignment = NSTextAlignmentCenter;
        _contentLabel.backgroundColor = [UIColor clearColor];
    }
    return _contentLabel;
}
- (UILabel*)locationLabel
{
    if(_locationLabel == nil) {
        _locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, self.contentView.frame.size.width/5 + 5+25 , self.contentView.frame.size.width - 20, 30)];
        _locationLabel.font = [UIFont systemFontOfSize:13];
        _locationLabel.numberOfLines = 0;
        _locationLabel.textAlignment = NSTextAlignmentCenter;
        _locationLabel.textColor = [UIColor grayColor];
//        _locationLabel.backgroundColor = [UIColor greenColor];
    }
    return _locationLabel;
    
    
}
- (VLSUserRelationView *)bottomView
{
    if (_bottomView == nil) {
        _bottomView = [[VLSUserRelationView alloc] initWithFrame:CGRectMake(0, self.contentView.frame.size.height-160, self.contentView.frame.size.width, 40)];
//        _bottomView.backgroundColor = [UIColor cyanColor];
    }
    return _bottomView;
}

- (VLSUserActionView *)btnBottomView
{
    if (_btnBottomView == nil) {
        _btnBottomView = [[VLSUserActionView alloc] initWithFrame:CGRectMake(0, self.contentView.frame.size.height-120, self.contentView.frame.size.width, 60)];
//        _btnBottomView.backgroundColor = [UIColor grayColor];
        /**
         *  按钮添加事件
         */
        [self.btnBottomView.thirdTopBut addTarget:self action:@selector(followClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnBottomView.secondTopBut addTarget:self action:@selector(mainAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnBottomView.firstTopBut addTarget:self action:@selector(getBlack:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnBottomView;
}

- (VLSUserVideoActionView *)videoAction
{
    if (_videoAction == nil) {
        _videoAction = [[VLSUserVideoActionView alloc] initWithFrame:CGRectMake(0, self.contentView.frame.size.height-60, self.contentView.frame.size.width, 60)];
        [_videoAction.firstBtn addTarget:self action:@selector(firstClick:) forControlEvents:UIControlEventTouchUpInside];
        [_videoAction.secondBtn addTarget:self action:@selector(secondClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return _videoAction;
}

- (VLSUserInviteView *)inviteView
{
    if (_inviteView == nil) {
        _inviteView = [[VLSUserInviteView alloc] initWithFrame:CGRectMake(0, self.contentView.frame.size.height-60, self.contentView.frame.size.width, 60)];
        _inviteView.hidden = YES;
        [_inviteView.invitBtn addTarget:self action:@selector(inviteAudience:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _inviteView;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [touches anyObject];
    CGPoint currentLocation = [touch locationInView:self.view];
    if (!CGRectContainsPoint(self.contentView.frame, currentLocation)) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) return;
    WS(ws)
    NSNumber *shutUpTime = [self.dumbButton.titleLabel.text isEqualToString:LocalizedString(@"dumbd")] ? @0 : @2147483647;
    NSString *path = [NSString stringWithFormat:@"/liveshow/forbidSendMsg?token=%@", [[[AccountManager sharedAccountManager] account] token]];
    NSDictionary *params = @{
                             @"roomId" : self.roomId,
                             @"shutUpTime" : shutUpTime,
                             @"userIds" : self.userProfile.userId,
                             };
    [HTTPFacade postPath:path param:params body:nil successBlock:^(id  _Nonnull object) {
        if ([shutUpTime integerValue] == 0) {
            [ws setIsDumbd:false];
        }else {
            [ws setIsDumbd:true];
        }
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        
    }];
}

@end
