//
//  VLSRecomModel.h
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSRecomModel : NSObject

@property (nonatomic ,strong) NSString *avatars;//解析头像
@property (nonatomic, copy) NSString *nickName;//解析的用户名
@property (nonatomic, strong) NSString *ID;//解析id
@property (nonatomic, copy) NSString *intro;
@property (nonatomic,copy) NSString * is_both_attend;
@property (nonatomic, copy) NSString *isLiving;
@property (nonatomic, copy)NSString *gender;
@property (nonatomic, copy)NSString *level;
@property (nonatomic) BOOL isPress;

@end
