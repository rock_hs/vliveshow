//
//  VLSSeachResultModel.h
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSSeachResultModel : NSObject

@property (nonatomic, strong) NSString *avatars;//解析头像
@property (nonatomic, copy) NSString *nickname;//解析的用户名
@property (nonatomic, copy) NSString *intro;
@property (nonatomic, strong) NSString *ID;//解析id
@property (nonatomic, copy) NSString * is_both_attend;
@property (nonatomic, assign) BOOL isPress;
@property (nonatomic, copy) NSString *living;
@property (nonatomic, copy)NSString *level;
@property (nonatomic, copy)NSString *relation;
@property (nonatomic, copy)NSString *createtime;
@property (nonatomic, copy)NSString *updatetime;

@end
