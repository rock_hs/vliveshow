//
//  VLSFocusViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/8/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSFocusViewController.h"
#import "VLSNoFocusListView.h"
#import "VLSLiveHttpManager.h"
#import "VLSRecomFocusViewController.h"
#import "VLSSearchViewController.h"
#import "VLSLiveListModel.h"
#import "VLSFocusManLivingView.h"
#import "VLSAdminViewController.h"
#import "VLSShowViewController.h"
#import "VLSAnchorViewController.h"
#import <MJRefresh/MJRefresh.h>
#import "VLiveShow-Swift.h"
#import "VLSCourseDetailViewController.h"

@interface VLSFocusViewController ()<VLSFocusManLivingViewDelegate>
{
    NSInteger page;
    BOOL isUP;
}
@property (nonatomic, strong)UIButton *goHomeBtn;
@property (nonatomic, strong)VLSNoFocusListView *noFocusListView;
@property (nonatomic, strong)NSMutableArray *livingLists;
@property (nonatomic, strong)VLSFocusManLivingView *livingView;
@property (nonatomic, copy) void (^enterLiveBlock) (NSString *courseId, NSIndexPath *indexPath);     //进入直播间方法

@end

@implementation VLSFocusViewController
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firsttouchfouch"];
    [self.livingLists removeAllObjects];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setTranslucent: NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage ImageFromColor:[UIColor whiteColor]] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:119/255.f green:119/255.f blue:119/255.f alpha:1];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_17],
       NSForegroundColorAttributeName:[UIColor colorWithRed:119/255.f green:119/255.f blue:119/255.f alpha:1]}];
    page = 0;
    [self getFocusLists];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB16(COLOR_BG_FFFFFF);
    [self initNavigationBar];
    [self initSubViews];
    [self refreshLivingLists];
    
    WS(ws)
    self.enterLiveBlock = ^(NSString *courseId, NSIndexPath *indexPath){
        VLSLiveListModel *model = self.livingView.livingArr[indexPath.row];
        [VLSUtily isSubscription:model.courseId handle:^(id result, ManagerEvent *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [ws hideProgressHUDAfterDelay:.5f];
            });
            if ([[result objectForKey:@"isParticipant"] boolValue] || [[result objectForKey:@"isCourseVipUser"] boolValue] || [[result objectForKey:@"isSubCourse"] boolValue]) {
                //连线嘉宾,VIP，观众
                UIViewController *vc = nil;
                AccountModel *account = [AccountManager sharedAccountManager].account;
                if (account.isSuperUser) {
                    VLSAdminViewController *admin = [[VLSAdminViewController alloc] init];
                    admin.anchorId = model.host;
                    admin.roomId = model.roomId;
                    admin.orientation = [model.orientation isEqualToNumber:@2] ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
                    admin.courseId = model.roomId;
                    admin.lessonId = model.lessonId;
                    vc = admin;
                }else {
                    VLSShowViewController *show = [[VLSShowViewController alloc] init];
                    show.anchorId = model.host;
                    show.roomId = model.roomId;
                    show.orientation = [model.orientation isEqualToNumber:@2] ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
                    show.courseId = model.courseId;
                    show.lessonId = model.lessonId;
                    vc = show;
                }
                vc.hidesBottomBarWhenPushed = YES;
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    // code here
//                    if (self.navigationController.viewControllers.count > 1) {
//                        return ;
//                    }
                    [self.navigationController pushViewController:vc animated:YES];
//                });
            }else {
                //没有权限进直播间
                VLSCourseModel *courseModel = [VLSCourseModel new];
                courseModel.courseId = @([model.courseId integerValue]);
                VLSCourseDetailViewController *vc = [[VLSCourseDetailViewController alloc] initWithNibName:@"VLSCourseDetailViewController" bundle:nil];
                vc.state = course_after;
                vc.model = courseModel;
                vc.hidesBottomBarWhenPushed = true;
                [ws.navigationController pushViewController:vc animated:YES];
            }
        }];
    };
}


- (void)initNavigationBar {
    self.navigationItem.title = LocalizedString(@"MINE_FOCUS");
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"but_search_tri"] style:UIBarButtonItemStylePlain target:self action:@selector(searchAction:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"but_add"] style:UIBarButtonItemStylePlain target:self action:@selector(recommendAction:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

- (void)initSubViews {
    
}

- (void)refreshLivingLists {
    // 上拉加载
//    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        page++;
//        isUP = YES;
//        [self getFocusLists];
//    }];
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        page++;
        isUP = YES;
        [self getFocusLists];
    }];
    [footer setTitle:LocalizedString(@"LIVE_PULL_LOAD_MORE") forState:MJRefreshStateRefreshing];
    self.livingView.livingTableView.mj_footer = footer;
    
    // 下拉刷新
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        page = 0;
        isUP = NO;
        [self getFocusLists];
        
    }];
    self.livingView.livingTableView.mj_header = header;
    
}

- (void)searchAction:(UIBarButtonItem *)item {
    // 搜索
    VLSSearchViewController *searchVC = [[VLSSearchViewController alloc] init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)recommendAction:(UIBarButtonItem *)item {
    // 推荐关注
    VLSRecomFocusViewController *recommendVC = [[VLSRecomFocusViewController alloc] init];
    [self.navigationController pushViewController:recommendVC animated:YES];
}

- (void)showNoFocusListViewEnable:(BOOL)enable AndIsNoLiving:(BOOL)noLiving {
    if (enable) {
        
        [self.livingView removeFromSuperview];
        
        [self.view addSubview:self.goHomeBtn];
        self.goHomeBtn.sd_layout
        .bottomSpaceToView(self.view, Y_CONVERT(246))
        .centerXEqualToView(self.view)
        .widthIs(X_CONVERT(240))
        .heightIs(Y_CONVERT(38));
        
        [self.view addSubview:self.noFocusListView];
        self.noFocusListView.sd_layout
        .leftSpaceToView(self.view, 0)
        .rightSpaceToView(self.view, 0)
        .topSpaceToView(self.view, 0)
        .bottomSpaceToView(self.goHomeBtn, Y_CONVERT(34));
        
        // 显示对应图片
        [self.noFocusListView showNoFocusManOrNoLiving:noLiving];
        
    } else {
        [self.goHomeBtn removeFromSuperview];
        [self.noFocusListView removeFromSuperview];
    }
}

- (void)goHomeAction:(UIButton *)button {
    
     self.tabBarController.selectedIndex = 0;
}

- (void)getFocusLists {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if (!isUP) {
            [self.livingLists removeAllObjects];
        }
        [self.livingView.livingTableView.mj_header endRefreshing];
        [self.livingView.livingTableView.mj_footer endRefreshing];
        
        NSInteger follwingCount = [[result valueForKey:@"follwingCount"] integerValue];
        if (follwingCount) {
            // 有关注结果
            NSInteger totalCount = [result[@"totalCount"] integerValue];
            if (totalCount) {
                // 有人直播
                NSArray *list = [VLSHotModel mj_objectArrayWithKeyValuesArray:result[@"list"]];
                for (int i=0 ; i<list.count; i++) {
                    VLSLiveListModel *model = [[VLSLiveListModel alloc] initWidthHotModel:[list objectAtIndex:i]];
                    [self.livingLists addObject:model];
                }
                
                [self.livingView setDataSourceWithArray:self.livingLists];
                [self.view addSubview:self.livingView];
                [self.noFocusListView removeFromSuperview];
                
            } else {
                [self showNoFocusListViewEnable:YES AndIsNoLiving:YES];
            }
        } else {
        // 无关注结果
            BOOL firstfouce = [[NSUserDefaults standardUserDefaults] boolForKey:@"firsttouchfouch"];
            if (firstfouce==YES) {
                [self showNoFocusListViewEnable:YES AndIsNoLiving:NO];
            }else
            {
                VLSRecomFocusViewController *recommendVC = [[VLSRecomFocusViewController alloc] init];
                [self.navigationController pushViewController:recommendVC animated:NO];

            }
        }
        
    };
    callback.errorBlock = ^(id result){
        [self.livingView.livingTableView.mj_header endRefreshing];
        [self.livingView.livingTableView.mj_footer endRefreshing];
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [VLSLiveHttpManager getFocusList:page userid:[AccountManager sharedAccountManager].account.uid callback:callback];
}

#pragma mark - VLSFocusManLivingViewDelegate

- (void)enterIntoLivingRoom:(NSIndexPath *)indexPath {
    [self ToTheLiveShow:indexPath];
}

#pragma mark - 跳转进直播间
- (void)ToTheLiveShow:(NSIndexPath *)indexPath
{
    // 在此判断 IM 是否已经登录，如果登录成功进行，再进行跳转
    if (![[VLSMessageManager sharedManager] GetLoginState]) return;
    
    VLSLiveListModel *model = self.livingView.livingArr[indexPath.row];
    AccountModel *account = [AccountManager sharedAccountManager].account;
    if (account.isSuperUser) {
        if (model.lessonId) {
            self.enterLiveBlock(model.courseId, indexPath);
        }else {
            VLSAdminViewController *admin = [[VLSAdminViewController alloc] init];
            admin.orientation = [model.orientation isEqualToNumber:@2] ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
            admin.anchorId = model.host;
            admin.roomId = model.roomId;
            admin.hidesBottomBarWhenPushed = YES;
//            dispatch_async(dispatch_get_main_queue(), ^{
//                // code here
//                if (self.navigationController.viewControllers.count > 1) {
//                    return ;
//                }
                [self.navigationController pushViewController:admin animated:YES];
//            });
        }
    }else{
        if (account.uid == [model.host integerValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                [self intoAnchorVC:model];
                
            });
        }else{
            /*
            VLSShowViewController *show = [[VLSShowViewController alloc] init];
            show.anchorId = model.host;
            show.roomId = model.roomId;
            show.hidesBottomBarWhenPushed = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                if (self.navigationController.viewControllers.count > 1) {
                    return ;
                }
                [self.navigationController pushViewController:show animated:YES];
            });
            */
            if (model.courseId == nil || [model.courseId length] == 0) {
                VLSShowViewController *show = [[VLSShowViewController alloc] init];
                show.anchorId = model.host;
                show.roomId = model.roomId;
                show.orientation = [model.orientation isEqualToNumber:@2] ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
                show.hidesBottomBarWhenPushed = YES;
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    // code here
//                    if (self.navigationController.viewControllers.count > 1) {
//                        return ;
//                    }
                    [self.navigationController pushViewController:show animated:YES];
//                });
            }else {
                WS(ws)
                //判断用户权限
                [ws showProgressHUD];
                self.enterLiveBlock(model.courseId, indexPath);
            }
        }
    }
}

- (void)intoAnchorVC:(VLSLiveListModel *)model
{
    VLSAnchorViewController *anchorVC = [[VLSAnchorViewController alloc] init];
    [VLSIMManager sharedInstance].RoomID = model.roomId;
    anchorVC.roomId = model.roomId;
    anchorVC.anchorId = model.host;
    anchorVC.isReconnection = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:anchorVC];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark - getters

- (UIButton *)goHomeBtn {
    if (!_goHomeBtn) {
        self.goHomeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.goHomeBtn.layer.borderColor = RGB16(COLOR_BG_FF1130).CGColor;
        self.goHomeBtn.layer.borderWidth = 1.;
        self.goHomeBtn.layer.cornerRadius = X_CONVERT(20.);
        self.goHomeBtn.layer.masksToBounds = YES;
        [self.goHomeBtn setTitle:LocalizedString(@"FOCUS_GO_HOME") forState:UIControlStateNormal];
        self.goHomeBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        [self.goHomeBtn setTitleColor:RGB16(COLOR_BG_FF1130) forState:UIControlStateNormal];
        [self.goHomeBtn addTarget:self action:@selector(goHomeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _goHomeBtn;
}

- (VLSNoFocusListView *)noFocusListView {
    if (!_noFocusListView) {
        self.noFocusListView = [[VLSNoFocusListView alloc] init];
    }
    return _noFocusListView;
}

- (NSMutableArray *)livingLists {
    if (!_livingLists) {
        self.livingLists = [NSMutableArray array];
    }
    return _livingLists;
}

- (VLSFocusManLivingView *)livingView {
    if (!_livingView) {
        _livingView = [[VLSFocusManLivingView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - 49)];
        _livingView.delegate = self;
    }
    return _livingView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
