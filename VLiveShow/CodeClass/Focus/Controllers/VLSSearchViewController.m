//
//  VLSSearchViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSSearchViewController.h"
#import "VLSSearchInputView.h"
#import "VLSSearchHistoryView.h"
#import "VLSNoSearchResultView.h"
#import "VLSSearchResultView.h"
#import "VLSOthersViewController.h"

@interface VLSSearchViewController ()<VLSSearchInputViewDelegate, VLSSearchHistoryViewDelegate, VLSNoSearchResultViewDelegate, VLSSearchResultViewDelegate>

@property (nonatomic, strong)VLSSearchInputView *searchView;
@property (nonatomic, strong)VLSSearchHistoryView *historyView;
@property (nonatomic, strong)VLSNoSearchResultView *noResultView;
@property (nonatomic, strong)VLSSearchResultView *searchResultView;


@end

@implementation VLSSearchViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initNavigationBar];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB16(COLOR_BG_FFFFFF);
    [self initSubViews];
    
}


- (void)initNavigationBar {
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarStyle: UIBarStyleDefault];
    [self.navigationController.navigationBar setTranslucent: NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage ImageFromColor:[UIColor whiteColor]] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:119/255.f green:119/255.f blue:119/255.f alpha:1];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_17],
       NSForegroundColorAttributeName:[UIColor colorWithRed:119/255.f green:119/255.f blue:119/255.f alpha:1]}];
    
    UIImage *image = [[UIImage imageNamed:@"but_search_tri"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:image]];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"CANCLE") style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction:)];
    [rightItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                       [UIFont systemFontOfSize:SIZE_FONT_16],NSFontAttributeName, nil]
                             forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
    self.navigationItem.titleView = self.searchView;
    [self.navigationItem.leftBarButtonItem setTitlePositionAdjustment:UIOffsetMake(-10, -10) forBarMetrics:UIBarMetricsDefault];
}

- (void)initSubViews {
    
    
    
}

#pragma mark - VLSSearchInputViewDelegate

- (void)showSearchHistoryViewWithArray:(NSArray *)array {
    [self.searchResultView removeFromSuperview];
    [self.historyView setHistoryDataWithArray:array];
    [self.view addSubview:self.historyView];
}

- (void)showNoSearchResultViewWithKeyWordString:(NSString *)keyWord {
    [self.noResultView setNoResultViewContentWithKeyWord:keyWord];
    [self.searchView endEditing:YES];
    [self.historyView removeFromSuperview];
    [self.view addSubview:self.noResultView];
}

- (void)showSearchResultTableViewWithArray:(NSMutableArray *)lists AndRefreshKeyWord:(NSString *)keyword {
    [self.searchResultView setDataSourseWithResultArray:lists];
    [self.searchResultView setRefreshKeyWordWithString:keyword];
    [self.view addSubview:self.searchResultView];
    [self.noResultView removeFromSuperview];
    [self.historyView removeFromSuperview];
    [self.searchView endEditing:YES];
}

#pragma mark - VLSSearchHistoryViewDelegate
- (void)clearSearchHistory {
    [self.historyView removeFromSuperview];
    [self.searchView clearSearchHistoryData];
}
- (void)selectSearchHistoryWithString:(NSString *)historyStr {
    [self.searchView becomeFirstResponse];
    [self.searchView showSelectedHistoryString:historyStr];
}

#pragma mark - VLSNoSearchResultViewDelegate
- (void)noSearchResultAndGoHome {
    self.tabBarController.selectedIndex = 0;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - VLSSearchResultViewDelegate

- (void)clickHeadBtnAndPushVC:(NSMutableArray *)array and:(UITableViewCell *)cell with:(UITableView *)tableView {
    NSIndexPath *indpath = [tableView indexPathForCell:cell];
    MineModel *model = [array objectAtIndex:indpath.row];
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    vlsVC.hidesBottomBarWhenPushed = YES;
    [[VLSUserTrackingManager shareManager] trackingViewprofile:model.ID];
    vlsVC.userId = model.ID;
    [self.navigationController pushViewController:vlsVC animated:YES];
}

- (void)cancelAction:(UIBarButtonItem *)item {
    // 取消返回
    [self.searchView endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.searchView endEditing:YES];
}

#pragma mark - getters

- (VLSSearchInputView *)searchView {
    if (!_searchView) {
        self.searchView = [[VLSSearchInputView alloc] initWithFrame:CGRectMake(0, Y_CONVERT(0), X_CONVERT(260), Y_CONVERT(30))];
        self.searchView.delegate = self;
        
    }
    return _searchView;
}

- (VLSSearchHistoryView *)historyView {
    if (!_historyView) {
        self.historyView = [[VLSSearchHistoryView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, Y_CONVERT(200))];
        self.historyView.delegate = self;
    }
    return _historyView;
}
- (VLSNoSearchResultView *)noResultView {
    if (!_noResultView) {
        _noResultView = [[VLSNoSearchResultView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, Y_CONVERT(44))];
        _noResultView.delegate = self;
    }
    return _noResultView;
}

- (VLSSearchResultView *)searchResultView {
    if (!_searchResultView) {
        _searchResultView = [[VLSSearchResultView alloc] initWithFrame:self.view.bounds];
        _searchResultView.delegate = self;
    }
    return _searchResultView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
