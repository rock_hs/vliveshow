//
//  VLSRecomFocusViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSRecomFocusViewController.h"
#import "VLSSearchViewController.h"
#import "VLSLiveHttpManager.h"
#import "VLSRecomView.h"
#import "VLSOthersViewController.h"

@interface VLSRecomFocusViewController ()<VLSRecomViewDelegate>

@property (nonatomic, strong)VLSRecomView *recomView;

@end

@implementation VLSRecomFocusViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initNavigationBar];
    [self getRecommendLists];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB16(COLOR_BG_FFFFFF);
    [self initSubViews];

}



- (void)initNavigationBar {
    self.navigationItem.title = LocalizedString(@"MINE_RECOMMEND_FOCUS");
    self.navigationController.navigationBarHidden = NO;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"but_search_tri"] style:UIBarButtonItemStylePlain target:self action:@selector(searchAction:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"CANCLE") style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction:)];
    [rightItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont systemFontOfSize:SIZE_FONT_16],NSFontAttributeName, nil]
                              forState:UIControlStateNormal];
 
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

- (void)initSubViews {
    
}

- (void)getRecommendLists {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        [self.recomView setDataSourseWithResultArray:result];
        [self.view addSubview:self.recomView];
        
    };
    callback.errorBlock = ^(id result){
        
    };
    
    [VLSLiveHttpManager getRecommedLists:callback];
    

}
- (void)searchAction:(UIBarButtonItem *)item {
    // 搜索
    VLSSearchViewController *searchVC = [[VLSSearchViewController alloc] init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)cancelAction:(UIBarButtonItem *)item {
    // 取消返回
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mamrk - VLSRecomViewDelegate

- (void)clickHeadBtnAndPushVC:(NSMutableArray *)array and:(UITableViewCell *)cell with:(UITableView *)tableView {
    NSIndexPath *indpath = [tableView indexPathForCell:cell];
    MineModel *model = [array objectAtIndex:indpath.row];
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    vlsVC.hidesBottomBarWhenPushed = YES;
    [[VLSUserTrackingManager shareManager] trackingViewprofile:model.ID];
    
    vlsVC.userId = model.ID;
    [self.navigationController pushViewController:vlsVC animated:YES];
}

#pragma mark - getters

- (VLSRecomView *)recomView {
    if (!_recomView) {
        _recomView = [[VLSRecomView alloc] initWithFrame:self.view.bounds];
        _recomView.delegate = self;
    }
    return _recomView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
