//
//  VLSSearchInputView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSSearchInputView.h"
#import "VLSLiveHttpManager.h"

@interface VLSSearchInputView ()<UITextFieldDelegate>

@property (nonatomic, strong)UITextField *searchTF;
@property (nonatomic, strong)NSMutableArray *archiveArr;
@property (nonatomic, strong)NSArray *unarchiveArr;
@property (nonatomic, strong)NSString *filePath;

@end

@implementation VLSSearchInputView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doInit];
        
    }
    return self;
}

- (void)doInit {

    [self addSubview:self.searchTF];
    self.searchTF.sd_layout
    .leftSpaceToView(self, -10)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, -5);

}



- (void)searchTFEditing {
   
    
}

- (void)showSelectedHistoryString:(NSString *)historyStr {
    self.searchTF.text = historyStr;
}
- (void)becomeFirstResponse {
    [self.searchTF becomeFirstResponder];
}
- (void)recordSearchHistoryAndPerformSearchAPI {
    
    if (!self.searchTF.text.length) return;
    
    [self storageSearchHistory];
    
    // 呼叫搜索的 API
    [self getSearchUserLists];
    
}

- (void)getSearchUserLists {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        NSMutableArray *array = result;
        if (!array.count) {
            // 没有搜索结果时执行代理
            if ([self.delegate respondsToSelector:@selector(showNoSearchResultViewWithKeyWordString:)]) {
                [self.delegate showNoSearchResultViewWithKeyWordString:self.searchTF.text];
            }
        } else {
            // 有搜索结果
            if ([self.delegate respondsToSelector:@selector(showSearchResultTableViewWithArray:AndRefreshKeyWord:)]) {
                [self.delegate showSearchResultTableViewWithArray:array AndRefreshKeyWord:self.searchTF.text];
            }
        }
        
    };
    callback.errorBlock = ^(id result){
        
    };
    
    [VLSLiveHttpManager searchUserWithPage:0 size:20 keyword:self.searchTF.text callback:callback];

}


- (void)storageSearchHistory {
    
     [self.archiveArr insertObject:self.searchTF.text atIndex:0];
    if (self.archiveArr.count > 3) {
        self.archiveArr = [(NSMutableArray *)[self.archiveArr subarrayWithRange:NSMakeRange(0, 3)] mutableCopy];
    }
    
    [NSKeyedArchiver archiveRootObject:self.archiveArr toFile:self.filePath];

}

- (void)clearSearchHistoryData {
    [self.archiveArr removeAllObjects];
    [NSKeyedArchiver archiveRootObject:self.archiveArr toFile:self.filePath];
}
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self recordSearchHistoryAndPerformSearchAPI];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.unarchiveArr = [NSKeyedUnarchiver unarchiveObjectWithFile:self.filePath];
    if (self.unarchiveArr.count) {
        if ([self.delegate respondsToSelector:@selector(showSearchHistoryViewWithArray:)]) {
            [self.delegate showSearchHistoryViewWithArray:self.unarchiveArr];
        }
    }
    return YES;
}

#pragma mark - getters

- (UITextField *)searchTF {
    if (!_searchTF) {
        self.searchTF = [[UITextField alloc] init];
        self.searchTF.delegate = self;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_14]};
        self.searchTF.tintColor = RGB16(COLOR_LINE_FF1130);
        self.searchTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"FOCUS_SEARCH_PLACEHOLDER") attributes:attributes];
        self.searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.searchTF.returnKeyType = UIReturnKeySearch;
        [self.searchTF addTarget:self action:@selector(searchTFEditing) forControlEvents:UIControlEventEditingChanged];
        
    }
    return _searchTF;
}

- (NSMutableArray *)archiveArr {
    if (!_archiveArr) {
        self.archiveArr = [NSMutableArray array];
        NSObject *ob = [NSKeyedUnarchiver unarchiveObjectWithFile:self.filePath];
        if (ob) {
            self.archiveArr = [ob mutableCopy];
        }

    }
    
    return _archiveArr;
}

- (NSArray *)unarchiveArr {
    if (!_unarchiveArr) {
        self.unarchiveArr = [NSArray array];
    }
    return _unarchiveArr;
}

- (NSString *)filePath {
    if (!_filePath) {
        NSString *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject;
       
        self.filePath = [path stringByAppendingPathComponent:@"search.data"];
       
    }
    return _filePath;
}


@end
