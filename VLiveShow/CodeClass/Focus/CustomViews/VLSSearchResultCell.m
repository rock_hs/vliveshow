//
//  VLSSearchResultCell.m
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSSearchResultCell.h"


@implementation VLSSearchResultCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {

        [self.focusBtn addTarget:self action:@selector(focusBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.headBtn addTarget:self action:@selector(headBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    }
    return self;
}

- (void)setModel:(VLSSeachResultModel *)model {
    _model = model;
    [self setFocusBtnState:model.relation];
    [self.headerImage sd_setImageWithURL:[NSURL URLWithString:model.avatars] placeholderImage:ICON_PLACEHOLDER];
    self.focusBtn.selected = model.isPress;
    if ([model.nickname isEqualToString:@"null"] || model.nickname == nil) {
        
        self.nameLabel.text = LocalizedString(@"MINE_NICKNAMENOTSETTED");
        
    }else
    {
        NSMutableAttributedString *str = [self getNickNameAndSexString:model.nickname withModelSexStr:nil withLevelNumber:0];
        self.nameLabel.attributedText = str;
    }

    [self getUserIntroWith:model.intro withLevelNumber:0 withIsLive:[model.living isEqualToString:@"1"] ? @"Y" : @"N"];
}

- (void)focusBtnAction:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(focusBtnViewBtn:)]) {
        [self.delegate focusBtnViewBtn:self];
    }
}

- (void)headBtnAction:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(headBtnViewBtn:)]) {
        [self.delegate headBtnViewBtn:self];
    }
}

- (void)setFocusBtnState:(NSString *)state {
    if ([state isEqualToString:MutualFollowed]) {
        [self.focusBtn setTitle:LocalizedString(@"MINE_FOCUSEACHOTHER") forState:UIControlStateNormal];
        [self.focusBtn setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateNormal];
        [self.focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flow"] forState:UIControlStateNormal];
    } else if ([state isEqualToString:NoFollowed]) {
        [self.focusBtn setTitle:LocalizedString(@"MINE_TOFOCUSED") forState:UIControlStateNormal];
        [self.focusBtn setTitleColor:RGB16(COLOR_FONT_FFFFFF) forState:UIControlStateNormal];
        [self.focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flowing"] forState:UIControlStateNormal];
        
    } else {
        [self.focusBtn setTitle:LocalizedString(@"ALREADY_CONCERONED") forState:UIControlStateNormal];
        [self.focusBtn setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateNormal];
        [self.focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flow"] forState:UIControlStateNormal];
    }
}

@end
