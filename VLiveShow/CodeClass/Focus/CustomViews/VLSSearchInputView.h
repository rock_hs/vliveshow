//
//  VLSSearchInputView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSSearchInputViewDelegate <NSObject>

- (void)showSearchHistoryViewWithArray:(NSArray *)array;
- (void)showNoSearchResultViewWithKeyWordString:(NSString *)keyWord;
- (void)showSearchResultTableViewWithArray:(NSMutableArray *)lists AndRefreshKeyWord:(NSString *)keyword;

@end
@interface VLSSearchInputView : UIView

@property (nonatomic, weak)id <VLSSearchInputViewDelegate> delegate;
- (void)clearSearchHistoryData;
- (void)showSelectedHistoryString:(NSString *)historyStr;
- (void)becomeFirstResponse;
@end
