//
//  VLSNoSearchResultView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSNoSearchResultView.h"

@interface VLSNoSearchResultView ()

@property (nonatomic, strong)UILabel *titleLabel;
@property (nonatomic, strong)UIImageView *arrowImgView;
@property (nonatomic, strong)UILabel *lineLable;

@end

@implementation VLSNoSearchResultView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goHomeMethod)];
    [self addGestureRecognizer:tap];
    
    [self addSubview:self.arrowImgView];
    self.arrowImgView.sd_layout
    .rightSpaceToView(self, X_CONVERT(8))
    .centerYEqualToView(self)
    .widthIs(X_CONVERT(24))
    .heightIs(Y_CONVERT(24));
    
    [self addSubview:self.titleLabel];
    self.titleLabel.sd_layout
    .rightSpaceToView(self.arrowImgView, X_CONVERT(10))
    .leftSpaceToView(self, X_CONVERT(15))
    .centerYEqualToView(self)
    .heightIs(Y_CONVERT(20));
    
    [self addSubview:self.lineLable];
    self.lineLable.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .bottomSpaceToView(self, 0)
    .heightIs(0.5);
    
    
}

- (void)goHomeMethod {
    if ([self.delegate respondsToSelector:@selector(noSearchResultAndGoHome)]) {
        [self.delegate noSearchResultAndGoHome];
    }
}

- (void)setNoResultViewContentWithKeyWord:(NSString *)keyWord {
    self.titleLabel.text = [NSString stringWithFormat:@"%@“%@”, %@", LocalizedString(@"FOCUS_SEARCH_NO_FIND"), keyWord, LocalizedString(@"FOCUS_SEARCH_GO_ORTHER_LIVING")];
}
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        _titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    }
    return _titleLabel;
}
- (UIImageView *)arrowImgView {
    if (!_arrowImgView) {
        _arrowImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_goto"]];
    }
    return _arrowImgView;
}
- (UILabel *)lineLable {
    if (!_lineLable) {
        _lineLable = [[UILabel alloc] init];
        _lineLable.backgroundColor = RGB16(COLOR_LINE_ECECEC);
    }
    return _lineLable;
}
@end
