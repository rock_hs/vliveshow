//
//  VLSSearchHistoryView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSSearchHistoryViewDelegate <NSObject>

- (void)clearSearchHistory;
- (void)selectSearchHistoryWithString:(NSString *)historyStr;

@end
@interface VLSSearchHistoryView : UIView

@property (nonatomic, weak)id <VLSSearchHistoryViewDelegate> delegate;
- (void)setHistoryDataWithArray:(NSArray *)array;

@end
