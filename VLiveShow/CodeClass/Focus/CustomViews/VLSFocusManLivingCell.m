//
//  VLSFocusManLivingCell.m
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSFocusManLivingCell.h"

@implementation VLSFocusManLivingCell

- (void)showTimeLableOnListWithModel:(VLSLiveListModel *)liveListModel {
    
    NSTimeInterval updateTime = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval createTime = [liveListModel.createTime longLongValue]/1000 ;
    if (updateTime <= createTime) {
        updateTime = createTime;
    }
    // 时间差
    NSTimeInterval time = updateTime - createTime;
    
    if (time < 3600) {
        // 秒转分钟
        NSInteger sec = time/60;
        if (sec<60) {
            self.timeLabel.text = [NSString stringWithFormat:@"%ld%@",sec, LocalizedString(@"MINUTESAGO")];
        }
    } else {
        // 秒转小时
        NSInteger hours = time/3600;
        if (hours<24) {
            self.timeLabel.text = [NSString stringWithFormat:@"%ld%@",hours, LocalizedString(@"HOURSAGO")];
        }
    }
//    秒转天
//    NSInteger days = time/3600/24;
//    if (days < 30) {
//        self.timeLabel.text = [NSString stringWithFormat:@"%ld天前",days];
//    }
    
    // 数字千分位格式化
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSString *newAmount = [formatter stringFromNumber:[NSNumber numberWithInteger:[NSString stringWithFormat:@"%@", liveListModel.scNum].integerValue]];
    
    [self layoutSubviews];
    CGRect rect1 = self.timeLabel.frame;
    CGRect rect2 = self.countLabel.frame;
    
    CGFloat width1 = [self widthOfString:self.timeLabel.text font:[UIFont systemFontOfSize:SIZE_FONT_12] height:Y_CONVERT(20)];
    CGFloat width2 = [self widthOfString:newAmount font:[UIFont systemFontOfSize:SIZE_FONT_12] height:Y_CONVERT(20)];
    
    rect1.size.width = width1;
    rect2.size.width = width2;
    
    CGFloat overlap = CGRectGetMinX(rect1) - CGRectGetMaxX(rect2);
    CGFloat width = self.nameLabel.bounds.size.width + overlap;
    
    if (overlap < 0) {
        self.nameLabel.sd_layout
        .leftSpaceToView(self.iconImageView,X_CONVERT(7))
        .topSpaceToView(self.tags,Y_CONVERT(6))
        .widthIs(width)
        .heightIs(Y_CONVERT(19));
    }
}

-(CGFloat)widthOfString:(NSString *)string font:(UIFont *)font height:(CGFloat)height
{
    NSDictionary * dict=[NSDictionary dictionaryWithObject: font forKey:NSFontAttributeName];
    CGRect rect=[string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return rect.size.width;
}

@end
