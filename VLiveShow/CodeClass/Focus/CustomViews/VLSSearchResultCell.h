//
//  VLSSearchResultCell.h
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMineBaseViewCell.h"
#import "VLSSeachResultModel.h"

static NSString *MutualFollowed = @"mutualFollowed";
static NSString *NoFollowed = @"noFollowed";
static NSString *Followed = @"followed";


@class VLSSearchResultCell;
@protocol VLSSearchResultCellDelegate <NSObject>

- (void)focusBtnViewBtn:(VLSSearchResultCell *)cell;
- (void)headBtnViewBtn:(VLSSearchResultCell *)cell;

@end


@interface VLSSearchResultCell : VLSMineBaseViewCell

@property (nonatomic, weak) id <VLSSearchResultCellDelegate> delegate;
@property (nonatomic, strong)VLSSeachResultModel *model;
- (void)setFocusBtnState:(NSString *)state;

@end
