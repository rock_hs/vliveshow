//
//  VLSHistoryTableViewCell.m
//  VLiveShow
//
//  Created by Cavan on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSHistoryTableViewCell.h"

@interface VLSHistoryTableViewCell ()

@property (nonatomic, strong)UIImageView *historyImgView;
@property (nonatomic, strong)UILabel *lineLabel;

@end

@implementation VLSHistoryTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self addSubview:self.historyImgView];
        self.historyImgView.sd_layout
        .leftSpaceToView(self, X_CONVERT(12))
        .centerYEqualToView(self)
        .widthIs(X_CONVERT(15))
        .heightIs(Y_CONVERT(15));
        
        [self addSubview:self.historyLabel];
        self.historyLabel.sd_layout
        .leftSpaceToView(self.historyImgView, X_CONVERT(10))
        .rightSpaceToView(self, 0)
        .centerYEqualToView(self)
        .heightIs(Y_CONVERT(20));
        
        [self addSubview:self.lineLabel];
        self.lineLabel.sd_layout
        .leftSpaceToView(self, X_CONVERT(33.))
        .rightSpaceToView(self, 0)
        .bottomSpaceToView(self, 0)
        .heightIs(0.5);
        
    }
    return self;
}

- (void)setHistoryDataWithString:(NSString *)string {
    self.historyLabel.text = string;
}

- (void)setLastCellSeparatorFullScreen {
    self.lineLabel.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .bottomSpaceToView(self, 0)
    .heightIs(0.5);
}
#pragma mark - getters

- (UIImageView *)historyImgView {
    if (!_historyImgView) {
        self.historyImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_history"]];
    }
    return _historyImgView;
}

- (UILabel *)historyLabel {
    if (!_historyLabel) {
        self.historyLabel = [[UILabel alloc] init];
        self.historyLabel.textColor = RGB16(COLOR_FONT_4A4A4A);
        self.historyLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        
        
    }
    return _historyLabel;
}

- (UILabel *)lineLabel {
    if (!_lineLabel) {
        _lineLabel = [[UILabel alloc] init];
        _lineLabel.backgroundColor = RGB16(COLOR_LINE_ECECEC);
    }
    return _lineLabel;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
