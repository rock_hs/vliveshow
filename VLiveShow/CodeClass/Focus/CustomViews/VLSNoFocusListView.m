//
//  VLSNoFocusListView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSNoFocusListView.h"

@interface VLSNoFocusListView ()

@property (nonatomic, strong)UIImageView *imgView;
@property (nonatomic, strong)UILabel *titleLabel;

@end

@implementation VLSNoFocusListView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doInit];
    }
    return self;
}

- (void)doInit {
    
    [self addSubview:self.titleLabel];
    self.titleLabel.sd_layout
    .bottomSpaceToView(self, 0)
    .leftSpaceToView(self, X_CONVERT(50))
    .rightSpaceToView(self, X_CONVERT(50))
    .heightIs(Y_CONVERT(20));
    
    [self addSubview:self.imgView];
    self.imgView.sd_layout
    .bottomSpaceToView(self.titleLabel, X_CONVERT(20))
    .centerXEqualToView(self.titleLabel)
    .widthIs(X_CONVERT(84))
    .heightIs(Y_CONVERT(84));
    
}

- (void)showNoFocusManOrNoLiving:(BOOL)noLiving {
    if (noLiving) {
        self.imgView.image = [UIImage imageNamed:@"ic_vliveshow"];
        self.titleLabel.text = LocalizedString(@"FOCUS_NO_LIVING");
    } else {
        self.imgView.image = [UIImage imageNamed:@"ic_sofa"];
        self.titleLabel.text = LocalizedString(@"FOCUS_NO_MAN");
    }
}


#pragma mark - getter

- (UIImageView *)imgView {
    if (!_imgView) {
        self.imgView = [[UIImageView alloc] init];
    }
    return _imgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        self.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

@end
