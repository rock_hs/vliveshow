//
//  VLSNoSearchResultView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSNoSearchResultViewDelegate <NSObject>

- (void)noSearchResultAndGoHome;

@end
@interface VLSNoSearchResultView : UIView

@property (nonatomic, weak)id <VLSNoSearchResultViewDelegate> delegate;
- (void)setNoResultViewContentWithKeyWord:(NSString *)keyWord;

@end
