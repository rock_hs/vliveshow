//
//  VLSSearchResultView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSSearchResultView.h"
#import "VLSSearchResultCell.h"
#import "VLSLiveHttpManager.h"
#import "VLSSeachResultModel.h"
#import "MJRefresh.h"

static NSString *identifier = @"SearchResultCell";

@interface VLSSearchResultView ()<UITableViewDelegate, UITableViewDataSource, VLSSearchResultCellDelegate>
{
    NSInteger page;
    BOOL isUP;
}
@property (nonatomic, strong)UITableView *searchTableView;
@property (nonatomic, strong)NSMutableArray *searchResults;
@property (nonatomic, strong)NSString *refreshKeyWord;

@end

@implementation VLSSearchResultView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initSubViews];
        [self refreshLivingLists];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
        [self refreshLivingLists];
    }
    return self;
}

- (void)initSubViews {
    
    [self addSubview:self.searchTableView];
    self.searchTableView.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0);
}

- (void)refreshLivingLists {
    // 上拉加载
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        page++;
        isUP = YES;
        [self getSearchUserLists];
    }];
    [footer setTitle:LocalizedString(@"LIVE_PULL_LOAD_MORE") forState:MJRefreshStateRefreshing];
    self.searchTableView.mj_footer = footer;
    
    // 下拉刷新
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        page = 0;
        isUP = NO;
        [self getSearchUserLists];
        
    }];
    self.searchTableView.mj_header = header;
    
}
- (void)getSearchUserLists {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        [self.searchTableView.mj_footer endRefreshing];
        [self.searchTableView.mj_header endRefreshing];
        
        if (!isUP) {
            [self.searchResults removeAllObjects];
        }
        [self.searchResults addObjectsFromArray:result];
        [self.searchTableView reloadData];
        
        
    };
    callback.errorBlock = ^(id result){
        [self.searchTableView.mj_footer endRefreshing];
        [self.searchTableView.mj_header endRefreshing];
    };
    
    [VLSLiveHttpManager searchUserWithPage:page size:20 keyword:self.refreshKeyWord callback:callback];
    
}

- (void)setDataSourseWithResultArray:(NSMutableArray *)lists {
    [self.searchResults removeAllObjects];
    self.searchResults = lists;
    [self.searchTableView reloadData];
}

- (void)setRefreshKeyWordWithString:(NSString *)keyWord {
    self.refreshKeyWord = keyWord;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VLSSearchResultCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    
    VLSSeachResultModel *searchResultModel = self.searchResults[indexPath.row];
    [cell setModel:searchResultModel];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return Y_CONVERT(70.);
}



#pragma mark - VLSSearchResultCellDelegate

-(void)focusBtnViewBtn:(VLSSearchResultCell *)cell {
    NSIndexPath *indpath = [self.searchTableView indexPathForCell:cell];
    VLSSeachResultModel *model = [self.searchResults objectAtIndex:indpath.row];
    
    if (!model.ID) return;
    
    if ([cell.focusBtn.titleLabel.text isEqualToString:LocalizedString(@"MINE_TOFOCUSED")]) {
        // 判断关系并关注
        [self judgeReleationAndFollowWithCell:cell Model:model];
    } else if ([cell.focusBtn.titleLabel.text isEqualToString:LocalizedString(@"MINE_FOCUSEACHOTHER")]) {
        // 取消关注
        [self cancelFocusReleationWithCell:cell Model:model];
    } else {
        // 取消关注
        [self cancelFocusReleationWithCell:cell Model:model];
    }
    
}


- (void)headBtnViewBtn:(VLSSearchResultCell *)cell {
    
    if ([self.delegate respondsToSelector:@selector(clickHeadBtnAndPushVC:and:with:)]) {
        [self.delegate clickHeadBtnAndPushVC:self.searchResults and:cell with:self.searchTableView];
    }
    
}

- (void)judgeReleationAndFollowWithCell:(VLSSearchResultCell *)cell Model:(VLSSeachResultModel *)model {
    
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        [self judgeReleationWithCell:cell Model:model];
    };
    callback.errorBlock = ^(id result)
    {
        
    };
    [VLSLiveHttpManager focusToUser:model.ID callback:callback];

}

- (void)cancelFocusReleationWithCell:(VLSSearchResultCell *)cell Model:(VLSSeachResultModel *)model {
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        [cell setFocusBtnState:NoFollowed];
    };
    callback.errorBlock = ^(id result)
    {
        
    };
    [VLSLiveHttpManager cancelFocusToUser:model.ID callback:callback];

}

- (void)judgeReleationWithCell:(VLSSearchResultCell *)cell Model:(VLSSeachResultModel *)model {
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        NSString *isfocues = [result objectForKey:@"is_attended_by"];
        if ([isfocues isEqualToString:@"N"]) {
            [cell setFocusBtnState:Followed];
        } else {
            [cell setFocusBtnState:MutualFollowed];
        }
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
        }
    };
    [VLSLiveHttpManager getrelationShip:model.ID callBack:callback];

}

#pragma mark - getters

- (NSMutableArray *)searchResults {
    if (!_searchResults) {
        _searchResults = [NSMutableArray array];
    }
    return _searchResults;
}

- (UITableView *)searchTableView {
    if (!_searchTableView) {
        _searchTableView = [[UITableView alloc] init];
        [_searchTableView registerClass:[VLSSearchResultCell class] forCellReuseIdentifier:identifier];
        _searchTableView.tableFooterView = [UIView new];
        _searchTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _searchTableView.delegate = self;
        _searchTableView.dataSource = self;
    }
    return _searchTableView;
}

@end
