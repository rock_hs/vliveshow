//
//  VLSSearchResultView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSSearchResultViewDelegate <NSObject>

- (void)clickHeadBtnAndPushVC:(NSMutableArray*)array and:(UITableViewCell*)cell with:(UITableView*)tableView;

@end

@interface VLSSearchResultView : UIView

@property (nonatomic, weak)id <VLSSearchResultViewDelegate> delegate;

- (void)setDataSourseWithResultArray:(NSMutableArray *)lists;
- (void)setRefreshKeyWordWithString:(NSString *)keyWord;

@end
