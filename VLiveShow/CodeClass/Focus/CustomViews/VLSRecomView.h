//
//  VLSRecomView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSRecomViewDelegate <NSObject>

- (void)clickHeadBtnAndPushVC:(NSMutableArray*)array and:(UITableViewCell*)cell with:(UITableView*)tableView;

@end
@interface VLSRecomView : UIView

@property (nonatomic, weak)id <VLSRecomViewDelegate> delegate;
- (void)setDataSourseWithResultArray:(NSMutableArray *)lists;

@end
