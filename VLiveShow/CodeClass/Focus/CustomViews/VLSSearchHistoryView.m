//
//  VLSSearchHistoryView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSSearchHistoryView.h"
#import "VLSHistoryTableViewCell.h"

static NSString *identifier = @"HistoryCell";

@interface VLSSearchHistoryView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong)UITableView *historyTableView;
@property (nonatomic, strong)NSArray *historyArr;

@end
@implementation VLSSearchHistoryView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    [self addSubview:self.historyTableView];
    self.historyTableView.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0);
    
    
}

- (void)setHistoryDataWithArray:(NSArray *)array {
    self.historyArr = array;
}

- (void)clearSearchHistoryAction {
    if ([self.delegate respondsToSelector:@selector(clearSearchHistory)]) {
        [self.delegate clearSearchHistory];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.historyArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VLSHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    [cell setHistoryDataWithString:self.historyArr[indexPath.row]];
    if (indexPath.row == self.historyArr.count - 1) {
        [cell setLastCellSeparatorFullScreen];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VLSHistoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([self.delegate respondsToSelector:@selector(selectSearchHistoryWithString:)]) {
        [self.delegate selectSearchHistoryWithString:cell.historyLabel.text];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - getters

- (UITableView *)historyTableView {
    if (!_historyTableView) {
        self.historyTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [self.historyTableView registerClass:[VLSHistoryTableViewCell class] forCellReuseIdentifier:identifier];
        self.historyTableView.separatorInset = UIEdgeInsetsMake(0, SCREEN_WIDTH, 0, 0);
        self.historyTableView.scrollEnabled = NO;
        UILabel *footerView = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, SCREEN_WIDTH, Y_CONVERT(44.))];
        footerView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearSearchHistoryAction)];
        [footerView addGestureRecognizer:tap];
        footerView.text = LocalizedString(@"FOCUS_SEARCH_CLEAR_HISTORY");
        footerView.textColor = RGB16(COLOR_FONT_777777);
        footerView.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        footerView.textAlignment = NSTextAlignmentCenter;
        self.historyTableView.tableFooterView = footerView;
        self.historyTableView.delegate = self;
        self.historyTableView.dataSource = self;
    }
    return _historyTableView;
}

- (NSArray *)historyArr {
    if (!_historyArr) {
        self.historyArr = [NSArray array];
    }
    return _historyArr;
}

@end
