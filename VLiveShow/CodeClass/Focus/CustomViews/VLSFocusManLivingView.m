//
//  VLSFocusManLivingView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSFocusManLivingView.h"
#import "VLSFocusManLivingCell.h"

static NSString *identifier = @"FocusManLivingCell";

@interface VLSFocusManLivingView ()<UITableViewDelegate, UITableViewDataSource>


@end
@implementation VLSFocusManLivingView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    [self addSubview:self.livingTableView];
    self.livingTableView.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0);
}

- (void)setDataSourceWithArray:(NSMutableArray *)livingLists {
    
    self.livingArr = [livingLists mutableCopy];
    [self.livingTableView reloadData];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.livingArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VLSFocusManLivingCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    if (self.livingArr.count == 1) {
        cell.lines.hidden = YES;
    }
    cell.liveListModel = self.livingArr[indexPath.row];;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SCREEN_HEIGHT / 2.375;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(enterIntoLivingRoom:)]) {
            [self.delegate enterIntoLivingRoom:indexPath];
        }
}


#pragma mark - getters

- (NSMutableArray *)livingArr {
    if (!_livingArr) {
        _livingArr = [NSMutableArray array];
    }
    return _livingArr;
}

- (UITableView *)livingTableView {
    if (!_livingTableView) {
        _livingTableView = [[UITableView alloc] init];
        [_livingTableView registerClass:[VLSFocusManLivingCell class] forCellReuseIdentifier:identifier];
        _livingTableView.tableFooterView = [UIView new];
        _livingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _livingTableView.delegate = self;
        _livingTableView.dataSource = self;
    }
    return _livingTableView;
}



@end
