//
//  VLSRecomCell.h
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMineBaseViewCell.h"
#import "VLSRecomModel.h"


@class VLSRecomCell;
@protocol VLSRecomCellDelegate <NSObject>

- (void)focusBtnViewBtn:(VLSRecomCell *)cell;
- (void)headBtnViewBtn:(VLSRecomCell *)cell;

@end

@interface VLSRecomCell : VLSMineBaseViewCell

@property (nonatomic, weak)id <VLSRecomCellDelegate> delegate;
@property (nonatomic, strong)VLSRecomModel *model;

@end
