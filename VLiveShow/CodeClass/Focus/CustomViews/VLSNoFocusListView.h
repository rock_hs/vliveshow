//
//  VLSNoFocusListView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSNoFocusListView : UIView

- (void)showNoFocusManOrNoLiving:(BOOL)noLiving;

@end
