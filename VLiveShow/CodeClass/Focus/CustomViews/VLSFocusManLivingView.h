//
//  VLSFocusManLivingView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSFocusManLivingViewDelegate <NSObject>

- (void)enterIntoLivingRoom:(NSIndexPath *)indexPath;

@end
@interface VLSFocusManLivingView : UIView

@property (nonatomic, weak)id <VLSFocusManLivingViewDelegate> delegate;
@property (nonatomic, strong)NSMutableArray *livingArr;
@property (nonatomic, strong)UITableView *livingTableView;

- (void)setDataSourceWithArray:(NSMutableArray *)livingLists;

@end
