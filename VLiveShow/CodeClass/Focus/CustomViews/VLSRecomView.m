//
//  VLSRecomView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSRecomView.h"
#import "VLSRecomCell.h"
#import "VLSLiveHttpManager.h"

static NSString *identifier = @"RecomCell";

@interface VLSRecomView ()<UITableViewDelegate, UITableViewDataSource, VLSRecomCellDelegate>

@property (nonatomic, strong)UITableView *recomTableView;
@property (nonatomic, strong)NSMutableArray *recomResults;

@end

@implementation VLSRecomView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    
    [self addSubview:self.recomTableView];
    self.recomTableView.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0);
}

- (void)setDataSourseWithResultArray:(NSMutableArray *)lists {
    [self.recomResults removeAllObjects];
    self.recomResults = lists;
    [self.recomTableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.recomResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VLSRecomCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    
    VLSRecomModel *recomModel = self.recomResults[indexPath.row];
    [cell setModel:recomModel];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return Y_CONVERT(70.);
}



#pragma mark - VLSSearchResultCellDelegate

-(void)focusBtnViewBtn:(VLSRecomCell *)cell {
    NSIndexPath *indpath = [self.recomTableView indexPathForCell:cell];
    VLSRecomModel *model = [self.recomResults objectAtIndex:indpath.row];
    model.isPress = !model.isPress;
    
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        cell.focusBtn.selected = !cell.focusBtn.selected;
    };
    callback.errorBlock = ^(id result)
    {
        
    };
    
    if ( cell.focusBtn.selected == NO) {
        [VLSLiveHttpManager focusToUser:model.ID callback:callback];
    }
    else
    {
        [VLSLiveHttpManager cancelFocusToUser:model.ID callback:callback];
    }
}

- (void)headBtnViewBtn:(VLSRecomCell *)cell {
    
    if ([self.delegate respondsToSelector:@selector(clickHeadBtnAndPushVC:and:with:)]) {
        [self.delegate clickHeadBtnAndPushVC:self.recomResults and:cell with:self.recomTableView];
    }

}

#pragma mark - getters

- (NSMutableArray *)recomResults {
    if (!_recomResults) {
        _recomResults = [NSMutableArray array];
    }
    return _recomResults;
}

- (UITableView *)recomTableView {
    if (!_recomTableView) {
        _recomTableView = [[UITableView alloc] init];
        [_recomTableView registerClass:[VLSRecomCell class] forCellReuseIdentifier:identifier];
        _recomTableView.tableFooterView = [UIView new];
        _recomTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _recomTableView.delegate = self;
        _recomTableView.dataSource = self;
    }
    return _recomTableView;
}


@end
