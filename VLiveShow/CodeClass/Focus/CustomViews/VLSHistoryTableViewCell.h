//
//  VLSHistoryTableViewCell.h
//  VLiveShow
//
//  Created by Cavan on 16/8/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSHistoryTableViewCell : UITableViewCell

@property (nonatomic, strong)UILabel *historyLabel;
- (void)setHistoryDataWithString:(NSString *)string;
- (void)setLastCellSeparatorFullScreen;

@end
