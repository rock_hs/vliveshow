//
//  VLSRecomCell.m
//  VLiveShow
//
//  Created by Cavan on 16/8/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSRecomCell.h"

@implementation VLSRecomCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
        
    }
    return self;
}

- (void)initSubViews {
    [self.focusBtn addTarget:self action:@selector(focusBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.focusBtn setTitle:LocalizedString(@"MINE_TOFOCUSED") forState:UIControlStateNormal];
    [self.focusBtn setTitle:LocalizedString(@"ALREADY_CONCERONED") forState:UIControlStateSelected];
    [self.focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flowing"] forState:UIControlStateNormal];
    [self.focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flow"] forState:UIControlStateSelected];
    [self.focusBtn setTitleColor:RGB16(COLOR_FONT_FFFFFF) forState:UIControlStateNormal];
    [self.focusBtn setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateSelected];
    [self.headBtn addTarget:self action:@selector(headBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setModel:(VLSRecomModel *)model {
    
    _model = model;
    
    [self.headerImage sd_setImageWithURL:[NSURL URLWithString:model.avatars] placeholderImage:ICON_PLACEHOLDER];
    self.focusBtn.selected = model.isPress;
    if ([model.nickName isEqualToString:@"null"] || model.nickName == nil) {
        
        self.nameLabel.text = LocalizedString(@"MINE_NICKNAMENOTSETTED");
        
    }else
    {
        NSMutableAttributedString *str = [self getNickNameAndSexString:model.nickName withModelSexStr:model.gender withLevelNumber:0];
        self.nameLabel.attributedText = str;
    }
    
    if (model.intro == nil||[model.intro isEqualToString:@" "]) {
        [self getUserIntroWith:LocalizedString(@"MINE_USER_PROFILE_LAZY_NOTHING") withLevelNumber:0 withIsLive:model.isLiving];

    } else {
        [self getUserIntroWith:model.intro withLevelNumber:0 withIsLive:model.isLiving];

    }
}


- (void)focusBtnAction:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(focusBtnViewBtn:)]) {
        [self.delegate focusBtnViewBtn:self];
    }
}

- (void)headBtnAction:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(headBtnViewBtn:)]) {
        [self.delegate headBtnViewBtn:self];
    }
}


@end
