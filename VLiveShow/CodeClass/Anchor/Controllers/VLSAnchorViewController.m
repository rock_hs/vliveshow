//
//  VLSAnchorViewController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAnchorViewController.h"
#import "CountDownView.h"
#import "SelectOnLineViewController.h"
#import "VLSVoiceViewController.h"
#import "InviteModel.h"
//#import "VLSReConnectionManager.h"
#import "HeartBeatManager.h"

#define anchorHight 35
@interface VLSAnchorViewController ()<VLSMessageManagerDelegate>{
    //交换位置用的参数
    VLSVideoItem *moveItem;
    CGPoint point;
    NSMutableArray *requestIds;
    VLSVoiceViewController *voice;
}

@end

@implementation VLSAnchorViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(didHeartBeatFailed:) name: HeartBeatManagerHeartBeatFailed object: nil];
    requestIds = [[NSMutableArray alloc] initWithCapacity:0];
    // 倒计时动画
    CountDownView *count = [[CountDownView alloc]initWithFrame:CGRectZero];
    [self.contentView addSubview:count];
    [self.contentView insertSubview:count atIndex:0];
    count.sd_layout
    .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
    
    [[VLSReConnectionManager sharedManager] saveRoomID:self.roomId andAnchorID:self.anchorId];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [count setNumAnimation];
    });
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    longPress.minimumPressDuration = 0.7;
    [self.contentView addGestureRecognizer:longPress];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AnchorConfirm) name:@"AnchorConfirm" object:nil];
    
    //直播房间名称传值
    self.contentView.anchor.roomName = self.title;
    [self.contentView.bottomView setActionType: self.actionType];
    
    if (self.actionType == ActionTypeLessonTeacher || self.actionType == ActionTypeLessonAttendance || self.actionType == ActionTypeLessonAudience) {
        [self.contentView.audiencesView setHidden:YES];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void)AnchorConfirm
{
    [self.contentView.bottomView changefriendStatus:NO];
}

//继承自父类 点击屏幕隐藏弹出的view
- (void)touchOnLiveBaseView:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchOnLiveBaseView:touches withEvent:event];
}


#pragma mark - VLSMessageManagerDelegate
#pragma mark - 连线部分
- (void)receiveGuestMuteSelf:(BOOL)mute
{
    if (voice) {
        [voice reloadData];
    }
}

//主播禁麦
- (void)receiveAnchorMuteMic:(BOOL)mute
{
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:mute];
    [self.contentView.bottomView showVoiceIcon:mute];
}

//收到更新主播信息view viewmode为空删除嘉宾；else 添加这个人;
- (void)receiveUpdataAnchorView:(VLSVideoViewModel *)viewModel
{
    if (viewModel == nil) {
        self.contentView.guestView.hidden = YES;
        [self.contentView.anchor setAnchorType:AnchorOnScreen];
    }else{
        self.contentView.guestView.hidden = NO;
        [self.contentView.guestView setGuestID:viewModel.userId];
        [self.contentView.anchor setAnchorType:GuestOnScreen];
    }
}

- (void)videoViewUpdateAnchorView:(VLSVideoViewModel*)model {
    [self receiveUpdataAnchorView:model];
}

// 主播收到申请
- (void)receiveLinkRequest:(VLSUserProfile *)user
{
    
    [self.contentView.bottomView isFirstRequestLink];
    [self.contentView.bottomView changefriendStatus:YES];
    [requestIds addObject:user.userId];
}

// 嘉宾取消连线请求（未连线成功之前）
- (void)receiveGuestCancelLinkRequest:(VLSUserProfile *)user
{
    if ([requestIds containsObject:user.userId]) {
        [requestIds removeObject:user.userId];
        if (requestIds.count == 0) {
            [self.contentView.bottomView changefriendStatus:NO];
            [self.contentView.bottomView checkMessageExistence];
        }
     }
}
// 收到站内信提醒
- (void)receiveMail
{
    [self.contentView.bottomView changeMailIcon:YES];
}

#pragma mark - VLSVideoViewDelegate 视频交换位置
- (void)videoViewReplaceUser:(VLSVideoViewModel *)fromModel toUser:(VLSVideoViewModel *)toModel
{
    NSLog(@"replace");
    //change
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){

    };
    callback.errorBlock = ^(id result){
        [self error:result];
    };
    [[VLSAVMultiManager sharedManager] anchorReplacePositionFrom:fromModel.userId to:toModel.userId callback:callback];
    
}

//声音点击
- (void)bottomVoiceClicked:(UIButton *)sender
{
    voice = [[VLSVoiceViewController alloc] init];
    voice.videoViews = self.videoView.subviews;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:voice];
    nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    nav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:nav animated:NO completion:^{
    }];
}

// 好友
- (void)bottomFriendClicked:(id)sender
{
    [self.contentView.bottomView checkMessageExistence];
    SelectOnLineViewController *SelectVC = [[SelectOnLineViewController alloc]init];
    SelectVC.actionType = self.actionType;
    //将当前已上线的用户传递到下一级展示
    __block NSMutableArray<InviteModel *>* collectionUsers = [[NSMutableArray alloc] init];
    if (self.videoView.NewGuestInfos.count > 0) {
        [self.videoView.NewGuestInfos enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            VLSVideoViewModel *model = obj;
            if (model.userType != UserTypeHost) { //剔除主播本身
                InviteModel* inviteModel = [InviteModel new];
                inviteModel.userID = model.userId;
                [collectionUsers addObject:inviteModel];
            }
        }];
    }
    SelectVC.collectionUsers = collectionUsers;
    [self.navigationController pushViewController:SelectVC animated:YES];
}

- (void)longPress:(UILongPressGestureRecognizer *)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan){
        self.isLongPressGesture = YES;
        CGPoint point2 = [longPress locationInView:self.videoView];
        moveItem = [self chectPoint:point2];
        if (moveItem) {
            point = [longPress locationInView:moveItem];
            self.contentView.hidden = YES;
            [self.videoView exchangeSubviewAtIndex:[self.videoView.subviews indexOfObject:moveItem] withSubviewAtIndex:[[self.videoView subviews] count] - 1];
            [self.videoView moveBeganWidthItem:moveItem];
        }
        
    }else if (longPress.state == UIGestureRecognizerStateChanged){
        CGPoint movePoint = [longPress locationInView:self.videoView];
        if (moveItem) {
            [moveItem setFrame:CGRectMake( movePoint.x - point.x, movePoint.y - point.y, moveItem.frame.size.width, moveItem.frame.size.height)];
            [self.videoView moveChangeWidthItem:moveItem];
        }
    }else if (longPress.state == UIGestureRecognizerStateEnded){
        if (moveItem) {
            self.contentView.hidden = NO;
            
            CGPoint endPoint = [longPress locationInView:self.videoView];
            CGFloat newX = endPoint.x - point.x ;
            CGFloat newY = endPoint.y - point.y ;
            [self.videoView moveEndWidthItem:moveItem newPoint:CGPointMake(newX, newY)];
            self.isLongPressGesture = NO;

        }
    }
}

- (VLSVideoItem *)chectPoint:(CGPoint)checkPoint
{
    return [super chectPoint:checkPoint];
}

- (void)didHeartBeatFailed: (NSNotification*)notification
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [self.headAlert showTitleWithString: LocalizedString(@"ANCHOR_POOR_NETWORK_ALERT") withDuration: 4];
                   });
}
@end
