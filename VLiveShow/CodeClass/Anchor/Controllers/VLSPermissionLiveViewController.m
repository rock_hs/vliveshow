//
//  VLSPermissionLiveViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/7/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSPermissionLiveViewController.h"
#import "VLSMessageViewController.h"

@interface VLSPermissionLiveViewController ()

{
    UIImageView *dotImageView;

}
@end

@implementation VLSPermissionLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpNavBar];
    [self setupUI];
    // 添加观察者
//    [self addlistener];

}

- (void)setUpNavBar {
    
    UIImageView *titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_logo2"]];
    titleView.frame = CGRectMake(0, 0, 60, 18);
    self.navigationItem.titleView = titleView;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 22, 14);
    btn.tag = 1009;
    [btn setBackgroundImage:[UIImage imageNamed:@"home_massage"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(systemMailAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

- (void)setupUI {
    
    UIImageView *bgImageView = [[UIImageView alloc] init];
    bgImageView.image = [UIImage imageNamed:@"livebefore_whoops"];
    [self.view addSubview:bgImageView];
    bgImageView.sd_layout
    .centerXEqualToView(self.view)
    .topSpaceToView(self.view, SCREEN_HEIGHT / 2 - 150)
    .widthIs(60)
    .heightIs(60);
    
    UILabel *whoopsLabel = [[UILabel alloc] init];
    whoopsLabel.textAlignment = NSTextAlignmentCenter;
    whoopsLabel.text = @"Whoops!";
    whoopsLabel.textColor = [UIColor grayColor];
    whoopsLabel.font = [UIFont systemFontOfSize:SIZE_FONT_13];
    [self.view addSubview:whoopsLabel];
    whoopsLabel.sd_layout
    .topSpaceToView(bgImageView, 10)
    .centerXEqualToView(self.view)
    .widthIs(200)
    .heightIs(20);
    
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = LocalizedString(@"LIVEBEFORE_NO_PERMISSION");
    label.textColor = [UIColor grayColor];
    label.font = [UIFont systemFontOfSize:SIZE_FONT_13];
    [self.view addSubview:label];
    label.sd_layout
    .topSpaceToView(whoopsLabel, 0)
    .centerXEqualToView(self.view)
    .widthIs(200)
    .heightIs(20);
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:LocalizedString(@"LIVEBEFORE_GO_OTHER_LIVE") forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
    [button setBackgroundImage:[UIImage imageNamed:@"button_red"] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(nextAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    button.sd_layout
    .topSpaceToView(label, 100)
    .rightSpaceToView(self.view, 40)
    .leftSpaceToView(self.view, 40)
    .heightIs(40);
    
    
    
}

#pragma mark - 点击进入系统信息

- (void)systemMailAction:(id)sender {
    //隐藏点点
    [dotImageView removeFromSuperview];
    // 跳转到站内信
    [VLSMessageManager sharedManager].newMail = NO;
    VLSMessageViewController *messageVC = [[VLSMessageViewController alloc] init];
    messageVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:messageVC animated:YES];
}

//加红点，收到新消息时调用
- (void)changeStationMailToAddDot{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 22, 14);
    btn.tag = 1009;
    [btn setBackgroundImage:[UIImage imageNamed:@"home_massage"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(systemMailAction:) forControlEvents:UIControlEventTouchUpInside];
    
    dotImageView = [[UIImageView alloc] initWithFrame:CGRectMake(18,-2, 8, 8)];
    dotImageView.layer.cornerRadius = 4;
    dotImageView.clipsToBounds = YES;
    dotImageView.image = [UIImage imageNamed:@"broadcast-news-v_tv-notice-tip1-9"];
    [btn addSubview:dotImageView];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

#pragma mark 添加监听者

- (void)addlistener
{
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(listenerMethod:) name:@"JoinRoom" object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(maillistener) name:@"mail" object:nil];
    
    // 监听Manager的newmail属性
    [[VLSMessageManager sharedManager] addObserver:self forKeyPath:@"newMail" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"newMail"]) {
        if ([VLSMessageManager sharedManager].newMail == YES) {
            [self changeStationMailToAddDot];
        }else{
            [dotImageView removeFromSuperview];
        }
    }
    
}


//- (void)listenerMethod:(NSNotification *)notifi
//{
//    NSDictionary *dict = notifi.userInfo;
//    NSString *roomId = dict[@"roomID"];
//    NSString *anchorId ;
//    
//    id longhost = dict[@"anchorID"];
//    if ([longhost isKindOfClass:[NSNumber class]]) {
//        
//        anchorId = [NSString stringWithFormat:@"%ld",(long)[longhost integerValue]];
//    }else{
//        anchorId= longhost;
//    }
//    VLSShowViewController *show = [[VLSShowViewController alloc] init];
//    show.anchorId = anchorId;
//    show.roomId =roomId;
//    show.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:show animated:YES];
//}
//
//- (void)maillistener
//{
//    [self.navigationController popToRootViewControllerAnimated:NO];
//    VLSMessageViewController *messageVC = [[VLSMessageViewController alloc] init];
//    messageVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:messageVC animated:YES];
//}

- (void)dealloc
{
//    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
//    @try{
//        [[VLSMessageManager sharedManager] removeObserver:self forKeyPath:@"newMail"];
//
//    }
//    @catch(NSException *exception) {
//        NSLog(@"exception:%@", exception);
//    }
//    @finally {
//        
//    }
    
}

- (void)nextAction {
   
    self.tabBarController.selectedIndex = 0;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
