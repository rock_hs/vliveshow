//
//  VLSAnchorSearchViewController.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAnchorSearchViewController.h"

#import "VLSLiveHttpManager.h"
#import "VLSSearchGuestTableViewCell.h"
#import "VLSInviteGuestModel.h"
#import "MJRefresh.h"


@interface VLSAnchorSearchViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    UIButton *button;
    NSInteger page;
    NSInteger size;
    UITextField *_textField;
    UITableView *_tableView;
    NSMutableArray <VLSInviteGuestModel *>* dateArray;
    

}

@property (nonatomic, strong) MJRefreshAutoNormalFooter *footer;

@end

@implementation VLSAnchorSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    page = 0;
    size = 20;
    dateArray = [NSMutableArray arrayWithCapacity:0];
    [self setNavigationTitle];
    [self settableview];
}


- (void)setNavigationTitle
{
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(5, 0, 40, 20);
    button.titleLabel.font = [UIFont systemFontOfSize:16.f];
    button.titleLabel.textAlignment = NSTextAlignmentRight;
    [button setTitleColor:[UIColor colorWithRed:119/255.f green:119/255.f blue:119/255.f alpha:1] forState:UIControlStateNormal];
    [button setTitle:LocalizedString(@"INVITE_CANCEL")  forState:UIControlStateNormal];
    [button addTarget:self action:@selector(cancleInviteGuset) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
    [view addSubview:button];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:view];
    
    
    
    UIButton *leftbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftbutton.frame = CGRectMake(0, 0, 20, 20);
    [leftbutton setBackgroundImage:[UIImage imageNamed:@"ic_search"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftbutton];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-100, 30)];
    _textField.font = [UIFont systemFontOfSize:SIZE_FONT_16];
    _textField.backgroundColor = [UIColor clearColor];
    _textField.borderStyle = UITextBorderStyleNone;
    _textField.tintColor = [UIColor redColor];
    _textField.placeholder = LocalizedString(@"INVITE_PLACEHOLDER");
    _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _textField.returnKeyType = UIReturnKeySearch;
    _textField.delegate = self;
    self.navigationItem.titleView = _textField;
    [_textField becomeFirstResponder];
    
}

- (void)settableview
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:_tableView];
    _tableView.sd_layout
    .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
    [_tableView registerClass:[VLSSearchGuestTableViewCell class] forCellReuseIdentifier:@"SearchGuestCell"];
    
    _tableView.separatorInset = UIEdgeInsetsMake(0, 70, 0, 12);
    _tableView.separatorColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
    
    self.footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadDate)];
    [self.footer setTitle:LocalizedString(@"LIVE_PULL_LOAD_MORE") forState:MJRefreshStateRefreshing];
    
    
    [_tableView reloadData];
}

- (void)cancleInviteGuset
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 网络请求

- (void)loadDate
{
   
    
    [self netWorkTaskSucc:^(NSInteger count){
        [_tableView.mj_footer endRefreshing];

        // 没有数据时界面
        if (count == 0) {
            _tableView.mj_footer = nil;
        }else if(count < size && count > 0){
            _tableView.mj_footer = nil;
        }else{
            if(page == 0){
                _tableView.mj_footer = self.footer;
            }
            page++;
        }
        
    } Fail:^{
        [_tableView.mj_footer endRefreshing];

    }];
        
    
}

- (void)netWorkTaskSucc:(SearchNetSucc)succ Fail:(NetFail)fail
{
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        NSMutableArray *array = result;
//        NSInteger addArraycount = dateArray.count;

        if (page == 0) {
            dateArray = array;
            
        }else{
            [dateArray addObjectsFromArray:array];
        }
        [_tableView reloadData];
        succ(array.count);
    };
    callback.errorBlock = ^(id result){
        [self error:result];
        fail();

    };
    
    [VLSLiveHttpManager getSearchRelationshipPage:page size:size keyword:_textField.text callback:callback]  ;
    
}


#pragma mark 代理协议

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dateArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VLSSearchGuestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchGuestCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = dateArray[indexPath.row];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView cellHeightForIndexPath:indexPath model:dateArray[indexPath.row] keyPath:@"model" cellClass:[VLSSearchGuestTableViewCell class] contentViewWidth:[self cellContentViewWith]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    // 适配ios7
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
        width = [UIScreen mainScreen].bounds.size.height;
    }
    return width;
}

#pragma mark textfield点击搜索代理

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    page = 0;
    [self loadDate];
    [textField resignFirstResponder];
    return YES;
}

@end
