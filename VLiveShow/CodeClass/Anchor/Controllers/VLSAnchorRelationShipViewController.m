//
//  VLSAnchorRelationShipViewController.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAnchorRelationShipViewController.h"
#import "VLSAnchorSearchViewController.h"
#import "VLSLiveHttpManager.h"
#import "VLSInviteGuestCell.h"
#import "VLSInviteGuestModel.h"
#import "MJRefresh.h"
#import "VLSMessageManager.h"
#import "MBProgressHUD+Add.h"


#import "TableNilView.h"

@interface VLSAnchorRelationShipViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UIButton *button;
    NSInteger page;
    NSInteger size;
    UITableView *_tableView;
    TableNilView *tablenilView;
    NSMutableArray <VLSInviteGuestModel *>* dateArray;
    NSMutableArray <VLSInviteGuestModel *>* selectedArray;
    MJRefreshAutoNormalFooter *footer;
}

@end

@implementation VLSAnchorRelationShipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    page = 1;
    size = 20;
    dateArray = [NSMutableArray arrayWithCapacity:0];
    [self setNavigationTitle];
    [self setupUI];
    [self loadDate];
}

- (void)setNavigationTitle
{
    UILabel *titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    titlelabel.font = [UIFont systemFontOfSize:SIZE_FONT_16];
    titlelabel.textColor = [UIColor blackColor];
    titlelabel.text = LocalizedString(@"INVITE_WILL_GUEST");
    titlelabel.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = titlelabel;
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(5, 0, 40, 20);
    button.titleLabel.font = [UIFont systemFontOfSize:16.f];
    button.titleLabel.textAlignment = NSTextAlignmentRight;
    [button setTitleColor:[UIColor colorWithRed:119/255.f green:119/255.f blue:119/255.f alpha:1] forState:UIControlStateNormal];
    [button setTitle:LocalizedString(@"INVITE_CANCEL")  forState:UIControlStateNormal];
    [button addTarget:self action:@selector(cancleInviteGuset) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
    [view addSubview:button];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:view];
    
    
    
    UIButton *leftbutton = [UIButton buttonWithType:UIButtonTypeSystem];
    leftbutton.frame = CGRectMake(0, 0, 20, 20);
    [leftbutton setBackgroundImage:[UIImage imageNamed:@"ic_search"] forState:UIControlStateNormal];
    [leftbutton addTarget:self action:@selector(AcceptOrReject) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftbutton];
    
    
    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self loadDate];
    }];
    [footer setTitle:LocalizedString(@"LIVE_PULL_LOAD_MORE") forState:MJRefreshStateRefreshing];
    
}

- (void)setupUI
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:_tableView];
    _tableView.sd_layout
    .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
    [_tableView registerClass:[VLSInviteGuestCell class] forCellReuseIdentifier:@"InviteGuestCell"];
    _tableView.separatorInset = UIEdgeInsetsMake(0, 70, 0, 12);
    _tableView.separatorColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
    
    tablenilView = [[TableNilView alloc] initWithFrame:self.view.bounds];
    
}

#pragma mark 网络请求

- (void)loadDate
{
    [self netWorkTaskSucc:^(NSInteger count){
        // 没有数据时界面
        if (count == 0) {
            _tableView.mj_footer = nil;
        }else if(count < size && count > 0){
            _tableView.mj_footer = nil;
        }else{
            _tableView.mj_footer = footer;
            page++;
        }
    } Fail:^{
        
    }];
    
}

- (void)netWorkTaskSucc:(NetSucc)succ Fail:(NetFail)fail
{
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        NSMutableArray *array = result;
        if (page == 1) {
            dateArray = array;
        }else{
            [dateArray addObjectsFromArray:array];
        }

        [_tableView reloadData];
        succ(array.count);
        [_tableView.mj_footer endRefreshing];
    };
    callback.errorBlock = ^(id result){
        [self error:result];
        fail();
        [_tableView.mj_footer endRefreshing];

    };
    
    [VLSLiveHttpManager getAllRelationshipPage:page size:size roomid:[VLSMessageManager sharedManager].getRoomId callback:callback];

}

#pragma mark 代理协议

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dateArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VLSInviteGuestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InviteGuestCell" forIndexPath:indexPath];
    cell.model = dateArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView cellHeightForIndexPath:indexPath model:dateArray[indexPath.row] keyPath:@"model" cellClass:[VLSInviteGuestCell class] contentViewWidth:[self cellContentViewWith]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (selectedArray.count == 20) {
        // 彈出提示框，并return
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = nil;
        hud.detailsLabelText = LocalizedString(@"INVITE_FULL_INVITEGUEST");
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        return;
    }
    VLSInviteGuestCell *cell = (VLSInviteGuestCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell seletedStatus];
    [self AskApplyNumberFromArr];
    
}

#pragma mark 子线程遍历数组，得到当前点击的成员信息

- (void)AskApplyNumberFromArr
{
    
    selectedArray = [NSMutableArray arrayWithCapacity:0];
    for (VLSInviteGuestModel *model in dateArray) {
        if (model.isSelected) {
            [selectedArray addObject:model];
        }
    }
    if (selectedArray.count) {
        // 如果有已经被选择的
        [button setTitle:LocalizedString(@"INVITE_SURE") forState:UIControlStateNormal];
    }else{
        [button setTitle:LocalizedString(@"INVITE_CANCEL") forState:UIControlStateNormal];
    }
}



- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    // 适配ios7
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
        width = [UIScreen mainScreen].bounds.size.height;
    }
    return width;
}

- (void)cancleInviteGuset
{
    if (selectedArray.count) {
        ManagerCallBack *callback = [[ManagerCallBack alloc] init];
        callback.updateBlock = ^(id result){
            
            
        };
        callback.errorBlock = ^(id result){
            
            
        };
        
        NSMutableArray *IDarray = [NSMutableArray arrayWithCapacity:0];
        
        for (VLSInviteGuestModel *model in selectedArray) {
            [IDarray addObject:model.userId];
        }
        
        [[VLSAVMultiManager sharedManager] inviteHonoredGuestWithInviteIds:IDarray callback:callback];
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)AcceptOrReject
{
    
    
    VLSAnchorSearchViewController *searchVC = [[VLSAnchorSearchViewController alloc] init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

@end
