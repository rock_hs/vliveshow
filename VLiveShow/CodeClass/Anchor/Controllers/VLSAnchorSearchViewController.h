//
//  VLSAnchorSearchViewController.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
typedef void (^SearchNetSucc)(NSInteger count);
typedef void (^NetFail)();

@interface VLSAnchorSearchViewController : VLSBaseViewController

@end
