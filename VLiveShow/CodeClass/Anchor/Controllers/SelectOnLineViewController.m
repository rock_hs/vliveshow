//
//  SelectOnLineViewController.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "SelectOnLineViewController.h"
#import "InviteSectionHeaderView.h"
#import "InviteTableViewCell.h"
#import "VLSAnchorRelationShipViewController.h"
#import "VLSAVMultiManager.h"
#import "VLSMessageManager.h"
#import "VLSLinkRequestModel.h"
#import "ManagerEvent.h"
#import "inviteButton.h"
#import "VLSLiveHttpManager.h"
//#import "InviteModel.h"

@interface SelectOnLineViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic, strong) InviteSectionHeaderView *inviteView;

@property (nonatomic, strong) UILabel *label;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray<InviteModel *> *inviteUsers;

@property (nonatomic, strong) UIBarButtonItem *rightItem;

@property (nonatomic, strong) inviteButton *invitebtn;

@property (nonnull, strong) UIButton* rightButton;

@property (nonatomic, assign) NSInteger curSelectedNum;//记录当前选中“请求连线的观众”数

@end

@implementation SelectOnLineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 创建navigationbar
    
    //    self.navigationItem.title = LocalizedString(@"INVITE_CONNECT");
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 60)];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:16.f];
    label.text = LocalizedString(@"INVITE_CONNECT");
    label.textAlignment = NSTextAlignmentCenter;
    //    [self.navigationItem.titleView addSubview:label];
    self.navigationItem.titleView = label;
    
    
    
    UIButton *leftbutton = [UIButton buttonWithType:UIButtonTypeSystem];
    leftbutton.frame = CGRectMake(0, 0, 40, 60);
    leftbutton.titleLabel.font = [UIFont systemFontOfSize:16.f];
    [leftbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [leftbutton setTitle:LocalizedString(@"INVITE_CANCEL") forState:UIControlStateNormal];
    [leftbutton addTarget:self action:@selector(cancleInviteGuset) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftbutton];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(10, 0, 60, 40);
    button.titleLabel.font = [UIFont systemFontOfSize:16.f];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    [button setTitle:[NSString stringWithFormat:@"0/4%@",LocalizedString(@"INVITE_SURE")] forState:UIControlStateNormal];
    [button setTitle:[NSString stringWithFormat:@"0/6%@",LocalizedString(@"INVITE_SURE")] forState:UIControlStateNormal];
    button.titleLabel.textAlignment = NSTextAlignmentRight;
    [button addTarget:self action:@selector(AcceptOrReject) forControlEvents:UIControlEventTouchUpInside];
    self.rightButton = button;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.inviteView = [[InviteSectionHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120)];
    [self.view addSubview:self.inviteView];
    
    self.view.backgroundColor = [UIColor colorWithRed:241.0f/255.0f green:241.0f/255.0f blue:241.0f/255.0f alpha:1.0f];
    
    self.label = [[UILabel alloc]init];
    self.label.text = LocalizedString(@"INVITE_NO_AUDIES");
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    self.label.backgroundColor = [UIColor whiteColor];
    self.label.textColor = [UIColor colorWithRed:151/255.f green:151/255.f blue:151/255.f alpha:1];
    [self.view addSubview:self.label];
    self.label.sd_layout
    .topSpaceToView(self.inviteView,10)
    .leftSpaceToView(self.view,0)
    .rightSpaceToView(self.view,0)
    .heightIs(30);
    
    self.invitebtn = [[inviteButton alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.invitebtn];
    self.invitebtn.sd_layout
    .leftSpaceToView(self.view,0)
    .rightSpaceToView(self.view,0)
    .bottomSpaceToView(self.view,0)
    .heightIs(50);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction)];
    [self.invitebtn addGestureRecognizer:tapGesture];
    
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[InviteTableViewCell class] forCellReuseIdentifier:@"GTCell"];
    
    self.tableView.sd_layout
    .topSpaceToView(self.label,0)
    .leftSpaceToView(self.view,0)
    .rightSpaceToView(self.view,0)
    .bottomSpaceToView(self.invitebtn,0);
    
    
    
    //显示空时，下划线消失
    self.tableView.tableFooterView = [[UIView alloc]init];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 80, 0, 12);
    self.tableView.separatorColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
    
    // 改变状态栏颜色
    [self setNeedsStatusBarAppearanceUpdate];
    
    // 网络请求
    [self loadData];
    
    [self AskApplyNumberFromArr];
    if (_actionType == ActionTypeLessonTeacher) {
        self.invitebtn.userInteractionEnabled = false;
        self.invitebtn.backgroundColor = RGB16(COLOR_FONT_DBDBDB);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}


-(void)viewWillDisappear:(BOOL)animated

{
    
    [super viewWillDisappear:animated];
    
}
// 跳转到邀请嘉宾界面
- (void)tapGestureAction
{
    VLSAnchorRelationShipViewController *reletionship = [[VLSAnchorRelationShipViewController alloc] init];
    [self.navigationController pushViewController:reletionship animated:NO];
}

/**
 *  获取连线嘉宾数组
 */
- (void)setCollectionUsers:(NSMutableArray<InviteModel *> *)collectionUsers
{
    _collectionUsers = collectionUsers;
}

- (void)loadData
{
    
    //    NSMutableArray *listarray = [NSMutableArray array];
    
    self.inviteUsers = [NSMutableArray array];
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        for (VLSLinkRequestModel *model in result) {
            //判断连线关注是否已经在线
            __block BOOL isOnLineFlag = NO;
            [self.collectionUsers enumerateObjectsUsingBlock:^(InviteModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                InviteModel* inviteModel = obj;
                if ([model.applyAccount isEqualToString:inviteModel.userID]) {//代表在线
                    isOnLineFlag = YES;
                    *stop = YES;
                }
            }];
            if (!isOnLineFlag) {
                InviteModel *invitmodel = [[InviteModel alloc]init];
                [invitmodel infoFromVLSLinkRequest:model];
                [self.inviteUsers addObject:invitmodel];
            }
        }
        if (self.inviteUsers.count == 0) {
            self.label.text = LocalizedString(@"INVITE_NO_AUDIES");
        }else{
            self.label.text = LocalizedString(@"INVITE_CONTINUE_AUDIENCE");
        }
        
//        [self AskApplyNumberFromArr];
        [self.tableView reloadData];
    };
    callback.errorBlock = ^(id result){
        
        
    };
    [[VLSAVMultiManager sharedManager] getLinkRequestListCallback:callback];
    
}

#pragma mark tableview 代理

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%ld",(unsigned long)self.inviteUsers.count);
    return self.inviteUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InviteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GTCell" forIndexPath:indexPath];
    cell.model = self.inviteUsers[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView cellHeightForIndexPath:indexPath model:self.inviteUsers[indexPath.row] keyPath:@"model" cellClass:[InviteTableViewCell class] contentViewWidth:[self cellContentViewWith]];
}

- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    // 适配ios7
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
        width = [UIScreen mainScreen].bounds.size.height;
    }
    return width;
}

#pragma mark 点击cell

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    InviteTableViewCell *cell = (InviteTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if ((self.collectionUsers.count + self.curSelectedNum) >= 6 && !cell.model.isSelected) {//4) {
        // 彈出提示框，并return
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = nil;
        hud.detailsLabelText = LocalizedString(@"INVITE_SEAT_FULL");
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        return;
    }
    
    [cell seletedStatus];
    if (cell.model.isSelected) {
        self.curSelectedNum ++;
    } else {
        self.curSelectedNum --;
    }
}

#pragma mark leftItem

- (void)backToPreview
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark rightItem

- (void)AcceptOrReject
{
    // 选中的id数组
    NSMutableArray *userIDs = [NSMutableArray arrayWithCapacity:6];//4];
    NSMutableArray *kickOffUserIDs = [NSMutableArray arrayWithCapacity:6];//4];
    
    [self.inviteUsers enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        InviteModel *model = obj;
        if (model.isSelected && model.isGuest) {
            // 是嘉宾且没有被主播踢出, 不作处理
            
        }else if(model.isSelected && !model.isGuest){
            // 不是嘉宾，且被邀请
            
            //                ManagerCallBack *callback = [[ManagerCallBack alloc] init];
            //                callback.updateBlock = ^(id result){
            //                    [self.navigationController popViewControllerAnimated:YES];
            //                };
            //                callback.errorBlock = ^(id result){
            //
            //                };
            //                [[VLSAVMultiManager sharedManager] anchorAcceptLinkRequest:@[model.userID] callback:callback];
            [userIDs addObject:model.userID];
            
            
        }else if (model.isGuest && !model.isSelected){
            // 是嘉宾，且被踢出
            [kickOffUserIDs addObject:model.userID];
        }
        //        [userIDs addObject:model.userID];
    }];
    // 网络请求IDs 
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        ManagerEvent *event = result;
        if (event.code == 0000) {
        }
        //        [self error:result];
    };
    [[VLSAVMultiManager sharedManager] anchorAcceptLinkRequest:userIDs callback:callback];
    
    ManagerCallBack *canclecallback = [[ManagerCallBack alloc] init];
    canclecallback.updateBlock = ^(id result){
    };
    canclecallback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        ManagerEvent *event = result;
        if (event.code == 0000) {
        }
        //        [self error:result];
    };
    [[VLSAVMultiManager sharedManager] anchorCancelLink:kickOffUserIDs callback:canclecallback];
    
    // 发送通知直播间底部状态看变灰
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AnchorConfirm" object:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark 子线程遍历数组，得到当前点击的成员信息

- (void)AskApplyNumberFromArr
{
    
//    self.collectionUsers = [NSMutableArray arrayWithCapacity:6];//4];
//    
//    for (InviteModel *model in self.inviteUsers) {
//        if (model.isSelected) {
//            [self.collectionUsers addObject:model];
//        }
//    }
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        VLSUserProfile* Vmodel = result;
        [self.collectionUsers enumerateObjectsUsingBlock:^(InviteModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            InviteModel* model = obj;
            if ([model.userID isEqualToString:Vmodel.userId]) {
                model.faceUrl = Vmodel.userFaceURL;
                model.nickName = Vmodel.userName;
                [self.collectionUsers replaceObjectAtIndex:idx withObject:model];
                *stop = YES;
            }
        }];
        [self.inviteView ReloadCollection:self.collectionUsers];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    for (InviteModel* model in self.collectionUsers) {
        [VLSLiveHttpManager getUserInfo:[NSString stringWithFormat:@"%lld",model.userID.longLongValue] callback:callback];
    }
    
    [self.rightButton setTitle:[NSString stringWithFormat:@"%lu/6%@",(unsigned long)self.collectionUsers.count,LocalizedString(@"INVITE_SURE")
                      ] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.rightButton];
    [self.inviteView ReloadCollection:self.collectionUsers];
}

- (void)cancleInviteGuset
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController popViewControllerAnimated:YES];
}


@end
