//
//  SelectOnLineViewController.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "InviteModel.h"

@interface SelectOnLineViewController : VLSBaseViewController

@property (nonatomic, strong) NSMutableArray<InviteModel *> *collectionUsers;
@property(nonatomic, assign) ActionType actionType;
@end
