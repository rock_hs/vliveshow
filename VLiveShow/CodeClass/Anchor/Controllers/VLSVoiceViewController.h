//
//  VLSVoiceViewController.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/21.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSVoiceViewController : UIViewController

@property (nonatomic, strong)NSArray *videoViews;

- (void)reloadData;

@end
