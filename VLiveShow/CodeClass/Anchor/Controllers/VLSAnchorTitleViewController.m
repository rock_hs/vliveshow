//
//  VLSAnchorTitleViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/5/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAnchorTitleViewController.h"
#import "VLSAnchorViewController.h"
#import "VLSMessageManager.h"
#import "VLSLiveHttpManager.h"
#import "UUIDShortener.h"
#import "VLSAgoraVideoService.h"
#import "SNLocationManager.h"
#import "CropperViewController.h"
#import "VLSPhotoSelectView.h"
#import "VLSHeadAlertView.h"
#import "AccountManager.h"
#import "ShareActionSheet.h"

@interface VLSAnchorTitleViewController ()<UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropperViewControllerDelegate, VLSPhotoSelectViewDelegate, CLLocationManagerDelegate>
/// 创建相机翻转按钮
@property (nonatomic, strong)UIButton *cameraButton;
/// 模糊滤镜背景
@property (nonatomic, strong)UIView *effectView;
/// 封面背景图
@property (nonatomic, strong)UIImageView *coverImageView;
/// 直播标题
@property (nonatomic, strong)UITextView *titleTextView;
/// 直播标题提示
@property (nonatomic, strong)UILabel *titleLabel;
/// 横向分割线
@property (nonatomic, strong)UILabel *lineLabel;
/// 竖向分割线
@property (nonatomic, strong)UILabel *verticalLabel;
/// 地理位置图标
@property (nonatomic, strong)UIImageView *locationImageView;
/// 地理位置
@property (nonatomic, strong)UILabel *locationLabel;
/// 关闭地理位置按钮
@property (nonatomic, strong)UIButton *locationCloseButton;
/// 上传封面按钮
@property (nonatomic, strong)UIButton *uploadCoverButton;
/// 微信分享按钮
@property (nonatomic, strong)UIButton *weChatButton;
/// QQ 空间分享按钮
@property (nonatomic, strong)UIButton *zoneButton;
/// 朋友圈分享按钮
@property (nonatomic, strong)UIButton *friendShipButton;
/// QQ 分享按钮
@property (nonatomic, strong)UIButton *qqButton;
/// 微博分享按钮
@property (nonatomic, strong)UIButton *sinaButton;
/// 开始直播按钮
@property (nonatomic, strong)UIButton *startButton;
/// 关闭按钮
@property (nonatomic, strong)UIButton *closeButton;
/// 输入框错误提示图片
@property (nonatomic, strong)UIImageView *warningImageView;
/// 封面图片错误提示图片
@property (nonatomic, strong)UIImageView *coverWaringImage;
/// 封面图片错误提示文字
@property (nonatomic, strong)UILabel *waringLabel;
/// 照片选择器
@property (nonatomic, strong)UIImagePickerController *imagePicker;
/// 地理位置
@property (nonatomic, strong)SNLocationManager *locationManager;
/// 系统地理位置管理类
@property (nonatomic, strong)CLLocationManager *clLocationManager;
/// 相册选取通用组件
@property (nonatomic, strong)VLSPhotoSelectView *photoSelectView;

@property (nonatomic, strong)VLSHeadAlertView *headAlert;

@end

@implementation VLSAnchorTitleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getHeartConfig];
    
    self.view.backgroundColor = [UIColor purpleColor];
    
    VLSVideoItem *item = [[VLSAgoraVideoService sharedInstance] createLocalVideoOnly];
    item.loadingView.hidden = YES;
    item.frame = self.view.bounds;
    [self.view addSubview:item];
    [self setupUI];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    if (self.coverImageView.image) {
        
        [self.uploadCoverButton setTitle:LocalizedString(@"PHOTO_CHANGE_COVER") forState:UIControlStateNormal];
    }
    [[VLSAgoraVideoService sharedInstance] startPreview];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[VLSAgoraVideoService sharedInstance] stopPreview];

}
- (void)setupUI {
    
    [self.view addSubview:self.cameraButton];
    self.cameraButton.sd_layout
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(self.view, 20)
    .widthIs(40)
    .heightIs(40);
    
    [self.view addSubview:self.effectView];
    self.effectView.sd_layout
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .topSpaceToView(self.view, 70)
    .heightIs(180);
    
    UIVisualEffectView *effect = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    effect.alpha = 0.5;
    [self.effectView insertSubview:effect atIndex:0];
    self.effectView.backgroundColor = [UIColor clearColor];
    effect.sd_layout
    .topSpaceToView(self.effectView, 0)
    .leftSpaceToView(self.effectView, 0)
    .rightSpaceToView(self.effectView, 0)
    .bottomSpaceToView(self.effectView, 0);
    
    [self.effectView addSubview:self.coverImageView];
    self.coverImageView.sd_layout
    .leftSpaceToView(self.effectView, 15)
    .topSpaceToView(self.effectView, 30)
    .widthIs(130)
    .heightIs(70);
    
    [self.coverImageView addSubview:self.waringLabel];
    self.waringLabel.sd_layout
    .centerXEqualToView(self.coverImageView)
    .centerYEqualToView(self.coverImageView)
    .widthIs(110)
    .heightIs(20);
    
    [self.effectView addSubview:self.titleTextView];
    self.titleTextView.sd_layout
    .leftSpaceToView(self.coverImageView, 15)
    .rightSpaceToView(self.effectView, 15)
    .topSpaceToView(self.effectView, 20)
    .heightIs(80);
    
    [self.effectView addSubview:self.titleLabel];
    self.titleLabel.sd_layout
    .leftSpaceToView(self.coverImageView, 10)
    .rightSpaceToView(self.effectView, 15)
    .topSpaceToView(self.effectView, 30)
    .heightIs(20);
    
    [self.effectView addSubview:self.lineLabel];
    self.lineLabel.sd_layout
    .leftSpaceToView(self.effectView, 0)
    .rightSpaceToView(self.effectView, 0)
    .topSpaceToView(self.coverImageView, 20)
    .heightIs(0.5);
    
    [self.effectView addSubview:self.verticalLabel];
    self.verticalLabel.sd_layout
    .topSpaceToView(self.lineLabel, 15)
    .centerXEqualToView(self.effectView)
    .widthIs(0.5)
    .heightIs(30);
    
    [self.effectView addSubview:self.locationImageView];
    self.locationImageView.sd_layout
    .leftSpaceToView(self.effectView, 30)
    .centerYEqualToView(self.verticalLabel)
    .widthIs(17)
    .heightIs(20);
    
    
    [self.effectView addSubview:self.locationLabel];
    [self.locationLabel setSingleLineAutoResizeWithMaxWidth:150];
    self.locationLabel.sd_layout
    .topSpaceToView(self.lineLabel, 18)
    .leftSpaceToView(self.locationImageView, 5)
    .heightIs(20);
    
    // 给 LocationLabel 添加一个手势，解决按钮触发范围太小的问题
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationTapAction)];
    [self.locationLabel addGestureRecognizer:tap];
    
    
    [self.effectView addSubview:self.locationCloseButton];
    self.locationCloseButton.sd_layout
    .leftSpaceToView(self.locationLabel, -5)
    .centerYEqualToView(self.verticalLabel)
    .heightIs(40)
    .widthIs(40);
    
    [self.effectView addSubview:self.uploadCoverButton];
    self.uploadCoverButton.sd_layout
    .leftSpaceToView(self.verticalLabel, 30)
    .rightSpaceToView(self.effectView, 30)
    .centerYEqualToView(self.verticalLabel)
    .heightIs(40);
    
    [self.view addSubview:self.weChatButton];
    self.weChatButton.sd_layout
    .topSpaceToView(self.effectView, 20)
    .centerXEqualToView(self.view)
    .widthIs(40)
    .heightIs(40);
    
    [self.view addSubview:self.zoneButton];
    self.zoneButton.sd_layout
    .rightSpaceToView(self.weChatButton, 15)
    .topSpaceToView(self.effectView, 20)
    .widthIs(40)
    .heightIs(40);
    
    [self.view addSubview:self.friendShipButton];
    self.friendShipButton.sd_layout
    .leftSpaceToView(self.weChatButton, 15)
    .topSpaceToView(self.effectView, 20)
    .widthIs(40)
    .heightIs(40);
    
    [self.view addSubview:self.qqButton];
    self.qqButton.sd_layout
    .rightSpaceToView(self.zoneButton, 15)
    .topSpaceToView(self.effectView, 20)
    .widthIs(40)
    .heightIs(40);
    
    [self.view addSubview:self.sinaButton];
    self.sinaButton.sd_layout
    .leftSpaceToView(self.friendShipButton, 15)
    .topSpaceToView(self.effectView, 20)
    .heightIs(40)
    .widthIs(40);
    
    [self.view addSubview:self.startButton];
    self.startButton.sd_layout
    .leftSpaceToView(self.view, 38)
    .rightSpaceToView(self.view, 38)
    .topSpaceToView(self.weChatButton, 26)
    .heightIs(40);
    
    [self.view addSubview:self.closeButton];
    self.closeButton.sd_layout
    .bottomSpaceToView(self.view, 54 / 667. * SCREEN_HEIGHT)
    .centerXEqualToView(self.view)
    .widthIs(40)
    .heightIs(40);
    
    [self.view addSubview:self.headAlert];
    
}

#pragma mark - 获取心跳信息配置
- (void)getHeartConfig {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
        }
    };
    [AccountManager getHeartConfigCallback:callback];
    
}

#pragma mark - 切换镜头
- (void)changeCamera
{
    [[VLSAgoraVideoService sharedInstance] switchCamera];
}

#pragma mark - 关闭地理位置按钮的方法
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            NSLog(@"Got authorization, start tracking location");
            [self startTrackingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [self.clLocationManager requestWhenInUseAuthorization];
        case kCLAuthorizationStatusDenied:
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"LIVEBEFORE_OPEN_LOCATION") preferredStyle:UIAlertControllerStyleAlert];
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"LIVEBEFORE_I_KONWEN") style:UIAlertActionStyleDefault handler: nil];
            
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }
        default:
            break;
    }
    
}
- (void)startTrackingLocation {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status == kCLAuthorizationStatusNotDetermined) {
        [self.clLocationManager requestWhenInUseAuthorization];
    }
    else if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways) {
        [self.clLocationManager startUpdatingLocation];
    }
}
- (void)locationCloseAction:(UIButton *)button {
    
    // 判断地理位置权限
    if (self.locationLabel.text == nil || [self.locationLabel.text isEqualToString:LocalizedString(@"LIVEBEFORE_LOCATION_CLOSE")]) {
        
        self.clLocationManager = [[CLLocationManager alloc] init];
        self.clLocationManager.delegate = self;
        [self startTrackingLocation];
    }
    
    
    
    button.selected = !button.selected;
    
    
    
    if (!button.selected) {
        
        [self.locationCloseButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self.locationLabel.text = LocalizedString(@"LIVEBEFORE_LOCATION_CLOSE");
        
    } else {
        
        [self.locationCloseButton setImage:[UIImage imageNamed:@"livebefore_close4"] forState:UIControlStateNormal];
        
        // 获取地理位置
        
        self.locationManager = [[SNLocationManager alloc] init];
        [self.locationManager startUpdatingLocationWithSuccess:^(CLLocation *location, CLPlacemark *placemark) {
            MyLog(@"%@", placemark.locality);
            if (!placemark.locality) {
                self.locationLabel.text = LocalizedString(@"LIVEBEFORE_LOCATION_CLOSE");
            } else {
                self.locationLabel.text = [NSString stringWithFormat:@"%@", placemark.locality];
            }
            
        } andFailure:^(CLRegion *region, NSError *error) {
            
        }];
    }
    
    if ([self.locationLabel.text isEqualToString:LocalizedString(@"LIVEBEFORE_LOCATION_CLOSE")]) {
        [self.locationCloseButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
}
#pragma mark - 用户点击地理位置也可以关闭定位的手势方法
- (void)locationTapAction {
    
    [self locationCloseAction:self.locationCloseButton];
    
}

#pragma mark - 上传封面按钮的方法
- (void)uploadCoverAction {
    
    [self.view addSubview:self.photoSelectView];
    
    if (!self.coverImageView.image) {
        self.photoSelectView.photoLabel.text = LocalizedString(@"LIVEBEFORE_UPLOAD_COVER");
    } else {
        self.photoSelectView.photoLabel.text = LocalizedString(@"PHOTO_CHANGE_COVER");
    }
    [self.photoSelectView showPhotoView:YES animation:YES];
    
    
}


#pragma mark - 微信分享按钮

- (void)weChatAction:(UIButton *)button {
    
    button.selected = !button.selected;
    if (button.selected) {
        
        self.qqButton.selected = NO;
        self.zoneButton.selected = NO;
        self.friendShipButton.selected = NO;
        self.sinaButton.selected = NO;
    }
}
#pragma mark - qq 空间分享按钮

- (void)zoneAction:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        
        self.qqButton.selected = NO;
        self.weChatButton.selected = NO;
        self.friendShipButton.selected = NO;
        self.sinaButton.selected = NO;
    }
}
#pragma mark - 朋友圈分享按钮

- (void)friendShipAction:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        
        self.qqButton.selected = NO;
        self.zoneButton.selected = NO;
        self.weChatButton.selected = NO;
        self.sinaButton.selected = NO;
    }
}
#pragma mark - QQ 分享按钮

- (void)qqAction:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        
        self.zoneButton.selected = NO;
        self.weChatButton.selected = NO;
        self.friendShipButton.selected = NO;
        self.sinaButton.selected = NO;
    }
}
#pragma mark - 微博分享按钮

- (void)sinaAction:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        
        self.qqButton.selected = NO;
        self.zoneButton.selected = NO;
        self.weChatButton.selected = NO;
        self.friendShipButton.selected = NO;
    }
    
}
#pragma mark - 开始直播按钮

- (void)startAction:(UIButton *)button {
    
    
    if (self.titleTextView.text.length > 4 && self.coverImageView.image && self.titleTextView.text.length < 61) {
        
        [self getUserRoomID];
        
    } else if (!self.titleTextView.text.length && self.coverImageView.image) {
        // 有封面无标题
        [self showErrorWaringWithString:LocalizedString(@"LIVEBEFORE_WARING_LIVE_TITLE")];
        self.titleLabel.hidden = NO;
        [self.titleTextView endEditing:YES];
        
    } else if (self.titleTextView.text.length < 5 && self.titleTextView.text.length > 0) {
        // 标题长度少于5个字符
        [self showErrorWaringWithString:LocalizedString(@"LIVEBEFORE_WARING_TITLE_MORE_FIVE")];
        [self.titleTextView endEditing:YES];
        
    }else if (!self.coverImageView.image) {
        // 封面为空
        [self showErrorWaringWithString:LocalizedString(@"LIVEBEFORE_WARING_UPLOAD_COVER")];
        
    }
}

- (void)shareThirdWithRoomID:(NSString *)roomID{
    
    for (int i = 0; i <= 4; i ++) {
        
        UIButton * btn = [self.view viewWithTag:i + 300];
        
        if (btn.selected == YES) {
            
            if ([ShareActionSheet isInstallation:i]) {
                
                UIImage *coverImage = _coverImageView.image;
                NSString *userName = [AccountManager sharedAccountManager].account.nickName;
                NSString *content = [NSString stringWithFormat:@"%@%@%@", LocalizedString(@"SHARE_HTML_URL"), [AccountManager sharedAccountManager].account.nickName, LocalizedString(@"SHARE_HTML_URL_OPEN_LIVE")];
                NSString *p4 = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];

                NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       coverImage,  @"coverImage",
                                       userName,    @"userName",
                                       roomID,          @"roomID",
                                       content,       @"content",
                                       p4,          @"userID",
                                       @YES,         @"overrideTitleInWXTimeline",
                                              nil];
                [self shareToPlatform:i messageObject:dict completion:^(NSDictionary *item) {
                    [self shareTrackingLiveBefore:roomID Index:i];
                }];
            }else{
                [self showAlterviewController:LocalizedString(@"SHARE_HTML_NO_INSTALL_APP")];
            }
        }
    }
}

#pragma mark - 关闭按钮

- (void)closeAction:(UIButton *)button {
    [[VLSAgoraVideoService sharedInstance] leaveChannel];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 获取直播的 roomID

- (void)getUserRoomID {
    
    [self showProgressHUDWithStr:LocalizedString(@"ALERT_OPENING") dimBackground:YES];
    
    PostFileModel *fileModel = [[PostFileModel alloc] init];
    fileModel.fileName = [NSUUID UUID].UUIDString;
    fileModel.fileArray = @[UIImageJPEGRepresentation(self.coverImageView.image, 0.2)];
    
    VLSCreateLiveModel *model = [[VLSCreateLiveModel alloc] init];
    model.liveTitle = _titleTextView.text;
    model.topics = @[@"海南",@"三亚"];
    
    if (!self.locationLabel.text) {
        [self showErrorWaringWithString:LocalizedString(@"LIVEBEFORE_WARING_LOCATION_CLOSED")];
        model.location = LocalizedString(@"LIVEBEFORE_LOCATION_CLOSE");
    } else {
        model.location = self.locationLabel.text;
    }
    model.fileModel = fileModel;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        NSString *roomId = [result objectForKey:@"roomId"];
        NSString *coverUrl = [[result objectForKey:@"liveShow"] objectForKey:@"coverUrl"];
        NSString *totaltickets = [[result objectForKey:@"totalGainTicket"] stringValue];
        /*
         *  加入频道前需把RDK和channelKey设置
         */
        [VLSAgoraVideoService sharedInstance].RDK = [result objectForKey:@"record-key"];
        [VLSAgoraVideoService sharedInstance].channelKey = [result objectForKey:@"channel-key"];
        
        NSLog(@"创建聊天室成功");
        [self hideProgressHUDAfterDelay:0];
        
        // 创建直播间成功后保存本次上传的封面
        NSData *coverData = UIImageJPEGRepresentation(self.coverImageView.image, 0.4);
        AccountModel *account = [AccountManager sharedAccountManager].account;
        NSString *key = [NSString stringWithFormat:@"%ld", (long)account.uid];
        [[NSUserDefaults standardUserDefaults] setObject:coverData forKey:key];
        
        [VLSIMManager sharedInstance].RoomID = roomId;
        VLSAnchorViewController *anchorVC = [[VLSAnchorViewController alloc] init];
        anchorVC.roomId = roomId;
        anchorVC.title = _titleTextView.text;
        anchorVC.coverUrl = coverUrl;
        anchorVC.anchorId = [NSString stringWithFormat:@"%ld",(long)account.uid];
        if (totaltickets) {
            anchorVC.anchorTickets = totaltickets;
        }
        UINavigationController *Nav = [[UINavigationController alloc]initWithRootViewController:anchorVC];
        
        [self presentViewController:Nav animated:NO completion:nil];
        
        //分享
        [self shareThirdWithRoomID:roomId];
        
       
        
    };
    
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [VLSLiveHttpManager createLive:model callback:callback];
    
}

#pragma mark - 错误警告提示

- (void)showErrorWaringWithString:(NSString *)string {
    
    self.headAlert.message = string;
    [self.headAlert showHeadAlertThenHidden];
}

#pragma mark - VLSPhotoSelectViewDelegate


- (void)jumpToSystemPhotoAlbum {
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void)jumpToSystemCamera {
    
    [[VLSAgoraVideoService sharedInstance] stopPreview];
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
    
}

- (void)closePhotoSelectView {
    [self.photoSelectView removeFromSuperview];
}



#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:NO completion:nil];
    
    [self.photoSelectView removeFromSuperview];
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
    image = [image fixOrientation:image];
    CropperViewController *cropperVC = [[CropperViewController alloc] initWithImage:image andStyle:ProportionStyle];
    cropperVC.delegate = self;
    [self presentViewController:cropperVC animated:YES completion:nil];
    
}

#pragma mark - CropperViewControllerDelegate

-(void)AfterCuttingThePictures:(UIImage *)image{
    self.coverImageView.image = image;
    self.waringLabel.text = @"";
    
    if (self.titleTextView.text.length > 4) {
        [self.startButton setBackgroundImage:[UIImage imageNamed:@"button_red"] forState:UIControlStateNormal];
        
    } else if (!self.titleLabel.text.length){
        
        [self.startButton setBackgroundImage:[UIImage imageNamed:@"button_write_shadows"] forState:UIControlStateNormal];
        
        self.titleLabel.text = LocalizedString(@"LIVEBEFORE_LABEL_LIVE_TITLE");
        
    } else {
        [self.startButton setBackgroundImage:[UIImage imageNamed:@"button_write_shadows"] forState:UIControlStateNormal];
    }
    
}


#pragma mark UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    self.titleLabel.hidden = YES;
    [self.warningImageView removeFromSuperview];
    [self.titleTextView endEditing:YES];
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    
    if (textView.text.length > 60 && textView.markedTextRange == nil) {
        
        // 标题长度多于60个字符
        [self showErrorWaringWithString:LocalizedString(@"LIVEBEFORE_WARING_TITLE_LESS_SIXTY")];
        [self.titleTextView endEditing:YES];
        
        textView.text = [textView.text substringToIndex:60];
    }
    
    if (textView.markedTextRange == nil) {
        self.titleLabel.hidden = YES;
        if (textView.text.length > 4 && self.coverImageView.image) {
            [self.startButton setBackgroundImage:[UIImage imageNamed:@"button_red"] forState:UIControlStateNormal];
            
        } else {
            
            [self.startButton setBackgroundImage:[UIImage imageNamed:@"button_write_shadows"] forState:UIControlStateNormal];
            
            self.titleLabel.text = LocalizedString(@"LIVEBEFORE_LABEL_LIVE_TITLE");
            
        }
    }
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if (!textView.text.length) {
        self.titleLabel.hidden = NO;
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"] || range.location >= 60) {
        [textView resignFirstResponder];
        return NO;
    } else {
        return YES;
    }
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.titleTextView endEditing:YES];
}

#pragma mark - getters

- (UIButton *)cameraButton {
    if (!_cameraButton) {
        self.cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cameraButton setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
        [_cameraButton addTarget:self action:@selector(changeCamera) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _cameraButton;
}

- (UIView *)effectView {
    if (!_effectView) {
        self.effectView = [[UIView alloc] init];
        
    }
    return _effectView;
}

- (UIImageView *)coverImageView {
    if (!_coverImageView) {
        self.coverImageView = [[UIImageView alloc] init];
        self.coverImageView.backgroundColor = [UIColor blackColor];
        self.coverImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadCoverAction)];
        [self.coverImageView addGestureRecognizer:tap];
        
        // 如果之前上传过封面，就取上一次上传的封面
        AccountModel *account = [AccountManager sharedAccountManager].account;
        NSString *key = [NSString stringWithFormat:@"%ld", (long)account.uid];
        ;
        if ([[NSUserDefaults standardUserDefaults] objectForKey:key]) {
            self.coverImageView.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:key]];
        }
        
        
    }
    return _coverImageView;
}
- (UITextView *)titleTextView {
    if (!_titleTextView) {
        self.titleTextView = [[UITextView alloc] init];
        self.titleTextView.returnKeyType = UIReturnKeyDone;
        self.titleTextView.backgroundColor = [UIColor clearColor];
        self.titleTextView.textColor = [UIColor whiteColor];
        self.titleTextView.tintColor = [UIColor whiteColor];
        self.titleTextView.font = [UIFont systemFontOfSize:SIZE_FONT_17];
        self.titleTextView.delegate = self;
        
    }
    return _titleTextView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.text = LocalizedString(@"LIVEBEFORE_LABEL_LIVE_TITLE");
        self.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_18];
        self.titleLabel.textColor = [UIColor whiteColor];
    }
    return _titleLabel;
}
- (UIView *)lineLabel {
    if (!_lineLabel) {
        self.lineLabel = [[UILabel alloc] init];
        self.lineLabel.backgroundColor = [UIColor grayColor];
        
    }
    return _lineLabel;
}
- (UIView *)verticalLabel {
    if (!_verticalLabel) {
        self.verticalLabel = [[UILabel alloc] init];
        self.verticalLabel.backgroundColor = [UIColor grayColor];
    }
    return _verticalLabel;
}
- (UIImageView *)locationImageView {
    if (!_locationImageView) {
        self.locationImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"livebefore_loc_open"]];
        
    }
    return _locationImageView;
}

- (UILabel *)locationLabel {
    if (!_locationLabel) {
        self.locationLabel = [[UILabel alloc] init];
        self.locationLabel.textColor = [UIColor whiteColor];
        self.locationLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        self.locationLabel.userInteractionEnabled = YES;
        
        self.locationManager = [[SNLocationManager alloc] init];
        // 获取地理位置
        [self.locationManager startUpdatingLocationWithSuccess:^(CLLocation *location, CLPlacemark *placemark) {
            if (!placemark.locality) {
                self.locationLabel.text = LocalizedString(@"LIVEBEFORE_LOCATION_CLOSE");
            } else {
                self.locationLabel.text = [NSString stringWithFormat:@"%@", placemark.locality];
            }
        } andFailure:^(CLRegion *region, NSError *error) {
            
        }];
    }
    return _locationLabel;
}


- (UIButton *)locationCloseButton {
    
    if (!_locationCloseButton) {
        self.locationCloseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.locationCloseButton setImage:[UIImage imageNamed:@"livebefore_close4"] forState:UIControlStateNormal];
        [self.locationCloseButton addTarget:self action:@selector(locationCloseAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _locationCloseButton;
}

- (UIButton *)uploadCoverButton {
    if (!_uploadCoverButton) {
        self.uploadCoverButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.uploadCoverButton setImage:[UIImage imageNamed:@"livebefore_picture"] forState:UIControlStateNormal];
        [self.uploadCoverButton setTitle:LocalizedString(@"LIVEBEFORE_UPLOAD_COVER") forState:UIControlStateNormal];
        self.uploadCoverButton.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
        self.uploadCoverButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        [self.uploadCoverButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.uploadCoverButton addTarget:self action:@selector(uploadCoverAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _uploadCoverButton;
}

- (UIButton *)weChatButton {
    
    if (!_weChatButton) {
        self.weChatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.weChatButton setImage:[UIImage imageNamed:@"wechat"] forState:UIControlStateNormal];
        self.weChatButton.tag = 302;
        [self.weChatButton setImage:[UIImage imageNamed:@"wechat_choose"] forState:UIControlStateSelected];
        [self.weChatButton addTarget:self action:@selector(weChatAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _weChatButton;
}

- (UIButton *)zoneButton {
    if (!_zoneButton) {
        self.zoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.zoneButton setImage:[UIImage imageNamed:@"QQzone"] forState:UIControlStateNormal];
        self.zoneButton.tag = 301;
        [self.zoneButton setImage:[UIImage imageNamed:@"QQzone_choose"] forState:UIControlStateSelected];
        [self.zoneButton addTarget:self action:@selector(zoneAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _zoneButton;
}

- (UIButton *)friendShipButton {
    if (!_friendShipButton) {
        self.friendShipButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.friendShipButton setImage:[UIImage imageNamed:@"friends-choose"] forState:UIControlStateNormal];
        [self.friendShipButton setImage:[UIImage imageNamed:@"friends_choose"] forState:UIControlStateSelected];
        self.friendShipButton.tag = 303;
        [self.friendShipButton addTarget:self action:@selector(friendShipAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _friendShipButton;
}

- (UIButton *)qqButton {
    if (!_qqButton) {
        self.qqButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.qqButton setImage:[UIImage imageNamed:@"QQ"] forState:UIControlStateNormal];
        self.qqButton.tag = 300;
        [self.qqButton setImage:[UIImage imageNamed:@"QQ_choose"] forState:UIControlStateSelected];
        [self.qqButton addTarget:self action:@selector(qqAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _qqButton;
}

- (UIButton *)sinaButton {
    if (!_sinaButton) {
        self.sinaButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.sinaButton setImage:[UIImage imageNamed:@"sina"] forState:UIControlStateNormal];
        self.sinaButton.tag = 304;
        [self.sinaButton setImage:[UIImage imageNamed:@"sina_choose"] forState:UIControlStateSelected];
        [self.sinaButton addTarget:self action:@selector(sinaAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sinaButton;
}


- (UIButton *)startButton {
    if (!_startButton) {
        self.startButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.startButton setBackgroundImage:[UIImage imageNamed:@"button_write_shadows"] forState:UIControlStateNormal];
        [self.startButton addTarget:self action:@selector(startAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.startButton setTitle:LocalizedString(@"LIVEBEFORE_BUTTON_START_LIVE") forState:UIControlStateNormal];
        self.startButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        [self.startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    return _startButton;
}


- (UIButton *)closeButton {
    if (!_closeButton) {
        self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeButton setBackgroundImage:[UIImage imageNamed:@"livebefore_close3"] forState:UIControlStateNormal];
        [self.closeButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _closeButton;
}


- (UIImageView *)warningImageView {
    if (!_warningImageView) {
        self.warningImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"livebefore_warn"]];
    }
    return _warningImageView;
}

- (UIImageView *)coverWaringImage {
    if (!_coverWaringImage) {
        self.coverWaringImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"livebefore_warn"]];
    }
    return _coverWaringImage;
}

- (UILabel *)waringLabel {
    if (!_waringLabel) {
        self.waringLabel = [[UILabel alloc] init];
        self.waringLabel.textColor = [UIColor whiteColor];
        self.waringLabel.textAlignment = NSTextAlignmentCenter;
        self.waringLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        
        // 如果之前上传过封面，就取上一次上传的封面
        AccountModel *account = [AccountManager sharedAccountManager].account;
        NSString *key = [NSString stringWithFormat:@"%ld", (long)account.uid];
        ;
        if (![[NSUserDefaults standardUserDefaults] objectForKey:key]) {
            self.waringLabel.text = LocalizedString(@"LIVEBEFORE_UPLOAD_COVER");
        }
    }
    return _waringLabel;
}

- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
    }
    return _imagePicker;
}

- (SNLocationManager *)locationManager {
    
    if (!_locationManager) {
        
        
    }
    return _locationManager;
}

- (VLSHeadAlertView *)headAlert
{
    if (_headAlert == nil) {
        _headAlert = [[VLSHeadAlertView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    }
    return _headAlert;
}
-(UIView *)photoSelectView{
    
    if (!_photoSelectView) {
        
        _photoSelectView = [[VLSPhotoSelectView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _photoSelectView.delegate = self;
        _photoSelectView.photoLabel.text = LocalizedString(@"PHOTO_CHANGE_COVER");
        [_photoSelectView showPhotoView:NO animation:NO];
        
    }
    
    return _photoSelectView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
