//
//  LiveBeforeTestViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/7/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "VLSLiveBeforeInfoView.h"

@interface LiveBeforeTestViewController : VLSBaseViewController

@property (nonatomic, strong)VLSLiveBeforeInfoView *infoView;
@property (nonatomic, strong)NSString *topicStr;
@property (nonatomic, strong, nullable) NSNumber* lessonId;
@property (nonatomic, strong)NSString *liveBeforeTitle;

- (void)getUserRoomID:(UINavigationController*)nav;

@end
