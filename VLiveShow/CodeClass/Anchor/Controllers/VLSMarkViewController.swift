//
//  VLSMarkViewController.swift
//  VLiveShow
//
//  Created by rock on 2017/1/17.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSMarkViewControllerDelegate {
    func marked(markedId: String, markedName: String)
}

class VLSMarkViewController: VLSCollectionViewController {

    private var backView: UIView!
    var markname: String?
    weak var delegate: VLSMarkViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        
        self.markView()
        
        collectionView.register(VLSMarkCollectionViewCell.self, forCellWithReuseIdentifier: "markcollectionCell")
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "markcollectionHeader")
        let flowLayout = UICollectionViewFlowLayout().then {
            $0.scrollDirection = .vertical
        }
        collectionView.collectionViewLayout = flowLayout
        collectionView.isPagingEnabled = false
        collectionView.backgroundColor = HEX(COLOR_BG_EEEEEE)
        
        self.collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(backView.snp.bottom)
            make.left.bottom.right.equalTo(self.view)
        }
        
        viewModel.api = APIConfiguration.shared().getHostMarkListUrl
        viewModel.itemClass = VLSMarkModel.self

        // Do any additional setup after loading the view.
    }
    
    func markView() {
        backView = UIView().then {
            $0.backgroundColor = HEX(COLOR_BG_FFFFFF)
        }
        self.view.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.equalTo(200)
            make.left.right.equalTo(self.view)
            make.height.equalTo(40)
        }
        let markLab = UILabel().then {
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.textColor = HEX(COLOR_FONT_9B9B9B)
            $0.textAlignment = .center
            $0.text = LocalizedString("LIVEBEFORE_MARK_GOOD_FIELD")
            $0.sizeToFit()
        }
        backView.addSubview(markLab)
        markLab.snp.makeConstraints { (make) in
            make.top.bottom.centerX.equalTo(backView)
        }
        let deleteBt = UIButton().then {
            $0.setImage(#imageLiteral(resourceName: "but_ad._close.png"), for: .normal)
            $0.addTarget(self, action: #selector(VLSMarkViewController.disappear), for: .touchUpInside)
        }
        backView.addSubview(deleteBt)
        deleteBt.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(backView)
            make.right.equalTo(backView).offset(-20)
        }
    }
    
    func disappear() {
        self.view.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
    }
    
    override func addErrorView() {
        let errorView = VLSEmptyView(frame: self.view.bounds).then {
            $0.delegate = self
            $0.errorImageHidden(true)
            $0.errorMsg(LocalizedString("VLS_LIVE_MARK_DEFAULT_TEXT"))
            $0.errorBtHidden(true)
        }
        self.addErrorView(errorView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.reloadData("list")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension VLSMarkViewController: VLSMarkCollectionViewCellDelegate {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let model = ObjectForKeySafety(viewModel.dataSource, index: section) as? VLSMarkModel {
            return model.data.count
        } else {
            return 0
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "markcollectionHeader", for: indexPath)
        let headerLab = UILabel().then {
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.textColor = HEX(COLOR_FONT_9B9B9B)
            $0.textAlignment = .left
            $0.sizeToFit()
        }
        header.addSubview(headerLab)
        headerLab.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(header)
            make.left.equalTo(header).offset(scheduleSpace)
        }
        if let model = ObjectForKeySafety(viewModel.dataSource, index: indexPath.section) as? VLSMarkModel {
            headerLab.text = model.name
        }
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width/4, height: 40)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "markcollectionCell", for: indexPath)
        if let model = ObjectForKeySafety(viewModel.dataSource, index: indexPath.section) as? VLSMarkModel {
            if let item = ObjectForKeySafety(NSArraySafty(model.data), index: indexPath.row) {
                if let cell = cell as? VLSMarkCollectionViewCell {
                    if let newItem = item as? VLSMarkSubTypeModel {
                        if markname == newItem.name {
                            newItem.isSelected = true
                        }
                        cell.setNewItem(newItem)
                        cell.delegate = self
                        cell.indexPath = indexPath
                    }
                }
            }
        }
        return cell
    }
    
    func marked(model: VLSMarkSubTypeModel, indexPath: IndexPath) {
        self.disappear()
        self.delegate?.marked(markedId: StringSafty(model.topicId), markedName: StringSafty(model.name))
    }
}
