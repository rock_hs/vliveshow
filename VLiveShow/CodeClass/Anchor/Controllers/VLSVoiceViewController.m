//
//  VLSVoiceViewController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/21.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSVoiceViewController.h"
#import "VLSVideoItem.h"
#import "VLSAVMultiManager.h"
#import "VLSMuteModel.h"
@interface VLSVoiceViewController()
{
    NSMutableArray *muteModels;
}
@property (nonatomic, strong) NSMutableArray<VLSMuteModel *> *selectIds;
@end
@implementation VLSVoiceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = LocalizedString(@"ANCHOR_MUTE_MIC_MANAGER");
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"CANCLE") style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"INVITE_SURE") style:UIBarButtonItemStylePlain target:self action:@selector(sure:)];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    [self.navigationItem.leftBarButtonItem setTintColor:RGB16(COLOR_FONT_4A4A4A)];
    self.navigationItem.rightBarButtonItem = rightItem;
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor redColor]];
    self.view.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
    muteModels = [NSMutableArray arrayWithCapacity:0];
    [self reloadData];
    
}

- (void)reloadData
{
    [muteModels removeAllObjects];
    
    for (int i=0; i<self.videoViews.count; i++) {
        VLSVideoItem *view = [self.videoViews objectAtIndex:i];
        VLSMuteModel *model = [[VLSMuteModel alloc] init];
        model.userId = view.model.userId;
        model.muteVoice = view.model.muteVoice;
        [muteModels addObject:model];
    }
    
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    [self initVoiceImage];
}


- (void)cancel:(UIBarButtonItem *)item
{
    for (int i= 0; i<self.videoViews.count; i++) {
        UIView *v = [self.videoViews objectAtIndex:i];
        if ([v isKindOfClass:[VLSVideoItem class]]) {
            VLSVideoItem *view = (VLSVideoItem *)v;
            view.model = view.model;
        }
    }
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

- (void)sure:(UIBarButtonItem *)item
{
    //禁麦
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
  
    };
    callback.errorBlock = ^(id result){
        
    };
    [[VLSAVMultiManager sharedManager] anchorMuteModels:muteModels callback:callback];

    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

- (void)click:(UIButton *)sender
{
    sender.selected = !sender.selected;
    NSInteger tag = sender.tag -100;
    VLSMuteModel *model = [muteModels objectAtIndex:tag];
    if (sender.selected ) {
        model.muteVoice = YES;
    }else{
        model.muteVoice = NO;
    }
}

- (void)initVoiceImage
{
    BOOL selfIsAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:[VLSMessageManager sharedManager].host.userId];
    BOOL selfIsSuperUser = [AccountManager sharedAccountManager].account.isSuperUser;
    for (int i= 0; i<self.videoViews.count; i++) {
        UIView *view = [self.videoViews objectAtIndex:i];
        if ([view isKindOfClass:[VLSVideoItem class]]) {
            VLSVideoItem *vi = (VLSVideoItem *)view;
            vi.muteButton.hidden = YES;
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            if (view.frame.size.width == SCREEN_WIDTH) {
                UIImage* bgImage = [UIImage imageNamed:@"mute_zhubo"];
                
                button.frame = CGRectMake(0, 0, bgImage.size.width, bgImage.size.height);
                [button setBackgroundImage:bgImage forState:UIControlStateNormal];
                [button setBackgroundImage:[UIImage imageNamed:@"mute_zhubo_selected"] forState:UIControlStateSelected];

            }else{
                UIImage* bgImage = [UIImage imageNamed:@"mute_jiabin"];
                
                button.frame = CGRectMake(0, 0, bgImage.size.width, bgImage.size.height);
                
                [button setBackgroundImage: bgImage forState:UIControlStateNormal];
                [button setBackgroundImage:[UIImage imageNamed:@"mute_jiabin_selected"] forState:UIControlStateSelected];
            }
            
            [button setBackgroundImage:[UIImage imageNamed:@"mute_jiabin_disselected"] forState:UIControlStateDisabled];
   
            button.tag = 100+i;
            VLSMuteModel *model = [muteModels objectAtIndex:i];
            
            if (selfIsAnchor || selfIsSuperUser) {
                if (model.muteVoice) {//已静音
                    if (!model.muteVoiceBySelf) {//被自己静音
                        button.selected = YES;
                    } else {
                        button.enabled = NO;
                    }
                }else{
                    button.selected = model.muteVoice;
                }
            } else {
                if (model.muteVoice) {//已静音
                    if (model.muteVoiceBySelf) {//被自己静音
                        button.selected = YES;
                    } else {
                        button.enabled = NO;
                    }
                }else{
                    button.selected = model.muteVoice;
                }
            }            
            button.center = view.center;
            [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
        }
    }
}

@end
