//
//  VLSAnchorRelationShipViewController.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
typedef void (^NetSucc)(NSInteger count);
typedef void (^NetFail)();

@interface VLSAnchorRelationShipViewController : VLSBaseViewController

@end
