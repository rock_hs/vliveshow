//
//  VLSAnchorTitleViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/5/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "LiveBeforeTestViewController.h"
#import "VLSAnchorViewController.h"
#import "VLSPhotoSelectView.h"
#import "CropperViewController.h"
#import "VLSShareView.h"
#import "VLSNavigationController.h"
#import "AppDelegate.h"
#import "VLiveShow-Swift.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VLSNavigationController.h"

#define ImageCompressionRate 0.4

@interface LiveBeforeTestViewController ()<VLSPhotoSelectViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropperViewControllerDelegate, VLSLiveBeforeInfoViewDelegate, VLSShareViewDelegate, VLSAgoraVideoServiceDelegate, VLSMarkViewControllerDelegate>

@property (nonatomic, strong)UIButton *closeButton;
@property (nonatomic, strong)VLSPhotoSelectView *photoSelectView;
@property (nonatomic, strong)UIImagePickerController *imagePicker;
@property (nonatomic, strong)UIButton *cameraButton;
@property (nonatomic, strong)VLSHeadAlertView *headAlert;
@property (nonatomic, strong)VLSShareView *shareView;
@property (nonatomic, strong)UIView *bgView;
@property (nonatomic, strong)UIButton *startButton;
@property (nonatomic, assign)BOOL shareFlag;
@property (nonatomic, assign)NSInteger shareIndex;
@property (nonatomic, strong)NSMutableArray *moreMarkLists;

@end

@implementation LiveBeforeTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self getHostMarkList];
    [self getHeartConfig];
    [self setupUI];
    [self fetchCourseInfo];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[VLSAgoraVideoService sharedInstance] startPreview];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appHasGoneInForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)appHasGoneInForeground {
    [self.infoView updateLocationChangeWithNoAuthorInSetting];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[VLSAgoraVideoService sharedInstance] stopPreview];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    app.presentLivingController = nil;
}

- (void)setupUI {
//    [VLSAgoraVideoService sharedInstance].delegate = self;
    UIView *maskView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    maskView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.35f];
    [self.view addSubview:maskView];
    VLSVideoItem *item = [[VLSAgoraVideoService sharedInstance] createLocalVideoOnly];
    item.loadingView.hidden = YES;
    item.frame = self.view.bounds;
    [self.view addSubview:item];

    [self.view addSubview:self.bgView];
    [self.bgView addSubview:self.infoView];
    [self.bgView addSubview:self.closeButton];
    [self.bgView addSubview:self.cameraButton];
    [self.bgView addSubview:self.headAlert];
    [self.bgView addSubview:self.shareView];
    [self.bgView addSubview:self.startButton];
    
    self.cameraButton.sd_layout
    .rightSpaceToView(self.bgView, 16)
    .topSpaceToView(self.bgView, 30)
    .widthIs(X_CONVERT(21.))
    .heightIs(Y_CONVERT(20.));
    
    self.closeButton.sd_layout
    .bottomSpaceToView(self.bgView, Y_CONVERT(45.))
    .centerXEqualToView(self.bgView)
    .widthIs(36)
    .heightIs(36);
    
    self.infoView.sd_layout
    .leftSpaceToView(self.bgView, 15)
    .rightSpaceToView(self.bgView, 15)
    .topSpaceToView(self.bgView, Y_CONVERT(90.))
    .heightIs(130);

    self.shareView.sd_layout
    .topSpaceToView(self.infoView, 20)
    .leftSpaceToView(self.bgView, 0)
    .rightSpaceToView(self.bgView, 0)
    .heightIs(40);
    
    self.startButton.sd_layout
    .leftSpaceToView(self.bgView, 38)
    .rightSpaceToView(self.bgView, 37)
    .topSpaceToView(self.shareView, Y_CONVERT(20.))
    .heightIs(40);
}


- (void)changeCamera {
    [self.infoView.titleTextView endEditing:YES];
    [[VLSAgoraVideoService sharedInstance] switchCamera];
}

- (void)closeAction:(UIButton *)button {
    [self.infoView.titleTextView endEditing:YES];
    [[VLSAgoraVideoService sharedInstance] stopPreview];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - VLSAgoraVideoServiceDelegate

- (void)agoraListenerNetworkQuality:(NSUInteger)quality {
    if (quality >= AgoraRtc_Quality_Poor) {
        [self.headAlert showLeftImage:[UIImage imageNamed:@"ic_bc_single_yel"] WithTitle:LocalizedString(@"ALERT_NETWORK_POOR")];
    }
}

#pragma mark - VLSLiveBeforeInfoViewDelegate

- (void)clickLocationBtn {
    [self.infoView.titleTextView endEditing:YES];
}

- (void)showMarkView {
    /**  隐藏“更多”浮层显示，直接进入标签列表  --2016.12.01 13:52:00
    [self.infoView.titleTextView endEditing:YES];
    
    [self.view addSubview:self.markView];
    self.markView.sd_layout
    .rightSpaceToView(self.view, 13)
    .topSpaceToView(self.infoView, -10)
    .widthIs(163. / 375 * SCREEN_WIDTH)
    .heightIs([self.markView getMarkViewHeight]);
     */
    [self clickMoreMark];
}

- (void)getLiveBeforeTitleWithString:(NSString *)string {
    if (string.length > 4 && self.infoView.coverImage && ![self.infoView.markBtn.markLabel.text isEqualToString:LocalizedString(@"LIVEBEFORE_MARK_SELECT")]) {
        [self startBtnEnable:YES];
    } else {
        [self startBtnEnable:NO];
    }
    self.liveBeforeTitle = string;
}

- (void)showTopErrorWaring {
    [self.headAlert showTitleWithString:LocalizedString(@"LIVEBEFORE_WARING_TITLE_LESS_SIXTY")];
}

- (void)selectPhotoWithImage:(UIImage *)coverImage {
    [self.view addSubview:self.photoSelectView];
    
    if (!coverImage) {
        self.photoSelectView.photoLabel.text = LocalizedString(@"LIVEBEFORE_UPLOAD_COVER");
    } else {
        self.photoSelectView.photoLabel.text = LocalizedString(@"PHOTO_CHANGE_COVER");
    }
    [self.photoSelectView showPhotoView:YES animation:YES];
    
    
}

#pragma mark - VLSPhotoSelectViewDelegate

- (void)jumpToSystemPhotoAlbum {
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void)jumpToSystemCamera {
    [[VLSAgoraVideoService sharedInstance] stopPreview];
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
    
}

- (void)closePhotoSelectView {
    [self.photoSelectView removeFromSuperview];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:NO completion:nil];
    
    [self.photoSelectView removeFromSuperview];
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
    image = [image fixOrientation:image];
    CropperViewController *cropperVC = [[CropperViewController alloc] initWithImage:image andStyle:ProportionStyle];
    cropperVC.delegate = self;
    [self presentViewController:cropperVC animated:YES completion:nil];
    
}
#pragma mark - CropperViewControllerDelegate

-(void)AfterCuttingThePictures:(UIImage *)image{
    
    self.infoView.coverView.coverImageView.image = image;
    
    if (self.liveBeforeTitle.length > 4 && ![self.infoView.markBtn.markLabel.text isEqualToString:LocalizedString(@"LIVEBEFORE_MARK_SELECT")]) {
        [self startBtnEnable:YES];
    } else {
        [self startBtnEnable:NO];
    }
    
}

#pragma mark - VLSLiveBeforeMarkViewDelegate

- (void)transferMarkString:(NSString *)markStr WithMarkID:(NSString *)markID{
    self.infoView.markBtn.markLabel.text = markStr;
    self.topicStr = [NSString stringWithFormat:@"%@,%@", markID, markStr];
    
    if (self.infoView.titleTextView.text.length > 4 && self.infoView.coverImage && ![markStr isEqualToString:LocalizedString(@"LIVEBEFORE_MARK_SELECT")]) {
        [self startBtnEnable:YES];
    } else {
        [self startBtnEnable:NO];
    }

}

- (void)clickMoreMark {
    VLSMarkViewController* markVC = [[VLSMarkViewController alloc] init];
    markVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    markVC.delegate = self;
    markVC.markname = self.infoView.markBtn.markLabel.text;
    [self presentViewController:markVC animated:YES completion:^{
        [UIView animateWithDuration:0.25 animations:^{
            markVC.view.backgroundColor = [UIColor colorWithWhite:0.25 alpha:0.25];
        }];
    }];
}

#pragma mark - VLSMarkViewControllerDelegate

- (void)markedWithMarkedId:(NSString * _Nonnull)markedId markedName:(NSString * _Nonnull)markedName
{
    self.infoView.markBtn.markLabel.text = markedName;
    self.topicStr = [NSString stringWithFormat:@"%@,%@", markedId, markedName];
    
    if (self.infoView.titleTextView.text.length > 4
        && self.infoView.coverImage
        && ![self.infoView.markBtn.markLabel.text isEqualToString:LocalizedString(@"LIVEBEFORE_MARK_SELECT")]) {
        [self startBtnEnable:YES];
    } else {
        [self startBtnEnable:NO];
    }
}

#pragma mark - VLSShareViewDelegate

-(void)shareBtnTriggerWithIndex:(ShareBtnIndex)index AndState:(BOOL)state {
    [self.infoView.titleTextView endEditing:YES];
    self.shareIndex = index;
    self.shareFlag = state;
}

- (void)startAction:(UIButton *)button {
    
    self.lessonId = nil;
    [self.infoView.titleTextView endEditing:YES];

    if (self.liveBeforeTitle.length > 4
        && self.infoView.coverImage
        && self.liveBeforeTitle.length <= LIVE_BEFORE_TITLE_COUNT
        && ![self.infoView.markBtn.markLabel.text isEqualToString:LocalizedString(@"LIVEBEFORE_MARK_SELECT")]) {
        [[VLSAgoraVideoService sharedInstance] enableNetworkTest];
        [self getUserRoomID];
        
    } else if (!self.liveBeforeTitle.length && self.infoView.coverImage) {
        // 有封面无标题
        [self.headAlert showTitleWithString:LocalizedString(@"LIVEBEFORE_WARING_LIVE_TITLE")];
        
    } else if (self.liveBeforeTitle.length < 5 && self.liveBeforeTitle.length > 0) {
        // 标题长度少于5个字符
        [self.headAlert showTitleWithString:LocalizedString(@"LIVEBEFORE_WARING_TITLE_MORE_FIVE")];
        
    }else if (!self.infoView.coverImage) {
        // 封面为空
        [self.headAlert showTitleWithString:LocalizedString(@"LIVEBEFORE_WARING_UPLOAD_COVER")];
    } else if ([self.infoView.markBtn.markLabel.text isEqualToString:LocalizedString(@"LIVEBEFORE_MARK_SELECT")]) {
        // 没有选择标签
        [self.headAlert showTitleWithString:LocalizedString(@"LIVEBEFORE_WARING_SELECT_MARK")];
    }
}

- (void)getUserRoomID {
    [self getUserRoomID:nil];
}

- (void)getUserRoomID:(UINavigationController*)nav {
    
    [self showProgressHUD:LocalizedString(@"ALERT_OPENING")];    
    PostFileModel *fileModel = [[PostFileModel alloc] init];
    fileModel.fileName = [NSUUID UUID].UUIDString;
    if (self.infoView.coverImage != nil) {
        fileModel.fileArray = @[UIImageJPEGRepresentation(self.infoView.coverImage, ImageCompressionRate)];
    }
    
    VLSCreateLiveModel *model = [[VLSCreateLiveModel alloc] init];
    model.liveTitle = self.liveBeforeTitle;
    model.topics = self.topicStr;
    model.geoLocaltion = self.infoView.geoLocation;
    model.lessonId = self.lessonId;
    model.fileModel = fileModel;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        NSString *roomId = [result objectForKey:@"roomId"];
        NSString *coverUrl = [[result objectForKey:@"liveShow"] objectForKey:@"coverUrl"];
        /*
         *  加入频道前需把RDK和channelKey设置
         */
        [VLSAgoraVideoService sharedInstance].RDK = [result objectForKey:@"record-key"];
        [VLSAgoraVideoService sharedInstance].channelKey = [result objectForKey:@"channel-key"];
        [[VLSAgoraVideoService sharedInstance] disableNetworkTest];

        NSLog(@"创建聊天室成功");
        [self hideProgressHUDAfterDelay:0];
        
        // 创建直播间成功后保存本次上传的封面
        NSData *coverData = UIImageJPEGRepresentation(self.infoView.coverImage, ImageCompressionRate);
        NSLog(@"==> Cover size: %ld", [coverData length]);
        AccountModel *account = [AccountManager sharedAccountManager].account;
        NSString *key = [NSString stringWithFormat:@"%ld", (long)account.uid];
        [[NSUserDefaults standardUserDefaults] setObject:coverData forKey:key];
        NSString *totaltickets = [[result objectForKey:@"totalGainTicket"] stringValue];
        
        [VLSIMManager sharedInstance].RoomID = roomId;
        VLSAnchorViewController *anchorVC = [[VLSAnchorViewController alloc] init];
        anchorVC.roomId = roomId;
        anchorVC.title = self.liveBeforeTitle;
        anchorVC.coverUrl = coverUrl;
        anchorVC.anchorId = [NSString stringWithFormat:@"%ld",(long)account.uid];
        if (self.lessonId) {
            [anchorVC setActionType: ActionTypeLessonTeacher];
        }else {
            [anchorVC setActionType:ActionTypeAnchor];
        }
        if (totaltickets) {
            anchorVC.anchorTickets = totaltickets;
        }
        anchorVC.lessonId = [NSString stringWithFormat: @"%@", self.lessonId];
        anchorVC.hidesBottomBarWhenPushed = YES;
        // 分享
        [self shareThirdWithRoomID:roomId];
        
        if (self.shareFlag) {
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            app.presentLivingController = ^(void){
                VLSNavigationController *Nav = [[VLSNavigationController alloc]initWithRootViewController:anchorVC];
                [self presentViewController:Nav animated:NO completion:nil];
                
            };
        } else {
            if (nav != nil) {
                [nav pushViewController:anchorVC animated:YES];
            } else {
                VLSNavigationController *Nav = [[VLSNavigationController alloc]initWithRootViewController:anchorVC];
                [self presentViewController:Nav animated:NO completion:nil];
            }
        }
        
        
    };
    callback.loadBlock = ^(id result){
    
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [VLSLiveHttpManager createLive:model callback:callback];
    
}

- (void)shareThirdWithRoomID:(NSString *)roomID{
    
    if (self.shareFlag) {
        
        if ([ShareActionSheet isInstallation:self.shareIndex]) {
            
            UIImage *coverImage = self.infoView.coverImage;
            NSString *userName = [AccountManager sharedAccountManager].account.nickName;
            NSString *content = [NSString stringWithFormat:@"%@%@%@", LocalizedString(@"SHARE_HTML_URL"), [AccountManager sharedAccountManager].account.nickName, LocalizedString(@"SHARE_HTML_URL_OPEN_LIVE")];
            NSString *p4 = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
            
            NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          coverImage,  @"coverImage",
                                          userName,    @"userName",
                                          roomID,          @"roomID",
                                          content,       @"content",
                                          p4,          @"userID",
                                          @YES,         @"overrideTitleInWXTimeline",
                                          nil];
            [self shareToPlatform:self.shareIndex messageObject:dict completion:^(NSDictionary *item) {
                [self shareTrackingLiveBefore:roomID Index:self.shareIndex];
            }];
        }else{
            [self showAlterviewController:LocalizedString(@"SHARE_HTML_NO_INSTALL_APP")];
        }
    }
    
}


- (void)startBtnEnable:(BOOL)enable {
    if (enable) {
        [self.startButton setBackgroundColor:RGB16(COLOR_BG_FF1130)];
        self.startButton.layer.borderWidth = 0;
    } else {
        [self.startButton setBackgroundColor:nil];
        self.startButton.layer.borderWidth = 1.;
    }
}

- (void)getHeartConfig {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
        }
    };
    [AccountManager getHeartConfigCallback:callback];
    
}

#pragma mark - getters

- (VLSLiveBeforeInfoView *)infoView {
    if (!_infoView) {
        self.infoView = [[VLSLiveBeforeInfoView alloc] init];
        self.infoView.delegate = self;
    }
    return _infoView;
}


- (UIButton *)closeButton {
    if (!_closeButton) {
        self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeButton setBackgroundImage:[UIImage imageNamed:@"ic_close"] forState:UIControlStateNormal];
        [self.closeButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _closeButton;
}

-(UIView *)photoSelectView{
    
    if (!_photoSelectView) {
        
        _photoSelectView = [[VLSPhotoSelectView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _photoSelectView.delegate = self;
        _photoSelectView.photoLabel.text = LocalizedString(@"PHOTO_CHANGE_COVER");
        [_photoSelectView showPhotoView:NO animation:NO];
        
    }
    
    return _photoSelectView;
}

- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
    }
    return _imagePicker;
}
- (UIButton *)cameraButton {
    if (!_cameraButton) {
        self.cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cameraButton setImage:[UIImage imageNamed:@"ic_camera"] forState:UIControlStateNormal];
        [_cameraButton addTarget:self action:@selector(changeCamera) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _cameraButton;
}

- (VLSHeadAlertView *)headAlert
{
    if (_headAlert == nil) {
        _headAlert = [[VLSHeadAlertView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60. / 667 * SCREEN_HEIGHT)];
    }
    return _headAlert;
}

- (VLSShareView *)shareView {
    if (!_shareView) {
        self.shareView = [[VLSShareView alloc] init];
        self.shareView.delegate = self;
    }
    return _shareView;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:self.view.bounds];
        _bgView.backgroundColor = RGBA16(COLOR_BG_000000, 0.35);
    }
    return _bgView;
}

- (UIButton *)startButton {
    
    if (!_startButton) {
        self.startButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.startButton.layer.borderWidth = 1.;
        self.startButton.layer.borderColor = RGB16(COLOR_BG_FFFFFF).CGColor;
        self.startButton.layer.cornerRadius = 20;
        self.startButton.clipsToBounds = YES;
        [self.startButton addTarget:self action:@selector(startAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.startButton setTitle:LocalizedString(@"LIVEBEFORE_BUTTON_START_LIVE") forState:UIControlStateNormal];
        self.startButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        [self.startButton setTitleColor:RGB16(COLOR_BG_FFFFFF) forState:UIControlStateNormal];
    }
    return _startButton;
}

- (NSMutableArray *)moreMarkLists {
    if (!_moreMarkLists) {
        self.moreMarkLists = [NSMutableArray arrayWithCapacity:0];
    }
    return _moreMarkLists;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.infoView.titleTextView endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchCourseInfo
{
    HttpCallBack* callback = [[HttpCallBack alloc] init];
    callback.doneBlock = ^(id result) {
        if ([result isKindOfClass: [NSDictionary class]]
            )
        {
            id dict = result[@"results"][@"timingLesson"];
            if (dict != nil && ![dict isKindOfClass: [NSNull class]])
            {
                LessonModel* lesson = [LessonModel mj_objectWithKeyValues: dict];
                if ([lesson.countdowntime doubleValue] == 0)
                {
                    lesson.countdowntime = @([lesson.startTime doubleValue] - [lesson.systemTime doubleValue]);
                }
                if (lesson.lessonId != nil)
                {
                    [self showCourseAlert: lesson];
                }
            }
        }
        
    };
    callback.errorBlock = ^(id error) {
        
    };
    [HTTPFacade getCourseTiming: callback];
}

- (void)showCourseAlert: (LessonModel*)lesson
{
    CourseLiveShowAlertViewController* vc = (CourseLiveShowAlertViewController*)[[UIStoryboard storyboardWithName: @"CourseShow" bundle: nil] instantiateInitialViewController];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.lessonModel = lesson;
    vc.starLessonBlock = ^(LessonModel * lessonModel, void (^completeBlock)(BOOL))
    {
        self.liveBeforeTitle = lessonModel.lessonName;
        self.lessonId = lessonModel.lessonId;
        self.topicStr = [NSString stringWithFormat: @"%d,%@", 0, lessonModel.courseTypeName];
        [self.infoView.coverView.coverImageView sd_setImageWithURL: [NSURL URLWithString: lessonModel.picCover] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
        {
            if (image)
            {
                dispatch_async(dispatch_get_main_queue(), ^
                {
                    [[VLSAgoraVideoService sharedInstance] enableNetworkTest];
                    [self getUserRoomID];
                    if (completeBlock)
                    {
                        completeBlock(YES);
                    }
                });

            }
        }];
    };
    [self presentViewController: vc animated: NO completion:^{
        
    }];
}
@end
