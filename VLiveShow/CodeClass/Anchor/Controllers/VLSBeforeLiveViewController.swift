//
//  VLSBeforeLiveViewController.swift
//  VLiveShow
//
//  Created by rock on 2017/2/22.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSBeforeLiveViewController: VLSBaseViewController {

    var liveInfoView: VLSBeforeLiveInfoView!
    var topic: String?
    var shareView: VLSShareView!
    var startLiveBt: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "live_camera"), style: .plain, target: self, action: #selector(VLSBeforeLiveViewController.camera))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "live_cancel"), style: .plain, target: self, action: #selector(VLSBeforeLiveViewController.disapper))
        
        if let item = VLSAgoraVideoService.sharedInstance().createLocalVideoOnly() {
            item.loadingView.isHidden = true
            self.view.addSubview(item)
            item.snp.makeConstraints({ (make) in
                make.top.left.bottom.right.equalTo(self.view)
            })
        }
        let maskView = UIView().then {
            $0.backgroundColor = UIColor.black.withAlphaComponent(0.35)
        }
        self.view.addSubview(maskView)
        maskView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self.view)
        }
        liveInfoView = VLSBeforeLiveInfoView().then {
            $0.delegate = self
        }
        maskView.addSubview(liveInfoView)
        liveInfoView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(maskView)
            make.height.equalTo(260)
        }
        
        shareView = VLSShareView().then {
            $0.delegate = self
        }
        maskView.addSubview(shareView)
        shareView.snp.makeConstraints { (make) in
            make.top.equalTo(liveInfoView.snp.bottom).offset(20)
            make.left.right.equalTo(maskView)
            make.height.equalTo(40)
        }
        
        startLiveBt = UIButton().then {
            $0.setTitleColor(HEX(COLOR_FONT_FFFFFF), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.setTitle(LocalizedString("LIVEBEFORE_BUTTON_START_LIVE"), for: .normal)
            $0.layer.cornerRadius = 20
            $0.addTarget(self, action: #selector(VLSBeforeLiveViewController.startLive), for: .touchUpInside)
            $0.backgroundColor = VLSSepartorColor
        }
        maskView.addSubview(startLiveBt)
        startLiveBt.snp.makeConstraints { (make) in
            make.top.equalTo(shareView.snp.bottom).offset(20)
            make.left.equalTo(40)
            make.right.equalTo(-40)
            make.height.equalTo(40)
        }
        
        let bookingLiveLab = UILabel().then {
            $0.attributedText = kAttributeStrings([(LocalizedString("VLS_LIVE_BOOKING_2"), HEX(COLOR_FONT_FFFFFF), UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14)), NSNumber(value: 0)), (LocalizedString("VLS_LIVE_BOOKING"), HEX(COLOR_FONT_FF1130), UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14)), NSNumber(value: 1))])
            $0.textAlignment = .center
            $0.sizeToFit()
            $0.clickAction(target: self, action: #selector(VLSBeforeLiveViewController.bookingLive))
        }
        maskView.addSubview(bookingLiveLab)
        bookingLiveLab.snp.makeConstraints { (make) in
            make.top.equalTo(startLiveBt.snp.bottom).offset(20)
            make.left.right.equalTo(maskView)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        VLSAgoraVideoService.sharedInstance().startPreview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func camera() {
        VLSAgoraVideoService.sharedInstance().switchCamera()
    }
    
    func disapper() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func startLive() {
        
    }
    
    func bookingLive() {
        print("ahah")
    }
}

extension VLSBeforeLiveViewController: VLSBeforeLiveInfoViewDelegate, VLSMarkViewControllerDelegate, VLSShareViewDelegate {
    func mark() {
        let markVC = VLSMarkViewController().then {
            $0.modalPresentationStyle = .currentContext
            $0.delegate = self
            $0.markname = self.liveInfoView.markView.markLabel.text
        }
        self.present(markVC, animated: true) { 
            markVC.view.backgroundColor = UIColor(white: 0.25, alpha: 0.25)
        }
    }
    
    func marked(markedId: String, markedName: String) {
        liveInfoView.markView.markLabel.text = markedName
        topic = "\(markedId),\(markedName)"
    }
    
    func shareBtnTrigger(with index: ShareBtnIndex, andState state: Bool) {
        
    }
    
}
