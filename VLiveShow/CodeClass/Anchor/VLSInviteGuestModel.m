//
//  VLSInviteGuestModel.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSInviteGuestModel.h"


@implementation VLSInviteGuestModel

- (void)getFromAnchorAboutModel:(VLSAnchorAboutModel *)anchorModel
{
    self.nickName = anchorModel.nick_name;
    self.faceURL = anchorModel.avatars;
    self.userId = anchorModel.userId;
    self.isSelected = NO;
}
- (void)getFromAnchorAboutSearchModel:(VLSAnchorSearchModel *)anchorModel
{
    self.nickName = anchorModel.nickname;
    self.faceURL = anchorModel.avatars;
    self.userId = anchorModel.userId;
    self.isSelected = NO;
}

@end
