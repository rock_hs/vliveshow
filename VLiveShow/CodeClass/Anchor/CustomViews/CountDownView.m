//
//  CountDownView.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/3.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "CountDownView.h"
#import "VLSMultiLiveViewController.h"
#import <SDAutoLayout/UIView+SDAutoLayout.h>
#define KNum 5


@interface CountDownView ()

@property (nonatomic, strong) UILabel *numLabel;

@end

@implementation CountDownView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        /**
         *  ios8自带毛玻璃效果
         */
        UIBlurEffect *beffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *view = [[UIVisualEffectView alloc]initWithEffect:beffect];
        
        view.frame = self.bounds;
        
        [self addSubview:view];
        view.sd_layout
        .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
        
        
        self.numLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.numLabel.text = [NSString stringWithFormat:@"%d",KNum];
//        self.NumLabel.backgroundColor = [UIColor redColor];
        self.numLabel.textAlignment = NSTextAlignmentCenter;
        self.numLabel.adjustsFontSizeToFitWidth = YES;
        self.numLabel.textColor = [UIColor whiteColor];
        self.numLabel.font = [UIFont systemFontOfSize:50];
        [self addSubview:self.numLabel];
        self.numLabel.sd_layout
        .topSpaceToView(self,0)
        .leftSpaceToView(self,0)
        .rightSpaceToView(self,0)
        .bottomSpaceToView(self,0);
        
    }
    return self;
}

- (void)setNumAnimation
{
    CAKeyframeAnimation *springAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    springAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(5, 5, 1)],[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.5, 0.5, 0.5)],[NSValue valueWithCATransform3D:CATransform3DMakeScale(1, 1, 1)]];
    springAnimation.duration = 1;
    springAnimation.repeatCount = 1;
    springAnimation.removedOnCompletion = NO;
    springAnimation.fillMode = kCAFillModeForwards;
    springAnimation.delegate = self;
    [springAnimation setValue:@"spring" forKey:@"animationType"];
    
    [self.numLabel.layer addAnimation:springAnimation forKey:@"spring"];
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if ([[anim valueForKey:@"animationType"] isEqualToString:@"spring"]) {
        int Num = [self.numLabel.text intValue];
        if (Num > 1) {
            
            self.numLabel.text = [NSString stringWithFormat:@"%d",(Num-1)];
            [self setNumAnimation];
        }else{
            [self.numLabel removeFromSuperview];
            [self removeFromSuperview];
            if(!_delegate){
                return;
            }
            
            if ([_delegate respondsToSelector:@selector(countComplete)]) {
                [_delegate countComplete];
            }
            
        }
    }
}


@end
