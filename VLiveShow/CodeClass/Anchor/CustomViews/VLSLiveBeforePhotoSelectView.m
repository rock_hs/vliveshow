//
//  VLSLiveBeforePhotoSelectView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveBeforePhotoSelectView.h"
#import "VLSPhotoSelectView.h"
#import "CropperViewController.h"

@interface VLSLiveBeforePhotoSelectView ()

@property (nonatomic, strong)UIImageView *placeholderImageView;

@end

@implementation VLSLiveBeforePhotoSelectView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadCoverAction)];
    [self addGestureRecognizer:tap];

    [self addSubview:self.placeholderImageView];
    [self addSubview:self.coverImageView];
    
    self.placeholderImageView.sd_layout
    .leftSpaceToView(self, 40)
    .topSpaceToView(self, 20)
    .rightSpaceToView(self, 40)
    .bottomSpaceToView(self, 20);
    
    self.coverImageView.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0);
}

- (void)uploadCoverAction {
    if ([self.delegate respondsToSelector:@selector(selectPhotoWithImage:)]) {
        [self.delegate selectPhotoWithImage:self.coverImageView.image];
    }
}



#pragma mark - getters 

- (UIImageView *)placeholderImageView {
    if (!_placeholderImageView) {
        self.placeholderImageView = [[UIImageView alloc] init];
        self.placeholderImageView.image = [UIImage imageNamed:@"ic_addpic"];
        self.placeholderImageView.userInteractionEnabled = YES;
    }
    return _placeholderImageView;
}

- (UIImageView *)coverImageView {
    if (!_coverImageView) {
        self.coverImageView = [[UIImageView alloc] init];
        self.placeholderImageView.userInteractionEnabled = YES;
        
        // 如果之前上传过封面，就取上一次上传的封面
        AccountModel *account = [AccountManager sharedAccountManager].account;
        NSString *key = [NSString stringWithFormat:@"%ld", (long)account.uid];
        ;
        if ([[NSUserDefaults standardUserDefaults] objectForKey:key]) {
            self.coverImageView.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:key]];
        }

    }
    return _coverImageView;
}


@end
