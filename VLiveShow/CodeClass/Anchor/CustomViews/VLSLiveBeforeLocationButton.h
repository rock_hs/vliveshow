//
//  VLSLiveBeforeLocationButton.h
//  VLiveShow
//
//  Created by Cavan on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSLiveBeforeLocationButton : UIButton

@property (nonatomic, strong)UILabel *locaLabel;
@property (nonatomic, strong)NSString *geoLocation;

- (void)locationClose;
- (void)locationOpen;
- (CGFloat)getButtonTotalWidth;

@end
