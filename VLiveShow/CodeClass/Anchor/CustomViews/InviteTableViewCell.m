//
//  InviteTableViewCell.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "InviteTableViewCell.h"
#import "InviteModel.h"

@interface InviteTableViewCell ()


@property (nonatomic, strong) UILabel *nickLabel;

@property (nonatomic, strong) UILabel *City;

@property (nonatomic, strong) UILabel *FromInvite;

@property (nonatomic, strong) UILabel *Sign;

@property (nonatomic, strong) UIButton *button;

@property (nonatomic, strong) UIImageView *gender;

@property (nonatomic, strong) UIImageView *tagview;

@end

@implementation InviteTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.Face = [[UIImageView alloc]initWithFrame:CGRectZero];
        self.Face.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:self.Face];
        
        
        self.nickLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.nickLabel.textColor = [UIColor blackColor];
        self.nickLabel.font = [UIFont systemFontOfSize:12.f];
        [self.contentView addSubview:self.nickLabel];
        
        
        
        self.Sign = [[UILabel alloc]initWithFrame:CGRectZero];
        self.Sign.textColor = [UIColor blackColor];
        self.Sign.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:self.Sign];
        
        
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.button setImage:[UIImage imageNamed:@"but_bc_uncheck"] forState:UIControlStateNormal];
        [self.button setImage:[UIImage imageNamed:@"but_bc_check"] forState:UIControlStateSelected];
        self.button.userInteractionEnabled = NO;
        [self.contentView addSubview:self.button];
        
        self.City = [[UILabel alloc]initWithFrame:CGRectZero];
        self.City.textColor = [UIColor colorWithRed:151/255.f green:151/255.f blue:151/255.f alpha:1];
        self.City.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:self.City];
        
        self.FromInvite = [[UILabel alloc] initWithFrame:CGRectZero];
        self.FromInvite.textColor = [UIColor colorWithRed:193/255.f green:190/255.f blue:190/255.f alpha:1];
        self.FromInvite.font = [UIFont systemFontOfSize:SIZE_FONT_10];
        [self.contentView addSubview:self.FromInvite];
        
        [self AddLayout];
    }
    return self;
}

- (void)AddLayout
{
    // 头像约束
    self.Face.sd_layout
    .topSpaceToView(self.contentView,10)
    .leftSpaceToView(self.contentView,18)
    .heightIs(50)
    .widthIs(50);
    __weak __typeof__(self) weakSelf = self;
    [self.Face setDidFinishAutoLayoutBlock:^(CGRect frame) {
        if (frame.size.width == frame.size.height) {
            weakSelf.Face.layer.cornerRadius = frame.size.width/2;
            weakSelf.Face.layer.masksToBounds = YES;
        }
    }];
    
    //昵称约束 计算出来高度宽度之后更新约束
    self.nickLabel.sd_layout
    .topSpaceToView(self.contentView,12)
    .leftSpaceToView(self.Face,14)
    .heightIs(17);
    [self.nickLabel setSingleLineAutoResizeWithMaxWidth:200];
    
    // 等级
    
    // 签名约束
    self.Sign.sd_layout
    .topSpaceToView(self.nickLabel,3)
    .leftEqualToView(self.nickLabel)
    .rightSpaceToView(self.contentView,50)
    .autoHeightRatio(0);
    
    
    // 城市约束
    
    self.City.sd_layout
    .topSpaceToView(self.Sign,2)
    .leftEqualToView(self.nickLabel)
    .heightIs(17);
    [self.City setSingleLineAutoResizeWithMaxWidth:100];
    
    // 是否来自嘉宾邀请
    self.FromInvite.sd_layout
    .leftSpaceToView(self.City,5)
    .centerYEqualToView(self.City)
    .heightIs(14);
    [self.FromInvite setSingleLineAutoResizeWithMaxWidth:100];
    
    // button约束
    self.button.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(self.contentView,25)
    .heightIs(20)
    .widthIs(20);
    
    [self setupAutoHeightWithBottomViewsArray:@[self.City,self.Face,self.FromInvite] bottomMargin:7];
}

- (void)setModel:(InviteModel *)model
{
    _model = model;
    if (model) {
        [self.Face sd_setImageWithURL:[NSURL URLWithString:model.faceUrl] placeholderImage:ICON_PLACEHOLDER];
        self.nickLabel.text = model.nickName;
        //        [self.gender setImage:[UIImage imageNamed:@"female01@3x"]];
        
        self.Sign.text = model.sign;
        self.City.text = model.city;
        
        if (model.isSelected) {
            self.button.selected = YES;
        }else{
            self.button.selected = NO;
        }
        
        if (model.city) {
            self.FromInvite.sd_layout.leftSpaceToView(self.City,5);
        }else{
            self.FromInvite.sd_layout.leftSpaceToView(self.Face,14);
        }
        
        if (model.isSpecial) {
            self.FromInvite.text = LocalizedString(@"INVITE_FROM_GUEST");
        }else{
            self.FromInvite.text = nil;
        }
    }
    
    
}

- (void)seletedStatus
{
    if (_model.isSelected) {
        self.button.selected = NO;
        _model.isSelected = NO;
    }else{
        self.button.selected = YES;
        _model.isSelected = YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
