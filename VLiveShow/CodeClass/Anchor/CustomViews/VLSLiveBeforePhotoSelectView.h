//
//  VLSLiveBeforePhotoSelectView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSLiveBeforePhotoSelectViewDelegate <NSObject>

- (void)selectPhotoWithImage:(UIImage *)coverImage;

@end

@interface VLSLiveBeforePhotoSelectView : UIView

@property (nonatomic, weak)id <VLSLiveBeforePhotoSelectViewDelegate> delegate;

@property (nonatomic, strong)UIImageView *coverImageView;

@end
