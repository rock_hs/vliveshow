//
//  TableNilView.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "TableNilView.h"

@implementation TableNilView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectZero];
    imageview.contentMode = UIViewContentModeScaleAspectFill;
    [imageview setImage:[UIImage imageNamed:@"ic_sofa"]];
    imageview.backgroundColor = [UIColor clearColor];
    [self addSubview:imageview];
    imageview.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(self,100)
    .widthRatioToView(self,85/375.f)
    .heightEqualToWidth();
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:119/255.f green:119/255.f blue:119/255.f alpha:1];
    label.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    [self addSubview:label];
    label.sd_layout
    .centerXEqualToView(imageview)
    .topSpaceToView(imageview,14)
    .heightIs(20);
    [label setSingleLineAutoResizeWithMaxWidth:200];
    label.text = LocalizedString(@"INVITE_NO_PEOPLE");
}
@end
