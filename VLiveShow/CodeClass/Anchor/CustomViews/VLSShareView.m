//
//  VLSShareView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSShareView.h"

#define SHARE_BTN_WIDTH 40. / 375 * SCREEN_WIDTH
#define SHARE_BTN_SPACE 15. / 375 * SCREEN_WIDTH

@interface VLSShareView ()

@property (nonatomic, strong)UIButton *weChatButton;
@property (nonatomic, strong)UIButton *zoneButton;
@property (nonatomic, strong)UIButton *friendShipButton;
@property (nonatomic, strong)UIButton *qqButton;
@property (nonatomic, strong)UIButton *sinaButton;

@end

@implementation VLSShareView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doInit];
    }
    return self;
}
- (void)doInit {
    
    [self addSubview:self.weChatButton];
    [self addSubview:self.zoneButton];
    [self addSubview:self.friendShipButton];
    [self addSubview:self.qqButton];
    [self addSubview:self.sinaButton];

    
    self.weChatButton.sd_layout
    .topSpaceToView(self, 0)
    .centerXEqualToView(self)
    .widthIs(SHARE_BTN_WIDTH)
    .heightIs(SHARE_BTN_WIDTH);
    
    self.zoneButton.sd_layout
    .rightSpaceToView(self.weChatButton, SHARE_BTN_SPACE)
    .topSpaceToView(self, 0)
    .widthIs(SHARE_BTN_WIDTH)
    .heightIs(SHARE_BTN_WIDTH);
    
    self.friendShipButton.sd_layout
    .leftSpaceToView(self.weChatButton, SHARE_BTN_SPACE)
    .topSpaceToView(self, 0)
    .widthIs(SHARE_BTN_WIDTH)
    .heightIs(SHARE_BTN_WIDTH);
    
    self.qqButton.sd_layout
    .rightSpaceToView(self.zoneButton, SHARE_BTN_SPACE)
    .topSpaceToView(self, 0)
    .widthIs(SHARE_BTN_WIDTH)
    .heightIs(SHARE_BTN_WIDTH);
    
    self.sinaButton.sd_layout
    .leftSpaceToView(self.friendShipButton, SHARE_BTN_SPACE)
    .topSpaceToView(self, 0)
    .heightIs(SHARE_BTN_WIDTH)
    .widthIs(SHARE_BTN_WIDTH);

}

- (void)weChatAction:(UIButton *)button {
    
    button.selected = !button.selected;
    if (button.selected) {
        
        self.qqButton.selected = NO;
        self.zoneButton.selected = NO;
        self.friendShipButton.selected = NO;
        self.sinaButton.selected = NO;
    }
    
    [self shareBtnClickedWithIndex:ShareBtnIndexWeChat AndState:button.selected];
}

- (void)zoneAction:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        
        self.qqButton.selected = NO;
        self.weChatButton.selected = NO;
        self.friendShipButton.selected = NO;
        self.sinaButton.selected = NO;
    }
    [self shareBtnClickedWithIndex:ShareBtnIndexZone AndState:button.selected];

}

- (void)friendShipAction:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        
        self.qqButton.selected = NO;
        self.zoneButton.selected = NO;
        self.weChatButton.selected = NO;
        self.sinaButton.selected = NO;
    }
    [self shareBtnClickedWithIndex:ShareBtnIndexFriendShip AndState:button.selected];

}

- (void)qqAction:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        
        self.zoneButton.selected = NO;
        self.weChatButton.selected = NO;
        self.friendShipButton.selected = NO;
        self.sinaButton.selected = NO;
    }
    [self shareBtnClickedWithIndex:ShareBtnIndexQQ AndState:button.selected];

}

- (void)sinaAction:(UIButton *)button {
    button.selected = !button.selected;
    if (button.selected) {
        
        self.qqButton.selected = NO;
        self.zoneButton.selected = NO;
        self.weChatButton.selected = NO;
        self.friendShipButton.selected = NO;
    }
    [self shareBtnClickedWithIndex:ShareBtnIndexSina AndState:button.selected];

}

- (void)shareBtnClickedWithIndex:(ShareBtnIndex)index AndState:(BOOL)state {
    if ([self.delegate respondsToSelector:@selector(shareBtnTriggerWithIndex:AndState:)]) {
        [self.delegate shareBtnTriggerWithIndex:index AndState:state];
    }
}

#pragma mark - getters

- (UIButton *)weChatButton {
    
    if (!_weChatButton) {
        self.weChatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.weChatButton setImage:[UIImage imageNamed:@"wechat"] forState:UIControlStateNormal];
        self.weChatButton.tag = 302;
        [self.weChatButton setImage:[UIImage imageNamed:@"wechat_choose"] forState:UIControlStateSelected];
        [self.weChatButton addTarget:self action:@selector(weChatAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _weChatButton;
}

- (UIButton *)zoneButton {
    if (!_zoneButton) {
        self.zoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.zoneButton setImage:[UIImage imageNamed:@"QQzone"] forState:UIControlStateNormal];
        self.zoneButton.tag = 301;
        [self.zoneButton setImage:[UIImage imageNamed:@"QQzone_choose"] forState:UIControlStateSelected];
        [self.zoneButton addTarget:self action:@selector(zoneAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _zoneButton;
}

- (UIButton *)friendShipButton {
    if (!_friendShipButton) {
        self.friendShipButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.friendShipButton setImage:[UIImage imageNamed:@"friends-choose"] forState:UIControlStateNormal];
        [self.friendShipButton setImage:[UIImage imageNamed:@"friends_choose"] forState:UIControlStateSelected];
        self.friendShipButton.tag = 303;
        [self.friendShipButton addTarget:self action:@selector(friendShipAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _friendShipButton;
}

- (UIButton *)qqButton {
    if (!_qqButton) {
        self.qqButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.qqButton setImage:[UIImage imageNamed:@"QQ"] forState:UIControlStateNormal];
        self.qqButton.tag = 300;
        [self.qqButton setImage:[UIImage imageNamed:@"QQ_choose"] forState:UIControlStateSelected];
        [self.qqButton addTarget:self action:@selector(qqAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _qqButton;
}

- (UIButton *)sinaButton {
    if (!_sinaButton) {
        self.sinaButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.sinaButton setImage:[UIImage imageNamed:@"sina"] forState:UIControlStateNormal];
        self.sinaButton.tag = 304;
        [self.sinaButton setImage:[UIImage imageNamed:@"sina_choose"] forState:UIControlStateSelected];
        [self.sinaButton addTarget:self action:@selector(sinaAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sinaButton;
}




@end
