//
//  CourseLiveShowAlertViewController.swift
//  VLiveShow
//
//  Created by VincentX on 14/11/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit

class CourseLiveShowAlertViewController: UIViewController {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var countDownLabel: UILabel!
    @IBOutlet weak var clockImageView: UIImageView!
    @IBOutlet weak var alertPopupView: UIView!
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var popCenterConstraint: NSLayoutConstraint!
    
    var starLessonBlock: ((_ lesson: LessonModel?, _ completeBlock: ((Bool) -> Void)?) -> Void)?
    
    var countDown: Int64 = 0
    var countDownTimer: Timer?
    
    var lessonModel: LessonModel?
    {
        didSet
        {
            if self.isViewLoaded
            {
                refreshUI()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startButton.setTitle(_LS("ANCHOR_BEGIN"), for: .normal)
        self.startButton.setBackgroundImage(UIImage(from: UIColor.bgLightGray()), for: .disabled)
        self.startButton.setBackgroundImage(UIImage(from: UIColor.themeRed()), for: .normal)
        self.popCenterConstraint.constant = (self.view.frame.height + self.alertPopupView.frame.height) / 2
        refreshUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showAlertPopup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        self.destoryTimer()
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil
        {
            self.destoryTimer()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlertPopup()
    {
        UIView.animate(withDuration: 0.4, animations: {
            self.popCenterConstraint.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func closeAlertPopup()
    {
        UIView.animate(withDuration: 0.4, animations: {
            self.popCenterConstraint.constant = (self.view.frame.height + self.alertPopupView.frame.height) / 2
            self.view.layoutIfNeeded()
        }, completion: {[weak self] _ in
            self?.dismiss(animated: false, completion: nil)
        })
    }
    
    func refreshUI()
    {
        guard let lm = self.lessonModel else
        {
            return
        }
        if let imageURL = lm.picCover
        {
            self.coverImageView.sd_setImage(with: URL(string: imageURL))
        }
        self.titleLabel.text = lm.lessonName
        if let startTime = lm.startTime
        {
            self.startTimeLabel.text = Date.dateStr(startTime.doubleValue)
        }
        
        self.countDown = lm.countdowntime.int64Value / 1000

        updateCountdownLabel()
        updateStartButtonStatus()
        if self.countDown > 0
        {
            startCountDown()
        }
    }
    
    func startCountDown()
    {
        if let existingTimer = self.countDownTimer
        {
            existingTimer.invalidate()
        }
        self.countDownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerFire), userInfo: nil, repeats: true)
    }
    
    func timerFire()
    {
        countDown -= 1
        updateCountdownLabel()
        updateStartButtonStatus()
    }
    
    func updateCountdownLabel()
    {
        if countDown > 0
        {
            self.countDownLabel.text = _LS("COURSE_COUNTDOWN_TEXT") + " " + CommonSwift.timeDecrease(seconds: countDown)
        }
        else
        {
            if let tm = self.countDownTimer
            {
                tm.invalidate()
                self.countDownTimer = nil
            }
            self.countDownLabel.text = _LS("YOU_HAVE_BEEN_LATE")
        }
    }
    
    func updateStartButtonStatus()
    {
        let toEnable = (countDown < 5 * 60)
        if toEnable != self.startButton.isEnabled
        {
            self.startButton.isEnabled = toEnable
        }
    }
    
    @IBAction func didPressStartLesson()
    {
        guard let block = self.starLessonBlock else
        {
            closeAlertPopup()
            return
        }
        
        block(self.lessonModel)
        {[weak self] result in
            if result
            {
                self?.closeAlertPopup()
            }
        }
    }
    
    func destoryTimer()
    {
        self.countDownTimer?.invalidate()
        self.countDownTimer = nil
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
