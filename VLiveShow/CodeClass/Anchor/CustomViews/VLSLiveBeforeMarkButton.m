//
//  VLSLiveBeforeMarkButton.m
//  VLiveShow
//
//  Created by Cavan on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveBeforeMarkButton.h"

@interface VLSLiveBeforeMarkButton ()

@property (nonatomic, strong)UIImageView *triangle;


@end
@implementation VLSLiveBeforeMarkButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}

- (void)doInit {
    
    [self addSubview:self.markLabel];
    [self addSubview:self.triangle];
    
    self.markLabel.sd_layout
    .leftSpaceToView(self, 0)
    .centerYEqualToView(self)
    .heightIs(Y_CONVERT(15));
    
    self.triangle.sd_layout
    .leftSpaceToView(self.markLabel, X_CONVERT(5))
    .centerYEqualToView(self)
    .heightIs(Y_CONVERT(6))
    .widthIs(X_CONVERT(10));
    
}


#pragma mark - getters

- (UIImageView *)triangle {
    if (!_triangle) {
        self.triangle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconfont-triangle-down"]];
        self.triangle.userInteractionEnabled = YES;
    }
    return _triangle;
}

- (UILabel *)markLabel {
    if (!_markLabel) {
        self.markLabel = [[UILabel alloc] init];
        [self.markLabel setSingleLineAutoResizeWithMaxWidth:X_CONVERT(100)];
        self.markLabel.textColor = RGB16(COLOR_FONT_FFFFFF);
        self.markLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.markLabel.text = LocalizedString(@"LIVEBEFORE_MARK_SELECT");
        }
    return _markLabel;
}



@end
