//
//  InviteSectionHeaderView.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "InviteSectionHeaderView.h"
#import "InviteCollectionViewCell.h"

@interface InviteSectionHeaderView ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>

@property (nonatomic, strong) UILabel *label;

@property (nonatomic, strong) UICollectionView *collection;

@property (nonatomic, strong) NSMutableArray *userData;
@end

@implementation InviteSectionHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        self.userData = [NSMutableArray arrayWithCapacity:6];//4];
        
        self.label = [[UILabel alloc]initWithFrame:CGRectZero];
        self.label.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.label.textColor = [UIColor colorWithRed:151/255.f green:151/255.f blue:151/255.f alpha:1];
        self.label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.label];
        self.label.text = LocalizedString(@"INVITE_GUEST_SEAT");
        
        self.collection = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self flowLayout]];
        self.collection.backgroundColor = [UIColor whiteColor];
//        self.collection.showsHorizontalScrollIndicator = NO;
//        self.collection.scrollEnabled = NO;
        [self addSubview:self.collection];
        
        self.collection.scrollEnabled = NO;
        self.collection.userInteractionEnabled = NO;
        [self.collection registerClass:[InviteCollectionViewCell class] forCellWithReuseIdentifier:@"InviteCell"];
        self.collection.delegate = self;
        self.collection.dataSource = self;
   
        // 存入空数据
        for (int i = 0; i < 6; i++) {
            InviteModel *model = [[InviteModel alloc]init];
            [self.userData addObject:model];
        }
        
        self.label.sd_layout
        .topSpaceToView(self,10)
        .leftSpaceToView(self,0)
        .rightSpaceToView(self,0)
        .heightIs(30);
        
        self.collection.sd_layout
        .topSpaceToView(self.label,0)
        .leftSpaceToView(self,0)
        .rightSpaceToView(self,0)
        .bottomSpaceToView(self,0);
    }
    return self;
}
- (UICollectionViewFlowLayout *)flowLayout
{
    UICollectionViewFlowLayout *flowlayout = [[UICollectionViewFlowLayout alloc]init];
    flowlayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowlayout.minimumLineSpacing = 0.0f;
    flowlayout.minimumInteritemSpacing = 0.0f;
    
    return flowlayout;
}



- (void)ReloadCollection:(NSMutableArray *)array
{
    self.userData = [NSMutableArray arrayWithArray:array];
    
    if (self.userData.count == 0) {
        self.sd_layout
        .heightIs(120);
        
        
    }else{
        self.sd_layout
        .heightIs(140);
    }
    
    if (self.userData.count < 6) {
        for (int i = 0; i < (6 - array.count); i++) {
            InviteModel *model = [[InviteModel alloc]init];
            [self.userData addObject:model];
        }
    }
    
    [self.collection reloadData];
}

#pragma mark flowlayoutDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(collectionView.bounds.size.width/6, collectionView.bounds.size.height);
//    NSLog(@"%@",[NSValue valueWithCGSize:collectionView.bounds.size]);
//    NSLog(@"%@",[NSValue valueWithCGSize:size]);
    return size;
}

#pragma mark dalegate datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.userData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    InviteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"InviteCell" forIndexPath:indexPath];
    [cell setModel:self.userData[indexPath.row]];
    return cell;
}



@end
