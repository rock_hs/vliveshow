//
//  VLSLiveBeforeTitleView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveBeforeTitleView.h"

@interface VLSLiveBeforeTitleView ()<UITextViewDelegate>

@property (nonatomic, strong)UILabel *titleLabel;


@end

@implementation VLSLiveBeforeTitleView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.titleTextView];
    [self addSubview:self.titleLabel];

    self.titleTextView.sd_layout
    .topEqualToView(self)
    .leftEqualToView(self)
    .bottomEqualToView(self)
    .rightEqualToView(self);
    
    self.titleLabel.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 20)
    .heightIs(20);
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(getLiveBeforeTitleWithString:)]) {
        [self.delegate getLiveBeforeTitleWithString:textView.text];
    }
    self.titleLabel.hidden = YES;
    [self.titleTextView endEditing:YES];
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    
    if (textView.text.length >= LIVE_BEFORE_TITLE_COUNT && textView.markedTextRange == nil) {
        
        // 标题长度多于60个字符
        if ([self.delegate respondsToSelector:@selector(showTopErrorWaring)]) {
            [self.delegate showTopErrorWaring];
        }
        [self.titleTextView endEditing:YES];
        
        textView.text = [textView.text substringToIndex:LIVE_BEFORE_TITLE_COUNT];
    }
    
    if ([self.delegate respondsToSelector:@selector(getLiveBeforeTitleWithString:)]) {
        [self.delegate getLiveBeforeTitleWithString:textView.text];
    }
    
    if (textView.markedTextRange == nil) {
        self.titleLabel.hidden = YES;
    }
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if (!textView.text.length) {
        self.titleLabel.hidden = NO;
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"] || range.location >= LIVE_BEFORE_TITLE_COUNT) {
        [textView resignFirstResponder];
        return NO;
    } else {
        return YES;
    }
}


#pragma mark - getters

- (UITextView *)titleTextView {
    if (!_titleTextView) {
        self.titleTextView = [[UITextView alloc] init];
        self.titleTextView.returnKeyType = UIReturnKeyDone;
        self.titleTextView.backgroundColor = [UIColor clearColor];
        self.titleTextView.textColor = RGB16(COLOR_FONT_FFFFFF);
        self.titleTextView.tintColor = RGB16(COLOR_FONT_FFFFFF);
        self.titleTextView.font = [UIFont systemFontOfSize:SIZE_FONT_17];
        self.titleTextView.delegate = self;

    }
    return _titleTextView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.text = LocalizedString(@"LIVEBEFORE_LABEL_LIVE_TITLE");
        if (IS_IPHONE_5) {
            self.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_16];
        } else {
        self.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_20];
        }
        self.titleLabel.textColor = [UIColor whiteColor];
    }
    return _titleLabel;
}

@end
