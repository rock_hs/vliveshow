//
//  InviteCollectionViewCell.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InviteModel.h"

@interface InviteCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) InviteModel *model;


@end
