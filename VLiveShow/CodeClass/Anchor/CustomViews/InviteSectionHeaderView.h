//
//  InviteSectionHeaderView.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteSectionHeaderView : UIView

- (void)ReloadCollection:(NSMutableArray *)array;

@end
