//
//  InviteTableViewCell.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
@class InviteModel;

@interface InviteTableViewCell : UITableViewCell

@property (nonatomic, strong) InviteModel *model;

@property (nonatomic, strong) UIImageView *Face;

- (void)seletedStatus;

@end
