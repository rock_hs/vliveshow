//
//  VLSMarkCollectionViewCell.swift
//  VLiveShow
//
//  Created by rock on 2017/1/17.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

protocol VLSMarkCollectionViewCellDelegate: NSObjectProtocol {
    func marked(model: VLSMarkSubTypeModel, indexPath: IndexPath)
}

class VLSMarkCollectionViewCell: UICollectionViewCell {
    
    private var markBt: UIButton!
    private var item: VLSMarkSubTypeModel?
    var indexPath: IndexPath?
    weak var delegate: VLSMarkCollectionViewCellDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        let backView = UIView().then {
            $0.backgroundColor = UIColor.clear
        }
        self.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
        markBt = UIButton().then {
            $0.backgroundColor = HEX(COLOR_BG_FFFFFF)
            $0.layer.cornerRadius = 16
            $0.setTitleColor(HEX(COLOR_FONT_9B9B9B), for: .normal)
            $0.setTitleColor(HEX(COLOR_FONT_FF1130), for: .selected)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.addTarget(self, action: #selector(VLSMarkCollectionViewCell.marked(selected:)), for: .touchUpInside)
            $0.layer.borderColor = HEX(COLOR_BG_FF1130).cgColor
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
        }
        backView.addSubview(markBt)
        markBt.snp.makeConstraints { (make) in
            make.center.equalTo(backView.snp.center)
            make.size.equalTo(CGSize(width: 80, height: 32))
        }
    }
    
    func setNewItem(_ item: VLSMarkSubTypeModel?) {
        self.item = item
        if let item = item {
            markBt.isSelected = item.isSelected
            markBt.layer.borderWidth = item.isSelected ? 1 : 0
            markBt.setTitle(StringSafty(item.name), for: .normal)
        }
    }
    
    func marked(selected: UIButton) {
        if !selected.isSelected {
            if let item = self.item {
                if let indexPath = self.indexPath {
                    self.delegate?.marked(model: item, indexPath: indexPath)
                }
            }
        }
        markBt.isSelected = !selected.isSelected
        markBt.layer.borderWidth = !selected.isSelected ? 0 : 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
