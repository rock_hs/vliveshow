//
//  VLSLiveBeforeInfoView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveBeforeInfoView.h"
#import "VLSLiveBeforeTitleView.h"
#import "VLSLiveBeforeLocationView.h"
#import "VLSLocationManager.h"

@interface VLSLiveBeforeInfoView ()<UITextViewDelegate, VLSLiveBeforePhotoSelectViewDelegate, VLSLiveBeforeTitleViewDelegate, VLSLiveBeforeLocationViewDelegate>

@property (nonatomic, strong)VLSLiveBeforeTitleView *titleView;
@property (nonatomic, strong)VLSLiveBeforeLocationView *locaView;
@property (nonatomic, strong)UILabel *lineLabel;
@property (nonatomic, strong)UILabel *verticalLabel;
@property (nonatomic, strong)UIImageView *markImgView;


@end

@implementation VLSLiveBeforeInfoView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    [self addSubview:self.coverView];
    [self addSubview:self.titleView];
    [self addSubview:self.lineLabel];
    [self addSubview:self.verticalLabel];
    [self addSubview:self.locaView];
    [self addSubview:self.markBtn];
    [self addSubview:self.markImgView];

    self.coverView.sd_layout
    .topSpaceToView(self, 0)
    .leftSpaceToView(self, 0)
    .widthIs(126. / 375 * SCREEN_WIDTH)
    .heightIs(71. / 667 * SCREEN_HEIGHT);
    
    self.titleView.sd_layout
    .leftSpaceToView(self.coverView, 10)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .heightIs(70);
    
    self.lineLabel.sd_layout
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .topSpaceToView(self.coverView, 10)
    .heightIs(0.5);
    
    self.verticalLabel.sd_layout
    .topSpaceToView(self.lineLabel, 15)
    .centerXEqualToView(self)
    .widthIs(0.5)
    .heightIs(30);
    
    self.locaView.sd_layout
    .centerYEqualToView(self.verticalLabel)
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self.verticalLabel, 0)
    .heightIs(20);
    
    self.markImgView.sd_layout
    .leftSpaceToView(self.verticalLabel, 35. / 375 * SCREEN_WIDTH)
    .centerYEqualToView(self.verticalLabel)
    .heightIs(14)
    .widthIs(14);
    
    self.markBtn.sd_layout
    .leftSpaceToView(self.markImgView, 10)
    .centerYEqualToView(self.verticalLabel)
    .heightIs(20)
    .widthIs(80);

}
- (void)selectMark {
    if ([self.delegate respondsToSelector:@selector(showMarkView)]) {
        [self.delegate showMarkView];
    }
}

- (void)updateLocationChangeWithNoAuthorInSetting {
    
    [self.locaView locationBtnChangeWithState:[VLSLocationManager shareManager].locaAuthi];
    
}

#pragma mark - VLSLiveBeforeLocationViewDelegate

- (void)clickLocationBtn {
    if ([self.delegate respondsToSelector:@selector(clickLocationBtn)]) {
        [self.delegate clickLocationBtn];
    }
}

#pragma mark - VLSLiveBeforeTitleViewDelegate

- (void)showTopErrorWaring {
    if ([self.delegate respondsToSelector:@selector(showTopErrorWaring)]) {
        [self.delegate showTopErrorWaring];
    }
}

- (void)getLiveBeforeTitleWithString:(NSString *)string {
    if ([self.delegate respondsToSelector:@selector(getLiveBeforeTitleWithString:)]) {
        [self.delegate getLiveBeforeTitleWithString:string];
    }
}

#pragma mark - VLSLiveBeforePhotoSelectViewDelegate

- (void)selectPhotoWithImage:(UIImage *)coverImage {
    if ([self.delegate respondsToSelector:@selector(selectPhotoWithImage:)]) {
        [self.delegate selectPhotoWithImage:coverImage];
    }
}

#pragma mark - getters

- (VLSLiveBeforePhotoSelectView *)coverView {
    if (!_coverView) {
        self.coverView = [[VLSLiveBeforePhotoSelectView alloc] init];
        self.coverView.backgroundColor = RGB16(COLOR_BG_FFFFFF);
        self.coverView.delegate = self;
    }
    return _coverView;
}

- (VLSLiveBeforeTitleView *)titleView {
    if (!_titleView) {
        
        self.titleView = [[VLSLiveBeforeTitleView alloc] init];
        self.titleView.backgroundColor = [UIColor clearColor];
        self.titleView.delegate = self;
        
    }
    return _titleView;
}

- (UIView *)lineLabel {
    if (!_lineLabel) {
        self.lineLabel = [[UILabel alloc] init];
        self.lineLabel.backgroundColor = RGB16(COLOR_LINE_FFFFFF);
        
    }
    return _lineLabel;
}
- (UIView *)verticalLabel {
    if (!_verticalLabel) {
        self.verticalLabel = [[UILabel alloc] init];
        self.verticalLabel.backgroundColor = RGB16(COLOR_LINE_FFFFFF);
    }
    return _verticalLabel;
}
- (VLSLiveBeforeLocationView *)locaView {
    if (!_locaView) {
        self.locaView = [[VLSLiveBeforeLocationView alloc] init];
        self.locaView.delegate = self;
    }
    return _locaView;
}


- (UIImageView *)markImgView {
    if (!_markImgView) {
        self.markImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconfont-biaoqian (1)"]];
    }
    return _markImgView;
}

- (VLSLiveBeforeMarkButton *)markBtn {
    if (!_markBtn) {
        self.markBtn = [[VLSLiveBeforeMarkButton alloc] init];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectMark)];
        [self.markBtn addGestureRecognizer:tap];
    }
    return _markBtn;
}

- (UIImage *)coverImage {
    _coverImage = self.coverView.coverImageView.image;
    return _coverImage;
}

- (NSString *)locaStr {
    if (!_locaStr) {
        self.locaStr = self.locaView.locaStr;
    }
    return _locaStr;
}

- (UITextView *)titleTextView {
    if (!_titleTextView) {
        self.titleTextView = self.titleView.titleTextView;
    }
    return _titleTextView;
}
- (NSString *)geoLocation {
    if (!_geoLocation) {
        _geoLocation = self.locaView.geoLocation;
    }
    return _geoLocation;
}

@end
