//
//  CountDownView.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/3.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CountDownViewDelegate<NSObject>
- (void)countComplete;

@end

@interface CountDownView : UIView
@property (nonatomic, weak, nullable) id <CountDownViewDelegate>delegate;

- (void)setNumAnimation;

@end
