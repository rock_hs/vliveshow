//
//  VLSLiveBeforeInfoView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSLiveBeforePhotoSelectView.h"
#import "VLSLiveBeforeMarkButton.h"

@protocol VLSLiveBeforeInfoViewDelegate <NSObject>

- (void)selectPhotoWithImage:(UIImage *)coverImage;
- (void)showTopErrorWaring;
- (void)showMarkView;
- (void)getLiveBeforeTitleWithString:(NSString *)string;
- (void)clickLocationBtn;

@end
@interface VLSLiveBeforeInfoView : UIView

@property (nonatomic, weak)id <VLSLiveBeforeInfoViewDelegate> delegate;

@property (nonatomic, strong)VLSLiveBeforePhotoSelectView *coverView;
@property (nonatomic, strong)VLSLiveBeforeMarkButton *markBtn;
@property (nonatomic, strong)UIImage *coverImage;
@property (nonatomic, strong)NSString *locaStr;
@property (nonatomic, strong)NSString *geoLocation;
@property (nonatomic, strong)UITextView *titleTextView;

- (void)updateLocationChangeWithNoAuthorInSetting;

@end
