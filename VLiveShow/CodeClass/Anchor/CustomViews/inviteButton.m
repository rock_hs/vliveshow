//
//  inviteButton.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "inviteButton.h"

@implementation inviteButton

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
        
    }
    return self;
}

- (void)setup
{
    self.backgroundColor = [UIColor redColor];
    
    UILabel *textlabel = [[UILabel alloc] initWithFrame:CGRectZero];
    textlabel.backgroundColor = [UIColor clearColor];
    textlabel.textColor = [UIColor whiteColor];
    textlabel.textAlignment = NSTextAlignmentCenter;
    textlabel.font = [UIFont systemFontOfSize:SIZE_FONT_16];
    [self addSubview:textlabel];
    textlabel.sd_layout
    .centerYEqualToView(self)
    .centerXEqualToView(self)
    .heightIs(22)
    .widthRatioToView(self,1);
    
    
//    textlabel.text = LocalizedString(@"INVITE_WILL_GUEST");
    NSString *string = LocalizedString(@"INVITE_WILL_GUEST");
    NSMutableAttributedString *imageStr = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSTextAttachment *attch = [[NSTextAttachment alloc] init];
    // 设置图片大小
    attch.bounds = CGRectMake(0, -2, 14, 14);
    attch.image = [UIImage imageNamed:@"but_copy_invite"];
    NSAttributedString *img = [NSAttributedString attributedStringWithAttachment:attch];
    [imageStr insertAttributedString:img atIndex:0];
    textlabel.attributedText = imageStr;
}
@end
