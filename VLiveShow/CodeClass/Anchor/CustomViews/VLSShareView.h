//
//  VLSShareView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    ShareBtnIndexQQ,
    ShareBtnIndexZone,
    ShareBtnIndexWeChat,
    ShareBtnIndexFriendShip,
    ShareBtnIndexSina
} ShareBtnIndex;


@protocol VLSShareViewDelegate <NSObject>

- (void)shareBtnTriggerWithIndex:(ShareBtnIndex)index AndState:(BOOL)state;

@end

@interface VLSShareView : UIView

@property (nonatomic, weak)id <VLSShareViewDelegate> delegate;

@end
