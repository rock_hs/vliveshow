//
//  VLSLiveBeforeLocationView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSLiveBeforeLocationViewDelegate <NSObject>

-(void)clickLocationBtn;

@end
@interface VLSLiveBeforeLocationView : UIView

@property (nonatomic, strong)NSString *locaStr;
@property (nonatomic, strong)NSString *geoLocation;
@property (nonatomic, weak)id <VLSLiveBeforeLocationViewDelegate> delegate;
- (void)locationBtnChangeWithState:(BOOL)state;

@end
