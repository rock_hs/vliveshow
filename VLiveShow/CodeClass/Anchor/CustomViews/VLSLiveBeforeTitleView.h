//
//  VLSLiveBeforeTitleView.h
//  VLiveShow
//
//  Created by Cavan on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSLiveBeforeTitleViewDelegate <NSObject>

- (void)showTopErrorWaring;
- (void)getLiveBeforeTitleWithString:(NSString *)string;

@end
@interface VLSLiveBeforeTitleView : UIView

@property (nonatomic, strong)UITextView *titleTextView;
@property (nonatomic, weak)id <VLSLiveBeforeTitleViewDelegate> delegate;

@end
