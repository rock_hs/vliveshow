//
//  InviteCollectionViewCell.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "InviteCollectionViewCell.h"

@interface InviteCollectionViewCell ()

@property (nonatomic, strong) UIImageView *BGView;

@property (nonatomic, strong) UILabel *label;

@property (nonatomic, strong) UIImageView *tagView;

@end

@implementation InviteCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.BGView = [[UIImageView alloc]initWithFrame:CGRectZero];
        [self addSubview:self.BGView];
        
        self.label = [[UILabel alloc]initWithFrame:CGRectZero];
//        self.label.adjustsFontSizeToFitWidth = YES;
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.font = [UIFont systemFontOfSize:12.0f];
        self.label.textColor = [UIColor blackColor];
        [self addSubview:self.label];
        
        self.tagView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.tagView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:self.tagView];
        
        [self layoutWholeView];
        
//        self.backgroundColor = [UIColor redColor];
    }
    return self;
}

- (void)layoutWholeView
{
//    [super layoutSubviews];
    self.BGView.sd_layout
    .topSpaceToView(self,10)
    .centerXEqualToView(self)
    .heightIs(60)
    .widthIs(60);
    
    self.label.sd_layout
    .bottomSpaceToView(self,10)
    .centerXEqualToView(self)
    .heightIs(17);
    [self.label setSingleLineAutoResizeWithMaxWidth:60];
    
    self.tagView.sd_layout
    .leftSpaceToView(self.label,10)
    .centerYEqualToView(self.label)
    .heightIs(15)
    .widthIs(10);
    
    
    self.BGView.layer.cornerRadius = self.BGView.frame.size.width/2;
    self.BGView.layer.masksToBounds = YES;
    
}

- (void)setModel:(InviteModel *)model
{
    [self.BGView sd_setImageWithURL:[NSURL URLWithString:model.faceUrl] placeholderImage:[UIImage imageNamed:@"broadcast_Host_view_site"]];
    
    // 创建一个富文本
//    NSMutableAttributedString *attri =     [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@  ",model.nickName]];
//    // 添加表情
//    NSTextAttachment *attch = [[NSTextAttachment alloc] init];
//    if (model.gender) {
//        attch.image = [UIImage imageNamed:@"male 01"];
//    }else{
//        attch.image = [UIImage imageNamed:@"female01@3x"];
//    }
//    
//    
//    // 设置图片大小
//    attch.bounds = CGRectMake(0, -2, 15, 15);
//    // 创建带有图片的富文本
//    NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:attch];
//    [attri appendAttributedString:string];
//    // 用label的attributedText属性来使用富文本
//    self.label.attributedText = attri;
    self.label.text = model.nickName;
    
    if (model.isSpecial) {
        self.label.sd_layout
        .leftSpaceToView(self,[self leftSpaceWhenTag]);
        
        [self.tagView setImage:[UIImage imageNamed:@"vip"]];
    }else{
        self.label.sd_layout
        .bottomSpaceToView(self,10)
        .centerXEqualToView(self)
        .heightIs(17);
        [self.label setSingleLineAutoResizeWithMaxWidth:60];
        [self.tagView setImage:nil];
    }
    
}

- (CGFloat)leftSpaceWhenTag
{
    CGFloat labelwidth = self.label.frame.size.width;
    if (labelwidth > 60) {
        labelwidth = 60;
    }
    
    CGFloat width = labelwidth + 20;
    CGFloat left = (SCREEN_WIDTH/4 - width)/2;
    return left;
}

@end
