//
//  VLSBeforeLiveInfoView.swift
//  VLiveShow
//
//  Created by rock on 2017/2/22.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSBeforeLiveInfoViewDelegate {
    func mark()
}

class VLSBeforeLiveInfoView: UIView {
    
    var textView: VLSTextView!
    var coverImageView: UIImageView!
    var addBt: UIButton!
    var tagLab: UILabel!
    var locationAndMarkView: UIView!
    var locationView: VLSLiveBeforeLocationView!
    var markView: VLSLiveBeforeMarkButton!
    weak var delegate: VLSBeforeLiveInfoViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textView = VLSTextView().then {
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_17))
            $0.showsVerticalScrollIndicator = false
            $0.backgroundColor = UIColor.clear
            $0.textColor = HEX(COLOR_FONT_FFFFFF);
            $0.placeholder = LocalizedString("LIVEBEFORE_LABEL_LIVE_TITLE")
        }
        self.addSubview(textView)
        textView.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.right.equalTo(self)
            make.height.equalTo(40)
        }
        coverImageView = UIImageView().then {
            $0.contentMode = .scaleAspectFill
        }
        self.addSubview(coverImageView)
        coverImageView.snp.makeConstraints { (make) in
            make.top.equalTo(textView.snp.bottom)
            make.left.right.equalTo(self)
            make.height.equalTo(150)
        }
        addBt = UIButton().then {
            $0.setImage(#imageLiteral(resourceName: "ic_jia"), for: .normal)
            $0.setTitle(LocalizedString("VLS_LIVE_COVER_ADD"), for: .normal)
            $0.verticalImageAndTitle(spacing: 10)
        }
        coverImageView.addSubview(addBt)
        addBt.snp.makeConstraints { (make) in
            make.center.equalTo(coverImageView)
        }
        tagLab = UILabel().then {
            $0.textAlignment = .center
            $0.textColor = HEX(COLOR_FONT_FFFFFF)
            $0.text = "直播"
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
            $0.backgroundColor = HEX(COLOR_BG_FF1130)
            $0.layer.cornerRadius = 15
            $0.clipsToBounds = true
        }
        coverImageView.addSubview(tagLab)
        tagLab.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.right.equalTo(-20)
            make.size.equalTo(CGSize(width: 60, height: 30))
        }
        
        locationAndMarkView = UIView()
        locationAndMarkView.backgroundColor = UIColor.red
        self.addSubview(locationAndMarkView)
        locationAndMarkView.snp.makeConstraints { (make) in
            make.top.equalTo(coverImageView.snp.bottom)
            make.left.right.equalTo(self)
            make.height.equalTo(60)
        }
        let verticalLab = UILabel().then {
            $0.backgroundColor = HEX(COLOR_LINE_FFFFFF)
        }
        locationAndMarkView.addSubview(verticalLab)
        verticalLab.snp.makeConstraints { (make) in
            make.center.equalTo(locationAndMarkView)
            make.size.equalTo(CGSize(width: 0.5, height: 26))
        }
        locationView = VLSLiveBeforeLocationView().then {
            $0.delegate = self
        }
        locationAndMarkView.addSubview(locationView)
        locationView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(verticalLab.snp.left).offset(-20)
            make.centerY.equalTo(locationAndMarkView)
        }
        markView = VLSLiveBeforeMarkButton().then {
            $0.clickAction(target: self, action: #selector(VLSBeforeLiveInfoView.mark))
        }
        locationAndMarkView.addSubview(markView)
        markView.snp.makeConstraints { (make) in
            make.left.equalTo(verticalLab.snp.right).offset(20)
            make.right.equalTo(-20)
            make.centerY.equalTo(locationAndMarkView)
        }        
    }
    
    func mark() {
        delegate?.mark()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension VLSBeforeLiveInfoView: VLSLiveBeforeLocationViewDelegate {
    func clickLocationBtn() {
        
    }
}
