//
//  VLSLiveBeforeLocationButton.m
//  VLiveShow
//
//  Created by Cavan on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveBeforeLocationButton.h"
#import "VLSLocationManager.h"

#define LocationCloseFlag @"定位关"
@interface VLSLiveBeforeLocationButton ()

@property (nonatomic, strong)UIImageView *closeImgView;

@end

@implementation VLSLiveBeforeLocationButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}

- (void)doInit {
    
    [self addSubview:self.locaLabel];
    [self addSubview:self.closeImgView];
    
    self.locaLabel.sd_layout
    .leftSpaceToView(self, 0)
    .centerYEqualToView(self)
    .heightIs(15);
    
    self.closeImgView.sd_layout
    .leftSpaceToView(self.locaLabel, 10)
    .centerYEqualToView(self)
    .heightIs(14)
    .widthIs(14);
    
}

- (void)locationClose {
    self.closeImgView.hidden = YES;
    self.locaLabel.text = LocalizedString(@"LIVEBEFORE_LOCATION_CLOSE");
    self.geoLocation = [NSString stringWithFormat:@",,%@", LocationCloseFlag];
}

- (void)locationOpen {
    self.closeImgView.hidden = NO;
    if ([VLSLocationManager shareManager].locaStr && ![[VLSLocationManager shareManager].locaStr isEqualToString:@"(null)"]) {
        self.locaLabel.text = [VLSLocationManager shareManager].locaStr;
        self.geoLocation = [NSString stringWithFormat:@"%@,%@,%@", [VLSLocationManager shareManager].latitude, [VLSLocationManager shareManager].longitude, [VLSLocationManager shareManager].locaStr];
    } else {
        [self locationClose];
    }
}

- (CGFloat)getButtonTotalWidth {
    return CGRectGetWidth(self.locaLabel.frame) + CGRectGetWidth(self.closeImgView.frame) + 10;
}

#pragma mark - getters

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        self.closeImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_cancel"]];
        self.closeImgView.userInteractionEnabled = YES;
    }
    return _closeImgView;
}

- (UILabel *)locaLabel {
    if (!_locaLabel) {
        
        self.locaLabel = [[UILabel alloc] init];
        [self.locaLabel setSingleLineAutoResizeWithMaxWidth:80];
        self.locaLabel.textColor = RGB16(COLOR_FONT_FFFFFF);
        self.locaLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        if ([VLSLocationManager shareManager].locaStr && ![[VLSLocationManager shareManager].locaStr isEqualToString:@"(null)"]) {
            self.locaLabel.text = [VLSLocationManager shareManager].locaStr;
            self.geoLocation = [NSString stringWithFormat:@"%@,%@,%@", [VLSLocationManager shareManager].latitude, [VLSLocationManager shareManager].longitude, [VLSLocationManager shareManager].locaStr];
        } else {
            [self locationClose];
        }
    }
    return _locaLabel;
}



@end
