//
//  VLSLiveBeforeMarkButton.h
//  VLiveShow
//
//  Created by Cavan on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSLiveBeforeMarkButton : UIButton

@property (nonatomic, strong)UILabel *markLabel;

@end
