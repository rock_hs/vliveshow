//
//  VLSLiveBeforeLocationView.m
//  VLiveShow
//
//  Created by Cavan on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveBeforeLocationView.h"
#import "VLSLiveBeforeLocationButton.h"

@interface VLSLiveBeforeLocationView ()

@property (nonatomic, strong)UIImageView *locaImgView;
@property (nonatomic, strong)VLSLiveBeforeLocationButton *locaButton;

@end

@implementation VLSLiveBeforeLocationView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self doInit];
    }
    return self;
}

- (void)doInit {
    
    [self addSubview:self.locaImgView];
    [self addSubview:self.locaButton];
    
    CGFloat width = [self.locaButton getButtonTotalWidth];
    self.locaButton.sd_layout
    .rightSpaceToView(self, 40. / 375 * SCREEN_WIDTH)
    .centerYEqualToView(self)
    .widthIs(width)
    .heightIs(20);
    
    self.locaImgView.sd_layout
    .rightSpaceToView(self.locaButton, 10)
    .centerYEqualToView(self)
    .widthIs(15)
    .heightIs(15);
    
}

- (void)locaButtonAction {
    if ([self.delegate respondsToSelector:@selector(clickLocationBtn)]) {
        [self.delegate clickLocationBtn];
    }
    if (self.locaButton.selected) {
        [self.locaButton locationOpen];
    } else {
        [self.locaButton locationClose];
    }
    self.locaButton.selected = !self.locaButton.selected;
}

- (void)locationBtnChangeWithState:(BOOL)state {
    if (state) {
        [self.locaButton locationOpen];
    } else {
        [self.locaButton locationClose];
    }
}

#pragma getters

- (UIImageView *)locaImgView {
    if (!_locaImgView) {
        self.locaImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_location "]];
    }
    return _locaImgView;
}
- (VLSLiveBeforeLocationButton *)locaButton {
    if (!_locaButton) {
        self.locaButton = [[VLSLiveBeforeLocationButton alloc] init];
        self.locaButton.imageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locaButtonAction)];
        [self.locaButton addGestureRecognizer:tap];
    }
    return _locaButton;
}

- (NSString *)locaStr {
    if (!_locaStr) {
        self.locaStr = self.locaButton.locaLabel.text;
    }
    return _locaStr;
}
- (NSString *)geoLocation {
    if (!_geoLocation) {
        _geoLocation = self.locaButton.geoLocation;
    }
    return _geoLocation;
}
@end
