//
//  VLSInviteGuestModel.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSAnchorAboutModel.h"
#import "VLSAnchorSearchModel.h"

@interface VLSInviteGuestModel : NSObject

@property (nonatomic, copy) NSString *userId;

@property (nonatomic, copy) NSString *faceURL;

@property (nonatomic, copy) NSString *level;

@property (nonatomic, copy) NSString *nickName;

@property (nonatomic, copy) NSString *lineCount;

@property (nonatomic, assign) BOOL isInvited;

@property (nonatomic, assign) BOOL isSelected;

@property (nonatomic, assign) BOOL isRequest;

- (void)getFromAnchorAboutModel:(VLSAnchorAboutModel *)anchorModel;

- (void)getFromAnchorAboutSearchModel:(VLSAnchorSearchModel *)anchorModel;

@end
