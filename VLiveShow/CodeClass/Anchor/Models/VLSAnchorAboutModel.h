//
//  VLSAnchorAboutModel.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSAnchorAboutModel : NSObject

@property (nonatomic, copy) NSString *nick_name;

@property (nonatomic, copy) NSString *userId;

@property (nonatomic, copy) NSString *avatars;

@end
