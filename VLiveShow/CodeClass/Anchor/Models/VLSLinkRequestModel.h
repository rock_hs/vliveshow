//
//  LinkRequestUserModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSApplyAcountInfo.h"

@interface VLSLinkRequestModel : NSObject
@property (nonatomic, strong) NSString *applyAccount;
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *updateTime;

@property (nonatomic, strong) VLSApplyAcountInfo *applyAccountInfo;

@end
