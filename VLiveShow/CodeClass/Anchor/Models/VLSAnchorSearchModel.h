//
//  VLSAnchorSearchModel.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/24.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSAnchorSearchModel : NSObject

@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, copy) NSString *userId;

@property (nonatomic, copy) NSString *intro;

@property (nonatomic, assign) NSInteger role;

@property (nonatomic, copy) NSString *gender;

@property (nonatomic, copy) NSString *avatars;


@end
