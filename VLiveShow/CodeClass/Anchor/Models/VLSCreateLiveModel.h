//
//  VLSCreateLiveModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/21.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostFileModel.h"

@interface VLSCreateLiveModel : NSObject
@property (nonatomic, strong) NSString *liveTitle;
//@property (nonatomic, strong) NSArray *topics;
@property (nonatomic, strong) NSString *topics;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong)NSString *geoLocaltion;
@property (nonatomic, nullable, strong) NSNumber* lessonId;
//封面
@property (nonatomic, strong) PostFileModel *fileModel;

@end
