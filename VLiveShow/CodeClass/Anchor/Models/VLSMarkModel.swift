//
//  VLSMarkModel.swift
//  VLiveShow
//
//  Created by rock on 2017/1/17.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

class VLSMarkModel: VLSTableViewItem {
    
    var channelId: NSNumber?
    var data: [VLSMarkSubTypeModel] = []
    var name: String?
    var orderNo: NSNumber?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        channelId <- map["channelId"]
        data <- map["data"]
        name <- map["name"]
        orderNo <- map["orderNo"]
    }
}

class VLSMarkSubTypeModel: VLSTableViewItem {
    var topicId: NSNumber?
    var name: String?
    //自定义
    var isSelected: Bool = false
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        topicId <- map["topicId"]
        name <- map["name"]
    }
}
