//
//  VLSApplyAcountInfo.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/21.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSApplyAcountInfo : NSObject

@property (nonatomic, copy) NSString *avatars;

@property (nonatomic, copy) NSString *gender;

@property (nonatomic, copy) NSString *intro;

@property (nonatomic, copy) NSString *location;

@property (nonatomic, copy) NSString *nickName;

@property (nonatomic, assign) NSInteger role;


@end
