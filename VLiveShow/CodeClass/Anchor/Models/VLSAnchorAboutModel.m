//
//  VLSAnchorAboutModel.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAnchorAboutModel.h"

@implementation VLSAnchorAboutModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"userId" : @"id"};
}
@end
