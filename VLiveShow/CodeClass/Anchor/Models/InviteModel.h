//
//  InviteModel.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSLinkRequestModel.h"

@interface InviteModel : NSObject

@property (nonatomic, copy) NSString *faceUrl;

@property (nonatomic, copy) NSString *city;

@property (nonatomic, copy) NSString *userID;

@property (nonatomic, copy) NSString *sign;

@property (nonatomic, copy) NSString *gender;
// 邀请标签
@property (nonatomic, copy) NSString *tagor;

@property (nonatomic, copy) NSString *nickName;

//@property (nonatomic, copy) NSString *status;

@property (nonatomic, assign) BOOL isSpecial;

@property (nonatomic, assign) BOOL isGuest;

@property (nonatomic, assign) BOOL isSelected;

- (void)infoFromVLSLinkRequest:(VLSLinkRequestModel *)linkModel;

@end
