//
//  InviteModel.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "InviteModel.h"
#import "userRoleJudge.h"

@implementation InviteModel

- (void)infoFromVLSLinkRequest:(VLSLinkRequestModel *)linkModel
{
    self.faceUrl = linkModel.applyAccountInfo.avatars;
    self.city = linkModel.applyAccountInfo.location;
    self.userID = linkModel.applyAccount;
    self.sign = linkModel.applyAccountInfo.intro;
    self.gender = linkModel.applyAccountInfo.gender;
    self.nickName = linkModel.applyAccountInfo.nickName;
    NSLog(@"申请角色：%ld",(long)linkModel.applyAccountInfo.role);
    if ([linkModel.status isEqualToString:@"APPLY"]) {
        self.isGuest = NO;
        self.isSelected = NO;
    }else{
        self.isGuest = YES;
        self.isSelected = YES;
    }
    
    if ([userRoleJudge userRoleJudge:linkModel.applyAccountInfo.role compare:1]) {
        
    }
    if ([userRoleJudge userRoleJudge:linkModel.applyAccountInfo.role compare:2]) {
        // 超级管理员
    }
    if ([userRoleJudge userRoleJudge:linkModel.applyAccountInfo.role compare:4]) {
        // 特邀嘉宾
        self.isSpecial = YES;
    }
    if ([userRoleJudge userRoleJudge:linkModel.applyAccountInfo.role compare:8]) {
        // 嘉宾
    }
    if ([userRoleJudge userRoleJudge:linkModel.applyAccountInfo.role compare:16]) {
        // 观众
    }
    if ([userRoleJudge userRoleJudge:linkModel.applyAccountInfo.role compare:32]) {
        // admin
    }
    
    
    
//    switch (linkModel.applyAccountInfo.role) {
//        case 1:
//            
//            break;
//        case 2:
//            // 超级管理员
//            break;
//        case 4:
//            // 特邀嘉宾
//            self.isSpecial = YES;
//            break;
//        case 8:{
//            // 已经是嘉宾
//            self.isGuest = YES;
//            self.isSelected = YES;
//        }
//            break;
//        case 16:{
//            // 观众
//        }
//            break;
//        case 32:
//            // admin
//            break;
//        default:
//            break;
//    }
}

@end
