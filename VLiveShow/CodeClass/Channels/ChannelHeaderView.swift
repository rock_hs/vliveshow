//
//  ChannelHeaderView.swift
//  VLiveShow
//
//  Created by VincentX on 10/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

import UIKit

class ChannelHeaderView: UICollectionReusableView
{
    @IBOutlet weak var titleLabel: UILabel!
}
