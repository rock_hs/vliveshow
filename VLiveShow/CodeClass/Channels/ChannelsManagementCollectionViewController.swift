//
//  ChannelsManagementCollectionViewController.swift
//  VLiveShow
//
//  Created by VincentX on 06/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

import UIKit
import NHAlignmentFlowLayout
import MBProgressHUD

private let reuseIdentifier = "Cell"

enum ChannelSelectionSection: Int
{
    case mine
    case others
    
    case count
}

class ChannelsManagementCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, LXReorderableCollectionViewDataSource {
    
    var selectedChannels: [VLSChannel] = []
    var unselectedChannes: [VLSChannel] = []
    var allChannels: [VLSChannel] = []
    var isLoading = false
    
    var calculateCell: ChannelSelectionCell?
    var movingIndexPath: IndexPath?
    
    lazy var channelManager: UserChannelManager = UserChannelManager()
    
    var didUpdateStarredChannels: (()-> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.calculateCell = Bundle.main.loadNibNamed("ChannelSelectionCell", owner: nil, options: nil)?[0] as? ChannelSelectionCell
        self.collectionView!.register(UINib(nibName: "ChannelSelectionCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        let defaultLayout = self.collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        if #available(iOS 9.0, *)
        {
            let layout = NHAlignmentFlowLayout()
            layout.estimatedItemSize = CGSize(width: 60, height: 35)
            layout.alignment = .topLeftAligned
            self.collectionView!.collectionViewLayout = layout
        }
        else
        {
            let layout = LXReorderableCollectionViewFlowLayout()
            //layout.estimatedItemSize = CGSize(width: 60, height: 35)
            layout.alignment = .topLeftAligned
            self.collectionView!.collectionViewLayout = layout
        }
        if let flowLayout = self.collectionView!.collectionViewLayout as? UICollectionViewFlowLayout
        {
            flowLayout.headerReferenceSize = defaultLayout.headerReferenceSize
            flowLayout.sectionInset =  defaultLayout.sectionInset
        }
        self.loadChannelsFromLocal()


        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes

        // Do any additional setup after loading the view.
        self.loadChannelsFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.useLightTheme()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.saveOrderredChannels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return ChannelSelectionSection.count.rawValue
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        var number = 0
        if section == ChannelSelectionSection.mine.rawValue
        {
            number = selectedChannels.count
        }
        else if section == ChannelSelectionSection.others.rawValue
        {
            number = unselectedChannes.count
        }
        return number
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ChannelSelectionCell
        var channel: VLSChannel? = nil
        cell.nameLabel.textColor = UIColor.x777777()
        if indexPath.section == ChannelSelectionSection.mine.rawValue
        {
            channel = self.selectedChannels[indexPath.row]
            cell.actionButton.isHidden = channel!.type == "pinned" || channel!.type == "special"
            if channel!.type == "pinned"
            {
                cell.nameLabel.textColor = UIColor.c7c7c7()
            }
        }
        else if indexPath.section == ChannelSelectionSection.others.rawValue
        {
            channel = self.unselectedChannes[indexPath.row]
            cell.actionButton.isHidden = false
        }
        cell.nameLabel.text = channel?.channelName
        
        cell.actionButton.isHighlighted = (indexPath.section == ChannelSelectionSection.others.rawValue)
        cell.actionBlock =
        {[weak self] cell in
            if let idx = collectionView.indexPath(for: cell)
            {
                self?.didPressActionButton(indexPath: idx)
            }
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! ChannelHeaderView
        if indexPath.section == ChannelSelectionSection.mine.rawValue
        {
            let font0 = UIFont(name: "PingFangSC-Regular", size: 15) ?? UIFont.systemFont(ofSize:  15)
            
            let attrStr0 = NSMutableAttributedString(string: _LS("My Channels"), attributes: [NSFontAttributeName : font0,
                                                                                             NSForegroundColorAttributeName: UIColor.titleColorInLightNavi()])
            
            let font1 = UIFont(name: "PingFangSC-Regular", size: 12) ?? UIFont.systemFont(ofSize:  12)
            let attrStr1 = NSAttributedString(string: _LS("(Drag to order)"), attributes: [NSFontAttributeName : font1,
                                                                                         NSForegroundColorAttributeName: UIColor.c7c7c7()])
            attrStr0.append(attrStr1)
            headerView.titleLabel.attributedText = attrStr0
        }
        else if indexPath.section == ChannelSelectionSection.others.rawValue
        {
            headerView.titleLabel.text = _LS("Other Channels")
            headerView.titleLabel.textColor = UIColor.titleColorInLightNavi()
        }
        return headerView
    }

    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool
    {
        if indexPath.section != 0
        {
            return false
        }
        let channel = self.selectedChannels[indexPath.row]
        return channel.type != "pinned"
    }
    
    public func collectionView(_ collectionView: UICollectionView!, itemAt fromIndexPath: IndexPath!, canMoveTo toIndexPath: IndexPath!) -> Bool
    {
        return self.collectionView(collectionView, targetIndexPathForMoveFromItemAt: fromIndexPath, toProposedIndexPath: toIndexPath) == toIndexPath
    }
    
    func collectionView(_ collectionView: UICollectionView!, itemAt fromIndexPath: IndexPath!, willMoveTo toIndexPath: IndexPath!)
    {
        self.collectionView(collectionView, moveItemAt: fromIndexPath, to: toIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var channel: VLSChannel? = nil
        var size = CGSize(width: 60, height: 35)
        if indexPath.section == ChannelSelectionSection.mine.rawValue
        {
            guard indexPath.row < self.selectedChannels.count  else
            {
                return size
            }
            channel = self.selectedChannels[indexPath.row]
            
        }
        else if indexPath.section == ChannelSelectionSection.others.rawValue
        {
            guard indexPath.row < self.unselectedChannes.count  else
            {
                return size
            }
            channel = self.unselectedChannes[indexPath.row]
        }
        if let calCell = self.calculateCell
        {
            calCell.nameLabel.text = channel?.channelName
            size = calCell.contentView.systemLayoutSizeFitting(size, withHorizontalFittingPriority: UILayoutPriorityDefaultLow, verticalFittingPriority: UILayoutPriorityRequired)
        }

        return size
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    {
        if sourceIndexPath.section != 0 || destinationIndexPath.section != 0
        {
            return
        }
        let moveItem = self.selectedChannels[sourceIndexPath.row]
        self.selectedChannels.remove(at: sourceIndexPath.row)
        self.selectedChannels.insert(moveItem, at: destinationIndexPath.row)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        if #available(iOS 9.0, *)
        {
            if let movingIndex = self.movingIndexPath,
            let movingCell = collectionView.cellForItem(at: movingIndex)
            {
                movingCell.contentView.transform = CGAffineTransform.identity
            }
        }
        self.movingIndexPath = nil
    }

    override func collectionView(_ collectionView: UICollectionView, targetIndexPathForMoveFromItemAt originalIndexPath: IndexPath, toProposedIndexPath proposedIndexPath: IndexPath) -> IndexPath
    {
        if proposedIndexPath.section != ChannelSelectionSection.mine.rawValue
        {
            return originalIndexPath
        }
        let channel = self.selectedChannels[proposedIndexPath.row]
        let notAllowed = channel.type == "pinned"
        if !notAllowed
        {
            self.movingIndexPath = originalIndexPath
            if #available(iOS 9.0, *)
            {
                if let movingCell = collectionView.cellForItem(at: originalIndexPath)
                {
                    movingCell.contentView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                }
            }

        }
        
        return notAllowed ? originalIndexPath : proposedIndexPath
    }

    func loadChannelsFromLocal()
    {
        self.selectedChannels = UserChannelManager.loadStarredChannels()
        self.unselectedChannes = UserChannelManager.loadAllChannels().filter({ (c) -> Bool in
            let index = self.selectedChannels.index(where:
                { (channel) -> Bool in
                    guard let channel0 = channel.channelId,
                        let channel1 = c.channelId else
                    {
                        return false
                    }
                    return channel0.intValue == channel1.intValue
            })
            return index == nil
        })
        self.collectionView?.reloadData()
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    func loadChannelsFromServer()
    {
        if isLoading
        {
            return
        }
        let hud = MBProgressHUD()
        self.view.addSubview(hud)
        hud.graceTime = 1
        hud.show(true)
        let callback = HttpCallBack()
        callback.doneBlock =
            {[weak self] result in
                if let dict = result as? [String: Any],
                    let resultDict = dict["results"] as? [String: Any],
                    let list = resultDict["list"] as? [[String: Any]]
                {
//                    self?.hasDataFromServer = list.count > 0
//                    if (list.count > 0)
//                    {
//                        self?.saveChannels(list: list)
//                    }
//                    if let channelVCs = self?.listMapVC(list: list)
//                    {
//                        self?.subViewControllers = channelVCs
//                    }
                    UserChannelManager.saveAllChannels(channels: list)
                    self?.loadChannelsFromLocal()
                }
                self?.isLoading = false
                hud.hide(true)
        }
        
        callback.errorBlock =
            {[weak self] error in
                hud.hide(true)
                self?.isLoading = false
        }
        isLoading = true
        HTTPFacade.getChannelInfo(callback);
    }
    
    func didPressActionButton(indexPath: IndexPath)
    {
        var newIndexPath: IndexPath? = nil
        if indexPath.section == ChannelSelectionSection.mine.rawValue
        {
            let elm = self.selectedChannels.remove(at: indexPath.row)
            //self.collectionView?.deleteItems(at: [indexPath])
            self.unselectedChannes.append(elm)
            //self.collectionView?.insertItems(at: [])
            newIndexPath = IndexPath(row: self.unselectedChannes.count - 1, section: ChannelSelectionSection.others.rawValue)

        }
        else if indexPath.section == ChannelSelectionSection.others.rawValue
        {
            let elm = self.unselectedChannes.remove(at: indexPath.row)
            //self.collectionView?.deleteItems(at: [indexPath])
            self.selectedChannels.append(elm)
            //self.collectionView?.insertItems(at: [IndexPath(row: self.selectedChannels.count - 1, section: ChannelSelectionSection.mine.rawValue)])
            
            newIndexPath = IndexPath(row: self.selectedChannels.count - 1, section: ChannelSelectionSection.mine.rawValue)

        }
        if let nIndexPath = newIndexPath
        {
            self.collectionView?.performBatchUpdates(
            {
                self.collectionView?.moveItem(at: indexPath, to: nIndexPath)
            }, completion: { (_) in
                self.collectionView?.reloadItems(at: [nIndexPath])
            })
        }
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func saveOrderredChannels()
    {
        UserChannelManager.saveStarredChannels(channels: self.selectedChannels)
    }
    
    @IBAction func didPressSaveButton()
    {
        self.saveOrderredChannels()
        self.didUpdateStarredChannels?()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressCloseButton()
    {
        if self.navigationItem.rightBarButtonItem?.isEnabled == true
        {
            let alert = UIAlertController(title: _LS("CONFIRM_CHANNEL_MANAGEMENT_CLOSE"), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: _LS("CANCLE"), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: _LS("IM_SURE_TO_CLOSE"), style: .default, handler: { (_) in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            alert.view.tintColor = UIColor.gray
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            _ = self.navigationController?.popViewController(animated: true)
        }

    }
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
//    func handleLongGesture(gesture: UILongPressGestureRecognizer)
//    {
//        print("====> handleLongGesture")
//
//        switch(gesture.state)
//        {
//        case UIGestureRecognizerState.began:
//            guard let selectedIndexPath = self.collectionView!.indexPathForItem(at: gesture.location(in: self.collectionView!))
//                else
//            {
//                break
//            }
//            if #available(iOS 9.0, *) {
//                self.collectionView!.beginInteractiveMovementForItem(at: selectedIndexPath)
//            } else {
//                // Fallback on earlier versions
//            }
//            print("====> begin")
//            
//        case UIGestureRecognizerState.changed:
//        
//            if #available(iOS 9.0, *) {
//                self.collectionView!.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view))
//            } else {
//                // Fallback on earlier versions
//            }
//            
//        case UIGestureRecognizerState.ended:
//            if #available(iOS 9.0, *) {
//                self.collectionView!.endInteractiveMovement()
//            } else {
//                // Fallback on earlier versions
//            }
//            print("====> end")
//            
//        default:
//            if #available(iOS 9.0, *) {
//                self.collectionView!.cancelInteractiveMovement()
//            } else {
//                // Fallback on earlier versions
//            }
//            print("====> cancel")
//
//        }
//    }
}
