//
//  ChannelSelectionCell.swift
//  VLiveShow
//
//  Created by VincentX on 10/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

import UIKit

class ChannelSelectionCell: UICollectionViewCell
{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var actionBlock: ((_ cell: ChannelSelectionCell)-> Void)?
    
    @IBAction func didPressActionButton()
    {
        actionBlock?(self)
    }
}
