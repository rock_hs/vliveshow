//
//  UserChannelManager.swift
//  VLiveShow
//
//  Created by VincentX on 10/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

import UIKit

class UserChannelManager
{
    static let channelKey = "channels:all"
    static let starredKey = "channels:starred"
    
    lazy var allChannels: [String: VLSChannel] = UserChannelManager.loadAllChannelDictionary()
    
    static func saveAllChannels(channels: [[String: Any]])
    {
        guard let account = AccountManager.shared().account,
            account.uid > 0
        else
        {
            return
        }
        UserDefaults.standard.set(channels, forKey: "\(account.uid):" + channelKey)
        UserDefaults.standard.synchronize()
    }
    
    static func loadAllChannelDictionary() ->  [String: VLSChannel]
    {
        guard let account = AccountManager.shared().account,
            account.uid > 0
            else
        {
            return [:]
        }
        guard let list = UserDefaults.standard.value(forKey: "\(account.uid):" + channelKey) as? [[String: Any]] else
        {
            return [:]
        }
        var result: [String: VLSChannel] = [:]
        list.forEach
        { c in
            if let channel = VLSChannel.mj_object(withKeyValues: c),
                let channelId = channel.channelId
            {
                result["\(channelId.intValue)"] = channel
            }
        }

        return result
    }
    
    static func loadAllChannels() ->  [VLSChannel]
    {
        guard let account = AccountManager.shared().account,
            account.uid > 0
            else
        {
            return []
        }
        guard let list = UserDefaults.standard.value(forKey: "\(account.uid):" + channelKey) as? [[String: Any]] else
        {
            return []
        }
        let result = list.flatMap
        { (v) -> VLSChannel? in
            return VLSChannel.mj_object(withKeyValues: v)
        }
        
        return result
    }
    
    static func saveStarredChannels(channels: [VLSChannel])
    {
        guard let account = AccountManager.shared().account,
            account.uid > 0
            else
        {
            return
        }
        let ids = channels.flatMap{ $0.channelId?.intValue}
        UserDefaults.standard.set(ids, forKey: "\(account.uid):" + starredKey)
    }
    
    static func loadStarredChannels() -> [VLSChannel]
    {
        guard let account = AccountManager.shared().account,
            account.uid > 0
            else
        {
            return []
        }
        let all = loadAllChannelDictionary()
        var toShowChannels: [VLSChannel]  = []
        if let ids = UserDefaults.standard.value(forKey: "\(account.uid):" + starredKey) as? [Int],
            ids.count > 0
        {
            var pinnedChannels: [VLSChannel] = []
            var newChannels: [VLSChannel] = []
            let channels = ids.flatMap(
                { (id) -> VLSChannel? in
                    if let channel = all["\(id)"]
                    {
                        if let type = channel.type,
                            type != "pinned"
                        {
                            // 过滤掉本地pinned频道，统一从all里面获取
                            return channel
                        }
                    }
                    return nil
            })
            all.forEach(
            { (id, channel) in
                if let type = channel.type
                {
                    if (type == "pinned")
                    {
                        pinnedChannels.append(channel)
                    }
                    else if (type == "special")
                    {
                        if let _ = channels.index(where: { (c) -> Bool in
                            if let cid0 = c.channelId,
                                let cid1 = channel.channelId,
                                cid0 == cid1
                            {
                                return true
                            }
                            return false
                        })
                        {
                            
                        }
                        else
                        {
                            // 新的special标签
                            newChannels.append(channel)
                        }

                    }
                }
            })
            toShowChannels.append(contentsOf: pinnedChannels)
            toShowChannels.append(contentsOf: channels)
            toShowChannels.append(contentsOf: newChannels)
            
            return toShowChannels
        }
        return  loadDefaultStarred()
    }
    
    static func loadDefaultStarred() -> [VLSChannel]
    {
        let all = loadAllChannels()
        return all.flatMap
        { (channel) -> VLSChannel? in
            if channel.type == "pinned"
                || channel.type == "special"
            {
                return channel
            }
            return nil
            }
    }
}
