//
//  VLSPaymentHistoryLoader.swift
//  VLiveShow
//
//  Created by VincentX on 8/19/16.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit

class VLSPaymentHistoryLoader: VLSPageLoader<VLSPaymentRecord>
{
    override func loadMore(_ complete: ((_ error: NSError?) -> Void)?)
    {
        let callback = HttpCallBack()
        callback.doneBlock = { object in
            
            if  let dict = object as? [String: Any],
                let results = dict["results"] as? [String: AnyObject],
                let success = dict["success"] as? Bool,
                let list = results["list"] as? [[String: AnyObject]], success == true
            {
                self.updateListPage(list)
                complete?(nil)
            }
        }
        callback.errorBlock = { error in
            if let err = error as? NSError
            {
                complete?(err)
            }
        }
        HTTPFacade.getMoneyPayment(withPageNumber: self.currentPage + 1, pageSize: self.pageSize, callback: callback)
    }
}
