//
//  VLSPageLoader.swift
//  VLiveShow
//
//  Created by VincentX on 8/19/16.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit

class VLSPageLoader<V: VLSJSONConvertable>: NSObject
{
    var currentPage: Int = 0
    var pageSize: Int = 50
    fileprivate var _hasMore: Bool = true
    
    var dictionary: [V.ID : V] = [:]
    var array: [V] = []
    
    func loadBegin(_ complete: ((_ error: NSError?) -> Void)?)
    {
        _hasMore = true
        currentPage = 0
        dictionary = [:]
        array = []
        loadMore(complete)
    }
    
    func loadMore(_ complete: ((_ error: NSError?) -> Void)?)
    {
        
    }
    
    func hasMore () -> Bool
    {
        return _hasMore
    }
    
    func updateListPage(_ values: [[String: AnyObject]])
    {
        //var validNum = 0
        values.forEach { (dict) in
            if let value = V.fromJSON(dict as AnyObject)
            {
                if let _ = dictionary[value.identifier()]
                {
                    return
                }
                //validNum += 1
                dictionary[value.identifier()] = value
                array.append(value)
            }
        }
        if values.count < self.pageSize // || validNum == 0
        {
            _hasMore = false
        }
        currentPage += 1
    }
}
