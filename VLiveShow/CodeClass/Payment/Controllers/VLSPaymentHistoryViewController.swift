//
//  VLSPaymentHistoryViewController.swift
//  VLiveShowSwift
//
//  Created by VincentX on 8/4/16.
//  Copyright © 2016 vipabc. All rights reserved.
//

import UIKit

class VLSPaymentHistoryViewController: VLSBaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var emptyView: UIView!
    
    var loader: VLSPaymentHistoryLoader = VLSPaymentHistoryLoader()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loader.pageSize = 50
        self.showProgressHUD()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.loader.loadBegin(self.didReloadData)
        // Do any additional setup after loading the view.x
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.loader.array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VLSPaymentHistoryCell
        cell.setPaymentRecord(self.loader.array[indexPath.row])
        return cell
    }
    
    func showEmptyViewIfNeeded()
    {
        self.tableView.isHidden = self.loader.array.count == 0
        self.emptyView.isHidden = !self.tableView.isHidden
    }
    
    func didReloadData(_ error: NSError?)
    {
        self.hideProgressHUD(afterDelay: 0)
        if let errStr = error?.localizedDescription
        {
            self.toastHUD(withStr: errStr, dimBackground: true)
        }
        else
        {
            self.tableView.reloadData()
            showEmptyViewIfNeeded()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        guard self.needShowLoading() else
        {
            return nil
        }

        let view = Bundle.main.loadNibNamed("VLSPaymentLoadMore", owner: nil, options: [:])?[0] as? UIView

        if let label = view?.viewWithTag(1001) as? UILabel
        {
            label.text = _LS("LIVE_PULL_LOAD_MORE")
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return self.needShowLoading() ? 44: 0.5
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int)
    {
        if self.loader.hasMore()
        {
            self.loader.loadMore(self.didReloadData)
        }
    }
    
    func needShowLoading() -> Bool
    {
        return self.loader.hasMore() && loader.array.count > 0
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
