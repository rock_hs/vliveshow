//
//  VLSDiamondsViewController.swift
//  VLiveShowSwift
//
//  Created by VincentX on 8/3/16.
//  Copyright © 2016 vipabc. All rights reserved.
//

import UIKit
import StoreKit

enum VLSDiamondsTableSection: Int
{
    case current = 0
    case purchaseItem
    
    case count
}

class VLSDiamondsViewController: VLSBaseViewController, UITableViewDataSource, UITableViewDelegate, SKProductsRequestDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var teamVButton: UIButton!
    var productsRequest: SKProductsRequest?
    
    var originalBarBackgroundImage: UIImage?
    var originalTintColor: UIColor?
    var originalTitleAttr: [String: Any]?;
    
    // hold the model to prevent dealloc
    var accountModel: AccountModel?
    
    var products:[SKProduct] = []
    {
        didSet
        {
            if self.isViewLoaded
            {
                self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
            }
        }
    }
    
    deinit
    {
        self.productsRequest?.cancel()
        self.accountModel?.removeObserver(self, forKeyPath: "diamondBalance", context: nil)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)        
        
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(loadCurrentBlance), name: VLSIAPManagerTransactionFinishedWithSuccessNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didStartUploadingReceipt), name: NSNotification.Name(rawValue: VLSIAPManagerStartUploadingReceiptNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didFinishUploadingReceipt), name: NSNotification.Name(rawValue: VLSIAPManagerFinishUploadingReceiptNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEndTransactionWithFailure(_:)), name: NSNotification.Name(rawValue: VLSIAPManagerTransactionFinishedWithFailureNotification), object: nil)
        
        self.accountModel = AccountManager.shared().account
        self.accountModel?.addObserver(self, forKeyPath: "diamondBalance", options: [.new], context: nil)
        self.localizeTeamVButton()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.loadIdentifiers(self.loadProducts)
        self.reloadCurrentBlance()
        
        self.originalBarBackgroundImage = self.navigationController?.navigationBar.backgroundImage(for: .topAttached, barMetrics: .default)
        self.originalTintColor = self.navigationController?.navigationBar.tintColor
        self.originalTitleAttr = self.navigationController?.navigationBar.titleTextAttributes
        
        var attrDict = self.navigationController?.navigationBar.titleTextAttributes
        if attrDict == nil
        {
            attrDict = [NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        }
        
        attrDict![NSForegroundColorAttributeName] = UIColor.titleColorInLightNavi()
        self.navigationController?.navigationBar.titleTextAttributes = attrDict
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .topAttached, barMetrics: .default)
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.tintColor = UIColor.titleColorInLightNavi()
        self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func willMove(toParentViewController parent: UIViewController?)
    {
        super.willMove(toParentViewController: parent)
        if parent == nil
        {
            self.navigationController?.navigationBar.tintColor = self.originalTintColor
            self.navigationController?.navigationBar.titleTextAttributes = self.originalTitleAttr
            self.navigationController?.navigationBar.setBackgroundImage(self.originalBarBackgroundImage, for: .topAttached, barMetrics: .default)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return VLSDiamondsTableSection.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == VLSDiamondsTableSection.purchaseItem.rawValue
        {
            return products.count // test
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let ids = ["CurrentDiamonds", "PurchaseItem"]
        let identifier = ids[indexPath.section]
        var cell: UITableViewCell? = nil
        if indexPath.section == VLSDiamondsTableSection.current.rawValue
        {
            let dCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? VLSCurrentDiamondCell
            if let account = self.accountModel
            {
                dCell?.currentDiamondLabel.text = "\(account.diamondBalance)"
            }
            cell = dCell
        }
        else if indexPath.section == VLSDiamondsTableSection.purchaseItem.rawValue
        {
            let product = self.products[indexPath.row]
            let productCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? VLSDiamondPurchaseCell
            productCell?.product = product
            productCell?.buyBlock = {[weak self] cell in
                self?.didPressBuyButton(cell)
            }
            cell = productCell
        }
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == VLSDiamondsTableSection.purchaseItem.rawValue
        {
            return 32
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        guard section == VLSDiamondsTableSection.purchaseItem.rawValue else
        {
            return UITableViewHeaderFooterView(frame: CGRect.zero)
        }
        
        let identifier = "section"
        var header = tableView.dequeueReusableHeaderFooterView(withIdentifier: identifier)
        if header == nil
        {
            header = UITableViewHeaderFooterView(reuseIdentifier: identifier)
            header?.contentView.backgroundColor = UIColor.bgLightGray()
            header?.textLabel?.text = _LS("PLEASE_SELECT_RPODUCT_TO_PURCHASE")
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView
        {
            header.textLabel?.font = UIFont.systemFont(ofSize: 12)
            header.textLabel?.textColor = UIColor.black
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
    {
        self.hideProgressHUD(afterDelay: 0)
        self.products = response.products.sorted
        {
            return $0.0.price.compare($0.1.price) != .orderedDescending
        }
        if self.products.count == 0
        {
            self.showRequestError()
        }
        self.productsRequest = nil
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error)
    {
        self.showRequestError()
        self.productsRequest = nil
    }
    
    func loadIdentifiers(_ complete: (([String])-> ())?)
    {
        // Call API to fetch identifiers
        guard SKPaymentQueue.canMakePayments() else
        {
            complete?([])
            return
        }
        self.showProgressHUD()
        let callback = HttpCallBack()
        callback.doneBlock = {[weak self] object in
            if let results = object as? [String: AnyObject],
                let products = results["results"] as? [String: AnyObject],
                let list = products["products"] as? [String]
            {
                complete?(list)
                return
            }
            self?.hideProgressHUD(afterDelay: 0)
            complete?([])
        }
        callback.errorBlock = {[weak self] error in
            self?.hideProgressHUD(afterDelay: 0)
            complete?([])
        }
        HTTPFacade.getIAPProducts(callback)
        
//        complete?(["com.live.vipabc.vdiamond.60",
//                "com.live.vipabc.vdiamond.300",
//                "com.live.vipabc.vdiamond.980"
//            ])
    }
    
    func loadProducts(_ identifiers: [String])
    {
        if let _ = productsRequest
        {
            return
        }
        self.productsRequest = SKProductsRequest(productIdentifiers: Set(identifiers))
        self.productsRequest?.delegate = self
        self.productsRequest?.start()
    }
    

    
    func didPressBuyButton(_ cell: VLSDiamondPurchaseCell)
    {
        if let product = cell.product
        {
            self.showProgressHUD()
            SKPaymentQueue.default().add(SKPayment(product: product))
        }
    }

    func localizeTeamVButton()
    {
        let attrStr = NSAttributedString(string: _LS("V_TEAM"), attributes:
            [NSFontAttributeName : UIFont.systemFont(ofSize: 15),
            NSForegroundColorAttributeName: UIColor.themeRed(),
            NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue
            ])
        self.teamVButton.setAttributedTitle(attrStr, for: UIControlState())
    }
    
    func showPaymentFailure()
    {
        self.toastHUD(withStr: _LS("FAILED_TO_PAY"), dimBackground: false)
    }
    
    func showRequestError()
    {
        self.toastHUD(withStr: _LS("FAILED_TO_FETCH_IAP_PRODUCTS"), dimBackground: false)
    }
    
    @IBAction func didPressVTeamButton()
    {
        let vc = ContactUsController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func reloadCurrentBlance()
    {
        VLSIAPManager.sharedInstance.loadCurrentBlance(nil)
    }
    
    func didStartUploadingReceipt()
    {
        DispatchQueue.main.async
        {[weak self] in
            self?.showProgressHUD(withStr: _LS("ADDING_VDIAMONDS_PLEASE_WAIT"), dimBackground: false)
        }
    }
    
    func didFinishUploadingReceipt()
    {
        DispatchQueue.main.async
        {[weak self] in
            self?.hideProgressHUD(afterDelay: 0)
        }
    }
    
    func didEndTransactionWithFailure(_ notification: Notification)
    {
        DispatchQueue.main.async
        {[weak self] in
            self?.hideProgressHUD(afterDelay: 0)
        }
    }
   
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if keyPath == "diamondBalance"
        {
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
