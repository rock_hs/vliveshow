//
//  VLSIAPManager.swift
//  VLiveShow
//
//  Created by VincentX on 8/15/16.
//  Copyright © 2016 vliveshow. All rights reserved.
//

// 开源的IAPHelper写得太烂了，RMStore写得可以，但是需要添加openssl，在无法添加pod情况下过于麻烦，所以我只能自己实现IAP的一些功能。
// 之所以说IAPHelper烂是因为这个货只能检测某一次的购买行为，
// 而用户可能在购买后退出了App，苹果的StoreKit会在每次Launch时候去检测哪些被中断的Transaction，然后重新执行Transaction流程，
// IAPHelper不单不能监测到这样的Transaction，甚至没有walk around方法。
// 苹果官方提供了 In-App Purchase Best Practices文章：https://developer.apple.com/library/ios/technotes/tn2387/_index.html



import UIKit
import StoreKit
import SSKeychain
import MBProgressHUD

enum ReceiptVerificationResult: String
{
    case Success = "SUCCESS"
    case Duplicate = "RECEIPT_IN_USED"
    case Invalid = "RECEIPT_INVALID"
    case AppleUnavailable = "APP_STORE_OUT_OF_REACH"
    case ServereInternalError = "SYSTEM_INTERNAL_ERROR"
}

let VLSIAPManagerTransactionFinishedWithSuccessNotification = "VLSIAPManagerTransactionFinishedWithSuccessNotification"
let VLSIAPManagerStartUploadingReceiptNotification = "VLSIAPManagerStartUploadReceiptNotification"
let VLSIAPManagerFinishUploadingReceiptNotification = "VLSIAPManagerFinishUploadingReceiptNotification"
let VLSIAPManagerTransactionFinishedWithFailureNotification = "VLSIAPManagerTransactionFinishedWithFailureNotification"



@objc class VLSIAPManager: NSObject, SKPaymentTransactionObserver
{
    static var sharedInstance = VLSIAPManager()
    
    static let KeyChainDomain = "com.live.vliveshow.receipt"
    
    override init()
    {
        super.init()
        self.addObserverAfterLaunch()
        self.recoverUnfinishedReceipts()
        
    }
    
    func addObserverAfterLaunch()
    {
        SKPaymentQueue.default().add(self)
    }
    
    func removeObserverAfterTerminated()
    {
        SKPaymentQueue.default().remove(self)
    }
    
    // MARK: - SKPaymentTransactionObserver
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
    {
        transactions.forEach
        { (transaction) in
            if transaction.transactionState == .purchased
               || transaction.transactionState == .restored
            {
                queue.finishTransaction(transaction)
                if let receiptURL = Bundle.main.appStoreReceiptURL
                {
                    print("url: \(receiptURL)")
                    if let receiptData = try? Data(contentsOf: receiptURL)
                    {
                        var  transactionIdentifier = transaction.original?.transactionIdentifier
                        if transactionIdentifier == nil
                        {
                            transactionIdentifier = transaction.transactionIdentifier
                        }
                        if let trId = transactionIdentifier
                        {
                            VLSUserTrackingManager.share().trackingIAP(AccountManager.shared().account, orderId: trId, amount: transaction.payment.quantity, currencyType: "", payType: "IAP")
                            storeReceipt(receiptData, identifier: trId)
                            requestToServer(receiptData, identifier: trId)
                        }
                    }
                }
            }
            else if transaction.transactionState == .failed
            {
                NotificationCenter.default.post(name: Notification.Name(rawValue: VLSIAPManagerTransactionFinishedWithFailureNotification), object: self, userInfo: nil)
                queue.finishTransaction(transaction)
            }
        }
    }
    
    func recoverUnfinishedReceipts()
    {
        guard let serName = getServiceName() else
        {
            return
        }
        
        if let accounts = SSKeychain.accounts(forService: serName)
        {
            accounts.forEach(
            { (dict) in
                if let transactionIdentifier = dict[kSSKeychainAccountKey] as? String,
                let receiptData = SSKeychain.passwordData(forService: serName, account: transactionIdentifier)
                {
                    
                    self.requestToServer(receiptData, identifier: transactionIdentifier)
                }
            })
        }
    }
    
    func storeReceipt(_ receipt: Data, identifier: String)
    {
        guard let serName = getServiceName() else
        {
            return
        }
        SSKeychain.setPasswordData(receipt, forService: serName, account: identifier)
    }
    
    func requestToServer(_ receipt: Data, identifier: String)
    {
        guard let serName = getServiceName() else
        {
            return
        }
        
        let callback = HttpCallBack()
        
        callback.doneBlock = {[weak self] object in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: VLSIAPManagerFinishUploadingReceiptNotification), object: nil, userInfo: nil)
            
            if let dict = object as? [String: AnyObject],
                let success = dict["success"] as? Bool,
                let results = dict["results"] as? [String: AnyObject],
                let state = results["state"] as? String
            {
                if success && state == ReceiptVerificationResult.Success.rawValue
                {
                    self?.didPaySucceed(identifier)
                }
                else
                {
                    if state == ReceiptVerificationResult.Invalid.rawValue
                    {
                        self?.showAlert(_LS("RECHARGE_FAILED"), message: _LS(state))
                    }
                    else if state == ReceiptVerificationResult.AppleUnavailable.rawValue
                    || state == ReceiptVerificationResult.ServereInternalError.rawValue
                    {
                        self?.showAlert(_LS(state), message: _LS("PLEASE_TRY_AFTER_RELAUNCH"))
                    }
                    
                    if state == ReceiptVerificationResult.Duplicate.rawValue
                    || state == ReceiptVerificationResult.Invalid.rawValue
                    {
                        SSKeychain.deletePassword(forService: serName, account: identifier)
                    }
                }
            }
            else
            {
                self?.showToast(_LS("ALERT_UNKNOW_ERROR"))
            }
        }
        callback.errorBlock = {[weak self] error in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: VLSIAPManagerFinishUploadingReceiptNotification), object: nil, userInfo: nil)
            if let err = error as? NSError
            {
                self?.showToast(err.localizedDescription)
            }
            else
            {
                self?.showToast(_LS("ALERT_UNKNOW_ERROR"))
            }
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: VLSIAPManagerStartUploadingReceiptNotification), object: nil, userInfo: nil)
        HTTPFacade.uploadIAPReceipt(receipt, callback: callback)
    }
    
    @objc func loadCurrentBlance(_ complete: ((Int, NSError?)-> Void)?)
    {
        let callback = HttpCallBack()
        callback.doneBlock = {object in
            if let dict = object as? [String: AnyObject],
                let success = dict["success"] as? Bool, success == true,
                let results = dict["results"] as? [String: AnyObject],
                let diamond = results["diamond"] as? Int
            {
                AccountManager.shared().account.diamondBalance = diamond
                complete?(diamond, nil)
                return
            }
            complete?(0, NSError(domain: "", code: -1, userInfo: [:]))
        }
        callback.errorBlock = { error in
            complete?(0, NSError(domain: "", code: -1, userInfo: [:]))
        }
        HTTPFacade.getUserMoneyProperty(withCallback: callback)
    }
    
    func didPaySucceed(_ identifier: String)
    {
        guard let serName = getServiceName() else
        {
            return
        }
        self.loadCurrentBlance(nil)
        //self.showAlert(_LS("PURCHASE_COMPLETED"))
        SSKeychain.deletePassword(forService: serName, account: identifier)
        NotificationCenter.default.post(name: Notification.Name(rawValue: VLSIAPManagerTransactionFinishedWithSuccessNotification), object: self)
    }
    
    func showAlert(_ str: String, message: String? = nil)
    {
        if UIApplication.shared.windows.count > 0
        {
            let alert = UIAlertController(title: str, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: _LS("OK"), style: .default, handler:
            { (_) in
                alert.dismiss(animated: true, completion: nil)
            }))
            UIApplication.shared.windows[0].rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func showToast(_ str: String)
    {
        if UIApplication.shared.windows.count > 0
        {
            let window = UIApplication.shared.windows[0]
            let hud = MBProgressHUD.showAdded(to: window, animated: true)
            hud?.labelText = _LS("RECHARGE")
            hud?.detailsLabelText = str
            hud?.mode = .text
            hud?.dimBackground = true
            hud?.hide(true, afterDelay: 3)
        }
    }
    
    func getServiceName() -> String?
    {
        guard let userAccount = AccountManager.shared().account else
        {
            return nil
        }
        return VLSIAPManager.KeyChainDomain + ":\(userAccount.uid)"
    }
}
