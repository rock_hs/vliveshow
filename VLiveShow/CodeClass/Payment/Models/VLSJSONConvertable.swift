//
//  VLSJSONConvertable.swift
//  VLiveShow
//
//  Created by VincentX on 8/19/16.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import Foundation

protocol VLSJSONConvertable: NSObjectProtocol
{
    associatedtype ID: Hashable
    static func fromJSON(_ jsonObject: AnyObject) -> Self?
    
    func identifier() -> ID
}
