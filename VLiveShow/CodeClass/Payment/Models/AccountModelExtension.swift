//
//  AccountModelExtension.swift
//  VLiveShow
//
//  Created by VincentX on 8/22/16.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import Foundation

let DiamondKey = "diamond"

extension AccountModel
{
    var diamondBalance: Int
    {
        get {
            return UserDefaults.standard.integer(forKey: "\(self.uid):" + DiamondKey) ?? 0
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "\(self.uid):" + DiamondKey)
        }
    }
}
