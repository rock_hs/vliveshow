//
//  VLSPaymentRecord.swift
//  VLiveShowSwift
//
//  Created by VincentX on 8/4/16.
//  Copyright © 2016 vipabc. All rights reserved.
//

import UIKit

class VLSPaymentRecord: NSObject, VLSJSONConvertable {
    var charge: Int = 0
    var balance: Int?
    var date: Date?
    fileprivate var _identifier: Int = 0
    var method: String?
    
    typealias ID = Int
    
    required override init() {
        
    }
    
    static func fromJSON(_ jsonObject: AnyObject) -> Self?
    {
        guard let dictionary = jsonObject as? [String: AnyObject] else
        {
            return nil
        }
        if let id = dictionary["id"] as? Int,
           let charge = dictionary["vdiamonds"] as? Int
        {
            let paymentRecord = self.init()
            paymentRecord._identifier = id
            paymentRecord.charge = charge
            paymentRecord.method = dictionary["method"] as? String
            paymentRecord.balance = dictionary["balance"] as? Int
            if let dateStr = dictionary["date"] as? String
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                paymentRecord.date = formatter.date(from: dateStr)
            }
            return paymentRecord
        }
        return nil
    }
    
    func identifier() -> ID
    {
        return _identifier
    }
}
