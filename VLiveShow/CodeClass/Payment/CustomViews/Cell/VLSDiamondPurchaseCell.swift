//
//  VLSDiamondPurchaseTableViewCell.swift
//  VLiveShowSwift
//
//  Created by VincentX on 8/3/16.
//  Copyright © 2016 vipabc. All rights reserved.
//

import UIKit
import StoreKit

class VLSDiamondPurchaseCell: UITableViewCell {
    
    @IBOutlet var diamondNumberLabel: UILabel!
    @IBOutlet var priceButton: UIButton!
    var buyBlock: ((_ cell: VLSDiamondPurchaseCell) -> ())?
    var product: SKProduct?
    {
        didSet
        {
            self.updateData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateData()
    {
        // When IAP items is rejected, roduct?.localizedTitle will be empty.
        if let localisedTitle = product?.localizedTitle, !localisedTitle.isEmpty
        {
            self.diamondNumberLabel.text = localisedTitle
        }
        else if let identifier = product?.productIdentifier
        {
            self.diamondNumberLabel.text = _LS("IAP-\(identifier)")
        }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.formatterBehavior = .behavior10_4
        formatter.locale = product?.priceLocale
        if let price = self.product?.price
        {
            let priceStr = formatter.string(from: price)
            self.priceButton.setTitle(priceStr?.replacingOccurrences(of: ".00", with: ""), for: UIControlState())
        }
        else
        {
            self.priceButton.setTitle("", for: UIControlState())
        }
    }
    
    @IBAction func didPressBuyButton()
    {
        buyBlock?(self)
    }
}
