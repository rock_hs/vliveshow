//
//  VLSPaymentHistoryCell.swift
//  VLiveShowSwift
//
//  Created by VincentX on 8/4/16.
//  Copyright © 2016 vipabc. All rights reserved.
//

import UIKit

class VLSPaymentHistoryCell: UITableViewCell {
    
    @IBOutlet var boughtNumberLabel: UILabel!
    @IBOutlet var remainingLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension VLSPaymentHistoryCell
{
    func setPaymentRecord(_ record: VLSPaymentRecord)
    {
        let attrStr = NSMutableAttributedString(string: _LS("RECHARGE"),
                                                attributes: [NSForegroundColorAttributeName : boughtNumberLabel.textColor,
                                                    NSFontAttributeName: boughtNumberLabel.font
        ])
        let numberStr = NSAttributedString(string: " \(record.charge) ", attributes: [NSForegroundColorAttributeName : UIColor.themeRed(),
            NSFontAttributeName: boughtNumberLabel.font
            ])
        attrStr.append(numberStr)
        
        let lastStr = NSAttributedString(string: _LS("VDIAMONDS"), attributes: [NSForegroundColorAttributeName : boughtNumberLabel.textColor,
            NSFontAttributeName: boughtNumberLabel.font
            ])
        attrStr.append(lastStr)
        self.boughtNumberLabel.attributedText = attrStr
        self.remainingLabel.text = record.balance != nil ? String(format: _LS("BALANCE: %d"), record.balance!): ""

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.dateLabel.text = record.date != nil ? formatter.string(from: record.date! as Date) : ""
        
        self.typeLabel.text = record.method != nil ? _LS("\(record.method!.lowercased())") : ""
    }
}
