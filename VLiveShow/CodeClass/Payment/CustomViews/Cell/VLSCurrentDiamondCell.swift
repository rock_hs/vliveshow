//
//  VLSCurrentDiamondCell.swift
//  VLiveShowSwift
//
//  Created by VincentX on 8/3/16.
//  Copyright © 2016 vipabc. All rights reserved.
//

import UIKit

class VLSCurrentDiamondCell: UITableViewCell {
    
    @IBOutlet var currentDiamondLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
