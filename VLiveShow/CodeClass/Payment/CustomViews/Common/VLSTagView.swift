//
//  VLSTagView.swift
//  VLiveShowSwift
//
//  Created by VincentX on 7/22/16.
//  Copyright © 2016 vipabc. All rights reserved.
//

import UIKit

@IBDesignable class VLSTagView2: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 5
        self.backgroundColor = UIColor.red
        self.titleLabel?.textColor = UIColor.darkGray
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
    }
}
