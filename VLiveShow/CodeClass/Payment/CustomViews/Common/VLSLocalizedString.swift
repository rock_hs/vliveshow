//
//  VLSLocalizedString.swift
//  VLiveShowSwift
//
//  Created by VincentX on 7/25/16.
//  Copyright © 2016 vipabc. All rights reserved.
//

import UIKit

func _LS(_ str: String) -> String
{
    return NSLocalizedString(str, comment: "")
}
