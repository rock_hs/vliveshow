;//
//  UIColor_UX.h
//  VLiveShow
//
//  Created by VincentX on 8/10/16.
//  Copyright © 2016 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (UX)

+ (UIColor*)themeRed;
+ (UIColor*)bgLightGray;
+ (UIColor*)tagYellow;
+ (UIColor*)tagPurple;
+ (UIColor*)textGray;
+ (UIColor*)titleColorInLightNavi;
+ (UIColor*)c7c7c7;
+ (UIColor*)x777777;
@end
