//
//  UIColor_UX.h
//  VLiveShow
//
//  Created by VincentX on 8/10/16.
//  Copyright © 2016 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@implementation UIColor(UX)

+ (UIColor*)themeRed
{
    return RGB16(COLOR_BG_FF1130);
}

+ (UIColor*)bgLightGray
{
    return RGB16(COLOR_BG_F2F2F2);
}

+ (UIColor*)tagYellow
{
    return RGB16(COLOR_FONT_C3A851);
}
    
+ (UIColor*)tagPurple
{
    return RGB16(0x7780ff);
}

+ (UIColor*)textGray
{
    return RGB16(COLOR_FONT_9B9B9B);
}

+ (UIColor*)titleColorInLightNavi
{
    return RGB16(COLOR_FONT_4A4A4A);
}

+ (UIColor*)c7c7c7
{
    return RGB16(COLOR_FONT_C7C7C7);
}

+ (UIColor*)x777777
{
    return RGB16(COLOR_FONT_777777);
}
@end
