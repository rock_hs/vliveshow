//
//  FansTableViewCell.h
//  
//
//  Created by SXW on 16/6/3.
//
//

#import "VLSMineBaseViewCell.h"
#import "MineModel.h"
@class FansTableViewCell;
@protocol FansTableViewCellDelegate <NSObject>

- (void)fansBtnViewBtn:(FansTableViewCell *)cell;
- (void)blackBtnViewBtn:(FansTableViewCell *)cell;
- (void)pushNextFNVC:(FansTableViewCell *)cell;

@end
@interface FansTableViewCell : VLSMineBaseViewCell

@property (nonatomic, weak) id<FansTableViewCellDelegate>delegate;

- (void)configData:(MineModel*)model;
- (void)configBlackData:(MineModel*)model;
- (void)changeButtonInformation;




@end
