//
//  VLSMineBaseViewCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMineBaseViewCell.h"

@implementation VLSMineBaseViewCell

// 自绘分割线
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, rect);
    
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_F0F0F0).CGColor);
    CGContextStrokeRect(context, CGRectMake(75, rect.size.height, rect.size.width - 75, 1));
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
        self.nickImageView.hidden = YES;
    }
    return self;
}

- (void)initView{
    
    //头像frame
    _headerImage = [[UIImageView alloc] initWithImage:ICON_PLACEHOLDER];
    _headerImage.frame = CGRectMake(15, 8, 50, 50);
    _headerImage.layer.cornerRadius = 25;
    _headerImage.clipsToBounds = YES;
    _headerImage.contentMode = UIViewContentModeScaleAspectFill;
    
    _headBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _headBtn.frame = CGRectMake(15, 8, 50, 50);
    _headBtn.layer.cornerRadius = 25;
    _headBtn.clipsToBounds = YES;
    
    //用户名
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 14, 0, 22)];
    _nameLabel.font = [UIFont systemFontOfSize:16];
//    _nameLabel.backgroundColor = [UIColor greenColor];
    _nameLabel.textColor = [UIColor colorFromHexRGB:@"4A4A4A"];
    _nameBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _nameBtn.frame = CGRectZero;
    
    //个性签名
    _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, CGRectGetMaxY(_nameLabel.frame), 0,20)];
    _contentLabel.font = [UIFont systemFontOfSize:14];
    _contentLabel.textColor = [UIColor colorFromHexRGB:@"979797"];
//    _contentLabel.backgroundColor = [UIColor cyanColor];
    
    _focusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _focusBtn.frame = CGRectMake(SCREEN_WIDTH - 68, 21, 56,28);
    [_focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flow@3x"] forState:UIControlStateNormal];
    [_focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flowing@3x"] forState:UIControlStateSelected];
    _focusBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];

    [_focusBtn setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateNormal];
    [_focusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    _videoView = [[UIView alloc] init];
    _videoView.frame = CGRectMake(SCREEN_WIDTH - 91 - 46, 28, 46, 14);
    
    _liveImage = [[UIImageView alloc] init];
    _liveImage.frame = CGRectMake(0, 0, 13, 13);
    _liveImage.image = [UIImage imageNamed:@"video02"];
    
    _liveLabel = [[UILabel alloc] init];
    _liveLabel.frame = CGRectMake(16, 0, 30, 14);
    _liveLabel.textColor = RGB16(COLOR_FONT_FF1130);
    _liveLabel.text = LocalizedString(@"MINE_USER_PLAYING");
    _liveLabel.font = [UIFont systemFontOfSize:SIZE_FONT_10];
    
    _nameView = [[UIView alloc] init];
    _nameView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 14, 0, 22)];
    
    _nickNameLable = [[UILabel alloc] init];
    _nickNameLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 22)];
    _nickNameLable.font = [UIFont systemFontOfSize:16];
    _nickNameLable.textColor = [UIColor colorFromHexRGB:@"4A4A4A"];
    
    _nickImageView = [[UIImageView alloc] init];
    _nickImageView.frame = CGRectMake(self.videoView.frame.origin.x - 37, 27, 30, 16);//之前这里是35，不协调的话就改成35
    
    [self.contentView addSubview:_headerImage];
    [self.contentView addSubview:self.headBtn];
    [self.contentView addSubview:_nameLabel];
    [self.contentView addSubview:self.nameBtn];
//    [self.contentView addSubview:_nameView];
//    [self.nameView addSubview:self.nickNameLable];
    [self.contentView addSubview:self.nickImageView];
    [self.contentView addSubview:_contentLabel];
    [self.contentView addSubview:_focusBtn];
    
    [self.contentView addSubview:_videoView];
    /*直播中
    [self.videoView addSubview:_liveImage];
    [self.videoView addSubview:_liveLabel];
     */
}

//根据内容计算按钮长度
- (CGFloat)getWidthOfNameLabel:(NSString*)aString withFont:(CGFloat)fontSize withSize:(CGFloat)fontHeight{
    CGSize titleSize = [aString sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(MAXFLOAT, fontHeight)];
    return titleSize.width;
}

//获取连接
- (NSString*)getHttpString{
    NSURL* url = [NSURL URLWithString: BASE_URL];
    NSString* host = url.host;
    NSString* scheme = url.scheme;
    return [NSString stringWithFormat: @"%@://%@", scheme, host];
}

//获得昵称和性别
- (NSMutableAttributedString*)getNickNameAndSexString:(NSString*)modelNameStr withModelSexStr:(NSString*)modelSexStr withLevelNumber:(NSInteger)levelNumber{
    
    NSString *nameStr = modelNameStr;
    NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@  ",nameStr]];
    _attchLevel = [[NSTextAttachment alloc] init];
    // 添加表情
    _attchLevel.bounds = CGRectMake(0, -2, 30, 16);
    // 设置图片大小
    [self setBackGroundImageWithNumber:levelNumber withAttchment:_attchLevel andAttchment:_attch];

    NSAttributedString *stringLevel = [NSAttributedString attributedStringWithAttachment:_attchLevel];
    [attri appendAttributedString:stringLevel];
    if (levelNumber <=0 ) {
        [attri deleteCharactersInRange:NSMakeRange(nameStr.length, attri.length - nameStr.length)];
    }
    return attri;
}

#pragma mark - 简化代码方法
- (UIImage*)addText:(NSString*)text andImage:(UIImage*)img{
    //开启位图上下文
    UIGraphicsBeginImageContextWithOptions(img.size, NO, 0);
    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
    [text drawInRect:CGRectMake(0+img.size.width/2, 2, 30, img.size.height) withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10],NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIImage*currentImage=UIGraphicsGetImageFromCurrentImageContext();
    //关闭位图上下文
    UIGraphicsEndImageContext();
    return currentImage;
}

- (UIImage*)getImageFromNSInteger:(NSInteger)levelNum withLevelString:(NSString*)levelString{
    
    if (levelNum >= 1 && levelNum <=9) {
        UIImage *img = [self addText:[NSString stringWithFormat:@" %ld",levelNum] andImage:[UIImage imageNamed:levelString]];
        return img;
    }else{
        
        UIImage *img = [self addText:[NSString stringWithFormat:@"%ld",levelNum] andImage:[UIImage imageNamed:levelString]];
        return img;
    }
}

//设置头像
- (void)setHeadImageWithModelFileName:(NSString*)fileName{
    
    [self.headerImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[self getHttpString],fileName]] placeholderImage:ICON_PLACEHOLDER];
}

//个性签名、昵称、直播中
- (void)getUserIntroWith:(NSString*)modelIntro withLevelNumber:(NSInteger)levleNumber withIsLive:(NSString*)isLive{
    
    if ([isLive isEqualToString: @"Y"]) {
        self.videoView.hidden = NO;
        if (modelIntro == nil || [modelIntro isEqualToString:@" "]) {
            
            _contentLabel.hidden = YES;
            _nameBtn.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 25, [self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:20], 20);
            if (([self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:20] + 36 + 75) > self.videoView.frame.origin.x) {
                if (levleNumber <= 0) {
                    
                    _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 25, self.videoView.frame.origin.x - 75 - 10, 20);
                }else{
                
                    _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 25, self.videoView.frame.origin.x - 75 - 40, 20);
                }
                _nickImageView.hidden = NO;//显示等级需要修改部分NO
                [self setImageWithNumber:levleNumber withImageView:_nickImageView];
            }else{
            
                _nickImageView.hidden = YES;
                _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 25, [self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:20] + 36, 20);
            }
        }else{
        
            _contentLabel.hidden = NO;
            _nameBtn.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 14, [self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:22], 22);
            if (([self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:22] + 36) + 75 > self.videoView.frame.origin.x) {
                _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 14, self.videoView.frame.origin.x - 75 - 20, 22);
                _nickImageView.hidden = NO;//显示等级需要修改部分NO
                _nickImageView.frame = CGRectMake(self.videoView.frame.origin.x - 35, 17, 30, 16);
                [self setImageWithNumber:levleNumber withImageView:_nickImageView];
            }else{
            
                _nickImageView.hidden = YES;
                _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 14, [self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:20] + 36, 22);
            }
            
            _contentLabel.text = modelIntro;
            if ([self getWidthOfNameLabel:modelIntro withFont:14 withSize:20] + 75 > self.videoView.frame.origin.x) {
                _contentLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, CGRectGetMaxY(_nameLabel.frame), self.videoView.frame.origin.x - 75 - 20, 20);
            }else{
                _contentLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, CGRectGetMaxY(_nameLabel.frame), [self getWidthOfNameLabel:_contentLabel.text withFont:14 withSize:20], 20);
            }
        }
    }else{
    
        self.videoView.hidden = YES;
        if (modelIntro == nil || [modelIntro isEqualToString:@" "]) {
            
            _contentLabel.hidden = YES;
            _nameBtn.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 25, [self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:20], 20);
            if (([self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:20] + 36 + 75) > self.focusBtn.frame.origin.x) {
                _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 25, self.focusBtn.frame.origin.x - 75 - 34, 20);
                _nickImageView.hidden = NO;//显示等级需要修改部分NO
                _nickImageView.frame = CGRectMake(self.focusBtn.frame.origin.x - 35, 27, 30, 16);
                [self setImageWithNumber:levleNumber withImageView:_nickImageView];
            }else{
                
                _nickImageView.hidden = YES;
                _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 25, [self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:20] + 36, 20);
            }
        }else{
            
            _nameBtn.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 14, [self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:22], 22);
            if (([self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:22] + 36) + 75 > self.focusBtn.frame.origin.x) {
                _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 14, self.focusBtn.frame.origin.x - 75 - 30, 22);
                _nickImageView.hidden = NO;//显示等级需要修改部分NO
                _nickImageView.frame = CGRectMake(self.focusBtn.frame.origin.x - 35, 17, 30, 16);
                [self setImageWithNumber:levleNumber withImageView:_nickImageView];
            }else{
                
                _nickImageView.hidden = YES;
                _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, 14, [self getWidthOfNameLabel:_nameLabel.text withFont:16 withSize:20] + 36, 22);
            }
            
            _contentLabel.hidden = NO;
            _contentLabel.text = modelIntro;
            if ([self getWidthOfNameLabel:_contentLabel.text withFont:14 withSize:20] +36 + 75 > self.focusBtn.frame.origin.x) {
                _contentLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, CGRectGetMaxY(_nameLabel.frame), self.focusBtn.frame.origin.x - 75 - 30, 20);
            }else{
                _contentLabel.frame = CGRectMake(CGRectGetMaxX(_headerImage.frame) + 10, CGRectGetMaxY(_nameLabel.frame), [self getWidthOfNameLabel:_contentLabel.text withFont:14 withSize:20], 20);
            }
        }
    }
}

#pragma mark - 自定义菊花转
-(void)showWaiting:(UIView *)parent {
    
    int width = 20, height = 20;
    
    CGRect frame = CGRectMake(0, 0, 56,28);
    int x = frame.size.width;
    int y = frame.size.height;
    
    frame = CGRectMake((x - width) / 2, (y - height) / 2, width, height);
    UIActivityIndicatorView* progressInd = [[UIActivityIndicatorView alloc]initWithFrame:frame];
    [progressInd startAnimating];
    progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    
    frame =  CGRectMake(0, 0, 56,28);
    UIView *theView = [[UIView alloc] initWithFrame:frame];
    theView.alpha = 1;
    
    [theView addSubview:progressInd];
    [theView setTag:9999];
    [self.focusBtn addSubview:theView];
}

//消除滚动轮指示器
-(void)hideWaiting
{
    [[self viewWithTag:9999] removeFromSuperview];
}


//富文本 付图片
- (void)setBackGroundImageWithNumber:(NSInteger)number withAttchment:(NSTextAttachment*)attchment1 andAttchment:(NSTextAttachment*)attchment2{

    if (1 <= number && number <= 9) {
        
        attchment1.image = [self getImageFromNSInteger:number withLevelString:@"lv_1-9"];
    }else if (10 <= number && number <= 19){
        
        attchment1.image = [self getImageFromNSInteger:number withLevelString:@"lv_10-19"];
    }else if (20 <= number && number <= 29){
        
        attchment1.image = [self getImageFromNSInteger:number withLevelString:@"lv_20-29"];
    }else if (30 <= number && number <= 39){
        
        attchment1.image = [self getImageFromNSInteger:number withLevelString:@"lv_30-39"];
    }else if (40 <= number && number <= 49){
        
        attchment1.image = [self getImageFromNSInteger:number withLevelString:@"lv_40-49"];
    }else if(50 <= number){
        
        attchment1.image = [self getImageFromNSInteger:number withLevelString:@"lv_50"];
    }else{
        attchment1.bounds = CGRectMake(0, -2, 0, 0);
    }
}

//UIImageView 付图片
- (void)setImageWithNumber:(NSInteger)number withImageView:(UIImageView*)imageView{

    if (1 <= number && number <= 9) {
        
        imageView.image = [self getImageFromNSInteger:number withLevelString:@"lv_1-9"];
    }else if (10 <= number && number <= 19){
        
        imageView.image = [self getImageFromNSInteger:number withLevelString:@"lv_10-19"];
    }else if (20 <= number && number <= 29){
        
        imageView.image = [self getImageFromNSInteger:number withLevelString:@"lv_20-29"];
    }else if (30 <= number && number <= 39){
        
        imageView.image = [self getImageFromNSInteger:number withLevelString:@"lv_30-39"];
    }else if (40 <= number && number <= 49){
        
        imageView.image = [self getImageFromNSInteger:number withLevelString:@"lv_40-49"];
    }else if(50 <= number){
        
        imageView.image = [self getImageFromNSInteger:number withLevelString:@"lv_50"];
    }else{
        imageView.hidden = YES;
    }
}

















@end
