//
//  RemindTableViewCell.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemindTableViewCell : UITableViewCell

@property (nonatomic,strong)UISwitch *mySwitch;

@property (nonatomic,strong)UILabel *nameLabel;

@end
