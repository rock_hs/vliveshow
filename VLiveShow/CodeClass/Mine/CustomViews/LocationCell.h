//
//  LocationCell.h
//  VLiveShow
//
//  Created by SXW on 16/6/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationCell : UITableViewCell

@property (nonatomic, strong) UILabel *resultLabel;
@end
