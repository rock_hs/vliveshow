//
//  VLSMySettingCell.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSMySettingCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLable;
@property (nonatomic, strong) UILabel *detailTitleLabel;
@property (nonatomic, strong) UIImageView *inditorImageView;

@end
