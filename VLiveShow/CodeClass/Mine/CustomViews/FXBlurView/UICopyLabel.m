//
//  UICopyLabel.m
//  Menucontroller
//
//  Created by sxw on 16/1/13.
//  Copyright © 2016年 sxw. All rights reserved.
//

#import "UICopyLabel.h"


@implementation UICopyLabel

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return (action == @selector(copy:));
}

- (void)copy:(id)sender {
//    UIPasteboard 粘贴板
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.text;
}

//UILabel默认是不接收事件的，我们需要自己添加touch事件
- (void)attachTapHandler {
    self.userInteractionEnabled = YES;//用户交互
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    tap.numberOfTapsRequired = 1;//设置为双击补获事件
    [self addGestureRecognizer:tap];
}

//绑定事件
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self attachTapHandler];
    }
    return self;
}


//awakeFromNib是在UIView中用  viewDidLoad是在UIViewController里面用
- (void)awakeFromNib {
    [super awakeFromNib];
    [self attachTapHandler];
}

//接下来，我们需要处理这个tap，以便让菜单栏弹出来：
- (void)handleTap:(UITapGestureRecognizer *)recogniaer {
    [self becomeFirstResponder];//3.成为第一响应者
    
//当苹果增加了剪切、复制和粘贴功能时，它同时为开发者提供了 UIMenuController 组件用来定制该弹出菜单，
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setTargetRect:self.frame inView:self.superview];
    [menu setMenuVisible:YES animated:YES];
    
}


@end
