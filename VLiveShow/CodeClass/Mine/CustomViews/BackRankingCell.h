//
//  BackRankingCell.h
//  VLiveShow
//
//  Created by sp on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankingModel.h"
@interface BackRankingCell : UITableViewCell
@property (nonatomic,strong)UILabel *rankdingLabel;
@property (nonatomic,strong)UIImageView *headImageView;
@property (nonatomic,strong)UILabel * nameLabel;
@property (nonatomic,strong)UILabel *contentLabel;
@property (nonatomic,strong)UIView *separateLine;
@property (nonatomic,strong)RankingModel *rankingModel;
@property (nonatomic,strong)UILabel *levelabel;
@property (nonatomic,strong)UIView *levelView;
@property (nonatomic,strong)UIImageView *levelImageView;

@end
