//
//  VLSLineTableViewCell.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSLineTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *label;

@end
