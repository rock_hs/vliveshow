//
//  NavView.h
//  VLiveShow
//
//  Created by sp on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NavViewDelegate <NSObject>
- (void)turnBack;
@end
@interface NavView : UIView
@property (nonatomic,strong)UIImageView * backImageView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton*backButton;
@property (nonatomic,assign)id<NavViewDelegate>delegate;
@end
