//
//  NavView.m
//  VLiveShow
//
//  Created by sp on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "NavView.h"

@implementation NavView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self loadSubView];
    }
    return self;
}

- (void)loadSubView
{
    [self addSubview:self.titleLabel];
    [self addSubview:self.backImageView];
    [self addSubview:self.backButton];
//    self.titleLabel.sd_layout
//    .centerXIs(SC_WIDTH / 2);
//    self.backgroundColor = [UIColor yellowColor];
}

#pragma mark-get
- (UIImageView*)backImageView
{
    if (_backImageView == nil) {
        _backImageView = [[UIImageView alloc] initWithFrame:CGRectMake(18, 14, 9, 16)];
        _backImageView.image = [UIImage imageNamed:@"but_rankinglist_back"];
    }

    return _backImageView;
}
- (UILabel*)titleLabel
{
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2 -60, 0, 120, 44)];
        _titleLabel.attributedText = [self getTitleText];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont systemFontOfSize:17];
        _titleLabel.textColor = [UIColor whiteColor];
    }
    return _titleLabel;
}

- (NSMutableAttributedString*)getTitleText
{
    NSMutableAttributedString *nameAttri = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"V_TICKETS_RANKING")];
   NSTextAttachment * attch = [[NSTextAttachment alloc] init];
    attch.bounds = CGRectMake(0, -3, 16, 17);
    attch.image = [UIImage imageNamed:@"but_rankinglist_tickte"];
    NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:attch];

   [nameAttri insertAttributedString:string atIndex:0];

    return nameAttri;
}
- (UIButton*)backButton
{
    if (_backButton == nil) {
        _backButton = [UIButton buttonWithType:0];
        _backButton.frame = CGRectMake(0, 0, 50, 44);
        [_backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }

    return _backButton;
}
- (void)backAction
{
    if ([self.delegate respondsToSelector:@selector(turnBack)]) {
        
        [self.delegate turnBack];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
