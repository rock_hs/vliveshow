//
//  RemindTableViewCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "RemindTableViewCell.h"
#import "VLSLiveHttpManager.h"
@implementation RemindTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.mySwitch];
        [self.contentView addSubview:self.nameLabel];
    }
    return self;
}
- (UISwitch*)mySwitch
{
    if (_mySwitch == nil) {
        
        _mySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-60, 13, 100,20)];
        _mySwitch.onTintColor = [UIColor redColor];
        [_mySwitch addTarget:self action:@selector(switchChange:) forControlEvents:UIControlEventValueChanged];
    }
    return _mySwitch;
    
}

- (UILabel*)nameLabel
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 18, 200, 20)];
        _nameLabel.font = [UIFont systemFontOfSize:15];
        _nameLabel.textColor = [UIColor blackColor];
    }
    return _nameLabel;
}
- (void)switchChange:(UISwitch*)sender
{
    if (sender.on == YES) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isOP"];
        ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
        callback.updateBlock = ^(id result)
        {
            
        };
        callback.errorBlock = ^(id result)
        {
        };
     
        [VLSLiveHttpManager postLiveNotice:@"true" callback:callback];
    }else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isOP"];
        ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
        callback.updateBlock = ^(id result)
        {
            
        };
        callback.errorBlock = ^(id result)
        {
        };
        
        [VLSLiveHttpManager postLiveNotice:@"false" callback:callback];
    }
}


@end
