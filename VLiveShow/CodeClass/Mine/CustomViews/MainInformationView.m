//
//  MainInformationView.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/5/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "MainInformationView.h"
#import <SDAutoLayout/UIView+SDAutoLayout.h>
#import "AccountManager.h"
#import "Masonry.h"
#import "VLiveShow-Swift.h"

@interface MainInformationView()<VLSVoteViewDelegate>

@property (nonatomic, strong) VLSVoteView* voteView;

@end

@implementation MainInformationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self loadSubView];
        
        self.sexImage.hidden = YES;
        self.levelImg.hidden = YES;
        self.setBut.hidden = YES;
        self.topSetBut.hidden = YES;
        self.playRoomLab.hidden = YES;
    }
    return self;
}

- (void)loadSubView
{
    _userIcon = [[UIImageView alloc] init];
    _userIcon.layer.cornerRadius = 40;
    _userIcon.clipsToBounds = YES;
    _userIcon.image = ICON_PLACEHOLDER;
    
    _imageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_imageBtn addTarget:self action:@selector(btnImageClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _setBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_setBtn setImage:[UIImage imageNamed:@"ic_edit"] forState:UIControlStateNormal];
    [_setBtn addTarget:self action:@selector(setBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _nameLab = [[UILabel alloc] init];
    _nameLab.textColor = [UIColor blackColor];
    _nameLab.font = [UIFont boldSystemFontOfSize:SIZE_FONT_20];
    _nameLab.textAlignment = NSTextAlignmentCenter;
    
    _simpleLab = [[UILabel alloc] init];
    _simpleLab.textColor = [UIColor colorFromHexRGB:@"4A4A4A"];
    _simpleLab.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    _simpleLab.textAlignment = NSTextAlignmentCenter;
//    _simpleLab.numberOfLines = 0;
//    [_simpleLab sizeToFit];
    
    _numLabel = [[UILabel alloc] init];
    _numLabel.textColor = [UIColor colorFromHexRGB:@"9B9B9B"];
    _numLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
    _numLabel.textAlignment = NSTextAlignmentCenter;
    
    _levelLabel = [[UILabel alloc] init];
    _levelLabel.textColor = [UIColor whiteColor];
    _levelLabel.font = [UIFont systemFontOfSize:SIZE_FONT_10];
    _levelLabel.textAlignment = NSTextAlignmentRight;
    
    _sexImg = [[UIImageView alloc] init];
    _sexImage = [[UIImageView alloc] init];
    
    _levelImg = [[UIImageView alloc] init];
    
    _parentView = [[UIView alloc] init];
    _parentView.backgroundColor = [UIColor redColor];
    _levelImageView = [[UIImageView alloc] init];
    
    _setBut = [UIButton buttonWithType:0];
    [_setBut setImage:[UIImage imageNamed:@"video02"] forState:UIControlStateNormal];
    _topSetBut = [UIButton buttonWithType:0];
    [_topSetBut addTarget:self action:@selector(setViewBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    _playRoomLab = [[UILabel alloc] init];
    _playRoomLab.font = [UIFont systemFontOfSize:14];
    _playRoomLab.textAlignment = NSTextAlignmentCenter;
    _playRoomLab.text = LocalizedString(@"MINE_USER_PLAYING");
    _playRoomLab.textColor = RGB16(COLOR_FONT_FF1130);

    [self addSubview:self.userIcon];
    [self addSubview:self.imageBtn];
    [self addSubview:self.setBtn];
    [self addSubview:self.nameLab];
    [self addSubview:self.numLabel];
    [self addSubview:self.simpleLab];
    [self addSubview:self.sexImage];
    [self addSubview:self.levelImg];
    /*直播中
    [self addSubview:self.setBut];
    [self addSubview:self.topSetBut];
    [self addSubview:self.playRoomLab];
    */
//    [self addSubview:self.levelLabel];
//    [self addSubview:self.sexImg];
    
    _userIcon.sd_layout
    .leftSpaceToView(self,(SC_WIDTH - 80) / 2)
    .topSpaceToView(self, 54)
    .widthIs(80)
    .heightIs(80);
    
    _imageBtn.sd_layout
    .leftSpaceToView(self,(SC_WIDTH - 80) / 2)
    .topSpaceToView(self, 36)
    .widthIs(80)
    .heightIs(80);

    _nameLab.sd_layout
    .topSpaceToView(self.userIcon, 15)
    .leftSpaceToView(self, 20)
    .rightSpaceToView(self, 20)
    .heightIs(20);
    
    _sexImage.sd_layout
    .rightSpaceToView(self, 20)
    .topSpaceToView(self.userIcon, 17)
    .widthIs(16)
    .heightIs(16);
    
    _levelImg.sd_layout
    .leftSpaceToView(self.nameLab, 2)
    .topSpaceToView(self.userIcon, 17)
    .widthIs(30)
    .heightIs(16);
    
    _levelLabel.sd_layout
    .leftSpaceToView(self.nameLab, 8)
    .topSpaceToView(self.userIcon, 18)
    .widthIs(30)
    .heightIs(16);
    
    _sexImg.sd_layout
    .leftSpaceToView(self.levelLabel, 8)
    .topSpaceToView(self.userIcon, 18)
    .widthIs(16)
    .heightIs(16);
    
    _simpleLab.sd_layout
    .topSpaceToView(self.nameLab ,12)
    .rightSpaceToView(self, 30)
    .leftSpaceToView(self, 30)
    .heightIs(30);
//    .autoHeightRatio(0);
    
    _setBtn.sd_layout
    .topSpaceToView(self.nameLab ,12)
    .leftSpaceToView(self.simpleLab, 6)
    .widthIs(20)
    .heightIs(20);
    
    _numLabel.sd_layout
    .topSpaceToView(self.simpleLab, 10)
    .rightSpaceToView(self, 20)
    .leftSpaceToView(self, 20)
    .heightIs(20);
    
    _setBut.sd_layout
    .rightSpaceToView(self, 57)
    .topSpaceToView(self,37)
    .widthIs(13)
    .heightIs(13);
    
    _topSetBut.sd_layout
    .rightSpaceToView(self, 15)
    .topSpaceToView(self, 25)
    .widthIs(55)
    .heightIs(45);
    
    _playRoomLab.sd_layout
    .leftSpaceToView(self.setBut, 6)
    .topSpaceToView(self, 35)
    .widthIs(42)
    .heightIs(20);
    
    //新增 投票
    self.voteView = [[VLSVoteView alloc] init];
    self.voteView.delegate = self;
    [self addSubview:self.voteView];
    self.voteView.sd_layout
    .rightSpaceToView(self, 11)
    .widthIs(70)
    .heightIs(70)
    .topSpaceToView(self, 24);
}

//更新投票数
- (void)updateVoteModel:(NSDictionary*)dic
{
    VoteModel* voteModel = [VoteModel new];
    voteModel.votedId = [dic objectForKey:@"votedId"];
    voteModel.nickname = [dic objectForKey:@"nickname"];
    voteModel.residueVotes = [dic objectForKey:@"residueVotes"];
    voteModel.totalVotes = [dic objectForKey:@"totalVotes"];
    [self.voteView setVoteModel: voteModel];
}

#pragma  mark - 代理
- (void)btnImageClick:(id)sender{
    
    if ([_delegate respondsToSelector:@selector(imageViewBtn)]) {
        [_delegate imageViewBtn];
    }
}

- (void)setBtnClick:(id)sender{
    
    if ([_delegate respondsToSelector:@selector(setButton)]) {
        [_delegate setButton];
    }
}

//投票
- (void)vote {
    [self.voteView vote];
}

- (void)voteRusult:(NSString * _Nullable)message dic:(NSDictionary * _Nullable)dic
{
    if ([_delegate respondsToSelector:@selector(vote:dic:)]) {
        [_delegate vote:message dic:dic];
    }
}

//余额不足
- (void)recharge:(UIViewController * _Nonnull)vc
{
    if ([_delegate respondsToSelector:@selector(recharge:)]) {
        [_delegate recharge:vc];
    }
}

- (void)setViewBtn:(id)sender{
    /*直播中
    if ([_delegate respondsToSelector:@selector(jumpLiveRoom)]) {
        [_delegate jumpLiveRoom];
    }
     */
}

@end
