//
//  tableViewHeadView.m
//  VLiveShow
//
//  Created by SXW on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "TableViewHeadView.h"

@implementation TableViewHeadView


-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView{

    [self addSubview:self.headLabel1];
    [self addSubview:self.headLabel2];
    [self addSubview:self.headLabel3];
    [self addSubview:self.imageView];
    [self addSubview:self.btn];
}

-(UILabel *)headLabel1{

    if (!_headLabel1) {
        _headLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.frame.size.height / 4)];
        _headLabel1.textColor = [UIColor blackColor];
        _headLabel1.backgroundColor = RGB16(COLOR_BG_ECECEC);
        _headLabel1.text = [NSString stringWithFormat:@"   %@",LocalizedString(@"MINE_GPSLOCATION")];
        _headLabel1.font = [UIFont systemFontOfSize:15];

    }
    return _headLabel1;
}

-(UIImageView *)imageView{
    
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, self.frame.size.height / 4 + 18, 15, 15)];
        _imageView.image = [UIImage imageNamed:@"location02"];
    }
    return _imageView;
}

-(UILabel *)headLabel2{
    
    if (!_headLabel2) {
        _headLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(30, self.frame.size.height / 4, SCREEN_WIDTH, self.frame.size.height / 4 * 2)];
        _headLabel2.textColor = [UIColor blackColor];
        _headLabel2.font = [UIFont systemFontOfSize:15];
        _headLabel2.backgroundColor = [UIColor whiteColor];
        _headLabel2.text = LocalizedString(@"MINE_BEINGLOCATON");
    }
    return _headLabel2;
}

-(UIButton *)btn{

    if (_btn == nil) {
        _btn = [UIButton buttonWithType:UIButtonTypeCustom];
        _btn.frame = CGRectMake(0, self.frame.size.height / 4, SCREEN_WIDTH, self.frame.size.height / 4 * 2);
    }
    return _btn;
}

-(UILabel *)headLabel3{
    
    if (!_headLabel3) {
        _headLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height * 3 / 4, SCREEN_WIDTH, self.frame.size.height / 4)];
        _headLabel3.textColor = [UIColor blackColor];
        _headLabel3.backgroundColor = RGB16(COLOR_BG_ECECEC);
        _headLabel3.text = [NSString stringWithFormat:@"   %@",LocalizedString(@"MINE_ALL")];
        _headLabel3.font = [UIFont systemFontOfSize:15];

    }
    return _headLabel3;
}

-(UISearchBar *)search{

    if (!_search) {
        //搜索框的位置
        _search = [[UISearchBar alloc] init];
        _search.frame = CGRectMake(0, self.frame.size.height / 6 * 4, SCREEN_WIDTH, self.frame.size.height / 6 * 2);
    }
    return _search;
}



@end
