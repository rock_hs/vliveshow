//
//  VLSMiddleView.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMiddleView.h"

@implementation VLSMiddleView

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self initView];
    }
    return self;
}

- (void)initView{

    _view1 = [[UIView alloc] init];
    _view2 = [[UIView alloc] init];
    
    _numLable1 = [[UILabel alloc] init];
    _numLable1.textColor = [UIColor blackColor];
    _numLable1.font = [UIFont systemFontOfSize:14];
    _numLable1.textAlignment = NSTextAlignmentCenter;
    _numLable1.text = @"0";
    
    _numLable2 = [[UILabel alloc] init];
    _numLable2.textColor = [UIColor blackColor];
    _numLable2.font = [UIFont systemFontOfSize:14];
    _numLable2.textAlignment = NSTextAlignmentCenter;
    _numLable2.text = @"0";
    
    _lable1 = [[UILabel alloc] init];
    _lable1.textColor = [UIColor colorFromHexRGB:@"9B9B9B"];
    _lable1.font = [UIFont systemFontOfSize:10];
    _lable1.textAlignment = NSTextAlignmentCenter;
    _lable1.text = LocalizedString(@"MINE_FOCUS");
    
    _lable2 = [[UILabel alloc] init];
    _lable2.textColor = [UIColor colorFromHexRGB:@"9B9B9B"];
    _lable2.font = [UIFont systemFontOfSize:10];
    _lable2.textAlignment = NSTextAlignmentCenter;
    _lable2.text = LocalizedString(@"MINE_FANS");
    
    _btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btn1 addTarget:self action:@selector(btnFocusClick) forControlEvents:UIControlEventTouchUpInside];
    _btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btn2 addTarget:self action:@selector(btnFansClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.view1];
    [self addSubview:self.view2];
    [self.view1 addSubview:self.numLable1];
    [self.view2 addSubview:self.numLable2];
    [self.view1 addSubview:self.lable1];
    [self.view2 addSubview:self.lable2];
    [self addSubview:self.btn1];
    [self addSubview:self.btn2];
    
    _view1.sd_layout
    .leftSpaceToView(self, 0)
    .widthIs(SCREEN_WIDTH / 2)
    .heightIs(50);
    
    _view2.sd_layout
    .leftSpaceToView(self.view1, 0)
    .rightSpaceToView(self, 0)
    .heightIs(50);
    
    _btn1.sd_layout
    .leftSpaceToView(self, 0)
    .widthIs(SCREEN_WIDTH / 2)
    .heightIs(50);
    
    _btn2.sd_layout
    .leftSpaceToView(self.view1, 0)
    .rightSpaceToView(self, 0)
    .heightIs(50);
    
    _numLable1.sd_layout
    .topSpaceToView(self.view1, 8)
    .widthIs(SCREEN_WIDTH / 2)
    .heightIs(20);
    
    _lable1.sd_layout
    .topSpaceToView(self.numLable1, 0)
    .widthIs(SCREEN_WIDTH / 2)
    .heightIs(14);
    
    _numLable2.sd_layout
    .topSpaceToView(self.view2, 8)
    .widthIs(SCREEN_WIDTH / 2)
    .heightIs(20);
    
    _lable2.sd_layout
    .topSpaceToView(self.numLable2, 0)
    .widthIs(SCREEN_WIDTH / 2)
    .heightIs(14);
    
}

- (void)btnFocusClick{

    if ([_delegate respondsToSelector:@selector(pushToFocusList)]) {
        [_delegate pushToFocusList];
    }
}

- (void)btnFansClick{
    
    if ([_delegate respondsToSelector:@selector(pushToFansList)]) {
        [_delegate pushToFansList];
    }
}

@end
