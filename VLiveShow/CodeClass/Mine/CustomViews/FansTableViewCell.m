//
//  FansTableViewCell.m
//  
//
//  Created by SXW on 16/6/3.
//
//

#import "FansTableViewCell.h"
#import "VLSLiveHttpManager.h"

@implementation FansTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self.focusBtn addTarget:self action:@selector(fansBtnClickss:) forControlEvents:UIControlEventTouchUpInside];
        [self.headBtn addTarget:self action:@selector(btnPushNext:) forControlEvents:UIControlEventTouchUpInside];
        [self.nameBtn addTarget:self action:@selector(btnPushNext:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.focusBtn setTitle:LocalizedString(@"MINE_FOCUSEACHOTHER") forState:UIControlStateNormal];
        [self.focusBtn setTitle:LocalizedString(@"MINE_TOFOCUSED") forState:UIControlStateSelected];
    }
    return self;
}

-(void)configData:(MineModel *)model{

    self.focusBtn.tag = 1000;
    if ([model.is_both_attend isEqualToString:@"Y"]) {
        self.focusBtn.selected = NO;
    }else{
    
        self.focusBtn.selected = YES;
    }
    //头像
    if (model.avatars.fileName !=nil) {
        [self setHeadImageWithModelFileName:model.avatars.fileName];
    }else
    {
        [self.headerImage sd_setImageWithURL:[NSURL URLWithString:model.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
    }
    
    if ([model.nickName isEqualToString:@"null"] || model.nickName == nil) {
        
        self.nameLabel.text = LocalizedString(@"MINE_NICKNAMENOTSETTED");
        
    }else
    {
        self.nameLabel.attributedText = [self getNickNameAndSexString:model.nickName withModelSexStr:model.gender withLevelNumber:[model.level integerValue]];
    }
    [self getUserIntroWith:model.intro withLevelNumber:[model.level integerValue] withIsLive:model.isLiving];
}
//黑名单信息
- (void)configBlackData:(MineModel*)model
{
    [self.focusBtn setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
    [self.focusBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self.focusBtn setBackgroundImage:nil forState:UIControlStateSelected];
    self.focusBtn.layer.borderColor = [UIColor redColor].CGColor;
    self.focusBtn.layer.borderWidth = 1;
    self.focusBtn.frame = CGRectMake(SCREEN_WIDTH - 95, 21, 80,28);
    
    [self.focusBtn setBackgroundImage:nil forState:UIControlStateNormal];
    self.focusBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.focusBtn setTitle:LocalizedString(@"MINE_CANCLEBLACK") forState:UIControlStateNormal];
    [self.focusBtn setTitle:LocalizedString(@"MINE_LOOKPROFILE") forState:UIControlStateSelected];
    self.focusBtn.tag = 2000;
    self.focusBtn.selected = model.isblack;

    [self setHeadImageWithModelFileName:model.avatars.fileName];
    
    if ([model.nickName isEqualToString:@"null"] || model.nickName == nil) {
        
        self.nameLabel.text = LocalizedString(@"MINE_NICKNAMENOTSETTED");;
        
    }else
    {
        self.nameLabel.attributedText = [self getNickNameAndSexString:model.nickName withModelSexStr:model.gender withLevelNumber:0];
    }
    [self getUserIntroWith:model.intro withLevelNumber:0 withIsLive:model.isLiving];
}

- (void)fansBtnClickss:(UIButton*)sender{
    
    if (sender.tag == 1000) {
        if ([_delegate respondsToSelector:@selector(fansBtnViewBtn:)]) {
            [_delegate fansBtnViewBtn:self];
        }
    }else
    {
        if ([_delegate respondsToSelector:@selector(blackBtnViewBtn:)]) {
            [_delegate blackBtnViewBtn:self];
        }
    }
    
}

- (void)btnPushNext:(id)sender{
    
    if ([_delegate respondsToSelector:@selector(pushNextFNVC:)]) {
        [_delegate pushNextFNVC:self];
    }
}

- (void)changeButtonInformation
{
    self.nameBtn.userInteractionEnabled = NO;
    self.headBtn.userInteractionEnabled = NO;
    [self.focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flow"] forState:UIControlStateSelected];
    [self.focusBtn setBackgroundImage:[UIImage imageNamed:@"but_flow"] forState:UIControlStateNormal];
    self.focusBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.focusBtn setTitle:LocalizedString(@"MINE_CHECK") forState:UIControlStateNormal];
    [self.focusBtn setTitle:LocalizedString(@"MINE_CHECK") forState:UIControlStateSelected];
    [self.focusBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    [self.focusBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    self.focusBtn.userInteractionEnabled = NO;
}

@end
