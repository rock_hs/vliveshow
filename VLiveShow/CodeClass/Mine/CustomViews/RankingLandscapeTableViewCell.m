//
//  RankingLandscapeTableViewCell.m
//  VLiveShow
//
//  Created by tom.zhu on 2016/12/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "RankingLandscapeTableViewCell.h"
#import "RankingModel.h"

@interface RankingLandscapeTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rankingImage;
@property (weak, nonatomic) IBOutlet UILabel *rankingLabel;
@end

@implementation RankingLandscapeTableViewCell
- (void)setModel:(RankingModel *)model indexPath:(NSIndexPath *)indexPath {
    self.rankingImage.image = indexPath.row ? nil : [UIImage imageNamed:@"NO1"];
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/v1/users/%ld/avatars.png",BASE_URL,model.userId]] placeholderImage:ICON_PLACEHOLDER];
    self.nameLabel.text = model.userName;
    self.numLabel.text = [model.vNumberStr stringByAppendingString:LocalizedString(@"ticket")];
    self.rankingLabel.text = [@(indexPath.row + 1) stringValue];
    
    UIColor *rankingColor = RGB16(COLOR_FONT_DBDBDB);
    if (indexPath.row == 0) {
        rankingColor = [UIColor whiteColor];
    }else if (indexPath.row == 1) {
        rankingColor = RGB16(COLOR_FONT_FF784A);
    }else if (indexPath.row == 2) {
        rankingColor = RGB16(COLOR_FONT_FF1130);
    }
    self.rankingLabel.textColor = rankingColor;
}

@end
