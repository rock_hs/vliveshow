//
//  ContactUsCell.h
//  VLiveShow
//
//  Created by sp on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsCell : UITableViewCell
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *emailLabel;

@end
