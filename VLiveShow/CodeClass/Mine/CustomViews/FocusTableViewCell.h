//
//  FocusTableViewCell.h
//  VLiveShow
//
//  Created by SXW on 16/6/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMineBaseViewCell.h"
#import "MineModel.h"

@class FocusTableViewCell;

@protocol FocusTableViewCellDelegate <NSObject>

- (void)focusBtnViewBtn:(FocusTableViewCell *)cell;
- (void)pushNextVC:(FocusTableViewCell *)cell;

@end

@interface FocusTableViewCell : VLSMineBaseViewCell

@property (nonatomic, weak) id<FocusTableViewCellDelegate>delegate;
@property (nonatomic, strong) MineModel *model;


@end






