//
//  RankingLandscapeTableViewCell.h
//  VLiveShow
//
//  Created by tom.zhu on 2016/12/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RankingModel;

static NSString *RankingLandscapeTableViewCellIdentifier = @"RankingLandscapeTableViewCellIdentifier";

@interface RankingLandscapeTableViewCell : UITableViewCell
- (void)setModel:(RankingModel *)model indexPath:(NSIndexPath *)indexPath;
@end
