//
//  LocationCell.m
//  VLiveShow
//
//  Created by SXW on 16/6/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "LocationCell.h"

@implementation LocationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview:self.resultLabel];
    }
    return self;
}

-(UILabel *)resultLabel{

    if (!_resultLabel) {
        _resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, 100, 20)];
        _resultLabel.textColor = [UIColor grayColor];
    }
    return _resultLabel;
}

@end
