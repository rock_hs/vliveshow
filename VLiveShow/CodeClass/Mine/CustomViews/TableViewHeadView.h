//
//  tableViewHeadView.h
//  VLiveShow
//
//  Created by SXW on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TableViewHeadViewDelegate <NSObject>

@end

@interface TableViewHeadView : UIView{
    
    UIView *headViewView;
}

@property (nonatomic, strong) UISearchBar *search;
@property (nonatomic, strong) UILabel *headLabel1;
@property (nonatomic, strong) UILabel *headLabel2;
@property (nonatomic, strong) UILabel *headLabel3;
@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) UIButton *btn;

@property (nonatomic, weak) id<TableViewHeadViewDelegate>delegate;

@end
