//
//  VLSSonTableCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSSonTableCell.h"

@implementation VLSSonTableCell

// 自绘分割线
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, rect);
    
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_F0F0F0).CGColor);
    CGContextStrokeRect(context, CGRectMake(0, rect.size.height, rect.size.width, 1));
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.focusBtn.frame = CGRectMake(SCREEN_WIDTH - 68, 16, 56,28);
        self.headerImage.frame = CGRectMake(15, 9, 42, 42);
        self.headerImage.layer.cornerRadius = 21;
        
        self.nameLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.contentLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 6, 0, 20);
        self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - 2, 0, 17);
        self.nickImageView.frame = CGRectMake(self.videoView.frame.origin.x - 35, 22, 26, 14);
        self.videoView.frame = CGRectMake(SCREEN_WIDTH - 91 - 46, 23, 46, 14);
    }
    return self;
}


//个性签名、昵称、直播中
- (void)getUserIntroWith:(NSString*)modelIntro withLevelNumber:(NSInteger)levleNumber withIsLive:(NSString*)isLive{
    if ([isLive isEqualToString:@"Y"]) {
        self.videoView.hidden = NO;
        if (modelIntro == nil || [modelIntro isEqualToString:@" "]) {
            
            self.contentLabel.hidden = YES;
            self.nameBtn.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 20, [self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20], 20);
            if (([self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20] + 36 + 67) > self.videoView.frame.origin.x) {
                if (levleNumber <= 0) {
                    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 20, self.videoView.frame.origin.x - 67 - 10, 20);
                }else{
                
                    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 20, self.videoView.frame.origin.x - 67 - 32, 20);
                }
                self.nickImageView.hidden = NO;//显示等级需要修改部分NO
                [self setImageWithNumber:levleNumber withImageView:self.nickImageView];
            }else{
                
                self.nickImageView.hidden = YES;
                self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 20, [self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20] + 36, 20);
            }
        }else{
            
            self.contentLabel.hidden = NO;
            self.nameBtn.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 14, [self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20], 20);
            if (([self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20] + 36) + 67 > self.videoView.frame.origin.x) {
                self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 14, self.videoView.frame.origin.x - 67 - 32, 20);
                self.nickImageView.hidden = NO;//显示等级需要修改部分NO
                self.nickImageView.frame = CGRectMake(self.videoView.frame.origin.x - 35, 17, 26, 14);
                [self setImageWithNumber:levleNumber withImageView:self.nickImageView];
            }else{
                
                self.nickImageView.hidden = YES;
                self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 11, [self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20] + 36, 20);
            }
            
            self.contentLabel.text = modelIntro;
            if ([self getWidthOfNameLabel:modelIntro withFont:12 withSize:17] + 67 > self.videoView.frame.origin.x) {
                self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - 2, self.videoView.frame.origin.x - 67 - 18, 17);
            }else{
                self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - 2, [self getWidthOfNameLabel:self.contentLabel.text withFont:12 withSize:17], 17);
            }
        }
    }else{
        
        self.videoView.hidden = YES;
        if (modelIntro == nil || [modelIntro isEqualToString:@" "]) {
            
            self.contentLabel.hidden = YES;
            self.nameBtn.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 20, [self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20], 20);
            if (([self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20] + 100) > self.focusBtn.frame.origin.x) {
                self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 20, self.focusBtn.frame.origin.x - 67 - 34, 20);
                self.nickImageView.hidden = NO;//显示等级需要修改部分NO
                self.nickImageView.frame = CGRectMake(self.focusBtn.frame.origin.x - 35, 22, 26, 14);
                [self setImageWithNumber:levleNumber withImageView:self.nickImageView];
            }else{
                
                self.nickImageView.hidden = YES;
                self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 20, [self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20] + 36, 20);
            }
        }else{
            
            self.contentLabel.hidden = NO;
            self.nameBtn.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 14, [self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20], 20);
            if (([self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20]) + 100 > self.focusBtn.frame.origin.x) {
                self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 11, self.focusBtn.frame.origin.x - 67 - 34, 20);
                self.nickImageView.hidden = NO;//显示等级需要修改部分NO
                self.nickImageView.frame = CGRectMake(self.focusBtn.frame.origin.x - 35, 17, 26, 14);
                [self setImageWithNumber:levleNumber withImageView:self.nickImageView];
            }else{
                
                self.nickImageView.hidden = YES;
                self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, 11, [self getWidthOfNameLabel:self.nameLabel.text withFont:14 withSize:20] + 36, 20);
            }
            
            self.contentLabel.text = modelIntro;
            if ([self getWidthOfNameLabel:modelIntro withFont:12 withSize:17] + 67 > self.focusBtn.frame.origin.x) {
                self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - 2, self.focusBtn.frame.origin.x - 67 - 34, 20);
            }else{
                self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - 2, [self getWidthOfNameLabel:self.contentLabel.text withFont:12 withSize:17], 20);
            }
        }
    }
}


@end
