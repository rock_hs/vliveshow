//
//  VLSNotFocusInvitationCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSNotFocusInvitationCell.h"
#import "VLSLiveHttpManager.h"
@implementation VLSNotFocusInvitationCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self.contentView addSubview:self.switchInvite];
        [self.switchInvite addTarget:self action:@selector(inviteMsg:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (UISwitch*)switchInvite
{
    if (_switchInvite == nil) {
        
        _switchInvite = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-60, 13, 100,20)];
        _switchInvite.onTintColor = [UIColor redColor];
        [_switchInvite addTarget:self action:@selector(inviteMsg:) forControlEvents:UIControlEventValueChanged];
    }
    return _switchInvite;
    
}

- (void)inviteMsg:(UISwitch*)sender{

    if (sender.on == YES) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isInvite"];
        ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
        callback.updateBlock = ^(id result)
        {
            
        };
        callback.errorBlock = ^(id result)
        {
        };
        
        [VLSLiveHttpManager postLiveInvite:@"true" callback:callback];
    }else
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isInvite"];
        ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
        callback.updateBlock = ^(id result)
        {
            
        };
        callback.errorBlock = ^(id result)
        {
        };
        
        [VLSLiveHttpManager postLiveInvite:@"false" callback:callback];
    }

}
@end
