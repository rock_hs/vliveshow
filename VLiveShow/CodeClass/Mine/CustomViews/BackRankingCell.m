//
//  BackRankingCell.m
//  VLiveShow
//
//  Created by sp on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "BackRankingCell.h"

@implementation BackRankingCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self loadAllView];
    }
    return self;
}
- (void)loadAllView
{
    [self addSubview:self.rankdingLabel];
    [self addSubview:self.headImageView];
    [self addSubview:self.nameLabel];
    [self addSubview:self.contentLabel];
    [self addSubview:self.separateLine];
    [self addSubview:self.levelView];
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIImageView*)headImageView
{
    if (_headImageView == nil) {
        _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(80, 10, 50, 50)];
        _headImageView.layer.cornerRadius = 25;
        _headImageView.clipsToBounds = YES;
        _headImageView.image = [UIImage imageNamed:@"swim"];
    }
    return _headImageView;
}
- (UILabel*)rankdingLabel
{
    if (_rankdingLabel == nil) {
        _rankdingLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, 45, 70)];
        _rankdingLabel.font = [UIFont systemFontOfSize:14];
        _rankdingLabel.textColor = [UIColor colorFromHexRGB:@"C3A851"];
        
    }
    return _rankdingLabel;
}
- (UILabel*)nameLabel
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(145, 10, SCREEN_WIDTH -160, 20)];
        _nameLabel.textColor = [UIColor colorFromHexRGB:@"4A4A4A"];
        _nameLabel.font = [UIFont systemFontOfSize:16];
    }
    return _nameLabel;
}
-(UILabel*)contentLabel
{
    if (_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(145, 35, SCREEN_WIDTH-160, 20)];
        _contentLabel.font = [UIFont systemFontOfSize:14];
        _contentLabel.textColor = [UIColor colorFromHexRGB:@"979797"];
    }
    return _contentLabel;
}
- (UIView*)separateLine
{
    if (_separateLine == nil) {
        _separateLine = [[UIView alloc] initWithFrame:CGRectMake(10, 70, SCREEN_WIDTH - 20, 1)];
        _separateLine.backgroundColor = [UIColor colorFromHexRGB:@"F4F4F4"];
    }
    return _separateLine;
}
- (UILabel*)levelabel
{
    if (_levelabel == nil) {
        _levelabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 15, 15)];
        _levelabel.textAlignment = NSTextAlignmentCenter;
        _levelabel.font = [UIFont systemFontOfSize:10];
        _levelabel.textColor = [UIColor whiteColor];
    }
    return _levelabel;
}
- (UIView*)levelView
{
    if (_levelView == nil) {
        _levelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [_levelView addSubview:self.levelImageView];
        [_levelView addSubview: self.levelabel];
    }
    return _levelView;
}
- (UIImageView*)levelImageView
{
    if (_levelImageView == nil) {
        _levelImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 16)];
    }
    return _levelImageView;
}

- (void)setRankingModel:(RankingModel *)rankingModel
{
     [self.headImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/v1/users/%ld/avatars.png",BASE_URL,rankingModel.userId]] placeholderImage:ICON_PLACEHOLDER];
    
    self.rankdingLabel.text = [NSString stringWithFormat:@"NO.%ld",rankingModel.index];
    self.contentLabel.text = rankingModel.userContent;
    self.nameLabel.text = rankingModel.userName;
    CGSize marksize = [rankingModel.userName sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16],NSFontAttributeName, nil]];
    CGFloat marklength = marksize.width;
    
    if (rankingModel.userLevel >=0)
    {
        if (marklength +30+5+15>=SCREEN_WIDTH-145)
        {
            if (rankingModel.userContent == nil||[rankingModel.userContent isEqualToString:@" "])
         {
                self.nameLabel.frame = CGRectMake(145, 25, SCREEN_WIDTH -145-5-30-15, 20);
                self.contentLabel.frame = CGRectMake(0, 0, 0, 0);
                self.levelView.frame = CGRectMake(SCREEN_WIDTH -45, 27, 30, 16);
            }else
            {
                self.nameLabel.frame = CGRectMake(145, 10, SCREEN_WIDTH -145-5-30-15, 20);
                self.levelView.frame = CGRectMake(SCREEN_WIDTH -45, 12, 30, 16);

            }

        }else
        {
            if (rankingModel.userContent == nil) {
                self.nameLabel.frame = CGRectMake(145, 25, marklength, 20);
                self.contentLabel.frame = CGRectMake(0, 0, 0, 0);
                self.levelView.frame = CGRectMake(145+marklength+5, 27, 30, 16);
            }else
            {
                self.nameLabel.frame = CGRectMake(145, 10, marklength, 20);
                self.levelView.frame = CGRectMake(145+marklength+5, 12, 30, 16);
            }
        }
//        self.levelImageView.image = [UIImage imageNamed:@"lv_40-49"];
        if (rankingModel.userLevel) {
            self.levelabel.text = [NSString stringWithFormat:@"%ld",rankingModel.userLevel];
        }
    }else
    {
        if (rankingModel.userContent == nil)
        {
            self.nameLabel.frame = CGRectMake(145, 25, SCREEN_WIDTH -145-5-30-15, 20);
            self.contentLabel.frame = CGRectMake(0, 0, 0, 0);
        }
    
    }
    
}


@end
