//
//  VLSLineTableViewCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLineTableViewCell.h"

@implementation VLSLineTableViewCell


// 自绘分割线
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, rect);
    
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_ECECEC).CGColor);
    CGContextStrokeRect(context, CGRectMake(5, rect.size.height - 1, rect.size.width - 10, 1));
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.label];
    }
    return self;
}

-(UILabel *)label{

    if (_label == nil) {
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 115, 15, 100, 20)];
        _label.font = [UIFont systemFontOfSize:14];
        _label.textColor = RGB16(COLOR_FONT_9B9B9B);
    }
    return _label;
}

@end
