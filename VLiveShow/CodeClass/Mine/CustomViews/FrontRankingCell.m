//
//  FrontRankingCell.m
//  VLiveShow
//
//  Created by sp on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "FrontRankingCell.h"

@implementation FrontRankingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self loadAllView];
    }
    return self;
}
- (void)loadAllView
{
    [self addSubview:self.rankingLabel];
    [self addSubview:self.headerImageView];
    [self addSubview:self.nameLabel];
    [self addSubview:self.separateLine];
    [self addSubview:self.levelView];
}
- (UILabel*)levelabel
{
    if (_levelabel == nil) {
        _levelabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 15, 15)];
        _levelabel.textAlignment = NSTextAlignmentCenter;
        _levelabel.font = [UIFont systemFontOfSize:10];
        _levelabel.textColor = [UIColor whiteColor];
    }
    return _levelabel;
}
- (UIView*)levelView
{
    if (_levelView == nil) {
        _levelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [_levelView addSubview:self.levelImageView];
        [_levelView addSubview: self.levelabel];
    }
    return _levelView;
}
- (UIImageView*)levelImageView
{
    if (_levelImageView == nil) {
        _levelImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 16)];
    }
    return _levelImageView;
}
- (UILabel*)rankingLabel
{
    if (_rankingLabel == nil) {
        _rankingLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, 40, 103)];
        _rankingLabel.font = [UIFont systemFontOfSize:55];
        _rankingLabel.textAlignment = NSTextAlignmentLeft;
        
    }
    return _rankingLabel;
}
- (UIImageView*)headerImageView
{
    if (_headerImageView == nil) {
        _headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-25, 15, 50, 50)];
        _headerImageView.layer.cornerRadius = 25;
        _headerImageView.clipsToBounds = YES;
        
    }
    return _headerImageView;
}
- (UILabel*)nameLabel
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, 70, SCREEN_WIDTH - 130, 20)];
        _nameLabel.font = [UIFont systemFontOfSize:15];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.textColor = [UIColor colorFromHexRGB:@"4A4A4A"];
    }
    return _nameLabel;
}
- (UIView*)separateLine
{
    if (_separateLine == nil) {
        _separateLine = [[UIView alloc] initWithFrame:CGRectMake(30, 102, SCREEN_WIDTH - 60, 2)];
        _separateLine.backgroundColor = [UIColor colorFromHexRGB:@"F4F4F4"];
    }
    return _separateLine;
}

- (void)setRankModel:(RankingModel *)rankModel
{
    self.rankingLabel.text = [NSString stringWithFormat:@"%ld",rankModel.index];
    self.nameLabel.text = rankModel.userName;
    
     [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/v1/users/%ld/avatars.png",BASE_URL,rankModel.userId]] placeholderImage:ICON_PLACEHOLDER];
    if (rankModel.index  == 1) {
        self.rankingLabel.textColor = [UIColor colorFromHexRGB:@"F8E71C"];
    }else if (rankModel.index  == 2)
    {
        self.rankingLabel.textColor = [UIColor colorFromHexRGB:@"FF784A"];

    }else if (rankModel.index == 3)
    {
        self.rankingLabel.textColor = [UIColor colorFromHexRGB:@"FF1130"];
        
    }
    CGSize marksize = [rankModel.userName sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:15],NSFontAttributeName, nil]];
      CGFloat marklength = marksize.width;
    if (rankModel.userLevel >=0) {
        if (marklength +30+5>=SCREEN_WIDTH-130) {
            self.nameLabel.frame = CGRectMake(65, 70, SCREEN_WIDTH - 30-130-5, 20);
            self.levelView.frame = CGRectMake(SCREEN_WIDTH - 30-65, 72, 30, 16);
        }else
        {
//            self.nameLabel.frame = CGRectMake((SCREEN_WIDTH- 130 - 30 - marklength-5)/2+65, 70, marklength, 20);
            self.nameLabel.frame = CGRectMake((SCREEN_WIDTH- 130 - marklength-5)/2+65, 70, marklength, 20);
            self.levelView.frame = CGRectMake((SCREEN_WIDTH- 130 - 30 - marklength-5)/2+65+marklength+5, 72, 30, 16);
            
        }
        
//        self.levelImageView.image = [UIImage imageNamed:@"lv_40-49"];
        if (rankModel.userLevel) {
            self.levelabel.text = [NSString stringWithFormat:@"%ld",rankModel.userLevel];

        }

    }
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
