//
//  VLSMySettingCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMySettingCell.h"

@implementation VLSMySettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self initView];
    }
    return self;
}

- (void)initView{

    [self.contentView addSubview:self.titleLable];
    [self.contentView addSubview:self.detailTitleLabel];
    [self.contentView addSubview:self.inditorImageView];
}

-(UILabel *)titleLable{

    if (_titleLable == nil) {
        
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(24, 17, 180, 22)];
        _titleLable.font = [UIFont systemFontOfSize:SIZE_FONT_16];
        _titleLable.textColor = [UIColor blackColor];
    }
    return _titleLable;
}

-(UIImageView *)inditorImageView{

    if (_inditorImageView == nil) {
        
        _inditorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 40, 18, 20, 20)];
        _inditorImageView.image = [UIImage imageNamed:@"ic_into_gray"];
    }
    return _inditorImageView;
}

-(UILabel *)detailTitleLabel{

    if (_detailTitleLabel == nil) {
        
        _detailTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 189, 17, 130, 22)];
        _detailTitleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_16];
        _detailTitleLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        _detailTitleLabel.textAlignment = NSTextAlignmentRight;
    }
    return _detailTitleLabel;
}









@end
