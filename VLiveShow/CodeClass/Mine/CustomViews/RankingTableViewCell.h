//
//  RankingTableViewCell.h
//  VLiveShow
//
//  Created by tom.zhu on 2016/11/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RankingModel;

static NSString *RankingTableViewCellCellIdentity = @"RankingTableViewCellCellIdentity";

@interface RankingTableViewCell : UITableViewCell
- (void)setModel:(RankingModel *)model indexPath:(NSIndexPath *)indexPath;
@end
