//
//  VLSNotFocusInvitationCell.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "RemindTableViewCell.h"

@interface VLSNotFocusInvitationCell : RemindTableViewCell

@property (nonatomic,strong)UISwitch *switchInvite;

@end
