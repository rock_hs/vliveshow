//
//  VLSSegmentView.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSSegmentView.h"
@interface VLSSegmentView()

@end
@implementation VLSSegmentView


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setUp];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [self setUp];
}

- (void)setUp
{
    //背景色和阴影
//    self.backgroundColor = [UIColor whiteColor];
//    self.layer.shadowOpacity = 0.1;
//    self.layer.shadowOffset = CGSizeMake(0, 2);
    
    self.view1 = [[UIView alloc] init];
    self.view2 = [[UIView alloc] init];
    [self addSubview:self.view1];
    [self addSubview:self.view2];
    
    self.firstButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.firstButton.tag = 0;
    self.focNumLab = [[UILabel alloc] init];
    self.focNumLab.textAlignment = NSTextAlignmentCenter;
    self.focNumLab.font = [UIFont systemFontOfSize:14];
    self.focNumLab.textColor= [UIColor blackColor];
    self.focNumLab.text = @"0";
    
    self.focLabel = [[UILabel alloc] init];
    self.focLabel.textAlignment = NSTextAlignmentCenter;
    self.focLabel.font = [UIFont systemFontOfSize:10];
    self.focLabel.textColor= RGB16(COLOR_BG_9B9B9B);
    self.focLabel.text = LocalizedString(@"MINE_FOCUS");
    self.label1 = [[UILabel alloc] init];
    self.label1.backgroundColor = RGB16(COLOR_BG_EEEEEE);
    
    
    [self.view1 addSubview:self.focNumLab];
    [self.view1 addSubview:self.focLabel];
    [self.view1 addSubview:self.firstButton];
    [self.view1 addSubview:self.label1];
    
    self.secondButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.secondButton.tag = 1;
    
    self.fansNumLab = [[UILabel alloc] init];
    self.fansNumLab.textAlignment = NSTextAlignmentCenter;
    self.fansNumLab.font = [UIFont systemFontOfSize:14];
    self.fansNumLab.textColor = [UIColor blackColor];
    self.fansNumLab.text = @"0";
    
    self.fansLabel = [[UILabel alloc] init];
    self.fansLabel.textAlignment = NSTextAlignmentCenter;
    self.fansLabel.font = [UIFont systemFontOfSize:10];
    self.fansLabel.textColor = RGB16(COLOR_BG_9B9B9B);
    self.fansLabel.text = LocalizedString(@"MINE_FANS");
    self.label2 = [[UILabel alloc] init];
    self.label2.backgroundColor = RGB16(COLOR_BG_EEEEEE);
    
    [self.view2 addSubview:self.fansNumLab];
    [self.view2 addSubview:self.fansLabel];
    [self.view2 addSubview:self.secondButton];
    [self.view2 addSubview:self.label2];

    self.slideLineView = [[UIImageView alloc] init];
    self.slideLineView.backgroundColor = RGB16(COLOR_FONT_FF1130);
    [self addSubview:self.slideLineView];
    
    [self.firstButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchDown];
    [self.secondButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchDown];
    [self lineToIndex:0];
}


//设置控件的frame
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //这里需要判断下是否显示commentBtn,抓不到数据，暂时先不显示，如果需要显示就给也设置frame
    CGFloat viewH = self.bounds.size.height;
    CGFloat viewW = self.bounds.size.width;
    CGFloat btnW = viewW * 0.5;
    CGFloat btnH = viewH;
    self.view1.frame = CGRectMake(0, 0, btnW, btnH-2);
    self.view2.frame = CGRectMake(btnW , 0, btnW, btnH-2);
    self.firstButton.frame = self.view1.frame;
    self.focNumLab.frame = CGRectMake(0, 5, self.view1.frame.size.width, self.view1.frame.size.height / 5 * 2);
    self.focLabel.frame = CGRectMake(0, self.view1.frame.size.height / 5 * 2, self.view1.frame.size.width, self.view1.frame.size.height / 5 * 3);
    self.label1.frame = CGRectMake(0, self.view1.frame.size.height, self.view1.frame.size.width, 2);
    
    self.secondButton.frame = self.view1.frame;
    self.fansNumLab.frame = CGRectMake(0, 5, self.view2.frame.size.width, self.view2.frame.size.height / 5 * 2);
    self.fansLabel.frame = CGRectMake(0, self.view2.frame.size.height / 5 * 2, self.view2.frame.size.width, self.view2.frame.size.height / 5 * 3);
    self.label2.frame = CGRectMake(0, self.view2.frame.size.height, self.view2.frame.size.width, 2);

    self.slideLineView.frame = CGRectMake(viewW / 6, viewH - 2, viewW / 6, 2);
}

#pragma mark - 按钮的Action
- (void)btnClick:(UIButton *)sender
{
    if (self.nowSelectedBtn == sender) return;
    //通知代理点击
    if ([self.delegate respondsToSelector:@selector(selectView:didSelectedButtonFrom:to:)]) {
        [self.delegate selectView:self didSelectedButtonFrom:self.nowSelectedBtn.tag to:sender.tag];
    }
    [self lineToIndex:sender.tag];
}

//有代理时，点击按钮
- (void)setDelegate:(id<VLSSegmentViewDelegate>)delegate
{
    _delegate = delegate;
    [self btnClick:self.firstButton];
}

- (void)lineToIndex:(NSInteger)index
{
    CGRect rect = self.slideLineView.frame;
    switch (index) {
        case 0:
            if ([self.delegate respondsToSelector:@selector(selectView:didChangeSelectedView:)]) {
                [self.delegate selectView:self didChangeSelectedView:0];
            }
            self.focNumLab.textColor = RGB16(COLOR_FONT_FF1130);
            self.focLabel.textColor = RGB16(COLOR_FONT_FF1130);
            self.fansNumLab.textColor = RGB16(COLOR_BG_000000);
            self.fansLabel.textColor =RGB16(COLOR_BG_9B9B9B);
            self.nowSelectedBtn = self.firstButton;
            rect.origin.x = SCREEN_WIDTH / 6;

            break;
        case 1:
            if ([self.delegate respondsToSelector:@selector(selectView:didChangeSelectedView:)]) {
                [self.delegate selectView:self didChangeSelectedView:1];
            }
            self.fansNumLab.textColor = RGB16(COLOR_FONT_FF1130);
            self.fansLabel.textColor = RGB16(COLOR_FONT_FF1130);
            self.focLabel.textColor = RGB16(COLOR_BG_9B9B9B);
            self.focNumLab.textColor = RGB16(COLOR_BG_000000);
            self.nowSelectedBtn = self.secondButton;
            rect.origin.x = self.nowSelectedBtn.superview.frame.origin.x * 4 / 3 ;

            break;
        default:
            break;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.slideLineView.frame = rect;
    }];
}
@end
