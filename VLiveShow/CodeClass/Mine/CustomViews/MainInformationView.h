//
//  MainInformationView.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/5/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainInformationViewDelegate <NSObject>
@optional
- (void)imageViewBtn;
- (void)setButton;
- (void)jumpLiveRoom;
- (void)vote:(NSString*)message dic:(NSDictionary*)dic ; //投票
- (void)recharge:(UIViewController*)vc;
@end

@interface MainInformationView : UIView

@property (nonatomic, strong) UIImageView *userIcon;
@property (nonatomic, strong) UIButton *setBtn;
@property (nonatomic, strong) UILabel *nameLab;
@property (nonatomic, strong) UILabel *simpleLab;

@property (nonatomic, strong) UIButton *imageBtn;
@property (nonatomic, strong) UILabel *locationLab;
@property (nonatomic, strong) UILabel *numLabel;

@property (nonatomic, strong) UIImageView *sexImg;//性别
@property (nonatomic, strong) UILabel *levelLabel;//等级
@property (nonatomic, strong) UIView *parentView;
@property (nonatomic, strong) UIImageView *levelImageView;

@property (nonatomic, strong) UIImageView *sexImage;
@property (nonatomic, strong) UIImageView *levelImg;

@property (nonatomic, strong) UIButton *setBut;
@property (nonatomic, strong) UIButton *topSetBut;
@property (nonatomic, strong) UILabel *playRoomLab;
@property (nonatomic, assign) id<MainInformationViewDelegate>delegate;

//更新投票数
- (void)updateVoteModel:(NSDictionary*)dic;
- (void)vote; //投票

@end
