//
//  VLSMineBaseViewCell.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSMineBaseViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UIButton *headBtn;

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *nameBtn;

@property (nonatomic, strong) UIImageView *sexImage;
@property (nonatomic, strong) NSTextAttachment *attch;
@property (nonatomic, strong) NSTextAttachment *attchLevel;

@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UIButton *focusBtn;
@property (nonatomic, assign) NSInteger index;

@property (nonatomic, strong) UIView *videoView;
@property (nonatomic, strong) UIImageView *liveImage;
@property (nonatomic, strong) UILabel *liveLabel;

@property (nonatomic, strong) UIView *nameView;
@property (nonatomic, strong) UILabel *nickNameLable;
@property (nonatomic, strong) UIImageView *nickImageView;

- (CGFloat)getWidthOfNameLabel:(NSString*)aString withFont:(CGFloat)fontSize withSize:(CGFloat)fontHeight;
- (NSString*)getHttpString;
- (NSMutableAttributedString*)getNickNameAndSexString:(NSString*)modelNameStr withModelSexStr:(NSString*)modelSexStr withLevelNumber:(NSInteger)levelNumber;
- (void)setHeadImageWithModelFileName:(NSString*)fileName;
- (void)getUserIntroWith:(NSString*)modelIntro withLevelNumber:(NSInteger)levleNumber withIsLive:(NSString*)isLive;
- (void)setImageWithNumber:(NSInteger)number withImageView:(UIImageView*)imageView;
- (void)setBackGroundImageWithNumber:(NSInteger)number withAttchment:(NSTextAttachment*)attchment1 andAttchment:(NSTextAttachment*)attchment2;

-(void)hideWaiting;
-(void)showWaiting:(UIView *)parent;
@end
