//
//  VLSSegmentView.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VLSSegmentView;

@protocol VLSSegmentViewDelegate <NSObject>

@optional
//当按钮点击时通知代理
- (void)selectView:(VLSSegmentView *)selectView didSelectedButtonFrom:(NSInteger)from to:(NSInteger)to;

- (void)selectView:(VLSSegmentView *)selectView didChangeSelectedView:(NSInteger)to;
@end

@interface VLSSegmentView : UIView

/** 用来确定是否显示评论列表的 默认是NO，只显示俩个button, 如果是YES就显示3个button */
@property(nonatomic, assign) BOOL isShowComment;


@property (nonatomic, strong) UIButton *firstButton;
@property (nonatomic, strong) UIButton *secondButton;
@property (nonatomic, strong) UIView *view1;
@property (nonatomic, strong) UIView *view2;

@property (nonatomic, strong) UILabel *focLabel;
@property (nonatomic, strong) UILabel *focNumLab;
@property (nonatomic, strong) UILabel *fansLabel;
@property (nonatomic, strong) UILabel *fansNumLab;

@property (nonatomic, strong) UIButton *nowSelectedBtn;
@property (nonatomic, strong) UIImageView *slideLineView;
@property(nonatomic, weak) id <VLSSegmentViewDelegate> delegate;

@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;

//提供给外部一个可以滑动底部line的方法
- (void)lineToIndex:(NSInteger)index;
@end
