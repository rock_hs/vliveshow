//
//  FocusTableViewCell.m
//  VLiveShow
//
//  Created by SXW on 16/6/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "FocusTableViewCell.h"

@implementation FocusTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self.focusBtn addTarget:self action:@selector(focusBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.headBtn addTarget:self action:@selector(btnPushNext:) forControlEvents:UIControlEventTouchUpInside];
        [self.nameBtn addTarget:self action:@selector(btnPushNext:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.focusBtn setTitle:LocalizedString(@"MINE_FOCUSED") forState:UIControlStateNormal];
        [self.focusBtn setTitle:LocalizedString(@"MINE_TOFOCUSED") forState:UIControlStateSelected];
    }
    return self;
}


-(void)setModel:(MineModel *)model{
    
    self.focusBtn.selected = model.isPress;
    [self setHeadImageWithModelFileName:model.avatars.fileName];
    if ([model.nickName isEqualToString:@"null"] || model.nickName == nil) {
        
        self.nameLabel.text = LocalizedString(@"MINE_NICKNAMENOTSETTED");
        
    }else
    {
        NSMutableAttributedString *str = [self getNickNameAndSexString:model.nickName withModelSexStr:model.gender withLevelNumber:[model.level integerValue]];
        self.nameLabel.attributedText = str;
    }
    [self getUserIntroWith:model.intro withLevelNumber:[model.level integerValue] withIsLive:model.isLiving];
}

- (void)focusBtnClicked:(UIButton*)sender{
    sender.selected = !sender.selected;
    
    if ([_delegate respondsToSelector:@selector(focusBtnViewBtn:)]) {
        [_delegate focusBtnViewBtn:self];
    }
}

- (void)btnPushNext:(id)sender{
    
    if ([_delegate respondsToSelector:@selector(pushNextVC:)]) {
        [_delegate pushNextVC:self];
    }
}

@end
