//
//  FrontRankingCell.h
//  VLiveShow
//
//  Created by sp on 16/8/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankingModel.h"
@interface FrontRankingCell : UITableViewCell
@property (nonatomic,strong)UILabel * rankingLabel;
@property (nonatomic,strong)UIImageView *headerImageView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UIView *separateLine;
@property (nonatomic,strong)RankingModel *rankModel;
@property (nonatomic,strong)UILabel *levelabel;
@property (nonatomic,strong)UIView *levelView;
@property (nonatomic,strong)UIImageView *levelImageView;
@end
