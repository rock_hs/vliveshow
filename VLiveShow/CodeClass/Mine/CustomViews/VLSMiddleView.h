//
//  VLSMiddleView.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VLSMiddleViewDelegate <NSObject>

- (void)pushToFocusList;
- (void)pushToFansList;

@end
@interface VLSMiddleView : UIView

@property (nonatomic, strong) UIView *view1;
@property (nonatomic, strong) UIView *view2;

@property (nonatomic, strong) UILabel *lable1;
@property (nonatomic, strong) UILabel *lable2;
@property (nonatomic, strong) UILabel *numLable1;
@property (nonatomic, strong) UILabel *numLable2;

@property (nonatomic, strong) UIButton *btn1;
@property (nonatomic, strong) UIButton *btn2;

@property (nonatomic, weak) id<VLSMiddleViewDelegate>delegate;

@end
