//
//  ContactUsCell.m
//  VLiveShow
//
//  Created by sp on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "ContactUsCell.h"

@implementation ContactUsCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview:self.nameLabel];
        [self addSubview:self.emailLabel];
    }
    return self;
}
- (UILabel*)nameLabel
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30 * (SCREEN_HEIGHT / 667), SCREEN_WIDTH, 20 * (SCREEN_HEIGHT / 667))];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.font = [UIFont systemFontOfSize:16];
    }
    return _nameLabel;
}
- (UILabel*)emailLabel
{
    if (_emailLabel == nil) {
        _emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_nameLabel.frame) + 1 , SCREEN_WIDTH, 20 * (SCREEN_HEIGHT / 667))];
        _emailLabel.textAlignment = NSTextAlignmentCenter;
        _emailLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        _emailLabel.font = [UIFont systemFontOfSize:14];
    }

    return _emailLabel;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
