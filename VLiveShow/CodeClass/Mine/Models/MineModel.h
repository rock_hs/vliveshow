//
//  MineModel.h
//  VLiveShow
//
//  Created by SXW on 16/5/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSUserProfile.h"
#import "AvatarsModel.h"
@interface MineModel :VLSUserProfile

@property (nonatomic ,strong) AvatarsModel *avatars;//解析头像
@property (nonatomic, copy) NSString *nickName;//解析的用户名
@property (nonatomic, copy) NSString *intro;
@property (nonatomic, strong) NSString *ID;//解析id
@property (nonatomic,assign)BOOL isblack;
@property (nonatomic,copy) NSString * is_both_attend;
@property (nonatomic) BOOL isPress;
@property (nonatomic, copy) NSString *isLiving;
@property (nonatomic, copy) NSString *level;

@end
