//
//  RankingModel.h
//  VLiveShow
//
//  Created by sp on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RankingModel : NSObject
@property (nonatomic, assign) long userLevel;//等级
@property (nonatomic, assign) long userId;//用户id
@property (nonatomic, copy) NSString *userName;//用户昵称
@property (nonatomic, copy) NSString *userContent;//用户个性签名
@property (nonatomic,assign)long vNumber;//V票数
@property (nonatomic, strong) NSString *vNumberStr;
@property (nonatomic,assign)long index;//排名

@end
