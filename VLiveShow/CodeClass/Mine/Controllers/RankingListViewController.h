//
//  RankingListViewController.h
//  VLiveShow
//
//  Created by sp on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FrontRankingCell.h"
#import "BackRankingCell.h"
#import "NavView.h"
#import "VLSLiveHttpManager.h"
#import "VLSBaseViewController.h"
#import "VLSOthersViewController.h"
@interface RankingListViewController : VLSBaseViewController<UITableViewDataSource,UITableViewDelegate,NavViewDelegate>
@property (nonatomic,copy)NSString *userId;
@property (nonatomic, assign) KLiveRoomOrientation orientation;
@end
