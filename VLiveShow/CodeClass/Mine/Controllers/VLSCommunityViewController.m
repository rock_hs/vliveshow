//
//  VLSCommunityViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSCommunityViewController.h"

@interface VLSCommunityViewController ()

@end

@implementation VLSCommunityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = LocalizedString(@"MINE_COMMUNITYAPPOINT");

    [self loadRequestWebViewWithString:@"VCommunity.html"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
