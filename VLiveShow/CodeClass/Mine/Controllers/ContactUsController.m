//
//  ContactUsController.m
//  VLiveShow
//
//  Created by sp on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "ContactUsController.h"
#import "ContactUsCell.h"
#define topHight 200
@interface ContactUsController ()
{
    NSArray *nameArray;
    NSArray *emailArray;
}
@end

@implementation ContactUsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    /*
     媒体合作：vls_media@vipabc.com
     市场合作：vls_marketing@vipabc.com
     联系电话：021-61713030 ext.19527/
     客服邮箱：vsupport@vipabc.com
     */
    self.title = LocalizedString(@"MINE_CONTACTUS");
    [self.view addSubview:self.logoImage];
    [self.view addSubview:self.baseTable];
    self.logoImage.sd_layout
    .topSpaceToView(self.view, 73 * (SCREEN_HEIGHT / 667))
    .centerXEqualToView(self.view)
    .widthIs(64 * (SCREEN_WIDTH / 375))
    .heightIs(80 * (SCREEN_HEIGHT / 667));
    
    self.baseTable.sd_layout
    .bottomSpaceToView(self.view, 84 * (SCREEN_HEIGHT / 667))
    .widthIs(SCREEN_WIDTH)
    .heightIs(80 * 4 * (SCREEN_HEIGHT / 667));
    
    nameArray = [NSArray arrayWithObjects:LocalizedString(@"MINE_MEDIACOOPERATION"),LocalizedString(@"MINE_MARKETCOOPERATION"),LocalizedString(@"MINE_CONTACTTELEPHONE"), LocalizedString(@"SEVRVICE_MAILBOX"),nil];
    emailArray = [NSArray arrayWithObjects:@"vls_media@vipabc.com",@"vls_marketing@vipabc.com",@"021-61713030 ext.19527", @"vsupport@vipabc.com",nil];

}
#pragma mark - tabledelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return nameArray.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 80 * (SCREEN_HEIGHT / 667);
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactUsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactcell" forIndexPath:indexPath];
    cell.emailLabel.text = [emailArray objectAtIndex:indexPath.row];
    cell.nameLabel.text = [nameArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (UIImageView*)logoImage
{
    if (_logoImage == nil) {
        _logoImage = [[UIImageView alloc] init];
        _logoImage.image = [UIImage imageNamed:@"logo2.png"];
        _logoImage.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _logoImage;
}
- (UITableView*)baseTable
{
    if (_baseTable == nil) {
//        _baseTable = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_logoImage.frame) + 10 * (667 / SCREEN_HEIGHT), SCREEN_WIDTH, SCREEN_HEIGHT - 110 - 64) style:UITableViewStylePlain];
        _baseTable = [[UITableView alloc] init];
        _baseTable.delegate = self;
        _baseTable.dataSource = self;
        [_baseTable registerClass:[ContactUsCell class] forCellReuseIdentifier:@"contactcell"];
        _baseTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _baseTable.scrollEnabled = NO;
    }
    return _baseTable;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
