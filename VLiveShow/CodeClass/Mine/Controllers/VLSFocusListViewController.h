//
//  VLSFocusListViewController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSRefreshViewController.h"
#import "FocusTableViewCell.h"
#import "VLSLiveHttpManager.h"
#import "VLSOthersViewController.h"
@interface VLSFocusListViewController : VLSRefreshViewController<FocusTableViewCellDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *focNumArray;
@property (nonatomic) NSInteger page;
@property (nonatomic, strong) UILabel *focusLabel;
@property (nonatomic, assign) NSInteger userId;

@end
