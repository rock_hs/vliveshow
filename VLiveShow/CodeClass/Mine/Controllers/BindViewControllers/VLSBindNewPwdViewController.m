//
//  VLSBindNewPwdViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBindNewPwdViewController.h"
#import "VLSPwdInputView.h"
#import "VLSNewPhoneVerifyViewController.h"

@interface VLSBindNewPwdViewController ()<VLSPwdInputViewDelegate>
@property (nonatomic, strong)VLSPwdInputView *pwdView;
@end

@implementation VLSBindNewPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = LocalizedString(@"LOGIN_SET_NEW_PWD");
    [self.nextButton setTitle:LocalizedString(@"BIND_SURE_COMMIT") forState:UIControlStateNormal];
    self.pwdView.placeholder = LocalizedString(@"LOGIN_PAGE_PASSWORD");
    
    [self doInit];
    
}

- (void)doInit {
    [self.view addSubview:self.pwdView];
    [self.view addSubview:self.nextButton];
    
    self.pwdView.sd_layout
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .topSpaceToView(self.view, 20)
    .heightIs(40);
    
    self.nextButton.sd_layout
    .topSpaceToView(self.pwdView, 100)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(40);
    
}
#pragma mark - VLSPwdInputViewDelegate

- (void)hidePwdInputWaring {
    [self hideWarning];
}
- (void)nextActionEnable:(BOOL)enable {
    [self nextBtnEnable:enable];
}

- (void)nextAction:(UIButton *)button {
    
    self.model.password = self.pwdView.pwdTF.text;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        
        // 設置密碼成功后进行以下跳转操作
        VLSNewPhoneVerifyViewController *newPhoneVerifyVC = [[VLSNewPhoneVerifyViewController alloc] init];
        [self.navigationController pushViewController:newPhoneVerifyVC animated:YES];
        
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [AccountManager setNewPsd:self.model callback:callback];
    
    
}

#pragma mark - getters

- (VLSPwdInputView *)pwdView {
    if (!_pwdView) {
        self.pwdView = [[VLSPwdInputView alloc] init];
        self.pwdView.delegate = self;
        self.pwdView.placeholder = LocalizedString(@"LOGIN_PAGE_CURRENT_PASSWORD");
    }
    return _pwdView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
