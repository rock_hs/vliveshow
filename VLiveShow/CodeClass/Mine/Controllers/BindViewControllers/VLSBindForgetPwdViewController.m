//
//  VLSBindForgetPwdViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBindForgetPwdViewController.h"
#import "VLSBindNewPwdViewController.h"

@interface VLSBindForgetPwdViewController ()


@end

@implementation VLSBindForgetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = LocalizedString(@"LOGIN_LABEL_FORGET_PWD_OTHER");
    [self.nextButton setTitle:LocalizedString(@"LOGIN_BUTTON_NEXT") forState:UIControlStateNormal];

    [self doInit];
}

- (void)doInit {
    
    [self.view addSubview:self.countryView];
    self.countryView.sd_layout
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .topSpaceToView(self.view, 20)
    .heightIs(40);
    
    [self.view addSubview:self.phoneInputView];
    self.phoneInputView.sd_layout
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .topSpaceToView(self.countryView, 10)
    .heightIs(40);
    
    [self.view addSubview:self.verCodeInputView];
    self.verCodeInputView.sd_layout
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .topSpaceToView(self.phoneInputView, 10)
    .heightIs(40);
    
    [self.view addSubview:self.nextButton];
    self.nextButton.sd_layout
    .topSpaceToView(self.verCodeInputView, 80)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(40);
    
}


- (void)nextAction:(UIButton *)button {
    
    
    if (![self checkCurrentBindNum]) return;
    
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneInputView.normalPhoneNum;
    model.verificationCode = self.verCodeInputView.verCodeTF.text;
    model.nationCode = [self.phoneInputView.countryLabel.text substringFromIndex:1];

    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        
        VLSBindNewPwdViewController *setNewPwdVC = [[VLSBindNewPwdViewController alloc] init];
        
        AccountModel *model = [[AccountModel alloc] init];
        model.mobilePhone = self.phoneInputView.normalPhoneNum;
        model.verificationCode = self.verCodeInputView.verCodeTF.text;
        model.nationCode = [self.phoneInputView.countryLabel.text substringFromIndex:1];
        setNewPwdVC.model = model;

        [self.navigationController pushViewController:setNewPwdVC animated:YES];
    
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            ManagerEvent *event = result;
            if (event.code == 20003) {
                [self.verCodeInputView verCodeInputWrong];
                [self showWarningWithString:LocalizedString(@"VERCODE_ERROR")];
            } else if (event.code == 20005) {
                event.info = LocalizedString(@"PARAM_PHONE_ERROR");
                [self error:event];
            } else {
                [self error:result];
            }
        }
    };
    [AccountManager verifyVerCode:model callback:callback];


}

- (void)phoneNumberExisted {
    
}

- (BOOL)checkCurrentBindNum {
    if (![self.phoneInputView.normalPhoneNum isEqualToString:self.currentBindNum]) {

        [self.phoneInputView inputWrongPhoneNumber];
        [self showWarningWithString:LocalizedString(@"BIND_INPUT_CURRENT_PHONE")];
        return NO;
    } else {
        return YES;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
