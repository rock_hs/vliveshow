//
//  VLSBindNewPwdViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLoginBaseViewController.h"

@interface VLSBindNewPwdViewController : VLSLoginBaseViewController

@property (nonatomic, strong)AccountModel *model;

@end
