//
//  VLSNewPhoneVerifyViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSNewPhoneVerifyViewController.h"
#import "SettingViewController.h"
#import "MBProgressHUD+Add.h"

@interface VLSNewPhoneVerifyViewController ()

@end

@implementation VLSNewPhoneVerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self doInitNavigationBar];
    [self doInit];

}

- (void)doInitNavigationBar {
    
    self.navigationItem.title = LocalizedString(@"LOGIN_NEW_PHONE_VERIFY");

    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftItem;

}

- (void)doInit {
    
    
    [self.nextButton setTitle:LocalizedString(@"MINE_COMPELETE") forState:UIControlStateNormal];
    
    [self.view addSubview:self.countryView];
    [self.view addSubview:self.phoneInputView];
    [self.view addSubview:self.verCodeInputView];
    [self.view addSubview:self.nextButton];
    
    self.countryView.sd_layout
    .leftSpaceToView(self.view,30)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(self.view, 20)
    .heightIs(40);
    
    self.phoneInputView.sd_layout
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(self.countryView, 10)
    .heightIs(40);
    
    self.verCodeInputView.sd_layout
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(self.phoneInputView, 10)
    .heightIs(40);
    
    self.nextButton.sd_layout
    .topSpaceToView(self.verCodeInputView, 80)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(40);
    
}

- (void)backAction {
    
    
    [self performAlertChangeBindPhone];
    
}

- (void)nextAction:(UIButton *)button {
    
    if (![self.phoneInputView.countryLabel.text isEqualToString:COUNTRY_AREA_TW]
        && ![self.phoneInputView.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
        if ([self clickNextBtnAndVerifyPhoneExists]) {
            
            [self phoneNumberExisted];
            
            return;
        }
        
    }
    
    
    AccountModel *model = [[AccountModel alloc] init];
    
    model.mobilePhone = self.phoneInputView.normalPhoneNum;
    model.verificationCode = self.verCodeInputView.verCodeTF.text;
    model.nationCode = [self.phoneInputView.countryLabel.text substringFromIndex:1];
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self success:result];
        }
        [self getUserInfo];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            for (VLSBaseViewController *vc in self.navigationController.viewControllers) {
                if ([vc isKindOfClass:[SettingViewController class]]) {
                    
                    SettingViewController *settingVC = (SettingViewController *)vc;
                    settingVC.bindNewNum = self.phoneInputView.normalPhoneNum;
                    [self.navigationController popToViewController:settingVC animated:YES];
                    NSString *showStr = [NSString stringWithFormat:@"%@ %@", LocalizedString(@"MINE_PHONENUMBER"), settingVC.myPhoneNumber];
                    [MBProgressHUD show:showStr detailText:LocalizedString(@"INVITE_CHANGE_SUCCESS") icon:@"broadcast_audience_view_verify_succeed" view:settingVC.view];
                    break;
                }
            }
            
            
        });
        
    };
    callback.errorBlock = ^(id result){
        ManagerEvent *event = result;
        if (event.code == 20003)
        {
            [self.verCodeInputView verCodeInputWrong];
            [self showWarningWithString:LocalizedString(@"VERCODE_ERROR")];
        } else {
            [self error:result];
        }
    };
    
    [[VLSUserTrackingManager shareManager] trackingBindingphone:model bindType:@"rebinding"] ;
    [AccountManager phoneBind:model callback:callback];
}

- (void)performAlertChangeBindPhone {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"BIND_ALERT_CHANGE_PHONE") preferredStyle:UIAlertControllerStyleAlert];
    // 确定按钮
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    // 取消按钮
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleCancel handler:nil];
    
    [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    [okAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
