//
//  VLSBindForgetPwdViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLoginBaseViewController.h"

@interface VLSBindForgetPwdViewController : VLSLoginBaseViewController

/// 当前绑定的手机号
@property (nonatomic, copy)NSString *currentBindNum;

@end
