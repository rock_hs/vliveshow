//
//  VLSPwdVerifyViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLoginBaseViewController.h"

@interface VLSPwdVerifyViewController : VLSLoginBaseViewController
/// 当前绑定的手机号
@property (nonatomic, copy)NSString *currentBindNum;

@end
