//
//  VLSPwdVerifyViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSPwdVerifyViewController.h"
#import "VLSPwdInputView.h"
#import "VLSNewPhoneVerifyViewController.h"
#import "VLSBindForgetPwdViewController.h"

@interface VLSPwdVerifyViewController ()<VLSPwdInputViewDelegate>

@property (nonatomic, strong)VLSPwdInputView *pwdView;
@property (nonatomic, strong)UIButton *forgetPwdBtn;

@end

@implementation VLSPwdVerifyViewController


- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = LocalizedString(@"LOGIN_PWD_VERIFY");
    
    
    [self doInit];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)doInit {
    [self.view addSubview:self.pwdView];
    [self.view addSubview:self.forgetPwdBtn];
    [self.view addSubview:self.nextButton];

    self.pwdView.sd_layout
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .topSpaceToView(self.view, 20)
    .heightIs(40);
    
    self.forgetPwdBtn.sd_layout
    .topSpaceToView(self.pwdView, 20)
    .centerXEqualToView(self.view)
    .heightIs(30)
    .widthIs(80);
    
    self.nextButton.sd_layout
    .topSpaceToView(self.forgetPwdBtn, 100)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(40);
    
}

#pragma mark - VLSPwdInputViewDelegate 

- (void)hidePwdInputWaring {
    [self hideWarning];
}
- (void)nextActionEnable:(BOOL)enable {
    [self nextBtnEnable:enable];
}

- (void)nextAction:(UIButton *)button {
    self.nextButton.enabled = NO;
    // 添加验证密码的 API，验证成功后进行以下跳转操作
    
    AccountModel *model = [[AccountModel alloc] init];
    model.password = self.pwdView.pwdTF.text;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        VLSNewPhoneVerifyViewController *newPhoneVerifyVC = [[VLSNewPhoneVerifyViewController alloc] init];
        [self.navigationController pushViewController:newPhoneVerifyVC animated:YES];
        self.nextButton.enabled = YES;

    };
    callback.errorBlock = ^(id result){
        self.nextButton.enabled = YES;
        if ([result isKindOfClass:[ManagerEvent class]]) {
            ManagerEvent *event = result;
            if (event.code == 20005) {
                [self.pwdView showWarningWithWrongPwd];
                [self showWarningWithString:LocalizedString(@"LOGIN_PAGE_PWD_ERROR")];
            } else {
                [self error:result];
            }
        }
    };
    [AccountManager verifyCurrentPwd:model callback:callback];
    

    
}

- (void)forgetPwdAction {
    VLSBindForgetPwdViewController *forgetPwdVC = [[VLSBindForgetPwdViewController alloc] init];
    if (self.currentBindNum) {
        forgetPwdVC.currentBindNum = self.currentBindNum;
    }
    [self.navigationController pushViewController:forgetPwdVC animated:YES];
}

#pragma mark - getters

- (VLSPwdInputView *)pwdView {
    if (!_pwdView) {
        self.pwdView = [[VLSPwdInputView alloc] init];
        self.pwdView.delegate = self;
        self.pwdView.placeholder = LocalizedString(@"LOGIN_PAGE_CURRENT_PASSWORD");
    }
    return _pwdView;
}

- (UIButton *)forgetPwdBtn {
    if (!_forgetPwdBtn) {
        self.forgetPwdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.forgetPwdBtn setTitle:LocalizedString(@"LOGIN_LABEL_FORGET_PWD") forState:UIControlStateNormal];
        [self.forgetPwdBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.forgetPwdBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.forgetPwdBtn.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        [self.forgetPwdBtn addTarget:self action:@selector(forgetPwdAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forgetPwdBtn;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
