//
//  LocationViewController.m
//  VLiveShow
//
//  Created by SXW on 16/6/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "LocationViewController.h"
#import "SNLocationManager.h"
#import "LocationCell.h"
NSString *strr = nil;
@interface LocationViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *sectionArray;
@property (nonatomic, strong) UILabel *resultLabel1;



@end

@implementation LocationViewController



-(void)viewWillAppear:(BOOL)animated{

//    [self showProgressHUDWithStr:@"定位中..." dimBackground:YES];
    self.navigationController.navigationBarHidden = NO;
//    [[SNLocationManager shareLocationManager] startUpdatingLocation];
    
    [[SNLocationManager shareLocationManager] startUpdatingLocationWithSuccess:^(CLLocation *location, CLPlacemark *placemark) {
        
        [self hideProgressHUDAfterDelay:1];
        strr = [NSString stringWithFormat:@"%@ %@",placemark.country,placemark.locality];
        NSLog(@"DD:%@ %@",placemark.country,placemark.locality);
        NSLog(@"DD22:%@ ",strr);
        [self.tableView reloadData];

    } andFailure:^(CLRegion *region, NSError *error) {
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"选择国家和地区";
    
    [self createTableView];
    [self createDataSource];
}

- (void)createTableView{

    _tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [_tableView registerClass:[LocationCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
}

- (void)createDataSource{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"country"
                                                     ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc]
                          initWithContentsOfFile:path];
    NSArray *keys = [dict allKeys];
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    for (int i = 0; i < sortedArray.count; i++) {
        NSLog(@"ddd:%@",sortedArray );
        [self.sectionArray addObjectsFromArray:dict[[NSString stringWithFormat:@"%@",[sortedArray objectAtIndex:i]]]];
    }
    
    NSLog(@"%@",_sectionArray);
    [self.tableView reloadData];
}


#pragma mark - 代理方法

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (section == 0) {
        return 1;
    }else if (section == 1){
    
        return self.sectionArray.count;
    }else{
    
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        LocationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.imageView.image = [UIImage imageNamed:@"location02@3x"];
        cell.resultLabel.text = strr;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        NSLog(@"DD33:%@ ",strr);
        return cell;
        ;
    }else{
        
        static NSString *cellID = @"cellw";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        NSString* str1 = _sectionArray[indexPath.row];
        NSRange range = [str1 rangeOfString:@"+"];
        NSString* countryName = [str1 substringToIndex:range.location];
            cell.textLabel.text = countryName;
        return cell;
    }
    
    return nil;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    if (section == 0) {
        return @"定位到的位置";
    }else{
    
        return @"全部";
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [[SNLocationManager shareLocationManager] startUpdatingLocationWithSuccess:^(CLLocation *location, CLPlacemark *placemark) {
         strr = [NSString stringWithFormat:@"%@ %@",placemark.country,placemark.locality];
    } andFailure:^(CLRegion *region, NSError *error) {
        
    }];
}

//初始化，懒加载
-(NSMutableArray *)sectionArray{

    if (_sectionArray == nil) {
        _sectionArray = [[NSMutableArray alloc] init];
    }
    return _sectionArray;
}








































/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
