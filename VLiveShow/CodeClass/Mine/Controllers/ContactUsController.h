//
//  ContactUsController.h
//  VLiveShow
//
//  Created by sp on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"

@interface ContactUsController : VLSBaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)UIImageView *logoImage;
@property (nonatomic,strong)UITableView *baseTable;

@end
