//
//  MySettingViewController.m
//  VLiveShow
//
//  Created by SXW on 16/6/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "MySettingViewController.h"
#import "VLSMessageManager.h"
#import "VLSUserProfile.h"
#import "MineModel.h"
#import "MJExtension.h"
#import "VLSTabBarController.h"
#import "AppDelegate.h"
#import "HTTPFacade.h"
#import "UUIDShortener.h"
#import "VLSMineViewController.h"
#import "AccountManager.h"
#import "VLSLiveHttpManager.h"
#import "PhotoViewController.h"
#import "NickNameViewController.h"
#import "SignNameViewController.h"
#import "UICopyLabel.h"
#import "LocationViewController.h"
#import "AllSectionViewController.h"
#import "TableViewHeadView.h"
#import "VLSMySettingCell.h"
@interface MySettingViewController ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AllSecondViewControllerDelegate,TableViewHeadViewDelegate>{

    UITextField *nickTextField;
    NSString *gender;
    BOOL push;
}
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) VLSUserProfile *Vmodel;
@property (nonatomic, strong) MineModel *mModel;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *cellLine;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *sexLabel;

@property (nonatomic, copy) NSString *nameStr;
@property (nonatomic, copy) NSString *signStr;
@property (nonatomic, copy) NSString *areaStr;

@property (nonatomic, strong) UIButton *cpBtn;
@property (nonatomic, strong) UILabel *idLab;
@property (nonatomic, strong) UICopyLabel *idCopyLab;

/// 照片选择器
@property (nonatomic, strong)UIImagePickerController *imagePicker;

@end

@implementation MySettingViewController


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    _nameStr = [AccountManager sharedAccountManager].account.nickName;
    _signStr = [AccountManager sharedAccountManager].account.intro;//签名
    _areaStr = [AccountManager sharedAccountManager].account.area;
    NSLog(@"hahha222:%@",[AccountManager sharedAccountManager].account.avatars);
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"uploadImage"];
    UIImage *image = [UIImage imageWithData:data];
    if (!image) {
        image = ICON_PLACEHOLDER;
    }
    _headImageView.image = image;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUserInfo" object:nil];

    [self.tableView reloadData];
    if (!push) {
        [self.navigationController setNavigationBarHidden:NO animated:animated];
    }
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    push = NO;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (!push) {
        [self.navigationController setNavigationBarHidden:YES animated:animated];
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = LocalizedString(@"MINE_PERSONAL_DATA");

    //等待
    _Vmodel = [VLSMessageManager sharedManager].host;
    
    [self getMineInfo];
    [self createDataSource];
    [self createTableView];
    [self createHeadImageView];
}

#pragma mark - 创建数据源
- (void)createDataSource{
   
    _array = @[LocalizedString(@"MINE_PERSONAL_HEADSCULPTURE")
               ,LocalizedString(@"MINE_PERSONAL_NAME")
               ,LocalizedString(@"MINE_PERSONAL_SEX")
               ,LocalizedString(@"MINE_PERSONAL_ID")
               ,LocalizedString(@"MINE_PERSONAL_PLACE")
               ,LocalizedString(@"MINE_PERSONAL_SIGN")];
}

#pragma mark - 数据请求重重之重2016.5.27添加
//请求数据
- (void)getMineInfo{
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        _Vmodel = result;
        [self.tableView reloadData];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [VLSLiveHttpManager getUserInfo:[NSString stringWithFormat:@"%lu",(long)[AccountManager sharedAccountManager].account.uid] callback:callback];
}


//表格创建
- (void)createTableView{
    //设置组头后，如果style为UITableViewStylePlain， 拖动时，组头随着某一分组一起移动
    _tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    
    //数据源代理
    _tableView.dataSource = self;
    //tableview代理
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
    
    [_tableView registerClass:[VLSMySettingCell class] forCellReuseIdentifier:@"cell"];
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.tableFooterView = [UIView new];
    _tableView.scrollEnabled = NO;
   
}

#pragma mark - 代理方法
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _array.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VLSMySettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.titleLable.text = _array[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _cellLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 55, SCREEN_WIDTH, 1.0f)];
    _cellLine.backgroundColor = RGB16(COLOR_LINE_ECECEC);
    [cell.contentView addSubview:_cellLine];
    
    if (indexPath.row == 0) {
        
        cell.inditorImageView.hidden = YES;
        cell.titleLable.frame = CGRectMake(24, 30, 100, 22);
        _cellLine.frame = CGRectMake(0, 81, SCREEN_WIDTH, 1.0f);
        [cell.contentView addSubview:_headImageView];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else  if (indexPath.row == 1) {
    
        cell.detailTitleLabel.text = _nameStr;
    }
    else if (indexPath.row == 2) {
        
        cell.detailTitleLabel.hidden = YES;
        if (!_sexLabel) {
            _sexLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 189, 17, 130, 22)];
            _sexLabel.font = [UIFont systemFontOfSize:SIZE_FONT_16];
            _sexLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
            _sexLabel.textAlignment = NSTextAlignmentRight;
            [cell.contentView addSubview:_sexLabel];
            
        }
        _sexLabel.text = [AccountManager sharedAccountManager].account.gender;
        
        if ([_sexLabel.text isEqualToString:@"MALE"]) {
            _sexLabel.text = LocalizedString(@"MINE_PERSONAL_MALE");
        }else if ([_sexLabel.text isEqualToString:@"FEMALE"]){
            _sexLabel.text = LocalizedString(@"MINE_PERSONAL_FEMALE");
        }else{
            
            _sexLabel.text = LocalizedString(@"MINE_PERSONAL_SECRET");
        }

    }else if (indexPath.row == 3){

        cell.inditorImageView.hidden = YES;
        cell.detailTitleLabel.frame = CGRectMake(SCREEN_WIDTH - 160, 17, 130, 22);
        cell.detailTitleLabel.text = _Vmodel.userId;
    }
    
    else if (indexPath.row == 4){
        
        cell.detailTitleLabel.text = _areaStr;
    }
    else if (indexPath.row == 5){
        
        if ([_signStr isEqualToString:@" "]) {
            _signStr = LocalizedString(@"NOTWRITE");
        }
        cell.detailTitleLabel.text = _signStr;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        return 82;
    }else{
        return 56;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    push = YES;
    if (indexPath.row == 0) {
        
        PhotoViewController *photo = [[PhotoViewController alloc] init];
        photo.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:photo animated:YES];
    }
    
    if (indexPath.row == 1) {
        NickNameViewController *nickVC = [[NickNameViewController alloc] init];
        [self.navigationController pushViewController:nickVC animated:YES];
    }
    
    if (indexPath.row == 2) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"MINE_PERSONAL_CHANGE") preferredStyle:UIAlertControllerStyleActionSheet];
        // 男
        UIAlertAction *maleAction = [UIAlertAction actionWithTitle:LocalizedString(@"MINE_PERSONAL_MALE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _sexLabel.text = @"MALE";
            [[NSUserDefaults standardUserDefaults] setObject:_sexLabel.text forKey:@"male"];
            [self showProgressHUD];
            [self uploadUserGender];
        }];
        // 女
        UIAlertAction *femaleAction = [UIAlertAction actionWithTitle:LocalizedString(@"MINE_PERSONAL_FEMALE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _sexLabel.text = @"FEMALE";
            [[NSUserDefaults standardUserDefaults] setObject:_sexLabel.text forKey:@"female"];
            [self showProgressHUD];
            [self uploadUserGender];
        }];
        // 保密
        UIAlertAction *baomiAction = [UIAlertAction actionWithTitle:LocalizedString(@"MINE_PERSONAL_SECRET") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _sexLabel.text = @"UNKNOWN";
            [self showProgressHUD];
            [self uploadUserGender];
        }];
        
        // 取消
        UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:LocalizedString(@"MINE_PERSONAL_CANCEL") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertController addAction:maleAction];
        [alertController addAction:femaleAction];
        [alertController addAction:baomiAction];
        [alertController addAction:cancleAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
   
    if (indexPath.row == 4) {
        
        AllSectionViewController *allLvc = [[AllSectionViewController alloc] init];
        [self.navigationController pushViewController:allLvc animated:YES];
        allLvc.delegate = self;
    }
    if (indexPath.row == 5) {
        SignNameViewController *signName = [[SignNameViewController alloc] init];
        [self.navigationController pushViewController:signName animated:YES];
    }
}
#pragma mark - 上传用户信息

- (void)uploadUserGender{
    
    NSData *data = UIImageJPEGRepresentation(_headImageView.image, 0.4);
    
    PostFileModel *fileModel = [[PostFileModel alloc] init];
    fileModel.fileName = [NSUUID UUID].shortUUIDString;
    fileModel.fileArray = @[data];
    AccountModel *accountModel = [[AccountModel alloc] init];
    accountModel.gender = _sexLabel.text;//性别
    if ([_sexLabel.text isEqualToString:@"MALE"]) {
        _sexLabel.text = LocalizedString(@"MINE_PERSONAL_MALE");
    }else if ([_sexLabel.text isEqualToString:@"FEMALE"]){
        _sexLabel.text = LocalizedString(@"MINE_PERSONAL_FEMALE");
    }else{
    
        _sexLabel.text = LocalizedString(@"MINE_PERSONAL_SECRET");
    }
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.loadBlock = ^(id result){
        
    };
    callback.updateBlock = ^(id result){
        
        [self hideProgressHUDAfterDelay:0];
        [self getUserInfo];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager postUserProfile:accountModel callback:callback];
}

#pragma mark - 获取用户信息的网络请求

- (void)getUserInfo {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [[VLSMessageManager sharedManager] getSelfProfile];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [AccountManager getUserProfileCallback:callback];
}

- (void)goBackB:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma  mark - 按钮点击事件
- (void)cpClick:(UIButton*)sender{

    sender.selected = !sender.selected;
    if (sender.selected) {
        UIPasteboard *pboard = [UIPasteboard generalPasteboard];
        pboard.string = _Vmodel.userId;
    }
}

- (void)createHeadImageView{

    _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 84, 14, 54, 54)];
    _headImageView.layer.cornerRadius = 27;
    _headImageView.clipsToBounds = YES;

    _headImageView.contentMode =  UIViewContentModeScaleAspectFill;
    NSString *url = [AccountManager sharedAccountManager].account.avatars;
    UIImage *image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uploadImage"]];
    if (!image) {
        image = ICON_PLACEHOLDER;
    }
    [_headImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:image];
}

#pragma mark - 地区
- (void)secondData:(NSString *)data{

    _Vmodel.userLocation = data;
}

-(void)sendStr:(NSString *)str{

    _Vmodel.userLocation = str;
}

@end
