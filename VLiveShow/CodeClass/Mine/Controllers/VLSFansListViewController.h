//
//  VLSFansListViewController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSRefreshViewController.h"
#import "FansTableViewCell.h"
#import "VLSLiveHttpManager.h"
#import "VLSOthersViewController.h"
@interface VLSFansListViewController : VLSRefreshViewController<FansTableViewCellDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *fanNumArray;
@property (nonatomic) NSInteger page;
@property (nonatomic, strong) UILabel *fansLabel;
@property (nonatomic, assign) NSInteger userId;

@end
