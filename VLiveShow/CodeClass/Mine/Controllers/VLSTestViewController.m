//
//  VLSTestViewController.m
//  VLiveShow
//
//  Created by tom.zhu on 16/9/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSTestViewController.h"
static NSString *CELL_ID = @"CELL_ID";

@interface Model : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *subTitle;
@property (nonatomic, strong) NSString *selector;
- (instancetype)initWithDic:(NSDictionary*)dic;
@end

@implementation Model
- (instancetype)initWithDic:(NSDictionary*)dic {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}
@end

@interface VLSTestViewController () <UIActionSheetDelegate>
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation VLSTestViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    [self uiconig];
}

- (void)uiconig {
    self.tableView.tableFooterView = UIView.new;
    
    NSString *title = @"返回";
    NSString *subTitle = @"";
    NSDictionary *dic = @{
                          @"title" : title,
                          @"subTitle" : subTitle,
                          @"selector" : NSStringFromSelector(@selector(back)),
                          };
    Model *model = [[Model alloc] initWithDic:dic];
    [self.dataArray addObject:model];
    
    title = @"base_url";
    subTitle = BASE_URL;
    dic = @{
                          @"title" : title,
                          @"subTitle" : subTitle,
                          @"selector" : NSStringFromSelector(@selector(baseURL)),
                          };
    model = [[Model alloc] initWithDic:dic];
    [self.dataArray addObject:model];
    
    title = @"VersionString";
    subTitle = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    dic = @{
            @"title" : title,
            @"subTitle" : subTitle,
            };
    model = [[Model alloc] initWithDic:dic];
    [self.dataArray addObject:model];
    
    title = @"Bundle Identifier";
    subTitle = [[NSBundle mainBundle] infoDictionary][@"CFBundleIdentifier"];
    dic = @{
            @"title" : title,
            @"subTitle" : subTitle,
            };
    model = [[Model alloc] initWithDic:dic];
    [self.dataArray addObject:model];
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)baseURL {
    UIActionSheet *act = [[UIActionSheet alloc] initWithTitle:@"baseurl切换" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"http://124.172.174.187:8080/vliveshow-api-app", @"http://www.vliveshow.com/vliveshow-api-app", @"http://124.172.174.187:8888/vliveshow-api-app", nil];
    [act showInView:self.view];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex NS_DEPRECATED_IOS(2_0, 8_3) __TVOS_PROHIBITED {
//    BASE_URL = [actionSheet buttonTitleAtIndex:buttonIndex];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CELL_ID];
    }
    Model *model = self.dataArray[indexPath.row];
    cell.textLabel.text = model.title;
    cell.detailTextLabel.text = model.subTitle;
    if ([model.title isEqualToString:@"base_url"]) {
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Model *model = self.dataArray[indexPath.row];
    SEL selector = NSSelectorFromString(model.selector);
    if (selector) {
        Method method = class_getInstanceMethod(self.class, selector);
        if (nil == method) {
            return;
        }
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:selector];
#pragma clang diagnostic pop
    }
}

@end
