//
//  MySettingViewController.h
//  VLiveShow
//
//  Created by SXW on 16/6/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"

@protocol sendUserImgDelegate <NSObject>
@optional
- (void)sendUserImg:(NSString *)image;

@end

@interface MySettingViewController : VLSBaseViewController

@property (nonatomic, weak) id<sendUserImgDelegate>delegate;

@end
