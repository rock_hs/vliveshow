//
//  PhotoViewController.m
//  VLiveShow
//
//  Created by SXW on 16/6/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "PhotoViewController.h"
#import "AJPhotoPickerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AJPhotoBrowserViewController.h"
#import "HTTPFacade.h"
#import "AccountManager.h"
#import "VLSLiveHttpManager.h"
#import "UUIDShortener.h"
#import "UIImage+new.h"

@interface PhotoViewController ()<AJPhotoPickerProtocol,AJPhotoBrowserDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate/*,VLSPhotoSelectViewDelegate*/>

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIView *btnBackgroundView;
@property (nonatomic, strong) UIButton *selectPhotoBtn;
@property (nonatomic, strong) UIButton *camaraBtn;
@property (strong, nonatomic) NSMutableArray *photos;

@end

@implementation PhotoViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
//    [self uploadUserImage];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = LocalizedString(@"MINE_SET_ICON");
    
    [self loadViews];
    UIImage *image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uploadImage"]];
    if (!image) {
        image = ICON_PLACEHOLDER;
    }
    _imageView.image = image;
//        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home_back"] style:UIBarButtonItemStylePlain target:self action:@selector(leftItemAction:)];
//    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)loadViews{

    [self.view addSubview:self.imageView];
    [self.view addSubview:self.btnBackgroundView];
    [self.btnBackgroundView addSubview:self.selectPhotoBtn];
    [self.btnBackgroundView addSubview:self.camaraBtn];
}


//初始化控件
-(UIImageView *)imageView{

    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 375 * SCREEN_HEIGHT / 667)];
        _imageView.backgroundColor = [UIColor whiteColor];
        _imageView.clipsToBounds = YES;
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _imageView.layer.borderWidth = 1;
    }
    return _imageView;
}

-(UIView *)btnBackgroundView{

    if (!_btnBackgroundView) {
        _btnBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_imageView.frame), SCREEN_WIDTH, 228 * SCREEN_HEIGHT / 667)];
        _btnBackgroundView.backgroundColor = [UIColor whiteColor];
    }
    return _btnBackgroundView;
}

-(UIButton *)selectPhotoBtn{

    if (!_selectPhotoBtn) {
        _selectPhotoBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        _selectPhotoBtn.frame = CGRectMake(38, (_btnBackgroundView.frame.size.height - 100 * SCREEN_HEIGHT / 667) * SCREEN_HEIGHT / 2 / 667, SCREEN_WIDTH - 76, 40 * SCREEN_HEIGHT / 667);
        _selectPhotoBtn.layer.cornerRadius = 20 * SCREEN_HEIGHT / 667;
        _selectPhotoBtn.backgroundColor = [UIColor whiteColor];
        _selectPhotoBtn.layer.borderColor = [UIColor grayColor].CGColor;
        _selectPhotoBtn.layer.borderWidth = 1;
        [_selectPhotoBtn setTitle:LocalizedString(@"PHOTO_SELECT_FROM_ALBUM") forState:UIControlStateNormal];
        [_selectPhotoBtn setTintColor:[UIColor blackColor]];
        [_selectPhotoBtn addTarget:self action:@selector(photoBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectPhotoBtn;
}

-(UIButton *)camaraBtn{

    if (!_camaraBtn) {
        _camaraBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        _camaraBtn.frame = CGRectMake(38, CGRectGetMaxY(_selectPhotoBtn.frame) + 20, SCREEN_WIDTH - 76, 40 * SCREEN_HEIGHT / 667);
        _camaraBtn.layer.cornerRadius = 20 * SCREEN_HEIGHT / 667;
        _camaraBtn.backgroundColor = [UIColor whiteColor];
        _camaraBtn.layer.borderColor = [UIColor grayColor].CGColor;
        _camaraBtn.layer.borderWidth = 1;
        [_camaraBtn setTitle:LocalizedString(@"MINE_TAKEPHOTO") forState:UIControlStateNormal];
        [_camaraBtn setTintColor:[UIColor blackColor]];
        [_camaraBtn addTarget:self action:@selector(camaraBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _camaraBtn;
}

#pragma mark - 按钮事件
- (void)photoBtnClick:(UIButton*)sender{
    
//    [self clearCache];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate=self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //是否允许编辑图片
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)camaraBtnClick:(id)sender{
    
//    [self clearCache];
    UIImagePickerController *cameraUI = [UIImagePickerController new];
    cameraUI.allowsEditing = YES;
    cameraUI.delegate = self;
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    cameraUI.cameraFlashMode=UIImagePickerControllerCameraFlashModeAuto;
    [self presentViewController: cameraUI animated: YES completion:nil];
}

#pragma mark - UIImagePickerDelegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *) picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo {
    if (!error) {
        NSLog(@"保存到相册成功");
    }else{
        NSLog(@"保存到相册出错%@", error);
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage *image = info[UIImagePickerControllerEditedImage];
    self.imageView.image = image;
    NSData *imageData = UIImagePNGRepresentation(image);
    [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"uploadImage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self uploadUserImage];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)checkCameraAvailability:(void (^)(BOOL auth))block {
    BOOL status = NO;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        status = YES;
    } else if (authStatus == AVAuthorizationStatusDenied) {
        status = NO;
    } else if (authStatus == AVAuthorizationStatusRestricted) {
        status = NO;
    } else if (authStatus == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                if (block) {
                    block(granted);
                }
            } else {
                if (block) {
                    block(granted);
                }
            }
        }];
        return;
    }
    if (block) {
        block(status);
    }
}

- (void)clearCache{

    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"avatars"];
    [[SDWebImageManager sharedManager].imageCache removeImageForKey:[AccountManager sharedAccountManager].account.avatars];
    [[SDWebImageManager sharedManager].imageCache clearMemory];
}

#pragma mark - 上传用户信息

- (void)uploadUserImage{
    [self showProgressHUD];

    NSData *data = UIImageJPEGRepresentation(_imageView.image, 0.2);
    PostFileModel *fileModel = [[PostFileModel alloc] init];
    fileModel.fileName = [NSUUID UUID].shortUUIDString;
    fileModel.fileArray = @[data];
    AccountModel *accountModel = [[AccountModel alloc] init];
    accountModel.fileModel = fileModel;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.loadBlock = ^(id result){
        
    };
    callback.updateBlock = ^(id result){
        [[VLSMessageManager sharedManager] getSelfProfile];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"uploadImage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUserInfo" object:nil];
        [self hideProgressHUDAfterDelay:0];

//        [self getUserInfo];
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];

        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
            NSString *avurl = [AccountManager sharedAccountManager].account.avatars;
            [SDWebImageManager.sharedManager downloadImageWithURL:[NSURL URLWithString:avurl] options:SDWebImageRefreshCached | SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
                NSData *imageData = UIImagePNGRepresentation(image);
                [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"uploadImage"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                _imageView.image = image;
            }];
        }

    };
    
    [AccountManager postUserProfile :accountModel callback:callback];
}

#pragma mark - 获取用户信息的网络请求

- (void)getUserInfo {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [[VLSMessageManager sharedManager] getSelfProfile];
        
        NSString *url = [AccountManager sharedAccountManager].account.avatars;
        UIImage *image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uploadImage"]];
        if (!image) {
            image = ICON_PLACEHOLDER;
        }
        [_imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:image];

    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];

        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [AccountManager getUserProfileCallback:callback];
}


//#pragma mark - 按钮事件
//
- (void)leftItemAction:(id)sender{

    if (!self.isLoading) {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

@end
