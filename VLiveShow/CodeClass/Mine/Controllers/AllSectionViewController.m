//
//  AllSectionViewController.m
//  VLiveShow
//
//  Created by SXW on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "AllSectionViewController.h"
#import "YJLocalCountryData.h"
#import <SMS_SDK/SMSSDK.h>
#import <SMS_SDK/Extend/SMSSDKCountryAndAreaCode.h>
#import "SNLocationManager.h"
#import "TableViewHeadView.h"
#import "HTTPFacade.h"
#import "UUIDShortener.h"
#import "VLSMineViewController.h"
#import "AccountManager.h"
#import "VLSLiveHttpManager.h"
#import "VLSLineTableViewCell.h"

@interface AllSectionViewController ()<TableViewHeadViewDelegate>
{
    NSMutableData*_data;
    
    NSMutableArray* _areaArray;
    
    NSBundle *_bundle;
    NSIndexPath *  selectIndpath;
    NSInteger selectsection;
    NSInteger selectrow;
    BOOL isselect;
    BOOL isloadLocation;
    NSTimer *myTimer;
}

@property (nonatomic, strong)TableViewHeadView *headView;
@property (nonatomic, strong) SNLocationManager *manager;
@property (nonatomic, strong) SMSSDKCountryAndAreaCode* countryNSObject;
@property (nonatomic, copy) NSString *languageStr;
@end

@implementation AllSectionViewController
@synthesize names;
@synthesize keys;
@synthesize table;
@synthesize allNames;
#pragma mark Custom Methods

-(void)viewWillAppear:(BOOL)animated{
    
    _languageStr = [self getPreferredLanguage];
    NSLog(@"2222:%@",_languageStr);
    
    [self startLoadLocation];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = LocalizedString(@"MINE_SELECTCOUNTRYANDAREA");
    self.view.backgroundColor = [UIColor whiteColor];
    
    /********************获取当前语言环境*********************/
    
    
    /********************plist文件读取*********************/
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SMSSDKUI" ofType:@"bundle"];
    NSBundle *bundle = [[NSBundle alloc] initWithPath:filePath];
    _bundle = bundle;
    [self createTabelView];
    
    _headView.search.delegate = self;
    [self readPlistWithLanguage];
    [self resetSearch];
    [table reloadData];
}

#pragma mark - 表格创建
- (void)createTabelView{
    
    //tableView的位置
    table = [[UITableView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.tableHeaderView = self.headView;
    [self.view addSubview:table];
    
    table.dataSource = self;
    table.delegate = self;
}

#pragma mark - 根据环境读取不同的plist文件
- (void)readPlistWithLanguage{
    
    _languageStr = [self getPreferredLanguage];
    //简体中文
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"country"ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    self.allNames = dict;
    NSLog(@"当前语言环境:%@", _languageStr);
}

//判断当前手机语言环境
- (NSString*)getPreferredLanguage
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSLog(@"当前语言:%@",preferredLang);
    return preferredLang;
}

#pragma mark - 开始定位 和 停止定位
- (void)startLoadLocation{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self stopLoadLocation];
    });
    //判断系统定位服务是否开启
    if ([CLLocationManager locationServicesEnabled]) {
        _manager = [[SNLocationManager alloc] init];
        self.headView.imageView.hidden = YES;
        [self showWaiting:self.view];//显示自定义的菊花转
        
        [_manager startUpdatingLocationWithSuccess:^(CLLocation *location, CLPlacemark *placemark) {
            isloadLocation = YES;
            
            //如果获取不到位置信息判断
            if (!placemark.country) {
                [self hideWaiting];
                self.headView.imageView.hidden = NO;
                
                _headView.imageView.image = [UIImage imageNamed:@"broadcast_audience_view_bbg_wrong_vercode_notice"];
                _headView.headLabel2.text = LocalizedString(@"MINE_CANNOTGETLOCATIONMESSAGE");//无法获得位置信息
                return ;
            }
            
            [[VLSUserTrackingManager shareManager] tackingLocation:[NSString stringWithFormat:@"%@%@",placemark.country,placemark.locality] longitude:[NSString stringWithFormat:@"%f",location.coordinate.longitude] latitude:[NSString stringWithFormat:@"%f",location.coordinate.latitude]];
            
            if ([[placemark.locality substringFromIndex:placemark.locality.length - 1] isEqualToString:LocalizedString(@"CITY")]) {
                _headView.headLabel2.text = [NSString stringWithFormat:@"%@ %@",placemark.country,[placemark.locality substringToIndex:placemark.locality.length - 1]];
            }else{
                _headView.headLabel2.text = [NSString stringWithFormat:@"%@ %@",placemark.country,placemark.locality];
            }
            NSLog(@"DD:%@ %@",placemark.country,placemark.locality);
            NSLog(@"BBSB22:%@",_headView.headLabel2.text);
            
            [table reloadData];
            
            [self hideWaiting];
            self.headView.imageView.hidden = NO;
            _headView.headLabel2.frame = CGRectMake(30, self.headView.frame.size.height / 4, SCREEN_WIDTH, self.headView.frame.size.height / 2);
        } andFailure:^(CLRegion *region, NSError *error) {
            
            self.headView.imageView.hidden = NO;
            _headView.imageView.image = [UIImage imageNamed:@"broadcast_audience_view_bbg_wrong_vercode_notice"];
            _headView.headLabel2.text = LocalizedString(@"MINE_CANNOTGETLOCATIONMESSAGE");//无法获得位置信息
            return ;
            
        }];
        
    }else
    {//不能定位用户的位置
        //1.提醒用户检查当前的网络状况
        //2.提醒用户打开定位开关
        _headView.imageView.image = [UIImage imageNamed:@"broadcast_audience_view_bbg_wrong_vercode_notice"];
        _headView.headLabel2.text = @"LOCATION_PERMISSIONS_DONTOPEN";
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"REMIND") message:LocalizedString(@"LOCATION_CLOSE_AREYOUSURETO_OPEN") preferredStyle:UIAlertControllerStyleAlert];
        // 确定按钮
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            NSURL *url = [NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"];
            if ([[UIApplication sharedApplication] canOpenURL:url])
            {
                [[UIApplication sharedApplication] openURL:url];
            }
        }];
        // 取消按钮
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)stopLoadLocation
{
    _headView.btn.enabled = YES;
    [_manager.locationManager stopUpdatingLocation];
    if (isloadLocation == YES) {
        
    }else
    {
        [self hideWaiting];
        _headView.headLabel2.text = LocalizedString(@"MINE_CANNOTGETLOCATIONMESSAGE");
        _headView.imageView.image = [UIImage imageNamed:@"broadcast_audience_view_bbg_wrong_vercode_notice"];
        self.headView.imageView.hidden = NO;
    }
}


#pragma mark - 自定义菊花转
-(void)showWaiting:(UIView *)parent {
    
    int width = 32, height = 32;
    
    CGRect frame = CGRectMake(10, CGRectGetMaxY(_headView.headLabel1.frame) + 17, 15, 15);
    int x = frame.size.width;
    int y = frame.size.height;
    
    frame = CGRectMake((x - width) / 2, (y - height) / 2, width, height);
    UIActivityIndicatorView* progressInd = [[UIActivityIndicatorView alloc]initWithFrame:frame];
    [progressInd startAnimating];
    progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    
    frame =  CGRectMake(10, CGRectGetMaxY(_headView.headLabel1.frame) + 17, 15, 15);
    UIView *theView = [[UIView alloc] initWithFrame:frame];
    theView.alpha = 1;
    
    [theView addSubview:progressInd];
    [theView setTag:9999];
    [_headView addSubview:theView];
}

//消除滚动轮指示器
-(void)hideWaiting
{
    [[self.view viewWithTag:9999] removeFromSuperview];
}


- (void)resetSearch
{
    NSMutableDictionary *allNamesCopy = [YJLocalCountryData mutableDeepCopy:self.allNames];
    self.names = allNamesCopy;
    NSMutableArray *keyArray = [NSMutableArray array];
    [keyArray addObject:UITableViewIndexSearch];
    [keyArray addObjectsFromArray:[[self.allNames allKeys]
                                   sortedArrayUsingSelector:@selector(compare:)]];
    self.keys = keyArray;
}

- (void)handleSearchForTerm:(NSString *)searchTerm
{
    NSMutableArray *sectionsToRemove = [NSMutableArray array];
    [self resetSearch];
    
    for (NSString *key in self.keys) {
        NSMutableArray *array = [names valueForKey:key];
        NSMutableArray *toRemove = [NSMutableArray array];
        for (NSString *name in array) {
            if ([name rangeOfString:searchTerm
                            options:NSCaseInsensitiveSearch].location == NSNotFound)
                [toRemove addObject:name];
        }
        if ([array count] == [toRemove count])
            [sectionsToRemove addObject:key];
        [array removeObjectsInArray:toRemove];
    }
    [self.keys removeObjectsInArray:sectionsToRemove];
    [table reloadData];
}

-(void)setAreaArray:(NSMutableArray*)array
{
    _areaArray = [NSMutableArray arrayWithArray:array];
}

#pragma mark Table View Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [keys count];
    
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    if ([keys count] == 0)
        return 0;
    
    NSString *key = [keys objectAtIndex:section];
    NSArray *nameSection = [names objectForKey:key];
    return [nameSection count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = [indexPath section];
    NSString *key = [keys objectAtIndex:section];
    NSArray *nameSection = [names objectForKey:key];
    
    static NSString *SectionsTableIdentifier = @"SectionsTableIdentifier";
    VLSLineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                  SectionsTableIdentifier ];
    if (cell == nil) {
        cell = [[VLSLineTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier: SectionsTableIdentifier ];
    }
    
    NSString* str1 = [nameSection objectAtIndex:indexPath.row];
    NSRange range = [str1 rangeOfString:@"+"];
    NSString* countryName = [str1 substringToIndex:range.location];
    
    cell.textLabel.text = countryName;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
    
    selectsection = [[[NSUserDefaults standardUserDefaults] objectForKey:@"section"] integerValue];
    selectrow = [[[NSUserDefaults standardUserDefaults] objectForKey:@"row"] integerValue];
    selectIndpath = [NSIndexPath indexPathForRow:selectrow inSection:selectsection];
    
    if (selectIndpath == indexPath) {
        cell.detailTextLabel.text = LocalizedString(@"SELECTEDAREA");
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
        cell.detailTextLabel.text = @"";
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section
{
    if ([keys count] == 0)
        return nil;
    NSString *key = [keys objectAtIndex:section];
    if (key == UITableViewIndexSearch)
        return nil;

    return key;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (isSearching)
        return nil;
    return keys;
}

- (NSInteger)tableView:(UITableView *)tableView
sectionForSectionIndexTitle:(NSString *)title
               atIndex:(NSInteger)index
{
    NSString *key = [keys objectAtIndex:index];
    if (key == UITableViewIndexSearch)
    {
        [tableView setContentOffset:CGPointZero animated:NO];
        return NSNotFound;
    }
    else return index;
    
}
#pragma mark TableViewDelegate Methods
- (NSIndexPath *)tableView:(UITableView *)tableView
  willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_headView.search resignFirstResponder];
    _headView.search.text = @"";
    isSearching = NO;
    [tableView reloadData];
    return indexPath;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[NSUserDefaults standardUserDefaults] setInteger:indexPath.section forKey:@"section"];
    [[NSUserDefaults standardUserDefaults] setInteger: indexPath.row forKey:@"row"];
    
    NSUInteger section = [indexPath section];
    NSString *key = [keys objectAtIndex:section];
    NSArray *nameSection = [names objectForKey:key];
    
    NSString* str1 = [nameSection objectAtIndex:indexPath.row];
    NSRange range = [str1 rangeOfString:@"+"];
    NSString* str2 = [str1 substringFromIndex:range.location];
    NSString* areaCode = [str2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    NSString* countryName = [str1 substringToIndex:range.location];
    
    _countryNSObject = [[SMSSDKCountryAndAreaCode alloc] init];
    _countryNSObject.countryName = countryName;
    _countryNSObject.areaCode = areaCode;
    [tableView reloadData];
    
    NSLog(@"%@ %@",countryName,areaCode);
    
    [self.view endEditing:YES];
    [self uploadUserCellArea];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
    //传递数据
    if ([self.delegate respondsToSelector:@selector(secondData:)]) {
        [self.delegate secondData:_countryNSObject.countryName];
    }
    
    NSLog(@"当前位置%ld %ld",indexPath.section, indexPath.row);
}

-(void)clickLeftButton
{
    [self dismissViewControllerAnimated:YES completion:^{
        ;
    }];
}

//赋值代理
- (void)btClick:(id)sender{
    
    _manager = [[SNLocationManager alloc] init];
    self.headView.imageView.hidden = YES;
    [self showWaiting:self.view];//显示菊花
    _headView.headLabel2.text = LocalizedString(@"MINE_BEINGLOCATON");
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [_manager startUpdatingLocationWithSuccess:^(CLLocation *location, CLPlacemark *placemark) {
            
            //如果获取不到位置信息判断
            if (!placemark.country) {
                [self hideWaiting];
                self.headView.imageView.hidden = NO;
                
                _headView.imageView.image = [UIImage imageNamed:@"broadcast_audience_view_bbg_wrong_vercode_notice"];
                _headView.headLabel2.text = LocalizedString(@"MINE_CANNOTGETLOCATIONMESSAGE");//无法获得位置信息
                return ;
            }
            
            if ([[placemark.locality substringFromIndex:placemark.locality.length - 1] isEqualToString:LocalizedString(@"CITY")]) {
                _headView.headLabel2.text = [NSString stringWithFormat:@"%@ %@",placemark.country,[placemark.locality substringToIndex:placemark.locality.length - 1]];
            }else{
                _headView.headLabel2.text = [NSString stringWithFormat:@"%@ %@",placemark.country,placemark.locality];
            }
            
            [[VLSUserTrackingManager shareManager] tackingLocation:[NSString stringWithFormat:@"%@%@",placemark.country,placemark.locality] longitude:[NSString stringWithFormat:@"%f",location.coordinate.longitude] latitude:[NSString stringWithFormat:@"%f",location.coordinate.latitude]];
            
            [self uploadUserArea];
            [table reloadData];
            
            if ([placemark.country isEqualToString:LocalizedString(@"CHINA")]) {
                
                [[NSUserDefaults standardUserDefaults] setInteger:26 forKey:@"section"];
                [[NSUserDefaults standardUserDefaults] setInteger: 4 forKey:@"row"];
            }else if ([placemark.country isEqualToString:LocalizedString(@"BRAZIL")]){
                
                [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"section"];
                [[NSUserDefaults standardUserDefaults] setInteger: 12 forKey:@"row"];
            }else if ([placemark.country isEqualToString:LocalizedString(@"AMERICA")]){
                
                [[NSUserDefaults standardUserDefaults] setInteger:13 forKey:@"section"];
                [[NSUserDefaults standardUserDefaults] setInteger: 21 forKey:@"row"];
            }else if ([placemark.country isEqualToString:LocalizedString(@"ARGENTINA")]){
                
                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"section"];
                [[NSUserDefaults standardUserDefaults] setInteger: 8 forKey:@"row"];
            }
            
            [self hideWaiting];
            self.headView.imageView.hidden = NO;
            self.headView.imageView.image = [UIImage imageNamed:@"location02"];
            _headView.headLabel2.frame = CGRectMake(30, self.headView.frame.size.height / 4, SCREEN_WIDTH, self.headView.frame.size.height / 2);
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        } andFailure:^(CLRegion *region, NSError *error) {
            
        }];
    });
    
    if ([_delegate respondsToSelector:@selector(sendStr:)]) {
        [_delegate sendStr:_headView.headLabel2.text];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (void)uploadUserArea{
    
    AccountModel *accountModel = [[AccountModel alloc] init];
    accountModel.area = _headView.headLabel2.text;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.loadBlock = ^(id result){
        
    };
    callback.updateBlock = ^(id result){
        
        [self hideProgressHUDAfterDelay:0];
        [self getUserInfo];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager postUserProfile: accountModel callback:callback];
}

- (void)uploadUserCellArea{
    
    [self showProgressHUD];
    AccountModel *accountModel = [[AccountModel alloc] init];
    accountModel.area = _countryNSObject.countryName;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.loadBlock = ^(id result){
        
    };
    callback.updateBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        [self getUserInfo];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager postUserProfile: accountModel callback:callback];
}

#pragma mark - 获取用户信息的网络请求
- (void)getUserInfo {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [[VLSMessageManager sharedManager] getSelfProfile];
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [AccountManager getUserProfileCallback:callback];
}

#pragma mark - 控件初始化
-(TableViewHeadView *)headView{
    
    if (!_headView) {
        _headView = [[TableViewHeadView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 100)];
        [_headView.btn addTarget:self action:@selector(btClick:) forControlEvents:UIControlEventTouchUpInside];    }
    return _headView;
}

@end
