//
//  AllSectionViewController.h
//  VLiveShow
//
//  Created by SXW on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"

@class SMSSDKCountryAndAreaCode;
@protocol AllSecondViewControllerDelegate;

@interface AllSectionViewController : VLSBaseViewController<UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate>
{
    UITableView *table;
    NSDictionary *allNames;
    NSMutableDictionary *names;
    NSMutableArray  *keys;
   
    BOOL    isSearching;
}
@property (nonatomic, strong)  UITableView *table;
@property (nonatomic, strong)  UISearchBar *search;
@property (nonatomic, strong) NSDictionary *allNames;
@property (nonatomic, strong) NSMutableDictionary *names;
@property (nonatomic, strong) NSMutableArray *keys;

@property (nonatomic, strong) id <AllSecondViewControllerDelegate> delegate;
@property(nonatomic,strong)  UIToolbar* toolBar;

- (void)resetSearch;
- (void)handleSearchForTerm:(NSString *)searchTerm;
-(void)setAreaArray:(NSMutableArray*)array;

@end

@protocol AllSecondViewControllerDelegate <NSObject>
- (void)secondData:(NSString *)data;
- (void)sendStr:(NSString*)str;
@end
