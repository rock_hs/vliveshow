//
//  AboutUsController.m
//  VLiveShow
//
//  Created by sp on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "AboutUsController.h"
#import "ContactUsController.h"
#import "VLSServiceViewController.h"
#import "VLSCommunityViewController.h"
#import "VLSPrivacyViewController.h"

@interface AboutUsController ()
{
    NSArray *nameArray;

}
@end

@implementation AboutUsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.baseTable.delegate = self;
    self.baseTable.dataSource = self;
    self.title = LocalizedString(@"MINE_ABOUTUS");
    [self loadAllView];
    self.view.backgroundColor = [UIColor whiteColor];
    nameArray = [NSArray arrayWithObjects:LocalizedString(@"MINE_COMMUNITYAPPOINT"),LocalizedString(@"MINE_PRIVACYPOLICY"),LocalizedString(@"MINE_SERVICETERMS"),LocalizedString(@"MINE_CONTACTUS"), nil];
    self.view.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)loadAllView
{
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.iconImage];
    [self.backView addSubview:self.nameLabel];
    [self.view addSubview:self.baseTable];
    [self.view addSubview:self.bottomLabel];
    
    self.backView.sd_layout
    .topSpaceToView(self.view, 0)
    .centerXEqualToView(self.view)
    .widthIs(SCREEN_WIDTH)
    .heightIs(251 * SCREEN_HEIGHT / 667);
    
    self.iconImage.sd_layout
    .topSpaceToView(self.backView, 73 * (SCREEN_HEIGHT / 667))
    .centerXEqualToView(self.backView)
    .widthIs(64 * (SCREEN_WIDTH / 375))
    .heightIs(80 * (SCREEN_HEIGHT / 667));
    
    self.nameLabel.sd_layout
    .topSpaceToView(self.iconImage, 2)
    .widthIs(SCREEN_WIDTH)
    .centerXEqualToView(self.iconImage)
    .heightIs(18);
    
    self.baseTable.sd_layout
    .topSpaceToView(self.backView, 0)
    .widthIs(SCREEN_WIDTH)
    .heightIs(180 * SCREEN_HEIGHT / 667);
    
    self.bottomLabel.sd_layout
    .bottomSpaceToView(self.view, 35 * SCREEN_HEIGHT / 667)
    .widthIs(SCREEN_WIDTH)
    .heightIs(15 * SCREEN_HEIGHT/ 667);
}
#pragma mark - tableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"countCell" forIndexPath:indexPath];
    cell.textLabel.text = [nameArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        VLSCommunityViewController *cvc = [VLSCommunityViewController new];
        [self.navigationController pushViewController:cvc animated:YES];
    }
    if (indexPath.row == 1) {
        VLSPrivacyViewController *pvc = [VLSPrivacyViewController new];
        [self.navigationController pushViewController:pvc animated:YES];
    }
    if (indexPath.row == 2) {
        VLSServiceViewController *svc = [VLSServiceViewController new];
        [self.navigationController pushViewController:svc animated:YES];

    }
    if (indexPath.row == 3) {
        ContactUsController *cuVC = [ContactUsController new];
        [self.navigationController pushViewController:cuVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 44.5f * (SCREEN_HEIGHT / 667);
}

-(UIView*)backView
{
    if (_backView == nil) {
        _backView = [[UIView alloc] init];
        _backView.backgroundColor = [UIColor clearColor];
    }

    return _backView;
}

- (UIImageView*)iconImage
{
    if (_iconImage == nil) {
        _iconImage = [[UIImageView alloc] init];
        _iconImage.image = [UIImage imageNamed:@"logo1"];
        _iconImage.contentMode = UIViewContentModeScaleAspectFill;
    }

    return _iconImage;
}
- (UILabel*)nameLabel
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.text = [NSString stringWithFormat:@"%@: V%@(%@)", LocalizedString(@"MINE_VERSIONCODE"), [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
        _nameLabel.font= [UIFont systemFontOfSize:14];
        _nameLabel.textColor = [UIColor grayColor];
    }

    return _nameLabel;
}
- (UITableView*)baseTable
{
    if (_baseTable == nil) {
        _baseTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_baseTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"countCell"];
        _baseTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _baseTable.backgroundColor = [UIColor clearColor];
        _baseTable.scrollEnabled = NO;
    }

    return _baseTable;
}
- (UILabel*)bottomLabel
{
    if (_bottomLabel==nil) {
        _bottomLabel = [[UILabel alloc] init];
        _bottomLabel.font = [UIFont systemFontOfSize:12];
        _bottomLabel.textAlignment = NSTextAlignmentCenter;
        _bottomLabel.text = LocalizedString(@"MINE_COPYRIGHT_VLIVESHOW");
        _bottomLabel.textColor = [UIColor grayColor];
    }
    return _bottomLabel;
}


@end
