//
//  SignNameViewController.m
//  VLiveShow
//
//  Created by SXW on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "SignNameViewController.h"
#import "HTTPFacade.h"
#import "AccountManager.h"
#import "VLSLiveHttpManager.h"
#import "UUIDShortener.h"
@interface SignNameViewController ()<UITextViewDelegate>

@property (nonatomic , strong ) UITextView *textView;//文本视
@property (nonatomic , strong ) UILabel *wordCountLabel;//字数Label

@end

@implementation SignNameViewController


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    if ([[AccountManager sharedAccountManager].account.intro  isEqualToString: @"  " ]|| [AccountManager sharedAccountManager].account.intro == nil) {
        _wordCountLabel.text = @"1/40";
        
    }
    _textView.text = [AccountManager sharedAccountManager].account.intro;
    
    [_textView becomeFirstResponder];
    _wordCountLabel.text = [NSString stringWithFormat:@"%lu/40" ,(unsigned long)_textView.text.length - 1];
    if ([_wordCountLabel.text isEqualToString:@"1/40"] || [_wordCountLabel.text isEqualToString:@"18446744073709551615/40"]) {
        _wordCountLabel.text = @"0/40";
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = LocalizedString(@"MINE_CHANGEPERSONALSIGN");
    self.view.backgroundColor = [UIColor whiteColor];
    [self createCustomRight];
    //初始化视图控件
    [self initViews];
}

- (void)createCustomRight{

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightBarButtonAction:)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    label.textColor = RGB16(COLOR_FONT_4A4A4A);
    label.userInteractionEnabled = YES;
    [label addGestureRecognizer:tap];
    label.text = LocalizedString(@"MINE_SAVE");
    label.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    label.textAlignment = NSTextAlignmentRight;
    
    //自定义的UIBarButtonItem
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:label];
    //设置右导航按钮
    self.navigationItem.rightBarButtonItem = rightItem;
}
- (void)initViews{
    
    _textView = [[UITextView alloc]initWithFrame:CGRectMake( 5 , 10, SCREEN_WIDTH - 10 , 150)];
    _textView.backgroundColor = [UIColor whiteColor];
    _textView.textColor = [UIColor blackColor];
    _textView.contentInset = UIEdgeInsetsMake(8.f, 0.f, -8.f, 0.f);
    self.automaticallyAdjustsScrollViewInsets = NO;
    _textView.layer.borderWidth = 1.0f;
    _textView.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
    _textView.clipsToBounds = YES;
    _textView.layer.cornerRadius = 5;
    _textView.font = [UIFont boldSystemFontOfSize:16];
    _textView.delegate = self;//代理
    _textView.returnKeyType = UIReturnKeyDone;
    [self.view addSubview:_textView];
    
    _wordCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetWidth(_textView.frame) - 60, CGRectGetHeight(_textView.frame) - 50, 60, 30)];
    _wordCountLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
    _wordCountLabel.text = @"0/40";
    _wordCountLabel.textAlignment = NSTextAlignmentCenter;
    _wordCountLabel.font = [UIFont systemFontOfSize:14];
    [_textView addSubview:_wordCountLabel];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_textView resignFirstResponder];
}


-(void)textViewDidChange:(UITextView *)textView{
    
    if (textView.text.length > 40 && textView.markedTextRange == nil) {
        
        [textView resignFirstResponder];
        textView.text = [textView.text substringToIndex:40];
    }
    if (textView.text.length == 0) {
        textView.text = @"";
    }
    if ([[AccountManager sharedAccountManager].account.intro isEqualToString:@" "]) {
        _wordCountLabel.text = [NSString stringWithFormat:@"%lu/40" ,(unsigned long)textView.text.length - 1];
        if ([_wordCountLabel.text isEqualToString:@"0/40"]) {
            [self.textView resignFirstResponder];
        }
        
    }else{
        _wordCountLabel.text = [NSString stringWithFormat:@"%lu/40" ,(unsigned long)textView.text.length];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@""]) {
        return YES;
    }
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    if (textView.text.length >= 40) {
        textView.text = [textView.text substringToIndex:40];
        _wordCountLabel.text = [NSString stringWithFormat:@"%@/40" ,@"40"];
        return NO;
        
    } else {
        
        return YES;
    }
}

#pragma mark ---rightBarButtonAction

- (void)rightBarButtonAction:(UIBarButtonItem *)sender{
    
    //释放第一响应者
    [self.textView resignFirstResponder];
    [self showProgressHUDWithStr:nil dimBackground:YES];
    [self uploadUserIntro];
 
}

#pragma mark - 上传用户信息
- (void)uploadUserIntro{
    
    AccountModel *accountModel = [[AccountModel alloc] init];
    if (_textView.text.length == 0) {
        _textView.text = @" ";
    }
    accountModel.intro = _textView.text;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.loadBlock = ^(id result){
        
    };
    callback.updateBlock = ^(id result){
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
        [self hideProgressHUDAfterDelay:0.0f];
        [self getUserInfo];
        
    };
    callback.errorBlock = ^(id result){
        
//        _textView.text = [AccountManager sharedAccountManager].account.intro;
        [self hideProgressHUDAfterDelay:0.0f];
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager postUserProfile:accountModel callback:callback];
}

#pragma mark - 获取用户信息的网络请求
- (void)getUserInfo {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [[VLSMessageManager sharedManager] getSelfProfile];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUserInfo" object:nil];
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [AccountManager getUserProfileCallback:callback];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
