//
//  VLSFansListViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSFansListViewController.h"
#import "MJRefreshAutoNormalFooter.h"
#import "MJRefreshGifHeader.h"
#import "VLiveShow-Swift.h"

@interface VLSFansListViewController ()
@property(nonatomic, assign) BOOL isMe;
@end

@implementation VLSFansListViewController

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar useLightTheme];
}

- (void)viewDidLoad {
    self.title = LocalizedString(@"MINE_FANS");
    [super viewDidLoad];
    self.isMe = self.userId == [AccountManager sharedAccountManager].account.uid;
    [self initView];
    [self getFansList];
    self.fansLabel.hidden = YES;
    self.page = 2;
    [self refreshDataWith:self.tableView];
}

- (void)initView{
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.fansLabel];
}

#pragma mark - 采集获取数据
//获取粉丝列表
- (void)getFansList{
    
    [self showProgressHUD];
    self.page = 2;
    [self.fanNumArray removeAllObjects];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){

        [self hideProgressHUDAfterDelay:0];
        [self.tableView.mj_header endRefreshing];
        [self.fanNumArray addObjectsFromArray: result];
        if (self.fanNumArray.count == 0) {
            self.fansLabel.hidden = NO;
        }else{
        
            self.fansLabel.hidden = YES;
        }
        [self.tableView reloadData];
        NSLog(@"小子fan%@",result);
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    NSInteger uid = self.userId;
    if (uid == 0)
    {
        uid = [AccountManager sharedAccountManager].account.uid;
    }
    [VLSLiveHttpManager getFanstList:1 userid: uid callback:callback];
}
#pragma mark - cell的代理方法
-(void)fansBtnViewBtn:(FansTableViewCell *)cell{
    
    if (!_isMe)
    {
        [self pushNextFNVC: cell];
        return;
    }
    
    [self showProgressHUD];
    NSIndexPath *indpath = [self.tableView indexPathForCell:cell];
    MineModel *model = [self.fanNumArray objectAtIndex:indpath.row];
    
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        [self hideProgressHUDAfterDelay:0];
        cell.focusBtn.selected = !cell.focusBtn.selected;
        if ([model.is_both_attend isEqualToString:@"Y"]) {
            model.is_both_attend = @"N";
        }else{
        
            model.is_both_attend = @"Y";
        }
    };
    callback.errorBlock = ^(id result)
    {
        [self hideProgressHUDAfterDelay:0];
        [self error:result];
    };
    
    if ([model.is_both_attend isEqualToString:@"N"]) {
        
        [[VLSUserTrackingManager shareManager] trackingUserAction:model.ID action:@"focus_on"];

        [VLSLiveHttpManager focusToUser:model.ID callback:callback];
    }
    else
    {
        [[VLSUserTrackingManager shareManager] trackingUserAction:model.ID action:@"focus_off"];

        [VLSLiveHttpManager cancelFocusToUser:model.ID callback:callback];
    }
}

-(void)pushNextFNVC:(FansTableViewCell *)cell{
    
    [self pushVC:self.fanNumArray and:cell with:self.tableView];
}

- (void)pushVC:(NSMutableArray*)array and:(UITableViewCell*)cell with:(UITableView*)tableView{
    
    NSIndexPath *indpath = [tableView indexPathForCell:cell];
    MineModel *model = [array objectAtIndex:indpath.row];
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    vlsVC.hidesBottomBarWhenPushed = YES;
    vlsVC.userId = model.ID;
    [[VLSUserTrackingManager shareManager] trackingViewprofile:model.ID];
    [self.navigationController pushViewController:vlsVC animated:YES];
}

-(void)blackBtnViewBtn:(FansTableViewCell *)cell{

}

#pragma mark - 代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _fanNumArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FansTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.delegate = self;
    if (self.fanNumArray.count > 0) {
        MineModel  *model = [self.fanNumArray objectAtIndex:indexPath.row];
        [cell configData:model];
        // Walk around for the user isn't self
        if (!_isMe)
        {
            cell.focusBtn.selected = NO;
            [cell.focusBtn setTitle: LocalizedString(@"MINE_CHECK") forState: UIControlStateNormal];
        }
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
}


#pragma mark - 控件初始化
-(UITableView *)tableView{
    
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[FansTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(NSMutableArray *)fanNumArray{

    if (_fanNumArray == nil) {
        _fanNumArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _fanNumArray;
}

-(UILabel *)fansLabel{

    if (_fansLabel == nil) {
        
        _fansLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, SCREEN_HEIGHT / 2 - 20 - 32, SCREEN_WIDTH - 40, 20)];
        _fansLabel.text = LocalizedString(@"MINE_GOTOROOM_TOSAVESENTIMENT");
        _fansLabel.textColor = RGB16(COLOR_FONT_979797);
        _fansLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        _fansLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _fansLabel;
}

- (void)refreshDataSource{
    
    [self getFansList];
}

- (void)loadMoreData{
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        [self.tableView.mj_footer endRefreshing];
        [self.fanNumArray addObjectsFromArray: result];
        if (self.fanNumArray.count == 0) {
            self.fansLabel.hidden = NO;
        }else{
            
            self.fansLabel.hidden = YES;
        }

        [self.tableView reloadData];
        self.page ++;
        NSLog(@"大子foc%@",result);
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    NSInteger uid = self.userId;
    if (uid == 0)
    {
        uid = [AccountManager sharedAccountManager].account.uid;
    }
    [VLSLiveHttpManager getFanstList:self.page userid: uid callback:callback];
}


@end
