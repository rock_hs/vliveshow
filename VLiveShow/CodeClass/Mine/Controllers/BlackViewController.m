//
//  BlackViewController.m
//  VLiveShow
//
//  Created by SXW on 16/6/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "BlackViewController.h"
#import "VLSLiveHttpManager.h"
#import "VLSOthersViewController.h"
#import "MJRefreshAutoNormalFooter.h"
#import "MJRefreshGifHeader.h"

@interface BlackViewController ()
{
    MJRefreshAutoNormalFooter *footer;
    MJRefreshGifHeader *header;
    NSInteger pageN;
    
}
@property (nonatomic , strong) UILabel *palceHolderLabel1;
@property (nonatomic , strong) UILabel *palceHolderLabel2;


@end

@implementation BlackViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar useLightTheme];
    [self getData];
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    [self.baseTable setTableFooterView:view];
    
    self.palceHolderLabel1.hidden = YES;
    self.palceHolderLabel2.hidden = YES;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = LocalizedString(@"MINE_BLACKFRIENDS");
    [self.view addSubview:self.baseTable];
    [self.view addSubview:self.palceHolderLabel1];
    [self.view addSubview:self.palceHolderLabel2];
    pageN = 2;
    [self refreshDataWith:self.baseTable];
}
- (void)getData
{
    pageN = 2;
    [self.dateArray removeAllObjects];
    [self showProgressHUD];
    ManagerCallBack *callback = [ManagerCallBack new];
    callback.updateBlock = ^(id result){
        [self.baseTable.mj_header endRefreshing];
        [self hideProgressHUDAfterDelay:.5];
        [self.dateArray addObjectsFromArray:result];
        if (self.dateArray.count == 0) {
            self.palceHolderLabel1.hidden = NO;
            self.palceHolderLabel2.hidden = NO;
            
        }else{
            
            self.palceHolderLabel1.hidden = YES;
            self.palceHolderLabel2.hidden = YES;
            
        }
        [self.baseTable  reloadData];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [VLSLiveHttpManager getuserBlackList:1 callback:callback];
    
}
#pragma mark- tableviewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dateArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FansTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"blackcell" forIndexPath:indexPath];
    
    cell.delegate = self;
    if (self.dateArray.count >0) {
        MineModel *model = [self.dateArray objectAtIndex:indexPath.row];
        [cell configBlackData:model];
        if (model.isblack == YES) {
            cell.focusBtn.layer.borderColor = [UIColor grayColor].CGColor;
        }
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

#pragma mark-FanstableviewCellDelegate
- (void)blackBtnViewBtn:(FansTableViewCell *)cell
{
    NSIndexPath *indpath = [self.baseTable indexPathForCell:cell];
    MineModel *model = [self.dateArray objectAtIndex:indpath.row];
    [self showProgressHUD];
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        
        model.isblack = YES;
        [self hideProgressHUDAfterDelay:0];
        [self.baseTable reloadData];
    };
    callback.errorBlock = ^(id result)
    {
        [self error:result];
    };
    
    if ( cell.focusBtn.selected == NO) {
        
        [[VLSUserTrackingManager shareManager] trackingUserAction:model.ID action:@"blacklist_off"];

        [VLSLiveHttpManager cancelBlack:model.ID callback:callback];
        
    }
    else
    {
        
        UserProfileViewController * vlsVC = [UserProfileViewController create];
        vlsVC.userId = model.ID;
        [[VLSUserTrackingManager shareManager] trackingViewprofile:model.ID];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vlsVC animated:YES];
    }
}

-(void)pushNextFNVC:(FansTableViewCell *)cell{
}

- (void)fansBtnViewBtn:(FansTableViewCell *)cell
{
    
}

- (UITableView*)baseTable
{
    if (_baseTable == nil) {
        _baseTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
        _baseTable.delegate = self;
        _baseTable.dataSource = self;
        [_baseTable registerClass:[FansTableViewCell class] forCellReuseIdentifier:@"blackcell"];
        _baseTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _baseTable;
}

-(UILabel *)palceHolderLabel1{
    
    if (_palceHolderLabel1 == nil) {
        
        _palceHolderLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(20, SCREEN_HEIGHT/ 2 - 80, SCREEN_WIDTH - 40, 18)];
        _palceHolderLabel1.textColor = RGB16(COLOR_FONT_979797);
        _palceHolderLabel1.font = [UIFont systemFontOfSize:12];
        _palceHolderLabel1.textAlignment = NSTextAlignmentCenter;
        _palceHolderLabel1.numberOfLines = 0;
        _palceHolderLabel1.text = LocalizedString(@"PUTSOMEONEINTHISLIST");
    }
    return _palceHolderLabel1;
}
-(UILabel *)palceHolderLabel2{
    
    if (_palceHolderLabel2 == nil) {
        
        _palceHolderLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_palceHolderLabel1.frame), SCREEN_WIDTH - 40, 18)];
        _palceHolderLabel2.textColor = RGB16(COLOR_FONT_979797);
        _palceHolderLabel2.font = [UIFont systemFontOfSize:12];
        _palceHolderLabel2.textAlignment = NSTextAlignmentCenter;
        _palceHolderLabel2.numberOfLines = 0;
        _palceHolderLabel2.text = LocalizedString(@"YOUWILLRECEIVENOTINGOFANYONE");
    }
    return _palceHolderLabel2;
}
- (NSMutableArray*)dateArray
{
    if (_dateArray == nil) {
        _dateArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _dateArray;
    
}
#pragma mark-上拉加在下拉刷新
- (void)loadMoreData
{
    [self showProgressHUD];
    ManagerCallBack *callback = [ManagerCallBack new];
    callback.updateBlock = ^(id result){
        
        [self.baseTable.mj_footer endRefreshing];
        [self hideProgressHUDAfterDelay:0];
        [self.dateArray addObjectsFromArray:result];
        
        if (self.dateArray.count == 0) {
            self.palceHolderLabel1.hidden = NO;
            self.palceHolderLabel2.hidden = NO;
            
        }else{
            
            self.palceHolderLabel1.hidden = YES;
            self.palceHolderLabel2.hidden = YES;
            
        }
        [self.baseTable  reloadData];
        
        pageN++;
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [VLSLiveHttpManager getuserBlackList:pageN callback:callback];
    
}
- (void)refreshDataSource
{
    [self getData];
    
}
@end
