//
//  SettingViewController.m
//  VLiveShow
//
//  Created by SXW on 16/6/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "SettingViewController.h"
#import "HTTPFacade.h"
#import "ManagerEvent.h"
#import "VLSLoginViewController.h"
#import "AppDelegate.h"
#import "BlackViewController.h"
#import "TiePhoneNumberController.h"
#import "VLSTeamMessageModel.h"
#import "VLSLiveRemindModel.h"
#import "VLSFansModel.h"
#import "VLSMessageManager.h"
#import "VLSNotFocusInvitationCell.h"
#import "VLSLiveHttpManager.h"
#import "VLSPwdVerifyViewController.h"
#import "VLSTestViewController.h"
#import "VLiveShow-Swift.h"

@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    BOOL push;
}
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) UILabel *cellLine;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic) BOOL isOP;
@property (nonatomic, strong) UIButton *tieButton;

@end

@implementation SettingViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    _myPhoneNumber = [AccountManager sharedAccountManager].account.mobilePhone;
    [_tableView reloadData];
    
    if (!push) {
        [self.navigationController setNavigationBarHidden:NO animated:animated];
    }
    [self chargeIsReceiveLiveNotice];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    push = NO;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (!push) {
        [self.navigationController setNavigationBarHidden:YES animated:animated];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = LocalizedString(@"MINE_SETTING");
    [self createDataSource];
    [self createTableView];
    [self.tableView registerClass:[RemindTableViewCell class] forCellReuseIdentifier:@"RemindTableViewCell"];
    [self.tableView registerClass:[VLSNotFocusInvitationCell class] forCellReuseIdentifier:@"VLSNotFocusInvitationCell"];
    [self setNavi];
}

- (void)setNavi {
    NSString *version = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    if ([version containsString:@"dev"]) {
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"系统设置" style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
    }
}

- (void)setting {
    VLSTestViewController *vc = [VLSTestViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)popSuper{
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - 创建数据源
- (void)createDataSource{
    
    _array = @[@[LocalizedString(@"MINE_PHONENUMBER")],@[LocalizedString(@"MINE_BLACKFRIENDS"),LocalizedString(@"MINE_DIRECTINFOREMIND"),LocalizedString(@"MINE_NO_FOCUSED_INVITATION")/*,LocalizedString(@"MINE_NO_FOCUSED_CHAT")*/], @[LocalizedString(@"VLS_FEEDBACK_TITLE"),LocalizedString(@"MINE_ABOUTUS")],@[LocalizedString(@"")]];
}

//表格创建
- (void)createTableView{
    //设置组头后，如果style为UITableViewStylePlain， 拖动时，组头随着某一分组一起移动
    _tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    
    //数据源代理
    _tableView.dataSource = self;
    //tableview代理
    _tableView.delegate = self;
    
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [UIView new];
    
}


#pragma mark - 代理方法
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return _array.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_array[section] count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    
    cell.textLabel.text = _array[indexPath.section][indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        cell.userInteractionEnabled = YES;
        _tieButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_tieButton addTarget:self action:@selector(btnTieClick) forControlEvents:UIControlEventTouchUpInside];
        _tieButton.frame = CGRectMake(SCREEN_WIDTH - 80, 18, 65, 20);
        [cell.contentView addSubview:_tieButton];
        
        cell.imageView.image = [UIImage imageNamed:@"phone_gray"];
        cell.detailTextLabel.textColor = [UIColor redColor];
        if (_myPhoneNumber == nil) {
            cell.detailTextLabel.text = LocalizedString(@"MINE_NOTBINDING");//未绑定
        }else
        {
            NSMutableString *str = [NSMutableString stringWithFormat:@"%@",_myPhoneNumber];
            NSRange range = NSMakeRange(3, 4);
            NSString *str1 = [str stringByReplacingCharactersInRange:range withString:@"****"];
            NSString *phStr = [NSString stringWithFormat:@"%@  %@",str1, LocalizedString(@"MINE_HAVEBINGDE")];
            NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:phStr];
            [attri addAttribute:NSForegroundColorAttributeName value:RGB16(COLOR_BG_9B9B9B) range:NSMakeRange(0, 11)];
            cell.detailTextLabel.attributedText = attri;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }else if (indexPath.section == 1 && (indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3)){
        
        if (indexPath.row == 1) {
            
            RemindTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RemindTableViewCell" forIndexPath:indexPath];
            cell.mySwitch.on = ![[NSUserDefaults standardUserDefaults] boolForKey:@"isOP"];
            cell.nameLabel.text = LocalizedString(@"MINE_DIRECTINFOREMIND");
            cell.nameLabel.font = [UIFont systemFontOfSize:16];
        }else if(indexPath.row == 2 /*|| indexPath.row == 3*/)
        {
            VLSNotFocusInvitationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VLSNotFocusInvitationCell" forIndexPath:indexPath];
            cell.nameLabel.font = [UIFont systemFontOfSize:16];
            //if (indexPath.row == 2) {
            cell.switchInvite.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"isInvite"];
            cell.nameLabel.text = LocalizedString(@"MINE_NO_FOCUSED_INVITATION");
            /*}else{
            
                cell.nameLabel.text = LocalizedString(@"MINE_NO_FOCUSED_CHAT");
            }*/
        }
        return cell;
    } else if (indexPath.section == 3 && indexPath.row == 0) {
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        UILabel *exitLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 18, SCREEN_WIDTH - 100, 20)];
        exitLabel.text = LocalizedString(@"MIEN_EXITLOGIN");
        exitLabel.font = [UIFont systemFontOfSize:16];
        exitLabel.textAlignment = NSTextAlignmentCenter;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        [cell.contentView addSubview:exitLabel];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}
//返回组头的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20 + 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    push =YES;
    if (indexPath.section == 0 && indexPath.row == 0) {
     
    }else if (indexPath.section == 1 && indexPath.row == 0){
        
        BlackViewController *black = [[BlackViewController alloc] init];
        [self.navigationController pushViewController:black animated:YES];
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        VLSFeedbackViewController* feedbackVC = [[VLSFeedbackViewController alloc] init];
        [self.navigationController pushViewController:feedbackVC animated:YES];
    } else if (indexPath.section == 2 && indexPath.row == 1) {
        AboutUsController *acVC = [[AboutUsController alloc] init];
        [self.navigationController pushViewController:acVC animated:YES];
    }
    else if (indexPath.section == 3 && indexPath.row == 0)
    {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"AREYOUSUERTOEXIT") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            AccountModel* account = [AccountManager sharedAccountManager].account;
            if (account) {
                ManagerCallBack *callback = [[ManagerCallBack alloc] init];
                callback.updateBlock = ^(id result){
                    
                    
                    ManagerEvent *event = [ManagerEvent toast:nil title:LocalizedString(@"SET_LOGIN_OUT_SUCCESS")];
                    [self success:event];
                    
                    [self performSelector:@selector(backLogin) withObject:nil afterDelay:2.2];
                    
                };
                callback.errorBlock = ^(id result){
                    
                    if ([result isKindOfClass:[ManagerEvent class]]) {
                        [self error:result];
                    }
                };
                [AccountManager logOutCallback:callback];
            }
            
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        
        [okAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        [okAction setValue:[UIFont boldSystemFontOfSize:14] forKey:@"titleFont"];
        
        [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

- (void)backLogin
{
    [[VLSMessageManager sharedManager] logoutIM];
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [app showLoginController];
    
    [[VLSUserTrackingManager shareManager] trackingUserExitLiveRoom:@"logout"];
}
- (void)goBackSS:(id)sender{
    
    [self.navigationController popViewControllerAnimated:NO];
    
}

- (void)btnTieClick{
    
    if (self.myPhoneNumber) {
        VLSPwdVerifyViewController *acc = [[VLSPwdVerifyViewController alloc] init];
        acc.currentBindNum = self.myPhoneNumber;
        [self.navigationController pushViewController:acc animated:YES];
    } else {
        TiePhoneNumberController *acc = [[TiePhoneNumberController alloc] init];
        [self.navigationController pushViewController:acc animated:YES];
        
    }
    
}

- (void)chargeIsReceiveLiveNotice{

    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(NSDictionary* result)
    {
        NSDictionary *dic = result[@"UserExtension"];
        NSLog(@"直播提醒的状态:%@",dic[@"enableLiveNotice"]);
        if ([dic[@"enableLiveNotice"] isKindOfClass:[NSNull class]] || [dic[@"enableLiveNotice"] boolValue] == YES) {
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isOP"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isOP"];
        }
        
        if ([dic[@"enableNotFollowInvite"] isKindOfClass:[NSNull class]] || [dic[@"enableNotFollowInvite"] boolValue] == YES) {
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isInvite"];
        }else{
        
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isInvite"];
        }
    };
    callback.errorBlock = ^(id result)
    {
    };
    
    [VLSLiveHttpManager getLiveNotice:callback];
}


@end
