//
//  SettingViewController.h
//  VLiveShow
//
//  Created by SXW on 16/6/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "VLSPhoneLoginViewController.h"
#import "AboutUsController.h"
#import "RemindTableViewCell.h"
@interface SettingViewController : VLSBaseViewController

@property (nonatomic, copy) NSString *myPhoneNumber;
@property (nonatomic, copy)NSString *bindNewNum;

@end
