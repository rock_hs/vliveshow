//
//  VLSMineViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMineViewController.h"
#import "MainInformationView.h"
#import "VLSMiddleView.h"
#import "VLSFocusListViewController.h"
#import "VLSFansListViewController.h"
#import "SettingViewController.h"
#import "MJExtension.h"
#import "VLSUserProfile.h"
#import "MineModel.h"
#import "UIImageView+WebCache.h"
#import "VLSMessageManager.h"
#import "AccountManager.h"
#import "VLSLiveHttpManager.h"
#import "VLSOthersViewController.h"
#import "SettingViewController.h"
#import "VLSPhotoSelectView.h"
#import "AJPhotoPickerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "UUIDShortener.h"
#import "UIImage+new.h"
#import "MJRefresh.h"
#import "MySettingViewController.h"
#import "ShareLiveOfView.h"
#import "HostApplicationViewController.h"
#import "WithdrawalVC.h"
#import "VLiveShow-Swift.h"

@interface VLSMineViewController ()<MainInformationViewDelegate,VLSMiddleViewDelegate,VLSPhotoSelectViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource, VLSShareSheetViewDelegate>{
    
    NSTextAttachment *attch;
    NSTextAttachment *attchLevel;
    BOOL doNotRefresh;
}

@property (nonatomic, strong) UITableView *tableView;
//@property (nonatomic, strong) MainInformationView *mainView;
@property (nonatomic, strong) VLSMiddleView *middleView;
@property (nonatomic, strong) VLSUserInfoViewModel *Vmodel;
@property (nonatomic, strong) MineModel *mModel;
@property (nonatomic ,strong) VLSPhotoSelectView *photoSelectView;
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, copy) NSString *areaString;

@property (nonatomic, strong) NSArray *isHostIconArray;
@property (nonatomic, strong) NSArray *isNotHostIconArray;
@property (nonatomic, strong) NSArray *isHostTitleArray;
@property (nonatomic, strong) NSArray *isNotHostTitleArray;

//@property (nonatomic, strong) UIView *contentView;
@property(nonatomic, strong) ProfileInfoViewController* profileInfoViewController;

@property (nonatomic, strong) UILabel *line;

@end

@implementation VLSMineViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[self reloadNameInformaton];
    if (!doNotRefresh)
    {
        [self getMineInfo];
    }
    doNotRefresh = NO;
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self loadAllView];
    //[self loadCacheData];
    [self getMineInfo];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUserInfo) name:@"UpdateUserInfo" object:nil];
    NSLog(@"空格Space%f", [self getWidthOfNameLabel:@"  " withFont:20 withSize:20]);
}
#pragma mark - 加载视图
-(void)loadAllView
{
    self.profileInfoViewController = [[UIStoryboard storyboardWithName: @"ProfileInfo" bundle: nil] instantiateInitialViewController];
    self.profileInfoViewController.view.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 278);
    self.profileInfoViewController.profileView.editable = YES;
    WS(ws)
    self.profileInfoViewController.didPressAvatarBlock = ^()
    {
        [ws.photoSelectView showPhotoView:YES animation:YES];
    };
    
    [self addChildViewController: self.profileInfoViewController];
    [self.tabBarController.view addSubview:self.photoSelectView];
    //[self.contentView addSubview:self.middleView];
    [self.view addSubview:self.tableView];
    [self createDataSource];
    
    UIButton* shareBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareBt setImage:[[UIImage imageNamed:@"share_gray"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    shareBt.frame = CGRectMake(CGRectGetWidth(self.profileInfoViewController.view.frame) - 36, 30, 20, 20);
    [shareBt addTarget:self action:@selector(shareAction) forControlEvents:UIControlEventTouchUpInside];
    shareBt.tintColor = RGB16(COLOR_FONT_FF1130);
    [self.profileInfoViewController.view addSubview:shareBt];
}

#pragma mark - tableview数据源
- (void)createDataSource{
#ifdef MINI_VERSION
    _isHostIconArray = @[@[/*@"ic_level",*/@"ic_leaderboard"],@[@"ic_vzuan"/*,@"ic_vpiao_gray"*/],@[@"ic_setting"]];
    _isHostTitleArray = @[@[/*@"我的等级",*/LocalizedString(@"V_TICKETRANGE")],@[LocalizedString(@"V_MYZUAN")/*,LocalizedString(@"V_MYTICKET")*/],@[LocalizedString(@"MINE_SETTING")]];
#else 
    _isHostIconArray = @[@[/*@"ic_level",*/@"ic_leaderboard"],@[@"ic_vzuan",@"ic_vpiao_gray"],@[@"ic_setting"]];
    _isHostTitleArray = @[@[/*@"我的等级",*/LocalizedString(@"V_TICKETRANGE")],@[LocalizedString(@"V_MYZUAN"),LocalizedString(@"V_MYTICKET")],@[LocalizedString(@"MINE_SETTING")]];
#endif

    _isNotHostIconArray = @[/*@"ic_level",*/@"ic_vzuan",@"ic_zhubo",@"ic_setting"];
    _isNotHostTitleArray = @[/*@"我的等级",*/LocalizedString(@"V_MYZUAN"),LocalizedString(@"V_ANCHORREQUEST"),LocalizedString(@"MINE_SETTING")];
}


//获取主界面数据
- (void)getMineInfo{
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        _Vmodel = result;
        self.profileInfoViewController.userProfile = _Vmodel;
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [VLSLiveHttpManager getUserInfo:[NSString stringWithFormat:@"%lu",(long)[AccountManager sharedAccountManager].account.uid] callback:callback];
}

#pragma mark - 简化代码方法
- (UIImage*)addText:(NSString*)text andImage:(UIImage*)img{
    //开启位图上下文
    UIGraphicsBeginImageContextWithOptions(img.size, NO, 0);
    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
    [text drawInRect:CGRectMake(0+img.size.width/2, 2, 30, img.size.height) withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10],NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIImage*currentImage=UIGraphicsGetImageFromCurrentImageContext();
    //关闭位图上下文
    UIGraphicsEndImageContext();
    return currentImage;
}

- (UIImage*)getImageFromNSInteger:(NSInteger)levelNum withLevelString:(NSString*)levelString{
    
    if (levelNum >= 1 && levelNum <=9) {
        UIImage *img = [self addText:[NSString stringWithFormat:@" %ld",levelNum] andImage:[UIImage imageNamed:levelString]];
        return img;
    }else{
        
        UIImage *img = [self addText:[NSString stringWithFormat:@"%ld",levelNum] andImage:[UIImage imageNamed:levelString]];
        return img;
    }
}

//根据内容计算按钮长度
- (CGFloat)getWidthOfNameLabel:(NSString*)aString withFont:(CGFloat)fontSize withSize:(CGFloat)fontHeight{
    CGSize titleSize = [aString sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(MAXFLOAT, fontHeight)];
    return titleSize.width;
}

#pragma mark- 自定义的代理方法
-(void)imageViewBtn{
    
    [self.photoSelectView showPhotoView:YES animation:YES];
}

-(void)setButton{
    
    MySettingViewController *mvc = [[MySettingViewController alloc] init];
    mvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mvc animated:YES];
}

-(void)pushToFocusList{
    
    VLSFocusListViewController *vfc = [VLSFocusListViewController new];
    vfc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vfc animated:YES];
}

-(void)pushToFansList{
    
    VLSFansListViewController *vfc = [VLSFansListViewController new];
    vfc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vfc animated:YES];
}

#pragma mark - VLSPhotoSelectViewDelegate
- (void)jumpToSystemPhotoAlbum {
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void)jumpToSystemCamera {
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void)closePhotoSelectView {
    
}

#pragma mark - UIImagePickerDelegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *) picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo {
    if (!error) {
        NSLog(@"保存到相册成功");
    }else{
        NSLog(@"保存到相册出错%@", error);
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    NSData *imageData = UIImagePNGRepresentation(image);
    [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"uploadImage"];
    //self.mainView.userIcon.image = image;
    
    [self uploadUserImage: image];
    [self.photoSelectView showPhotoView:NO animation:YES];
    doNotRefresh = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - 上传用户信息
- (void)uploadUserImage: (UIImage*) uploadImage{
    [self showProgressHUD];
    
    NSData *data = UIImageJPEGRepresentation(uploadImage, 0.2);
    PostFileModel *fileModel = [[PostFileModel alloc] init];
    fileModel.fileName = [NSUUID UUID].shortUUIDString;
    fileModel.fileArray = @[data];
    AccountModel *accountModel = [[AccountModel alloc] init];
    accountModel.fileModel = fileModel;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.loadBlock = ^(id result){
        
    };
    callback.updateBlock = ^(id result){
        
        [self hideProgressHUDAfterDelay:0];
        [[VLSMessageManager sharedManager] getSelfProfile];
        NSString *avurl = [AccountManager sharedAccountManager].account.avatars;
        
        [SDWebImageManager.sharedManager downloadImageWithURL:[NSURL URLWithString:avurl] options:SDWebImageRefreshCached progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            self.profileInfoViewController.profileView.avatarImage = uploadImage;
            [[NSUserDefaults standardUserDefaults] setObject: data forKey:@"uploadImage"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }];
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
            NSString *avurl = [AccountManager sharedAccountManager].account.avatars;
            [SDWebImageManager.sharedManager downloadImageWithURL:[NSURL URLWithString:avurl] options:SDWebImageRefreshCached progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
                [[NSUserDefaults standardUserDefaults] setObject: data forKey:@"uploadImage"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.profileInfoViewController.profileView.avatarImage = uploadImage;
                //self.mainView.userIcon.image = image;
            }];
        }
    };
    
    [AccountManager postUserProfile :accountModel callback:callback];
}

#pragma mark - 获取用户信息的网络请求
- (void)getUserInfo {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        [[VLSMessageManager sharedManager] getSelfProfile];
        NSString *url = [AccountManager sharedAccountManager].account.avatars;
        UIImage *image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"uploadImage"]];
        if (image == nil) {
            
            //[self.mainView.userIcon sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:ICON_PLACEHOLDER];
        }else{
            
            //[self.mainView.userIcon sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:image];
        }
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [AccountManager getUserProfileCallback:callback];
}

#pragma mark - UITabeViewDelegate和UITableviewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([AccountManager sharedAccountManager].account.isHost == NO) {
        return 1;
    }else{
        return _isHostTitleArray.count;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([AccountManager sharedAccountManager].account.isHost == NO) {
        return 3;
    }else{
        return [[_isHostTitleArray objectAtIndex:section] count];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = [UIColor colorFromHexRGB:@"4A4A4A"];
    _line = [[UILabel alloc] initWithFrame:CGRectMake(0, 59, SCREEN_WIDTH, 1)];
    _line.backgroundColor = RGB16(COLOR_LINE_F0F0F0);
    [cell.contentView addSubview:self.line];
    
    if ([AccountManager sharedAccountManager].account.isHost == NO) {
        
        cell.imageView.image = [UIImage imageNamed:_isNotHostIconArray[indexPath.row]]   ;
        cell.textLabel.text = _isNotHostTitleArray[indexPath.row];
    }else{
        
        cell.imageView.image = [UIImage imageNamed:_isHostIconArray[indexPath.section][indexPath.row]];
        cell.textLabel.text = _isHostTitleArray[indexPath.section][indexPath.row];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 8;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 0.00000000000001f;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor colorFromHexRGB:@"EEEEEE"];
    return label;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([AccountManager sharedAccountManager].account.isHost == NO) {
        if (indexPath.row == 0)
        {
            UIViewController* vc = [[UIStoryboard storyboardWithName: @"Payment" bundle: nil] instantiateInitialViewController];
            if (vc != nil)
            {
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController: vc animated: YES];
            }
        }else if (indexPath.row == 1){
        
            self.tabBarController.selectedIndex = 2;
            
        }else if (indexPath.row == 2) {
            
            SettingViewController *svc = [SettingViewController new];
            svc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:svc animated:YES];
        }
    }else{
        
        if (indexPath.section == 1)
        {
            if (indexPath.row == 0)
            {
                UIViewController* vc = [[UIStoryboard storyboardWithName: @"Payment" bundle: nil] instantiateInitialViewController];
                if (vc != nil)
                {
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController: vc animated: YES];
                }
            }else if (indexPath.row == 1){
            
                WithdrawalVC *mvc = [[WithdrawalVC alloc] init];
                mvc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:mvc animated:YES];
            }
        }
        else if (indexPath.section == 2) {
            SettingViewController *svc = [SettingViewController new];
            svc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:svc animated:YES];
        } else if (indexPath.section == 0&&indexPath.row == 0)
        {
            RankingListViewController *ranklist = [RankingListViewController  new];
            ranklist.hidesBottomBarWhenPushed = YES;
            NSInteger userid = [AccountManager sharedAccountManager].account.uid;

            ranklist.userId = [NSString stringWithFormat:@"%ld",userid];
            [self.navigationController pushViewController:ranklist animated:YES];
        }
    }
}

-(VLSPhotoSelectView *)photoSelectView{
    
    if (!_photoSelectView) {
        
        _photoSelectView = [[VLSPhotoSelectView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _photoSelectView.delegate = self;
        _photoSelectView.photoLabel.text = LocalizedString(@"SET_HEADIMAEG");
        [_photoSelectView showPhotoView:NO animation:NO];
        
    }
    [self.tabBarController.view bringSubviewToFront:_photoSelectView];
    return _photoSelectView;
}

- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        self.imagePicker = [[UIImagePickerController alloc] init];
        self.imagePicker.delegate = self;
        self.imagePicker.allowsEditing = YES;
        
    }
    return _imagePicker;
}

-(UITableView *)tableView{
    
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,-20, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - 20) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.tableHeaderView = self.profileInfoViewController.view;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (void)reloadUserInfo
{
    [self.profileInfoViewController updateUserProfileView];
}

//分享
- (void)shareAction {
    [[VLSShareSheetView shared] shareSheetWithView:self.view item:nil];
    [VLSShareSheetView shared].delegate = self;
}

#pragma mark -- VLSShareSheetViewDelegate
- (void)xs_actionSheet:(ShareActionSheet *)actionSheet clickedButtonIndex:(NSInteger)index {
    if (index > 5 || ![ShareActionSheet isInstallation:index]) {
        [self showAlterviewController:LocalizedString(@"SHARE_HTML_NO_INSTALL_APP")];
    } else {
        NSString* title = [NSString stringWithFormat:@"%@-%@", LocalizedString(@"SHARE_HTML_URL_TITLE"), [NSString stringWithFormat:LocalizedString(@"COURSE_LIST_SHARE_CONTENT"), self.Vmodel.userName]];
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:self.Vmodel.userFaceURL, @"coverImageURL", title, @"content", title, @"title", @NO, @"overrideTitleInWXTimeline", [NSString stringWithFormat:@"%@/pages/course/list.html?id=%@", VLIVESHOW_HOST, self.Vmodel.userId], @"urlString", nil];
        [self shareToPlatform:index messageObject:dict completion:^(NSDictionary *item) {
            [actionSheet dismiss];
            [self shareTrackingLiveAfter:self.Vmodel.userId Index:index];
        }];
    }
}

@end
