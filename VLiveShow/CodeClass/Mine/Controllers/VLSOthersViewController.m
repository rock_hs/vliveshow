//
//  VLSOthersViewController.m
//  VLiveShow
//
//  Created by sp on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//
#import "VLSOthersViewController.h"
#import "MJExtension.h"
#import "VLSUserProfile.h"
#import "MineModel.h"
#import "UIImageView+WebCache.h"
#import "VLSMessageManager.h"
#import "AccountManager.h"
#import "VLSLiveHttpManager.h"
#import "VLSShowViewController.h"
#import "SettingViewController.h"
#import "VLSSegmentView.h"
#import "VLSFansViewController.h"
#import "VLSLittleNavigationController.h"
#import "MJRefreshAutoNormalFooter.h"
#import "MJRefreshGifHeader.h"
#import "VLSSonTableCell.h"
//选择View的高度
static const CGFloat SelectViewHeight = 50;

@interface VLSOthersViewController ()<VLSSegmentViewDelegate,MainInformationViewDelegate>
{
    UICollectionView *mainCollectionView;
    NSTextAttachment *attch;
    NSTextAttachment *attchLevel;
    int acceptCount ;
    UIView*leftView;
    UIView*rightView;
    UIView *lineView;
    UIView *topLine;
    UIImageView *leftImageView;
    UIImageView *rightImageView;
    UILabel *leftLabel;
    UILabel *rightLabel;
    UIView *leftContentView;
    UIView *rightContentView;
    UIButton*getBlackBut;
    UIButton *focureBut;
    int numberOfBack;
    MJRefreshAutoNormalFooter *footer;
    MJRefreshGifHeader *header;
    MJRefreshAutoNormalFooter *fansFooter;
    MJRefreshGifHeader *fansHeader;

    NSInteger focuesN;
    NSInteger fanceN;

}
@property (nonatomic, strong) UIView *navView;
@property (nonatomic, strong) UILabel *navLab;
@property (nonatomic, strong) UILabel *lineLable;
@property (nonatomic, strong) UIScrollView *baseScrollerView;
@property (nonatomic, strong) UITableView *focusTableView;
@property (nonatomic, strong) UITableView *fansTableView;
@property (nonatomic, strong) UITableView *showingTableView;
@property (nonatomic, strong) VLSSegmentView *segmentView;
//@property (nonatomic,strong) UILabel *playRoomLab;
@property (nonatomic, strong) MainInformationView *mainView;
@property (nonatomic, strong) NSMutableArray *infoMUArray;
@property (nonatomic, strong) VLSUserInfoViewModel *Vmodel;
@property (nonatomic, strong) MineModel *mModel;
/** 记录scrollView上次偏移的Y距离 */
@property (nonatomic, assign) CGFloat scrollY;
/** 记录scrollView上次偏移X的距离 */
@property (nonatomic, assign) CGFloat scrollX;
@property (nonatomic,strong)NSMutableArray *backContentArray;//存放返回id
@property (nonatomic, strong) NSMutableArray *focNumArray;//关注数组
@property (nonatomic, strong) NSMutableArray *fanNumArray;//粉丝数组
@property (nonatomic, strong) UILabel *cellLine;
@property (nonatomic, strong) UIButton* numBt; //投票数

@end

@implementation VLSOthersViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    numberOfBack = 0;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    for (UINavigationController *nav in self.parentViewController.childViewControllers) {
        if ([nav isKindOfClass:[VLSLittleNavigationController class]]) {
            nav.view.frame = CGRectMake(0, SC_HEIGHT - 275, SC_WIDTH, 275);
        }
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;

    acceptCount = 0;
    [self getUserInformation:1];
    [self getFocusList];
    [self getFansList];
    //等待
    [self loadAllView];
        //添加上拉加载
    fansFooter = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [self loadMoreFances];
        }];
    
    self.fansTableView.mj_footer = fansFooter;
    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [self loadMoreFocuesN];
        }];
    self.focusTableView.mj_footer = footer;
    [footer setTitle:LocalizedString(@"LIVE_PULL_LOAD_MORE") forState:MJRefreshStateRefreshing];
}

#pragma mark -上拉加载更多
- (void)loadMoreFocuesN
{

    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        [self.focusTableView.mj_footer endRefreshing];
        NSArray *ary = result;
        if (ary.count>0) {
            [self.focNumArray addObjectsFromArray:result];

        }
        [_focusTableView reloadData];
        focuesN++;
        
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [VLSLiveHttpManager getAttentList:focuesN userid:[_userId integerValue] callback:callback];
}

- (void)loadMoreFances
{
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        [self hideProgressHUDAfterDelay:0];
        [self.fansTableView.mj_footer endRefreshing];
        [self.fanNumArray addObjectsFromArray:result];
        
        [_fansTableView reloadData];
        fanceN++;

        NSLog(@"fan%@",result);
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [VLSLiveHttpManager getFanstList:fanceN userid:[_userId integerValue] callback:callback];


}

-(void)loadAllView
{
    
    self.mainView.delegate = self;
    [self.view addSubview:self.baseScrollerView];
    [self.baseScrollerView addSubview:self.focusTableView];
    [self.baseScrollerView addSubview:self.fansTableView];
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.mainView];
    [self.view addSubview:self.segmentView];
    [self.view addSubview:self.navView];
    [self.view addSubview:self.lineLable];
    [self.view addSubview:self.xiuhaoNum];
    [self.view addSubview:self.topxiuhaoNum];
    [self.navView addSubview:self.numBt];
    [self.navView addSubview:self.navLab];
    
}

//获取用户信息
- (void)getUserInformation:(NSInteger)tag
{
    NSInteger userid = [AccountManager sharedAccountManager].account.uid;
    NSString *uid = [NSString stringWithFormat:@"%ld",userid];
    AccountModel *selfModel = [AccountManager sharedAccountManager].account;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        _Vmodel = result;
        if (tag == 3) {
            if (_Vmodel.fansNumber <= 10) {
                [self getFansList];
            }else if (_Vmodel.fansNumber >10&&fanceN >=_Vmodel.fansNumber/10+2)
            {
                
                MineModel *mmodel = [MineModel new];
                mmodel.nickName = selfModel.nickName;
                mmodel.ID = uid;
                mmodel.gender = selfModel.gender;
                mmodel.intro = selfModel.intro;
                mmodel.userFaceURL = selfModel.avatars;
                if ([_Vmodel.isAttent boolValue] == YES)
                {
                    [self.fanNumArray addObject:mmodel];
                    
                }else
                {
                    for (MineModel *model in self.fanNumArray) {
                        if ([model.ID isEqualToString:uid]) {
                            [self.fanNumArray removeObject:model];
                            break;
                        }
                    }
                }
                [self.fansTableView reloadData];
            }
        }else if (tag == 1)
        {
            focuesN = 2;
            fanceN = 2;
        
        }
        
        if ([_Vmodel.isAttent boolValue]== YES)
        {
            NSMutableAttributedString *attri2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",LocalizedString(@"MINE_FOCUSED")]];
            NSTextAttachment *attach2 = [[NSTextAttachment alloc] init];
            attach2.bounds = CGRectMake(0, -3, 16, 16);
            attach2.image = [UIImage imageNamed:@"ic_follow_pre"];
            NSAttributedString *string2 = [NSAttributedString attributedStringWithAttachment:attach2];
            [attri2 insertAttributedString:string2 atIndex:0];
            rightLabel.attributedText = attri2;
            rightLabel.textColor = [UIColor colorFromHexRGB:@"BEBEBE"];
        }else
        {
            NSMutableAttributedString *attri2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",LocalizedString(@"MINE_TOFOCUSED")]];
            NSTextAttachment *attach2 = [[NSTextAttachment alloc] init];
            attach2.bounds = CGRectMake(0, -3, 16, 16);
            attach2.image = [UIImage imageNamed:@"ic_follow_nor"];
            NSAttributedString *string2 = [NSAttributedString attributedStringWithAttachment:attach2];
            [attri2 insertAttributedString:string2 atIndex:0];
            rightLabel.attributedText = attri2;
            rightLabel.textColor = RGB16(COLOR_FONT_FF1130);
        }
        _segmentView.focNumLab.text = [NSString stringWithFormat:@"%ld",_Vmodel.followNumber];
        _segmentView.fansNumLab.text = [NSString stringWithFormat:@"%ld",_Vmodel.fansNumber];
        [_mainView.userIcon sd_setImageWithURL:[NSURL URLWithString:_Vmodel.userFaceURL] placeholderImage:ICON_PLACEHOLDER options:SDWebImageRefreshCached];
        _navLab.text = _Vmodel.userName;
        if (_Vmodel.userName == nil) {
             _mainView.nameLab.text = LocalizedString(@"MINE_DONOTSETNICKNAME");
        }else
        {
            [self reloadNameInformaton];
        }
        
        if (_Vmodel.userContent == nil || [_Vmodel.userContent isEqualToString:@""] || [_Vmodel.userContent isEqualToString:@" "]) {
            _mainView.simpleLab.text = LocalizedString(@"MINE_USER_PROFILE_LAZY_NOTHING");
        }else{
            _mainView.simpleLab.text = _Vmodel.userContent;//个性签名
        }
        //用户位置
        if (_Vmodel.userLocation == nil) {
            if (_Vmodel.userId == nil) {
                _mainView.numLabel.text = @"";
            }else{
            
                _mainView.numLabel.frame = CGRectMake(20, CGRectGetMaxY(_mainView.simpleLab.frame) - 3, SC_WIDTH - 40, 20);
                _mainView.numLabel.text = [NSString stringWithFormat:@"ID %@",_Vmodel.userId];
            }
        }else
        {
            if (_Vmodel.userId == nil) {
                
                _mainView.numLabel.text = _Vmodel.userLocation;
            }else{
            
                NSMutableAttributedString *nameAttri = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"ID %@  ",_Vmodel.userId]];
                attch = [[NSTextAttachment alloc] init];
                attch.bounds = CGRectMake(0, -3, 1, 15);
                attch.image = [UIImage ImageFromColor:[UIColor lightGrayColor]];
                NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:attch];
                [nameAttri appendAttributedString:string];
                NSAttributedString *stringArea = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@%@",LocalizedString(@"COMEFROM"),_Vmodel.userLocation]];
                [nameAttri appendAttributedString:stringArea];
                _mainView.numLabel.attributedText = nameAttri;
            }
        }
        //更新投票数
        NSString* userId = _Vmodel.userId == nil? @"" : _Vmodel.userId;
        NSString* userName = _Vmodel.userName == nil? @"" : _Vmodel.userName;
        [_mainView updateVoteModel:[NSDictionary dictionaryWithObjectsAndKeys:userId, @"votedId", userName, @"nickname", _Vmodel.residueVotes, @"residueVotes", _Vmodel.totalVotes, @"totalVotes", nil]];
        [self.numBt setTitle:_Vmodel.totalVotes.stringValue forState:UIControlStateNormal];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
        [VLSLiveHttpManager getUserInfo:_userId callback:callback ];
        [self getPlayerRoomid];
}
//获取主播roomid
- (void)getPlayerRoomid
{
    ManagerCallBack *callback1 = [[ManagerCallBack alloc] init];
    callback1.updateBlock = ^(id result){
        
        if ([result isKindOfClass:[NSNull class]]) {
            
            self.mainView.setBut.hidden = YES;
            self.mainView.playRoomLab.hidden = YES;
            self.mainView.topSetBut.hidden = YES;
        }else
        {
            
            self.mainView.setBut.hidden = NO;
            self.mainView.playRoomLab.hidden = NO;
            self.mainView.topSetBut.hidden = NO;
            self.roomID = result;
        }
    };
    callback1.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [VLSLiveHttpManager getRoomId:_userId callback:callback1];
}

//获取关注列表
- (void)getFocusList{
    
    [self showProgressHUD];
    [self.focNumArray removeAllObjects];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        [self.focusTableView.mj_header endRefreshing];
        [self hideProgressHUDAfterDelay:0];
        [self.focNumArray addObjectsFromArray:result];
        [_focusTableView reloadData];
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];

        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [VLSLiveHttpManager getAttentList:1 userid:[_userId integerValue] callback:callback];
    
}

//获取粉丝列表
- (void)getFansList{
    [self showProgressHUD];
    [self.fanNumArray removeAllObjects];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self.fansTableView.mj_header endRefreshing];
        [self hideProgressHUDAfterDelay:0];
        [self.fanNumArray addObjectsFromArray:result];
        [_fansTableView reloadData];
        NSLog(@"fan%@",result);
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
 [VLSLiveHttpManager getFanstList:1 userid:[_userId integerValue] callback:callback];
}

#pragma mark -UITableViewDelegate UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.focusTableView) {
                return _focNumArray.count;
    }else{
        
        return _fanNumArray.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellID = @"cell";
    VLSSonTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[VLSSonTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
  
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    [cell changeButtonInformation];
    if (tableView == self.focusTableView) {
        if (self.focNumArray.count>0) {
            MineModel  *model = [self.focNumArray objectAtIndex:indexPath.row];
            [cell configData:model];
        }
        
    }else
    {
        if (self.fanNumArray.count>0) {
            MineModel  *model = [self.fanNumArray objectAtIndex:indexPath.row];
            [cell configData:model];
        }
        
    }
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *userid;
    if (tableView==_focusTableView) {
        if (_focNumArray.count >0) {
            MineModel *model = [_focNumArray objectAtIndex:indexPath.row];
            
            self.mineModel = model;
            userid = model.ID;
        }
    }else if (tableView==_fansTableView)
    {
        if (_fanNumArray.count >0) {
            MineModel *model = [_fanNumArray objectAtIndex:indexPath.row];
            self.mineModel = model;
            userid = model.ID;
        }
    }
    VLSOthersViewController *otVC = [VLSOthersViewController new];
    [[VLSUserTrackingManager shareManager] trackingViewprofile:userid];
    otVC.userId = userid;
    //标示是从me页面进
    otVC.isFromMe = YES;
    [self.navigationController pushViewController:otVC animated:YES];
}

#pragma mark - VLSSegmentViewDelegate
- (void)selectView:(VLSSegmentView *)selectView didSelectedButtonFrom:(NSInteger)from to:(NSInteger)to
{
    
    switch (to) {
        case 0:
            self.showingTableView = self.focusTableView;
//            [self getFocusList];
//            focuesN = 2;
            break;
        case 1:
            self.showingTableView = self.fansTableView;
//            [self getFansList];
//            fanceN = 2;
            break;
        default:
            break;
    }
    //根据点击的按钮计算出backgroundScrollView的内容偏移量
    CGFloat offsetX = to * SC_WIDTH;
    CGPoint scrPoint = self.baseScrollerView.contentOffset;
    scrPoint.x = offsetX;
    [UIView animateWithDuration:0.3 animations:^{
        [self.baseScrollerView setContentOffset:scrPoint];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView == self.focusTableView || scrollView == self.fansTableView) {
        self.showingTableView = (UITableView *)scrollView;
        CGFloat yOffset  = scrollView.contentOffset.y;
        if (yOffset < - (225  + SelectViewHeight)) {
            CGRect f = _mainView.frame;
            f.origin.y = -yOffset-f.size.height-SelectViewHeight;
            _mainView.frame = f;
            [_mainView setNeedsDisplay];
            CGRect g = _segmentView.frame;
            g.origin.y = -yOffset-SelectViewHeight;
            _segmentView.frame = g;
            self.focusTableView.contentOffset = CGPointMake(0,yOffset);
            self.fansTableView.contentOffset = CGPointMake(0, yOffset);
        }else{
            CGRect f = _mainView.frame;
            f.origin.y = -yOffset - (225  + SelectViewHeight);
            f.size.height =  225 ;
            _mainView.frame = f;
            [_mainView setNeedsDisplay];
            CGRect g = _segmentView.frame;
            
            if (-yOffset-SelectViewHeight <= 64.0f){
                self.navView.alpha = 1;
                self.lineLable.alpha = 1;
                g.origin.y = 64;
            }else{
                g.origin.y = -yOffset-SelectViewHeight;
                if (-yOffset-SelectViewHeight <120) {
                    self.navView.alpha = 1- (float)(-yOffset-SelectViewHeight - 64.f)/(120.f-64.f);
                    self.lineLable.alpha = 1- (float)(-yOffset-SelectViewHeight - 64.f)/(120.f-64.f);
                }else{
                    _navView.alpha = 0;
                    _lineLable.alpha = 0;
                }
            }
            _segmentView.frame = g;
            self.focusTableView.contentOffset = CGPointMake(0,yOffset);
            self.fansTableView.contentOffset = CGPointMake(0, yOffset);
        }
    } else {
        //说明是backgroundScrollView在滚动
        CGFloat offsetX = self.baseScrollerView.contentOffset.x;
        NSInteger index = offsetX / SC_WIDTH;
        CGFloat seleOffsetX = offsetX - self.scrollX;
        self.scrollX = offsetX;
        //根据scrollViewX偏移量算出顶部selectViewline的位置
        if (seleOffsetX > 0 && offsetX / SC_WIDTH >= (0.5 + index)) {
            [self.segmentView lineToIndex:index + 1];
        } else if (seleOffsetX < 0 && offsetX / SC_WIDTH <= (0.5 + index)) {
            [self.segmentView lineToIndex:index];
        }
    }
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}
#pragma mark - 数组初始化
-(NSMutableArray *)infoMUArray{
    if (!_infoMUArray) {
        _infoMUArray = [NSMutableArray new];
    }
    return _infoMUArray;
}
-(NSMutableArray *)focNumArray{
    
    if (_focNumArray==nil) {
        _focNumArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _focNumArray;
}
-(NSMutableArray *)fanNumArray{
    if (_fanNumArray == nil) {
        _fanNumArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _fanNumArray;
}
#pragma mark 富文本图片
- (void)reloadNameInformaton
{
    
    NSInteger levelNumber;
    levelNumber = [_Vmodel.level integerValue];
    
    NSString *nameStr = _Vmodel.userName;
    if ([self getWidthOfNameLabel:nameStr withFont:20 withSize:20] + 27> SC_WIDTH - 40) {
        _mainView.sexImage.hidden = NO;
        if (levelNumber > 0) {
            _mainView.levelImg.hidden = NO;
            _mainView.nameLab.sd_layout
            .rightSpaceToView(_mainView, 20 + 16 + 40);
        }else{
            _mainView.levelImg.hidden = YES;
            _mainView.nameLab.sd_layout
            .rightSpaceToView(_mainView, 20 + 16);
        }
    }else{
        _mainView.sexImage.hidden = YES;
        _mainView.levelImg.hidden = YES;
        _mainView.nameLab.sd_layout
        .rightSpaceToView(_mainView, 20);
    }
    
    // 创建一个富文本
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@  ",nameStr]];
    // 添加表情
    attch = [[NSTextAttachment alloc] init];
    attchLevel = [[NSTextAttachment alloc] init];
    attchLevel.bounds = CGRectMake(0, -2, 30, 16);
    // 设置图片大小
    attch.bounds = CGRectMake(8, -2, 16, 16);
    if (1 <= levelNumber && levelNumber <= 9) {
        
        attchLevel.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_1-9"];
        _mainView.levelImg.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_1-9"];
    }else if (10 <= levelNumber && levelNumber <= 19){
        
        attchLevel.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_10-19"];
        _mainView.levelImg.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_10-19"];
    }else if (20 <= levelNumber && levelNumber <= 29){
        
        attchLevel.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_20-29"];
        _mainView.levelImg.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_20-29"];
    }else if (30 <= levelNumber && levelNumber <= 39){
        
        attchLevel.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_30-39"];
        _mainView.levelImg.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_30-39"];
    }else if (40 <= levelNumber && levelNumber <= 49){
        
        attchLevel.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_40-49"];
        _mainView.levelImg.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_40-49"];
    }else if(50 <= levelNumber){
        
        attchLevel.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_50"];
        _mainView.levelImg.image = [self getImageFromNSInteger:levelNumber withLevelString:@"lv_50"];
    }else{
        
        attchLevel.bounds = CGRectMake(0, -2, 0, 0);
        attch.bounds = CGRectMake(0, -2, 16, 16);
    }
    
    // 创建带有图片的富文本
    NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:attch];
    NSAttributedString *stringLevel = [NSAttributedString attributedStringWithAttachment:attchLevel];
    [attri appendAttributedString:stringLevel];
    [attri appendAttributedString:string];
    // 表情图片
    if ([[AccountManager sharedAccountManager].account.gender isEqualToString:@"MALE"]) {
        attch.image = [UIImage imageNamed:@"sex_male"];
        _mainView.sexImage.image = [UIImage imageNamed:@"sex_male"];
    }else if ([[AccountManager sharedAccountManager].account.gender isEqualToString:@"FEMALE"]){
        attch.image = [UIImage imageNamed:@"sex_female"];
        _mainView.sexImage.image = [UIImage imageNamed:@"sex_female"];
    }else{
        
        if (levelNumber <= 0) {
            [attri deleteCharactersInRange:NSMakeRange(nameStr.length, attri.length - nameStr.length)];
            _mainView.nameLab.sd_layout
            .rightSpaceToView(_mainView, 20);
            
        }else{
            if ([self getWidthOfNameLabel:nameStr withFont:20 withSize:20] + 27> SC_WIDTH - 40){
                
                [attri deleteCharactersInRange:NSMakeRange(nameStr.length + 3, attri.length - nameStr.length - 3)];
                _mainView.nameLab.sd_layout
                .rightSpaceToView(_mainView, 20 + 10 + 30);
            }else{
                
                [attri deleteCharactersInRange:NSMakeRange(nameStr.length + 3, attri.length - nameStr.length - 3)];
                _mainView.nameLab.sd_layout
                .rightSpaceToView(_mainView, 20);
            }
            
        }
        _mainView.nameLab.text = nameStr;
        _mainView.nameLab.textAlignment = NSTextAlignmentCenter;
        _mainView.sexImage.hidden = YES;
        
    }
    
    _mainView.nameLab.attributedText = attri;
    _mainView.nameLab.textAlignment = NSTextAlignmentCenter;
}

#pragma mark - 简化代码方法
- (UIImage*)addText:(NSString*)text andImage:(UIImage*)img{
    //开启位图上下文
    UIGraphicsBeginImageContextWithOptions(img.size, NO, 0);
    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
    [text drawInRect:CGRectMake(0+img.size.width/2, 2, 30, img.size.height) withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10],NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIImage*currentImage=UIGraphicsGetImageFromCurrentImageContext();
    //关闭位图上下文
    UIGraphicsEndImageContext();
    return currentImage;
}

- (UIImage*)getImageFromNSInteger:(NSInteger)levelNum withLevelString:(NSString*)levelString{
    
    if (levelNum >= 1 && levelNum <=9) {
        UIImage *img = [self addText:[NSString stringWithFormat:@" %ld",levelNum] andImage:[UIImage imageNamed:levelString]];
        return img;
    }else{
        
        UIImage *img = [self addText:[NSString stringWithFormat:@"%ld",levelNum] andImage:[UIImage imageNamed:levelString]];
        return img;
    }
}

-(void)jumpLiveRoom{

    NSString *userid = [NSString stringWithFormat:@"%ld",[AccountManager sharedAccountManager].account.uid];
    BOOL isAnchor = [[VLSMessageManager sharedManager] checkUserIsAnchor:userid];
    
    if (isAnchor==YES) {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else
    {
        NSArray *array = self.navigationController.viewControllers;
        for (int i = 0; i < array.count; i++) {
            UIViewController *showing = [array objectAtIndex:i];
            if ([showing isKindOfClass:[VLSShowViewController class]]) {
                VLSShowViewController *isshow = (VLSShowViewController*)showing;
                
                [isshow reloadLiveWidthRoomId:self.roomID anchorId:self.userId];
                [self.navigationController popToViewController:isshow animated:YES];
                return;
            }
        }
        VLSShowViewController *show = [[VLSShowViewController alloc] init];
        show.anchorId = self.userId;
        show.roomId = self.roomID;
        
        VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
        joinRoom.roomID = self.roomID;
        joinRoom.joinType = @"profile";
        show.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];
    
        show.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:show animated:YES];
    }
}

- (void)settingClick:(id)sender{
    
    SettingViewController *svc = [[SettingViewController alloc] init];
    svc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:svc animated:YES];
}
- (void)myHomeClick:(id)sender{
    

}

-(UIView *)screenView{
    if (_screenView == nil) {
        _screenView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, SC_HEIGHT)];
    }
    return _screenView;
}


- (UITableView *)focusTableView
{
    if (_focusTableView == nil) {
        _focusTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, SC_WIDTH, SC_HEIGHT -50) style:UITableViewStylePlain];
        _focusTableView.contentInset = UIEdgeInsetsMake(225 +SelectViewHeight, 0, 0, 0);

        _focusTableView.backgroundColor = [UIColor whiteColor];
        _focusTableView.delegate = self;
        _focusTableView.dataSource = self;
        _focusTableView.tableFooterView = [UIView new];
        _focusTableView.separatorColor = [UIColor clearColor];
        
    }
    return _focusTableView;
}
- (UITableView *)fansTableView
{
    if (_fansTableView == nil) {
        _fansTableView = [[UITableView alloc] initWithFrame: CGRectMake(SC_WIDTH, 0, SC_WIDTH, SC_HEIGHT-50) style:UITableViewStylePlain];
        _fansTableView.contentInset = UIEdgeInsetsMake(225 +SelectViewHeight, 0, 0, 0);
        _fansTableView.delegate = self;
        _fansTableView.dataSource = self;
        _fansTableView.backgroundColor = [UIColor whiteColor];
        _fansTableView.tableFooterView = [UIView new];
        _fansTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _fansTableView;
}

- (UIScrollView *)baseScrollerView
{
    if (_baseScrollerView == nil) {
        _baseScrollerView = [[UIScrollView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _baseScrollerView.backgroundColor = [UIColor lightGrayColor];
        _baseScrollerView.pagingEnabled = YES;
        _baseScrollerView.bounces = NO;
        _baseScrollerView.showsHorizontalScrollIndicator = NO;
        _baseScrollerView.delegate = self;
        _baseScrollerView.contentSize = CGSizeMake(SC_WIDTH * 2, 0);
        
    }
    return _baseScrollerView;
}

- (VLSSegmentView *)segmentView
{
    if (_segmentView == nil) {
        _segmentView = [[VLSSegmentView alloc] initWithFrame:CGRectMake(0, self.mainView.frame.size.height, SC_WIDTH, SelectViewHeight)];
        _segmentView.backgroundColor = [UIColor whiteColor];
        _segmentView.delegate = self;
    }
    return _segmentView;
}
- (UIView *)navView
{
    if (_navView == nil) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, 64)];
        _navView.backgroundColor = [UIColor colorWithPatternImage:[UIImage ImageFromColor:[UIColor whiteColor]]];
        _navView.alpha = 0.0;
    }
    return _navView;
}

-(UILabel *)lineLable{

    if (_lineLable == nil) {
        
        _lineLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 64, SC_WIDTH, 1)];
        _lineLable.backgroundColor = RGB16(COLOR_BG_EEEEEE);
        _lineLable.alpha = 0.0;
    }
    return _lineLable;
}

- (UIButton*)numBt {
    if (!_numBt) {
        UIImage* voteImage = [UIImage imageNamed:@"vote_num_back"];
        _numBt = [UIButton buttonWithType:UIButtonTypeCustom];
        _numBt.frame = CGRectMake(SCREEN_WIDTH - 70 - 11, 30, 70, voteImage.size.height);
        [_numBt setImage:voteImage forState:UIControlStateNormal];
        [_numBt setTitleColor:RGB16(COLOR_FONT_FFFFFF) forState:UIControlStateNormal];
        [_numBt setTitle:@"0" forState:UIControlStateNormal];
        _numBt.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        [_numBt addTarget:self action:@selector(vote) forControlEvents:UIControlEventTouchUpInside];
        _numBt.titleEdgeInsets = UIEdgeInsetsMake(0, -voteImage.size.width, 0, 0);
    }
    return _numBt;
}

//投票
- (void)vote {
    [self.mainView vote];
}

-(UILabel *)navLab{
    
    if (!_navLab) {
        _navLab = [[UILabel alloc] initWithFrame:CGRectMake(34, 34, _numBt.frame.origin.x - 34 - 6, 20)];
        _navLab.textColor = RGB16(COLOR_FONT_4A4A4A);
        _navLab.textAlignment = NSTextAlignmentCenter;
        _navLab.font = [UIFont boldSystemFontOfSize:16];
    }
    return _navLab;
}

- (MainInformationView*)mainView
{
    if (_mainView == nil) {
        _mainView =[[MainInformationView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, 225 )];
        _mainView.setBtn.hidden = YES;
        _mainView.delegate = self;
    }
    return _mainView;
}

-(UIButton *)xiuhaoNum{
    
    if (_xiuhaoNum == nil) {
        _xiuhaoNum = [UIButton buttonWithType:UIButtonTypeSystem];
        _xiuhaoNum.frame = CGRectMake(10, 33, 26, 26);
        [_xiuhaoNum setBackgroundImage:[UIImage imageNamed:@"ic_back"] forState:UIControlStateNormal];

    }
    return _xiuhaoNum;
}
#pragma mark-返回按钮
- (UIButton*)topxiuhaoNum
{
    if (_topxiuhaoNum == nil) {
        _topxiuhaoNum = [UIButton buttonWithType:UIButtonTypeSystem];
        _topxiuhaoNum.frame = CGRectMake(0, SC_WIDTH / 19, 50, 30);//CGRectMake(0, SC_WIDTH / 12- 5, 50, 30);
        _topxiuhaoNum.backgroundColor = [UIColor clearColor];
        [_topxiuhaoNum addTarget:self action:@selector(topxiuhaoViewBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _topxiuhaoNum;

}
- (void)topxiuhaoViewBtn:(UIButton*)sender
{
        [self.navigationController popViewControllerAnimated:YES];
}


- (UIView*)bottomView
{
    if (_bottomView == nil) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, SC_HEIGHT - 50, SC_WIDTH, 50)];
        leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH/2 -1, 50)];
        lineView = [[UIView alloc] initWithFrame:CGRectMake(SC_WIDTH/2, 10, 1, 30)];
        lineView.backgroundColor = RGB16(COLOR_BG_EEEEEE);
        topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, 1)];
        topLine.backgroundColor = RGB16(COLOR_BG_EEEEEE);
        
        leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, leftView.frame.size.width, 20)];
        leftLabel.textAlignment = NSTextAlignmentCenter;
        leftLabel.font = [UIFont systemFontOfSize:13];
        leftLabel.textColor = RGB16(COLOR_FONT_4A4A4A);
        NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",LocalizedString(@"MINE_USER_GETBLACK")]];
        NSTextAttachment *attach = [[NSTextAttachment alloc] init];
        attach.image = [UIImage imageNamed:@"mine_blacklist"];
        attach.bounds = CGRectMake(0, -3, 16, 16);
        NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:attach];
        [attri insertAttributedString:string atIndex:0];
        leftLabel.attributedText = attri;
        
        rightView = [[UIView alloc] initWithFrame:CGRectMake(SC_WIDTH/2, 0, SC_WIDTH/2, 50)];
        rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, rightView.frame.size.width, 20)];
        rightLabel.textAlignment = NSTextAlignmentCenter;
        rightLabel.font = [UIFont systemFontOfSize:13];

        getBlackBut = [UIButton buttonWithType:UIButtonTypeCustom];
        getBlackBut.frame = CGRectMake(0, 0, leftView.frame.size.width, leftView.frame.size.height);
        [getBlackBut addTarget:self action:@selector(getBlack:) forControlEvents:UIControlEventTouchUpInside];
        focureBut = [UIButton buttonWithType:UIButtonTypeCustom];
        focureBut.frame = CGRectMake(0, 0, rightView.frame.size.width, rightView.frame.size.height);
        [focureBut addTarget:self action:@selector(getFocure:) forControlEvents:UIControlEventTouchUpInside];
        
        [leftView addSubview:lineView];
        [leftView addSubview:leftLabel];
        
        [rightView addSubview:rightLabel];
        [leftView addSubview:getBlackBut];
        [rightView addSubview:focureBut];
        
        [_bottomView addSubview:leftView];
        [_bottomView addSubview:rightView];
        [_bottomView addSubview:topLine];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}
-(NSMutableArray*)backContentArray
{
    if (_backContentArray == nil) {
        _backContentArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _backContentArray;
}
#pragma mark －拉黑action
- (void)getBlack:(UIButton*)sender
{
    [self showProgressHUD];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        ManagerEvent *event = [ManagerEvent new];
        event.title = LocalizedString(@"MINE_USER_HEADBLACK");
        [self getUserInformation:3];
//        [self getFansList];
        [self success:event];
    };
    callback.errorBlock = ^(id result)
    {
        [self hideProgressHUDAfterDelay:0];

    };
    if ([AccountManager sharedAccountManager].account.uid == [_userId integerValue]) {
        [self hideProgressHUDAfterDelay:0];
        ManagerEvent *event = [ManagerEvent new];
        event.title = LocalizedString(@"MINE_USER_CANTBLACKSELF");
        [self success:event];
    }else
    {
    
    if ([self.Vmodel.isBlack boolValue]== NO) {
        
        [[VLSUserTrackingManager shareManager] trackingUserAction:_userId action:@"blacklist_on"];
        [VLSLiveHttpManager addBlack:_userId callback:callback];
    }
    else
    {
        [self hideProgressHUDAfterDelay:0];
        ManagerEvent *event = [ManagerEvent new];
        event.title = LocalizedString(@"MINE_USER_INBLACK");
        [self success:event];
    }
    }
}
#pragma mark - 关注action
- (void)getFocure:(UIButton*)sender
{
    
    [self showProgressHUD];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        [self getUserInformation:3];
//        [self getFansList];
        
    };
    callback.errorBlock = ^(id result)
    {
        [self hideProgressHUDAfterDelay:0];

    };
    if ([_Vmodel.isBlack boolValue] == NO)
{
    if ([_Vmodel.isAttent boolValue] == NO)
    {
        if ([AccountManager sharedAccountManager].account.uid == [_userId integerValue])
        {
            [self hideProgressHUDAfterDelay:0];
            ManagerEvent *event = [ManagerEvent new];
            event.title = LocalizedString(@"MINE_USER_CANTFOCUESELF");
            [self success:event];
        }else
        {
        [[VLSUserTrackingManager shareManager] trackingUserAction:_userId action:@"focus_on"];
        [VLSLiveHttpManager focusToUser:_userId callback:callback];
        }
    }else
    {
        [[VLSUserTrackingManager shareManager] trackingUserAction:_userId action:@"focus_off"];
        [VLSLiveHttpManager cancelFocusToUser:_userId callback:callback];
    }
}
    else
{
        [self hideProgressHUDAfterDelay:0];
        ManagerEvent *event = [ManagerEvent new];
        event.title = LocalizedString(@"MINE_USER_CANTBEFOCUE");
        [self success:event];
}
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
//根据内容计算按钮长度
- (CGFloat)getWidthOfNameLabel:(NSString*)aString withFont:(CGFloat)fontSize withSize:(CGFloat)fontHeight{
    CGSize titleSize = [aString sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(MAXFLOAT, fontHeight)];
    return titleSize.width;
}

#pragma mark --MainInformationViewDelegate
- (void)vote:(NSString *)message dic:(NSDictionary *)dic
{
    [self showHUD:message];
    if (dic != nil) {
        NSNumber* totalVotes = [dic objectForKey:@"totalVotes"];
        NSNumber* residueVotes = [dic objectForKey:@"residueVotes"];
        NSString* userId = _Vmodel.userId == nil? @"" : _Vmodel.userId;
        NSString* userName = _Vmodel.userName == nil? @"" : _Vmodel.userName;
        [_mainView updateVoteModel:[NSDictionary dictionaryWithObjectsAndKeys:userId, @"votedId", userName, @"nickname", residueVotes, @"residueVotes", totalVotes, @"totalVotes", nil]];
        [self.numBt setTitle:totalVotes.stringValue forState:UIControlStateNormal];
    }
}

- (void)recharge:(UIViewController *)vc
{
    if (vc != nil) {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
