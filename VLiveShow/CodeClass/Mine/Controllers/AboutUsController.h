//
//  AboutUsController.h
//  VLiveShow
//
//  Created by sp on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSBaseViewController.h"
@interface AboutUsController : VLSBaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UIView *backView;
@property (nonatomic,strong)UIImageView *iconImage;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UITableView *baseTable;
@property (nonatomic,strong)UILabel *bottomLabel;

@end
