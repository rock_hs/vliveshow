//
//  VLSServiceViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSServiceViewController.h"

@interface VLSServiceViewController ()

@property(nonatomic,strong)UIWebView *webView;

@property (nonatomic, copy) NSString *languageStr;


@end

@implementation VLSServiceViewController

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    _languageStr = [self getPreferredLanguage];
    NSLog(@"2222:%@",_languageStr);
    if (self.navigationController.navigationBarHidden) {
        self.navigationController.navigationBarHidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = LocalizedString(@"MINE_SERVICETERMS");

    _languageStr = [self getPreferredLanguage];
    [self loadRequestWebViewWithString:@"VProtocol.html"];
}

//判断当前手机语言环境
- (NSString*)getPreferredLanguage
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSLog(@"当前语言:%@",preferredLang);
    return preferredLang;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}









@end
