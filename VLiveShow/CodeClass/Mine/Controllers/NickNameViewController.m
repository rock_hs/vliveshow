//
//  NickNameViewController.m
//  VLiveShow
//
//  Created by SXW on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "NickNameViewController.h"
#import "HTTPFacade.h"
#import "AccountManager.h"
#import "VLSLiveHttpManager.h"
#import "UUIDShortener.h"
@interface NickNameViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) UITextField *nickNameTF;

@end

@implementation NickNameViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.nickNameTF.text = [AccountManager sharedAccountManager].account.nickName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB16(COLOR_BG_E9E7EF);
    self.title = LocalizedString(@"MINE_CHANGENICKNAME");
    [self createCustomRight];
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initView{

    [self.view addSubview:self.nickNameTF];
}

- (void)createCustomRight{

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadNickNameBtn:)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    label.textColor = RGB16(COLOR_FONT_4A4A4A);
    label.userInteractionEnabled = YES;
    [label addGestureRecognizer:tap];
    label.text = LocalizedString(@"MINE_COMPELETE");
    label.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    label.textAlignment = NSTextAlignmentRight;
    
    //自定义的UIBarButtonItem
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:label];
    //设置右导航按钮
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(UITextField *)nickNameTF{

    if (!_nickNameTF) {
        _nickNameTF = [[UITextField alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH, 50)];
        _nickNameTF.placeholder = LocalizedString(@"MINE_USER_PLACHODER_NICKNAME");
        _nickNameTF.clearButtonMode = UITextFieldViewModeAlways;
        _nickNameTF.clearsOnBeginEditing = NO;
        _nickNameTF.backgroundColor = [UIColor whiteColor];
        _nickNameTF.delegate = self;
        
        UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(20,0,20,50)];
        _nickNameTF.leftView = leftView;
        _nickNameTF.leftViewMode = UITextFieldViewModeAlways;
        _nickNameTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFiledEditChanged:) name:@"UITextFieldTextDidChangeNotification"object:_nickNameTF];
    }
    return _nickNameTF;
}


//点击收起键盘
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [_nickNameTF resignFirstResponder];
}

#pragma mark - UITextFieldDelegate 代理方法
- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    [_nickNameTF resignFirstResponder];
    return YES;
}

-(void)textFiledEditChanged:(NSNotification *)obj{
    UITextField *textField = (UITextField *)obj.object;
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        if (!position) {
            if (toBeString.length > 16) {
                textField.text = [toBeString substringToIndex:16];
            }
        }
        else{
            
        }
    }
    else{
        if (toBeString.length > 16) {
            textField.text = [toBeString substringToIndex:16];
        }  
    }  
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"UITextFieldTextDidChangeNotification" object:_nickNameTF];
}

//按钮事件
- (void)uploadNickNameBtn:(id)sender{
    
    [_nickNameTF resignFirstResponder];
    [self uploadUserName];
}

#pragma mark - 上传用户信息
- (void)uploadUserName{
    
    [self showProgressHUD];
    PostFileModel *fileModel = [[PostFileModel alloc] init];
    fileModel.fileName = [NSUUID UUID].shortUUIDString;
    AccountModel *accountModel = [[AccountModel alloc] init];
    accountModel.nickName = _nickNameTF.text;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.loadBlock = ^(id result){
        
    };
    callback.updateBlock = ^(id result){
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
        [self hideProgressHUDAfterDelay:0.0f];
        [self getUserInfo];
    };
    callback.errorBlock = ^(id result){
        
//        self.nickNameTF.text = [AccountManager sharedAccountManager].account.nickName;
        [self hideProgressHUDAfterDelay:0.0f];
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager postUserProfile:accountModel callback:callback];
}

#pragma mark - 获取用户信息的网络请求
- (void)getUserInfo {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [[VLSMessageManager sharedManager] getSelfProfile];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUserInfo" object:nil];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [AccountManager getUserProfileCallback:callback];
}


@end
