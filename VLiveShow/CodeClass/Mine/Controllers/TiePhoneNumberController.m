//
//  TiePhoneNumberController.m
//  VLiveShow
//
//  Created by sp on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "TiePhoneNumberController.h"
#import "AccountManager.h"
#import "VLSPhoneLoginViewController.h"
#import "VLSTextField.h"
#import "VLSMessageManager.h"
#import "SectionsViewController.h"
#import "VLSNavigationController.h"
#define lineHight 60
@interface TiePhoneNumberController ()<UITextFieldDelegate>

@end

@implementation TiePhoneNumberController

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.navigationController.navigationBar.tintColor = RGB16(COLOR_FONT_4A4A4A);
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_17],
       NSForegroundColorAttributeName:[UIColor blackColor]}];
    [self.navigationController.navigationBar setTranslucent: NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage ImageFromColor:[UIColor whiteColor]] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarStyle: UIBarStyleDefault];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_17],
       NSForegroundColorAttributeName:[UIColor blackColor]}];
    self.navigationItem.title = LocalizedString(@"MINE_PHONETOBING");
    [self.loginButton setTitle:LocalizedString(@"BESUER_BIND") forState:UIControlStateNormal];
    self.navigationController.navigationBar.tintColor = RGB16(COLOR_FONT_4A4A4A);
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(leftItemAction:)];
    self.navigationItem.leftBarButtonItem = leftItem;
}

#pragma mark - 验证手机号是否存在
- (void)phoneExists {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        if ([[result objectForKey:@"existPhone"] boolValue]) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"MINE_PHONENUMBERREGISTER_DIRECTCHANGE") preferredStyle:UIAlertControllerStyleAlert];
            
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.phoneTF endEditing:YES];
                [self.verCodeTF endEditing:YES];
                
                VLSPhoneLoginViewController *phoneRegisterVC = [[VLSPhoneLoginViewController alloc] init];
                [self.navigationController pushViewController:phoneRegisterVC animated:YES];
                
            }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            
            [okAction setValue:[UIColor redColor] forKey:@"titleTextColor"];
            [okAction setValue:[UIFont boldSystemFontOfSize:14] forKey:@"titleFont"];
            
            [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager phoneExists:model callback:callback];
    
}

- (void)phoneExistsOnly {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.phoneExistFlag = YES;
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if ([[result objectForKey:@"existPhone"] boolValue]) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"MINE_PHONENUMBERREGISTER_DIRECTCHANGE") preferredStyle:UIAlertControllerStyleAlert];
            
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.phoneTF endEditing:YES];
                [self.verCodeTF endEditing:YES];
                
                VLSPhoneLoginViewController *phoneRegisterVC = [[VLSPhoneLoginViewController alloc] init];
                [self.navigationController pushViewController:phoneRegisterVC animated:YES];
                
            }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            
            [okAction setValue:[UIColor redColor] forKey:@"titleTextColor"];
            [okAction setValue:[UIFont boldSystemFontOfSize:14] forKey:@"titleFont"];
            
            [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    };
    callback.errorBlock = ^(id result){
        self.phoneExistFlag = NO;
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager phoneExists:model callback:callback];
}


#pragma mark - 绑定按钮

- (void)loginAction {
    
    if (self.phoneNum.length != 11 && [self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
        
        self.phoneTF.lineLabel.backgroundColor = [UIColor redColor];
        self.phoneTF.textColor = [UIColor redColor];
        [self showWarningWithString:LocalizedString(@"ALERT_MESSAGE_RIGHT_PHONENUM")];
        return;
    }
    
    AccountModel *model = [[AccountModel alloc] init];
    
    model.mobilePhone = self.phoneNum;
    model.verificationCode = self.verCodeTF.text;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self success:result];
        }
        [self getUserInfo];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    };
    callback.errorBlock = ^(id result){
        ManagerEvent *event = result;
        if (event.code == 20003)
        {
            self.verCodeTF.lineLabel.backgroundColor = [UIColor redColor];
            self.verCodeTF.textColor = [UIColor redColor];
            [self showWarningWithString:LocalizedString(@"VERCODE_ERROR")];
        } else {
            [self error:result];
        }
    };
    
    [[VLSUserTrackingManager shareManager] trackingBindingphone:model bindType:@"binding"] ;
    [AccountManager phoneBind:model callback:callback];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.verCodeTF resignFirstResponder];
    [self.phoneTF resignFirstResponder];
    return YES;
}

- (void)leftItemAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}






@end
