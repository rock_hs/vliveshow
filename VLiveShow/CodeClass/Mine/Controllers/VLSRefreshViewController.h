//
//  VLSRefreshViewController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/24.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"

@class MJRefreshBackNormalFooter;
@class MJRefreshGifHeader;

@interface VLSRefreshViewController : VLSBaseViewController

//@property (nonatomic, strong) MJRefreshAutoNormalFooter *footer;
@property (nonatomic, strong) MJRefreshBackNormalFooter *footer;
@property (nonatomic, strong) MJRefreshGifHeader *header;

- (void)loadMoreData;
- (void)refreshDataSource;
- (void)refreshDataWith:(UITableView*)tableView;
@end
