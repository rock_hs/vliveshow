//
//  VLSOthersViewController.h
//  VLiveShow
//
//  Created by sp on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "MySettingViewController.h"
#import "MineModel.h"
#import "FocusTableViewCell.h"
#import "FansTableViewCell.h"
#import "MainInformationView.h"

@interface VLSOthersViewController : VLSBaseViewController<sendUserImgDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView * baseTable;

@property (nonatomic,strong) UIView *screenView;

@property (nonatomic, copy) NSString *faceUrlStr;//属性传值

@property (nonatomic, copy) NSString *roomID;//主播房间号

//@property (nonatomic, strong) UIButton *setBut;//直播按钮
//@property (nonatomic, strong) UIButton *topSetBut;//直播按钮
@property (nonatomic, strong) UIButton *xiuhaoNum;
@property (nonatomic, strong) UIButton *topxiuhaoNum;
@property (nonatomic,strong)MineModel *mineModel;
@property (nonatomic,strong)UIView *bottomView;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic,assign)BOOL isFromMe;
@property (nonatomic,assign)BOOL isFromLiveRoom;
@end
