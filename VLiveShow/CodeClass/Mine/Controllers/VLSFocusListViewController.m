//
//  VLSFocusListViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSFocusListViewController.h"
#import "MJRefreshAutoNormalFooter.h"
#import "MJRefreshGifHeader.h"
#import "AccountManager.h"

@interface VLSFocusListViewController ()
@property(nonatomic, assign) BOOL isMe;
@end

@implementation VLSFocusListViewController

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar useLightTheme];
}

- (void)viewDidLoad {
    self.title = LocalizedString(@"MINE_FOCUS");
    self.isMe = self.userId == [AccountManager sharedAccountManager].account.uid;
    [super viewDidLoad];
    [self initView];
    self.focusLabel.hidden = YES;
    [self getFocusList];

    self.page = 2;
    [self refreshDataWith:self.tableView];
}

- (void)initView{

    [self.view addSubview:self.tableView];
    [self.view addSubview:self.focusLabel];
}

#pragma mark - 采集获取数据
//获取关注列表
- (void)getFocusList{
    
    [self showProgressHUD];
    self.page = 2;
    [self.focNumArray removeAllObjects];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        [self hideProgressHUDAfterDelay:0];
        [self.tableView.mj_header endRefreshing];
        [self.focNumArray addObjectsFromArray: result];
        if (self.focNumArray.count == 0) {
            self.focusLabel.hidden = NO;
        }else{
            
            self.focusLabel.hidden = YES;
        }
        [self.tableView reloadData];
        NSLog(@"大子foc%@",result);
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    NSInteger uid = self.userId;
    if (uid == 0)
    {
        uid = [AccountManager sharedAccountManager].account.uid;
    }
    [VLSLiveHttpManager getAttentList:1 userid: uid callback:callback];
}
#pragma mark - cell的代理方法

-(void)focusBtnViewBtn:(FocusTableViewCell *)cell{
    
    if (!_isMe)
    {
        [self pushNextVC: cell];
        cell.focusBtn.selected = NO;
        return;
    }
    
    [self showProgressHUD];
//    [cell showWaiting:cell.focusBtn];
    NSIndexPath *indpath = [self.tableView indexPathForCell:cell];
    MineModel *model = [self.focNumArray objectAtIndex:indpath.row];
    model.isPress = !model.isPress;
    
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
//        [cell hideWaiting];
        [self hideProgressHUDAfterDelay:0];
        
    };
    callback.errorBlock = ^(id result)
    {
        [self hideProgressHUDAfterDelay:0];
        [self error:result];
    };
    
    if ( cell.focusBtn.selected == NO) {
        
        [[VLSUserTrackingManager shareManager] trackingUserAction:model.ID action:@"focus_on"];

        [VLSLiveHttpManager focusToUser:model.ID callback:callback];
        [VLSLiveHttpManager cancelBlack:model.ID callback:callback];
    }
    else
    {
        [[VLSUserTrackingManager shareManager] trackingUserAction:model.ID action:@"focus_off"];

        [VLSLiveHttpManager cancelFocusToUser:model.ID callback:callback];
    }
}

-(void)pushNextVC:(FocusTableViewCell *)cell{
    
    [self pushVC:self.focNumArray and:cell with:self.tableView];
}

- (void)pushVC:(NSMutableArray*)array and:(UITableViewCell*)cell with:(UITableView*)tableView{
    
    NSIndexPath *indpath = [tableView indexPathForCell:cell];
    MineModel *model = [array objectAtIndex:indpath.row];
    UserProfileViewController* vlsVC = [UserProfileViewController create];
    vlsVC.userId = model.ID;
    
    vlsVC.hidesBottomBarWhenPushed = YES;
    [[VLSUserTrackingManager shareManager] trackingViewprofile:model.ID];


    [self.navigationController pushViewController:vlsVC animated:YES];
}

#pragma mark - 代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _focNumArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FocusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.delegate = self;
    if (self.focNumArray.count > 0)
    {
        MineModel  *model = [self.focNumArray objectAtIndex:indexPath.row];
        [cell setModel:model];
        // Walk around for the user isn't self
        if (!_isMe)
        {
            [cell.focusBtn setTitle: LocalizedString(@"MINE_CHECK") forState: UIControlStateNormal];
        }
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
}


#pragma mark - 控件初始化
-(UITableView *)tableView{

    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[FocusTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(NSMutableArray *)focNumArray{

    if (_focNumArray == nil) {
        
        _focNumArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _focNumArray;
}

-(UILabel *)focusLabel{
    
    if (_focusLabel == nil) {
        
        _focusLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, SCREEN_HEIGHT / 2 - 20 - 32, SCREEN_WIDTH - 40, 20)];
        _focusLabel.text = LocalizedString(@"MINE_GOTOROOM_TOFOCUSHOTPEOPLE");
        _focusLabel.textColor = RGB16(COLOR_FONT_979797);
        _focusLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        _focusLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _focusLabel;
}

- (void)refreshDataSource{
    
    [self getFocusList];
}
-(void)loadMoreData{

    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        [self.tableView.mj_footer endRefreshing];
        [self.focNumArray addObjectsFromArray: result];
        if (self.focNumArray.count == 0) {
            self.focusLabel.hidden = NO;
        }else{
            
            self.focusLabel.hidden = YES;
        }
        [self.tableView reloadData];
        self.page ++;
        NSLog(@"大子foc%@",result);
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    NSInteger uid = self.userId;
    if (uid == 0)
    {
        uid = [AccountManager sharedAccountManager].account.uid;
    }
    [VLSLiveHttpManager getAttentList:self.page userid: uid callback:callback];
}


@end
