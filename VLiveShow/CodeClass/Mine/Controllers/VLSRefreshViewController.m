//
//  VLSRefreshViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/24.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSRefreshViewController.h"
#import <MJRefresh/MJRefresh.h>

@interface VLSRefreshViewController ()

@end

@implementation VLSRefreshViewController

- (void)refreshDataWith:(UITableView*)tableView{

    self.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self loadMoreData];
    }];
    
    self.header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self refreshDataSource];
    }];
    [self.header setTitle:LocalizedString(@"LIVE_PULL_REFRESH") forState:MJRefreshStateIdle];
    [self.header setTitle:LocalizedString(@"LIVE_PULL_REFRESH") forState:MJRefreshStatePulling];
    [self.header setTitle:LocalizedString(@"LIVE_PULL_REFRESHING") forState:MJRefreshStateRefreshing];
    [self.footer setTitle:LocalizedString(@"LIVE_PULL_LOAD_MORE") forState:MJRefreshStateRefreshing];
    tableView.mj_footer = self.footer;
    tableView.mj_header = self.header;

}

-(void)loadMoreData{
}

-(void)refreshDataSource{
}
@end
