//
//  VLSPrivacyViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/6/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSPrivacyViewController.h"

@interface VLSPrivacyViewController ()

@end

@implementation VLSPrivacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = LocalizedString(@"MINE_PRIVACYPOLICY");
    [self loadRequestWebViewWithString:@"VPravicy.html"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
