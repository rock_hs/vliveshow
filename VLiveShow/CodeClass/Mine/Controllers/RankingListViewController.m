//
//  RankingListViewController.m
//  VLiveShow
//
//  Created by sp on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "RankingListViewController.h"
#import "RankingModel.h"
#import "NSString+UserFile.h"
#import "RankingTableViewCell.h"
#import "RankingLandscapeTableViewCell.h"
#define topViewHight 392/750.f

static const K_TOPVIEW_HEIGHT = 105;

@interface RankingListViewController ()

@property (nonatomic,strong)UIView * topView;
@property (nonatomic,strong)NavView * navView;
@property (nonatomic,strong)UILabel *ticketNumLab;
@property (nonatomic,strong)UILabel *detaiLabel;
@property (nonatomic,strong)UITableView *baseTableView;
@property (nonatomic,strong)NSMutableArray *dateArray;
@property (nonatomic, strong) UIImageView *redImage;
@end

@implementation RankingListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = [UIColor clearColor];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.view addSubview:self.topView];
    [self.topView addSubview:self.navView];
    [self.topView addSubview:self.ticketNumLab];
    [self.topView addSubview:self.detaiLabel];
    [self.view addSubview:self.baseTableView];
//    [self.baseTableView registerClass:[FrontRankingCell class] forCellReuseIdentifier:@"frontcell"];
//    [self.baseTableView registerClass:[BackRankingCell class] forCellReuseIdentifier:@"backcell"];
    [self.baseTableView registerNib:[UINib nibWithNibName:@"RankingTableViewCell" bundle:nil] forCellReuseIdentifier:RankingTableViewCellCellIdentity];
    [self.baseTableView registerNib:[UINib nibWithNibName:@"RankingLandscapeTableViewCell" bundle:nil] forCellReuseIdentifier:RankingLandscapeTableViewCellIdentifier];
    [self.baseTableView setRowHeight:84];
    [self loadDate];
    [self configUI];
}

- (void)configUI {
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        CGRect rect = self.view.frame;
        rect.size.width = K_LANDSCAPEVIEWWIDTH;
        rect.size.height = SCREEN_HEIGHT;
        self.view.frame = rect;
        rect = self.navView.frame;
        rect.size.width = K_LANDSCAPEVIEWWIDTH;
        rect.origin.y = 7;
        self.navView.frame = rect;
        [self.navView.titleLabel setCenter:CGPointMake(K_LANDSCAPEVIEWWIDTH / 2, self.navView.titleLabel.center.y)];
        [self.navView.backButton removeFromSuperview];
        [self.navView.backImageView removeFromSuperview];
        
        rect = self.ticketNumLab.frame;
        self.ticketNumLab.center = CGPointMake(K_LANDSCAPEVIEWWIDTH / 2, self.ticketNumLab.center.y);
        rect.origin.y = CGRectGetMaxY(self.navView.frame) - 10;
        rect.size.width = K_LANDSCAPEVIEWWIDTH;
        self.ticketNumLab.frame = rect;
        [self.ticketNumLab setFont:[UIFont systemFontOfSize:23]];
        
        rect = self.detaiLabel.frame;
        rect.origin.y = CGRectGetMaxY(self.ticketNumLab.frame) - 15;
        self.detaiLabel.center = CGPointMake(K_LANDSCAPEVIEWWIDTH / 2, 0);
        rect.size.width = K_LANDSCAPEVIEWWIDTH;
        self.detaiLabel.frame = rect;
        [self.detaiLabel setFont:[UIFont systemFontOfSize:12]];
        
        
        rect = self.topView.frame;
        rect.size.width = K_LANDSCAPEVIEWWIDTH;
        rect.size.height = K_TOPVIEW_HEIGHT;
        self.topView.frame = rect;
        self.redImage.frame = rect;
        
        rect = self.baseTableView.frame;
        rect.origin.y = K_TOPVIEW_HEIGHT;
        rect.size.width = K_LANDSCAPEVIEWWIDTH;
        rect.size.height = SCREEN_HEIGHT - K_TOPVIEW_HEIGHT;
        self.baseTableView.frame = rect;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)loadDate
{
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        self.dateArray = [RankingModel mj_objectArrayWithKeyValuesArray:[result objectForKey:@"list"]];
        
        NSDictionary *dic = [result objectForKey:@"me"];
        if (dic) {
            NSInteger vNum = [[dic objectForKey:@"vNumber"] integerValue];
            self.ticketNumLab.text = [NSString thousandpointsNum:vNum];
        }
        
        [self.baseTableView reloadData];
         };
    callback.errorBlock = ^(id result){
        
    };
    if (self.userId!=nil) {
        [VLSLiveHttpManager getRankingList:callback userID:self.userId];

    }
}

#pragma mark-TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    /*
    if (self.dateArray.count==0) {
        return 0;
    }else if (self.dateArray.count <= 3)
    {
        if (section ==0) {
            return self.dateArray.count;
        }else
        {
            return 0;
        }
    }else if (self.dateArray.count >3 && self.dateArray.count<20)
    {
        if (section ==0) {
            return 3;
        }else
        {
            return self.dateArray.count - 3;
        }
    }else
    {
        if (section ==0) {
            return 3;
        }else
        {
            return 17;
        }
    }
    return self.dateArray.count;
     */
    return self.dateArray.count;
 }
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    if (indexPath.section==0) {
        FrontRankingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"frontcell" forIndexPath:indexPath];
        RankingModel *model = [self.dateArray objectAtIndex:indexPath.row];
        cell.rankModel = model;
        
        return cell;
    }else
    {
        
        BackRankingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"backcell" forIndexPath:indexPath];
        if (self.dateArray.count >0) {
            RankingModel *model = [self.dateArray objectAtIndex:indexPath.row +3];
            cell.rankingModel = model;

        }
        return cell;
    }
     */
    UITableViewCell *cell_ = nil;
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        RankingLandscapeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RankingLandscapeTableViewCellIdentifier forIndexPath:indexPath];
        RankingModel *model = self.dateArray[indexPath.row];
        model.vNumberStr = [NSString thousandpointsNum:model.vNumber];
        [cell setModel:self.dateArray[indexPath.row] indexPath:indexPath];
        cell_ = cell;
    }else {
        RankingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RankingTableViewCellCellIdentity forIndexPath:indexPath];
        RankingModel *model = self.dateArray[indexPath.row];
        model.vNumberStr = [NSString thousandpointsNum:model.vNumber];
        [cell setModel:self.dateArray[indexPath.row] indexPath:indexPath];
        cell_ = cell;
    }
    return cell_;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        return 58;
    }
    if (indexPath.section == 0) {
        return 103;
    }else
        return 70;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RankingModel *model;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
       model  = [self.dateArray objectAtIndex:indexPath.row];
        
    }else
    {
        model  = [self.dateArray objectAtIndex:indexPath.row +3];

    }
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    vlsVC.userId =[NSString stringWithFormat:@"%ld",model.userId];
    [[VLSUserTrackingManager shareManager] trackingViewprofile:vlsVC.userId];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vlsVC animated:YES];

    
    
}
#pragma mark-NavViewDelegate
- (void)turnBack
{
    [self.navigationController popViewControllerAnimated:YES];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIView*)topView
{
    if (_topView == nil) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, topViewHight*SC_WIDTH)];
        self.redImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SC_WIDTH, _topView.frame.size.height)];
        self.redImage.image = [UIImage imageNamed:@"but_rankinglist_number"];
        self.redImage.contentMode = UIViewContentModeScaleToFill;//UIViewContentModeScaleAspectFill;
        [_topView addSubview:self.redImage];
        
    }
    return _topView;
}
- (NavView*)navView
{
    if (_navView == nil) {
        _navView = [[NavView alloc] initWithFrame:CGRectMake(0, 20, SC_WIDTH, 44)];
        _navView.delegate = self;
    }
    return _navView;
}
- (UILabel*)ticketNumLab
{
    if (_ticketNumLab == nil) {
        _ticketNumLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, SC_WIDTH, 40)];
        _ticketNumLab.font = [UIFont boldSystemFontOfSize:32];
        _ticketNumLab.textColor = [UIColor whiteColor];
        _ticketNumLab.textAlignment = NSTextAlignmentCenter;
    }
    return _ticketNumLab;
}
- (UILabel *)detaiLabel {
    if (nil == _detaiLabel) {
        _detaiLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, _ticketNumLab.bottom_sd + 4, SC_WIDTH, 40)];
        _detaiLabel.font = [UIFont boldSystemFontOfSize:14];
        _detaiLabel.text = LocalizedString(@"V_TICKETS_RANKING_DETAIL");
        _detaiLabel.textColor = [UIColor whiteColor];
        _detaiLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _detaiLabel;
}
- (UITableView*)baseTableView
{
    if (_baseTableView == nil) {
        _baseTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, topViewHight*SC_WIDTH, SC_WIDTH, SC_HEIGHT-topViewHight*SC_WIDTH) style:UITableViewStyleGrouped];
        _baseTableView.delegate = self;
        _baseTableView.dataSource = self;
        _baseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _baseTableView.backgroundColor = [UIColor whiteColor];
    }
    return _baseTableView;
}

@end
