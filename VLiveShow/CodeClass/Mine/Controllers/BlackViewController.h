//
//  BlackViewController.h
//  VLiveShow
//
//  Created by SXW on 16/6/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSRefreshViewController.h"
#import "FansTableViewCell.h"

@interface BlackViewController : VLSRefreshViewController<UITableViewDataSource,UITableViewDelegate,FansTableViewCellDelegate>
@property (nonatomic,strong)NSMutableArray *dateArray;
@property (nonatomic,strong)UITableView *baseTable;
@end
