//
//  VLSTableViewItem.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

class VLSTableViewItem: NSObject, Mappable, NSCoding {

    var cellHeight: CGFloat = 44.0

    override init() {
        super.init()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init()
    }
    func encode(with aCoder: NSCoder) {
        
    }
    required init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
    }
}
