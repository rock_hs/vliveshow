//
//  LessonModel.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

class LessonModel: VLSTableViewItem {

    var id: NSNumber!
    var courseId: NSNumber!     //课程id
    var lessonId: NSNumber!
    var picCover: String?
    var lessonName: String?          // 课程标题
    var startTime: NSNumber?         //开课时间
    var countdowntime: NSNumber! = 0    //距离开课倒计时时间
    var status: NSNumber!       //课程状态包括   2：已结束； :1：正在直播:0：未直播
    var hasRating: NSNumber?
    var evaluatenum: NSNumber?  //评价人数
    var evaluatescore: NSNumber?    // 评价得分
    
    var endTime: NSNumber?
    var systemTime: NSNumber?
    var courseTypeName: String?
    var roomId: String?
    var teacherId: NSNumber?
    var currentNo: NSNumber! = 0   // 第几节课
    var totalNo: NSNumber! = 0 // NSInteger!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["id"]
        lessonId <- map["lessonId"]
        courseId <- map["courseId"]
        picCover <- map["picCover"]
        lessonName <- map["lessonName"]
        startTime <- map["startTime"]
        countdowntime <- map["countdowntime"]
        status <- map["status"]
        hasRating <- map["hasRating"]
        evaluatenum <- map["evaluatenum"]
        evaluatescore <- map["evaluatescore"]
        endTime <- map["endTime"]
        systemTime <- map["systemTime"]
        courseTypeName <- map["courseTypeName"]
        roomId <- map["roomId"]
        teacherId <- map["teacherId"]
        currentNo <- map["currentNo"]
        totalNo <- map["totalNo"]
    }
}
