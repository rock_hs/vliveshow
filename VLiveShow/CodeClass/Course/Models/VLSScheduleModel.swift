//
//  VLSScheduleModel.swift
//  VLiveShow
//
//  Created by rock on 2017/1/9.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

class VLSScheduleDataModel: VLSTableViewItem {
    var startTime: NSNumber?
    var list: [VLSScheduleCourseModel]? = []
    var dateList: [VLSScheduleDateModel]? = []
    var recommendationList: [VLSScheduleRecommendationModel]? = []
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        startTime <- map["startTime"]
        list <- map["list"]
        dateList <- map["dateList"]
        recommendationList <- map["recommendationList"]
    }
}

class VLSScheduleCourseModel: VLSTableViewItem {
    var courseDTO: VLSCustomScheduleModel?
    var calendarType: NSNumber? //0：表示预约， 1：发布
    var startTime: NSNumber?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        courseDTO <- map["courseDTO"]
        calendarType <- map["calendarType"]
        startTime <- map["startTime"]
    }
}

class VLSScheduleModel: VLSTableViewItem {
    var startTime: NSNumber?
    var lessonNo: NSNumber?
    var evaluatenum: NSNumber?
    var actualStartTime: NSNumber?
    var teacherName: String?
    var picTeacher: String?
    var lessonId: NSNumber?
    var roomId: String?
    var endTime: NSNumber?
    var teacherId: NSNumber?
    var subscriptionType: NSNumber?
    var courseName: String?
    var courseType: NSNumber?
    var picCover: String?
    var lessonName: String?
    var courseId: NSNumber?
    var evaluatescore: NSNumber?
    var subscriptionId: NSNumber?
    var countdowntime: NSNumber?
    var status: NSNumber?
    var oriStartTime: NSNumber?
    var hasRating: NSNumber?
    var courseTypeName: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        startTime <- map["startTime"]
        lessonNo <- map["lessonNo"]
        evaluatenum <- map["evaluatenum"]
        actualStartTime <- map["actualStartTime"]
        teacherName <- map["teacherName"]
        picTeacher <- map["picTeacher"]
        lessonId <- map["lessonId"]
        roomId <- map["roomId"]
        endTime <- map["endTime"]
        teacherId <- map["teacherId"]
        subscriptionType <- map["subscriptionType"]
        courseName <- map["courseName"]
        courseType <- map["courseType"]
        picCover <- map["picCover"]
        lessonName <- map["lessonName"]
        courseId <- map["courseId"]
        evaluatescore <- map["evaluatescore"]
        subscriptionId <- map["subscriptionId"]
        countdowntime <- map["countdowntime"]
        status <- map["status"]
        oriStartTime <- map["oriStartTime"]
        hasRating <- map["hasRating"]
        courseTypeName <- map["courseTypeName"]
    }
}

class VLSScheduleDateModel: VLSTableViewItem {
    var date: String?
    var datecount: NSNumber?
    var isCurDateFlag: Bool = false
    var isSelectedFlag: Bool = false
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        date <- map["date"]
        datecount <- map["count"]
        
        //自定义字段
        let day = Date.dateInterval(Date.date(serviceTime), to: Date.date(StringSafty(date), formatter: "yyyy-MM-dd"))
        isCurDateFlag = day == 0 ? true : false
        isSelectedFlag = isCurDateFlag
        if VLSScheduleCalendarPreDate != "" {
            if VLSScheduleCalendarPreDate == date {
                isSelectedFlag = true
            } else if isCurDateFlag {
                isSelectedFlag = false
            }
        }
    }
}

class VLSScheduleRecommendationModel: VLSTableViewItem {
    var courseId: NSNumber?
    var startTime: NSNumber?
    var priceParticipant: NSNumber?
    var lessonDescription: String?
    var courseTypeName: String?
    var priceAudience: NSNumber?
    var teacherDescription: String?
    var picCover: String?
    var courseType: NSNumber?
    var maxParticipants: NSNumber?
    var allowAudience: NSNumber?
    var numLessons: NSNumber?
    var courseName: String?
    var teacherId: NSNumber?
    var picDescription: String?
    var createTime: NSNumber?
    var courseDescription: String?
    var teacherName:String?
    var picTeacher: String?
    var endTime: NSNumber?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        courseId <- map["courseId"]
        startTime <- map["startTime"]
        priceParticipant <- map["priceParticipant"]
        lessonDescription <- map["lessonDescription"]
        courseTypeName <- map["courseTypeName"]
        priceAudience <- map["priceAudience"]
        teacherDescription <- map["teacherDescription"]
        picCover <- map["picCover"]
        courseType <- map["courseType"]
        maxParticipants <- map["maxParticipants"]
        allowAudience <- map["allowAudience"]
        numLessons <- map["numLessons"]
        courseName <- map["courseName"]
        teacherId <- map["teacherId"]
        picDescription <- map["picDescription"]
        createTime <- map["createTime"]
        courseDescription <- map["courseDescription"]
        teacherName <- map["teacherName"]
        picTeacher <- map["picTeacher"]
        endTime <- map["endTime"]
    }
}
