//
//  VLSCustomScheduleModel.swift
//  VLiveShow
//
//  Created by rock on 2017/1/9.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

class VLSCustomScheduleModel: VLSScheduleModel {

    var customTimeText: String? //时间显示
    var customIntervalSeconds: CLongLong! = 0  //距离开播还有多少秒
    var customCourseStatus: Int = CourseStatus.NotOnLaunch.rawValue
    var customPraisePeopleText: String?
    var customPraiseScoreText: String?
    var customCourseStatusText: String?
    var customCourseStatusColor: UIColor?
    var customCourseTimeColor: UIColor?
    var customCourseBtFlag: Bool = true
    var customCoursePaybackFlag: Bool = true
    var indexPath: IndexPath?   //当前cell的index
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        customPraisePeopleText = ""
        customPraiseScoreText = ""
        //自定义字段
        if let status = status?.intValue, let startTime = startTime?.doubleValue {
            switch status {
            case 0://未开播
                //计算课程距离开播时间差多久
                let day = Date.dateInterval(Date.date(serviceTime), to: Date.date(startTime))
                if day == 1 {//24小时倒计时
                    customIntervalSeconds = Date.dateIntervalSeconds(Date.date(serviceTime), to: Date.date(startTime))
                    customTimeText = " " + LocalizedString("COURSE_COUNTDOWN_TEXT") + Date.dateIntervalDateStr(Date.date(serviceTime), to: Date.date(startTime))
                    customCourseStatus = CourseStatus.LaunchCountDown.rawValue
                    customCourseStatusColor = HEX(COLOR_FONT_DBDBDB)
                    customCourseTimeColor = HEX(COLOR_FONT_FF1130)
                    customCoursePaybackFlag = true
                } else if day >= 2 { //未开播
                    customCourseStatus = CourseStatus.NotOnLaunch.rawValue
                    customTimeText = " " + Date.dateStr(startTime, formatter: "a HH:mm")
                    customCourseStatusColor = HEX(COLOR_FONT_DBDBDB)
                    customCourseTimeColor = HEX(COLOR_BG_D8D8D8)
                    customCoursePaybackFlag = false
                }
                customCourseStatusText = LocalizedString("COURSE_STATUS_NOT")
                customCourseBtFlag = false
            case 1://正在直播
                customCourseStatus = CourseStatus.HasBeenLaunch.rawValue
                customTimeText = " " + Date.dateStr(startTime, formatter: "a HH:mm")
                customCourseStatusText = LocalizedString("COURSE_STATUS_ON")
                customCourseStatusColor = HEX(COLOR_FONT_FF1130)
                customCourseTimeColor = HEX(COLOR_BG_D8D8D8)
                customCourseBtFlag = true
                customCoursePaybackFlag = true
            case 2://已结束
                customTimeText = " " + Date.dateStr(startTime, formatter: "a HH:mm")
                customCourseStatusText = LocalizedString("COURSE_APPRAISE")
                customCourseStatusColor = HEX(COLOR_FONT_DBDBDB)
                customCourseTimeColor = HEX(COLOR_BG_D8D8D8)
                customCourseBtFlag = true
                customCoursePaybackFlag = true
                if hasRating?.boolValue == true {
                    customCourseStatus = CourseStatus.HasBeenPraise.rawValue
                    customCourseStatusText = LocalizedString("COURSE_APPRAISE_END")
                    customCourseStatusColor = HEX(COLOR_FONT_DBDBDB)
                    customCourseBtFlag = false
                } else {
                    customCourseStatus = CourseStatus.NotPraise.rawValue
                    customCourseStatusText = LocalizedString("COURSE_APPRAISE")
                    customCourseStatusColor = HEX(COLOR_FONT_DBDBDB)
                    customCourseBtFlag = true
                }
                if let evaluatenum = evaluatenum?.intValue {
                    if evaluatenum > 0 {//获取多少人评价
                        customPraisePeopleText = StringSafty(evaluatenum) + LocalizedString("COURSE_APPRAISE_PEOPLE")
                        customPraiseScoreText = StringSafty(evaluatescore?.stringValue) + LocalizedString("COURSE_SCORE")
                    }
                }
            default:
                break
            }
        }
        
    }
}
