//
//  PublishCourseModel.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

enum CourseStatus: Int {
    case NotOnLaunch        //未开播（距离开播一天之外）
    case LaunchCountDown    //开播倒计时（距离开播一天之内)
    case HasBeenLaunch      //已经开播
    case EndOfLaunch        //开播已结束
    case OffTheLaunch       //直播已下架
    case HasBeenPraise      //已评价
    case NotPraise          //未评价
}

class PublishCourseModel: VLSCourseModel {

    var customCurLesson: String! //第1/10课时
    var customCurTime: String!  //倒计时
    var customCourseStatus: Int = CourseStatus.NotOnLaunch.rawValue
    var customIntervalSeconds: CLongLong! = 0  //距离开播还有多少秒
    var indexPath: IndexPath?   //当前cell的index
    var courseBtFlag: Bool! = true //按钮是否可以点击
    var courseStatusBtColor: UIColor!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        //自定义字段
        customCurLesson = LocalizedString("COURSE_CUR_NUM_TEXT") + overLesson.stringValue + "/" + numLessons.stringValue + LocalizedString("COURSE_NUM_TEXT")
        if published.boolValue == false {//代表已下架
            customCourseStatus = CourseStatus.OffTheLaunch.rawValue
            courseBtFlag = false
            courseStatusBtColor = HEX(COLOR_FONT_DBDBDB)
        } else if let roomId = roomId, roomId != "" { //如果roomId值不为空，则代表主播“已开播”
            customCourseStatus = CourseStatus.HasBeenLaunch.rawValue
            customCurTime = " " + Date.dateStr(nextLessonStartTime.doubleValue)
            courseBtFlag = true
            courseStatusBtColor = UIColor.red
        } else {
            if let isFinished = nextLessonName, isFinished != "" {//根据下一节课内容是否为空，为空代表课程“已结束”
                //计算课程距离开播时间差多久
                let day = Date.dateInterval(Date.date(serviceTime), to: Date.date(nextLessonStartTime.doubleValue))
                if day == 0 { //已开播
                    customCourseStatus = CourseStatus.HasBeenLaunch.rawValue
                    customCurTime = " " + Date.dateStr(nextLessonStartTime.doubleValue)
                    courseBtFlag = true
                    courseStatusBtColor = UIColor.red
                } else if day == 1 {
                    customCourseStatus = CourseStatus.LaunchCountDown.rawValue
                    customIntervalSeconds = Date.dateIntervalSeconds(Date.date(serviceTime), to: Date.date(nextLessonStartTime.doubleValue))
                    customCurTime = " " + Date.dateIntervalDateStr(Date.date(serviceTime), to: Date.date(nextLessonStartTime.doubleValue))
                    courseBtFlag = false
                    courseStatusBtColor = HEX(COLOR_FONT_DBDBDB)
                } else if day >= 2 { //未开播
                    customCourseStatus = CourseStatus.NotOnLaunch.rawValue
                    customCurTime = " " + Date.dateStr(nextLessonStartTime.doubleValue)
                    courseBtFlag = false
                    courseStatusBtColor = HEX(COLOR_FONT_DBDBDB)
                } else { //已结束
                    customCourseStatus = CourseStatus.EndOfLaunch.rawValue
                    customCurTime = " " + Date.dateStr(nextLessonStartTime.doubleValue)
                    courseBtFlag = false
                    courseStatusBtColor = HEX(COLOR_FONT_DBDBDB)
                }
            } else { //已结束
                customCourseStatus = CourseStatus.EndOfLaunch.rawValue
                customCurTime = " " + Date.dateStr(nextLessonStartTime.doubleValue)
                courseBtFlag = false
                courseStatusBtColor = HEX(COLOR_FONT_DBDBDB)
            }
        }
    }
}
