//
//  CourseListModel.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

class CourseListModel: LessonModel {

    var indexPath: IndexPath?   //当前cell的index
    var courseDetail: String!
    var appraiseStr: String! = ""    //多少人评价
    var appraiseScoreStr: String! = ""//多少分
    var courseStatusText: String!
    var courseStatusBtWidth: CGFloat! = 0
    var courseStatusBtColor: UIColor!
    var courseStatusBtTitleColor: UIColor!
    var courseStatusBtBorderWidth: CGFloat! = 0.0
    var courseCurText: String!
    var courseBtFlag: Bool! = true //按钮是否可以点击
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        //自定义字段
        courseCurText = LocalizedString("COURSE_CUR_NUM_TEXT") + currentNo.stringValue + "/" + totalNo.stringValue + LocalizedString("COURSE_NUM_TEXT")
        switch status.intValue {
        case 0://未开播
            if countdowntime.intValue > 0 && countdowntime.intValue <= 24 * 60 * 60 * 1000 { // 24小时倒计时
                courseDetail = " " + LocalizedString("COURSE_COUNTDOWN_TEXT") + CommonSwift.timeDecrease(seconds: CLongLong(countdowntime.intValue / 1000))
                courseStatusBtColor = HEX(COLOR_FONT_DBDBDB)
                courseBtFlag = false
            } else if countdowntime.intValue > 24 * 60 * 60 * 1000 { //离开播大于1天
                courseDetail = ""
                courseStatusBtColor = HEX(COLOR_FONT_DBDBDB)
                courseBtFlag = false
            } else {// 已过开播时间，请开播
                courseDetail = ""
                courseStatusBtColor = UIColor.red
                courseBtFlag = true
            }
            courseStatusText = LocalizedString("COURSE_STATUS_NOT")
            courseStatusBtWidth = 80
            courseStatusBtBorderWidth = 0.0
            courseStatusBtTitleColor = HEX(COLOR_FONT_FFFFFF)
        case 1://正在直播
            courseDetail = ""
            courseStatusText = LocalizedString("COURSE_STATUS_ON")
            courseStatusBtWidth = 90
            courseStatusBtColor = UIColor.red
            courseStatusBtTitleColor = HEX(COLOR_FONT_FFFFFF)
            courseStatusBtBorderWidth = 0.0
            courseBtFlag = true
        case 2://已结束
            if hasRating?.boolValue == true {
                courseStatusText = LocalizedString("COURSE_APPRAISE_END")
                courseStatusBtColor = HEX(COLOR_FONT_DBDBDB)
                courseStatusBtTitleColor = HEX(COLOR_FONT_FFFFFF)
                courseStatusBtBorderWidth = 0.0
            } else {
                courseStatusText = LocalizedString("COURSE_APPRAISE")
                courseStatusBtColor = HEX(COLOR_FONT_FFFFFF)
                courseStatusBtTitleColor = HEX(COLOR_FONT_DBDBDB)
                courseStatusBtBorderWidth = 1.0
            }
            appraiseStr = StringSafty(evaluatenum?.stringValue) + LocalizedString("COURSE_APPRAISE_PEOPLE")
            appraiseScoreStr = StringSafty(evaluatescore?.stringValue) + LocalizedString("COURSE_SCORE")
            courseBtFlag = false
            courseStatusBtWidth = 80
        default:
            courseDetail = ""
            courseStatusText = LocalizedString("COURSE_STATUS_NOT")
            courseStatusBtWidth = 80
            courseStatusBtColor = HEX(COLOR_FONT_DBDBDB)
            courseStatusBtTitleColor = HEX(COLOR_FONT_FFFFFF)
            courseStatusBtBorderWidth = 0.0
            courseBtFlag = false
        }
    }
}
