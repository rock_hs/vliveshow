//
//  VLSShareModel.swift
//  VLiveShow
//
//  Created by rock on 2016/12/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

class VLSShareModel: VLSTableViewItem {
    var title: String?
    var content: String?
    var thumbImage: UIImage?
    var webpageUrl: String?
    var webpageUrlParam: NSDictionary?
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        title <- map["title"]
        content <- map["content"]
        thumbImage <- map["thumbImage"]
        webpageUrl <- map["webpageUrl"]
        webpageUrlParam <- map["webpageUrlParam"]
        
    }
}
