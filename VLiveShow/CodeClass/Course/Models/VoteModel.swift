//
//  VoteModel.swift
//  VLiveShow
//
//  Created by rock on 2016/12/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class VoteModel: NSObject {

    var residueVotes: NSNumber?     //剩余投票次数
    var totalVotes: NSNumber?      //总的投票数
    var nickname: String?
    var votedId: NSNumber?
}
