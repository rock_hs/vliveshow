//
//  CommonSwift.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class CommonSwift: NSObject {
    //MARK:VC push present
    class func pushViewController(_ vc: UIViewController, navigation: UINavigationController? = nil, animated: Bool = true) {
        vc.hidesBottomBarWhenPushed = true
        if let navigation = navigation {
            navigation.pushViewController(vc, animated: animated)
        }
    }
    
    class func pushWithVCClass(_ vcClass: AnyClass!, properties: NSDictionary?, navigation: UINavigationController? = nil) {
        let type = vcClass as! UIViewController.Type
        let vc = type.init()
        
        vc.initializeWithDictionary(properties)
        CommonSwift.pushViewController(vc, navigation: navigation)
    }
    
    class func presentWithVCClass(_ vcClass: AnyClass!, properties: NSDictionary?, navigation: UINavigationController? = nil) {
        let type = vcClass as! UIViewController.Type
        let vc = type.init()
        
        vc.initializeWithDictionary(properties)
        CommonSwift.presentViewController(vc, navigation: navigation)
    }
    
    class func presentViewController(_ vc: UIViewController, navigation: UINavigationController? = nil, animated: Bool = true) {
        vc.hidesBottomBarWhenPushed = true
        if let navigation = navigation {
            navigation.present(vc, animated: animated, completion: nil)
        }
    }
    
    //根据秒一天内倒计时(时分秒）
    class func timeDecrease(seconds: CLongLong) -> String {
        let hour = seconds / (60 * 60)
        let minute = (seconds - hour * 60 * 60) / 60
        let second = (seconds - hour * 60 * 60 - minute * 60)
        var hourStr = String(hour)
        if hour >= 0 && hour < 10 {
            hourStr = "0" + hourStr
        }
        var minuteStr = String(minute)
        if minute >= 0 && minute < 10 {
            minuteStr = "0" + minuteStr
        }
        var secondStr = String(second)
        if second >= 0 && second < 10 {
            secondStr = "0" + secondStr
        }
        let dateStr = hourStr + ":" + minuteStr + ":" + secondStr
        return dateStr
    }
}
