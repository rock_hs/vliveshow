//
//  AppMacroSwift.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

//import Foundation

//App分隔符统一颜色值
let VLSSepartorColor = HEX(0xe6e6e6)
let VLSSepartorHeight = CGFloat(0.5)

func IntSafty(_ obj: Any?) -> Int {
    if let obj = obj, ((obj as? Int) != nil) {
        return obj as! Int
    }
    return 0
}

func StringSafty(_ obj: Any?) -> String {
    guard let obj = obj else {
        return ""
    }
    return String(describing: obj)
}

func NSDictionarySafty(_ obj: Any?) -> NSDictionary {
    if let obj = obj {
        if (obj as AnyObject).isKind(of: NSDictionary.classForCoder()) {
            return obj as! NSDictionary
        }
    }
    return NSDictionary()
}

func NSArraySafty(_ obj: Any?) -> NSArray {
    if let obj = obj {
        if (obj as AnyObject).isKind(of: NSArray.classForCoder()) {
            return obj as! NSArray
        }
    }
    return NSArray()
}

func NSMutableArraySafty(_ obj: Any?) -> NSMutableArray {
    if let obj = obj {
        if (obj as AnyObject).isKind(of: NSMutableArray.classForCoder()) {
            return obj as! NSMutableArray
        }
    }
    return NSMutableArray()
}

func ObjectForKeySafety(_ obj: NSDictionary?, key: String) -> Any? {
    if let obj = obj {
        if obj.object(forKey: key) is NSNull {
            return "" as Any?
        } else {
            return obj.object(forKey: key) as Any?
        }
    }
    return nil
}

func ObjectForKeySafety(_ obj: NSArray?, index: NSInteger) -> Any? {
    if let obj = obj {
        if obj.count > 0 && obj.count > index {
            return obj.object(at: index) as Any?
        }
    }
    return nil
}

func VLSClassFromName(_ className: String) -> AnyClass {
    let name = StringSafty(Bundle.main.infoDictionary?["CFBundleExecutable"]) + "." + className
    let type: AnyClass = NSClassFromString(name)!
    return type
}

func HEX(_ rgbValue: Int32?, alpha: CGFloat = 1.0) -> UIColor {
    guard let rgbValue = rgbValue else { return UIColor.clear }
    return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0, green: ((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0, blue: ((CGFloat)(rgbValue & 0xFF))/255.0, alpha: alpha)
}

func CustomAttributeStrings(_ array: [(String, UIColor, UIFont)]) -> NSMutableAttributedString{
    let mStr = NSMutableAttributedString()
    for text in array {
        let attrText = NSAttributedString(string: text.0, attributes: [NSForegroundColorAttributeName:text.1, NSFontAttributeName: text.2])
        mStr.append(attrText)
    }
    return mStr
}

func kAttributeStrings(_ array: [(String, UIColor, UIFont, NSNumber)]) -> NSMutableAttributedString{
    let mStr = NSMutableAttributedString()
    for text in array {
        let attrText = NSAttributedString(string: text.0, attributes: [NSForegroundColorAttributeName:text.1, NSFontAttributeName: text.2, NSUnderlineStyleAttributeName: text.3])
        mStr.append(attrText)
    }
    return mStr
}

func MultilineTextSize(text:String?,font:UIFont,maxSize:CGSize,lineSpacing:CGFloat = 0) -> CGSize{
    if let text = text {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpacing
        return (text as NSString).boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName:font,NSParagraphStyleAttributeName:style], context: nil).size
    }
    return CGSize.zero
}

// 语言国际化设置y
func LocalizedString(_ key: String) -> String {
    return Bundle.main.localizedString(forKey: key, value: "", table: nil)
}
