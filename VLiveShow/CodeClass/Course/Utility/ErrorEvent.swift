//
//  ErrorEvent.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/21.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class ErrorEvent: NSObject {
    /**
     *  通过error_code，获取国际化文字提醒
     */
    class func error(_ code: NSInteger, msg: String) -> String {
        switch code {
        case 0:
            return LocalizedString("COURSE_ERRORCODE_0")
        case 30001: //课程不存在
            return LocalizedString("COURSE_ERRORCODE_30001")
        case 30006: //课程已开播
            return LocalizedString("COURSE_ERRORCODE_30006")
        case 30008: //课程未发布
            return LocalizedString("COURSE_ERRORCODE_30008")
        case 32001: //已订阅该课程
            return LocalizedString("COURSE_ERRORCODE_32001")
        case 32002, 34004: //V钻不足
            return LocalizedString("COURSE_ERRORCODE_32002")
        case 32003: //嘉宾席位已满
            return LocalizedString("COURSE_ERRORCODE_32003")
        case 32004: //不向观众开放
            return LocalizedString("COURSE_ERRORCODE_32004")
        case 32005: //未找到预订信息
            return LocalizedString("COURSE_ERRORCODE_32005")
        case 32006: //课程已退订
            return LocalizedString("COURSE_ERRORCODE_32006")
        case 32007: //不可退订他人的预订信息
            return LocalizedString("COURSE_ERRORCODE_32007")
        case 32008: //24小时内将开播的课程不可退订
            return LocalizedString("COURSE_ERRORCODE_32008")
        case 34003: //当天已投10票
            return LocalizedString("VOTE_ERRORCODE_34003")
        case 20004: //反馈内容为空或者字数太长
            return LocalizedString("VLS_FEEDBACK_ERROR_20004")
        default:
            return msg
        }
    }
}
