//
//  VLSIMExtendManager.swift
//  VLiveShow
//
//  Created by rock on 2016/12/21.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class VLSIMExtendManager: NSObject {
    
    /**
     * 针对群组消息设置优先级
     */
    class func setPriority(type: SendMsgType, msg: String, imMessage: TIMMessage) -> TIMMessage {
        var message = imMessage
        switch type {
        case SendMsgTypeCustom:
            let msgArr = msg.components(separatedBy: "&")
            if msgArr.count > 0 {
                VLSIMExtendManager.setPriority(type: Int(msgArr[0])!, imMessage: &message)
            }
        case SendMsgTypeText://聊天优先级设置为低级
            message.setPriority(.MSG_PRIORITY_LOW)
        default:
            message.setPriority(.MSG_PRIORITY_NORMAL)
        }
        return message
    }
    
    /**
     *  设置IM优先级
     */
    class func setPriority(type: Int, imMessage: inout TIMMessage) {
        switch type {
        /**************************** 最高优先级 ************************begin*/
        case Int(VLS_GUEST_ON_CAMERA): //嘉宾立即上镜
            imMessage.setPriority(.MSG_PRIORITY_HIGH)
        case Int(VLS_GUEST_CANCEL): //嘉宾取消连线
            imMessage.setPriority(.MSG_PRIORITY_HIGH)
        case Int(VLS_GUEST_MUTE_SELF)://嘉宾自己禁麦
            imMessage.setPriority(.MSG_PRIORITY_HIGH)
        case Int(VLS_ANCHOR_MUTE)://主播禁嘉宾麦
            imMessage.setPriority(.MSG_PRIORITY_HIGH)
        case Int(VLS_ANCHOR_REPLACE)://主播切换连线嘉宾位置
            imMessage.setPriority(.MSG_PRIORITY_HIGH)
        case Int(VLS_ANCHOR_CANCEL)://主播取消连线
            imMessage.setPriority(.MSG_PRIORITY_HIGH)
        /**************************** 最高优先级 ************************end*/
            
        /**************************** 普通优先级 ************************begin*/
        case Int(VLS_JOIN_ROOM): //嘉宾进入房间
            imMessage.setPriority(.MSG_PRIORITY_NORMAL)
        case Int(VLS_QUIT_ROOM): //嘉宾退出房间
            imMessage.setPriority(.MSG_PRIORITY_NORMAL)
        /**************************** 普通优先级 ************************end*/
            
        /**************************** 较低优先级 ************************begin*/
        case Int(VLS_VTICKETS_UPDATED): //V票更新
            imMessage.setPriority(.MSG_PRIORITY_LOW)
        /**************************** 较低优先级 ************************end*/
            
        /**************************** 最低优先级 ************************begin*/
        case Int(VLS_SEND_PRAISE): //嘉宾点亮屏幕
            imMessage.setPriority(.MSG_PRIORITY_LOWEST)
        case Int(VLS_PRAISE): //嘉宾点赞（出现动画✋）
            imMessage.setPriority(.MSG_PRIORITY_LOWEST)
        /**************************** 最低优先级 ************************end*/
        default:
            imMessage.setPriority(.MSG_PRIORITY_NORMAL)
        }
    }
}
