//
//  VLSTextView.swift
//  VLiveShow
//
//  Created by rock on 2017/2/22.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSTextView: UITextView {

    public var placeholderLab: UILabel?
    public var placeholder: String? {
        get {
            return placeholderLab?.text
        }
        set {
            if placeholderLab == nil {
                placeholderLab = UILabel().then {
                    $0.text = StringSafty(placeholder)
                    $0.textColor = HEX(COLOR_FONT_AAAAAA)
                    $0.sizeToFit()
                    $0.isHidden = false
                    $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
                    $0.numberOfLines = 0
                }
                if let placeholderLab = placeholderLab {
                    self.addSubview(placeholderLab)
                    placeholderLab.snp.makeConstraints { (make) in
                        make.top.left.equalTo(self).offset(8)
                        make.right.equalTo(self)
                    }
                }
            }
            placeholderLab?.text = newValue
        }
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        NotificationCenter.default.addObserver(self, selector: #selector(VLSTextView.refreshPlaceholder), name: NSNotification.Name.UITextViewTextDidChange, object: nil)
    }
    
    func refreshPlaceholder() {
        if let placeholderLab = placeholderLab {
            placeholderLab.isHidden = !self.text.isEmpty
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
