//
//  UIButton.swift
//  VLiveShow
//
//  Created by rock on 2017/2/23.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import Foundation

extension UIButton {
    
    func verticalImageAndTitle(spacing: CGFloat) {
        if let titleLabel = titleLabel, let imageView = imageView {
            let imageSize = imageView.frame.size
            var titleSize = titleLabel.frame.size
            let textSize = NSString(string: StringSafty(titleLabel.text)).size(attributes: [NSFontAttributeName: titleLabel.font])
            let frameSize = CGSize(width: CGFloat(ceilf(Float(textSize.width))), height: CGFloat(ceilf(Float(textSize.height))))
            if titleSize.width + 0.5 < frameSize.width {
                titleSize.width = frameSize.width
            }
            let totalHeight = (imageSize.height + titleSize.height + spacing)
            imageEdgeInsets = UIEdgeInsetsMake(-(totalHeight - imageSize.height), 0, 0, -titleSize.width)
            titleEdgeInsets = UIEdgeInsetsMake(0, -imageSize.width, -(totalHeight - titleSize.height), 0)
        }
    }
}
