//
//  VLSShareManager.swift
//  VLiveShow
//
//  Created by rock on 2016/12/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

typealias shareHandler = (_ result: NSDictionary?) -> Void
typealias authSuccHandler = (_ item: UMSocialUserInfoResponse?) -> Void

class VLSShareManager: NSObject {
    
    //授权登录
    class func getAuthWithUserInfo(_ platformType: UMSocialPlatformType, complete: @escaping authSuccHandler) {
        UMSocialManager.default().getUserInfo(with: platformType, currentViewController: nil, completion: { (result, error) in
            if let result = result as? UMSocialUserInfoResponse {
                result.gender = result.gender == "男" ? "MALE" : "FEMALE"
                complete(result)
            }
        })
    }
    
    //分享
    class func share(_ platformType: NSInteger, messageObject: VLSShareModel, currentViewController: UIViewController?, complete: @escaping shareHandler) {
        
        var platform: UMSocialPlatformType = UMSocialPlatformType.QQ
        switch platformType {
        case 0:
            platform = UMSocialPlatformType.QQ
        case 1:
            platform = UMSocialPlatformType.qzone
        case 2:
            platform = UMSocialPlatformType.wechatSession
        case 3:
            platform = UMSocialPlatformType.wechatTimeLine
        case 4:
            platform = UMSocialPlatformType.sina
        default:
            break
        }
        let object = UMSocialMessageObject.init()
        object.text = messageObject.content
        let shareObject = UMShareWebpageObject.init()
        shareObject.title = messageObject.title
        shareObject.descr = messageObject.content
        shareObject.thumbImage = messageObject.thumbImage
        shareObject.webpageUrl = messageObject.webpageUrl
        object.shareObject = shareObject
        
        UMSocialManager.default().share(to: platform, messageObject: object, currentViewController: currentViewController, completion: { (result, error) in
            if error == nil {
                complete(NSDictionarySafty(result))
            }
        })
    }
}
