//
//  VLSAOPManager.swift
//  VLiveShow
//
//  Created by rock on 2016/12/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import Aspects
import SDWebImage
import Foundation

class VLSAOPManager: NSObject {

    class func hookManager() {
        VLSAOPManager.hookSDWebImage()
        VLSAOPManager.hookUIControl()
    }
    
    //针对SDWebImage 获取图片 支持 ssl 进行切片
    class func hookSDWebImage() {
        let wrappedBlock:@convention(block) (AspectInfo) -> Void = { aspectInfo in
            let instance = aspectInfo.instance() as? UIImageView
            let arguments = NSArraySafty(aspectInfo.arguments())
            if arguments.count == 2 {
                instance?.sd_setImage(with: URL(string: StringSafty(arguments[0])), placeholderImage: arguments[1] as? UIImage, options: SDWebImageOptions.allowInvalidSSLCertificates)
            }
        }
        let wrappedObject: AnyObject = unsafeBitCast(wrappedBlock, to: AnyObject.self)
        do {
            try UIImageView.aspect_hook(#selector(UIImageView.sd_setImage(with:placeholderImage:)), with: AspectOptions.positionInstead, usingBlock: wrappedObject)
        } catch {
            print(error)
        }
    }
    
    //针对UIControl 防止1秒内多次点击
    class func hookUIControl() {
        let wrappedBlock:@convention(block) (AspectInfo) -> Void = { aspectInfo in
            if let instance = aspectInfo.instance() as? UIControl {
                let arguments = NSArraySafty(aspectInfo.arguments())
                if arguments.count == 3 {
                    if let _ = arguments[1] as? UITabBar {
                        instance.setCs_acceptEventInterval(0)
                    } else if instance.cs_acceptEventIntervalFlag() == true {
                        instance.setCs_acceptEventInterval(0)
                    } else {//默认1秒内点击一次
                        instance.setCs_acceptEventInterval(1)
                    }
                }
            }
        }
        let wrappedObject: AnyObject = unsafeBitCast(wrappedBlock, to: AnyObject.self)
        do {
            try UIControl.aspect_hook(#selector(UIControl.cs_sendAction(_:to:for:)), with: AspectOptions.positionBefore, usingBlock: wrappedObject)
        } catch {
            print(error)
        }
    }
}
