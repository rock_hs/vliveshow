////
////  UIViewController.swift
////  VLiveShow
////
////  Created by vipabc on 2016/11/22.
////  Copyright © 2016年 vliveshow. All rights reserved.
////
//
//import Foundation
//import Aspects
//
//extension UIViewController {
//    open override static func initialize() {
//        let once: () = {
//            if self != UIViewController.self || self == VLSNavigationController.self {
//                return
//            }
//            let wrappedBlock:@convention(block) (AspectInfo) -> Void = { aspectInfo in
//                let instance = aspectInfo.instance() as? UIViewController
////                self.beginLogPageView(pageView: instance?.classForCoder)
//            }
//            let wrappedObject: AnyObject = unsafeBitCast(wrappedBlock, to: AnyObject.self)
//            let endwrappedBlock:@convention(block) (AspectInfo) -> Void = { aspectInfo in
//                let instance = aspectInfo.instance() as? UIViewController
////                self.beginLogPageView(pageView: instance?.classForCoder)
//            }
//            let endwrappedObject: AnyObject = unsafeBitCast(endwrappedBlock, to: AnyObject.self)
//            do {
//                try UIViewController.aspect_hook(#selector(UIViewController.viewWillAppear(_:)), with: AspectOptions.positionInstead, usingBlock: wrappedObject)
//                try UIViewController.aspect_hook(#selector(UIViewController.viewWillDisappear(_:)), with: AspectOptions.positionBefore, usingBlock: endwrappedObject)
//            } catch {
//                print(error)
//            }
//
//        }()
//    }
//    
//    class func beginLogPageView(pageView: AnyClass?) {
//        
//    }
//}
