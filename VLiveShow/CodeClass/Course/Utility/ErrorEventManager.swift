////
////  ErrorEventManager.swift
////  VLiveShow
////
////  Created by vipabc on 2016/11/21.
////  Copyright © 2016年 vliveshow. All rights reserved.
////
//
//import UIKit
//import Aspects
//
//class ErrorEventManager: NSObject {
//    
//    static let shared = ErrorEventManager()
//    
//    override init() {
//        
//        let wrappedBlock:@convention(block) (AspectInfo) -> Void = { aspectInfo in
////            let instance = aspectInfo.instance() as? UIViewController
//            print(aspectInfo)
//        }
//        let wrappedObject: AnyObject = unsafeBitCast(wrappedBlock, to: AnyObject.self)
//        do {
//            try ErrorEvent.aspect_hook(#selector(ErrorEvent.error(_:msg:)), with: AspectOptions.positionBefore, usingBlock: wrappedObject)
//        } catch {
//            print(error)
//        }
//        
//    }
//
//}
