//
//  FinishedCourseCell.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class FinishedCourseCell: CourseCell {

    fileprivate var courseNextTitleLab: UILabel!
    fileprivate var courseStatusBt: UIButton!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.initBottomView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //底部视图
    func initBottomView() {
        let bottomView = UIView()
        backView.addSubview(bottomView)
        bottomView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topView.snp.bottom)
            make.left.right.equalTo(backView)
            make.bottom.equalTo(backView)
        }
        //课程状态
        courseStatusBt = UIButton().then {
            $0.backgroundColor = HEX(COLOR_FONT_DBDBDB)
            $0.setTitleColor(UIColor.white, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.isEnabled = false
            $0.layer.cornerRadius = 6
        }
        bottomView.addSubview(courseStatusBt)
        courseStatusBt.snp.makeConstraints { (make) in
            make.right.equalTo(-6)
            make.top.equalTo(16)
            make.size.equalTo(CGSize(width: 70, height: 26))
            make.bottom.equalTo(bottomView.snp.bottom).offset(-10)
        }
        //直播间标题
        courseNextTitleLab = UILabel().then {
            $0.text = "给家人的心仪来调整每一份甜点，这就是纯净的好滋味"
            $0.textColor = HEX(COLOR_FONT_4A4A4A)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_18))
            $0.numberOfLines = 2
            $0.sizeToFit()
        }
        bottomView.addSubview(courseNextTitleLab)
        courseNextTitleLab.snp.makeConstraints { (make) in
            make.top.equalTo(courseStatusBt.snp.top)
            make.left.equalTo(bottomView.snp.left).offset(6)
            make.right.equalTo(courseStatusBt.snp.left).offset(-20)
            make.bottom.equalTo(bottomView.snp.bottom).offset(-20)
        }
    }
    
    override func setNewItem(_ item: VLSTableViewItem) {
        super.setNewItem(item)
        
        if let newItem = item as? PublishCourseModel {
            if let courseImageUrl = NSURL(string: newItem.picCover) {
                courseImageView.sd_setImage(with: courseImageUrl as URL!)
                courseTitleLab.text = newItem.courseName
                courseNextTitleLab.text = newItem.nextLessonName
                switch newItem.customCourseStatus {
                case CourseStatus.EndOfLaunch.rawValue:
                    courseStatusBt.setTitle(LocalizedString("COURSE_STATUS_END"), for: .normal)
                case CourseStatus.OffTheLaunch.rawValue:
                    courseStatusBt.setTitle(LocalizedString("COURSE_STATUS_OFF"), for: .normal)
                default:
                    courseStatusBt.setTitle(LocalizedString("COURSE_STATUS_END"), for: .normal)
                }
            }
        }
    }
}
