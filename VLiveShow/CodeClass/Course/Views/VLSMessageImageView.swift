//
//  VLSMessageImageView.swift
//  VLiveShow
//
//  Created by rock on 2017/1/5.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSMessageImageViewDelegate {
    func message()
}

class VLSMessageImageView: UIView {

    weak var delegate: VLSMessageImageViewDelegate?
    private var observerContext = 0
    private var messageBt: UIButton!
    private var dotColor: UIColor! = UIColor.red
    
    static let shared = VLSMessageImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 16))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        messageBt = UIButton().then {
            $0.setImage(UIImage(named: "home_massage")?.withRenderingMode(.alwaysTemplate), for: .normal)
            $0.addTarget(self, action: #selector(VLSMessageImageView.message), for: .touchUpInside)
        }
        self.changeTintColor(HEX(COLOR_BG_FFFFFF))
        self.addSubview(messageBt)
        messageBt.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
        // 监听Manager的newmail属性
        VLSMessageManager.shared().addObserver(self, forKeyPath: "newMail", options: [.new, .old], context: &observerContext)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &observerContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        if keyPath == "newMail" {
            if VLSMessageManager.shared().newMail {
                self.dot(dotColor)
            } else {
                self.removeDot()
            }
        }
    }
    
    func changeTintColor(_ color: UIColor) {
        messageBt.tintColor = color
    }
    
    func changeDotColor(_ color: UIColor) {
        dotColor = color
        if VLSMessageManager.shared().newMail {
            self.removeDot()
            self.dot(dotColor)
        }
    }
    
    func message() {
        //隐藏点点
        if VLSMessageManager.shared().newMail {
            self.removeDot()
            VLSMessageManager.shared().newMail = false
        }
        self.delegate?.message()
    }
    
    deinit {
        VLSMessageManager.shared().removeObserver(self, forKeyPath: "newMail")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
