//
//  VLSCollectionView.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSCollectionView: UIView {

    var collectionView: UICollectionView!
    var viewModel: VLSTableViewModel!
    var viewModelClass: AnyClass? {
        didSet {
            self.initViewModel()
            self.setViewModelLoadEvent()
        }
    }
    fileprivate var errorView: VLSEmptyView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initViewModel()
        self.setViewModelLoadEvent()
        let flowLayout = UICollectionViewFlowLayout().then {
            $0.scrollDirection = .horizontal
            $0.itemSize = CGSize(width: self.bounds.width/7, height: self.bounds.height)
        }
        viewModel.cellClass = UICollectionViewCell.self
        collectionView = UICollectionView(frame: self.bounds, collectionViewLayout: flowLayout)
        collectionView.register(viewModel.cellClass, forCellWithReuseIdentifier: "collectionCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = HEX(COLOR_BG_FFFFFF)

        self.addSubview(collectionView)
    }
    
    //重新加载数据
    func reloadData() {
        viewModel.page = 0
        viewModel.pageDirection = 1
        viewModel.loadDataSource()
    }
    
    //加载更多
    func loadMoreData() {
        viewModel.pageDirection = 1  //加载更多
        viewModel.loadDataSource()
    }
    
    //初始化viewModel
    func initViewModel() {
        if let viewModelClass = viewModelClass, let type = viewModelClass as? VLSTableViewModel.Type {
            viewModel = type.init()
        } else {
            viewModel = VLSTableViewModel()
        }
    }
    
    fileprivate func setViewModelLoadEvent() {//设置加载完成时间
        viewModel.loadSuccess = {[weak self] in
            self?.setViewModelLoadSuccess()
        }
        viewModel.loadEmpty = {[weak self] in
            self?.setViewModelLoadEmpty()
        }
        viewModel.loadFailed = {[weak self] in
            self?.setViewModelLoadFailed()
        }
        viewModel.loadMoreData = {
            [weak self](moreData,showFootor) in
            self?.setViewModelLoadMore(moreData, showFootor: showFootor)
        }
    }
    
    func setViewModelLoadSuccess() {//加载成功
        self.removeErrorView()
        collectionView.reloadData()
    }
    
    func setViewModelLoadEmpty() {//无数据
        self.addErrorView()
        collectionView.reloadData()
    }
    
    func setViewModelLoadFailed() {//加载失败
        self.addErrorView()
        collectionView.reloadData()
    }
    
    func setViewModelLoadMore(_ moreData:Bool,showFootor:Bool) {//加载更多
        self.setViewModelLoadSuccess()
    }
    
    func addErrorView() {
        let errorView = VLSEmptyView(frame: self.bounds)
        self.addErrorView(errorView)
    }
    
    //添加错误视图
    func addErrorView(_ errorView: VLSEmptyView?) {
        if self.errorView == nil {
            if let errorView = errorView {
                self.errorView = errorView
                self.addSubview(errorView)
            }
        }
    }
    
    func removeErrorView() {
        errorView?.removeFromSuperview()
        errorView = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension VLSCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //pragma mark -- UICollectionViewDataSource
    //定义展示的UICollectionViewCell个数
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)
        return cell
    }
    
    //pragma mark --UICollectionViewDelegateFlowLayout
    //定义每个UICollectionView 的 margin
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    //cell的最小行间距
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //cell的最小列间距
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func tryAction() {
        
    }
}
