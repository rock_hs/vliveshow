//
//  VLSScheduleEmptyCollectionViewCell.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSScheduleEmptyCollectionViewCell: UICollectionViewCell {
    
    fileprivate var coverImageView: UIImageView!
    fileprivate var tagLab: UILabel!
    fileprivate var avatarImageView: UIImageView!
    fileprivate var courseTitleLab: UILabel!
    fileprivate var courseTimeLab: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let backView = UIView().then {
            $0.backgroundColor = HEX(COLOR_BG_FFFFFF)
        }
        self.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
        coverImageView = UIImageView().then {//封面
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
        backView.addSubview(coverImageView)
        coverImageView.snp.makeConstraints { (make) in
            make.top.right.equalTo(backView)
            make.left.equalTo(scheduleSpace)
            make.height.equalTo(100)
        }
        tagLab = UILabel().then {//标志
            $0.backgroundColor = HEX(COLOR_BG_C3A851)
            $0.textColor = HEX(COLOR_BG_FFFFFF)
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_13))
            $0.layer.cornerRadius = 12
            $0.clipsToBounds = true
        }
        coverImageView.addSubview(tagLab)
        tagLab.snp.makeConstraints { (make) in
            make.top.equalTo(coverImageView.snp.top).offset(10)
            make.right.equalTo(coverImageView.snp.right).offset(-scheduleSpace)
            make.size.equalTo(CGSize(width: 50, height: 24))
        }
        avatarImageView = UIImageView().then {//头像
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
            $0.layer.cornerRadius = 20
        }
        backView.addSubview(avatarImageView)
        avatarImageView.snp.makeConstraints { (make) in
            make.top.equalTo(coverImageView.snp.bottom).offset(10)
            make.left.equalTo(coverImageView.snp.left)
            make.size.equalTo(CGSize(width: 40, height: 40))
        }
        courseTitleLab = UILabel().then {//标题
            $0.textColor = HEX(COLOR_BG_9B9B9B)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
            $0.sizeToFit()
        }
        backView.addSubview(courseTitleLab)
        courseTitleLab.snp.makeConstraints { (make) in
            make.top.equalTo(avatarImageView.snp.top)
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.right.equalTo(backView.snp.right).offset(-scheduleSpace)
        }
        courseTimeLab = UILabel().then{//预约时间
            $0.textColor = HEX(COLOR_BG_9B9B9B)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_11))
            $0.sizeToFit()
            $0.adjustsFontSizeToFitWidth = true
        }
        backView.addSubview(courseTimeLab)
        courseTimeLab.snp.makeConstraints { (make) in
            make.top.equalTo(courseTitleLab.snp.bottom).offset(2)
            make.left.right.equalTo(courseTitleLab)
            make.bottom.equalTo(avatarImageView.snp.bottom)
        }
    }
    
    func setNewItem(_ item: VLSScheduleRecommendationModel?) {
        if let item = item {
            if let teacherURL = URL(string: StringSafty(item.picTeacher)) {
                avatarImageView.sd_setImage(with: teacherURL)
            }
            if let coverURL = URL(string: StringSafty(item.picCover)) {
                coverImageView.sd_setImage(with: coverURL)
            }
            courseTitleLab.text = StringSafty(item.courseName)
            if let startTime = item.startTime?.doubleValue, let endTime = item.endTime?.doubleValue {
                courseTimeLab.text = StringSafty(Date.dateStr(startTime, formatter: "yyyy年MM月dd日 a HH:mm")) + "-" + StringSafty(Date.dateStr(endTime, formatter: "HH:mm"))
            }
            tagLab.text = StringSafty(LocalizedString("COURSE_RECOMMEND_TYPE_TEXT"))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
