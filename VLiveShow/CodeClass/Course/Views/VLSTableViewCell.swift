//
//  VLSTableViewCell.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class VLSTableViewCell: UITableViewCell {

    var item: VLSTableViewItem?
    var selectedBgView: UIView!
    var vcClass: AnyClass?
    var properties: NSDictionary?
    var indexPath: IndexPath?
    var navigation: UINavigationController?
    private var sepLine: UIView!
    var hiddenSepLine = false{
        didSet{
            self.sepLine.isHidden = hiddenSepLine
        }
    }
    var canLongPress:Bool = true
    //是否取消长按
    private var cancelLongPressFlag: Bool?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        selectedBgView = UIView().then {
            $0.backgroundColor = UIColor(white: 0.3, alpha: 0)
            let tap = UITapGestureRecognizer(target: self, action: #selector(VLSTableViewCell.click(_:)))
            $0.addGestureRecognizer(tap)
        }
        
        self.addSubview(selectedBgView)
        selectedBgView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        //分隔符
        sepLine = UIView().then {
            $0.backgroundColor = VLSSepartorColor
        }
        self.addSubview(sepLine)
        sepLine.snp.makeConstraints { (make) in
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.bottom.equalTo(self)
            make.height.equalTo(0.5)
        }
        //添加长按手势
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(VLSTableViewCell.longpress(_:))).then {
            $0.numberOfTapsRequired = 0
            $0.minimumPressDuration = 0.1;
        }
        selectedBgView.addGestureRecognizer(longpress)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setNewItem(_ item: VLSTableViewItem) {
        self.item = item
    }
    
    func click(_ sender: AnyObject) {
        self.selectedBgView?.backgroundColor = UIColor(white: 0.3, alpha: 0.3)
        self.perform(#selector(VLSTableViewCell.deselect), with: nil, afterDelay: 0.25)
        if let vcClass = vcClass {
            CommonSwift.pushWithVCClass(vcClass, properties: self.properties)
        }
    }
    
    func longpress(_ ges: UILongPressGestureRecognizer) {
        if canLongPress == false {
            return
        }
        if ges.state == .began {
            self.selectedBgView?.backgroundColor = UIColor(white: 0.3, alpha: 0.3)
        } else if ges.state == .ended {
            if cancelLongPressFlag == false {
                self.selectedBgView?.backgroundColor = UIColor(white: 0.3, alpha: 0)
                self.click(ges)
            }
            cancelLongPressFlag = false
        } else if ges.state == .changed || ges.state == .cancelled || ges.state == .failed {
            self.selectedBgView?.backgroundColor = UIColor(white: 0.3, alpha: 0)
            cancelLongPressFlag = true
        }
    }
    
    func deselect() {
        self.selectedBgView?.backgroundColor = UIColor(white: 0.3, alpha: 0)
    }

}
