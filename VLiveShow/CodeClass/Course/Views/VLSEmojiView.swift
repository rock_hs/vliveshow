//
//  VLSEmojiView.swift
//  VLiveShow
//
//  Created by rock on 2017/2/7.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

let emojiPageNum = 21

protocol VLSEmojiViewDelegate: NSObjectProtocol {
    func emoji(_ text: String)
}

class VLSEmojiView: VLSCollectionView {
    
    weak var delegate: VLSEmojiViewDelegate?
    var footerView: VLSEmojiFooterView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = HEX(COLOR_BG_EEEEEE)
        
        footerView = VLSEmojiFooterView()
        self.addSubview(footerView)
        footerView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self)
            make.height.equalTo(30)
        }
        
        let flowLayout = VLSCollectionViewFlowLayout().then {
            $0.itemCount_Column = 7
            $0.itemCount_Row = 3
            $0.scrollDirection = .horizontal
        }
        collectionView.collectionViewLayout = flowLayout
        collectionView.register(VLSEmojiCell.self, forCellWithReuseIdentifier: "emojiCell")
        collectionView.backgroundColor = HEX(COLOR_BG_EEEEEE)
        collectionView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self)
            make.bottom.equalTo(footerView.snp.top)
        }
        viewModel.dataSource = getEmojiAll()
        collectionView.reloadData()
        footerView.numberOfPages(num: viewModel.dataSource.count)
    }
    
    func getEmojiAll() -> NSMutableArray {
        if let path = Bundle.main.path(forResource: "emoji", ofType: "plist") {
            if let dic = NSDictionary(contentsOfFile: StringSafty(path)) {
                let emojiArr = NSMutableArray(array: NSArraySafty(ObjectForKeySafety(dic, key: "emojiList")))
                //重新对emoji分组
                let pageNum: Int = Int(emojiArr.count / (emojiPageNum - 1)) + (emojiArr.count % (emojiPageNum - 1) > 0 ? 1 : 0)
                let emojis: NSMutableArray = NSMutableArray(capacity: 0)
                for i in 0..<pageNum {
                    if (i + 1) * (emojiPageNum - 1) >= emojiArr.count {
                        emojis.addObjects(from: emojiArr.subarray(with: NSMakeRange(i * (emojiPageNum - 1), emojiArr.count - (i * (emojiPageNum - 1)))))
                        for _ in emojiArr.count..<(i + 1) * (emojiPageNum - 1) {
                            emojis.add("")
                        }
                    } else {
                        emojis.addObjects(from: emojiArr.subarray(with: NSMakeRange(i * (emojiPageNum - 1), (emojiPageNum - 1))))
                    }
                    emojis.add("delete")
                }
                return emojis
            }
        }
        return NSMutableArray()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VLSEmojiView {
    
    //cell的最小行间距
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "emojiCell", for: indexPath)
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            if let cell = cell as? VLSEmojiCell {
                cell.setNewItem(StringSafty(item))
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/7, height: collectionView.frame.height/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //进入课程详情
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            delegate?.emoji(StringSafty(item))
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = Int(fabs(scrollView.contentOffset.x)/scrollView.frame.width)
        footerView.currentPage(num: index)
    }
}

@objc protocol VLSEmojiCellDelegate {
    func emoji(text: String)
}

class VLSEmojiCell: UICollectionViewCell {
    private var emojiBt: UIButton!
    weak var delegate: VLSEmojiCellDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        emojiBt = UIButton().then {
            $0.isEnabled = false
            $0.titleLabel?.font = UIFont.systemFont(ofSize: 32)
        }
        self.contentView.addSubview(emojiBt)
        emojiBt.snp.makeConstraints { (make) in
            make.left.right.centerY.equalTo(self)
            make.height.equalTo(32)
        }
    }
    
    func setNewItem(_ item: String?) {
        if let item = item {
            if item == "delete" {
                emojiBt.setImage(#imageLiteral(resourceName: "emoji_delete"), for: .normal)
                emojiBt.setTitle("", for: .normal)
            } else {
                emojiBt.setTitle(StringSafty(item), for: .normal)
                emojiBt.setImage(UIImage(), for: .normal)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class VLSEmojiFooterView: UICollectionReusableView {
    var pageControl: UIPageControl!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = HEX(COLOR_BG_EEEEEE)
        
        pageControl = UIPageControl().then {
            $0.currentPageIndicatorTintColor = HEX(COLOR_FONT_4A4A4A)
            $0.pageIndicatorTintColor = HEX(COLOR_FONT_C6C6C6)
        }
        self.addSubview(pageControl)
        pageControl.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
    }
    
    func numberOfPages(num: Int) {
        let pageNum = Int(num / emojiPageNum) + (num % emojiPageNum > 0 ? 1 : 0)
        pageControl.numberOfPages = pageNum
    }
    
    func currentPage(num: Int) {
        pageControl.currentPage = num
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
