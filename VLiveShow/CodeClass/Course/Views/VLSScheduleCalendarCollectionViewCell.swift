//
//  VLSScheduleCalendarCollectionViewCell.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSScheduleCalendarCollectionViewCell: UICollectionViewCell {
    
    fileprivate var weeklyLab: UILabel! //周（一至日）
    fileprivate var dayLab: UILabel!    //日（1至31号）
    fileprivate var courseSignLab: UILabel! //区分当天是否有课
    override init(frame: CGRect) {
        super.init(frame: frame)
        let backView = UIView().then {
            $0.backgroundColor = HEX(COLOR_BG_FFFFFF)
        }
        self.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
        weeklyLab = UILabel().then {
            $0.textColor = HEX(COLOR_FONT_9B9B9B)
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_13))
            $0.sizeToFit()
        }
        backView.addSubview(weeklyLab)
        weeklyLab.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.right.equalTo(backView)
            make.height.equalTo(24)
        }
        dayLab = UILabel().then {
            $0.textColor = HEX(COLOR_FONT_9B9B9B)
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_18))
            $0.sizeToFit()
            $0.layer.cornerRadius = 12
            $0.clipsToBounds = true
            $0.adjustsFontSizeToFitWidth = true
        }
        backView.addSubview(dayLab)
        dayLab.snp.makeConstraints { (make) in
            make.top.equalTo(weeklyLab.snp.bottom).offset(2)
            make.left.right.equalTo(backView)
            make.height.equalTo(24)
        }
        courseSignLab = UILabel().then {
            $0.backgroundColor = HEX(COLOR_BG_9B9B9B)
            $0.isHidden = true
            $0.layer.cornerRadius = 4
            $0.clipsToBounds = true
        }
        backView.addSubview(courseSignLab)
        courseSignLab.snp.makeConstraints { (make) in
            make.top.equalTo(dayLab.snp.bottom).offset(2)
            make.centerX.equalTo(backView.snp.centerX)
            make.size.equalTo(CGSize(width: 8, height: 8))
            make.bottom.equalTo(backView).offset(-6)
        }
    }
    
    func setNewItem(_ item: VLSScheduleDateModel?) {
        if let item = item {
            weeklyLab.text = Date.weekday(from: Date.date(StringSafty(item.date), formatter: "yyyy-MM-dd"))
            dayLab.text = Date.day(from: Date.date(StringSafty(item.date), formatter: "yyyy-MM-dd"))
            if let dateCount = item.datecount?.intValue {
                courseSignLab.isHidden = dateCount > 0 ? false : true
            }
            if item.isSelectedFlag {//如果选中
                dayLab.backgroundColor = HEX(COLOR_BG_FF1130)
                dayLab.textColor = HEX(COLOR_FONT_FFFFFF)
            } else {
                dayLab.backgroundColor = HEX(COLOR_BG_FFFFFF)
                dayLab.textColor = HEX(COLOR_FONT_9B9B9B)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
