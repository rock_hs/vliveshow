//
//  OnCourseCell.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class OnCourseCell: CourseCell {

    fileprivate var courseNextLab: UILabel!
    fileprivate var courseSurplusLab: UILabel!
    fileprivate var courseNextTitleLab: UILabel!
    fileprivate var courseIntroducTextField: UITextField!
    fileprivate var courseEnterBt: UIButton!
    fileprivate var coursePaybackBt: UIButton!
    fileprivate var surplusCountDownSeconds: CLongLong! = 0  //复用cell，已跑多久

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.initBottomView()
        
    }
    //底部视图
    func initBottomView() {
        let bottomView = UIView()
        backView.addSubview(bottomView)
        bottomView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topView.snp.bottom)
            make.left.right.equalTo(self.topView)
            make.bottom.equalTo(self)
        }
        //下节直播
        courseNextLab = UILabel().then {
            $0.text = LocalizedString("COURSE_NEXT_TITLE")
            $0.textColor = HEX(COLOR_FONT_464646)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12))
            $0.sizeToFit()
        }
        bottomView.addSubview(courseNextLab)
        courseNextLab.snp.makeConstraints { (make) in
            make.top.equalTo(bottomView).offset(6)
            make.left.equalTo(bottomView.snp.left).offset(6)
        }
        //剩余课时
        courseSurplusLab = UILabel().then {
            $0.text = "第1/10课时"
            $0.textAlignment = .right
            $0.textColor = HEX(COLOR_FONT_464646)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12))
            $0.sizeToFit()
        }
        bottomView.addSubview(courseSurplusLab)
        courseSurplusLab.snp.makeConstraints { (make) in
            make.top.equalTo(courseNextLab.snp.top)
            make.right.equalTo(bottomView.snp.right).offset(-6)
            make.height.equalTo(courseNextLab.snp.height)
            make.left.equalTo(courseNextLab.snp.right).offset(20).priority(1000)
        }
        //下节课标题
        courseNextTitleLab = UILabel().then {
            $0.text = "给家人的心仪来调整每一份甜点，这就是纯净的好滋味"
            $0.textColor = HEX(COLOR_FONT_4A4A4A)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_18))
            $0.numberOfLines = 2
            $0.sizeToFit()
        }
        bottomView.addSubview(courseNextTitleLab)
        courseNextTitleLab.snp.makeConstraints { (make) in
            make.top.equalTo(courseNextLab.snp.bottom).offset(10)
            make.left.equalTo(courseNextLab.snp.left)
            make.right.equalTo(courseSurplusLab.snp.right)
        }
        //分割线
        let sepatorLab = UILabel().then {
            $0.backgroundColor = VLSSepartorColor
        }
        bottomView.addSubview(sepatorLab)
        sepatorLab.snp.makeConstraints { (make) in
            make.left.right.equalTo(courseNextTitleLab)
            make.top.equalTo(courseNextTitleLab.snp.bottom).offset(10)
            make.height.equalTo(VLSSepartorHeight)
        }
        //左边标题
        courseIntroducTextField = UITextField().then {
            $0.leftViewMode = .always
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12))
            $0.isEnabled = false
        }
        bottomView.addSubview(courseIntroducTextField)
        courseIntroducTextField.snp.makeConstraints { (make) in
            make.top.equalTo(sepatorLab.snp.bottom).offset(20)
            make.left.equalTo(sepatorLab.snp.left)
            make.bottom.equalTo(bottomView.snp.bottom).offset(-14)
        }
        //进入直播间或开始直播
        courseEnterBt = UIButton().then {
            $0.setTitle("开始直播", for: .normal)
            $0.setTitleColor(HEX(COLOR_FONT_FFFFFF), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.layer.cornerRadius = 6
            $0.layer.masksToBounds = true
            $0.addTarget(self, action: #selector(OnCourseCell.jumpToRoomAction), for: .touchUpInside)
        }
        bottomView.addSubview(courseEnterBt)
        courseEnterBt.snp.makeConstraints { (make) in
            make.top.equalTo(sepatorLab.snp.bottom).offset(10)
            make.right.equalTo(sepatorLab.snp.right)
            make.size.equalTo(CGSize(width: 80, height: 26))
            make.bottom.equalTo(courseIntroducTextField.snp.bottom).offset(4)
        }
        //退款
        coursePaybackBt = UIButton().then {
            $0.setTitle("退款", for: .normal)
            $0.setTitleColor(HEX(COLOR_FONT_DBDBDB), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.layer.cornerRadius = 6
            $0.layer.masksToBounds = true
            $0.layer.borderColor = HEX(COLOR_FONT_DBDBDB).cgColor
            $0.layer.borderWidth = 1.0
            $0.isHidden = true
            $0.addTarget(self, action: #selector(OnCourseCell.paybackAction), for: .touchUpInside)
        }
        bottomView.addSubview(coursePaybackBt)
        coursePaybackBt.snp.makeConstraints { (make) in
            make.top.equalTo(courseEnterBt.snp.top)
            make.right.equalTo(courseEnterBt.snp.left).offset(-10)
            make.size.equalTo(CGSize(width: 50, height: 26))
            make.height.equalTo(courseEnterBt.snp.height)
        }
    }
    
    override func setNewItem(_ item: VLSTableViewItem) {
        super.setNewItem(item)
        if let newItem = item as? PublishCourseModel {
            courseImageView.sd_setImage(with: URL(string: StringSafty(newItem.picCover)))
            courseTitleLab.text = newItem.courseName
            courseSurplusLab.text = newItem.customCurLesson
            courseSurplusLab.sizeToFit()
            courseNextTitleLab.text = newItem.nextLessonName
            courseIntroducTextField.leftView = UIImageView().then {
                $0.image = #imageLiteral(resourceName: "time")
                $0.frame = CGRect(x: 0, y: 0, width: 14, height: 14)
            }
            courseIntroducTextField.textColor = HEX(COLOR_FONT_9B9B9B)
            
            switch newItem.customCourseStatus {
            case CourseStatus.NotOnLaunch.rawValue://未开播
                courseIntroducTextField.text = newItem.customCurTime
                if self.pageName == "booking" {
                    coursePaybackBt.isHidden = false
                    courseEnterBt.setTitle(LocalizedString("COURSE_STATUS_ON"), for: .normal)
                    courseEnterBt.snp.updateConstraints({ (make) in
                        make.width.equalTo(90)
                    })
                } else {
                    coursePaybackBt.isHidden = true
                    courseEnterBt.setTitle(LocalizedString("COURSE_STATUS_NOT"), for: .normal)
                    courseEnterBt.snp.updateConstraints({ (make) in
                        make.width.equalTo(80)
                    })
                }
            case CourseStatus.LaunchCountDown.rawValue://倒计时
                //开播倒计时
                courseIntroducTextField.leftView = UIImageView().then {
                    $0.image = #imageLiteral(resourceName: "time_selected")
                    $0.frame = CGRect(x: 0, y: 0, width: 14, height: 14)
                }
                courseIntroducTextField.textColor = UIColor.red
                self.perform(#selector(OnCourseCell.resetTime))
                if self.pageName == "booking" {
                    coursePaybackBt.isHidden = false
                    courseEnterBt.setTitle(LocalizedString("COURSE_STATUS_ON"), for: .normal)
                } else {
                    coursePaybackBt.isHidden = true
                    courseEnterBt.setTitle(LocalizedString("COURSE_STATUS_NOT"), for: .normal)
                }
            case CourseStatus.HasBeenLaunch.rawValue://已开播
                courseIntroducTextField.text = newItem.customCurTime
                if self.pageName == "booking" {
                    courseEnterBt.setTitle(LocalizedString("COURSE_STATUS_ON"), for: .normal)
                    courseEnterBt.snp.updateConstraints({ (make) in
                        make.width.equalTo(90)
                    })
                } else {
                    courseEnterBt.setTitle(LocalizedString("COURSE_STATUS_NOT"), for: .normal)
                    courseEnterBt.snp.updateConstraints({ (make) in
                        make.width.equalTo(80)
                    })
                }
                coursePaybackBt.isHidden = true
            default:
                courseIntroducTextField.text = newItem.customCurTime
            }
            courseIntroducTextField.sizeToFit()
            courseEnterBt.isEnabled = newItem.courseBtFlag
            courseEnterBt.backgroundColor = newItem.courseStatusBtColor
        }
    }
    
    func resetTime() {
        if self.timer == nil {
            self.surplusCountDownSeconds = 0 //重新记录倒计时总的时间
            let queue: DispatchQueue = DispatchQueue.global(qos: .default)
            var timer: DispatchSourceTimer? = DispatchSource.makeTimerSource(flags: .init(rawValue: 0), queue: queue)
            timer?.setEventHandler(handler: {
                if let newItem = self.item as? PublishCourseModel {
                    if newItem.indexPath == self.indexPath {//防止cell复用问题
                        if newItem.customIntervalSeconds <= 0 {
                            self.courseIntroducTextField.text = newItem.customCurTime
                            self.courseIntroducTextField.textColor = HEX(COLOR_FONT_9B9B9B)
                        } else {
                            var timeout = (newItem.customIntervalSeconds - self.surplusCountDownSeconds)  //当前cell，倒计时开始前距离开播还有多久
                            if timeout <= 0 {
                                timer?.cancel()
                                timer = nil
                                DispatchQueue.main.async {
                                    newItem.customCurTime = " " + Date.dateStr(newItem.nextLessonStartTime.doubleValue)
                                    self.courseIntroducTextField.text = newItem.customCurTime
                                    self.courseIntroducTextField.textColor = HEX(COLOR_FONT_9B9B9B)
                                    self.courseEnterBt.backgroundColor = UIColor.red
                                    self.courseEnterBt.isEnabled = true
                                }
                            } else {
                                DispatchQueue.main.async {
                                    if self.pageName == "publish" && timeout <= 5 * 60 {//(主播5分钟内可以进入直播间)
                                        if self.courseEnterBt.isEnabled == false {
                                            self.roomIdAction()
                                        }
                                        self.courseEnterBt.backgroundColor = UIColor.red
                                        self.courseEnterBt.isEnabled = true
                                    }
                                    self.courseIntroducTextField.text = " " + LocalizedString("COURSE_COUNTDOWN_TEXT") + CommonSwift.timeDecrease(seconds: timeout)
                                }
                                timeout = timeout - 1
                                self.surplusCountDownSeconds = self.surplusCountDownSeconds + 1
                            }
                        }
                    }
                }
            })
            timer?.scheduleRepeating(deadline: .now(), interval: .seconds(1))
            timer?.resume()
            self.timer = timer
        }
    }

    //课程退款
    func paybackAction() {
        if let newItem = self.item as? PublishCourseModel {
            delegate?.paybackAction!(newItem.remark, indexPath: self.indexPath!)
        }
    }
    
    //开始直播或进入直播间
    func jumpToRoomAction() {
        if self.pageName == "booking" {//进入直播间
            delegate?.jumpToRoomAction!(self.item, type: CourseType.Play.rawValue)
        } else{//开始直播
            delegate?.jumpToRoomAction!(self.item, type: CourseType.Enter.rawValue)
        }
    }
    
    //倒计时5分钟内，获取roomId
    func roomIdAction() {
        if let newItem = self.item as? PublishCourseModel {
            delegate?.roomIdAction!(newItem.courseId.stringValue, indexPath: self.indexPath!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
