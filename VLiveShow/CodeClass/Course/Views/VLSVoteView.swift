//
//  VLSVoteView.swift
//  VLiveShow
//
//  Created by rock on 2016/12/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSVoteViewDelegate {
    @objc optional func voteRusult(_ message: String?, dic: NSDictionary?)
    @objc optional func recharge(_ vc: UIViewController)
}

class VLSVoteView: UIView {
    
    private var numBt: UIButton!
    private var voteBt: UIButton!
    private var postDic: NSDictionary?
    private var nickname: String?
    private var residueVotes: NSNumber?
    weak var delegate: VLSVoteViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //投票红色背景图+投票数
        numBt = UIButton().then {
            $0.setImage(#imageLiteral(resourceName: "vote_num_back"), for: .normal)
            $0.setTitle("0", for: .normal)
            $0.setTitleColor(HEX(COLOR_FONT_FFFFFF), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.addTarget(self, action: #selector(VLSVoteView.vote), for: .touchUpInside)
            $0.titleEdgeInsets = UIEdgeInsets(top: 0, left: -#imageLiteral(resourceName: "vote_num_back").size.width, bottom: 0, right: 0)
        }
        self.addSubview(numBt)
        numBt.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self)
            make.height.equalTo(30)
        }
        //投票操作
        voteBt = UIButton().then {
            $0.setTitle(LocalizedString("VOTE_BUTTOM_TITLE"), for: .normal)
            $0.setTitleColor(HEX(COLOR_FONT_C3A851), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_13))
            $0.addTarget(self, action: #selector(VLSVoteView.vote), for: .touchUpInside)
            $0.layer.borderColor = HEX(COLOR_FONT_C3A851).cgColor
            $0.layer.borderWidth = 1.0
            $0.layer.cornerRadius = 2
        }
        self.addSubview(voteBt)
        voteBt.snp.makeConstraints { (make) in
            make.top.equalTo(numBt.snp.bottom).offset(6)
            make.left.equalTo(numBt.snp.left).offset(6)
            make.right.equalTo(numBt.snp.right).offset(-6)
            make.height.equalTo(28)
        }
    }
    
    func setVoteModel(_ voteModel: VoteModel?) {
        if let voteModel = voteModel {
            nickname = StringSafty(voteModel.nickname)
            numBt.setTitle(StringSafty(voteModel.totalVotes?.intValue), for: .normal)
            if let votedId = voteModel.votedId {
                postDic = ["votedId": votedId]
            }
            residueVotes = voteModel.residueVotes
        }
    }
    
    //投票
    func vote() {
        let title = LocalizedString("VOTE_CONFIRM_TITLE") + StringSafty(nickname) + LocalizedString("VOTE_CONFIRM_TITLE_2")
        let residueVotesStr = LocalizedString("VOTE_CONFIRM_CONTENT_2").replacingOccurrences(of: "n", with: StringSafty(residueVotes?.stringValue))
        let content = LocalizedString("VOTE_CONFIRM_CONTENT") + StringSafty(nickname) + residueVotesStr

        let alertVC = UIAlertController(title: title, message: content, preferredStyle: .alert)
        let okAction = UIAlertAction(title: LocalizedString("VOTE_CONFIRM_DONE"), style: .default, handler: {[weak self] (action) in
            if AccountManager.shared().account.diamondBalance <= 0 {//余额不足
                self?.recharge()
            } else { //投票
                self?.voteAction(self?.postDic)
            }
        })
        alertVC.addAction(okAction)
        let cancelAction = UIAlertAction(title: LocalizedString("VOTE_CONFIRM_CANCEL"), style: .cancel, handler: nil)
        alertVC.addAction(cancelAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertVC, animated: true, completion: nil)
    }
    
    //正式投票
    func voteAction(_ dic: NSDictionary?) {
        let callback = HttpCallBack()
        callback.doneBlock =
            {[weak self] ret in
                var message = LocalizedString("VOTE_SUCCESS_MESSAGE")
                guard let result = ret as? [String: Any] else {
                    message = LocalizedString("VOTE_FAIL_MESSAGE")
                    return
                }
                if  let _ = result["success"] as? Bool, let error_code = result["error_code"] as? Int {
                    message = ErrorEvent.error(error_code, msg: StringSafty(result["message"]))
                    if error_code == 0 {
                        message = LocalizedString("VOTE_SUCCESS_MESSAGE")
                        let residueVotes = NSNumber(value: IntSafty(ObjectForKeySafety(NSDictionarySafty(result["results"]), key: "residueVotes")))
                        let totalVotes = NSNumber(value: IntSafty(ObjectForKeySafety(NSDictionarySafty(result["results"]), key: "totalVotes")))
                        self?.delegate?.voteRusult!(message, dic: ["residueVotes": residueVotes, "totalVotes": totalVotes])
                    } else if error_code == 34004 {
                        self?.recharge() //去充值
                    } else {
                        message = LocalizedString("VOTE_FAIL_CONTENT")
                        self?.delegate?.voteRusult!(message, dic: nil)
                    }
                }
        }
        callback.errorBlock = {[weak self] err in
            self?.delegate?.voteRusult!(LocalizedString("VOTE_FAIL_MESSAGE"), dic: nil)
        }
        HTTPFacade.put(APIConfiguration.shared().voteUrl, parameter: dic as! [AnyHashable : Any]!, callback: callback)
    }
    
    //余额不足提示
    func recharge() {
        let alertVC = UIAlertController(title: LocalizedString("VOTE_FAIL_MONEY_TITLE"), message: LocalizedString("VOTE_FAIL_MONEY_CONTENT"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: LocalizedString("VOTE_FAIL_MONEY_DONE"), style: .default, handler: { (action) in
            if let paymentVC = UIStoryboard(name: "Payment", bundle: nil).instantiateInitialViewController() {
                paymentVC.hidesBottomBarWhenPushed = true
                self.delegate?.recharge!(paymentVC)
            }
        })
        alertVC.addAction(okAction)
        let cancelAction = UIAlertAction(title: LocalizedString("VOTE_FAIL_MONEY_CANCEL"), style: .cancel, handler: nil)
        alertVC.addAction(cancelAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertVC, animated: true, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
