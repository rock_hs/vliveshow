//
//  VLSScheduleCalendarCollectionView.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

var VLSScheduleCalendarPreIndex = -1
var VLSScheduleCalendarPreDate = ""

@objc protocol VLSScheduleCalendarCollectionViewDelegate {
    func loadData(from date: String)
}

class VLSScheduleCalendarCollectionView: VLSCollectionView {

    weak var delegate: VLSScheduleCalendarCollectionViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        collectionView.register(VLSScheduleCalendarCollectionViewCell.self, forCellWithReuseIdentifier: "calendarcollectionCell")
        self.collectionView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(VLSScheduleCalendarCollectionView.loginout), name: NSNotification.Name(rawValue: NOTIFICATION_LOGINOUTED), object: nil)
    }
    
    func changeDataSource(_ dataSource: [VLSScheduleDateModel]?) {
        if let dataSource = dataSource {
            self.viewModel.dataSource = NSMutableArray(array: dataSource)
            self.collectionView.reloadData()
            self.viewModel.dataSource.enumerateObjects({[weak self] (obj, index, stop) in
                if let obj = obj as? VLSScheduleDateModel {
                    if VLSScheduleCalendarPreDate != "" {
                        if VLSScheduleCalendarPreDate == obj.date {
                            self?.collectionView.contentOffset = CGPoint(x: (self?.collectionView.frame.width)! * CGFloat(IntSafty(index/7)), y: 0)
                            VLSScheduleCalendarPreIndex = index
                        }
                    } else {
                        if obj.isCurDateFlag {
                                self?.collectionView.contentOffset = CGPoint(x: (self?.collectionView.frame.width)! * CGFloat(IntSafty(index/7)), y: 0)
                            VLSScheduleCalendarPreIndex = index
                        }
                    }
                }
            })
        }
    }
    
    func loginout() {
        //重置之前用户在我的计划界面的日期选择
        VLSScheduleCalendarPreDate = ""
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VLSScheduleCalendarCollectionView {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.bounds.width/7, height: self.bounds.height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "calendarcollectionCell", for: indexPath)
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            if let cell = cell as? VLSScheduleCalendarCollectionViewCell {
                if let newItem = item as? VLSScheduleDateModel {
                    cell.setNewItem(newItem)
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if VLSScheduleCalendarPreIndex != indexPath.row {
            if let predateModel = ObjectForKeySafety(viewModel.dataSource, index: VLSScheduleCalendarPreIndex) as? VLSScheduleDateModel {
                predateModel.isSelectedFlag = false
                self.viewModel.dataSource.replaceObject(at: VLSScheduleCalendarPreIndex, with: predateModel)
                self.collectionView.reloadItems(at: [IndexPath(row: VLSScheduleCalendarPreIndex, section: indexPath.section)])
            }
        }
        if let dateModel = ObjectForKeySafety(viewModel.dataSource, index: indexPath.row) as? VLSScheduleDateModel {
            dateModel.isSelectedFlag = true
            self.viewModel.dataSource.replaceObject(at: indexPath.row, with: dateModel)
            self.collectionView.reloadItems(at: [indexPath])
            VLSScheduleCalendarPreDate = StringSafty(dateModel.date)
        }
        VLSScheduleCalendarPreIndex = indexPath.row
        self.delegate?.loadData(from: VLSScheduleCalendarPreDate)
    }
}

@objc protocol VLSScheduleCalendarReusableViewDelegate {
    func loadData(from date: String)
}
class VLSScheduleCalendarReusableView: UICollectionReusableView, VLSScheduleCalendarCollectionViewDelegate {
    weak var delegate: VLSScheduleCalendarReusableViewDelegate?
    private var calendarView: VLSScheduleCalendarCollectionView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        calendarView = VLSScheduleCalendarCollectionView()
        calendarView.delegate = self
        self.addSubview(calendarView)
        calendarView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
    }
    
    func changeDataSource(_ dataSource: [VLSScheduleDateModel]?) {
        calendarView.changeDataSource(dataSource)
    }
    
    //查看每个日期对应的计划
    func loadData(from date: String) {
        self.delegate?.loadData(from: date)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
