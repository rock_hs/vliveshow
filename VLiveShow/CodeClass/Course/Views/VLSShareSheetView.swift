//
//  VLSShareSheetView.swift
//  VLiveShow
//
//  Created by rock on 2017/1/12.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSShareSheetViewDelegate {
    func xs_actionSheet(_ actionSheet: ShareActionSheet!, clickedButtonIndex index: Int)
}

class VLSShareSheetView: UIView {

    static let shared = VLSShareSheetView(frame: CGRect(x: 0, y: 0, width: 20, height: 16))
    weak var delegate: VLSShareSheetViewDelegate?
    var item: VLSTableViewItem?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    func shareSheet(view: UIView, item: VLSTableViewItem?) {
        self.item = item
        let images = ["broadcast-share-qq", "broadcast-share-zone", "broadcast-share-weixin", "share_moments", "weibo01"]
        let shareActionSheet = ShareActionSheet(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 258), delegate: self, images: images)
        shareActionSheet?.addTarget(self, action: #selector(VLSShareSheetView.dismissShareSheet(_:)), for: .touchUpInside)
        shareActionSheet?.show(in: view)
    }
    
    func dismissShareSheet(_ shareSheet: ShareActionSheet) {
        shareSheet.dismiss()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VLSShareSheetView: ShareActionSheetDelegate {
    func xs_actionSheet(_ actionSheet: ShareActionSheet!, clickedButtonIndex index: Int) {
        self.delegate?.xs_actionSheet(actionSheet, clickedButtonIndex: index)
    }
}
