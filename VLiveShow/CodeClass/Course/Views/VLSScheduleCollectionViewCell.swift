//
//  VLSScheduleCollectionViewCell.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

protocol VLSScheduleCollectionViewCellDelegate: NSObjectProtocol {
    func share(_ item: VLSScheduleCourseModel)
    func payback(_ result: VLSScheduleViewPaybackResult)
    func praise(_ result: VLSScheduleViewRateResult)    //评价
    func room(_ result: VLSScheduleViewRoomResult)      //进入直播间（或开始直播)
//    @objc optional func roomId(_ courseId: String, indexPath: IndexPath)    //倒计时5分钟内，获取roomId
    func courseDetail(_ vc: UIViewController)
}

class VLSScheduleCollectionViewCell: UICollectionViewCell {
    private var scheduleView: VLSScheduleView!
    weak var delegate: VLSScheduleCollectionViewCellDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        scheduleView = VLSScheduleView()
        scheduleView.delegate = self
        self.contentView.addSubview(scheduleView)
        scheduleView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
    }
    
    func changeDataSource(_ dataSource: [VLSScheduleCourseModel]?) {
        if let dataSource = dataSource {
            scheduleView.changeDataSource(dataSource)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VLSScheduleCollectionViewCell: VLSScheduleViewDelegate {
    func share(_ item: VLSScheduleCourseModel) {
        self.delegate?.share(item)
    }
    
    func payback(_ result: VLSScheduleViewPaybackResult) {
        self.delegate?.payback(result)
    }
    
    func praise(_ result: VLSScheduleViewRateResult) {
        self.delegate?.praise(result)
    }
    
    func room(_ result: VLSScheduleViewRoomResult) {
        self.delegate?.room(result)
    }
    
    func courseDetail(_ vc: UIViewController) {
        self.delegate?.courseDetail(vc)
    }
}
