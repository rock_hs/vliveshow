//
//  VLSScheduleCell.swift
//  VLiveShow
//
//  Created by rock on 2017/1/3.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

let scheduleSpace = 14 //间距
let sepatorHeight = 0.5 //分割线高度

@objc protocol VLSScheduleCellDelegate {
    func share(_ item: VLSScheduleCourseModel)
    func payback(_ courseId: String, indexPath: IndexPath)
    @objc optional func praise(_ lesson: VLSCustomScheduleModel, indexPath: IndexPath)    //评价
    @objc optional func room(_ lesson: AnyObject?, type: NSInteger)//进入直播间（或开始直播)
    func roomId(_ courseId: String, indexPath: IndexPath)    //倒计时5分钟内，获取roomId
    func courseDetail(_ vc: UIViewController)
}

class VLSScheduleCell: VLSTableViewCell {
    
    fileprivate var topView: UIView!
    fileprivate var middleView: UIView!
    fileprivate var bottomView: UIView!
    fileprivate var backView: UIView!
    fileprivate var timeLab: UITextField!
    fileprivate var shareBt: UIButton!
    fileprivate var avatorImageView: UIImageView!
    fileprivate var nicknameLab: UILabel!
    fileprivate var typeBt: UIButton!
    fileprivate var courseImageView: UIImageView!
    fileprivate var courseTitleLab: UILabel!
    fileprivate var appraiseLab: UILabel!
    fileprivate var courseBt: UIButton!
    fileprivate var paybackBt: UIButton!
    weak var delegate: VLSScheduleCellDelegate?
    fileprivate var surplusCountDownSeconds: CLongLong! = 0  //复用cell，已跑多久
    var timer: DispatchSourceTimer?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backView = UIView().then {
            $0.backgroundColor = HEX(COLOR_BG_FFFFFF)
            $0.layer.cornerRadius = 6
            $0.clipsToBounds = true
        }
        self.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.left.equalTo(10)
            make.right.equalTo(-10)
            make.bottom.equalTo(self)
        }
        self.initTopView()
        self.initMiddleView()
        self.initBottomView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initTopView() {
        topView = UIView()
        backView.addSubview(topView)
        topView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(backView)
        }
        shareBt = UIButton().then {//分享
            $0.setImage(#imageLiteral(resourceName: "share_gray"), for: .normal)
            $0.addTarget(self, action: #selector(VLSScheduleCell.share), for: .touchUpInside)
        }
        topView.addSubview(shareBt)
        shareBt.snp.makeConstraints { (make) in
            make.top.equalTo(5)
            make.bottom.equalTo(topView)
            make.right.equalTo(topView).offset(-scheduleSpace)
        }
        timeLab = UITextField().then {//时间
            $0.textColor = HEX(COLOR_BG_D8D8D8)
            $0.leftViewMode = .always
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
            $0.isEnabled = false
        }
        topView.addSubview(timeLab)
        timeLab.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(shareBt)
            make.left.equalTo(topView).offset(scheduleSpace)
        }
        let sepatorLab = UILabel().then {//分割线
            $0.backgroundColor = HEX(COLOR_BG_DCDCDC)
        }
        topView.addSubview(sepatorLab)
        sepatorLab.snp.makeConstraints { (make) in
            make.left.equalTo(timeLab.snp.left)
            make.right.equalTo(shareBt.snp.right)
            make.top.equalTo(shareBt.snp.bottom).offset(5)
            make.height.equalTo(sepatorHeight)
        }
    }
    
    func initMiddleView() {
        middleView = UIView()
        backView.addSubview(middleView)
        middleView.snp.makeConstraints { (make) in
            make.top.equalTo(topView.snp.bottom).offset(5)
            make.left.right.equalTo(backView)
            make.height.equalTo(120)
        }
        avatorImageView = UIImageView().then {//头像
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
            $0.layer.cornerRadius = 20
        }
        middleView.addSubview(avatorImageView)
        avatorImageView.snp.makeConstraints { (make) in
            make.top.equalTo(scheduleSpace)
            make.left.equalTo(middleView).offset(scheduleSpace)
            make.size.equalTo(CGSize(width: 40, height: 40))
        }
        nicknameLab = UILabel().then {//昵称
            $0.textColor = HEX(COLOR_FONT_979797)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
        }
        middleView.addSubview(nicknameLab)
        nicknameLab.snp.makeConstraints { (make) in
            make.left.equalTo(avatorImageView.snp.right).offset(5)
            make.centerY.equalTo(avatorImageView.snp.centerY)
        }
        typeBt = UIButton().then {//标签
            $0.layer.borderColor = HEX(COLOR_BG_D8D8D8).cgColor
            $0.layer.borderWidth = 1
            $0.isHidden = true
        }
        middleView.addSubview(typeBt)
        typeBt.snp.makeConstraints { (make) in
            make.left.equalTo(nicknameLab.snp.right).offset(10)
            make.centerY.equalTo(nicknameLab)
        }
        let shadowView = UIView().then {//阴影
            $0.backgroundColor = HEX(COLOR_BG_D8D8D8, alpha: 0.2)
            $0.clickAction(target: self, action: #selector(VLSScheduleCell.courseDetail))
        }
        middleView.addSubview(shadowView)
        shadowView.snp.makeConstraints { (make) in
            make.top.equalTo(avatorImageView.snp.bottom).offset(6)
            make.left.equalTo(avatorImageView.snp.left)
            make.right.equalTo(middleView).offset(-scheduleSpace)
            make.height.equalTo(60)
        }
        courseImageView = UIImageView().then {//课程（直播间）封面
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
        shadowView.addSubview(courseImageView)
        courseImageView.snp.makeConstraints { (make) in
            make.top.left.equalTo(shadowView).offset(10)
            make.bottom.equalTo(shadowView).offset(-10)
            make.width.equalTo(80)
        }
        courseTitleLab = UILabel().then {//课程（直播间）标题
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
            $0.numberOfLines = 2
            $0.sizeToFit()
        }
        shadowView.addSubview(courseTitleLab)
        courseTitleLab.snp.makeConstraints { (make) in
            make.top.equalTo(courseImageView.snp.top)
            make.left.equalTo(courseImageView.snp.right).offset(Left_Space)
            make.right.equalTo(shadowView.snp.right).offset(-5)
        }
    }
    
    func initBottomView() {
        bottomView = UIView()
        backView.addSubview(bottomView)
        bottomView.snp.makeConstraints { (make) in
            make.top.equalTo(middleView.snp.bottom)
            make.left.bottom.right.equalTo(backView)
        }
        appraiseLab = UILabel().then { //直播结束，评价数
            $0.sizeToFit()
        }
        bottomView.addSubview(appraiseLab)
        appraiseLab.snp.makeConstraints { (make) in
            make.left.equalTo(bottomView).offset(scheduleSpace)
            make.centerY.equalTo(bottomView.snp.centerY)
        }
        courseBt = UIButton().then { //进入直播间（开始直播...）
            $0.setTitle(LocalizedString("开始直播"), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.setTitleColor(HEX(COLOR_FONT_464646), for: .normal)
            $0.layer.cornerRadius = 4
            $0.layer.borderWidth = 1
            $0.addTarget(self, action: #selector(VLSScheduleCell.createRoom), for: .touchUpInside)
        }
        bottomView.addSubview(courseBt)
        courseBt.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.bottom.equalTo(bottomView.snp.bottom).offset(-10)
            make.right.equalTo(bottomView.snp.right).offset(-scheduleSpace)
            make.size.equalTo(CGSize(width: 100, height: 30))
        }
        paybackBt = UIButton().then {
            $0.setTitle(LocalizedString("退款"), for: .normal)
            $0.setTitleColor(HEX(COLOR_FONT_DBDBDB), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.layer.cornerRadius = 6
            $0.layer.masksToBounds = true
            $0.layer.borderColor = HEX(COLOR_FONT_DBDBDB).cgColor
            $0.layer.borderWidth = 1.0
            $0.isHidden = true
            $0.addTarget(self, action: #selector(VLSScheduleCell.paybackAction), for: .touchUpInside)
        }
        bottomView.addSubview(paybackBt)
        paybackBt.snp.makeConstraints { (make) in
            make.top.bottom.size.equalTo(courseBt)
            make.right.equalTo(courseBt.snp.left).offset(-scheduleSpace)
        }
    }
    
    override func setNewItem(_ item: VLSTableViewItem) {
        super.setNewItem(item)
        if let newItem = item as? VLSScheduleCourseModel {
            if let courseItem = newItem.courseDTO {
                if newItem.calendarType?.intValue == 0 {//预约
                    if let status = courseItem.status?.intValue {
                        switch status {
                        case 0, 1:
                            courseItem.customCourseStatusText = LocalizedString("COURSE_STATUS_ON")
                        case 2:
                            if courseItem.hasRating?.boolValue == true {
                                courseItem.customCourseStatusText = LocalizedString("COURSE_APPRAISE_END")
                            } else {
                                courseItem.customCourseStatusText = LocalizedString("COURSE_APPRAISE")
                            }
                        default:
                            break;
                        }
                    }
                } else if newItem.calendarType?.intValue == 1 {//发布
                    if let status = courseItem.status?.intValue {
                        switch status {
                        case 0, 1:
                            courseItem.customCourseStatusText = LocalizedString("COURSE_STATUS_NOT")
                        case 2:
                            courseItem.customCourseStatusText = LocalizedString("COURSE_STATUS_END")
                            courseItem.customCourseBtFlag = false
                        default:
                            break;
                        }
                    }
                    courseItem.customCoursePaybackFlag = true
                }
                if courseItem.customCourseStatus == CourseStatus.LaunchCountDown.rawValue {
                    self.perform(#selector(VLSScheduleCell.resetTime)) //倒计时
                }
                timeLab.text = StringSafty(courseItem.customTimeText)
                timeLab.textColor = courseItem.customCourseTimeColor
                timeLab.sizeToFit()
                if let teacherURL = URL(string: StringSafty(courseItem.picTeacher)) {
                    avatorImageView.sd_setImage(with: teacherURL)
                }
                if let coverURL = URL(string: StringSafty(courseItem.picCover)) {
                    courseImageView.sd_setImage(with: coverURL)
                }
                nicknameLab.text = StringSafty(courseItem.teacherName)
                nicknameLab.sizeToFit()
                courseTitleLab.text = StringSafty(courseItem.courseName)
                appraiseLab.attributedText = CustomAttributeStrings([(StringSafty(courseItem.customPraisePeopleText), HEX(COLOR_FONT_9B9B9B), UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12))), (StringSafty(courseItem.customPraiseScoreText), HEX(COLOR_MSG_FFB000), UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12)))])
                appraiseLab.sizeToFit()
                courseBt.setTitle(StringSafty(courseItem.customCourseStatusText), for: .normal)
                courseBt.setTitleColor(courseItem.customCourseStatusColor, for: .normal)
                courseBt.layer.borderColor = courseItem.customCourseStatusColor?.cgColor
                courseBt.isEnabled = courseItem.customCourseBtFlag
                paybackBt.isHidden = courseItem.customCoursePaybackFlag
            }
        }
    }
}

extension VLSScheduleCell {
    //分享
    func share() {
        if let item = self.item as? VLSScheduleCourseModel {
            self.delegate?.share(item)
        }
    }
    
    //课程退款
    func paybackAction() {
        if let newItem = self.item as? VLSScheduleCourseModel {
            if let courseItem = newItem.courseDTO {
                self.delegate?.payback(StringSafty(courseItem.subscriptionId?.stringValue), indexPath: self.indexPath!)
            }
        }
    }
    
    func createRoom() {
        if let newItem = self.item as? VLSScheduleCourseModel {
            if let courseItem = newItem.courseDTO {
                switch courseItem.customCourseStatus {
                case CourseStatus.NotPraise.rawValue://未评价
                    self.delegate?.praise!(courseItem, indexPath: self.indexPath!)
                case CourseStatus.HasBeenLaunch.rawValue, CourseStatus.LaunchCountDown.rawValue:
                    if newItem.calendarType?.intValue == 0 {//预约
                        self.delegate?.room!(courseItem, type: CourseType.Play.rawValue)
                    } else if newItem.calendarType?.intValue == 1 {//发布
                        self.delegate?.room!(courseItem, type: CourseType.Enter.rawValue)
                    }
                default:
                    break;
                }
            }
        }
    }
    
    //进入课程详情
    func courseDetail() {
        if let newItem = item as? VLSScheduleCourseModel {
            let courseVC = VLSCourseDetailViewController()
            courseVC.state = CourseState.course_orderd
            let model = PublishCourseModel()
            model.courseId = newItem.courseDTO?.courseId
            model.teacherId = newItem.courseDTO?.teacherId
            model.teacherName = newItem.courseDTO?.teacherName
            model.picTeacher = newItem.courseDTO?.picTeacher
            courseVC.model = model
            self.delegate?.courseDetail(courseVC)
        }
    }
    
    func resetTime() {
        if self.timer == nil {
            self.surplusCountDownSeconds = 0 //重新记录倒计时总的时间
            let queue: DispatchQueue = DispatchQueue.global(qos: .default)
            var timer: DispatchSourceTimer? = DispatchSource.makeTimerSource(flags: .init(rawValue: 0), queue: queue)
            timer?.setEventHandler(handler: {
                if let newItem = self.item as? VLSScheduleCourseModel {
                    if let courseItem = newItem.courseDTO {
                        if courseItem.indexPath == self.indexPath {//防止cell复用问题
                            if courseItem.customIntervalSeconds <= 0 {
                                self.timeLab.text = courseItem.customTimeText
                                self.timeLab.textColor = HEX(COLOR_FONT_9B9B9B)
                            } else {
                                var timeout = (courseItem.customIntervalSeconds - self.surplusCountDownSeconds)  //当前cell，倒计时开始前距离开播还有多久
                                if timeout <= 0 {
                                    timer?.cancel()
                                    timer = nil
                                    DispatchQueue.main.async {
                                        courseItem.customTimeText = " " + Date.dateStr((courseItem.startTime?.doubleValue)!)
                                        self.timeLab.text = courseItem.customTimeText
                                        self.timeLab.textColor = HEX(COLOR_FONT_9B9B9B)
                                        self.courseBt.setTitleColor(HEX(COLOR_FONT_FF1130), for: .normal)
                                        self.courseBt.layer.borderColor = HEX(COLOR_FONT_FF1130).cgColor
                                        self.courseBt.isEnabled = true
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        if newItem.calendarType?.intValue == 1 && timeout <= 5 * 60 {//(主播5分钟内可以进入直播间)
                                            if self.courseBt.isEnabled == false {
                                                self.delegate?.roomId(StringSafty(courseItem.courseId?.stringValue), indexPath: self.indexPath!)
                                            }
                                            self.courseBt.setTitleColor(HEX(COLOR_FONT_FF1130), for: .normal)
                                            self.courseBt.layer.borderColor = HEX(COLOR_FONT_FF1130).cgColor
                                            self.courseBt.isEnabled = true
                                        }
                                        self.timeLab.text = " " + LocalizedString("COURSE_COUNTDOWN_TEXT") + CommonSwift.timeDecrease(seconds: timeout)
                                    }
                                    timeout = timeout - 1
                                    self.surplusCountDownSeconds = self.surplusCountDownSeconds + 1
                                }
                            }
                        }
                    }
                }
            })
            timer?.scheduleRepeating(deadline: .now(), interval: .seconds(1))
            timer?.resume()
            self.timer = timer
        }
    }
}
