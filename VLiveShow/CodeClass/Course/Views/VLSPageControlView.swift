//
//  VLSPageControlView.swift
//  VLiveShow
//
//  Created by rock on 2017/1/5.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSPageControlViewDelegate {
    func change(view tag: NSInteger)
}

class VLSPageControlView: UIView {

    fileprivate var scrollView: UIScrollView!
    var coverView: UIView!
    weak var delegate: VLSPageControlViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        scrollView = UIScrollView().then {
            $0.showsVerticalScrollIndicator = false
            $0.showsHorizontalScrollIndicator = false
        }
        self.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
    }
    
    func addSubBt(_ textArr: NSArray) {
        guard textArr.count > 0 else {
            return
        }
        let textWidth = MultilineTextSize(text: StringSafty(textArr[0]), font: UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16)), maxSize: CGSize(width: self.frame.size.width/CGFloat(textArr.count), height: CGFloat(MAXFLOAT))).width
        coverView = UIView().then {
            $0.frame = CGRect(x: 0, y: 0, width: self.frame.size.width/CGFloat(textArr.count), height: self.frame.size.height)
            $0.backgroundColor = UIColor.clear
            $0.clipsToBounds = true
        }
        var i = 0
        for text in textArr {
            let textLab = UILabel().then {
                $0.frame = CGRect(x: self.frame.size.width/CGFloat(textArr.count) * CGFloat(i), y: coverView.frame.origin.y, width: coverView.frame.size.width, height: coverView.frame.size.height)
                $0.textColor = HEX(COLOR_FONT_464646)
                $0.textAlignment = .center
                $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
                $0.text = StringSafty(text)
                $0.tag = i+1
                $0.clickAction(target: self, action: #selector(VLSPageControlView.changeView(view:)))
            }
            scrollView.addSubview(textLab)
            let coverTextLab = UILabel().then {
                $0.frame = CGRect(x: self.frame.size.width/CGFloat(textArr.count) * CGFloat(i), y: 0, width: coverView.frame.size.width, height: coverView.frame.size.height)
                $0.textColor = HEX(COLOR_FONT_FF1130)
                $0.textAlignment = .center
                $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
                $0.text = StringSafty(text)
            }
            coverView.addSubview(coverTextLab)
            i += 1
        }
        scrollView.addSubview(coverView)
        let bottomSepatorLab = UILabel().then {
            $0.backgroundColor = HEX(COLOR_FONT_FF1130)
        }
        scrollView.addSubview(bottomSepatorLab)
        bottomSepatorLab.snp.makeConstraints { (make) in
            make.bottom.equalTo(coverView)
            make.height.equalTo(1)
            make.centerX.equalTo(coverView.snp.centerX)
            make.width.equalTo(textWidth).priority(1000)
        }
    }
    
    func changeView(view: UITapGestureRecognizer?) {
        if let view = view {
            let offset = view.view?.frame.origin.x
            self.changeView(by: offset)
            self.delegate?.change(view: (view.view?.tag)!)
        }
    }
    
    func changeView(by offset: CGFloat?) {
        if let offset = offset {
            for i in 0..<scrollView.subviews.count-1 {
                if let view = ObjectForKeySafety(NSArraySafty(coverView.subviews), index: i) as? UIView {
                    view.frame.origin.x = -(offset - coverView.frame.width * CGFloat(i))
                }
            }
            coverView.frame.origin.x = offset
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
