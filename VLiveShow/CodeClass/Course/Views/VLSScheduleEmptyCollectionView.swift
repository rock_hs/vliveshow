//
//  VLSScheduleEmptyCollectionView.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

protocol VLSScheduleEmptyCollectionViewDelegate: NSObjectProtocol {
    func courseDetail(_ vc: UIViewController)
}

class VLSScheduleEmptyCollectionView: VLSCollectionView {

    weak var delegate: VLSScheduleEmptyCollectionViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let headerView = VLSScheduleEmptyCollectionHeaderView()
        self.addSubview(headerView)
        headerView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self)
            make.height.equalTo(50)
        }
        self.collectionView.register(VLSScheduleEmptyCollectionViewCell.self, forCellWithReuseIdentifier: "scheduleEmptyCell")
        self.collectionView.isPagingEnabled = false
        self.collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom)
            make.left.bottom.right.equalTo(self)
        }
    }
    
    func changeDataSource(_ dataSource: [VLSScheduleRecommendationModel]?) {
        if let dataSource = dataSource {
            self.viewModel.dataSource = NSMutableArray(array: dataSource)
            self.collectionView.reloadData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VLSScheduleEmptyCollectionView {
    
    //cell的最小行间距
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "scheduleEmptyCell", for: indexPath)
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            if let cell = cell as? VLSScheduleEmptyCollectionViewCell {
                if let newItem = item as? VLSScheduleRecommendationModel {
                    cell.setNewItem(newItem)
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //进入课程详情
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            if let newItem = item as? VLSScheduleRecommendationModel {
                let courseVC = VLSCourseDetailViewController()
                courseVC.state = CourseState.course_orderd
                let model = PublishCourseModel()
                model.courseId = newItem.courseId
                model.teacherId = newItem.teacherId
                model.teacherName = newItem.teacherName
                model.allowAudience = newItem.allowAudience
                model.maxParticipants = newItem.maxParticipants
                model.priceAudience = newItem.priceAudience
                model.priceParticipant = newItem.priceParticipant
                model.picTeacher = newItem.picTeacher
                model.numLessons = newItem.numLessons
                courseVC.model = model
                self.delegate?.courseDetail(courseVC)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width*3/5, height: collectionView.frame.height)
    }
}

class VLSScheduleEmptyCollectionHeaderView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = HEX(COLOR_BG_FFFFFF)
        let titleLab = UILabel().then {
            $0.text = LocalizedString("COURSE_SCHEDULE_RECOMMEND")
            $0.textColor = HEX(COLOR_FONT_4A4A4A)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
        }
        self.addSubview(titleLab)
        titleLab.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(self)
            make.left.equalTo(scheduleSpace)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
