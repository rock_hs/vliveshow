//
//  CourseCell.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

enum CourseType: NSInteger {
    case Play   //开始直播
    case Enter  //进入直播
}

@objc protocol CourseCellDelegate {
    @objc optional func paybackAction(_ courseId: String, indexPath: IndexPath)      //退款
    @objc optional func praiseAction(_ lesson: LessonModel, indexPath: IndexPath)    //评价
    @objc optional func jumpToRoomAction(_ lesson: AnyObject?, type: NSInteger)//进入直播间（或开始直播)
    @objc optional func roomIdAction(_ courseId: String, indexPath: IndexPath)    //倒计时5分钟内，获取roomId
}

class CourseCell: VLSTableViewCell {
    
    var backView: UIView!
    var topView: UIView!
    var courseImageView: UIImageView!
    var courseTitleLab: UILabel!
    var courseShowBt: UIButton!
    var timer: DispatchSourceTimer?
    var pageName: String?
    weak var delegate: CourseCellDelegate?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backView = UIView().then {
            $0.backgroundColor = UIColor.white
        }
        self.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.left.bottom.right.equalTo(self)
        }
        
        self.initTopView()
    }
    
    //封面视图
    func initTopView() {
        topView = UIView()
        backView.addSubview(topView)
        topView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(backView)
            make.height.equalTo(200)
        }
        //课程封面
        courseImageView = UIImageView().then {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
        topView.addSubview(courseImageView)
        courseImageView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(topView)
        }        
        //封面阴影view
        let coverView = UIView().then {
            $0.backgroundColor = UIColor(white: 0.3, alpha: 0.2)
            $0.clickAction(target: self, action: #selector(CourseCell.enterCourseList))
        }
        topView.addSubview(coverView)
        coverView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(topView)
        }
        //课程标题
        courseTitleLab = UILabel().then {
            $0.text = "给家人的心仪来调整每一份甜点，这就是纯净的好滋味"
            $0.textAlignment = .center
            $0.textColor = HEX(COLOR_FONT_FFFFFF)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
            $0.numberOfLines = 2
            $0.layer.borderColor = HEX(COLOR_LINE_FFFFFF).cgColor
            $0.layer.borderWidth = 0.5
        }
        coverView.addSubview(courseTitleLab)
        courseTitleLab.snp.makeConstraints { (make) in
            make.top.equalTo(40)
            make.left.equalTo(60)
            make.right.equalTo(-60)
            make.height.equalTo(70)
        }
        //查看详情
        let title = NSMutableAttributedString(string: LocalizedString("COURSE_SHOW_DETAIL"))
        title.addAttribute(NSUnderlineStyleAttributeName, value: NSNumber(value: Int32(NSUnderlineStyle.styleSingle.rawValue)), range: NSMakeRange(0, title.length))
        let showDetailLab = UILabel().then {
            $0.attributedText = title
            $0.textColor = HEX(COLOR_FONT_FFFFFF)
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
            $0.clickAction(target: self, action: #selector(CourseCell.showCourseDetail))
        }
        coverView.addSubview(showDetailLab)
        showDetailLab.snp.makeConstraints { (make) in
            make.centerX.equalTo(coverView.snp.centerX)
            make.bottom.equalTo(coverView.snp.bottom).offset(-10)
            make.size.equalTo(CGSize(width: 100, height: 30))
        }
    }
    
    func enterCourseList() {
        if let newItem = item as? PublishCourseModel {
            if newItem.published.boolValue == true { //未下架的才显示课程详情
                CommonSwift.pushWithVCClass(self.vcClass, properties: self.properties, navigation: self.navigation)
            }
        }
    }
    
    //进入课程详情
    func showCourseDetail() {
        if let newItem = item as? PublishCourseModel {
            let courseVC = VLSCourseDetailViewController()
            courseVC.state = CourseState.course_orderd
            courseVC.model = newItem
            CommonSwift.pushViewController(courseVC, navigation: self.navigation)
        }
    }
        
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
