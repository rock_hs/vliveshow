//
//  VLSTableView.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSTableView: UIView {

    var tableView: UITableView!
    var viewModel: VLSTableViewModel!
    var viewModelClass: AnyClass? {
        didSet {
            self.initViewModel()
            self.setViewModelLoadEvent()
        }
    }
    fileprivate var errorView: VLSEmptyView?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = HEX(COLOR_BG_EEEEEE)
        self.initViewModel()
        self.setViewModelLoadEvent()
        tableView = UITableView().then {
            $0.register(viewModel.cellClass, forCellReuseIdentifier: "cell")
            $0.separatorStyle = .none
            $0.backgroundColor = UIColor.clear
            $0.delegate = self
            $0.dataSource = self
            $0.estimatedRowHeight = 100
            $0.rowHeight = UITableViewAutomaticDimension
        }
        self.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self)
        }
    }
    
    //初始化viewModel
    func initViewModel() {
        if let viewModelClass = viewModelClass, let type = viewModelClass as? VLSTableViewModel.Type {
            viewModel = type.init()
        } else {
            viewModel = VLSTableViewModel()
        }
    }
    
    //重新加载数据
    func reloadData() {
        viewModel.page = 0
        viewModel.pageDirection = 1
        viewModel.loadDataSource()
    }
    
    //加载更多
    func loadMoreData() {
        viewModel.pageDirection = 1  //加载更多
        viewModel.loadDataSource()
    }
    
    fileprivate func setViewModelLoadEvent() {//设置加载完成时间
        viewModel.loadSuccess = {[weak self] in
            self?.setViewModelLoadSuccess()
        }
        viewModel.loadEmpty = {[weak self] in
            self?.setViewModelLoadEmpty()
        }
        viewModel.loadFailed = {[weak self] in
            self?.setViewModelLoadFailed()
        }
        viewModel.loadMoreData = {
            [weak self](moreData,showFootor) in
            self?.setViewModelLoadMore(moreData, showFootor: showFootor)
        }
    }
    
    func setViewModelLoadSuccess() {//加载成功
        self.removeErrorView()
        self.tableView.reloadData()
    }
    
    func setViewModelLoadEmpty() {//无数据
        self.addErrorView()
        self.tableView.reloadData()
    }
    
    func setViewModelLoadFailed() {//加载失败
        self.addErrorView()
        self.tableView.reloadData()
    }
    
    func setViewModelLoadMore(_ moreData:Bool,showFootor:Bool) {//加载更多
        self.setViewModelLoadSuccess()
    }
    
    func addErrorView() {
//        let errorView = VLSEmptyView(frame: self.bounds).then {
//            $0.delegate = self
//        }
//        self.addErrorView(errorView)
    }
    
    //添加错误视图
    func addErrorView(_ errorView: VLSEmptyView?) {
        if self.errorView == nil {
            if let errorView = errorView {
                self.errorView = errorView
                self.addSubview(errorView)
            }
        }
    }
    
    func removeErrorView() {
        errorView?.removeFromSuperview()
        errorView = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension VLSTableView: UITableViewDelegate, UITableViewDataSource {
    //pragma mark -- UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            if let cell = cell as? VLSTableViewCell {
                if let newItem = item as? VLSTableViewItem {
                    cell.indexPath = indexPath
                    cell.setNewItem(newItem)
                }
            }
        }
        return cell!
    }
}
