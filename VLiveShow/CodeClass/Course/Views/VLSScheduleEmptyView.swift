//
//  VLSScheduleEmptyView.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

protocol VLSScheduleEmptyViewDelegate: NSObjectProtocol {
    func courseDetail(_ vc: UIViewController)
}

class VLSScheduleEmptyView: UIView {

    weak var delegate: VLSScheduleEmptyViewDelegate?
    fileprivate var recommendView: VLSScheduleEmptyCollectionView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let backView = UIView().then {
            $0.backgroundColor = HEX(COLOR_BG_EEEEEE)
        }
        self.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
//        let image = UIImage(v_setRadius: 4, image: nil, size:CGSize(width: 100, height: 100), borderColor: HEX(COLOR_BG_FFFFFF), borderWidth: 1, backgroundColor: UIColor.white, rectCornerType: .left)
//        let image = UIImage(I)
        let noView = UIView().then {
            $0.backgroundColor = HEX(COLOR_BG_FFFFFF)
        }
        backView.addSubview(noView)
        noView.snp.makeConstraints { (make) in
            make.top.left.equalTo(scheduleSpace)
            make.right.equalTo(-scheduleSpace)
            make.height.equalTo(100)
        }
        let tipsLab = UILabel().then {
            $0.text = LocalizedString("COURSE_SCHEDULE_EMPTY_TIPS")
            $0.textColor = HEX(COLOR_FONT_4A4A4A)
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
            $0.numberOfLines = 0
            $0.sizeToFit()
        }
        noView.addSubview(tipsLab)
        tipsLab.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(noView)
            make.left.equalTo(noView.snp.left).offset(scheduleSpace)
        }
        recommendView = VLSScheduleEmptyCollectionView()
        recommendView.delegate = self
        backView.addSubview(recommendView)
        recommendView.snp.makeConstraints { (make) in
            make.left.right.equalTo(backView)
            make.bottom.equalTo(backView.snp.bottom).offset(-10).priority(1000)
            make.height.equalTo(210)
        }
    }
    
    func changeDataSource(_ dataSource: [VLSScheduleRecommendationModel]?) {
        recommendView.changeDataSource(dataSource)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VLSScheduleEmptyView: VLSScheduleEmptyCollectionViewDelegate {
    func courseDetail(_ vc: UIViewController) {
        self.delegate?.courseDetail(vc)
    }
}
