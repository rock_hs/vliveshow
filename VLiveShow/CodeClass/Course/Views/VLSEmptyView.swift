//
//  VLSEmptyView.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSEmptyViewDelegate {
    func tryAction()
}

class VLSEmptyView: UIView {

    fileprivate var errorImageView: UIImageView!
    fileprivate var errorContentLab: UILabel!
    fileprivate var errorTryBt: UIButton!
    weak var delegate: VLSEmptyViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        //empty图片
        errorImageView = UIImageView().then {
            $0.image = #imageLiteral(resourceName: "mybooking_emptyPage")
        }
        self.addSubview(errorImageView)
        errorImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(160)
        }
        //文字描述
        errorContentLab = UILabel().then {
            $0.text = LocalizedString("COURSE_BOOKING_EMPTY")
            $0.textColor = UIColor.gray
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: 14)
            $0.numberOfLines = 0
            $0.sizeToFit()
        }
        self.addSubview(errorContentLab)
        errorContentLab.snp.makeConstraints { (make) in
            make.top.equalTo(errorImageView.snp.bottom).offset(30)
            make.left.equalTo(40)
            make.right.equalTo(-40)
        }
        //try button
        errorTryBt = UIButton().then {
            $0.setTitle(LocalizedString("COURSE_BOOKING_EMPTY_TRY"), for: .normal)
            $0.setTitleColor(UIColor.red, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: 16)
            $0.layer.cornerRadius = 20
            $0.layer.borderColor = UIColor.red.cgColor
            $0.layer.borderWidth = 1.0
            $0.addTarget(self, action: #selector(VLSEmptyView.tryAction), for: .touchUpInside)
        }
        self.addSubview(errorTryBt)
        errorTryBt.snp.makeConstraints { (make) in
            make.top.equalTo(errorContentLab.snp.bottom).offset(40)
            make.left.equalTo(80)
            make.right.equalTo(-80)
            make.height.equalTo(40)
        }
    }
    
    func tryAction() {
        delegate?.tryAction()
    }
    
    func errorImageName(_ errorImageName: String) {
        errorImageView.image = UIImage(named: errorImageName)
    }
    
    func errorImageHidden(_ hiddenFlag: Bool) {
        errorImageView.isHidden = hiddenFlag
    }
    
    func errorMsg(_ errorMsg: String) {
        errorContentLab.text = errorMsg
    }
    
    func errorTry(_ errorTry: String) {
        errorTryBt.setTitle(errorTry, for: .normal)
    }
    
    func errorBtHidden(_ hiddenFlag: Bool) {
        errorTryBt.isHidden = hiddenFlag
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
