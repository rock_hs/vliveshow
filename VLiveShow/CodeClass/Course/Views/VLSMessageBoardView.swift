//
//  VLSMessageBoardView.swift
//  VLiveShow
//
//  Created by rock on 2017/2/7.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

let textViewWordLimitNum = 54
let minKeyboardHeight: CGFloat = 162.0

@objc protocol VLSMessageBoardViewDelegate {
    func send(_ text: String)
}

class VLSMessageBoardView: UIView {

    var textView: VLSTextView!
    var emojiView: VLSEmojiView!
    var emojiBt: UIButton!
    var sendBt: UIButton!
    var tap: UITapGestureRecognizer!
    var keyboardHeight: CGFloat = 258.0
    var emojiViewHeight: CGFloat = 238.0
    var animationSec: Float = 0.0
    var keyboardHideType: Int = 0  //0:屏幕区域其他地方隐藏；1：emoji隐藏；
    var topY: CGFloat = 0
    weak var delegate: VLSMessageBoardViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = HEX(COLOR_BG_FFFFFF)
        self.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 46)
        sendBt = UIButton().then {
            $0.setTitle(LocalizedString("Send"), for: .normal)
            $0.setTitleColor(HEX(COLOR_FONT_FFFFFF), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
            $0.addTarget(self, action: #selector(VLSMessageBoardView.send), for: .touchUpInside)
            $0.layer.cornerRadius = 4
            $0.backgroundColor = VLSSepartorColor
            $0.isEnabled = false
        }
        self.addSubview(sendBt)
        sendBt.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.right.equalTo(self).offset(-10)
            make.size.equalTo(CGSize(width: 50, height: 30))
        }
        emojiBt = UIButton().then {
            $0.setImage(#imageLiteral(resourceName: "emoji_nor"), for: .normal)
            $0.setImage(#imageLiteral(resourceName: "emoji_pre"), for: .highlighted)
            $0.setImage(#imageLiteral(resourceName: "emoji_kb_nor"), for: .selected)
            $0.addTarget(self, action: #selector(VLSMessageBoardView.emojiAction(_:)), for: .touchUpInside)
            $0.isSelected = false
        }
        self.addSubview(emojiBt)
        emojiBt.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(self.snp.left).offset(10)
            make.size.equalTo(CGSize(width: 30, height: 30))
        }
        textView = VLSTextView().then {
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
            $0.delegate = self
            $0.layer.cornerRadius = 4
            $0.layer.borderColor = VLSSepartorColor.cgColor
            $0.layer.borderWidth = 0.5
            $0.showsVerticalScrollIndicator = false
            $0.placeholder = LocalizedString("VLS_MESSAGEBOARD_PLACEHOLDER")
        }
        self.addSubview(textView)
        textView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(emojiBt.snp.right).offset(6)
            make.right.equalTo(sendBt.snp.left).offset(-6)
            make.height.equalTo(30)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(VLSMessageBoardView.hide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(VLSMessageBoardView.willShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show(y: CGFloat) {
        topY = y
        self.frame.origin.y = topY
        self.textView.becomeFirstResponder()
    }
    
    func willShow(_ notify: Notification) {
        emojiBt.isSelected = false
        self.isHidden = false
        if tap == nil {
            tap = UITapGestureRecognizer(target: self, action: #selector(VLSMessageBoardView.touch))
            tap.delegate = self
            self.superview?.addGestureRecognizer(tap)
        }
        if let textView = self.textView, textView.isFirstResponder {
            let rect = (ObjectForKeySafety(NSDictionarySafty(notify.userInfo), key: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue
            animationSec = (ObjectForKeySafety(NSDictionarySafty(notify.userInfo), key: UIKeyboardAnimationDurationUserInfoKey) as AnyObject).floatValue
            if let height = rect?.height {
                keyboardHeight = height
                if UIScreen.main.applicationFrame.width > UIScreen.main.applicationFrame.height {//横屏
                    emojiViewHeight = keyboardHeight <= minKeyboardHeight ? keyboardHeight : keyboardHeight - 20
                } else {//竖屏
                    emojiViewHeight = keyboardHeight <= minKeyboardHeight ? keyboardHeight : keyboardHeight - 40
                }
                if let emojiView = self.emojiView {
                    emojiView.isHidden = true
                    emojiView.snp.updateConstraints({ (make) in
                        make.height.equalTo(0)
                    })
                }
            }
            UIView.animate(withDuration: TimeInterval(animationSec), animations: {
                self.frame.origin.y = self.topY
                self.superview?.frame.origin.y = -self.keyboardHeight + (UIScreen.main.bounds.height - self.topY - self.bounds.height)
            })
        }
    }

    func hide() {
        if keyboardHideType == 0 {
            self.superview?.frame.origin.y = 0
            self.frame.origin.y = UIScreen.main.bounds.height
            self.textView.resignFirstResponder()
            self.isHidden = true
            if let emojiView = self.emojiView {
                emojiView.isHidden = true
                emojiView.snp.updateConstraints({ (make) in
                    make.height.equalTo(0)
                })
            }
            if let tap = self.tap {
                self.superview?.removeGestureRecognizer(tap)
                self.tap = nil
            }
        } else if keyboardHideType == 1 {
            self.superview?.frame.origin.y = -self.emojiViewHeight + (UIScreen.main.bounds.height - self.topY - self.bounds.height)
        }
    }
    
    func emojiAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if emojiView == nil {
            emojiView = VLSEmojiView()
            emojiView.delegate = self
            emojiView.isHidden = false
            UIApplication.shared.keyWindow?.currentViewController()?.view.addSubview(emojiView)
            emojiView.snp.makeConstraints { (make) in
                make.left.right.equalTo(self)
                make.bottom.equalTo(UIApplication.shared.keyWindow!)
                make.height.equalTo(emojiViewHeight)
            }
        }
        if sender.isSelected {
            keyboardHideType = 1
            textView.resignFirstResponder()
            if let emojiView = self.emojiView {
                emojiView.isHidden = false
                emojiView.snp.updateConstraints({ (make) in
                    make.height.equalTo(emojiViewHeight)
                })
            }
        } else {
            keyboardHideType = 0
            textView.becomeFirstResponder()
        }
    }
    
    func send() {
        delegate?.send(StringSafty(textView.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)))
        textView.text = ""
        if sendBt.isEnabled {
            sendBt.backgroundColor = VLSSepartorColor
            sendBt.isEnabled = false
        }
    }
    
    func touch() {
        UIView.animate(withDuration: TimeInterval(animationSec), animations: {
            self.keyboardHideType = 0
            self.hide()
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        textView.removeObserver(self, forKeyPath: "text")
        if let emojiView = self.emojiView {
            emojiView.removeFromSuperview()
            self.emojiView = nil
        }
        if let tap = self.tap {
            self.superview?.removeGestureRecognizer(tap)
        }
    }
}

extension VLSMessageBoardView: UITextViewDelegate, VLSEmojiViewDelegate, UIGestureRecognizerDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.characters.count >= textViewWordLimitNum, text != "" {
            textView.resignFirstResponder()
            self.tips()
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text, text.characters.count > 0 {
            if text.characters.count >= textViewWordLimitNum {
                textView.text?.remove(at: StringSafty(textView.text).index(before: StringSafty(textView.text).endIndex))
            }
            if !sendBt.isEnabled {
                sendBt.backgroundColor = HEX(COLOR_BG_FF1130)
                sendBt.isEnabled = true
            }
        } else {
            if sendBt.isEnabled {
                sendBt.backgroundColor = VLSSepartorColor
                sendBt.isEnabled = false
            }
        }
    }
    
    func check() -> Bool {
        if let text = textView.text, text.characters.count >= textViewWordLimitNum {
            self.tips()
            return false
        }
        return true
    }
    
    func emoji(_ text: String) {
        if text == "delete" {
            if let text = textView.text {
                if text.characters.count > 0 {
                    textView.text?.remove(at: StringSafty(textView.text).index(before: StringSafty(textView.text).endIndex))
                }
                if text.characters.count == 1, sendBt.isEnabled {
                    sendBt.backgroundColor = VLSSepartorColor
                    sendBt.isEnabled = false
                }
            }
        } else {
            if check() {
                textView.text?.append(text)
                textView.scrollRangeToVisible(NSMakeRange((textView.text as NSString).length - 1, 1))
            }
            if !sendBt.isEnabled {
                sendBt.backgroundColor = HEX(COLOR_BG_FF1130)
                sendBt.isEnabled = true
            }
        }
    }
    
    func tips() {
        let alertVC = UIAlertController(title: nil, message: String(format: LocalizedString("VLS_MESSAGEBOARD_ALERTTEXT"), textViewWordLimitNum), preferredStyle: .alert)
        let action = UIAlertAction(title: LocalizedString("YES"), style: .cancel, handler: { [weak self] action in
            if self?.keyboardHideType == 0 {
                self?.textView.becomeFirstResponder()
            }
        })
        alertVC.addAction(action)
        UIApplication.shared.keyWindow?.currentViewController()?.present(alertVC, animated: true, completion: nil)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let point = touch.location(in: self)
        if point.y < 0 {
            self.touch()
            return true
        }
        return false
    }
}
