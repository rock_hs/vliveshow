//
//  VLSCollectionViewFlowLayout.swift
//  VLiveShow
//
//  Created by rock on 2017/2/9.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSCollectionViewFlowLayout: UICollectionViewFlowLayout {

    var itemCount_Column: Int = 1
    var itemCount_Row: Int = 1
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        if let attributes = super.layoutAttributesForElements(in: rect) {
            let attArr = NSArray(array: attributes, copyItems: true) as! [UICollectionViewLayoutAttributes]
            for att in attArr {
                let size = att.size
                if let collectionView = self.collectionView {
                    let x = CGFloat(att.indexPath.row%Int(itemCount_Column))*size.width+CGFloat(att.indexPath.row/Int(itemCount_Column*itemCount_Row)) * collectionView.frame.size.width
                    let y = CGFloat((att.indexPath.row%Int(itemCount_Column*itemCount_Row))/Int(itemCount_Column))*size.height
                    att.frame.origin.x = x
                    att.frame.origin.y = y
                }
            }
            return attArr
        }
        return nil
    }
}
