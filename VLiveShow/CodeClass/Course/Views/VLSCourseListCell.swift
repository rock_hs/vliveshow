//
//  VLSCourseListCell.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//
import UIKit

let Left_Space = 20

class VLSCourseListCell: VLSTableViewCell {
    
    fileprivate var courseImageView: UIImageView!
    fileprivate var courseTitleLab: UILabel!
    fileprivate var courseStartLab: UILabel!
    fileprivate var courseTextField: UITextField!
    fileprivate var courseBt: UIButton!
    fileprivate var courseCurLab: UILabel!
    var timer: DispatchSourceTimer?
    var pageName: String?
    weak var delegate: CourseCellDelegate?
    fileprivate var surplusCountDownSeconds: CLongLong! = 0  //复用cell，已跑多久
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //初始化视图
    func initView() {
        let backView = UIView().then {
            $0.backgroundColor = UIColor.white
        }
        self.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.left.bottom.right.equalTo(self)
        }
        //课程icon
        courseImageView = UIImageView().then {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
        backView.addSubview(courseImageView)
        courseImageView.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.left.equalTo(Left_Space)
            make.size.equalTo(CGSize(width: 125, height: 70))
        }
        //课程icon cover
        let courseCoverView = UIView().then {
            $0.backgroundColor = UIColor(white: 0.3, alpha: 0.2)
        }
        courseImageView.addSubview(courseCoverView)
        courseCoverView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(courseImageView)
        }
        //记录当前为第几节课
        courseCurLab = UILabel().then {
            $0.textColor = HEX(COLOR_FONT_FFFFFF)
            $0.textAlignment = .center
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
        }
        courseCoverView.addSubview(courseCurLab)
        courseCurLab.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(courseCoverView)
        }
        //课程标题
        courseTitleLab = UILabel().then {
            $0.textColor = HEX(COLOR_FONT_464646)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
            $0.numberOfLines = 2
            $0.sizeToFit()
        }
        backView.addSubview(courseTitleLab)
        courseTitleLab.snp.makeConstraints { (make) in
            make.top.equalTo(courseImageView.snp.top)
            make.left.equalTo(courseImageView.snp.right).offset(6)
            make.right.equalTo(backView.snp.right).offset(-Left_Space)
        }
        //课程开始时间
        courseStartLab = UILabel().then {
            $0.textColor = HEX(COLOR_FONT_9B9B9B)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.numberOfLines = 1
            $0.sizeToFit()
        }
        backView.addSubview(courseStartLab)
        courseStartLab.snp.makeConstraints { (make) in
            make.top.equalTo(courseTitleLab.snp.bottom).offset(6)
            make.left.right.equalTo(courseTitleLab)
        }
        //按钮显示
        courseBt = UIButton().then {
            $0.setTitleColor(UIColor.gray, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.layer.cornerRadius = 6
            $0.layer.masksToBounds = true
            $0.layer.borderColor = HEX(COLOR_FONT_DBDBDB).cgColor
            $0.addTarget(self, action: #selector(VLSCourseListCell.jumpAction), for: .touchUpInside)
        }
        backView.addSubview(courseBt)
        courseBt.snp.makeConstraints { (make) in
            make.top.equalTo(courseImageView.snp.bottom).offset(6)
            make.right.equalTo(-Left_Space)
            make.size.equalTo(CGSize(width: 80, height: 26))
            make.bottom.equalTo(backView.snp.bottom).offset(-10)
        }
        //评价或者倒计时显示
        courseTextField = UITextField().then {
            $0.leftView = UIImageView().then {
                $0.image = #imageLiteral(resourceName: "time_selected")
                $0.frame = CGRect(x: 0, y: 0, width: 14, height: 14)
            }
            $0.leftViewMode = .always
            $0.textColor = UIColor.red
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12))
            $0.isEnabled = false
        }
        backView.addSubview(courseTextField)
        courseTextField.snp.makeConstraints{ (make) in
            make.bottom.equalTo(courseBt).offset(-4)
            make.left.equalTo(courseImageView.snp.left)
            make.right.equalTo(courseBt.snp.left).offset(20)
        }
    }
    
    override func setNewItem(_ item: VLSTableViewItem) {
        super.setNewItem(item)
        if let newItem = item as? CourseListModel {
            courseImageView.sd_setImage(with: URL(string: StringSafty(newItem.picCover)))
            courseTitleLab.text = newItem.lessonName
            courseStartLab.text = Date.dateStr((newItem.startTime?.doubleValue)!)
            courseCurLab.text = newItem.courseCurText
            if (newItem.status.intValue == 2 && IntSafty(newItem.evaluatenum?.intValue) > 0 ) || newItem.hasRating?.boolValue == true { //已评价
                courseTextField.attributedText = CustomAttributeStrings([(newItem.appraiseStr, HEX(COLOR_FONT_9B9B9B), UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12))), (newItem.appraiseScoreStr, HEX(COLOR_MSG_FFB000), UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12)))])
            } else {
                courseTextField.text = newItem.courseDetail
            }
            if pageName == "booking" {//开播了，预约用户直接显示 “进入直播间”，否则显示“开始直播”
                newItem.courseStatusText = LocalizedString("COURSE_STATUS_ON")
            } else {
                newItem.courseStatusText = LocalizedString("COURSE_STATUS_NOT")
                newItem.courseStatusBtWidth = 80
            }
            if newItem.status.intValue == 0 && newItem.countdowntime.intValue > 0 && newItem.countdowntime.intValue <= 24 * 60 * 60 * 1000 {
                //倒计时
                self.perform(#selector(OnCourseCell.resetTime))
                courseTextField.leftView = UIImageView().then {
                    $0.image = #imageLiteral(resourceName: "time_selected")
                    $0.frame = CGRect(x: 0, y: 0, width: 14, height: 14)
                }
            } else if newItem.status.intValue == 1 {
                courseTextField.leftView = UIView()
            } else if newItem.status.intValue == 2 { //已结束
                if pageName == "booking" && newItem.hasRating == true {//预约用户，已评价
                    newItem.courseStatusText = LocalizedString("COURSE_APPRAISE_END")
                } else if pageName == "booking" && newItem.hasRating == false {
                    newItem.courseBtFlag = true //预约用户，未评价，则按钮可点击
                    newItem.courseStatusText = LocalizedString("COURSE_APPRAISE")
                } else {
                    newItem.courseStatusText = LocalizedString("COURSE_STATUS_END")
                }
                courseTextField.leftView = UIView()
            } else {
                courseTextField.leftView = UIView()
            }
            courseBt.setTitle(newItem.courseStatusText, for: .normal)
            courseBt.setTitleColor(newItem.courseStatusBtTitleColor, for: .normal)
            courseBt.backgroundColor = newItem.courseStatusBtColor
            courseBt.layer.borderWidth = newItem.courseStatusBtBorderWidth
            courseBt.isEnabled = newItem.courseBtFlag //设置是否可以点击
            courseBt.snp.updateConstraints({ (make) in
                make.width.equalTo(newItem.courseStatusBtWidth)
            })
        }
    }
    
    func resetTime() {
        if self.timer == nil {
            self.surplusCountDownSeconds = 0 //重新记录倒计时总的时间
            let queue: DispatchQueue = DispatchQueue.global(qos: .default)
            var timer: DispatchSourceTimer? = DispatchSource.makeTimerSource(flags: .init(rawValue: 0), queue: queue)
            timer?.setEventHandler(handler: {
                if let newItem = self.item as? CourseListModel {
                    if let countdowntime = newItem.countdowntime {
                        var timeout = (CLongLong(countdowntime.intValue/1000) - self.surplusCountDownSeconds)  //当前cell，倒计时开始前距离开播还有多久
                        if timeout <= 0 {
                            timer?.cancel()
                            timer = nil
                            DispatchQueue.main.async {
                                if newItem.indexPath == self.indexPath {//防止cell复用问题
                                    self.courseTextField.text = ""
                                    self.courseBt.backgroundColor = UIColor.red
                                    self.courseBt.isEnabled = true
                                }
                            }
                        } else {
                            DispatchQueue.main.async {
                                if newItem.indexPath == self.indexPath {//防止cell复用问题
                                    if self.pageName == "publish" && timeout <= 5 * 60 {//(主播5分钟内可以进入直播间)
                                        if self.courseBt.isEnabled == false {
                                            self.roomIdAction()
                                        }
                                        self.courseBt.backgroundColor = UIColor.red
                                        self.courseBt.isEnabled = true
                                    }
                                    self.courseTextField.text = " " + LocalizedString("COURSE_COUNTDOWN_TEXT") + CommonSwift.timeDecrease(seconds: CLongLong(timeout))
                                }
                            }
                            timeout = timeout - 1
                            self.surplusCountDownSeconds = self.surplusCountDownSeconds + 1
                        }
                    }
                }
            })
            timer?.scheduleRepeating(deadline: .now(), interval: .seconds(1))
            timer?.resume()
            self.timer = timer
        }
    }
    
    func jumpAction() {
        if let newItem = self.item as? CourseListModel {
            if newItem.status.intValue == 2 && newItem.hasRating?.boolValue == false {//课程已结束，并且未评价
                delegate?.praiseAction!(newItem, indexPath: self.indexPath!)
            } else {
                if self.pageName == "booking" {//进入直播间
                    delegate?.jumpToRoomAction!(self.item, type: CourseType.Play.rawValue)
                } else{//开始直播
                    delegate?.jumpToRoomAction!(self.item, type: CourseType.Enter.rawValue)
                }
            }
        }
    }
    
    //倒计时5分钟内，获取roomId
    func roomIdAction() {
        if let newItem = self.item as? CourseListModel {
            delegate?.roomIdAction!(newItem.lessonId.stringValue, indexPath: self.indexPath!)
        }
    }
}

