//
//  VLSScheduleView.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

enum VLSScheduleViewRateResult {
    case failed(msg: String)
    case success(vc: UIViewController)
}

enum VLSScheduleViewRoomResult {
    case failed(msg: String)
    case createRoom(vc: UIViewController)
    case joinRoom(model: VLSLiveListModel, type: CourseType)
}

enum VLSScheduleViewPaybackResult {
    case failed(msg: String)
    case success(vc: UIViewController)
}

protocol VLSScheduleViewDelegate: NSObjectProtocol {
    func share(_ item: VLSScheduleCourseModel)
    func payback(_ result: VLSScheduleViewPaybackResult)
    func praise(_ result: VLSScheduleViewRateResult)    //评价
    func room(_ result: VLSScheduleViewRoomResult)      //进入直播间（或开始直播)
    func courseDetail(_ vc: UIViewController)
}

class VLSScheduleView: VLSTableView {

    weak var delegate: VLSScheduleViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.viewModelClass = CourseViewModel.self
        self.tableView.register(VLSScheduleCell.self, forCellReuseIdentifier: "cell")
    }
    
    func changeDataSource(_ dataSource: [VLSScheduleCourseModel]?) {
        self.removeTimer()
        if let dataSource = dataSource {
            viewModel.dataSource = NSMutableArray(array: dataSource)
            self.tableView.reloadData()
        }
    }
    
    deinit {
        self.removeTimer()
    }
    
    //销毁定时器
    func removeTimer() {
        for i in 0 ..< self.tableView.visibleCells.count {
            if self.tableView.visibleCells[i].isKind(of: VLSScheduleCell.self), let cell = self.tableView.visibleCells[i] as? VLSScheduleCell {
                cell.timer?.cancel() //销毁定时器
                cell.timer = nil
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VLSScheduleView: VLSScheduleCellDelegate {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            if let cell = cell as? VLSScheduleCell {
                if let newItem = item as? VLSScheduleCourseModel {
                    cell.delegate = self
                    cell.indexPath = indexPath
                    newItem.courseDTO?.indexPath = indexPath
                    cell.setNewItem(newItem)
                }
            }
        }
        return cell!
    }
    
    func share(_ item: VLSScheduleCourseModel) {
        self.delegate?.share(item)
    }
    
    func payback(_ courseId: String, indexPath: IndexPath) {
        let alertVC = UIAlertController(title: LocalizedString("COURSE_PAYBACK_TITLE"), message: LocalizedString("COURSE_PAYBACK_MSG"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: LocalizedString("YES"), style: .default, handler: { (action) in
            //退款
            if let viewModel = self.viewModel as? CourseViewModel {
                viewModel.payback(dic: ["id": courseId], complete: {[weak self] result in
                    if case .failed(let msg) = result {
                        self?.delegate?.payback(.failed(msg: msg))
                    }
                    else if case .success( let msg, let dic) = result {
                        self?.delegate?.payback(.failed(msg: msg))
                        if let error_code = dic?["error_code"] as? Int {//只有删除成功才reload
                            if error_code == 0 {
                                self?.viewModel.dataSource.removeObject(at: indexPath.row)
                                self?.tableView.reloadData()
                            }
                        }
                    }
                })
            }
        })
        alertVC.addAction(okAction)
        let cancelAction = UIAlertAction(title: LocalizedString("CANCLE"), style: .default, handler: {
            (action) in
            //取消
        })
        alertVC.addAction(cancelAction)
        self.delegate?.payback(.success(vc: alertVC))
    }
    
    func praise(_ lesson: VLSCustomScheduleModel, indexPath: IndexPath) {
        let newLesson = LessonModel()
        newLesson.lessonId = lesson.lessonId
        newLesson.picCover = lesson.picCover
        if let rateVC = UIStoryboard(name: "CourseRate", bundle: nil).instantiateInitialViewController() as? CourseRateViewController
        {
            rateVC.modalPresentationStyle = .overCurrentContext
            rateVC.lesson = newLesson
            let userProfile = VLSUserProfile()
            userProfile.userId = lesson.teacherId?.stringValue
            userProfile.userName = lesson.teacherName
            userProfile.userFaceURL = lesson.picTeacher
            
            rateVC.anchorProfile = userProfile
            rateVC.completeBlock = {[weak self] result in
                if case .failed(let msg) = result {
                    self?.delegate?.praise(.failed(msg: msg))
                }
                else if case .success(let score, let numberOfPeople) = result {
                    if let sc = score, let ppl = numberOfPeople {
                        if let courseModel = ObjectForKeySafety(self?.viewModel.dataSource, index: indexPath.row) as? VLSScheduleCourseModel {
                            if let item = courseModel.courseDTO {
                                item.hasRating = true //已评价
                                item.customPraisePeopleText = String(ppl) + LocalizedString("COURSE_APPRAISE_PEOPLE")
                                item.customPraiseScoreText = String(sc) + LocalizedString("COURSE_SCORE")
                                item.customCourseStatus = CourseStatus.HasBeenPraise.rawValue
                                item.customCourseStatusText = LocalizedString("COURSE_APPRAISE_END")
                                item.customCourseStatusColor = HEX(COLOR_FONT_DBDBDB)
                                item.customCourseBtFlag = false
                                courseModel.courseDTO = item
                                self?.viewModel.dataSource.replaceObject(at: indexPath.row, with: courseModel)
                                self?.tableView.reloadRows(at: [indexPath], with: .automatic)
                            }
                        }
                    }
                }
            }
            self.delegate?.praise(.success(vc: rateVC))
        }
    }
    
    func room(_ lesson: AnyObject?, type: NSInteger) {
        switch type {
        case CourseType.Play.rawValue:
            if let item = lesson as? VLSCustomScheduleModel {
                if let roomId = item.roomId {//主播已开播
                    let model = VLSLiveListModel()
                    model.host = item.teacherId?.stringValue
                    model.courseId = item.courseId?.stringValue
                    model.lessonId = item.lessonId?.stringValue
                    model.roomId = StringSafty(roomId)
                    model.title = item.courseName
                    self.delegate?.room(.joinRoom(model: model, type: .Play))
                } else {//主播未开播
                    self.delegate?.room(.failed(msg: LocalizedString("COURSE_STATUS_ON_TEXT")))
                }
            }
        case CourseType.Enter.rawValue:
            if let item = lesson as? VLSCustomScheduleModel {
                if let roomId = item.roomId { //如果已创建过直播间，直接进入
                    let model = VLSLiveListModel()
                    model.host = item.teacherId?.stringValue
                    model.courseId = item.courseId?.stringValue
                    model.lessonId = item.lessonId?.stringValue
                    model.roomId = StringSafty(roomId)
                    model.title = item.courseName
                    self.delegate?.room(.joinRoom(model: model, type: .Enter))
                } else {//否则创建直播间
                    let liveViewController = LiveBeforeTestViewController()
                    liveViewController.liveBeforeTitle = item.courseName
                    liveViewController.lessonId = item.lessonId
                    liveViewController.topicStr = StringSafty(item.courseType?.stringValue) + "," + StringSafty(item.courseTypeName)
                    if let url = URL(string: StringSafty(item.picCover)) {
                        do {
                            let image = try UIImage(data: NSData(contentsOf: url) as Data)
                            liveViewController.infoView.coverImage = image
                        } catch {
                        }
                    }
                    self.delegate?.room(.createRoom(vc: liveViewController))
                }
            }
        default:
            break;
        }
    }
    
    //获取roomId
    func roomId(_ courseId: String, indexPath: IndexPath) {
        if let viewModel = self.viewModel as? CourseViewModel {
            viewModel.get(api: APIConfiguration.shared().courseRoomIdUrl, dic: ["lessonId": courseId], complete: { [weak self](result) in
                if case .failed( _) = result {
                }
                else if case .success( _, let dic) = result {
                    let roomId = ObjectForKeySafety(NSDictionarySafty(dic), key: "roomInfo")
                    if let courseModel = ObjectForKeySafety(self?.viewModel.dataSource, index: indexPath.row) as? CourseListModel {
                        if let newRoomId = roomId {
                            courseModel.roomId = StringSafty(newRoomId)
                            self?.viewModel.dataSource.replaceObject(at: indexPath.row, with: courseModel)
                            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
                        }
                    }
                }
            })
        }
    }
    
    func courseDetail(_ vc: UIViewController) {
        self.delegate?.courseDetail(vc)
    }
}
