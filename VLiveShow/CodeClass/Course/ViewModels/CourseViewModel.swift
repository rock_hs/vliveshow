//
//  CourseViewModel.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

enum RequestResult
{
    case failed(msg: String)
    case success(msg: String, dic: NSDictionary?)
    case progress(dic: NSDictionary?)
}

typealias completeHandler = (_ result: RequestResult) -> Void

class CourseViewModel: VLSTableViewModel {

    //退款
    func payback(dic: NSDictionary? = nil, complete: @escaping completeHandler) {
        
        let callback = HttpCallBack()
        callback.doneBlock =
            { ret in
                guard let result = ret as? [String: Any] else {
                    complete(.failed(msg: LocalizedString("ALERT_UNKNOW_ERROR")))
                    return
                }
                if  let _ = result["success"] as? Bool, let error_code = result["error_code"] as? Int {
                    var message = ErrorEvent.error(error_code, msg: StringSafty(result["message"]))
                    if error_code == 0 {
                        message = LocalizedString("COURSE_PAYBACK_SUCCESS")
                    }
                    complete(.success(msg: message, dic: result as NSDictionary?))
                }
        }
        callback.errorBlock = { err in
                complete(.failed(msg: LocalizedString("ALERT_NETWORK_ERROR")))
        }
        HTTPFacade.delete(APIConfiguration.shared().coursePaybackUrl, parameter: dic as! [AnyHashable : Any]! , callback: callback)
    }
    
    //获取roomId
    func roomId(api: String,dic: NSDictionary? = nil, complete: @escaping completeHandler) {
        self.get(api: api, dic: dic, complete: complete)
    }
    
}
