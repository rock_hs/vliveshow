//
//  VLSTableViewModel.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

typealias VLSTableViewModelSuccess = () -> Void
typealias VLSTableViewModelFail = () -> Void
typealias VLSTableViewModelMoreData = (_ moreData: Bool,_ showFootor: Bool) -> Void
typealias VLSTableViewModelEmpty = () -> Void

var serviceTime: Double = 0.0

class VLSTableViewModel: NSObject {
    
    var page = 0
    var pageDirection = 1 //默认刷新数据
    var cellClass: AnyClass? = VLSTableViewCell.self
    var itemClass: AnyClass = VLSTableViewItem.self
    var api: String?
    var postDic: NSDictionary?
    var dataSource: NSMutableArray = NSMutableArray()
    
    var loadSuccess: VLSTableViewModelSuccess!
    var loadMoreData: VLSTableViewModelMoreData!
    var loadFailed: VLSTableViewModelFail!
    var loadEmpty: VLSTableViewModelEmpty!

    required override init() {
        super.init()
    }
    
    //加载数据
    func loadDataSource(_ key:String = "list") {
        guard let api = api else {
            return //接口不能为空
        }
        self.get(api: api, dic: postDic, complete: {[weak self] result in
            if case .failed( _) = result {
//                self?.showProgressHUD(msg)
            }
            else if case .success( _, let dict) = result {
//                self?.showProgressHUD(msg)
                if let error_code = dict?["error_code"] as? Int {//只有删除成功才reload
                    if error_code == 0 {
                        let dic = ObjectForKeySafety(dict, key: "results")
                        if let curserviceTime = ObjectForKeySafety(NSDictionarySafty(dic), key: "serviceTime") {
                            serviceTime = (StringSafty(curserviceTime) as NSString).doubleValue
                        }
                        self?.handleRequestDicSuccess(NSDictionarySafty(dic), key: key, dataSource: NSMutableArraySafty(self?.dataSource))
                    }
                }
            }
        })
    }
    
    func handleRequestDicSuccess(_ dic: NSDictionary, key:String = "", dataSource: NSMutableArray) {
        if key == "" {
            if let type = self.itemClass as? VLSTableViewItem.Type {
                let item = type.init(map: Map(mappingType: .fromJSON, JSON: dic as! [String : Any]))
                if self.page == 0 {
                    dataSource.removeAllObjects()
                }
                dataSource.add(item!)
                if let loadSuccess = self.loadSuccess {
                    loadSuccess()
                }
            }
        } else {
            let array = NSArraySafty(ObjectForKeySafety(dic, key: key))
            self.handleRequestSuccess(array, dataSource: dataSource)
            self.loadMoreFlag(array, dataSource: dataSource)
        }
    }
    
    func handleRequestSuccess(_ data: NSArray, dataSource: NSMutableArray){
        if self.page == 0 {
            dataSource.removeAllObjects()
        }
        if data.count > 0{
            for dic in data {
                if let type = self.itemClass as? VLSTableViewItem.Type {
                    let item = type.init(map: Map(mappingType: .fromJSON, JSON: dic as! [String : Any]))
                    dataSource.add(item!)
                }
            }
        }
    }
    
    func loadMoreFlag(_ data: NSArray, dataSource: NSMutableArray) {
        if dataSource.count == 0 {
            if let loadEmpty = self.loadEmpty {
                loadEmpty()
            }
        } else {
            if data.count > 0{
                self.page += 1
                if let loadMoreData = self.loadMoreData {
                    loadMoreData(true,true)
                }
            }else{
                if self.page == 0{
                    if let loadMoreData = self.loadMoreData {
                        loadMoreData(false,false)
                    }
                }else{
                    if let loadMoreData = self.loadMoreData {
                        loadMoreData(false,true)
                    }
                }
            }
        }
    }
    
    func get(api: String, dic: NSDictionary? = nil, complete: @escaping completeHandler) {
        let callback = HttpCallBack()
        callback.doneBlock =
            { ret in
                guard let result = ret as? [String: Any] else {
                    complete(.failed(msg: LocalizedString("ALERT_UNKNOW_ERROR")))
                    return
                }
                if  let _ = result["success"] as? Bool {
                    complete(.success(msg: result["message"] as! String, dic: result as NSDictionary?))
                }
        }
        callback.errorBlock = { err in
            complete(.failed(msg: LocalizedString("ALERT_NETWORK_ERROR")))
        }
        HTTPFacade.get(api, parameter: dic as! [AnyHashable : Any]! , callback: callback)
    }
}
