//
//  VLSAllCourseViewController.swift
//  VLiveShow
//
//  Created by rock on 2017/1/5.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSAllCourseViewController: VLSBaseViewController {

    fileprivate var pageView: VLSPageControlView!
    fileprivate var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let textWidth = MultilineTextSize(text: LocalizedString("COURSE_MY_PUBLISH"), font: UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_17)), maxSize: CGSize(width: self.view.frame.width, height: CGFloat(MAXFLOAT))).width
        pageView = VLSPageControlView().then {
            $0.frame = CGRect(x: self.view.frame.width - 2*textWidth*2 - 40, y: 20, width: textWidth*2 + 40, height: (self.navigationController?.navigationBar.frame.height)!)
            $0.addSubBt([LocalizedString("COURSE_MY_PUBLISH"), LocalizedString("COURSE_MY_BOOKING")])
            $0.delegate = self
        }
        self.navigationItem.titleView = pageView
        
        let messageView = VLSMessageImageView.shared
        messageView.delegate = self
        messageView.changeTintColor(UIColor.titleColorInLightNavi())
        messageView.changeDotColor(UIColor.red)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: messageView)
        
        scrollView = UIScrollView().then {
            $0.showsVerticalScrollIndicator = false
            $0.showsHorizontalScrollIndicator = false
            $0.isPagingEnabled = true
            $0.contentSize = CGSize(width: self.view.frame.width * 2, height: 0)
            $0.delegate = self
        }
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self.view)
        }
        
        let publishVC = PublishCourseViewController()
        self.addChildViewController(publishVC)
        publishVC.view.frame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
        scrollView.addSubview(publishVC.view)
        
        let bookingVC = BookingCourseViewController()
        self.addChildViewController(bookingVC)
        bookingVC.view.frame = CGRect(x: self.view.frame.width, y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
        scrollView.addSubview(bookingVC.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.setStatusBarStyle(.default, animated: false)
        self.navigationController?.navigationBar.useLightTheme()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension VLSAllCourseViewController: VLSMessageImageViewDelegate, UIScrollViewDelegate, VLSPageControlViewDelegate {
    //查看信息
    func message() {
        // 跳转到站内信
        let messageVC = VLSMessageViewController()
        CommonSwift.pushViewController(messageVC, navigation: self.navigationController)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.x * pageView.coverView.frame.width / scrollView.frame.width
        pageView.changeView(by: offset)
    }
    
    func change(view tag: NSInteger) {
        scrollView.contentOffset = CGPoint(x: self.view.frame.width * CGFloat(tag-1), y: 0)
    }
}
