//
//  VLSIntroduceViewController.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSIntroduceViewControllerDelegate {
    func jointAction()
}

class VLSIntroduceViewController: UIViewController {

    public weak var delegate: VLSIntroduceViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scrollView = UIScrollView().then {
            $0.frame = UIScreen.main.bounds
            $0.contentSize = CGSize(width: UIScreen.main.bounds.width * 4, height: 0)
            $0.isPagingEnabled = true
        }
        self.view.addSubview(scrollView)
        //添加引导图
        for i in 1...4 {
            let imageView = UIImageView().then {
                $0.frame = CGRect(x: CGFloat(i-1) * scrollView.bounds.width, y: 0, width: scrollView.bounds.width, height: scrollView.bounds.height)
                $0.image = UIImage(named: "Intro_\(i)")
            }
            scrollView.addSubview(imageView)
            if i == 4 {
                imageView.isUserInteractionEnabled = true
                //进入首页
                let jointBt = UIButton().then {
                    $0.backgroundColor = UIColor.clear
                    $0.addTarget(self, action: #selector(VLSIntroduceViewController.jointAction), for: .touchUpInside)
                }
                imageView.addSubview(jointBt)
                jointBt.snp.makeConstraints({ (make) in
                    make.left.right.equalTo(imageView)
                    make.bottom.equalTo(imageView).offset(-45)
                    make.height.equalTo(60)
                })
            }
        }
        UserDefaults.standard.set(false, forKey: isFirstStart)
        UserDefaults.standard.synchronize()
        VLSScheduleCalendarPreDate = ""

        // Do any additional setup after loading the view.
    }
    
    //进入首页
    func jointAction() {
        self.view.removeFromSuperview()
        delegate?.jointAction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
