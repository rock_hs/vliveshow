//
//  VLSTableViewController.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import SnapKit
import MJRefresh

class VLSTableViewController: VLSBaseViewController {

    var tableView: UITableView!
    var viewModel: VLSTableViewModel!
    var viewModelClass: AnyClass? {
        didSet {
            self.initViewModel()
            self.setViewModelLoadEvent()
        }
    }
    fileprivate var errorView: VLSEmptyView?
    fileprivate var isremoveMJ_HeaderFlag: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = HEX(COLOR_BG_EEEEEE)
        self.initViewModel()
        self.setViewModelLoadEvent()
        tableView = UITableView().then {
            $0.register(viewModel.cellClass, forCellReuseIdentifier: "cell")
            $0.separatorStyle = .none
            $0.backgroundColor = UIColor.clear
            $0.delegate = self
            $0.dataSource = self
            $0.estimatedRowHeight = 100
            $0.rowHeight = UITableViewAutomaticDimension
        }
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self.view)
        }
        let header = MJRefreshGifHeader(refreshingTarget: self, refreshingAction: #selector(VLSTableViewController.reloadData))
        header?.setTitle(LocalizedString("LIVE_PULL_REFRESH"), for: .idle)
        header?.setTitle(LocalizedString("LIVE_PULL_REFRESH"), for: .pulling)
        header?.setTitle(LocalizedString("LIVE_PULL_REFRESHING"), for: .refreshing)
        header?.setImages([UIImage(named: "home_logo") as Any], duration: 2, for: .idle)
        tableView.mj_header = header
        
        // Do any additional setup after loading the view.
    }
    
    //初始化viewModel
    func initViewModel() {
        if let viewModelClass = viewModelClass, let type = viewModelClass as? VLSTableViewModel.Type {
            viewModel = type.init()
        } else {
            viewModel = VLSTableViewModel()
        }
    }
    
    //重新加载数据
    func reloadData() {
        viewModel.page = 0
        viewModel.pageDirection = 1
        viewModel.loadDataSource()
    }
    
    //加载更多
    func loadMoreData() {
        viewModel.pageDirection = 1  //加载更多
        viewModel.loadDataSource()
    }
    
    fileprivate func setViewModelLoadEvent() {//设置加载完成时间
        viewModel.loadSuccess = {[weak self] in
            self?.setViewModelLoadSuccess()
        }
        viewModel.loadEmpty = {[weak self] in
            self?.setViewModelLoadEmpty()
        }
        viewModel.loadFailed = {[weak self] in
            self?.setViewModelLoadFailed()
        }
        viewModel.loadMoreData = {
            [weak self](moreData,showFootor) in
            self?.setViewModelLoadMore(moreData, showFootor: showFootor)
        }
    }
    
    func setViewModelLoadSuccess() {//加载成功
        self.removeErrorView()
        self.tableView.reloadData()
        if isremoveMJ_HeaderFlag == false, let mj_header = self.tableView.mj_header {
            mj_header.endRefreshing()
        }
    }
    
    func setViewModelLoadEmpty() {//无数据
        self.addErrorView()
        self.tableView.reloadData()
        if isremoveMJ_HeaderFlag == false, let mj_header = self.tableView.mj_header {
            mj_header.endRefreshing()
        }
        if let mj_footer = self.tableView.mj_footer {
            mj_footer.endRefreshing()
        }
        self.tableView.mj_footer = nil
    }
    
    func setViewModelLoadFailed() {//加载失败
        self.addErrorView()
        self.tableView.reloadData()
        if isremoveMJ_HeaderFlag == false, let mj_header = self.tableView.mj_header {
            mj_header.endRefreshing()
        }
        if let mj_footer = self.tableView.mj_footer {
            mj_footer.endRefreshing()
        }
        self.tableView.mj_footer = nil
    }
    
    func setViewModelLoadMore(_ moreData:Bool,showFootor:Bool) {//加载更多
        self.setViewModelLoadSuccess()
        //如果请求数据需要分页，则显示上拉刷新
        if self.viewModel.dataSource.count > 0, let _ = self.tableView.mj_footer {
            self.tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(VLSTableViewController.loadMoreData))
        }
        //是否提示“没有更多数据”
        if let mj_footer = self.tableView.mj_footer {
            if moreData == true {
                mj_footer.resetNoMoreData()
            } else {
                mj_footer.endRefreshingWithNoMoreData()
            }
        }
    }
    
    func addErrorView() {
        let errorView = VLSEmptyView(frame: self.view.bounds).then {
            $0.delegate = self
        }
        self.addErrorView(errorView)
    }
    
    //添加错误视图
    func addErrorView(_ errorView: VLSEmptyView?) {
        if self.errorView == nil {
            if let errorView = errorView {
                self.errorView = errorView
                self.view.addSubview(errorView)
            }
        }
    }
    
    func removeErrorView() {
        errorView?.removeFromSuperview()
        errorView = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        UIApplication.shared.setStatusBarStyle(.default, animated: false)
        self.navigationController?.navigationBar.useLightTheme()
    }
    
    //进入直播
    func room(_ model: VLSLiveListModel) {
        let show = VLSShowViewController()
        show.anchorId = model.host
        show.roomId = model.roomId
        show.orientation = KLiveRoomOrientation_Portrait
        show.courseId = model.courseId
        show.lessonId = model.lessonId
        let joinRoom = VLSJoinRoomModel()
        joinRoom.roomID = model.roomId
        joinRoom.joinType = "living_list"
        show.roomTrackContext = VLSUserTrackingManager.share().trackingJoinroom(joinRoom)
        show.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(show, animated: true)
    }
    
    //主播重连
    func intoAnchorVC(_ model: VLSLiveListModel) {
        let anchorVC = VLSAnchorViewController()
        VLSIMManager.sharedInstance().roomID = model.roomId
//        VLSMessageManager.shared().lesssonId = model.lessonId
        if model.lessonId.characters.count > 0 {
            anchorVC.actionType = ActionTypeLessonTeacher
        }
        anchorVC.roomId = model.roomId
        anchorVC.anchorId = model.host
        anchorVC.lessonId = model.lessonId
        anchorVC.courseId = model.courseId
        anchorVC.isReconnection = true
        anchorVC.title = model.title
        let nav = UINavigationController(rootViewController: anchorVC)
        self.present(nav, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension VLSTableViewController: UITableViewDelegate, UITableViewDataSource, VLSEmptyViewDelegate {

    //pragma mark -- UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            if let cell = cell as? VLSTableViewCell {
                if let newItem = item as? VLSTableViewItem {
                    cell.indexPath = indexPath
                    cell.setNewItem(newItem)
                }
            }
        }
        return cell!
    }
    
    func tryAction() {
        
    }
}
