//
//  BookingCourseViewController.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class BookingCourseViewController: CourseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = LocalizedString("COURSE_MY_BOOKING")
        let messageView = VLSMessageImageView.shared
        messageView.delegate = self
        messageView.changeTintColor(UIColor.titleColorInLightNavi())
        messageView.changeDotColor(UIColor.red)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: messageView)

        self.pageName = "booking"
        
        self.viewModelClass = CourseViewModel.self
        viewModel.itemClass = PublishCourseModel.self
        self.viewModel.api = APIConfiguration.shared().courseBookingUrl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension BookingCourseViewController: VLSMessageImageViewDelegate {
    
    //退款
    func paybackAction(_ courseId: String, indexPath: IndexPath) {
        let alertVC = UIAlertController(title: LocalizedString("COURSE_PAYBACK_TITLE"), message: LocalizedString("COURSE_PAYBACK_MSG"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: LocalizedString("YES"), style: .default, handler: { (action) in
            //退款
            if let viewModel = self.viewModel as? CourseViewModel {
                viewModel.payback(dic: ["id": courseId], complete: {[weak self] result in
                    if case .failed(let msg) = result {
                        self?.showHUD(msg)
                    }
                    else if case .success( let msg, let dic) = result {
                        self?.showProgressHUD(msg)
                        if let error_code = dic?["error_code"] as? Int {//只有删除成功才reload
                            if error_code == 0 {
                                self?.viewModel.dataSource.removeObject(at: indexPath.row)
                                self?.tableView.reloadData()
                            }
                        }
                    }
                })
            }
        })
        alertVC.addAction(okAction)
        let cancelAction = UIAlertAction(title: LocalizedString("CANCLE"), style: .default, handler: {
            (action) in
            //取消
        })
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    override func tryAction() {//去看看
        self.tabBarController?.selectedIndex = 0;
    }
    
    //查看信息
    func message() {
        // 跳转到站内信
        let messageVC = VLSMessageViewController()
        CommonSwift.pushViewController(messageVC, navigation: self.navigationController)
    }
}
