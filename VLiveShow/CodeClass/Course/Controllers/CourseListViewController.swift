//
//  CourseListViewController.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class CourseListViewController: VLSTableViewController {

    var course_id: String?
    var pageName: String?
    var teacherId: String?
    var teacherName: String?
    var picTeacher: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = LocalizedString("COURSE_LIST")
        
        //tableView上移8px
        self.tableView.snp.updateConstraints { (make) in
            make.top.equalTo(-8)
        }
        self.tableView.register(VLSCourseListCell.self, forCellReuseIdentifier: "listCell")
        self.viewModel.itemClass = CourseListModel.self
        self.viewModel.api = APIConfiguration.shared().courseListUrl
        self.viewModel.postDic = ["courseId": StringSafty(course_id)]
        self.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeTimer()
    }
    
    override func reloadData() {
        self.removeTimer()//重新加载数据前，先销毁定时器，重新开始计算总的倒计时数
        super.reloadData()
    }
    
    //销毁定时器
    func removeTimer() {
        for i in 0 ..< self.tableView.visibleCells.count {
            if self.tableView.visibleCells[i].isKind(of: VLSCourseListCell.self), let cell = self.tableView.visibleCells[i] as? VLSCourseListCell {
                cell.timer?.cancel() //销毁定时器
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CourseListViewController: CourseCellDelegate {

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell")
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            if let cell = cell as? VLSCourseListCell {
                if let newItem = item as? CourseListModel {
                    cell.pageName = pageName
                    cell.indexPath = indexPath
                    newItem.indexPath = indexPath
                    cell.setNewItem(newItem)
                    cell.delegate = self
                }
            }
        }
        return cell!
    }
    
    //评价
    func praiseAction(_ lesson: LessonModel, indexPath: IndexPath) {
        if let rateVC = UIStoryboard(name: "CourseRate", bundle: nil).instantiateInitialViewController() as? CourseRateViewController
        {
            rateVC.modalPresentationStyle = .overCurrentContext
            let userProfile = VLSUserProfile()
            userProfile.userId = lesson.teacherId?.stringValue
            userProfile.userName = self.teacherName
            userProfile.userFaceURL = self.picTeacher
            rateVC.lesson = lesson
            rateVC.anchorProfile = userProfile
            rateVC.completeBlock = {[weak self] result in
                if case .failed(let msg) = result {
                    self?.showHUD(msg)
                }
                else if case .success(let score, let numberOfPeople) = result {
                    if let sc = score, let ppl = numberOfPeople {
                        if let courseModel = ObjectForKeySafety(self?.viewModel.dataSource, index: indexPath.row) as? CourseListModel {
                            courseModel.hasRating = true //已评价
                            courseModel.appraiseStr = String(ppl) + LocalizedString("COURSE_APPRAISE_PEOPLE")
                            courseModel.appraiseScoreStr = String(sc) + LocalizedString("COURSE_SCORE")
                            courseModel.courseBtFlag = false //按钮不可点击
                            self?.viewModel.dataSource.replaceObject(at: indexPath.row, with: courseModel)
                            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
                        }
                    }
                }
            }
            self.present(rateVC, animated: false, completion: nil)
        }
    }
    
    //开始直播或进入直播
    func jumpToRoomAction(_ lesson: AnyObject?, type: NSInteger) {
        self.hideProgressHUD(afterDelay: 1.5)
        switch type {
        case CourseType.Play.rawValue:
            if lesson is CourseListModel {
                if let item = lesson as? CourseListModel {
                    if let roomId = item.roomId {//主播已开播
                        let model = VLSLiveListModel()
                        model.host = item.teacherId?.stringValue
                        model.courseId = item.courseId.stringValue
                        model.lessonId = item.lessonId.stringValue
                        model.roomId = roomId
                        model.title = item.lessonName
                        self.room(model)
                    } else {//主播未开播
                        self.hideProgressHUD(afterDelay: 0)
                        self.showHUD(LocalizedString("COURSE_STATUS_ON_TEXT"))
                    }
                }
            }
        case CourseType.Enter.rawValue:
            if lesson is CourseListModel {
                if let item = lesson as? CourseListModel {
                    if let roomId = item.roomId { //如果已创建过直播间，直接进入
                        let model = VLSLiveListModel()
                        model.host = item.teacherId?.stringValue
                        model.courseId = item.courseId.stringValue
                        model.lessonId = item.lessonId.stringValue
                        model.roomId = roomId
                        model.title = item.lessonName
                        self.intoAnchorVC(model)
                    } else {//否则创建直播间
                        let liveViewController = LiveBeforeTestViewController()
                        liveViewController.liveBeforeTitle = item.lessonName
                        liveViewController.lessonId = item.lessonId
                        liveViewController.topicStr = item.courseTypeName
                        if let url = URL(string: StringSafty(item.picCover)) {
                            do {
                                let image = try UIImage(data: NSData(contentsOf: url) as Data)
                                liveViewController.infoView.coverImage = image
                            } catch {
                            }
                        }
                        liveViewController.getUserRoomID(self.navigationController)
                    }
                }
            }
        default:
            return
        }
    }
    
    //获取roomId
    func roomIdAction(_ courseId: String, indexPath: IndexPath) {
        if let viewModel = self.viewModel as? CourseViewModel {
            viewModel.get(api: APIConfiguration.shared().courseRoomIdUrl, dic: ["lessonId": courseId], complete: { [weak self](result) in
                if case .failed(let msg) = result {
                    self?.showProgressHUD(msg)
                }
                else if case .success( _, let dic) = result {
                    let roomId = ObjectForKeySafety(NSDictionarySafty(dic), key: "roomInfo")
                    if let courseModel = ObjectForKeySafety(self?.viewModel.dataSource, index: indexPath.row) as? CourseListModel {
                        if let newRoomId = roomId {
                            courseModel.roomId = StringSafty(newRoomId)
                            self?.viewModel.dataSource.replaceObject(at: indexPath.row, with: courseModel)
                            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
                        }
                    }
                }
            })
        }
    }
}
