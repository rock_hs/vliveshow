//
//  PublishCourseViewController.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class PublishCourseViewController: CourseViewController {

    var userId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = LocalizedString("COURSE_MY_PUBLISH")
        
        self.pageName = "publish"
        self.viewModel.api = APIConfiguration.shared().coursePublishUrl

        // Do any additional setup after loading the view.
    }
    
    override func addErrorView() {
        let errorView = VLSEmptyView(frame: self.view.bounds).then {
            $0.delegate = self
            $0.errorImageName("mypublish_emptyPage")
            $0.errorMsg(LocalizedString("COURSE_PUBLISH_EMPTY"))
            $0.errorBtHidden(true)
        }
        self.addErrorView(errorView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
