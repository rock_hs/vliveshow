//
//  VLSScheduleViewController.swift
//  VLiveShow
//
//  Created by rock on 2017/1/4.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSScheduleViewController: VLSCollectionViewController {

    fileprivate var headerView: VLSScheduleCalendarReusableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = LocalizedString("COURSE_SCHEDULE_PLAN")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: LocalizedString("COURSE_SCHEDULE_HISTORY"), style: .plain, target: self, action: #selector(VLSScheduleViewController.historyCourse))
        self.navigationItem.leftBarButtonItem?.tintColor = HEX(COLOR_FONT_4A4A4A)
        
        headerView = VLSScheduleCalendarReusableView()
        headerView.delegate = self
        self.view.addSubview(headerView)
        headerView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.height.equalTo(76)
        }
        
        self.collectionView.register(VLSScheduleCollectionViewCell.self, forCellWithReuseIdentifier: "coursecollectionCell")
        self.collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom)
            make.left.right.bottom.equalTo(self.view)
        }
        self.viewModelClass = CourseViewModel.self
        viewModel.api = APIConfiguration.shared().scheduleUrl;
        viewModel.itemClass = VLSScheduleDataModel.self
    }
    
    override func setViewModelLoadSuccess() {//加载成功
        let scheduleDataItem = ObjectForKeySafety(viewModel.dataSource, index: 0)
        if let scheduleDataItem = scheduleDataItem as? VLSScheduleDataModel {
            headerView.changeDataSource(scheduleDataItem.dateList)
            if let list = scheduleDataItem.list {
                if list.count  > 0 {
                    super.setViewModelLoadSuccess()
                } else {
                    super.setViewModelLoadEmpty()
                }
            }
        }
    }
    
    override func addErrorView() {
        let errorView = VLSScheduleEmptyView(frame: self.collectionView.bounds)
        errorView.delegate = self
        let scheduleDataItem = ObjectForKeySafety(viewModel.dataSource, index: 0)
        if let scheduleDataItem = scheduleDataItem as? VLSScheduleDataModel {
            errorView.changeDataSource(scheduleDataItem.recommendationList)
        }
        self.addErrorView(errorView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let messageView = VLSMessageImageView.shared
        messageView.delegate = self
        messageView.changeTintColor(UIColor.titleColorInLightNavi())
        messageView.changeDotColor(UIColor.red)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: messageView)
        self.navigationController?.navigationBar.useLightTheme()
        
        if VLSScheduleCalendarPreDate != "" {
            viewModel.postDic = ["from": VLSScheduleCalendarPreDate]
        }
        self.reloadData("")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension VLSScheduleViewController: VLSMessageImageViewDelegate, VLSScheduleCollectionViewCellDelegate, VLSScheduleCalendarReusableViewDelegate, VLSShareSheetViewDelegate, VLSScheduleEmptyViewDelegate {

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coursecollectionCell", for: indexPath)
        if let cell = cell as? VLSScheduleCollectionViewCell {
            cell.delegate = self
            let scheduleDataItem = ObjectForKeySafety(viewModel.dataSource, index: 0)
            if let scheduleDataItem = scheduleDataItem as? VLSScheduleDataModel {
                cell.changeDataSource(scheduleDataItem.list)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    //进入历史课程
    func historyCourse() {
        if AccountManager.shared().account.isHost {
            CommonSwift.pushViewController(VLSAllCourseViewController(), navigation: self.navigationController)
        } else {
            CommonSwift.pushViewController(BookingCourseViewController(), navigation: self.navigationController)
        }
    }
    
    //查看信息
    func message() {
        // 跳转到站内信
        let messageVC = VLSMessageViewController()
        CommonSwift.pushViewController(messageVC, navigation: self.navigationController)
    }
    
    //查看每个日期对应的计划
    func loadData(from date: String) {
        viewModel.postDic = ["from": date]
        self.reloadData("")
    }
    
    func share(_ item: VLSScheduleCourseModel) {//分享
        VLSShareSheetView.shared.shareSheet(view: self.view, item: item)
        VLSShareSheetView.shared.delegate = self
    }
    
    func payback(_ result: VLSScheduleViewPaybackResult) {
        if case .failed(let msg) = result {
            self.showHUD(msg)
        } else if case .success(let vc) = result {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func praise(_ result: VLSScheduleViewRateResult) {
        if case .failed(let msg) = result {
            self.showHUD(msg)
        } else if case .success(let vc) = result {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func room(_ result: VLSScheduleViewRoomResult) {
        self.showProgressHUD("")
        if case .failed(let msg) = result {
            self.hideProgressHUD(afterDelay: 0)
            self.showHUD(msg)
        } else if case .createRoom(let vc) = result {
            if let vc = vc as? LiveBeforeTestViewController {
                vc.getUserRoomID(self.navigationController)
            }
        } else if case .joinRoom(let model, let type) = result {
            if type == .Play {
                self.room(model)
            } else if type == .Enter {
                self.intoAnchorVC(model)
            }
        }
    }
    
    func xs_actionSheet(_ actionSheet: ShareActionSheet!, clickedButtonIndex index: Int) {
        if index > 5 || !ShareActionSheet.isInstallation(index) {
            self.showAlterviewController(LocalizedString("SHARE_HTML_NO_INSTALL_APP"))
        } else {
            if let item = VLSShareSheetView.shared.item as? VLSScheduleCourseModel {
                if let courseItem = item.courseDTO {
                    var dict = [:] as [String : Any]
                    dict = ["coverImageURL": StringSafty(courseItem.picCover), "content": StringSafty(courseItem.courseName), "title": StringSafty(courseItem.courseName), "overrideTitleInWXTimeline": false, "urlString": StringSafty(String(format: "%@/pages/course/detail.html?id=%@", VLIVESHOW_HOST, StringSafty(courseItem.courseId?.stringValue)))] as [String : Any]
                    self.share(toPlatform: index, messageObject: dict, completion: { (item) in
                        actionSheet.dismiss()
                        self.shareTrackingLive(after: StringSafty(courseItem.courseId?.stringValue), index: index)
                    })
                }
            }
        }
    }
    
    func courseDetail(_ vc: UIViewController) {
        CommonSwift.pushViewController(vc, navigation: self.navigationController)
    }
}
