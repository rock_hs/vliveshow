//
//  CourseRateViewController.swift
//  VLiveShow
//
//  Created by VincentX on 16/11/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

enum CourseRateResult
{
    case failed(msg: String)
    case success(score: Float?, numberOfRatedPeople: Int?)
}

enum RatingParameters
{
    case lessonModel(LessonModel)
    case lessonRaw(coverImage: UIImage?, coverURL: URL?, lessonId: NSNumber)
    case liveShowModel(VLSLiveInfoViewModel)
    case liveShowRaw(coverImage: UIImage?, coverURL: URL?, roomId: String)
}

class CourseRateViewController: UIViewController, UITextViewDelegate {
    
    //@IBOutlet var coverImageView: UIImageView!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var displayNameLabel: UILabel!
    
    @IBOutlet var rateTextLabel: UILabel!
    @IBOutlet var rateButtons: [UIButton]!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet weak var ratePopupView: UIView!
    
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var commentNumberLabel: UILabel!
    

    @IBOutlet weak var popCenterConstraint: NSLayoutConstraint!
    var commentString: String = ""
    let CommentPlaceHolderString = _LS("COURSE_INPUT_COMMNENT")
    
    var completeBlock: ((_ result: CourseRateResult) -> Void)?
    
    @objc var finishBlock: ((_ result: Bool) -> Void)?
    
    let MaxCommentTextLength = 100
    
    // 如果传入lesson model，则忽略coverImage,coverImageURL和lessonId
    var lesson: LessonModel?
    {
        didSet
        {
            if self.isViewLoaded
            {
                self.refreshUI()
            }
        }
    }
    
    @objc var liveShowModel: VLSLiveInfoViewModel?
    {
        didSet
        {
            if self.isViewLoaded
            {
                self.refreshUI()
            }
        }
    }
    
    @objc var anchorProfile: VLSUserProfile?
    {
        didSet
        {
            if self.isViewLoaded
            {
                self.refreshUI()
            }
        }
    }
    
    
    // 如果传入lesson model，则忽略coverImage
    var coverImage: UIImage?
    
    // 如果传入了coverImage，则忽略coverImageURL
    var coverImageURL: URL?
    
    // 如果传入lesson model， 则忽略lessonId
    var lessonId: NSNumber?
    
    // 如果传入roomId，则忽略lesson参数
    var roomId: String?
    
    var rate: Int = 0
    
    var ratingParameter: RatingParameters?
    {
        didSet
        {
            guard let param = self.ratingParameter else
            {
                return
            }
            
            switch(param)
            {
            case .lessonModel(let aLesson):
                self.lesson = aLesson
                break
            case .lessonRaw(let anImage, let anImageURL, let lessonId):
                self.coverImage = anImage
                self.coverImageURL = anImageURL
                self.lessonId = lessonId
                break
            case .liveShowModel(let liveShow):
                self.liveShowModel = liveShow
                break
            case .liveShowRaw(coverImage: let anImage, coverURL: let anImageURL, roomId: let roomId):
                self.coverImage = anImage
                self.coverImageURL = anImageURL
                self.roomId = roomId
                break
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.rateTextLabel.text = "";
        self.commentTextView.text = CommentPlaceHolderString
        self.submitButton.setBackgroundImage(UIImage(from: UIColor.bgLightGray()), for: .disabled)
        self.submitButton.setBackgroundImage(UIImage(from: UIColor.themeRed()), for: .normal)
        self.popCenterConstraint.constant = (self.view.frame.height + self.ratePopupView.frame.height) / 2
        self.view.layoutIfNeeded()

        refreshUI()
        showRatePopup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.commentTextView.resignFirstResponder()
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didPressRateButton(_ button: UIButton)
    {
        let displayText = [
            _LS("Not interesting"),
            _LS("So so"),
            _LS("Not bad"),
            _LS("Strongly recommanded"),
            _LS("Excellent")
        ]
        rate = button.tag + 1
        self.rateButtons.enumerated().forEach
        { (index, btn) in
            btn.isSelected = index <= button.tag
        }
        self.submitButton.isEnabled = true
        self.closeButton.isEnabled = true
        if button.tag < displayText.count
        {
            self.rateTextLabel.text = displayText[button.tag]
        }
        
    }
    
    @IBAction func submit()
    {
        if self.lessonId != nil
        {
            self.rateLesson()
        }
        else if (self.liveShowModel != nil)
        {
            self.rateLiveShow()
        }
    }
    
    func rateLiveShow()
    {
        guard rate > 0,
        let roomId = self.liveShowModel?.roomId
            
            else
        {
            self.finishBlock?(false)
            self.closeRatePopup()
            return
        }
        let callback = HttpCallBack()
        callback.doneBlock =
            {[weak self] ret in
                guard let result = ret as? [String: Any] else
                {
                    self?.completeBlock?(.failed(msg: _LS("ALERT_UNKNOW_ERROR")))
                    self?.finishBlock?(false)
                    self?.closeRatePopup()
                    return
                }
                
                if  let success = result["success"] as? Bool,
                    success
                {
                    let info = (result["results"] as? [String: Any])?["list"] as? [String: Any]
                    let score = info?["evaluatescore"] as? Float
                    let numberOfPeople = info?["evaluatenum"] as? Int
                    
                    self?.completeBlock?(.success(score: score, numberOfRatedPeople: numberOfPeople))
                    self?.finishBlock?(true)
                }
                else if let errMsg = result["message"] as? String
                {
                    self?.completeBlock?(.failed(msg: errMsg))
                    self?.finishBlock?(false)
                }
                else
                {
                    self?.completeBlock?(.failed(msg: _LS("ALERT_UNKNOW_ERROR")))
                    self?.finishBlock?(false)
                }
                self?.closeRatePopup()
        }
        callback.errorBlock =
            {[weak self] err in
                self?.completeBlock?(.failed(msg: _LS("ALERT_NETWORK_ERROR")))
                self?.finishBlock?(false)
                self?.closeRatePopup()
        }
        
        HTTPFacade.rateLiveShow(roomId, rating: rate, content: commentString, callback: callback)
    }
    
    func rateLesson()
    {
        guard rate > 0,
        let lessonId = self.lessonId
            else
        {
            self.completeBlock?(.failed(msg: _LS("ALERT_UNKNOW_ERROR")))
            self.finishBlock?(false)
            self.closeRatePopup()
            return
        }
        let callback = HttpCallBack()
        callback.doneBlock =
            {[weak self] ret in
                guard let result = ret as? [String: Any] else
                {
                    self?.completeBlock?(.failed(msg: _LS("ALERT_UNKNOW_ERROR")))
                    self?.finishBlock?(false)
                    self?.closeRatePopup()
                    return
                }
                
                if  let success = result["success"] as? Bool,
                    success
                {
                    let info = (result["results"] as? [String: Any])?["list"] as? [String: Any]
                    let score = info?["evaluatescore"] as? Float
                    let numberOfPeople = info?["evaluatenum"] as? Int
                    
                    self?.completeBlock?(.success(score: score, numberOfRatedPeople: numberOfPeople))
                    self?.finishBlock?(true)
                }
                else if let errMsg = result["message"] as? String
                {
                    self?.completeBlock?(.failed(msg: errMsg))
                    self?.finishBlock?(false)
                }
                else
                {
                    self?.completeBlock?(.failed(msg: _LS("ALERT_UNKNOW_ERROR")))
                    self?.finishBlock?(false)
                }
                self?.closeRatePopup()
        }
        callback.errorBlock =
            {[weak self] err in
                self?.completeBlock?(.failed(msg: _LS("ALERT_NETWORK_ERROR")))
                self?.finishBlock?(false)
                self?.closeRatePopup()
        }
        
        HTTPFacade.rateLesson(lessonId.intValue, rating: rate, content: commentString, callback: callback)
    }

    func refreshUI()
    {
        if let lm = self.lesson
        {
            if let lessonId = lm.lessonId,
                lessonId.intValue != 0
            {
                self.lessonId = lessonId
            }
            else
            {
                self.lessonId = lm.id
            }
        }
        else if let _ = self.liveShowModel
        {
            closeButton.isEnabled = true
        }
        
        if let anchor = self.anchorProfile
        {
            self.displayNameLabel.text = anchor.userName
            self.avatarImageView.sd_setImage(with: URL(string: anchor.userFaceURL), placeholderImage: #imageLiteral(resourceName: "headshot"))
        }
        else if let img = self.coverImage
        {
            self.avatarImageView.image = img
        }
        else if let imgUrl = self.coverImageURL
        {
            self.avatarImageView.sd_setImage(with: imgUrl)
        }
    }
    
    func showRatePopup()
    {
        UIView.animate(withDuration: 0.4, animations: {
            self.popCenterConstraint.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func closeRatePopup()
    {
        UIView.animate(withDuration: 0.4, animations: {
            self.popCenterConstraint.constant = (self.view.frame.height + self.ratePopupView.frame.height) / 2
            self.view.layoutIfNeeded()
        }, completion: {[weak self] _ in
            self?.dismiss(animated: false, completion: nil)
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - UITextViewDelegate
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if commentString.isEmpty
        {
            textView.text = ""
            textView.textColor = UIColor.titleColorInLightNavi()
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if commentString.isEmpty
        {
            textView.text = CommentPlaceHolderString
            textView.textColor = self.commentNumberLabel.textColor
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        let nsString = textView.text as NSString
        let content = nsString.replacingCharacters(in: range, with: text)
        let length = content.characters.count
        
//        commentString = content
//        let length = commentString.lengthOfBytes(using: .utf8)
//        self.commentNumberLabel.text =  String(format: _LS("COURSE_COMMENT_NUMBER"), length)
        return length <= MaxCommentTextLength
    }
    
    public func textViewDidChange(_ textView: UITextView)
    {
        let newLength = textView.text.characters.count
        // 中文输入法竟然没有回掉shouldChangeTextIn，只能用这种方法绕过
        if newLength <= MaxCommentTextLength
        {
            commentString = textView.text
        }
        else
        {
            textView.text = commentString
        }
        let length = commentString.characters.count
        self.commentNumberLabel.text =  String(format: _LS("COURSE_COMMENT_NUMBER"), length)
    }
}
