//
//  VLSMessageAlertViewController.swift
//  VLiveShow
//
//  Created by rock on 2016/12/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSMessageAlertViewControllerDelegate {
    func eventHandle()
}

class VLSMessageAlertViewController: UIViewController {

    var avatorUrl: String?
    var nickname: String?
    var content: String?
    var anchorId: String?
    fileprivate var avatorImageView: UIImageView!
    fileprivate var nicknameLab: UILabel!
    fileprivate var contentLab: UILabel!
    fileprivate var sexImageView: UIImageView!
    weak var delegate: VLSMessageAlertViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initView()
        self.anchorInfo() //获取主播信息
        self.view.backgroundColor = UIColor.clear
        
        // Do any additional setup after loading the view.
    }
    
    func initView() {
        let backView = UIView().then {
            $0.backgroundColor = UIColor.clear
        }
        self.view.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.center.equalTo(self.view.snp.center)
            make.left.equalTo(40)
            make.right.equalTo(-40)
        }
        //白底视图
        let whiteView = UIView().then {
            $0.backgroundColor = HEX(COLOR_FONT_FFFFFF)
            $0.layer.cornerRadius = 4
            $0.layer.masksToBounds = true
        }
        backView.addSubview(whiteView)
        whiteView.snp.makeConstraints { (make) in
            make.top.equalTo(backView.snp.top).offset(#imageLiteral(resourceName: "card-close").size.height)
            make.left.bottom.right.equalTo(backView)
        }
        //头像
        avatorImageView = UIImageView().then {
            $0.layer.cornerRadius = 45
            $0.clipsToBounds = true
            $0.sd_setImage(with: URL(string: StringSafty(avatorUrl)), placeholderImage: #imageLiteral(resourceName: "headshot"))
        }
        backView.addSubview(avatorImageView)
        avatorImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView)
            make.centerY.equalTo(whiteView.snp.top)
            make.size.equalTo(CGSize(width: 90, height: 90))
        }
        //删除按钮
        let deleteBt = UIButton().then {
            $0.setImage(#imageLiteral(resourceName: "card-close"), for: .normal)
            $0.addTarget(self, action: #selector(VLSMessageAlertViewController.out), for: .touchUpInside)
        }
        backView.addSubview(deleteBt)
        deleteBt.snp.makeConstraints { (make) in
            make.right.equalTo(whiteView).offset(-10)
            make.top.equalTo(backView.snp.top)
            make.bottom.equalTo(whiteView.snp.top)
        }
        //昵称
        nicknameLab = UILabel().then {
            $0.text = nickname
            $0.textColor = HEX(COLOR_FONT_4A4A4A)
            $0.textAlignment = .right
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.sizeToFit()
        }
        whiteView.addSubview(nicknameLab)
        nicknameLab.snp.makeConstraints { (make) in
            make.top.equalTo(avatorImageView.snp.bottom).offset(15)
            make.height.equalTo(30)
            make.centerX.equalTo(whiteView.snp.centerX)
        }
        //性别
        sexImageView = UIImageView()
        whiteView.addSubview(sexImageView)
        sexImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(nicknameLab.snp.centerY)
            make.left.equalTo(nicknameLab.snp.right).offset(6)
        }
        //状态（正在直播）
        let statusNameLab = UILabel().then {
            $0.text = LocalizedString("INVITE_MESSAGE_ALERT_ONLINE")
            $0.textColor = HEX(COLOR_FONT_FF1130)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.textAlignment = .center
            $0.sizeToFit()
        }
        whiteView.addSubview(statusNameLab)
        statusNameLab.snp.makeConstraints { (make) in
            make.top.equalTo(nicknameLab.snp.bottom).offset(5)
            make.left.equalTo(20)
            make.right.equalTo(-20)
        }
        //内容
        contentLab = UILabel().then {
            $0.text = content
            $0.textColor = HEX(COLOR_FONT_4A4A4A)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
            $0.textAlignment = .center
            $0.sizeToFit()
        }
        whiteView.addSubview(contentLab)
        contentLab.snp.makeConstraints { (make) in
            make.top.equalTo(statusNameLab.snp.bottom).offset(6)
            make.left.right.equalTo(statusNameLab)
        }
        //简介
        let remarkLab = UILabel().then {
            $0.text = LocalizedString("INVITE_MESSAGE_ALERT_REMARK")
            $0.textColor = HEX(COLOR_FONT_777777)
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.textAlignment = .center
            $0.sizeToFit()
        }
        whiteView.addSubview(remarkLab)
        remarkLab.snp.makeConstraints { (make) in
            make.top.equalTo(contentLab.snp.bottom).offset(6)
            make.left.right.equalTo(statusNameLab)
        }
        //操作（立即上镜）
        let onlineBt = UIButton().then {
            $0.backgroundColor = HEX(COLOR_FONT_FF1130)
            $0.setTitle(LocalizedString("INVITE_MESSAGE_ALERT_ONLIVE"), for: .normal)
            $0.setTitleColor(HEX(COLOR_FONT_FFFFFF), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
            $0.layer.cornerRadius = 19
            $0.clipsToBounds = true
            $0.addTarget(self, action: #selector(VLSMessageAlertViewController.eventHandle), for: .touchUpInside)
        }
        whiteView.addSubview(onlineBt)
        onlineBt.snp.makeConstraints { (make) in
            make.top.equalTo(remarkLab.snp.bottom).offset(22)
            make.left.equalTo(12)
            make.right.equalTo(-12)
            make.height.equalTo(38)
            make.bottom.equalTo(whiteView).offset(-13)
        }
    }
    
    //跳转事件处理
    func eventHandle() {
        if AccountManager.shared().account.mobilePhone != "" && AccountManager.shared().account.mobilePhone != nil {//邀请上线必须绑定手机号码
            self.dismiss()
            delegate?.eventHandle()
        } else {
            let acc = TiePhoneNumberController()
            let accNav = VLSNavigationController(rootViewController: acc)
            self.present(accNav, animated: true, completion: nil)
        }
    }
    
    //隐藏提示框
    func out() {
        self.dismiss()
    }
    
    func dismiss() {
        self.view.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
    }
    
    //通过id获取用户信息
    func anchorInfo() {
        let callback = HttpCallBack()
        callback.doneBlock =
            { [weak self] ret in
                guard let result = ret as? [String: Any] else {
                    return
                }
                if  let _ = result["success"] as? Bool {
                    let profile = ObjectForKeySafety(NSDictionarySafty(result["results"]), key: "profile")
                    let gender = StringSafty(ObjectForKeySafety(NSDictionarySafty(profile), key: "gender"))
                    if gender == "MALE" {
                        self?.sexImageView.image = #imageLiteral(resourceName: "sex_male")
                    } else if gender == "FEMALE" {
                        self?.sexImageView.image = #imageLiteral(resourceName: "sex_female")
                    }
                }
        }
        callback.errorBlock = { err in
        }
        HTTPFacade.get(APIConfiguration.shared().getUserInfoUrl, parameter: ["userId": StringSafty(anchorId)], callback: callback)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
