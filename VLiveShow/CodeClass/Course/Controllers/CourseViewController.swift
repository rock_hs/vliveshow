//
//  CourseViewController.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit

class CourseViewController: VLSTableViewController {

    var pageName: String? = "publish"
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView上移8px
        self.tableView.snp.updateConstraints { (make) in
            make.top.equalTo(-8)
        }
        self.tableView.register(OnCourseCell.self, forCellReuseIdentifier: "onCell")
        self.tableView.register(FinishedCourseCell.self, forCellReuseIdentifier: "finishedCell")
        self.viewModel.itemClass = PublishCourseModel.self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeTimer()
    }
    
    override func reloadData() {
        self.removeTimer()//重新加载数据前，先销毁定时器，重新开始计算总的倒计时数
        super.reloadData()
    }
    
    //销毁定时器
    func removeTimer() {
        for i in 0 ..< self.tableView.visibleCells.count {
            if self.tableView.visibleCells[i].isKind(of: CourseCell.self), let cell = self.tableView.visibleCells[i] as? CourseCell {
                cell.timer?.cancel() //销毁定时器
                cell.timer = nil
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CourseViewController: CourseCellDelegate {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) {
            if let newItem = item as? PublishCourseModel {
                if newItem.customCourseStatus == CourseStatus.EndOfLaunch.rawValue || newItem.customCourseStatus == CourseStatus.OffTheLaunch.rawValue {//已结束和已下架
                    cell = tableView.dequeueReusableCell(withIdentifier: "finishedCell")
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "onCell")
                }
                if let cell = cell as? CourseCell {
                    cell.indexPath = indexPath
                    newItem.indexPath = indexPath
                    cell.pageName = pageName
                    cell.setNewItem(newItem)
                    cell.vcClass = CourseListViewController.self
                    cell.properties = ["course_id": newItem.courseId.stringValue,
                                       "pageName": StringSafty(pageName),
                                       "teacherId": newItem.teacherId.stringValue,
                                       "teacherName": newItem.teacherName,
                                       "picTeacher": newItem.picTeacher
                                        ]
                    cell.navigation = self.navigationController
                    cell.delegate = self
                }
            }
        }
        return cell!
    }
    
    //开始直播或进入直播
    func jumpToRoomAction(_ lesson: AnyObject?, type: NSInteger) {
        self.hideProgressHUD(afterDelay: 1.5)
        switch type {
        case CourseType.Play.rawValue:
            if lesson is PublishCourseModel {
                if let item = lesson as? PublishCourseModel {
                    if let roomId = item.roomId {//主播已开播
                        let model = VLSLiveListModel()
                        model.host = item.teacherId.stringValue
                        model.courseId = item.courseId.stringValue
                        model.lessonId = item.nextLessonId.stringValue
                        model.roomId = roomId
                        model.title = item.courseName
                        self.room(model)
                    } else {//主播未开播
                        self.hideProgressHUD(afterDelay: 0)
                        self.showHUD(LocalizedString("COURSE_STATUS_ON_TEXT"))
                    }
                }
            }
        case CourseType.Enter.rawValue:
            if lesson is PublishCourseModel {
                if let item = lesson as? PublishCourseModel {
                    if let roomId = item.roomId { //如果已创建过直播间，直接进入
                        let model = VLSLiveListModel()
                        model.host = item.teacherId.stringValue
                        model.courseId = item.courseId.stringValue
                        model.lessonId = item.nextLessonId.stringValue
                        model.roomId = roomId
                        model.title = item.courseName
                        self.intoAnchorVC(model)
                    } else {//否则创建直播间
                        let liveViewController = LiveBeforeTestViewController()
                        liveViewController.liveBeforeTitle = item.courseName
                        liveViewController.lessonId = item.nextLessonId
                        liveViewController.topicStr = item.courseType.stringValue + "," + StringSafty(item.courseTypeName as AnyObject?)
                        if let url = URL(string: StringSafty(item.picCover as AnyObject?)) {
                            do {
                                let image = try UIImage(data: NSData(contentsOf: url) as Data)
                                liveViewController.infoView.coverImage = image
                            } catch {
                            }
                        }
                        liveViewController.getUserRoomID(self.navigationController)
                    }
                }
            }
        default:
            return
        }
    }
    
    //获取roomId
    func roomIdAction(_ courseId: String, indexPath: IndexPath) {
        if let viewModel = self.viewModel as? CourseViewModel {
            viewModel.get(api: APIConfiguration.shared().courseRoomIdUrl, dic: ["courseId": courseId], complete: { (result) in
                if case .failed(let msg) = result {
                    self.showProgressHUD(msg)
                }
                else if case .success( _, let dic) = result {
                    let roomId = ObjectForKeySafety(NSDictionarySafty(dic), key: "roomInfo")
                    if let courseModel = ObjectForKeySafety(self.viewModel.dataSource, index: indexPath.row) as? PublishCourseModel {
                        if let newRoomId = roomId {
                            courseModel.roomId = StringSafty(newRoomId)
                            self.viewModel.dataSource.replaceObject(at: indexPath.row, with: courseModel)
                            self.tableView.reloadRows(at: [indexPath], with: .automatic)
                        }
                    }
                }
            })
        }
    }
}
