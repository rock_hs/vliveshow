//
//  VLSFeedbackViewController.swift
//  VLiveShow
//
//  Created by rock on 2017/2/22.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSFeedbackViewController: VLSBaseViewController {

    var textView: VLSTextView!
    var submitBt: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = LocalizedString("VLS_FEEDBACK_TITLE")
        self.view.backgroundColor = HEX(COLOR_BG_F8F8F8)
        
        textView = VLSTextView().then {
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
            $0.delegate = self
            $0.showsVerticalScrollIndicator = false
            $0.backgroundColor = HEX(COLOR_BG_FFFFFF)
            $0.placeholder = LocalizedString("VLS_FEEDBACK_CONTENT_PLACEHOLDER")
        }
        self.view.addSubview(textView)
        textView.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.right.equalTo(self.view)
            make.height.equalTo(160)
        }
        
        submitBt = UIButton().then {
            $0.setTitle(LocalizedString("VLS_FEEDBACK_SUBMIT"), for: .normal)
            $0.setTitleColor(VLSSepartorColor, for: .normal)
            $0.addTarget(self, action: #selector(VLSFeedbackViewController.submit), for: .touchUpInside)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
            $0.layer.cornerRadius = 18
            $0.backgroundColor = UIColor.clear
            $0.layer.borderColor = VLSSepartorColor.cgColor
            $0.layer.borderWidth = 1
            $0.isEnabled = false
        }
        self.view.addSubview(submitBt)
        submitBt.snp.makeConstraints { (make) in
            make.top.equalTo(textView.snp.bottom).offset(30)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(36)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func submit() {
        textView.resignFirstResponder()
        let callback = HttpCallBack()
        callback.doneBlock =
            {[weak self] ret in
                var message = LocalizedString("SUCCESS")
                guard let result = ret as? [String: Any] else {
                    self?.showHUD(LocalizedString("VLS_FEEDBACK_ERROR"))
                    return
                }
                if  let _ = result["success"] as? Bool, let error_code = result["error_code"] as? Int {
                    message = ErrorEvent.error(error_code, msg: StringSafty(result["message"]))
                    if error_code == 0 {
                        self?.textView.text = ""
                        self?.textView.placeholderLab?.isHidden = false
                        self?.submitBt.setTitleColor(VLSSepartorColor, for: .normal)
                        self?.submitBt.backgroundColor = UIColor.clear
                        self?.submitBt.layer.borderColor = VLSSepartorColor.cgColor
                        self?.submitBt.layer.borderWidth = 1
                        self?.submitBt.isEnabled = false
                    }
                }
                self?.showHUD(message)
        }
        callback.errorBlock = {[weak self] err in
            self?.showHUD(LocalizedString("VLS_FEEDBACK_ERROR"))
        }
        let url = APIConfiguration.shared().feedbackUrl + "?token=\(StringSafty(AccountManager.shared().account.token))"
        HTTPFacade.post(url, parameter: ["content": textView.text], requestSerializer: "json", callback: callback)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension VLSFeedbackViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text, text.characters.count > 0 {
            if text.characters.count >= 100 {
                textView.text?.remove(at: StringSafty(textView.text).index(before: StringSafty(textView.text).endIndex))
            }
            if !submitBt.isEnabled {
                submitBt.setTitleColor(HEX(COLOR_FONT_FFFFFF), for: .normal)
                submitBt.backgroundColor = HEX(COLOR_BG_FF1130)
                submitBt.layer.borderWidth = 0
                submitBt.isEnabled = true
            }
        } else {
            if submitBt.isEnabled {
                submitBt.setTitleColor(VLSSepartorColor, for: .normal)
                submitBt.backgroundColor = UIColor.clear
                submitBt.layer.borderColor = VLSSepartorColor.cgColor
                submitBt.layer.borderWidth = 1
                submitBt.isEnabled = false
            }
        }
    }
}
