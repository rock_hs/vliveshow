//
//  VLSCollectionViewController.swift
//  VLiveShow
//
//  Created by rock on 2017/1/3.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit
import MJRefresh

class VLSCollectionViewController: VLSBaseViewController {

    var collectionView: UICollectionView!
    var viewModel: VLSTableViewModel!
    var viewModelClass: AnyClass? {
        didSet {
            self.initViewModel()
            self.setViewModelLoadEvent()
        }
    }
    fileprivate var errorView: UIView?
    fileprivate var isremoveMJ_HeaderFlag: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = HEX(COLOR_BG_EEEEEE)
        self.initViewModel()
        self.setViewModelLoadEvent()
        let flowLayout = UICollectionViewFlowLayout().then {
            $0.scrollDirection = .horizontal
            $0.itemSize = CGSize(width: self.view.bounds.width, height: self.view.bounds.height - 76)
        }
        collectionView = UICollectionView(frame:self.view.bounds, collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = HEX(COLOR_BG_EEEEEE)
        self.view.addSubview(collectionView)
        // Do any additional setup after loading the view.
    }
    
    //重新加载数据
    func reloadData(_ key:String) {
        viewModel.page = 0
        viewModel.pageDirection = 1
        viewModel.loadDataSource(key)
    }
    
    //加载更多
    func loadMoreData() {
        viewModel.pageDirection = 1  //加载更多
        viewModel.loadDataSource()
    }

    //初始化viewModel
    func initViewModel() {
        if let viewModelClass = viewModelClass, let type = viewModelClass as? VLSTableViewModel.Type {
            viewModel = type.init()
        } else {
            viewModel = VLSTableViewModel()
        }
    }
    
    fileprivate func setViewModelLoadEvent() {//设置加载完成时间
        viewModel.loadSuccess = {[weak self] in
            self?.setViewModelLoadSuccess()
        }
        viewModel.loadEmpty = {[weak self] in
            self?.setViewModelLoadEmpty()
        }
        viewModel.loadFailed = {[weak self] in
            self?.setViewModelLoadFailed()
        }
        viewModel.loadMoreData = {
            [weak self](moreData,showFootor) in
            self?.setViewModelLoadMore(moreData, showFootor: showFootor)
        }
    }
    
    func setViewModelLoadSuccess() {//加载成功
        self.removeErrorView()
        collectionView.reloadData()
        if isremoveMJ_HeaderFlag == false, let mj_header = collectionView.mj_header {
            mj_header.endRefreshing()
        }
    }
    
    func setViewModelLoadEmpty() {//无数据
        self.addErrorView()
        collectionView.reloadData()
        if isremoveMJ_HeaderFlag == false, let mj_header = collectionView.mj_header {
            mj_header.endRefreshing()
        }
        if let mj_footer = collectionView.mj_footer {
            mj_footer.endRefreshing()
        }
        collectionView.mj_footer = nil
    }
    
    func setViewModelLoadFailed() {//加载失败
        self.addErrorView()
        collectionView.reloadData()
        if isremoveMJ_HeaderFlag == false, let mj_header = collectionView.mj_header {
            mj_header.endRefreshing()
        }
        if let mj_footer = collectionView.mj_footer {
            mj_footer.endRefreshing()
        }
        collectionView.mj_footer = nil
    }
    
    func setViewModelLoadMore(_ moreData:Bool,showFootor:Bool) {//加载更多
        self.setViewModelLoadSuccess()
        //如果请求数据需要分页，则显示上拉刷新
        if self.viewModel.dataSource.count > 0, let _ = collectionView.mj_footer {
            collectionView.mj_footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(VLSTableViewController.loadMoreData))
        }
        //是否提示“没有更多数据”
        if let mj_footer = collectionView.mj_footer {
            if moreData == true {
                mj_footer.resetNoMoreData()
            } else {
                mj_footer.endRefreshingWithNoMoreData()
            }
        }
    }
    
    func addErrorView() {
        self.removeErrorView()
        let errorView = VLSEmptyView(frame: self.view.bounds).then {
            $0.delegate = self
        }
        self.addErrorView(errorView)
    }
    
    //添加错误视图
    func addErrorView(_ errorView: UIView?) {
        if self.errorView == nil {
            if let errorView = errorView {
                self.errorView = errorView
                self.collectionView.addSubview(errorView)
            }
        }
    }
    
    func removeErrorView() {
        errorView?.removeFromSuperview()
        errorView = nil
    }
    
    //进入直播
    func room(_ model: VLSLiveListModel) {
        let show = VLSShowViewController()
        show.anchorId = model.host
        show.roomId = model.roomId
        show.orientation = KLiveRoomOrientation_Portrait
        show.courseId = model.courseId
        show.lessonId = model.lessonId
        let joinRoom = VLSJoinRoomModel()
        joinRoom.roomID = model.roomId
        joinRoom.joinType = "living_list"
        show.roomTrackContext = VLSUserTrackingManager.share().trackingJoinroom(joinRoom)
        show.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(show, animated: true)
    }
    
    //主播重连
    func intoAnchorVC(_ model: VLSLiveListModel) {
        let anchorVC = VLSAnchorViewController()
        VLSIMManager.sharedInstance().roomID = model.roomId
        //        VLSMessageManager.shared().lesssonId = model.lessonId
        if model.lessonId.characters.count > 0 {
            anchorVC.actionType = ActionTypeLessonTeacher
        }
        anchorVC.roomId = model.roomId
        anchorVC.anchorId = model.host
        anchorVC.lessonId = model.lessonId
        anchorVC.courseId = model.courseId
        anchorVC.isReconnection = true
        anchorVC.title = model.title
        let nav = UINavigationController(rootViewController: anchorVC)
        self.present(nav, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.setStatusBarStyle(.default, animated: false)
        self.navigationController?.navigationBar.useLightTheme()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension VLSCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, VLSEmptyViewDelegate {
    //pragma mark -- UICollectionViewDataSource
    //定义展示的UICollectionViewCell个数
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)
        return cell
    }
    
    //pragma mark --UICollectionViewDelegateFlowLayout
    //定义每个UICollectionView 的 margin
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    //cell的最小行间距
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //cell的最小列间距
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func tryAction() {
        
    }
}
