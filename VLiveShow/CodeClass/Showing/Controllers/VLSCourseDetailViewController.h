//
//  VLSCourseDetailViewController.h
//  VLiveShow
//
//  Created by tom.zhu on 2016/11/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
@class VLSCourseModel;
@class VLSLiveListModel;

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSInteger, CourseState) {
    course_before   = 1UL << 0,     //课程未开始
    course_order    = 1UL << 1,     //课程未预约
    course_orderd   = 1UL << 2,     //课程已预约
    course_after    = 1UL << 3,     //课程已开始
};

@interface VLSCourseDetailViewController : VLSBaseViewController
@property (nonatomic, strong) VLSCourseModel *model;
@property (nonatomic, assign) CourseState state;        //⚠️⚠️⚠️因为接口没有支持课程是否开始，所以已经开课的状态必须传course_after！
@property (nonatomic, strong) VLSLiveListModel *liveModel;
@end
NS_ASSUME_NONNULL_END
