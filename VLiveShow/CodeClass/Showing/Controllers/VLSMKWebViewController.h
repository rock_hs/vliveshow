//
//  VLSMKWebViewController.h
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import <WebKit/WebKit.h>

@interface VLSMKWebViewController : VLSBaseViewController<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler>

@property (nonatomic, strong) WKWebView *WKWebView;

@property (nonatomic, copy)  NSString *Url;

@property(nonatomic, assign) BOOL fullScreenMode;

@end
