//
//  VLSShowVTeamViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSShowVTeamViewController.h"
#import "VLSTeamMessageModel.h"
#import "VTeamTableViewCell.h"
#import "VLSLittleNavigationController.h"
#import "VLSShowViewController.h"
#import "VLSAdminViewController.h"
#import "VLSHeartManager.h"
@interface VLSShowVTeamViewController ()

@end

@implementation VLSShowVTeamViewController

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage ImageFromColor:[UIColor whiteColor]] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.title = LocalizedString(@"VTEAM");
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];

    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, (275-44));
}
- (UIViewController *)getCurrentVC{
    
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    //app默认windowLevel是UIWindowLevelNormal，如果不是，找到UIWindowLevelNormal的
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    //    如果是present上来的appRootVC.presentedViewController 不为nil
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else{
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        //        UINavigationController * nav = tabbar.selectedViewController ; 上下两种写法都行
        result=nav.childViewControllers.lastObject;
        
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
    }
    
    return result;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIViewController *vc = [self getCurrentVC];
    if ([vc isKindOfClass:[VLSShowViewController class]]) {
//        VLSShowViewController *s = (VLSShowViewController *)vc;
//        [s.contentView.msgBoard removeFromSuperview];
//        s.contentView.msgBoard = nil;
    }
    VLSTeamMessageModel *modelR = [self.vTeamArray objectAtIndex:indexPath.row];
    if (modelR.roomID == nil) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }else{
    
        VLSMessageManager *manager = [VLSMessageManager sharedManager];
        if ([manager checkUserIsLiving:manager.host.userId]) {
        }else{
            
            VLSLittleNavigationController *little = (VLSLittleNavigationController*)self.navigationController;
            
            VLSShowViewController *show = (VLSShowViewController *)little.fatherVC;
            show.isFromMailInvite = YES;
            [show.contentView addSubview:show.contentView.msgBoard];
            
            
            NSString *language = [[NSLocale preferredLanguages] firstObject];
            NSArray *languageArray = [VLSHeartManager shareHeartManager].heartModel.liveAnnouncement;
            if ([language containsString:@"Hant"] || [language containsString:@"HK"] || [language containsString:@"TW"]) {
                VLSMessageViewModel *model = [[VLSMessageViewModel alloc]initWidthUser:nil message:languageArray[1][@"tw"] types:10016];
                [show.contentView.msgBoard addDataSource:@[model]];
            } else if([language containsString:@"EN"]){
                VLSMessageViewModel *model = [[VLSMessageViewModel alloc]initWidthUser:nil message:languageArray[2][@"en"] types:10016];
                [show.contentView.msgBoard addDataSource:@[model]];
            }else{
                VLSMessageViewModel *model = [[VLSMessageViewModel alloc]initWidthUser:nil message:languageArray[0][@"cn"] types:10016];
                [show.contentView.msgBoard addDataSource:@[model]];
            }
            [show reloadLiveWidthRoomId:modelR.roomID anchorId:modelR.anchorID];
            
            VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
            joinRoom.roomID = modelR.roomID;
            joinRoom.joinType = @"notice";
            show.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];

            [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
                
                self.navigationController.view.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 275 );
                
            } completion:^(BOOL finished) {
                
                [self.navigationController.view removeFromSuperview];
                
            }];
        }
    }
}

@end
