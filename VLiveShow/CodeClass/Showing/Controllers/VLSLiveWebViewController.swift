//
//  VLSLiveWebViewController.swift
//  VLiveShow
//
//  Created by VincentX on 07/12/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit

class VLSLiveWebViewController: VLSMKWebViewController, UIScrollViewDelegate {
    
    var lastOffset: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.wkWebView.scrollView.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        recoverNaviBarStatus()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y;
        if (self.pagerViewController != nil
            && self.pagerViewController!.selectedViewController != self
            )
        {
            self.lastOffset = offsetY;
            return;
        }
        
        let isUpwards = (offsetY - self.lastOffset) > 0;
        if (isUpwards
            && offsetY > 64
            )
        {
            self.parent?.navigationController?.setNavigationBarHidden(true, animated: false)
            self.pagerViewController?.view.layoutIfNeeded()
//            UIView.animate(withDuration: 0.3, animations: {
                if let constarints = self.pagerViewController?.viewPagerConstraints,
                    constarints.count > 0
                {
                    constarints[0].constant = UIApplication.shared.statusBarFrame.size.height
                }
                self.pagerViewController?.view.layoutIfNeeded()
//            })
            UIApplication.shared.statusBarStyle = .default
        }
        else if (!isUpwards && scrollView.isDragging
            && (offsetY < scrollView.contentSize.height - scrollView.frame.height)) || (scrollView.contentSize.height + 64 < scrollView.frame.height)
        {
            self.parent?.navigationController?.setNavigationBarHidden(false, animated: false)
            self.pagerViewController?.view.layoutIfNeeded()
//            UIView.animate(withDuration: 0.3, animations: {
                if let constarints = self.pagerViewController?.viewPagerConstraints,
                    constarints.count > 0
                {
                    constarints[0].constant = 0
                }
                self.pagerViewController?.view.layoutIfNeeded()
//            })
            UIApplication.shared.statusBarStyle = .lightContent
        }
        
        
        self.lastOffset = offsetY;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func recoverNaviBarStatus()
    {
        guard let pagerVC = self.pagerViewController,
            let constraints = pagerVC.viewPagerConstraints,
            constraints.count > 0,
            pagerVC.selectedViewController == self
        else
        {
            return
        }
        let showNavi = constraints[0].constant == 0
        self.navigationController?.isNavigationBarHidden = !showNavi
    }

}
