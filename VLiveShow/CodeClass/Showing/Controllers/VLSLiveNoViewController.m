//
//  VLSLiveNoticeViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveNoViewController.h"
#import "VLSVLiveShowTableViewCell.h"
#import "VLSTeamMessageModel.h"
#import "VLSLiveRemindModel.h"
#import "VLSFansModel.h"
#import "VLSVLiveViewModel.h"
#import "LKDBHelper.h"
#import "VLSDBHelper.h"
#import "VLSLRViewController.h"
#import "VLSShowFansViewController.h"
#import "VLSShowVTeamViewController.h"
@interface VLSLiveNoViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *messageArray;

@end

@implementation VLSLiveNoViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self dispathgroupOperation];
    [self.navigationController.navigationBar useLightTheme];
    self.tableView.frame = self.view.bounds;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = LocalizedString(@"NOTICE");
    [self loadViews];
    NSLog(@"pddd%f",self.parentViewController.view.frame.size.height);
    NSLog(@"pss%f",self.view.frame.size.height);
    
    self.hidesBottomBarWhenPushed = YES;
    self.view.clipsToBounds = NO;
    
    self.view.backgroundColor = [UIColor redColor];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_close_msg"] style:UIBarButtonItemStylePlain target:self action:@selector(rightItemAction:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    [self configUI];
}

- (void)configUI {
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        self.navigationController.navigationBar.tintColor = RGB16(COLOR_FONT_777777);
        
        self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize: 17],
                                                                        NSForegroundColorAttributeName : RGB16(COLOR_FONT_777777)};
        
        CGRect rect = self.view.frame;
        rect.size.width = K_LANDSCAPEVIEWWIDTH;
        self.view.frame = rect;
    }else {
        [self.navigationController.view setFrame:CGRectMake(0, SCREEN_HEIGHT,
                                                            SCREEN_WIDTH, 275)];
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            [self.navigationController.view setFrame:CGRectMake(0, SCREEN_HEIGHT-275,SCREEN_WIDTH, 275)];
        } completion:nil];
    }
}

- (void)dispathgroupOperation
{
    [[VLSDBHelper defaultHelper] getAllMessageArrFromDB:[self reloadWhenComplete]];
}

- (complete)reloadWhenComplete
{
    complete com = ^(NSMutableArray *arr){
        self.messageArray = arr;
        [self.tableView reloadData];
    };
    return com;
}

#pragma mark - 加载视图

- (void)loadViews{
    
    [self.view addSubview:self.tableView];
    
    VLSVLiveViewModel *model1 = [[VLSVLiveViewModel alloc] init];
    model1.title = LocalizedString(@"VTEAM");
    model1.imageName = @"ic_official";
    
    VLSVLiveViewModel *model2 = [[VLSVLiveViewModel alloc] init];
    model2.title = LocalizedString(@"BEGIN_LIVE_REMIND");
    model2.imageName = @"ic_kaibotixing";
    
    VLSVLiveViewModel *model3= [[VLSVLiveViewModel alloc] init];
    model3.title = LocalizedString(@"FANS");
    model3.imageName = @"ic_fans";
    [self.messageArray addObject:model1];
    [self.messageArray addObject:model2];
    [self.messageArray addObject:model3];
    
    [self.tableView reloadData];
}

#pragma mark - UITableviewDelegate 和 UITableviewDataSource 代理方法

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.messageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VLSVLiveShowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    VLSVLiveViewModel *model = [self.messageArray objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        VLSShowVTeamViewController *teamVC = [[VLSShowVTeamViewController alloc] init];
        teamVC.orientation = self.orientation;
        teamVC.view.frame = self.view.bounds;
        [self.navigationController pushViewController:teamVC animated:YES];
        [VLSDBHelper UpdateTeamDBandReload:[self reloadWhenComplete]];
        
    }else if (indexPath.row == 1){
        
        VLSLRViewController *noticeVC = [[VLSLRViewController alloc] init];
        noticeVC.view.frame = self.view.bounds;
        noticeVC.orientation = self.orientation;
        [self.navigationController pushViewController:noticeVC animated:YES];
        [VLSDBHelper UpdateLiveDBandReload:[self reloadWhenComplete]];
    }else{
        
        VLSShowFansViewController *fansVC = [[VLSShowFansViewController alloc] init];
        fansVC.view.frame = self.view.bounds;
        [self.navigationController pushViewController:fansVC animated:YES];
        [VLSDBHelper UpdateFansDBandReload:[self reloadWhenComplete]];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 50 ;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        UIView *view = [[UIView alloc] initWithFrame:tableView.tableFooterView.frame];
        view.backgroundColor = [UIColor whiteColor];
        return view;
    }
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = tableView.tableFooterView.frame;
    [button setTitle:LocalizedString(@"CANCLE") forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnFanClick) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (void)btnFanClick{
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        
        self.parentViewController.view.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 275);
    } completion:^(BOOL finished) {
        
        [self.parentViewController.view removeFromSuperview];
    }];
    
}
#pragma mark - 初始化控件

-(UITableView *)tableView{
    
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //注册
        [_tableView registerClass:[VLSVLiveShowTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.scrollEnabled = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

- (void)rightItemAction:(id)sender{
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft)
        {
            CGRect frame = self.parentViewController.view.frame;
            frame.origin.x = CGRectGetWidth(self.parentViewController.view.superview.frame);
            self.parentViewController.view.frame = frame;
        }
        else
        {
            self.parentViewController.view.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 275);
        }
    } completion:^(BOOL finished) {
        
        [self.parentViewController.view removeFromSuperview];
    }];
}
@end
