//
//  VLSCourseDetailViewController.m
//  VLiveShow
//
//  Created by tom.zhu on 2016/11/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSCourseDetailViewController.h"
#import "VLiveShow-Swift.h"
#import <WebKit/WebKit.h>
#import "ShareActionSheet.h"
#import "VLSOrderView.h"
#import "HTTPFacade+get.h"
#import "VLSUtily.h"
#import "VLiveShow-Swift.h"
#import "VLSLiveListModel.h"

static CGFloat const BUTTONHEIGHT = 52;
static NSInteger const CONFIRM_ALERT = 4505;
static NSInteger const BALANCENOTENOUGH_ALERT = 4506;

typedef NS_ENUM(NSInteger) {
    ORDER_ATTENDIENCE = 0,
    ORDER_AUDIENCE,
}ORDERTYPE;

@interface VLSCourseDetailViewController () <ShareActionSheetDelegate, OrderViewDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) WKWebView *WKWebView;
@property (nonatomic, strong) ShareActionSheet* shareActionSheet;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (nonatomic, strong) VLSOrderView *orderView;
@property (nonatomic, strong) UIView *maskView;
@property (nonatomic, assign) ORDERTYPE orderType;  //订阅类型
@property (nonatomic, strong) NSString *contentUrl; //课程Url
@property (nonatomic, assign) CourseState innerState;
@property (nonatomic, copy) void (^updateBalanceBlock) (NSString *newBalance);
@property(nonatomic,strong) AccountModel* account;
@end

@implementation VLSCourseDetailViewController
- (void)dealloc {
    [self.account removeObserver:self forKeyPath:@"diamondBalance"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setInnerState:(CourseState)innerState {
    WS(ws)
    _innerState = innerState;
    if (ws.state == course_after) {
        [ws.confirmBtn setHidden:true];
        CGRect rect = ws.WKWebView.frame;
        rect.size.height = ws.view.height_sd;
        ws.WKWebView.frame = rect;
        ws.title = LocalizedString(@"COURSE_DETAIL");
    }else {
        [ws.confirmBtn setHidden:false];
        dispatch_async(dispatch_get_main_queue(), ^{
            CGRect rect = ws.WKWebView.frame;
            rect.size.height = ws.view.height_sd - BUTTONHEIGHT;
            ws.WKWebView.frame = rect;
        });
        
        UIColor *color = [UIColor clearColor];
        NSString *str = @"";
        SEL sel = nil;
        if (innerState == course_order || innerState == course_before) {
            color = RGB16(COLOR_BG_FF1130);
            str = LocalizedString(@"order");
            sel = @selector(orderClicked:);
            ws.title = [NSString stringWithFormat:@"%@%@%@", LocalizedString(@"SUBSCRIBE"), ws.model.teacherName, LocalizedString(@"liveof")];
            ws.confirmBtn.enabled = YES;
        }else if (innerState == course_orderd) {
            color = RGB16(COLOR_BG_DBDBDB);
            str = LocalizedString(@"orderd");
            ws.title = LocalizedString(@"COURSE_DETAIL");
            ws.confirmBtn.enabled = NO;
        }
        [ws.confirmBtn setBackgroundColor:color];
        [ws.confirmBtn setTitle:str forState:UIControlStateNormal];
        [ws.confirmBtn addTarget:ws action:sel forControlEvents:UIControlEventTouchUpInside];
    }
    if ([ws.model.teacherId longValue] == [AccountManager sharedAccountManager].account.uid) {
        ws.title = LocalizedString(@"COURSE_DETAIL");
    }
}

- (NSString *)contentUrl {
    return [NSString stringWithFormat: @"%@/pages/course/detail.html?id=%@", VLIVESHOW_HOST, self.model.courseId];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden: NO animated: YES];
    [self.navigationController.navigationBar useRedTheme];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:ENABLEENTERLIVEROOM object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.orderType = ORDER_AUDIENCE;
    self.extendedLayoutIncludesOpaqueBars = YES;
    WS(ws)
    self.title = LocalizedString(@"COURSE_DETAIL");
    [self requestData];
    [self configUI];
    self.confirmBtn.enabled = false;
    
    self.updateBalanceBlock = ^(NSString *newBalance) {
        [ws.orderView updateBalanc:newBalance];
    };
    
    self.account = [[AccountManager sharedAccountManager] account];
    [self.account addObserver:self forKeyPath:@"diamondBalance" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)requestData {
    //获取用户V钻
    [[VLSIAPManager sharedInstance] loadCurrentBlance:nil];
    
    if ([self.model.teacherId longValue] == [AccountManager sharedAccountManager].account.uid) {
        self.confirmBtn.hidden = YES;
        CGRect rect = self.WKWebView.frame;
        rect.size.height = SCREEN_HEIGHT - BUTTONHEIGHT;
        self.WKWebView.frame = rect;
        return;
    }
    WS(ws)
    //用户是否已经购买课程
    NSString *path = [NSString stringWithFormat:@"/course/%@/subscription", self.model.courseId];
    NSDictionary *param = @{
                            @"token" : [AccountManager sharedAccountManager].account.token
                            };
    [HTTPFacade getPath:path param:param successBlock:^(id  _Nonnull object) {
        NSDictionary *dic = ((NSDictionary*)object);
        if ([[dic objectForKey:@"isSubCourse"] boolValue]) {
            //已经订阅
            [ws setInnerState:course_orderd];
        }else {
            //未订阅
            [ws setInnerState:course_order];
        }
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        
    }];
    
    path = [NSString stringWithFormat:@"/course/%@/detail", self.model.courseId];
    [HTTPFacade getPath:path param:param successBlock:^(id  _Nonnull object) {
        NSDictionary *dic = [object objectForKey:@"courseDetail"];
        ws.model.courseId = dic[@"id"];
        ws.model.allowAudience = dic[@"allowAudience"];
        //        ws.model.deleted = dic[@"deleted"];
        ws.model.published = dic[@"published"];
        ws.model.rmbPriceAudience = dic[@"rmbPriceAudience"];
        ws.model.rmbPriceParticipant = dic[@"rmbPriceParticipant"];
        //        ws.model.subscribe = dic[@"subscribe"];
        ws.model.surplusDiamond = dic[@"surplusDiamond"];
        ws.model.surplusParticipants = dic[@"surplusParticipants"];
        ws.model.numLessons = dic[@"numLessons"];
        ws.orderView.model = ws.model;
        ws.confirmBtn.enabled = _innerState != course_orderd;
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        
    }];
}

- (void)configUI {
    UIImage* shareImage = [[UIImage imageNamed:@"share"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithImage: shareImage landscapeImagePhone: shareImage style: UIBarButtonItemStylePlain target: self action: @selector(didClickShareButton)];
    item.tintColor = [UIColor clearColor];
    [self.navigationItem setRightBarButtonItem: item];
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc]init];
    config.preferences = [[WKPreferences alloc] init];
    config.preferences.minimumFontSize = 12;
    config.preferences.javaScriptEnabled = YES;
    config.preferences.javaScriptCanOpenWindowsAutomatically = NO;
    config.processPool = [[WKProcessPool alloc] init];
    config.userContentController = [[WKUserContentController alloc] init];
//    [config.userContentController addScriptMessageHandler:self name:@"myapp"];
    
    self.WKWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - BUTTONHEIGHT) configuration:config];
    self.WKWebView.scrollView.bouncesZoom = false;
    self.WKWebView.scrollView.maximumZoomScale = 1;
    self.WKWebView.scrollView.minimumZoomScale = 1;
    self.WKWebView.scrollView.showsVerticalScrollIndicator = false;
    NSURL *url = [NSURL URLWithString:self.contentUrl];
    [_WKWebView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view insertSubview:_WKWebView belowSubview:self.confirmBtn];
    
    _WKWebView.opaque = NO;
    _WKWebView.backgroundColor = [UIColor whiteColor];
    _WKWebView.scrollView.backgroundColor = [UIColor whiteColor];
    
    //maskView
    self.maskView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.maskView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3f];
    self.maskView.alpha = 0;
    [self.view insertSubview:self.maskView belowSubview:self.confirmBtn];
    self.maskView.userInteractionEnabled = true;
    [self.maskView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeOrderView)]];
    //orderView
    self.orderView = [[[NSBundle mainBundle] loadNibNamed:@"VLSOrderView" owner:nil options:nil] firstObject];
    self.orderView.delegate = self;
    self.orderView.width_sd = SCREEN_WIDTH;
    self.orderView.top_sd = CGRectGetHeight(self.view.frame) - BUTTONHEIGHT;
}

- (void)didClickShareButton {
    [self.WKWebView evaluateJavaScript:@"window.share()" completionHandler:^(id item, NSError * _Nullable error) {
        if (error != nil) {
            [self showShareSheet];
        }
    }];
}

- (void)showShareSheet {
    NSArray * images = @[@"broadcast-share-qq",@"broadcast-share-zone",@"broadcast-share-weixin",@"share_moments",@"weibo01"];
    self.shareActionSheet = [[ShareActionSheet alloc] initWithFrame:CGRectMake(0, 0 , SCREEN_WIDTH, 258) delegate:self images:images];
    [self.shareActionSheet addTarget: self action: @selector(dismissActionSheet:) forControlEvents: UIControlEventTouchUpInside];
    [self.shareActionSheet showInView: self.view];
}

- (void)dismissActionSheet:(ShareActionSheet *)actionSheet {
    [actionSheet dismiss];
}

- (void)orderClicked:(UIButton*)btn {
    //[self showOrderView];
}

- (void)showOrderView {
    [self.confirmBtn setTitle:LocalizedString(@"YES") forState:UIControlStateNormal];
    WS(ws)
    [self.view insertSubview:self.orderView belowSubview:self.confirmBtn];
    [UIView animateWithDuration:.25f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        ws.orderView.top_sd = ws.view.height_sd - BUTTONHEIGHT - ORDERVIEW_HEIGHT;
        ws.maskView.alpha = 1;
    } completion:nil];
}

- (void)closeOrderView {
    [self.confirmBtn setTitle:LocalizedString(@"order") forState:UIControlStateNormal];
    WS(ws)
    [UIView animateWithDuration:.25f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        ws.orderView.top_sd = CGRectGetHeight(self.view.frame) - BUTTONHEIGHT;
        ws.maskView.alpha = 0;
    } completion:nil];
}

- (IBAction)confirmButtonClicked:(id)sender {
    if (!self.maskView.alpha) {
        [self showOrderView];
        return;
    }
    //余额判断
    int order_diamond = self.orderType ? [self.model.priceAudience intValue] : [self.model.priceParticipant intValue];
//    if ([self.model.surplusDiamond intValue] < order_diamond) {
    if ([[[AccountManager sharedAccountManager] account] diamondBalance] < order_diamond) {
        UIAlertView *al = [[UIAlertView alloc] initWithTitle:LocalizedString(@"BALANCE_NOTENOUGH") message:LocalizedString(@"charging") delegate:self cancelButtonTitle:LocalizedString(@"CANCLE") otherButtonTitles:LocalizedString(@"gotocharge"), nil];
        al.tag = BALANCENOTENOUGH_ALERT;
        [al show];
    }else {
        UIAlertView *al = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@%@%@", LocalizedString(@"confirm_buy"), self.model.teacherName, LocalizedString(@"liveof")] delegate:self cancelButtonTitle:LocalizedString(@"CANCLE") otherButtonTitles:LocalizedString(@"INVITE_SURE"), nil];
        al.tag = CONFIRM_ALERT;
        [al show];
    }
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (![keyPath isEqualToString:@"diamondBalance"]) {
        return;
    }
    NSNumber *newBalance = change[@"new"];
    self.updateBalanceBlock([newBalance stringValue]);
}

#pragma mark - ShareActionSheetDelegate
-(void)xs_actionSheet:(ShareActionSheet *)actionSheet clickedButtonIndex:(NSInteger)index {
    if (index > 5 || ![ShareActionSheet isInstallation: index]) {
        [self showAlterviewController:LocalizedString(@"SHARE_HTML_NO_INSTALL_APP")];
    }
    else {
        NSString *title = nil;
        NSString *content = nil;
        NSURL *imageUrl = nil;
        if (self.liveModel) {
            title = [NSString stringWithFormat:@"%@的课程直播间", self.liveModel.hostNickName];
            content = self.self.liveModel.title;
            imageUrl = [NSURL URLWithString:self.liveModel.roomCover];
        }else {
            title = [NSString stringWithFormat:@"%@的课程直播间", self.model.teacherName];
            content = self.model.courseName;
            imageUrl = [NSURL URLWithString:self.model.picCover];
        }
//        [self umengShareToThirdPlatformName:umShares[index] title:title content:content imageURL:imageUrl urlString:urlString overrideTitleInWXTimeline: NO completion:^(UMSocialResponseEntity *response) {
//            [actionSheet dismiss];
//        }];
        
        
        
        NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                      [NSURL URLWithString:self.model.picCover].absoluteString,      @"coverImageURL",
                                      imageUrl.absoluteString,      @"coverImageURL",
                                      content,           @"content",
                                      title, @"title",
                                      @NO,         @"overrideTitleInWXTimeline",
                                      self.contentUrl, @"urlString",
                                      nil];
        [self shareToPlatform:index messageObject:dict completion:^(NSDictionary *item) {
            [actionSheet dismiss];
        }];
    }
}

#pragma mark - OrderViewDelegate
- (void)orderViewCloseClicked {
    [self closeOrderView];
}

- (void)orderViewAttendanceClicked {
    self.orderType = ORDER_ATTENDIENCE;
    if ([self.model.surplusParticipants intValue] <= 0) {
        [VLSUtily toastWithText:LocalizedString(@"attentiance_full") addtoview:[[[UIApplication sharedApplication] delegate] window] animation:YES duration:1];
    }
}

- (void)orderViewAudienceClicked {
    self.orderType = ORDER_AUDIENCE;
}

- (void)orderViewPayClicked {
    UIViewController* vc = [[UIStoryboard storyboardWithName: @"Payment" bundle: nil] instantiateInitialViewController];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        return;
    }
    if (alertView.tag == CONFIRM_ALERT) {
        WS(ws)
        //余额判断
        int order_diamond = self.orderType ? [self.model.priceAudience intValue] : [self.model.priceParticipant intValue];
        if ([self.model.surplusDiamond intValue] < order_diamond) {
            [VLSUtily toastWithText:LocalizedString(@"BALANCE_NOTENOUGH") addtoview:self.view animation:YES duration:1];
            return;
        }
        
        [self showProgressHUD];
        NSString *path = [@"/course/subscription?token=" stringByAppendingString:[AccountManager sharedAccountManager].account.token];
        NSDictionary *param = @{
                                @"token" : [AccountManager sharedAccountManager].account.token,
                                @"courseId" : self.model.courseId,
                                @"subscriptionType" : @(!self.orderType),
                                };
        [HTTPFacade postPath:path param:param body:nil successBlock:^(id  _Nonnull object) {
            [ws hideProgressHUDAfterDelay:.5f];
            
            UIColor *color = RGB16(COLOR_BG_DBDBDB);
            NSString *str = LocalizedString(@"orderd");
            dispatch_async(dispatch_get_main_queue(), ^{
                [ws closeOrderView];
                [ws.confirmBtn setBackgroundColor:color];
                ws.confirmBtn.enabled = NO;
                [ws.confirmBtn setTitle:str forState:UIControlStateDisabled];
                ws.confirmBtn.enabled = false;
            });
            
        } errorBlcok:^(ManagerEvent * _Nonnull error) {
            [ws hideProgressHUDAfterDelay:.5f];
            [VLSUtily toastWithText:error.title addtoview:self.view animation:YES duration:1];
        }];
    }else if (alertView.tag == BALANCENOTENOUGH_ALERT) {
        UIViewController* vc = [[UIStoryboard storyboardWithName: @"Payment" bundle: nil] instantiateInitialViewController];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setShadowImage:nil];
}

@end
