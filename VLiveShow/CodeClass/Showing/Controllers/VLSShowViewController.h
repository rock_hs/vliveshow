//
//  VLSShowViewController.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMultiLiveViewController.h"
#import "VLSLiveListModel.h"
#import "UserView.h"
#import "ApplyBGView.h"

@interface VLSShowViewController : VLSMultiLiveViewController
@property (nonatomic, assign) BOOL isHost;
@end
