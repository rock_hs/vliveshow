//
//  VLSLiveNoticeViewController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"

@interface VLSLiveNoViewController : VLSBaseViewController
@property (nonatomic, assign) KLiveRoomOrientation orientation;
@end
