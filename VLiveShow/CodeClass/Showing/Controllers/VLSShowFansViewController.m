//
//  VLSShowFansViewController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSShowFansViewController.h"
#import "VLSLittleNavigationController.h"

@interface VLSShowFansViewController ()

@end

@implementation VLSShowFansViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated{

//    self.parentViewController.view.frame = CGRectMake(0, SCREEN_HEIGHT - 275, SCREEN_WIDTH, 275);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = LocalizedString(@"FANS");
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];

    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, (275-44));
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    VLSFansModel *model = [self.fansArray objectAtIndex:indexPath.row];
    [[VLSUserTrackingManager shareManager] trackingViewprofile:model.UserID];
    vlsVC.userId = model.UserID;
    [self pushController:vlsVC];
}

#pragma mark - cell代理事件
-(void)focusBtnViewCellBtn:(VLSFansViewCell *)cell{
    
    NSIndexPath *indexpath = [self.tableView indexPathForCell:cell];
    VLSFansModel *model = [self.fansArray objectAtIndex:indexpath.row];
    UserProfileViewController *vlsVC = [UserProfileViewController create];
    [[VLSUserTrackingManager shareManager] trackingViewprofile:model.UserID];
    vlsVC.userId = model.UserID;
    [self pushController:vlsVC];
}

- (void)pushController:(VLSOthersViewController*)vls{

    VLSLittleNavigationController *little = (VLSLittleNavigationController*)self.navigationController;
    [little.fatherVC.navigationController pushViewController:vls animated:YES];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        
        self.navigationController.view.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 275 );
        
    } completion:^(BOOL finished) {
        
        [self.navigationController.view removeFromSuperview];
        
    }];

}

@end
