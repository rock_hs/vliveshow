//
//  VLSMailViewController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveNoticeViewController.h"

@interface VLSLRViewController : VLSLiveNoticeViewController
@property (nonatomic, assign) KLiveRoomOrientation orientation;
@end
