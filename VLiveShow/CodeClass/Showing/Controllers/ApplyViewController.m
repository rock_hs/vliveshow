//
//  ApplyViewController.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/20.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "ApplyViewController.h"

@interface ApplyViewController (){

    //获取到的键盘的高度
    CGFloat keyHeight;
}

@end

@implementation ApplyViewController

- (instancetype)init
{
    if (self = [super init]) {
        //创建键盘将要显示的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardUp:) name:UIKeyboardWillShowNotification object:nil];
        //键盘将要收起的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDown:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - keyboard通知处理
- (void)keyboardUp:(NSNotification*)notify{
    NSValue *value = [notify.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    //得到键盘高度
    CGFloat tempHeight = value.CGRectValue.size.height;
    //为了解决反复拖动键盘上联想内容部分，需要判断keyHeight和tempHeight是否相同，然后实时记录键盘高度的变化
    if (keyHeight != tempHeight) {
        //修改contentView的高度
        CGRect contentFrame = self.view.frame;
        contentFrame.origin.y -= tempHeight - keyHeight;
        self.view.frame = contentFrame;
        keyHeight = tempHeight;
    }
}

- (void)keyboardDown:(NSNotification*)notifiy{
    //修改contentView的高度
    CGRect contentFrame = self.view.frame;
    contentFrame.origin.y = 0;
    self.view.frame = contentFrame;
    //回到初始状态
    keyHeight = 0;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
