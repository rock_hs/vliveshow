//
//  VLSShowViewController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSShowViewController.h"
#import "UIView+SDAutoLayout.h"
#import "UIView+LYAdd.h"
#import "VLSTeamMessageModel.h"
#import "ApplyBGView.h"
#import "CancelOnLine.h"
#import "VLSAVMultiManager.h"
#import "MBProgressHUD+Add.h"
#import "VLSUserProfile.h"
#import "CountDownView.h"
#import "ApplyViewController.h"
#import "VLSLiveHttpManager.h"
#import <AVFoundation/AVFoundation.h>
#import "VLSLittleNavigationController.h"
#import "HTTPFacade+get.h"
#import "VLSUtily.h"
#import "TiePhoneNumberController.h"

static unsigned int CloundKey = CLOUND_SDK;
#define anchorHight 35
@interface VLSShowViewController ()<VLSMessageManagerDelegate,ApplyBGViewDelegate,CountDownViewDelegate>
{
    CountDownView *Count;
    BOOL _isMutedDuringCount;      //立即上镜，倒计时过程中是否已经静音muteLocal
    
}
// 连线状态
@property (nonatomic, assign) BOOL isOnLine;
// 连线申请成功 点击friend
@property (nonatomic, strong) CancelOnLine *cancleLine;
// 申请连线
@property (nonatomic, strong) ApplyBGView *GT_applyView;
/// 直播开始前的模糊背景图
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) NSTimer* timer;
@property (nonatomic, assign) BOOL isAVAudioSessionActive;
@property (nonatomic, strong) VLSUserTrackContext* linkTrackingContext;
@property (nonatomic, assign) BOOL isAcceptInvite;  //是否被邀请

@end

@implementation VLSShowViewController
- (void)setCourseId:(NSString *)courseId {
    super.courseId = courseId;
    NSNumber *hostID = [NSNumber numberWithInteger:[AccountManager sharedAccountManager].account.uid];
    if ([self.anchorId integerValue] == [hostID integerValue]) {
        [self.contentView.bottomView setActionType:ActionTypeLessonTeacher];
        self.actionType = ActionTypeLessonTeacher;
    }else if (courseId) {
        /*
         * 超管无权限进入直播间
        if ([[[AccountManager sharedAccountManager] account] isSuperUser]) {
            [self.contentView.bottomView setActionType:ActionTypeLessonAudience];
            self.actionType = ActionTypeLessonAudience;
            return;
        }
        */
        [VLSUtily isSubscription:self.courseId handle:^(id result, ManagerEvent *error) {
            if ([[result objectForKey:@"isParticipant"] boolValue]) {
                //连线嘉宾
                [self.contentView.bottomView setActionType:ActionTypeLessonAttendance];
                self.actionType = ActionTypeLessonAttendance;
            }else if ([[result objectForKey:@"isCourseVipUser"] boolValue] || [[result objectForKey:@"isSubCourse"] boolValue]) {
                //VIP，观众
                [self.contentView.bottomView setActionType:ActionTypeLessonAudience];
                self.actionType = ActionTypeLessonAudience;
            }else {/*没有权限进直播间*/}
        }];
    }
}

- (void)setAnchorId:(NSString *)anchorId {
    [super setAnchorId:anchorId];
    NSNumber *hostID = [NSNumber numberWithInteger:[AccountManager sharedAccountManager].account.uid];
    if ([self.anchorId integerValue] == [hostID integerValue])
        [self.contentView.bottomView setActionType:ActionTypeLessonTeacher];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoginAndCloseLiveRoom) name:@"LoginAgain" object:nil];
    [self receiveNotification];
    _isMutedDuringCount = NO;
//    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
////        [self.videoView orientation:horizontal];
//        [[VLSAgoraVideoService sharedInstance] setRemoteRenderMode:[self.anchorId integerValue] mode:AgoraRtc_Render_Fit];
//    }else {
////        [self.videoView orientation:Vertical];
//        [[VLSAgoraVideoService sharedInstance] setRemoteRenderMode:[self.anchorId integerValue] mode:AgoraRtc_Render_Hidden];
//    }
    
    //默认都是Hidden模式
    [[VLSAgoraVideoService sharedInstance] setRemoteRenderMode:[self.anchorId integerValue] mode:AgoraRtc_Render_Hidden];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChangeNotification:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    NSError* error = nil;
    //启用audio session
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    //设置audio session的category
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
//
//    //设置定时检测
//    self.timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(detectionVoice) userInfo:nil repeats:YES];
//    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    
    //1竖2横 nil竖
//    if ([self.orientation integerValue] == 2) {
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        [self setActionType:ActionTypeNormalLandscape];
    }
    //进入房间默认无法发送消息
    [self.contentView.bottomView setForbidSendMsg:YES];
    [self requestIsForbidSendMsg:0];
}

- (void)requestIsForbidSendMsg:(int)count {
        WS(ws)
        AccountManager *account = [AccountManager sharedAccountManager];
        NSString *path = [NSString stringWithFormat:@"/liveshow/userInfo?userId=%ld&roomId=%@", account.account.uid, self.roomId];
        [HTTPFacade getPath:path param:@{} successBlock:^(id  _Nonnull object) {
            BOOL isForbidden = [[object objectForKey:@"isForbidden"] boolValue];
            [ws.contentView.bottomView setForbidSendMsg:isForbidden];
        } errorBlcok:^(ManagerEvent * _Nonnull error) {
            int temCount = count;
            if (temCount >= 4) return;
            [ws requestIsForbidSendMsg:(++temCount)];
        }];
}

- (void)getForbidFendMsgList {
    NSString *path = [NSString stringWithFormat:@"https://console.tim.qq.com/v4/group_open_http_svc/get_group_shutted_uin?usersig=%@&identifier=admin&sdkappid=%d&random=99999999&contenttype=json", [[[AccountManager sharedAccountManager] account] tenderSig], CloundKey];
    NSDictionary *params = @{
                             @"GroupId" : self.roomId,
                             };
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [securityPolicy setAllowInvalidCertificates:YES];
    [securityPolicy setValidatesDomainName:NO];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 20.0;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.securityPolicy = securityPolicy;
    [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

//重新设置AVAudioSession是否处于活动状态
- (void)detectionVoice
{
    if (!self.isAVAudioSessionActive) {//重新将AVAudioSession设置为活跃状态，来达到按音量键静音效果
        NSError* error = nil;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
        self.isAVAudioSessionActive = [[AVAudioSession sharedInstance] setActive:YES error:&error];
        if (self.isAVAudioSessionActive) {
            [self.timer invalidate];
            self.timer = nil;
        }
    }
}

- (void)deviceOrientationDidChangeNotification:(NSNotification*)noti {
    UIDeviceOrientation ori = [[UIDevice currentDevice] orientation];
//    if ([self.orientation integerValue] == 2) {
//     [self.videoView rotate:ori];
//    }

    [self.videoView rotate:ori superView:self.view];
    self.contentView.oriention = ori;
    
    //退出通知
    VLSLittleNavigationController *nav = (VLSLittleNavigationController*)[self valueForKey:@"nav"];
    if (nav) {
        [nav.view removeFromSuperview];
    }
    
    BOOL contentShow = [[self contentView] isHidden];
    if (contentShow) {
//        self.contentView.transform  = CGAffineTransformMakeTranslation(SCREEN_WIDTH, 0);
        self.contentView.x = SCREEN_WIDTH;
        [self setValue:@(SCREEN_WIDTH) forKey:@"currentTranslate"];
    }
    
//    if ([self.orientation integerValue] == 2) {
    if (self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) {
        [self.closeViewButton setTop_sd:(ori == UIDeviceOrientationPortrait ? 24 : 14)];
        [self.contentView.anchor setTop_sd:(ori == UIDeviceOrientationPortrait ? 24 : 14)];
    }
    
    if (ori == UIDeviceOrientationPortrait || ori == UIDeviceOrientationPortraitUpsideDown) {
        kSaveOriention([NSNumber numberWithInt:1]) //1代表竖屏
    } else if (ori == UIDeviceOrientationLandscapeLeft || ori ==  UIDeviceOrientationLandscapeRight) {
        kSaveOriention([NSNumber numberWithInt:2]) //2代表横屏
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    id temDelegate = self.contentView.shareAction.delegate;
    if (self.contentView.shareAction) {
        [self.contentView.shareAction dismiss];
        self.contentView.shareAction = nil;
    }
    self.contentView.shareAction.delegate = temDelegate;
}

- (void)reloadAnchorInfo
{
    [super reloadAnchorInfo];
}

- (void)joinRoomSuccess
{
    [super joinRoomSuccess];
    [self audiuenceJoinInvite];
}

// 用户从邀请进入
- (void)audiuenceJoinInvite
{
    if (self.isFromMailInvite) {
        if (self.videoView.NewGuestInfos.count < 7) {//新增“如果上线嘉宾已满（6个），则取消上镜，默认只进入直播间”逻辑
            self.isAcceptInvite = YES;
            [self bottomFriendClicked:nil];
            self.GT_applyView.isAcceptInvite = YES;
            [self onLine];
        } else {
            [self showHUD:LocalizedString(@"INVITE_GUEST_SEAT_IS_FULL")];
        }
    }
}

//邀请成功直接上线
- (void)onLine
{
    // 网络请求IDs
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
    };
    callback.errorBlock = ^(id result){
        [self hideProgressHUDAfterDelay:0];
        ManagerEvent *event = result;
        if (event.code == 0000) {
        }
    };
    [[VLSAVMultiManager sharedManager] anchorAcceptLinkRequest:@[[VLSMessageManager sharedManager].host.userId] callback:callback];
    // 发送通知直播间底部状态看变灰
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AnchorConfirm" object:nil];
    [self performSelector:@selector(receiveLinkRequestAccept)];
    
    [self.GT_applyView comeBackGuest];
}

- (void)receiveNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMsgWindow:) name:@"showMsgWindow" object:nil];
}
- (void)showMsgWindow:(NSNotification*)notification{
    
    VLSTeamMessageModel *inviteInfo = notification.object;
    if ([inviteInfo.anchorID isEqualToString:self.anchorId] && [inviteInfo.roomID isEqualToString:self.roomId]) {
        VLSMessageManager *manager = [VLSMessageManager sharedManager];
        if ([manager checkUserIsGuests:manager.host.userId]) {
            return;
        }
        // 未申请状态 或者 没有申请界面
        if (self.GT_applyView.applyStatus == nonApply || !self.GT_applyView) {
            [self showInviteAlter];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"hideTip" object:nil];

    }
    
}
- (void)showInviteAlter
{
    //富文本属性
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"INVITE_ANCHOR") attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16],NSForegroundColorAttributeName:[UIColor blackColor]}];
    //富文本属性
    NSMutableAttributedString *attributedMessage= [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"INVITE_ANCHOR_MESSAGE") attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:RGB16(COLOR_FONT_888888)}];
    
    // 在 viewDidLoad 中创建
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:attributedTitle.string message:attributedMessage.string preferredStyle:UIAlertControllerStyleAlert];
    // 用 KVC 修改其 没有暴露出来的
    [alertVC setValue:attributedTitle forKey:@"attributedTitle"];
    [alertVC setValue:attributedMessage forKey:@"attributedMessage"];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *roomid = nil;
        if ([[VLSMessageManager sharedManager] getRoomId]) {
            
            roomid = [[VLSMessageManager sharedManager] getRoomId];
        }else{
            
            roomid = @"nil";
        }
        [[VLSUserTrackingManager shareManager] trackingAgreeGotoRoom:roomid anchorid:self.anchorId fromWhere:@"roomInvite_off"];
        // 取消 如果没有申请
        [self.contentView.bottomView changefriendStatus:YES];
    }];
    [cancelAction setValue:[UIColor blackColor] forKey:@"_titleTextColor"];
    [cancelAction setValue:[UIFont systemFontOfSize:14] forKey:@"titleFont"];
    
    [alertVC addAction:cancelAction];
    
    UIAlertAction *goOnAction = [UIAlertAction actionWithTitle:LocalizedString(@"INVITE_AGREE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([AccountManager sharedAccountManager].account.mobilePhone != nil && ![[AccountManager sharedAccountManager].account.mobilePhone isEqualToString:@""]) {
            self.isAcceptInvite = YES;
            // 接受申请
            if(!self.GT_applyView){
                [self bottomFriendClicked:nil];
            }
            self.GT_applyView.isAcceptInvite = YES;
            //直接上线
            [self onLine];
        } else {
            TiePhoneNumberController* acc = [[TiePhoneNumberController alloc] init];
            [self.navigationController pushViewController:acc animated:YES];
        }
    }];
    [goOnAction setValue:[UIColor blackColor] forKey:@"_titleTextColor"];
    [goOnAction setValue:[UIFont systemFontOfSize:14] forKey:@"titleFont"];
    [alertVC addAction:goOnAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertVC animated:YES completion:nil];
    });
}



//继承自父类 网络请求
- (void)guestCancelLinkSuccess
{
    [super guestCancelLinkSuccess];
    [self removeApplyView];
}

- (void)guestCancelLinkRequestSuccess
{
    [self removeApplyView];
}

- (void)touchOnLiveBaseView:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
{
//    if (self.GT_applyView) {
//        self.GT_applyView.hidden = YES;
//    }
    [super touchOnLiveBaseView:touches withEvent:event];
}

#pragma mark - Rotate
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

-(BOOL)shouldAutorotate {
//    if ([self.orientation integerValue] == 2) {
//        //在视频播放时刻开始允许横屏
//        BOOL shouldRoate = self.videoCoverView.hidden;
//        return shouldRoate;
//    }
    return NO;
}

#pragma mark - CountDownViewDelegate 倒记时结束代理
//倒计时结束显示画面
- (void)countComplete
{
    //倒计时结束，判断是否已经被mute
//    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:NO];
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:_isMutedDuringCount];
    [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:NO];
}

#pragma mark - VLSMessageManagerDelegate
//观众收到申请连线成功
- (void)receiveLinkRequestAccept
{
    NSLog(@"申请成功");
    if (self.GT_applyView) {
        self.GT_applyView.alpha = 1.0;
        [self.GT_applyView endSucc];
        [self.contentView.bottomView changefriendStatus:NO];
    }
    VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
    model.action = ANCHOR_ACCEPT;
    model.fromAccount = [VLSMessageManager sharedManager].host.userId;
    model.toAccount = [VLSMessageManager sharedManager].anchor.userId;
    model.roomId = [[VLSMessageManager sharedManager] getRoomId];
    self.linkTrackingContext = [[VLSUserTrackingManager shareManager] trackingLinkBegin: model];
}

//观众收到申请连线拒绝
- (void)receiveLinkRequestReject
{
    NSLog(@"申请拒绝");
    if (self.GT_applyView) {
        [self.GT_applyView endFail];
    }
}

- (void)receiveLinkOnCamera:(VLSUserProfile *)user
{
    [self.contentView.bottomView setActionType:ActionTypeGuest];
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:YES];
    [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:YES];
    // 倒计时动画
    Count = [[CountDownView alloc]initWithFrame:CGRectZero];
    Count.delegate  = self;
    [self.contentView addSubview:Count];
    [self.contentView insertSubview:Count atIndex:0];
    Count.sd_layout
    .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [Count setNumAnimation];
    });
}

- (void)receiveGuestMuteSelf:(BOOL)mute
{
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:mute];
    [self.contentView.bottomView showVoiceIcon:mute];
}

//禁麦
- (void)receiveAnchorMuteMic:(BOOL)mute
{
    if (mute) {
        self.headAlert.message = @"您已被主播静音";
        [self.headAlert showHeadAlertThenHidden];
    }
    [self.contentView.bottomView voiceEnable:!mute];
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:mute];
    _isMutedDuringCount = mute;
}

//自己取消连线
- (void)receiveGuestCancelLink
{
    [self.contentView.bottomView setActionType:ActionTypeLive];
    [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:YES];
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:YES];
    [[VLSAgoraVideoService sharedInstance] alternateUserType:AGUserTypeDefault];
    [self trackCancelLink];
}

//被取消连线
- (void)receiveLinkRequestCancel
{
    self.headAlert.message = @"您已被主播取消连线";
    [self.headAlert showHeadAlertThenHidden];
    [Count removeFromSuperview];
    Count.delegate = nil;
    [self.contentView.bottomView setActionType:ActionTypeLive];
    [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:YES];
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:YES];
    [[VLSAgoraVideoService sharedInstance] alternateUserType:AGUserTypeDefault];
    [self removeApplyView];
    [self trackCancelLink];
}

- (void)trackCancelLink
{
    VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
    model.action = ANCHOR_CANCEL;
    model.fromAccount = [VLSMessageManager sharedManager].host.userId;
    model.toAccount = [VLSMessageManager sharedManager].anchor.userId;
    model.roomId = [[VLSMessageManager sharedManager] getRoomId];
    [[VLSUserTrackingManager shareManager] trackingLinkEnd: model context: self.linkTrackingContext];
    self.linkTrackingContext = nil;
}

//收到更新主播信息view viewmode为空删除嘉宾；else 添加这个人;
- (void)receiveUpdataAnchorView:(VLSVideoViewModel *)viewModel
{
    if (viewModel == nil) {
        self.contentView.guestView.hidden = YES;
        [self.contentView.anchor setAnchorType:AnchorOnScreen];
    }else{
        self.contentView.guestView.hidden = NO;
        [self.contentView.guestView setGuestID:viewModel.userId];
        [self.contentView.anchor setAnchorType:GuestOnScreen];
    }
}

- (void)videoViewUpdateAnchorView:(VLSVideoViewModel*)model {
    [self receiveUpdataAnchorView:model];
}

// 收到站内信提醒
- (void)receiveMail
{
    [self.contentView.bottomView changeMailIcon:YES];
}

#pragma mark -ApplyBGViewDelegate
//取消连线直播中
- (void)fromApplyViewGuestCancelLink
{
    [self guestCancelLink];
}
//取消连线未直播
- (void)fromApplyViewGuestCancelLinkRequest
{
    [self guestCancelLinkRequest];
}

- (void)applyBGViewBindPhone {
    TiePhoneNumberController* acc = [[TiePhoneNumberController alloc] init];
    [self.navigationController pushViewController:acc animated:YES];
}

//声音点击
- (void)bottomVoiceClicked:(UIButton *)sender
{
    sender.selected = !sender.selected;
    _isMutedDuringCount = !sender.selected;
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:_isMutedDuringCount];
    //禁麦
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    callback.errorBlock = ^(id result){
        
    };
    [[VLSAVMultiManager sharedManager] geneusMuteSelf:_isMutedDuringCount callback:callback];
}

//朋友点击
- (void)bottomFriendClicked:(id)sender
{
    if (!self.GT_applyView) {
        self.GT_applyView = [[ApplyBGView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 280. / 667 * SCREEN_HEIGHT ) selfurl:[VLSMessageManager sharedManager].host hosturl:self.anchorProfile];
        self.GT_applyView.delegate = self;
        [self.view addSubview:self.GT_applyView];
        if ([VLSAVMultiManager sharedManager].linkStatus == UserLinkStatusOnCamera) {
            [self.GT_applyView comeBackGuest];
        }
    }else{
        self.GT_applyView.alpha = 1.0;
    }
    if (self.isAcceptInvite) { //是否被邀请上镜，被邀请，直接上镜
        self.GT_applyView.alpha = 0.0;
        self.GT_applyView.hidden = YES;
    } else {
        self.GT_applyView.alpha = 1.0;
        self.GT_applyView.hidden = NO;
    }
    self.isAcceptInvite = NO;
//    [self.contentView.bottomView changefriendStatus:NO];
}

#pragma mark - 申请连线如果已注册，跳转登录并关闭直播间
- (void)showLoginAndCloseLiveRoom {
    [self audienceQuiteRoom];
}

#pragma mark -actions
- (void)removeApplyView
{
    if (self.GT_applyView) {
        [self.GT_applyView removeFromSuperview];
        self.GT_applyView = nil;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSError* error = nil;
    //启用audio session
    //[[AVAudioSession sharedInstance] setActive:YES error:&error];
    //设置audio session的category
    //[[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    //销毁定时器
    if (self.timer != nil) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    /*
    [self configOriention];
     */
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    /*
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait) {
        return;
    }
    
//    if (SYSTEM_VERSION_GREATER_THAN(@"8.0") && SYSTEM_VERSION_LESS_THAN(@"9.0")) {
//        
//    }else {
//        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
//    }
    if (SYSTEM_VERSION_GREATER_THAN(@"10.0")) {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];
    }else {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    }
    
    UIViewController *v = [UIViewController new];
    [self presentViewController:v animated:NO completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [v dismissViewControllerAnimated:NO completion:nil];
        });
    }];
     */
}

//- (void)dealloc
//{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LoginAgain" object:nil];
//}

@end
