//
//  VLSLiveViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/5/24.
//  Copyright © 2016年 vliveshow. All rights reserved.
//
#import "LeaveOffViewController.h"
#import "VLSLiveViewController.h"
#import "AppDelegate.h"
#import "VLSShowViewController.h"
#import "VLSHotCell.h"
#import "VLSLiveListModel.h"
#import "VLSMessageManager.h"
#import "VLSLiveHttpManager.h"
#import "MJRefresh.h"
#import "VLSMessageViewController.h"
#import "VLSAnchorViewController.h"
#import "VLSNetworkBreakView.h"
#import "AccountManager.h"
#import "VLSAdminViewController.h"
#import "MBProgressHUD+Add.h"
#import "UINavigationBar+Awesome.h"
#import "UITabBar+badge.h"
#import "HomeVLSHotCellTableViewCell.h"
#import "VLSAdListModel.h"
#import "HomeVLSAdTableViewCell.h"
#import "VLSMKWebViewController.h"
#import "VLSReConnectionManager.h"
#import "VLSSearchViewController.h"
#import "VLSToolTipsView.h"
#import "TalkingData.h"
#import "VLSCourseDetailViewController.h"
#import "VLiveShow-Swift.h"
#import <AVKit/AVKit.h>
#import "VLSNavigationController.h"
//#import "VLSCourseListViewController.h"

@interface VLSLiveViewController ()<UITableViewDelegate, UITableViewDataSource,VLSMessageManagerDelegate, VLSNetworkBreakViewDelegate,CloseDelegate, VLSMessageImageViewDelegate>
{
    MJRefreshAutoNormalFooter *footer;
    UIImageView *dotImageView;
    BOOL isFirstAppear;
}
/// 直播列表
@property (nonatomic, strong) UITableView *liveTableView;
/// 直播列表数据源
@property (nonatomic, strong)NSMutableArray *liveListsArray;
/// 断网状态 View
@property (nonatomic, strong)VLSNetworkBreakView *networkBreakView;
/// 请求数据页数
@property (nonatomic, assign)NSInteger page;
//广告数组
@property (nonatomic, strong) NSMutableArray *adListArray;
//展示数组
@property (nonatomic, strong) NSMutableArray *showListArray;
// 课程数组
@property (nonatomic, strong) NSMutableArray *courseListArray;
//过滤数组
@property (nonatomic, strong) NSMutableArray *passListArray;

//麦克风是否打开
@property (nonatomic, assign)BOOL isOpen;

@property (nonatomic,strong)NSIndexPath* indpath;
//是否下拉刷新过
@property (nonatomic) BOOL isFresh;
//获取直播间列表是否成功
@property (nonatomic) BOOL isSeccess;
//获取广告列表成功
@property (nonatomic) BOOL isAdSeccess;
//消息提示框
@property (nonatomic, strong) UIImageView *messageImg;

@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, copy) void (^enterLiveBlock) (NSString *courseId, NSIndexPath *indexPath);     //进入直播间方法

@property(nonatomic, assign) CGFloat lastOffset;

@property (nonatomic, strong) VLSMessageManager *messageManager;
/*
 *  防止快速点击多次进入房间,尽管莫名其妙，但还是发生了多次进房间
 *  限制以VLSMultiLiveBaseViewController为基类，以及“课程详情”页面;精彩回放，网页跳转不限制
 */
@property (nonatomic, assign) BOOL canEnterRoom;

@end

@implementation VLSLiveViewController
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar lt_reset];
    
    if ([self.timer isValid]) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    // 在iOS 8.4, 从横屏返回后，tabbar的frame会莫名其妙的变短，
    // 这里用了一个walk around
    [self adjustTabbarByWalkAroundIfSystemVersionEqualTO_8_4_X];
    // 设置导航条内容
    [self setUpNavBar];
    if (self.pagerViewController == nil)
    {
        self.navigationController.navigationBarHidden = NO;
        self.navigationController.navigationBar.backgroundColor = [UIColor redColor];
    }
    
    self.isFresh = NO;
    self.isSeccess = NO;
    self.isAdSeccess = NO;
    if (!isFirstAppear)
    {
        [self requestData];
    }
    isFirstAppear = NO;
    if (self.timer == nil)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop]addTimer:self.timer forMode:NSDefaultRunLoopMode];
    }

    [self recoverNaviBarStatus];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    if (self.navigationController.isNavigationBarHidden)
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleDefault];
    }
    else
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
    }
}

-(NSMutableArray *)adListArray{
    if (!_adListArray) {
        self.adListArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _adListArray;
}
-(NSMutableArray *)showListArray{
    if (!_showListArray) {
        self.showListArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _showListArray;
}
-(NSMutableArray *)passListArray{
    if (!_passListArray) {
        self.passListArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _passListArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self enableCanEnterRoom];
    isFirstAppear = YES;
    [self.navigationController.navigationBar useRedTheme];

    self.page = 0;
    self.courseListArray = [NSMutableArray array];
    
    self.liveTableView = [[UITableView alloc] initWithFrame:self.view.frame];
    _liveTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.liveTableView.delegate = self;
    self.liveTableView.dataSource = self;
    [self.liveTableView registerClass:[HomeVLSHotCellTableViewCell class] forCellReuseIdentifier:@"HotCell"];
    [self.liveTableView registerClass:[HomeVLSAdTableViewCell class] forCellReuseIdentifier:@"AdCell"];
    [self.liveTableView registerNib: [UINib nibWithNibName: @"LiveShowCourseCell" bundle: [NSBundle mainBundle]] forCellReuseIdentifier: @"CourseCell"];
    [self.liveTableView registerNib: [UINib nibWithNibName: @"LiveShowCourseCell" bundle: [NSBundle mainBundle]] forCellReuseIdentifier: @"PlaybackCell"];
    
    [self.view addSubview:self.liveTableView];
    self.liveTableView.sd_layout
    .topSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .bottomSpaceToView(self.view, 0);
    // 上拉加载
//    footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [self requestData];
//        [self test];
//    }];
//    [footer setTitle:LocalizedString(@"LIVE_PULL_LOAD_MORE") forState:MJRefreshStateRefreshing];
//    self.liveTableView.mj_footer = footer;
    
    // 下拉刷新
    __weak VLSLiveViewController *weakVC = self;
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [[VLSUserTrackingManager shareManager] trackingClickWithAction: @"ClickRefresh" extraInfo: @{}];
        weakVC.page = 0;
        self.isFresh = YES;
        self.isAdSeccess = NO;
        self.isSeccess = NO;
        [weakVC requestData];
    }];
    [header setTitle:LocalizedString(@"LIVE_PULL_REFRESH") forState:MJRefreshStateIdle];
    [header setTitle:LocalizedString(@"LIVE_PULL_REFRESH") forState:MJRefreshStatePulling];
    [header setTitle:LocalizedString(@"LIVE_PULL_REFRESHING") forState:MJRefreshStateRefreshing];
    self.liveTableView.mj_header = header;
    UIImage *image = [UIImage imageNamed:@"home_logo"];
    [header setImages:@[image] duration:2 forState:MJRefreshStateIdle];
    
    // 该行代码的作用是解决MJ_header和 MJ_footer 同时出现刷新的问题。
    self.liveTableView.mj_footer.state = 0;
    // 添加观察者
    [self addlistener];
    // 添加远程消息判断
    //    [self remoteInfoHandle];
    WS(ws)
    self.enterLiveBlock = ^ (NSString *courseId, NSIndexPath *indexPath) {
        dispatch_async(dispatch_get_main_queue(), ^{
           [ws showProgressHUD];
        });
        [VLSUtily isSubscription:courseId handle:^(id result, ManagerEvent *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
               [ws hideProgressHUDAfterDelay:.5f];
            });
            if ([[result objectForKey:@"isParticipant"] boolValue] || [[result objectForKey:@"isCourseVipUser"] boolValue] || [[result objectForKey:@"isSubCourse"] boolValue]) {
                UIViewController *vc = [UIViewController new];
                VLSLiveListModel *model = ws.showListArray[indexPath.row];
                AccountModel *account = [AccountManager sharedAccountManager].account;
                if (account.isSuperUser) {
                    VLSAdminViewController *admin = [[VLSAdminViewController alloc] init];
                    admin.anchorId = model.host;
                    admin.roomId = model.roomId;
                    admin.orientation = [model.orientation isEqualToNumber:@2] ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
                    admin.courseId = model.courseId;
                    admin.lessonId = model.lessonId;
                    vc = admin;
                }else {
                    //连线嘉宾,VIP，观众
                    VLSShowViewController *show = [[VLSShowViewController alloc] init];
                    show.anchorId = model.host;
                    show.roomId = model.roomId;
                    show.orientation = [model.orientation isEqualToNumber:@2] ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
                    show.courseId = model.courseId;
                    show.lessonId = model.lessonId;
                    
                    VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
                    joinRoom.roomID = model.roomId;
                    joinRoom.joinType = @"living_list";
                    show.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];
                    vc = show;
                }
                vc.hidesBottomBarWhenPushed = YES;
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    // code here
//                    if (ws.navigationController.viewControllers.count > 1) {
//                        return ;
//                    }
                    [ws.navigationController pushViewController:vc animated:YES];
//                });
            }else {
                //没有权限进直播间ion
                VLSLiveListModel *model = ws.showListArray[indexPath.row];
                VLSCourseModel *courseModel = [VLSCourseModel new];
                courseModel.courseId = [@([model.courseId integerValue]) stringValue];
                VLSCourseDetailViewController *vc = [[VLSCourseDetailViewController alloc] initWithNibName:@"VLSCourseDetailViewController" bundle:nil];
                vc.state = course_after;
                vc.liveModel = model;
                vc.model = courseModel;
                vc.hidesBottomBarWhenPushed = true;
                [ws.navigationController pushViewController:vc animated:YES];
            }
        }];
    };
    //[self.liveTableView.mj_header beginRefreshing];
    [self requestData];
}

- (void)setUpNavBar {
    
    UIImageView *titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_logo2"]];
    titleView.frame = CGRectMake(0, 0, 61, 20);
    self.parentViewController.navigationItem.titleView = titleView;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage ImageFromColor:[UIColor redColor]] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];

    UIImage *image = [[UIImage imageNamed:@"but_search_nor"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(searchAction:)];
    self.parentViewController.navigationItem.leftBarButtonItem = leftItem;
    
    VLSMessageImageView* messageView = [VLSMessageImageView shared];
    messageView.delegate = self;
    [messageView changeTintColor:RGB16(COLOR_BG_FFFFFF)];
    [messageView changeDotColor:RGB16(COLOR_BG_FFFFFF)];
    self.parentViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:messageView];
}

- (void)searchAction:(UIBarButtonItem *)item {
    // 搜索
    VLSSearchViewController *searchVC = [[VLSSearchViewController alloc] init];
    [self.navigationController pushViewController:searchVC animated:YES];
}
//加红点，收到新消息时调用
- (void)changeStationMailToAddDot{
    UIBarButtonItem *rightItem = self.parentViewController.navigationItem.rightBarButtonItem;
    UIView *cusView = [rightItem customView];
    
    dotImageView = [[UIImageView alloc] initWithFrame:CGRectMake(16,-4, 8, 8)];
    dotImageView.layer.cornerRadius = 4;
    dotImageView.clipsToBounds = YES;
    dotImageView.backgroundColor = [UIColor whiteColor];
    [cusView addSubview:dotImageView];
}


- (void)judgeDoubleUpHaveAppear {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (!app.isPopup) {
        [app getHeartConfig];
    }
}
- (void)oneTime{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self checkIsLivingWithModelArray:self.showListArray];
    });
}

- (void)requestDataWithChannel
{
    __weak VLSLiveViewController *weakVC = self;

    ManagerCallBack* callback = [[ManagerCallBack alloc] init];
    
    
    callback.updateBlock = ^(NSDictionary* result)
    {
        NSDictionary* dict = result;
        
        if (dict != nil)
        {
            if ([dict[@"list"] isKindOfClass: [NSArray class]])
            {
                [self.showListArray removeAllObjects];
                NSArray* array = dict[@"list"];
                [array enumerateObjectsUsingBlock:^(id  _Nonnull dict, NSUInteger idx, BOOL * _Nonnull stop)
                {
                    if(dict[@"roomId"] && ![dict[@"roomId"] isKindOfClass: [NSNull class]])
                    {
                        VLSHotModel* hotModel = [VLSHotModel mj_objectWithKeyValues: dict];
                        VLSLiveListModel *model = [[VLSLiveListModel alloc] initWidthHotModel: hotModel];
                        [self.showListArray addObject: model];
                    }
                    else if (dict[@"courseName"])
                    {
                        VLSCourseModel* model = [VLSCourseModel mj_objectWithKeyValues: dict];
                        model.courseId = dict[@"id"];
                        [self.showListArray addObject: model];
                    }
                    else if (dict[@"videoUrl"])
                    {
                        VLSPlaybackModel* model = [VLSPlaybackModel mj_objectWithKeyValues: dict];
                        [self.showListArray addObject: model];
                    }
                    else
                    {
                        VLSAdListModel *model = [[VLSAdListModel alloc] initWithDic: dict];
                        [self.showListArray addObject: model];

                    }
                }];
            }
            
//            NSArray* livingList = nil;
//            NSArray* courseList = nil;
//            NSArray *bannerList = nil;
//            if ([dict[@"living"] isKindOfClass: [NSArray class]])
//            {
//                [self.liveListsArray removeAllObjects];
//                livingList = dict[@"living"];
//                NSArray *hotModelList = [VLSHotModel mj_objectArrayWithKeyValuesArray: livingList];
//                for (VLSHotModel* hotModel in hotModelList)
//                {
//                    VLSLiveListModel *model = [[VLSLiveListModel alloc] initWidthHotModel: hotModel];
//                    [self.liveListsArray addObject: model];
//                }
//            }
//            if ([dict[@"course"] isKindOfClass: [NSArray class]])
//            {
//                [self.courseListArray removeAllObjects];
//                courseList = dict[@"course"];
//                for (NSDictionary* itemDict in courseList)
//                {
//                    VLSCourseModel* course = [VLSCourseModel mj_objectWithKeyValues: itemDict];
//                    course.courseId = itemDict[@"id"];
//                    [weakVC.courseListArray addObject: course];
//                }
//            }
//            if ([dict[@"banner"] isKindOfClass: [NSArray class]])
//            {
//                [weakVC.adListArray removeAllObjects];
//                bannerList = dict[@"banner"];
//                for (NSDictionary* itemDict in bannerList) {
//                    VLSAdListModel *model = [[VLSAdListModel alloc] initWithDic: itemDict];
//                    [weakVC.adListArray addObject:model];
//                }
//            }
        }
        
        [weakVC.liveTableView.mj_header endRefreshing];
        weakVC.isSeccess = YES;
        [weakVC loadDataList];
        
        [weakVC judgeDoubleUpHaveAppear];
        [weakVC.networkBreakView removeFromSuperview];
        NSLog(@"========> %d", self.channel.channelId);
    };
    
    callback.errorBlock = ^(id result){
        [weakVC.liveTableView.mj_header endRefreshing];
        ManagerEvent *event = result;
        if (event.type == ManagerEventExpired) {
            [weakVC error:result];
        } else {
            
            weakVC.networkBreakView.titleLabel.text = [NSString stringWithFormat:@"%@\n%@", LocalizedString(@"NETWORK_NOT_BEST"), LocalizedString(@"NETWORK_CLICK_LOAD")];
            weakVC.networkBreakView.frame = self.liveTableView.frame;
            [weakVC.view addSubview:weakVC.networkBreakView];
            
        }
    };
    [VLSLiveHttpManager getChannelContents: [NSString stringWithFormat: @"%@", self.channel.channelId] page: 0 number: 1000 callback: callback];
}

#pragma mark - 请求网络数据
- (void)requestData {
    
    if (self.channel != nil)
    {
        [self requestDataWithChannel];
        return;
    }
    __weak VLSLiveViewController *weakVC = self;
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [weakVC.liveTableView.mj_header endRefreshing];
        weakVC.isSeccess = YES;
        [weakVC.liveListsArray removeAllObjects];
        [weakVC.liveListsArray addObjectsFromArray:result];
        [weakVC judgeDoubleUpHaveAppear];
        [weakVC.networkBreakView removeFromSuperview];
        if (self.adListArray.count != 0) {
          }
        
        if (weakVC.isSeccess == YES && weakVC.isAdSeccess == YES) {
            [weakVC loadDataList];
            return ;
        }
        [weakVC loadDataList];
    };
    
    callback.errorBlock = ^(id result){
        [weakVC.liveTableView.mj_header endRefreshing];
        ManagerEvent *event = result;
        if (event.type == ManagerEventExpired) {
            [weakVC error:result];
        } else {
            
            weakVC.networkBreakView.titleLabel.text = [NSString stringWithFormat:@"%@\n%@", LocalizedString(@"NETWORK_NOT_BEST"), LocalizedString(@"NETWORK_CLICK_LOAD")];
            [weakVC.view addSubview:weakVC.networkBreakView];
            
        }
    };
//    [self.liveTableView.mj_header endRefreshing];
//    [self.liveTableView.mj_footer endRefreshing];
    [self.liveTableView reloadData];
    [VLSLiveHttpManager getLiveList:0 callback:callback WithSizeNumber:1000];
    [self requestCourseList];
    [self requestAdData];
}
//获取广告列表
- (void)requestAdData{
    __weak VLSLiveViewController *weakVC = self;
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self.adListArray removeAllObjects];
        [self.adListArray addObjectsFromArray:result];
        weakVC.isAdSeccess = YES;
        if (weakVC.isSeccess == YES && weakVC.isAdSeccess == YES) {
            [weakVC loadDataList];
        }
    };
    callback.errorBlock = ^(id result){
        
        ManagerEvent *event = result;
        if (event.type == ManagerEventExpired) {
            [self error:result];
        } else {
        
        }
    };
    [VLSLiveHttpManager getAdListCallback:callback];
}
//关闭广告
-(void)closeAdCellWithIndex:(HomeVLSAdTableViewCell*)cell {
    NSIndexPath* indexPath =  [self.liveTableView indexPathForCell: cell];
    if (indexPath != nil)
    {
        VLSAdListModel *model = self.showListArray[indexPath.row];
        [self.passListArray addObject:model];
        [self loadDataList];
    }

}

- (void)requestCourseList
{
    __weak VLSLiveViewController *weakVC = self;
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        if ([result isKindOfClass: [NSArray class]])
        {
            self.courseListArray = [result mutableCopy];
        }
        weakVC.isAdSeccess = YES;
        if (weakVC.isSeccess == YES && weakVC.isAdSeccess == YES) {
            [weakVC loadDataList];
        }
    };
    callback.errorBlock = ^(id result){
        
        ManagerEvent *event = result;
        if (event.type == ManagerEventExpired) {
            [self error:result];
        } else {
            
        }
    };
    [VLSLiveHttpManager getCourseListCallback: callback];
}

//处理数据
-(void)loadDataList
{
#if 0
    [self.showListArray removeAllObjects];
    //先将直播间列表加入展示数组
//    for (int i = 0; i < self.liveListsArray.count; i++) {
//        
//        [self.showListArray addObject:self.liveListsArray[i]];
//        
//    }
    NSMutableArray* tmp = [NSMutableArray array];
    [tmp addObjectsFromArray: self.liveListsArray];
    [tmp addObjectsFromArray: self.courseListArray];
    
    NSSortDescriptor* prioritySort = [NSSortDescriptor sortDescriptorWithKey: @"priority" ascending: NO];
    NSSortDescriptor* createTimeSort = [NSSortDescriptor sortDescriptorWithKey: @"createTime" ascending: NO];
    
    [tmp sortUsingDescriptors:(@[prioritySort, createTimeSort])];
    self.showListArray = tmp;
    
    //再添加广告列表到数组并且是根据需要的下标进行插入
    for (int i = 0; i < self.adListArray.count; i++) {
        
        VLSAdListModel *model = self.adListArray[i];
        //如果广告需求下标大于、等于数组元素个数，就不添加
        if ([model.position integerValue] >= self.showListArray.count) {
            [self.showListArray addObject: model];
        }else{
            //否则添加
            [self.showListArray insertObject:model atIndex:[model.position integerValue]];
        }
        
        NSArray *tempShowListArray = [self.showListArray copy];
        //添加之后先看过滤黑名单数组中有没有被用户屏蔽掉的广告，双循环对比
        for (VLSAdListModel *model in self.passListArray) {
            
            for (int i = 0; i < tempShowListArray.count; i++) {
                //如果是广告Model
                if ([tempShowListArray[i] isKindOfClass:[VLSAdListModel class]]) {
                    //先获取
                    VLSAdListModel *m = tempShowListArray[i];
                    //如果id一样，说明是被过滤的广告，就删除，最终生成新列表
                    if ([model.adID isEqualToString:m.adID]) {
                        
                        [self.showListArray removeObject:m];
                        
                    }
                }
                
            }
        }
    }
#endif
    [self.liveTableView reloadData];
    [self oneTime];
}


- (void)checkIsLivingWithModelArray:(NSArray *)modelArray{
    NSString *userId = [[VLSReConnectionManager sharedManager] getAnchorID];
    NSString *roomId = [[VLSReConnectionManager sharedManager] getRoomID];
    for (int i = 0; i < modelArray.count; i++) {
        if ([modelArray[i] isKindOfClass:[VLSLiveListModel class]]) {
            VLSLiveListModel *model = modelArray[i];
            if ([model.host isEqualToString:userId] && [model.roomId isEqualToString:roomId]) {
                //富文本属性
                NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"ALERT_MESSAGE_LIVINGALERT") attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16],NSForegroundColorAttributeName:[UIColor blackColor]}];
                //富文本属性
                NSMutableAttributedString *attributedMessage= [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"ALERT_MESSAGE_ANCHOOUT") attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:RGB16(COLOR_FONT_888888)}];
                
                // 在 viewDidLoad 中创建
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:attributedTitle.string message:attributedMessage.string preferredStyle:UIAlertControllerStyleAlert];
                // 用 KVC 修改其 没有暴露出来的
                [alertVC setValue:attributedTitle forKey:@"attributedTitle"];
                [alertVC setValue:attributedMessage forKey:@"attributedMessage"];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[VLSReConnectionManager sharedManager] deleteRoomIDandAnchorID];                    
                }];
                [cancelAction setValue:[UIColor blackColor] forKey:@"_titleTextColor"];
                [cancelAction setValue:[UIFont systemFontOfSize:14] forKey:@"titleFont"];

                [alertVC addAction:cancelAction];
                
                UIAlertAction *goOnAction = [UIAlertAction actionWithTitle:LocalizedString(@"GOON") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self intoAnchorVC:model];
                }];
                [goOnAction setValue:[UIColor blackColor] forKey:@"_titleTextColor"];
                [goOnAction setValue:[UIFont systemFontOfSize:14] forKey:@"titleFont"];
                [alertVC addAction:goOnAction];
                [self presentViewController:alertVC animated:YES completion:nil];
            }
        }
    }
}
#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showListArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id item = self.showListArray[indexPath.row];
    if ([item isKindOfClass: [VLSCourseModel class]])
    {
        LiveTableCourseCell* cell = (LiveTableCourseCell*)[tableView dequeueReusableCellWithIdentifier: @"CourseCell" forIndexPath: indexPath];
        cell.objcDataModel = item;
        return cell;
    }
    else if ([item isKindOfClass: [VLSPlaybackModel class]])
    {
        LiveTableCourseCell* cell = (LiveTableCourseCell*)[tableView dequeueReusableCellWithIdentifier: @"PlaybackCell" forIndexPath: indexPath];
        cell.objcDataModel = item;
        return cell;
    }
    
    if ([self.showListArray[indexPath.row] isKindOfClass:[VLSAdListModel class]]) {
        HomeVLSAdTableViewCell *cell1 = [tableView
                                         dequeueReusableCellWithIdentifier:@"AdCell"];
        if (cell1 == nil) {
            cell1 = [[HomeVLSAdTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AdCell"];
        }
        cell1.delegate = self;
        cell1.model = self.showListArray[indexPath.row];
        return cell1;
    }else{
        HomeVLSHotCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotCell"];
        if (cell == nil) {
            cell = [[HomeVLSHotCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HotCell"];
        }
        [cell setLiveListModel:self.showListArray[indexPath.row]];
        if (indexPath.row == self.showListArray.count - 1) {
            cell.lines.hidden = YES;
        }else{
            cell.lines.hidden = NO;
        }
        return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return SCREEN_HEIGHT / 2.375;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    NSString __block * action = @"";
    if ([self.showListArray[indexPath.row] isKindOfClass:[VLSAdListModel class]]) {
        VLSAdListModel *model = self.showListArray[indexPath.row];
        [self appInnerRounting:[model.outerUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        action = @"ClickAd";
    }else if ([self.showListArray[indexPath.row] isKindOfClass:[VLSCourseModel class]])
    {
        action = @"ClickCourse";
        VLSCourseDetailViewController *vc = [[VLSCourseDetailViewController alloc] initWithNibName:@"VLSCourseDetailViewController" bundle:nil];
        vc.state = course_before;
        vc.model = self.showListArray[indexPath.row];
        vc.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([self.showListArray[indexPath.row] isKindOfClass:[VLSPlaybackModel class]])
    {
        VLSPlaybackModel* playback = self.showListArray[indexPath.row];
        if ([playback.videoUrl length] > 0)
        {
            AVPlayerViewController* playerController = [[AVPlayerViewController alloc] init];
            playerController.player = [AVPlayer playerWithURL: [NSURL URLWithString: playback.videoUrl]];
            [self presentViewController: playerController animated: YES completion:^
             {
                 [playerController.player play];
             }];
        }

        
    }
    else
    {
        if ([self isCelluar]) {
            [UIAlertController showAlertInViewController:self withTitle:LocalizedString(@"LIVEBEFORE_REMIND") message:LocalizedString(@"LIVEBEFORE_ANCHORNO_WIFI") cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"LIVEBEFORE_I_KONWEN")] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                action = @"ClickLiveRoom";
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                self.indpath = indexPath;
                [self checkSetting];
                [self unableCanEnterRoom];
            }];
        }else {
            action = @"ClickLiveRoom";
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            self.indpath = indexPath;
            [self checkSetting];
            [self unableCanEnterRoom];
        }
    }
    [[VLSUserTrackingManager shareManager] trackingClickWithAction: action extraInfo: @{}];
}

/**
 *  banner 内部路由跳转
 *  outerUrl 广告链接 appInnerRouting?pageVC=Profile&uid=6000049 为内部跳转
 */
- (void)appInnerRounting:(NSString *)outerUrl
{
    if(outerUrl != nil && ![outerUrl isEqualToString:@""]) {
        if([outerUrl containsString:@"appInnerRouting?pageVC="]) {//代表为app内部路由
            NSRange pageRange = [outerUrl rangeOfString:@"pageVC="];
            NSRange uidRange = [outerUrl rangeOfString:@"&uid="];
            //获取需要内部跳转的VC，通过比较跳转不同的VC
            NSString* pageVC = [[outerUrl substringWithRange:NSMakeRange(pageRange.location + pageRange.length, uidRange.location - pageRange.location - pageRange.length)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            //获取主播id
            NSString* uid = [[outerUrl substringWithRange:NSMakeRange(uidRange.location + uidRange.length, outerUrl.length - uidRange.location - uidRange.length)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if(pageVC != nil && [pageVC isEqualToString:@"Profile"] && uid != nil && ![uid isEqualToString:@""]) {//代表跳转到主播个人简介中
                UserProfileViewController *vlsVC = [UserProfileViewController create];
                vlsVC.hidesBottomBarWhenPushed = YES;
                vlsVC.userId = uid;
                [[VLSUserTrackingManager shareManager] trackingViewprofile:uid];
                [self.navigationController pushViewController:vlsVC animated:YES];
            }
        } else {//直接跳转外部链接
            VLSMKWebViewController *vc = [[VLSMKWebViewController alloc]init];
            vc.hidesBottomBarWhenPushed = YES;
            vc.Url = outerUrl;
            [TalkingData trackEvent: @"openOuterLink"];
            [self.navigationController pushViewController:vc animated:YES];
            [vc.navigationController.navigationBar useWhiteTheme];
        }
    }
}

/** 以下内容为导航栏渐隐效果，勿删 */
//#pragma mark - UIScrollViewDelegate
//
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetY = scrollView.contentOffset.y;
    if (self.pagerViewController != nil
        && self.pagerViewController.selectedViewController != self
        )
    {
        self.lastOffset = offsetY;
        return;
    }
    
    BOOL isUpwards = (offsetY - self.lastOffset) > 0;
    if (isUpwards
        && offsetY > 64
        && scrollView.contentSize.height > CGRectGetHeight(scrollView.frame) + 20
        )
    {
        [self.parentViewController.navigationController setNavigationBarHidden: YES animated: NO];
        //[UIView animateWithDuration: 0.3 animations:^
        //{
            if (self.pagerViewController.viewPagerConstraints.count > 0)
            {
                self.pagerViewController.viewPagerConstraints[0].constant = [[UIApplication sharedApplication] statusBarFrame].size.height;
            }
            [self.pagerViewController.view setNeedsLayout];
        //}];

        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleDefault];

        //[[UIApplication sharedApplication] setStatusBarHidden: YES withAnimation: YES];
    }
    else if (!isUpwards && scrollView.isDragging
             && ((offsetY + 1 < scrollView.contentSize.height - CGRectGetHeight(scrollView.frame)) || scrollView.contentSize.height + 64 < CGRectGetHeight(scrollView.frame))
             
             )
    {
        [self.parentViewController.navigationController setNavigationBarHidden: NO animated: NO];
        //[UIView animateWithDuration: 0.3 animations:^
        // {
             if (self.pagerViewController.viewPagerConstraints.count > 0)
             {
                 self.pagerViewController.viewPagerConstraints[0].constant = 0;
                 [self.pagerViewController.view setNeedsLayout];
                 [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
             }
         //}];

        //[[UIApplication sharedApplication] setStatusBarHidden: NO withAnimation: YES];
    }
    
    
    self.lastOffset = offsetY;

}
//
//- (void)setNavigationBarTransformProgress:(CGFloat)progress
//{
//    [self.navigationController.navigationBar lt_setTranslationY:(-44 * progress)];
//    [self.navigationController.navigationBar lt_setElementsAlpha:(progress)];
//}



//判断相机 声音是否打开
- (void)checkSetting
{
    if (!self.canEnterRoom) {
        return;
    }
    AVAudioSessionRecordPermission audioStatus = [AVAudioSession sharedInstance].recordPermission;
    switch (audioStatus) {
        case AVAudioSessionRecordPermissionUndetermined:{
            [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                if (granted) {
                    [self requestCapture];
                }
                else {
                    [self chooseSetting];
                }
            }];
        }
            break;
        case AVAudioSessionRecordPermissionDenied:{
            [self chooseSetting];
        }
            break;
        case AVAudioSessionRecordPermissionGranted:{
            [self requestCapture];
        }
            break;
        default:
            break;
    }
    
}

// 请求相机权限
- (void)requestCapture
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (status) {
        case AVAuthorizationStatusNotDetermined:{
            // 许可对话没有出现，发起授权许可
            
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                
                if (granted) {
                    
                    [self ToTheLiveShow:self.indpath];
                    //第一次用户接受
                    
                    
                }else{
                    [self chooseSetting];
                }
            }];
            break;
        }
        case AVAuthorizationStatusAuthorized:{
            [self ToTheLiveShow:self.indpath];
            // 已经开启授权，可继续
            
            break;
        }
        case AVAuthorizationStatusDenied:{
            
            [self chooseSetting];
            break;
        }
            
            
        case AVAuthorizationStatusRestricted:
            // 用户明确地拒绝授权，或者相机设备无法访问
        {
            [self chooseSetting];
            break;
            
        }
        default:
            break;
    }
}


//弹窗提醒用户进入设置页面
- (void)chooseSetting
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"LIVEBEFORE_STARTPLAY") message:LocalizedString(@"LIVEBEFORE_OPENCAMMANDMIC") preferredStyle:UIAlertControllerStyleAlert];
    //修改message
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"LIVEBEFORE_OPENCAMMANDMIC")];
    
    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(0, 11)];
    [alertController setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    
    // 取消按钮
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"INVITE_CANCEL") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self enableCanEnterRoom];
    }];
    
    // 继续按钮
    UIAlertAction *openAction = [UIAlertAction actionWithTitle:LocalizedString(@"LIVEBEFORE_GOOPEN") style:UIAlertActionStyleDefault handler: ^(UIAlertAction * _Nonnull action) {
        
        NSURL * url1 = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//        NSURL *url = [NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"];
        if ([[UIApplication sharedApplication] canOpenURL:url1])
        {
            [[UIApplication sharedApplication] openURL:url1];
        }
        
        [self enableCanEnterRoom];
    }];
    
    //    [openAction setValue:[UIFont systemFontOfSize:20] forKey:@"titleFont"];
    [alertController addAction:cancelAction];
    [alertController addAction:openAction];
//    dispatch_async(dispatch_get_main_queue(), ^{
        // code here
        [self presentViewController:alertController animated:YES completion:nil];
//    });
}


#pragma mark - VLSNetworkBreakViewDelegate

- (void)requestLiveListDataAgain {
    [[VLSUserTrackingManager shareManager] trackingClickWithAction: @"ClickReloadPage" extraInfo: @{}];
    [self requestData];
}

#pragma mark 点击进入直播间方法
- (void)ToTheLiveShow:(NSIndexPath *)indexPath
{
//    // 在此判断 IM 是否已经登录，如果登录成功进行，再进行跳转
    if (![[VLSMessageManager sharedManager] GetLoginState]) {
        [[VLSMessageManager sharedManager] IMLogin];//重连IM
    }
    
    VLSLiveListModel *model = self.showListArray[indexPath.row];
    AccountModel *account = [AccountManager sharedAccountManager].account;
    if (account.isSuperUser) {
        if (model.courseId) {
            self.enterLiveBlock(model.courseId, indexPath);
        }else {
        VLSAdminViewController *admin = [[VLSAdminViewController alloc] init];
        admin.orientation = [model.orientation isEqualToNumber:@2] ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
        admin.anchorId = model.host;
        admin.roomId = model.roomId;
        admin.hidesBottomBarWhenPushed = YES;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            if (self.navigationController.viewControllers.count > 1) {
//                return ;
//            }
            [self.navigationController pushViewController:admin animated:YES];
//        });
        }
    }else{
        if (account.uid == [model.host integerValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                [self intoAnchorVC:model];

            });
        }else{
            if (model.courseId) {
                //课程直播间
                self.enterLiveBlock(model.courseId, indexPath);
            }else {
                
                // 横屏直播间不支持手势滑动
                if (model.orientation.integerValue == 2)
                {
                    VLSShowViewController *show = [[VLSShowViewController alloc] init];
                    show.anchorId = model.host;
                    show.roomId = model.roomId;
                    show.orientation = [model.orientation isEqualToNumber:@2] ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
                    show.courseId = model.courseId;
                    show.lessonId = model.lessonId;
                    
                    VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
                    joinRoom.roomID = model.roomId;
                    joinRoom.joinType = @"living_list";
                    show.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];
                    
                    show.hidesBottomBarWhenPushed = YES;
                    //                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    // code here
                    //                    if (self.navigationController.viewControllers.count > 1) {
                    //                        return ;
                    //                    }
                    [self.navigationController pushViewController:show animated:NO];
                    //                });
                }
                else //普通直播间
                {
                    __block NSMutableArray* liveRooms = [NSMutableArray array];
                    __block NSInteger adjustedIndex = 0;
                    [self.showListArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
                     {
                         if ([obj isKindOfClass: [VLSLiveListModel class]])
                         {
                             VLSLiveListModel* liveModel = obj;
                             
                             // 课程直播和横屏直播不支持上下滑动
                             if ([liveModel.courseId length] > 0
                                 || liveModel.orientation.integerValue == 2
                                 || [liveModel.host integerValue] == account.uid
                                 )
                             {
                                 return;
                             }
                
                             if (idx == indexPath.row)
                             {
                                 adjustedIndex = liveRooms.count;
                             }
                             [liveRooms addObject: liveModel];
                             
                         }
                     }];
                    LiveSwipeViewController* vc = [[UIStoryboard storyboardWithName: @"LiveRoomSwipe" bundle: nil] instantiateInitialViewController];
                    vc.liveRooms = liveRooms;
                    vc.currentLiveRoomIndex = adjustedIndex;
                    
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController: vc animated: YES];
                }
            }
        }
    }
}

#pragma mark - 开始先做网络监测
- (BOOL)isCelluar {
    BOOL isWIfi = [AFNetworkReachabilityManager sharedManager].isReachableViaWiFi;
    BOOL iswan = [AFNetworkReachabilityManager sharedManager].isReachableViaWWAN;
    BOOL is3G = [[NSUserDefaults standardUserDefaults] boolForKey:@"is3G"];
    if (isWIfi) {
        return NO;
    }else if (is3G == NO&&isWIfi == NO) {
        return YES;
    }else{
        return NO;
    }
}

- (NSMutableArray *)liveListsArray {
    if (!_liveListsArray) {
        self.liveListsArray = [NSMutableArray array];
    }
    return _liveListsArray;
}


- (UIView *)networkBreakView {
    if (!_networkBreakView) {
        self.networkBreakView = [[VLSNetworkBreakView alloc] initWithFrame:self.view.bounds];
        self.networkBreakView.delegate = self;
    }
    return _networkBreakView;
}

#pragma mark - VLSMessageImageViewDelegate
- (void)message
{
    VLSMessageViewController *messageVC = [[VLSMessageViewController alloc] init];
    messageVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:messageVC animated:YES];
}

- (void)intoAnchorVC:(VLSLiveListModel *)model
{
    VLSAnchorViewController *anchorVC = [[VLSAnchorViewController alloc] init];
    [VLSIMManager sharedInstance].RoomID = model.roomId;
    anchorVC.actionType = ActionTypeAnchor;
    if ([model.lessonId length] > 0)
    {
        anchorVC.actionType = ActionTypeLessonTeacher;
    }
    anchorVC.roomId = model.roomId;
    anchorVC.anchorId = model.host;
    anchorVC.lessonId = model.lessonId;
    anchorVC.courseId = model.courseId;
    anchorVC.isReconnection = YES;
    anchorVC.title = model.title;
    VLSNavigationController *nav = [[VLSNavigationController alloc] initWithRootViewController:anchorVC];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark 添加监听者

- (void)addlistener
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(listenerMethod:) name:@"JoinRoom" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(maillistener) name:@"mail" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enableCanEnterRoom) name:ENABLEENTERLIVEROOM object:nil];
}

- (void)enableCanEnterRoom {
    self.canEnterRoom = YES;
}

- (void)unableCanEnterRoom {
    self.canEnterRoom = NO;
}

- (void)listenerMethod:(NSNotification *)notifi
{
    if ([self tababrIndex]
        || self.pagerViewController.subViewControllers[0] != self
        ) {
        // 用户在直播
        return ;
    }
    
    NSDictionary *dict = notifi.userInfo;
    NSString *roomId = dict[@"roomID"];
    NSString *anchorId ;
    
    id longhost = dict[@"anchorID"];
    if ([longhost isKindOfClass:[NSNumber class]]) {
        
        anchorId = [NSString stringWithFormat:@"%ld",(long)[longhost integerValue]];
    }else{
        anchorId= longhost;
    }
    VLSShowViewController *show = [[VLSShowViewController alloc] init];
    show.anchorId = anchorId;
    show.roomId =roomId;
//    show.orientation = model.orientation;
    
    VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
    joinRoom.roomID = roomId;
    joinRoom.joinType = @"living_list";
    show.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];
    
    show.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:show animated:YES];
}

- (void)maillistener
{
    if ([self tababrIndex]
        || self.pagerViewController.subViewControllers[0] != self
        ) {
        // 用户在直播
        return ;
    }
    [self.navigationController popToRootViewControllerAnimated:NO];
    VLSMessageViewController *messageVC = [[VLSMessageViewController alloc] init];
    messageVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:messageVC animated:YES];
}

// 收到推送通知，tabbar做一些调整
- (BOOL)tababrIndex
{
    
    NSString *userid = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
    if ([[VLSMessageManager sharedManager] checkUserIsLiving:userid]) {
        // 当前用户正在直播
        [MBProgressHUD showError:LocalizedString(@"IS_LIVING") toView:nil];
        return YES;
    }else{
        // 当前用户不在直播
        self.tabBarController.selectedIndex = 0;
        return NO;
    }
}

// 未启动状态下的推送跳转
- (void)remoteInfoHandle
{
    AppDelegate *appdele = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSDictionary *userInfo = appdele.remoteInfo;
    appdele.remoteInfo = nil;
    if (!userInfo) {
        // 如果没有说明不是点击远程推送进来的
        return;
    }
    NSLog(@"userinfo:%@",userInfo);
    NSLog(@"收到推送消息:%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]);
    
    
    
    // 收到IM推送消息
    NSAssert(userInfo[@"ext"], @"推送消息没有扩展字段");
    NSString *extstr = userInfo[@"ext"];
    //    [MBProgressHUD showMessag:extstr toView:self.window];
    
    NSArray* array = [extstr componentsSeparatedByString:VLS_SEPERATOR];
    
    if (array.count == 1) {
        //        [self toTheMail];
        [self maillistener];
    }else{
        
        NSString *dictstr = [extstr substringFromIndex:4];
        NSDictionary *dict = [VLSMessageManager parseJSONStringToNSDictionary:dictstr];
        NSAssert(dict, @"字典解析错误");
        int cmd = [array[0] intValue];
        if (cmd == 202) {

            // 进入直播间
            NSString *roomId = dict[@"roomID"];
            NSString *anchorId ;
            
            id longhost = dict[@"anchorID"];
            if ([longhost isKindOfClass:[NSNumber class]]) {
                
                anchorId = [NSString stringWithFormat:@"%ld",(long)[longhost integerValue]];
            }else{
                anchorId= longhost;
            }
            VLSShowViewController *show = [[VLSShowViewController alloc] init];
            show.anchorId = anchorId;
            show.roomId =roomId;
//            show.orientation = model.orientation;
            
            VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
            joinRoom.roomID = roomId;
            joinRoom.joinType = @"living_list";
            show.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];
            
            show.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:show animated:YES];
        }
    }
    
}

- (void)recoverNaviBarStatus
{
    if (self.pagerViewController.selectedViewController != self)
    {
        return;
    }
    BOOL showNavi = self.pagerViewController.viewPagerConstraints[0].constant == 0;
    [self.navigationController setNavigationBarHidden: !showNavi];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onTimer
{
    if (self.pagerViewController != nil
        && self.pagerViewController.selectedViewController != self
        )
    {
        return;
    }
   [self requestData];
}

- (void)adjustTabbarByWalkAroundIfSystemVersionEqualTO_8_4_X
{
    NSString* systemVersion = [UIDevice currentDevice].systemVersion;
    if ([systemVersion compare: @"8.4" options: NSNumericSearch] >= NSOrderedSame
        && [systemVersion compare: @"9.0" options: NSNumericSearch] == NSOrderedAscending)
    {
        self.tabBarController.tabBar.frame = CGRectMake(0, CGRectGetHeight(self.tabBarController.view.frame) - 44, CGRectGetWidth(self.tabBarController.view.frame), 44);
    }
}
@end
