//
//  VLSMKWebViewController.m
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMKWebViewController.h"
#import "ShareActionSheet.h"
#import "VLiveShow-Swift.h"
#import "AccountManager.h"
#import "Masonry.h"

@interface VLSShareItem : NSObject
@property(nonatomic, copy) NSString* url;
@property(nonatomic, copy) NSString* imageURL;
@property(nonatomic, copy) NSString* title;
@end

@implementation VLSShareItem

@end

@interface VLSMKWebViewController ()<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler, ShareActionSheetDelegate>
@property(nonatomic, strong) VLSShareItem* shareItem;
@property(nonatomic, strong) ShareActionSheet* shareActionSheet;
@property(nonatomic, strong) UIActivityIndicatorView *actView;
@end

@implementation VLSMKWebViewController

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.tintColor = RGB16(COLOR_FONT_4A4A4A);
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    if (self.pagerViewController == nil)
    {
        [self.navigationController setNavigationBarHidden: NO animated: YES];
    }
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.fullScreenMode)
    {
        self.edgesForExtendedLayout = UIRectEdgeAll;
        self.extendedLayoutIncludesOpaqueBars = YES;
        self.automaticallyAdjustsScrollViewInsets = NO;
        [self.navigationController.navigationBar useFusionTheme];
    }

    
    //self.Url = @"http://192.168.121.129:3000/missuniverse/";
    if (![self.Url containsString: @"://"])
    {
        self.Url  = [@"http://" stringByAppendingString: self.Url];
    }
    NSURL* url = [NSURL URLWithString: self.Url];
    
//    NSString* token = [AccountManager sharedAccountManager].account.token;
//    if ([token length] > 0
//        && [url.host length] > 0
//        )
//    {
//        NSDictionary* properties = @{
//                                     NSHTTPCookieName: @"token",
//                                     NSHTTPCookieValue: token,
//                                     NSHTTPCookieDomain: url.host,
//                                     NSHTTPCookieOriginURL: url.host
//                                     };
//        NSHTTPCookie* cookie = [NSHTTPCookie cookieWithProperties: properties];
//        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie: cookie];
//    }
    
    // Do any additional setup after loading the view.
    
    //创建配置对象
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc]init];
    //设置偏好设置 (根据需要设置)
    config.preferences = [[WKPreferences alloc] init];
    //最小字体 默认为0
    config.preferences.minimumFontSize = 12;
    //设置JS交互是否可用 默认为YES
    config.preferences.javaScriptEnabled = YES;
    //在iOS系统下默认为NO(不能自动通过窗口打开) 在OS X下默认为YES
    config.preferences.javaScriptCanOpenWindowsAutomatically = NO;
    //内容处理池 苹果没有公开对应的属性和方法 不需要配置
    config.processPool = [[WKProcessPool alloc] init];
    //WKUserContentController适用于给js注册对象的，注册对象之后，js就可以使用
    config.userContentController = [[WKUserContentController alloc] init];
    //设置代理 添加一个名称 js段就可以通过这个名称发送消息
    [config.userContentController addScriptMessageHandler:self name:@"myapp"];
    
    //初始化
    self.WKWebView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:config];
    //加载网页
    [_WKWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.Url] cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval: 30]];
    //设置代理
    //导航代理
    _WKWebView.navigationDelegate = self;
    //UI交互代理
    _WKWebView.UIDelegate = self;
    [self.view addSubview:_WKWebView];
    
    [_WKWebView mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.edges.equalTo(self.view);
    }];
    
    _WKWebView.opaque = NO;
    _WKWebView.backgroundColor = [UIColor whiteColor];
    _WKWebView.scrollView.backgroundColor = [UIColor whiteColor];
    
    //添加对webView属性的监测
    [_WKWebView addObserver:self forKeyPath:@"loading" options:(NSKeyValueObservingOptionNew) context:nil];
    [_WKWebView addObserver:self forKeyPath:@"title" options:(NSKeyValueObservingOptionNew) context:nil];
    [_WKWebView addObserver:self forKeyPath:@"estimatedProgress" options:(NSKeyValueObservingOptionNew) context:nil];
    UIBarButtonItem *left = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"ic_web_back.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal] style:0 target:self action:@selector(didBackButtonPress)];
    self.navigationItem.leftBarButtonItem = left;
    left.tag = 10036;
    UIImage* shareImage = [[UIImage imageNamed: @"ic_web_share"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithImage: shareImage landscapeImagePhone: shareImage style: UIBarButtonItemStylePlain target: self action: @selector(didClickShareButton)];
    item.tintColor = [UIColor clearColor];
    [self.navigationItem setRightBarButtonItem: item];
    [self.WKWebView sizeToFit];
    
    self.actView = [[UIActivityIndicatorView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.actView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.view addSubview:self.actView];
    [self.actView mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.center.equalTo(self.view);
    }];
    [self.actView startAnimating];
}

-(void)back{
    [self.WKWebView removeObserver: self forKeyPath: @"loading" context: nil];
    [self.WKWebView removeObserver: self forKeyPath: @"title" context: nil];
    [self.WKWebView removeObserver: self forKeyPath: @"estimatedProgress" context: nil];
    [self.navigationController popViewControllerAnimated:YES];
}
// KVO 在loading完成之后可以注入一些js到web中
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        NSLog(@"%f",self.WKWebView.estimatedProgress);
    }else if ([keyPath isEqualToString:@"loading"]){
        if (self.WKWebView.loading) {
            NSLog(@"网页加载完毕...");
        }
        [self.actView stopAnimating];
    }else if ([keyPath isEqualToString:@"title"]){

    }
}


//三类协议的代理方法
-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    NSURL *url = navigationAction.request.URL;
    NSString *urlStr = navigationAction.request.URL.absoluteString;
    NSString *host = url.host;//域名
    if ([urlStr hasPrefix: @"vliveshow://share"])
    {
        self.shareItem = [[VLSShareItem alloc] init];
        NSURLComponents* components = [NSURLComponents componentsWithString: urlStr];
        [components.queryItems enumerateObjectsUsingBlock:^(NSURLQueryItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            if([obj.name isEqualToString: @"title"])
            {
                self.shareItem.title = obj.value;
            }
            else if ([obj.name isEqualToString: @"url"])
            {
                self.shareItem.url = obj.value;
            }
            else if ([obj.name isEqualToString: @"image"])
            {
                self.shareItem.imageURL = obj.value;
            }
        }];
        
        if ([self.shareItem.title length] > 0
            && [self.shareItem.imageURL length] > 0
            && [self.shareItem.url length] > 0
            )
        {
            [self showShareSheet];
        }
        decisionHandler(WKNavigationActionPolicyCancel);
    }
    else if ([urlStr hasPrefix: @"vliveshow://query_token"])
    {
        NSString* token = [[AccountManager sharedAccountManager] account].token;
        if ([webView.URL.host hasSuffix: @"vliveshow.com"])
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSString* jsString = [NSString stringWithFormat: @"actVm.setToken(\"%@\");", [token length] > 0 ? token: @"null"];
                [self.WKWebView evaluateJavaScript: jsString completionHandler:^(id _Nullable ret, NSError * _Nullable error)
                 {
                     
                 }];
            });
        }
        decisionHandler(WKNavigationActionPolicyCancel);
    }
    else if (navigationAction.navigationType == WKNavigationTypeLinkActivated && ![host containsString:@"vliveshow.com"]) {
        //跨域处理 手动打开
        [[UIApplication sharedApplication] openURL:url];
        //禁止web内跳转访问
        decisionHandler(WKNavigationActionPolicyCancel);
    }else{
        //允许打开
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}


//响应完成时
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    decisionHandler(WKNavigationActionPolicyAllow);
}


/**
 *  JS调用alert函数时。会触发该代理方法
 *
 *  @param webView           webView 对象
 *  @param message           传递的数据
 *  @param frame             frame
 *  @param completionHandler 在原生得到结果后回调给JS
 */
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"alert" message:message preferredStyle:(UIAlertControllerStyleAlert)];
    /**确定*/
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }]];
    [self presentViewController:alert animated:YES completion:nil];
    
}


//当js端通过myapp发送消息到iOS原生应用端时，会触发该方法 对应消息会在该方法中接收到
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    //message的类型支持 NSString NSNumber NSArray NSDate NSNULL
    //将JS发送的消息接收到传递给后一界面
    
}

//JS调用confirm函数时，会触发该代理方法
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    
}


/**
 *  JS调用prompt函数时，会触发该代理方法
 *
 *  @param webView           webView 对象
 *  @param prompt            prompt
 *  @param defaultText       defaultText
 *  @param frame             frame
 *  @param completionHandler 在原生应用 回调给js端
 */
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)xs_actionSheet:(ShareActionSheet *)actionSheet clickedButtonIndex:(NSInteger)index
{
    if (index > 5 || ![ShareActionSheet isInstallation: index]) {
        [self showAlterviewController: LocalizedString(@"SHARE_HTML_NO_INSTALL_APP")];
    } else {
        if (self.shareItem != nil) {
            NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSURL URLWithString: self.shareItem.imageURL],      @"coverImageURL",
                                          self.Url,           @"content",
                                          self.shareItem.title, @"title",
                                          @YES,         @"overrideTitleInWXTimeline",
                                          self.shareItem.url, @"urlString",
                                          nil];
            [self shareToPlatform:index messageObject:dict completion:^(NSDictionary *item) {
                [actionSheet dismiss];
            }];
        } else {
            NSURL* iconURL = [[NSBundle mainBundle] URLForResource:@"AppIcon60x60@3x" withExtension: @"png"];
            NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          iconURL.absoluteString,      @"coverImageURL",
                                          self.Url,           @"content",
                                          self.WKWebView.title, @"title",
                                          @NO,         @"overrideTitleInWXTimeline",
                                          self.Url, @"urlString",
                                          nil];
            [self shareToPlatform:index messageObject:dict completion:^(NSDictionary *item) {
                [actionSheet dismiss];
            }];
        }
    }
}

- (void)dismissActionSheet:(ShareActionSheet *)actionSheet
{
    [actionSheet dismiss];
}

- (void)didClickShareButton
{
    [self.WKWebView evaluateJavaScript: @"window.share()" completionHandler:^(id item, NSError * _Nullable error) {
        if (error != nil)
        {
            [self showShareSheet];
        }
    }];
}

- (void)showShareSheet
{
    NSArray * images = @[@"broadcast-share-qq",@"broadcast-share-zone",@"broadcast-share-weixin",@"share_moments",@"weibo01"];
    self.shareActionSheet = [[ShareActionSheet alloc] initWithFrame:CGRectMake(0, 0 , SCREEN_WIDTH, 258) delegate: self images:images];
    [self.shareActionSheet addTarget: self action: @selector(dismissActionSheet:) forControlEvents: UIControlEventTouchUpInside];
    [self.shareActionSheet showInView: self.view];
}

- (void)didBackButtonPress
{
    if ([self.WKWebView canGoBack])
    {
        [self.WKWebView goBack];
    }
    else
    {
        [self back];
    }
}

@end
