
//
//  LiveSwipeViewController.swift
//  VLiveShow
//
//  Created by VincentX on 16/02/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

import UIKit

enum LiveSwipeAnimationStatus
{
    case none
    case transition
    case backToTop
    case backToBottom
}

class LiveSwipeViewController: UIViewController, UIDynamicAnimatorDelegate, UICollisionBehaviorDelegate {
    
    static let SwipeThreshold: CGFloat = 100
    
    @objc var liveRooms: [VLSLiveListModel] = []
    @objc var currentLiveRoomIndex: Int = 0
    
    @IBOutlet var panGensture: UIPanGestureRecognizer!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var gravity: UIGravityBehavior!
    var collision: UICollisionBehavior!
    var animator: UIDynamicAnimator!
    var attachBehavior: UIAttachmentBehavior!
    var lastPoint: CGPoint?
    var animationStatus: LiveSwipeAnimationStatus = .none
    
    var currentPanVelocity: CGPoint?
    
    weak var currentVC: VLSShowViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.loadCurrent()

        
        
//        let motionEffect = UIAttachmentBehavior
//        self.view.addMotionEffect(UIMotionEffect)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didSwipe(_ gesture: UIGestureRecognizer)
    {
        guard let panGesture = gesture as? UIPanGestureRecognizer,
        let view = panGesture.view
        else
        {
            return
        }
        let velocity = panGensture.velocity(in: self.view)
        let location = panGensture.location(in: self.view)

        if velocity.y == 0
        {
            return
        }
        guard let last = lastPoint else
        {
            self.lastPoint = location
            return
        }
        let y = view.frame.origin.y
        let newY = y + (location.y - last.y)
        
        
        let isToSwipeNext = (newY < 0)
        if gesture.state == .ended
            || gesture.state == .cancelled
            || gesture.state == .failed
        {
            decideRoomTrasition(view: view, isToSwipeNext: isToSwipeNext)
            self.lastPoint = nil
            return
        }
        
        self.currentPanVelocity = CGPoint(x: 0, y: velocity.y)
        
        
        var hasRoom = false
        if (isToSwipeNext)
        {
            hasRoom = hasNextRoom()
            showNextBackground()
        }
        else
        {
            hasRoom = hasPreviousRoom()
            showPreviousBackground()
        }

        var center = view.center
        center.y = center.y + (location.y - last.y)
        view.center = center
        
        self.lastPoint = location
        
        if !hasRoom
        {
            showDefaultBackground()
            if isToSwipeNext && (view.frame.origin.y >= -LiveSwipeViewController.SwipeThreshold)
            {
                backToBottom()
            }
            else if !isToSwipeNext && (view.frame.origin.y <= LiveSwipeViewController.SwipeThreshold)
            {
                backToTop()
            }
        }
    }
    
    func decideRoomTrasition(view: UIView, isToSwipeNext: Bool)
    {
        if isToSwipeNext
        {
            if (view.frame.origin.y < -LiveSwipeViewController.SwipeThreshold) && hasNextRoom()
            {
                transiteToNext()
            }
            else
            {
                backToBottom()
            }
        }
        else
        {
            if (view.frame.origin.y > LiveSwipeViewController.SwipeThreshold) && hasPreviousRoom()
            {
                transiteToPrevious()
            }
            else
            {
                backToTop()
            }
        }
    }
    
    func showNextBackground()
    {
        guard currentLiveRoomIndex + 1 < liveRooms.count else
        {
            return
        }
        let next = liveRooms[currentLiveRoomIndex + 1]
        self.backgroundImageView.sd_setImage(with: URL(string: next.roomCover), placeholderImage: UIImage(named: "Broadcast"))
    }
    
    func showPreviousBackground()
    {
        guard currentLiveRoomIndex > 0 else
        {
            return
        }
        let next = liveRooms[currentLiveRoomIndex - 1]
        self.backgroundImageView.sd_setImage(with: URL(string: next.roomCover), placeholderImage: UIImage(named: "Broadcast"))
    }
    
    func showDefaultBackground()
    {
        self.backgroundImageView.image = UIImage(named: "Broadcast")
    }
    
    func hasNextRoom() -> Bool
    {
        return currentLiveRoomIndex + 1 < liveRooms.count
    }
    
    func hasPreviousRoom() -> Bool
    {
        return currentLiveRoomIndex > 0
    }
    
    func transiteToNext()
    {
        stopCurrent
        {[weak self] (success) in

        }
        self.currentLiveRoomIndex += 1
        self.playTrasitionAnimation(positiveGravity: false)
    }
    
    func transiteToPrevious()
    {
        stopCurrent
        {[weak self] (success) in

        }
        self.currentLiveRoomIndex -= 1
        self.playTrasitionAnimation(positiveGravity: true)
    }
    
    func backToTop()
    {
        guard let vc = self.currentVC else
        {
            return
        }
        removeTopAndBottomBoundary()
        
        animationStatus = .backToTop
        self.collision.translatesReferenceBoundsIntoBoundary = false
        self.gravity.magnitude = 1
        self.gravity.gravityDirection = CGVector(dx: 0, dy: -1.0)
        self.collision.addBoundary(withIdentifier: "top" as NSCopying, from: CGPoint(x: 0, y: -0.5), to: CGPoint(x: self.view.frame.width, y: -0.5))
        self.animator.updateItem(usingCurrentState: vc.view)
    }
    
    func backToBottom()
    {
        guard let vc = self.currentVC else
        {
            return
        }
        removeTopAndBottomBoundary()
        
        animationStatus = .backToBottom
        self.collision.translatesReferenceBoundsIntoBoundary = false
        self.gravity.magnitude = 1
        self.gravity.gravityDirection = CGVector(dx: 0, dy: 1.0)
        self.collision.addBoundary(withIdentifier: "bottom" as NSCopying, from: CGPoint(x: 0, y: self.view.frame.height + 0.5), to: CGPoint(x: self.view.frame.width, y: self.view.frame.height + 0.5))
        self.animator.updateItem(usingCurrentState: vc.view)

    }
    
    func stopCurrent(completeBlock: ((Bool)-> Void)?)
    {
        guard let vc = self.currentVC else
        {
            return
        }
        
        vc.stopWatching(complete: completeBlock)
    }
    
    func loadCurrent()
    {
        if let last = self.currentVC
        {
            last.view.removeGestureRecognizer(self.panGensture)
            last.view.removeFromSuperview()
            last.willMove(toParentViewController: nil)
            last.removeFromParentViewController()
            
            self.currentVC = nil
            self.gravity = nil
            self.collision = nil
            self.animator = nil
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01, execute: loadCurrentBlock())
            return
        }
        loadCurrentBlock()()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func dynamicAnimatorWillResume(_ animator: UIDynamicAnimator)
    {
        
    }
    
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator)
    {
        guard let vc = self.currentVC else
        {
            return
        }
        // 过场
        if animationStatus == .transition
        {
            //self.loadCurrent()
        }
        else if (animationStatus == .backToTop)
        {
            self.collision.removeBoundary(withIdentifier: "top" as NSCopying)
            //self.collision.translatesReferenceBoundsIntoBoundary = true
            addAllBoundaries()
        }
        else if (animationStatus == .backToBottom)
        {
            self.collision.removeBoundary(withIdentifier: "bottom" as NSCopying)
            //self.collision.translatesReferenceBoundsIntoBoundary = true
            addAllBoundaries()
        }
        animationStatus = .none
    }
    
    func collisionBehavior(_ behavior: UICollisionBehavior, beganContactFor item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying?, at p: CGPoint)
    {
        guard let str = identifier as? String else
        {
            return
        }
        if str == "AnimationTop" || str == "AnimationBottom"
        {
            self.loadCurrent()
        }
    }
    
    func playTrasitionAnimation(positiveGravity: Bool)
    {
        guard let vc = self.currentVC else
        {
            return
        }
        removeTopAndBottomBoundary()
        animationStatus = .transition
        self.gravity.magnitude = 8
        self.collision.translatesReferenceBoundsIntoBoundary = false
        self.gravity.gravityDirection = CGVector(dx: 0, dy: positiveGravity ?  1.0 : -1.0)
        
        if let velocity = self.currentPanVelocity
        {
            let itemBehavior = UIDynamicItemBehavior(items: [vc.view])
            itemBehavior.addLinearVelocity(velocity, for: vc.view)
            itemBehavior.elasticity = 0
            self.gravity.addChildBehavior(itemBehavior)
        }

        
        self.animator.updateItem(usingCurrentState: vc.view)
    }
    
    func removeTopAndBottomBoundary()
    {
        self.collision.removeBoundary(withIdentifier: "top" as NSCopying)
        self.collision.removeBoundary(withIdentifier: "bottom" as NSCopying)
    }
    
    func loadCurrentBlock() -> ((Void) -> Void)
    {
        return {
            if !VLSMessageManager.shared().getLoginState()
            {
                VLSMessageManager.shared().imLogin()
            }
            let roomInfo = self.liveRooms[self.currentLiveRoomIndex]
            
            let vc = VLSShowViewController()
            vc.anchorId = roomInfo.host
            vc.roomId = roomInfo.roomId
            
            vc.orientation = roomInfo.orientation.intValue == 2 ? KLiveRoomOrientation_LandscapeRight : KLiveRoomOrientation_Portrait;
            self.view.addSubview(vc.view)
            
            // 消除闪烁
            vc.videoCoverView.coverbg.sd_setImage(with: URL(string: roomInfo.roomCover), placeholderImage: UIImage(named: "Broadcast"))
            vc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height:  self.view.frame.height)
            self.currentVC = vc
            self.addChildViewController(vc)
            
            vc.view.addGestureRecognizer(self.panGensture)
            
            self.gravity = UIGravityBehavior(items: [vc.view])
            self.gravity.magnitude = 1
            self.collision = UICollisionBehavior(items: [vc.view])
            //self.collision.translatesReferenceBoundsIntoBoundary = true
            self.addAllBoundaries()
            self.collision.collisionDelegate = self
            
            self.collision.addBoundary(withIdentifier: "AnimationTop" as NSCopying, from: CGPoint(x: 0, y: -self.view.frame.height - 20), to: CGPoint(x: self.view.frame.width, y: -self.view.frame.height - 20))
            self.collision.addBoundary(withIdentifier: "AnimationBottom" as NSCopying, from: CGPoint(x: 0, y: self.view.frame.height * 2 + 20), to: CGPoint(x: self.view.frame.width, y: self.view.frame.height * 2 + 20))
            
            self.animator = UIDynamicAnimator(referenceView: self.view)
            self.animator.delegate = self
            self.animator.addBehavior(self.collision)
            self.animator.addBehavior(self.gravity)
            
            let joinRoomTracking = VLSJoinRoomModel()
            joinRoomTracking.roomID = roomInfo.roomId;
            joinRoomTracking.joinType = "swipe";
            vc.roomTrackContext = VLSUserTrackingManager.share().trackingJoinroom(joinRoomTracking)
        }
    }
    
    func addAllBoundaries()
    {
        self.collision.addBoundary(withIdentifier: "top" as NSCopying, from: CGPoint(x: -0.5, y: -0.5), to: CGPoint(x: self.view.frame.width + 0.5, y: -0.5))
        self.collision.addBoundary(withIdentifier: "bottom" as NSCopying, from: CGPoint(x: -0.5, y: self.view.frame.height + 0.5), to: CGPoint(x: self.view.frame.width + 0.5, y: self.view.frame.height + 0.5))
        
        self.collision.addBoundary(withIdentifier: "lef" as NSCopying, from: CGPoint(x: -0.5, y: -0.5), to: CGPoint(x: -0.5, y: self.view.frame.height + 0.5))
        self.collision.addBoundary(withIdentifier: "right" as NSCopying, from: CGPoint(x: self.view.frame.width + 0.5, y: -0.5), to: CGPoint(x: self.view.frame.width + 0.5, y: self.view.frame.height + 0.5))
    }
}
