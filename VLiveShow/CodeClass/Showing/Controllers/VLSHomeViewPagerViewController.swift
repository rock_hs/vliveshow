//
//  VLSHomeViewPagerViewController.swift
//  
//
//  Created by VincentX on 02/12/2016.
//
//

import UIKit

class VLSHomeViewPagerViewController: PAViewPagerViewController {
    
    static let ChannelSavedKey = "home:channels"
    
    static let maxNumberOfTabs = 5
    var isLoading: Bool = false
    var hasDataFromServer: Bool = false
    
    var retryTimer: Timer?
    lazy var addChannelButton: UIButton = UIButton(type: .custom)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = self.viewPager.then
        {
            $0.allowScroll = true
            $0.tabBackgroundColor = UIColor.white
            $0.titleColor = UIColor.x777777()
            $0.titleFont = UIFont(name: "PingFangSC-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14)
            $0.tabHeight = 36
            $0.selectionIndicatorHeight = 2
            $0.selectionIndicatorY = $0.tabHeight - $0.selectionIndicatorHeight
            $0.selectedTitleColor = UIColor.themeRed()
            $0.tabSelectedBackgroundColor = UIColor.themeRed()
            $0.tabLeftOffset = 0
            $0.tabRightOffset = 0
            $0.tabInset = UIEdgeInsetsMake(0, 0, 0, 40)
            $0.tabMinimumWidth = max(62.5, self.view.frame.width / 5)
            $0.seperatorView.backgroundColor = HEX(0xdddddd)
            $0.invalidateLayout()
            

        }
        if let parentView = self.viewPager.tabCollectionView.superview
        {
            parentView.addSubview(self.addChannelButton)
            _ = self.addChannelButton.then
            {
                $0.setImage(#imageLiteral(resourceName: "ic_add"), for: .normal)
                $0.adjustsImageWhenHighlighted = false
                $0.addTarget(self, action: #selector(didPressChannelButton), for: .touchUpInside)
            }
            self.addChannelButton.snp.makeConstraints(
            { (maker) in
                maker.top.trailing.equalToSuperview()
                maker.bottom.equalToSuperview().offset(-1)
                maker.width.equalTo(69)
            })
        }
        self.loadChannels()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.useRedTheme()
        self.extendedLayoutIncludesOpaqueBars = false
        self.edgesForExtendedLayout = []
        if !hasDataFromServer
        {
            loadChannels()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        retryTimer?.invalidate()
        self.retryTimer = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadChannels()
    {
        
        if isLoading
        {
            return
        }
        
        guard let account = AccountManager.shared().account,
            let _ = account.token
            else {
                self.retryTimer?.invalidate()
                self.retryTimer = nil
                return
        }
        
        let callback = HttpCallBack()
        callback.doneBlock =
        {[weak self] result in
            if let dict = result as? [String: Any],
               let resultDict = dict["results"] as? [String: Any],
                let list = resultDict["list"] as? [[String: Any]]
            {
                self?.hasDataFromServer = list.count > 0
                if (list.count > 0)
                {
                    UserChannelManager.saveAllChannels(channels: list)
                }
                if let viewControllers =  self?.listMapVC(list: UserChannelManager.loadStarredChannels())
                {
                    self?.subViewControllers = viewControllers
                }
            }
            self?.isLoading = false
            self?.startRetryIfNeeded()
        }
        
        callback.errorBlock =
        {[weak self] error in
            self?.isLoading = false
            self?.startRetryIfNeeded()
        }
        isLoading = true
        HTTPFacade.getChannelInfo(callback);
    }
    
    func startRetryIfNeeded()
    {
        if !hasDataFromServer
        {
            if self.subViewControllers.count == 0
            {
                loadChannelsFromLocal()
            }
            startRetry()
        }
        else
        {
            self.retryTimer?.invalidate()
            self.retryTimer = nil
        }
    }
    
    func startRetry()
    {
        guard let account = AccountManager.shared().account,
            let _ = account.token
            else {
            return
        }
        
        if let _ = self.retryTimer
        {
            return
        }
        self.retryTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(loadChannels), userInfo: nil, repeats: true)
    }
    
    func layoutChannels()
    {
        
    }
    
    func loadChannelsFromLocal()
    {
        let list = UserChannelManager.loadStarredChannels()
        self.subViewControllers = listMapVC(list: list)
        self.viewPager.setSelectedIndex(0, animated: false, scrollTab: true)
    }
    
    func listMapVC(list: [VLSChannel]) -> [UIViewController]
    {
        return list.flatMap
            { channelModel -> UIViewController? in
                if let channelURL = channelModel.channelUrl,
                    !channelURL.isEmpty
                {
                    let vc = VLSLiveWebViewController()
                    vc.url = channelURL
                    vc.title = channelModel.channelName
                    return vc
                }
                else
                {
                    let vc = VLSLiveViewController()
                    vc.title = channelModel.channelName
                    vc.channel = channelModel
                    return vc
                }
        }
    }
    
    override  open func viewPager(_ viewPager: PAViewPager, didShowViewAtIndex: Int, previousIndex:Int, animated: Bool) -> Void
    {
        super.viewPager(viewPager, didShowViewAtIndex: didShowViewAtIndex, previousIndex: previousIndex, animated: animated)
        if (previousIndex != -1)
        {
            VLSUserTrackingManager.share().trackingClick(withAction: "ClickChannelTab", extraInfo: [:])
        }
    }
    
    func didPressChannelButton()
    {
        if let channelVC = UIStoryboard(name: "Channels", bundle: nil).instantiateInitialViewController() as? ChannelsManagementCollectionViewController
        {
            channelVC.hidesBottomBarWhenPushed = true
            channelVC.didUpdateStarredChannels =
            {[weak self] in
                self?.loadChannelsFromLocal()
            }
            self.navigationController?.pushViewController(channelVC, animated: true)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
