//
//  VLSLiveViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/5/24.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import <AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>
#import <AVFoundation/AVFoundation.h>
#import "VLSTeamMessageModel.h"

@class VLSChannel;

@interface VLSLiveViewController : VLSBaseViewController
@property(nonatomic, strong) VLSChannel* channel;
@end
