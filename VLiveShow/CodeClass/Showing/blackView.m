//
//  blackView.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "blackView.h"

@implementation blackView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        // ios8 模糊图
        UIBlurEffect *beffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *view = [[UIVisualEffectView alloc]initWithEffect:beffect];
        
        view.frame = self.bounds;
        
        [self addSubview:view];
        
        UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectZero];
        [imageview setImage:[UIImage imageNamed:@"home_refuse"]];
        [self addSubview:imageview];
        imageview.sd_layout
        .centerXEqualToView(self)
        .topSpaceToView(self,150)
        .heightIs(90)
        .widthIs(137);
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectZero];
        label.text = LocalizedString(@"BLACK_LIST");
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:18];
        label.textColor = [UIColor whiteColor];
        [self addSubview:label];
        label.sd_layout
        .topSpaceToView(imageview,10)
        .centerXEqualToView(self)
        .widthIs(200)
        .heightIs(52);
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:@"button-write"] forState:UIControlStateNormal];
        [button setTitle:LocalizedString(@"WATCH_OTHER") forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:15];
        [self addSubview:button];
        button.sd_layout
        .leftSpaceToView(self,30)
        .rightSpaceToView(self,30)
        .centerXEqualToView(self)
        .heightIs(40);
        [button addTarget:self action:@selector(buttonclick) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)buttonclick
{
    self.clickBlock();
}

@end
