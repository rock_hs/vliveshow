//
//  blackView.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface blackView : UIView

typedef void (^btnclick)();

@property (nonatomic, strong) btnclick clickBlock;

@end
