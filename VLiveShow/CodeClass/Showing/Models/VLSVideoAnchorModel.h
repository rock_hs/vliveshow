//
//  VLSAnchorModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//  对应服务器端能够识别的 anchor model

#import <Foundation/Foundation.h>
#define USER_TYPE_MASTER    @"master"
#define USER_TYPE_SLAVE     @"slave"
#define USER_TYPE_GUEST     @"guest"

#define VIDEO_OPENED        @"opened"
#define VIDEO_CLOSED        @"closed"

#define VOICE_OPENED        @"opened"
#define VOICE_MUTED         @"muted"
#define VOICE_MUTEDS        @"muteSelf"

@class VLSVideoViewModel;
@interface VLSVideoAnchorModel : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, strong) NSString *video;
@property (nonatomic, strong) NSString *voice;
@property (nonatomic, strong) NSString *position;

- (instancetype)initWidthViewModel:(VLSVideoViewModel*)model;

@end
