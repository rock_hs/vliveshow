//
//  VLSUserProfile.h
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ImSDK/ImSDK.h>

@interface VLSUserProfile : NSObject

@property (nonatomic, assign) NSInteger userLevel;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userFaceURL;
//@property (nonatomic, strong) NSString *userHeadImage;//替换
@property (nonatomic, strong) NSString *userLocation;
//性别
@property (nonatomic, copy) NSString *sex;
//个性签名
@property (nonatomic, copy) NSString *gender;

//@property (nonatomic, copy) NSString *userContent;
//
////关注
//@property (nonatomic, assign) NSInteger followNumber;
////粉丝
//@property (nonatomic, assign) NSInteger fansNumber;
////好友
//@property (nonatomic, assign) NSInteger friendNumber;
////黑名单
//@property (nonatomic, assign) NSInteger blackNumber;
//
//@property (nonatomic, assign) NSInteger sendNumber;//要替换
////关注列表
@property (nonatomic,strong)NSMutableArray *focusList;
- (void)GetUserProfileFromTIMUserProfile:(TIMUserProfile *)userprofile;
- (void)GetUserProfileFromDict:(NSDictionary *)dict;

@end
