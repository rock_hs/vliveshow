//
//  VLSSendGiftModel.h
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSSendGiftModel : NSObject
@property (nonatomic, assign) NSInteger giftId;
@property (nonatomic, assign) NSInteger giftCount;



@end
