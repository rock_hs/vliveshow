//
//  VLSHotModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/21.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AvatarsModel.h"
@interface VLSHotModel : NSObject

/// 主播头像
@property (nonatomic, strong)AvatarsModel *cover;
@property (nonatomic, copy)NSString *coverUrl;
/// 位置信息
@property (nonatomic, copy)NSString *city;
@property (nonatomic, copy)NSString *location;
/// 真实的在线人数
@property (nonatomic, copy)NSString *onlineNum;
/// 观看过的人数
@property (nonatomic, copy)NSString *watchNum;
/// 计算出来的人数
@property (nonatomic, copy)NSString *cNum;
/// 新的人数
@property (nonatomic, copy)NSString *scNum;
/// 主播昵称
@property (nonatomic, copy)NSString *hostNickName;
/// 主播 ID
@property (nonatomic, copy)NSNumber *host;
// 直播间 ID
@property (nonatomic, copy)NSString *roomId;
/// 主播标题
@property (nonatomic, copy)NSString *title;
/// 直播状态
@property (nonatomic, copy)NSString *living;
// 标签内容
@property (nonatomic, retain)NSArray *topics;
@property (nonatomic, copy) NSNumber *priority;
@property (nonatomic, copy)NSString *updateTime;
@property (nonatomic, copy)NSNumber *createTime;
//课程ID
@property (nonatomic, copy) NSString *courseId;
@property (nonatomic, copy) NSString *lessonId;
//课程ID 这个名称有点儿唬人，改用courseID
@property (nonatomic, strong) NSString *id;
//1竖2横 nil竖
@property (nonatomic, strong) NSNumber *orientation;

@end
