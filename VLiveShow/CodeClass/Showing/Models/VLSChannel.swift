//
//  VLSChannel.swift
//  VLiveShow
//
//  Created by VincentX on 02/12/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit

class VLSChannel: NSObject {
    
    var channelId: NSNumber?
    var channelName: String?
    var channelType: String?
    var channelUrl: String?
    var type: String?

}
