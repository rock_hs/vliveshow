//
//  VLSAnchorModel.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSVideoAnchorModel.h"

@implementation VLSVideoAnchorModel

- (instancetype)initWidthViewModel:(VLSVideoViewModel*)model
{
    if (self = [super init]) {
        
        self.userId = model.userId;
        if (model.userType == UserTypeHost) {
            self.userType = USER_TYPE_MASTER;
        }else if (model.userType == UserTypeGuest) {
            self.userType = USER_TYPE_GUEST;
        }else{
            self.userType = USER_TYPE_SLAVE;
        }
        
        if (model.muteVoiceBySelf) {
            self.voice = VOICE_MUTEDS;
        }else{
            if (model.muteVoice) {
                self.voice = VOICE_MUTED;
            }else{
                self.voice = VOICE_OPENED;
            }
        }
        if (model.muteVideo) {
            self.video = VIDEO_CLOSED;
        }else{
            self.video = VIDEO_OPENED;
        }

    }
    return self;
}

@end
