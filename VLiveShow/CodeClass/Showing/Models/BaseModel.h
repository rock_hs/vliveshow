//
//  BaseModel.h
//  VLS_BarrageViewDemo
//
//  Created by SXW on 16/5/26.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKDBHelper.h"

@interface BaseModel : NSObject
+(LKDBHelper *)getUsingLKDBHelper;



@end
