//
//  VLSLiveInfoModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSVideoLayoutModel.h"
@interface VLSLiveInfoModel : NSObject

/// 主播头像
@property (nonatomic, strong)AvatarsModel *cover;
@property (nonatomic, copy)NSString *coverUrl;
/// 位置信息
@property (nonatomic, copy)NSString *city;
@property (nonatomic, copy)NSString *location;
/// 在线观看人数
@property (nonatomic, copy)NSString *onlineNum;
//观看人数
@property (nonatomic, copy)NSString *watchNum;

@property (nonatomic, copy)NSString *scNum;

/// 主播昵称
@property (nonatomic, copy)NSString *hostNickName;
/// 主播 ID
@property (nonatomic, copy)NSString *host;
// 直播间 ID
@property (nonatomic, copy)NSString *roomId;
/// 主播标题
@property (nonatomic, copy)NSString *title;
/// 直播状态
@property (nonatomic, copy)NSString *living;

@property (nonatomic, copy)NSString *totalGainTicket;

@property (nonatomic, strong) NSNumber *orientation;

@property (nonatomic, strong)VLSVideoLayoutModel *layout;
@property (nonatomic, strong)NSArray *topics;

@property (nonatomic, assign) BOOL rated;
@end
