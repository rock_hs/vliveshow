//
//  VLSCourseModel.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
import ObjectMapper

class VLSCourseModel: VLSTableViewItem {

    var courseId: NSNumber! //CLongLong!
    var courseName: String?
    var courseType: NSNumber! // NSInteger!
    var numLessons: NSNumber! // NSInteger!
    var allowAudience: NSNumber! //Bool!
    var maxParticipants: NSNumber! //NSInteger!
    var priceParticipant: NSNumber! //NSInteger!
    var priceAudience: NSNumber! // NSInteger!
    var lessonDescription: String!
    var teacherId: NSNumber! // NSInteger!
    var teacherName: String!
    var teacherDescription: String!
    var picCover: String!
    var picTeacher: String!
    var picDescription: String!
    var createTime: NSNumber! //CLongLong!
    var updateTime: NSNumber! //CLongLong!
    var published: NSNumber! //Bool!
    var deleted: NSNumber! // Bool!
    var cancellationType: NSNumber! //NSInteger!
    var cancellationCustom: String!
    var startTime: NSNumber! = 0 //CLongLong!
    var priority: NSNumber = NSNumber(integerLiteral: 0)
    var courseTypeName: String?
    
    var surplusParticipants: NSNumber!
    var surplusDiamond: NSNumber!
    var rmbPriceParticipant: NSNumber!
    var rmbPriceAudience: NSNumber!
//    var subscribe: String!
    var userId: NSNumber!
    var overLesson: NSNumber!   // 第几节课
    var remark: String!     //（目前用来放订阅课程id）
    var lessonId: NSNumber!
    var roomId: String?
    var nextLessonStartTime: NSNumber = 0
    var nextLessonEndTime: NSNumber = 0
    var nextLessonName: String?
    var nextLessonId: NSNumber! = 0
    
    override func mapping(map: Map) {
        courseId <- map["id"]
        courseName <- map["courseName"]
        courseType <- map["courseType"]
        courseTypeName <- map["courseTypeName"]
        numLessons <- map["numLessons"]
        allowAudience <- map["allowAudience"]
        maxParticipants <- map["maxParticipants"]
        priceParticipant <- map["priceParticipant"]
        priceAudience <- map["priceAudience"]
        lessonDescription <- map["lessonDescription"]
        teacherId <- map["teacherId"]
        teacherName <- map["teacherName"]
        teacherDescription <- map["teacherDescription"]
        picCover <- map["picCover"]
        picTeacher <- map["picTeacher"]
        picDescription <- map["picDescription"]
        createTime <- map["createTime"]
        updateTime <- map["updateTime"]
        published <- map["published"]
        deleted <- map["deleted"]
        cancellationType <- map["cancellationType"]
        cancellationCustom <- map["cancellationCustom"]
        startTime <- map["startTime"]
        priority <- map["priority"]
        surplusParticipants <- map["surplusParticipants"]
        surplusDiamond <- map["surplusDiamond"]
        rmbPriceParticipant <- map["rmbPriceParticipant"]
        rmbPriceAudience <- map["rmbPriceAudience"]
//        subscribe <- map["subscribe"]
        userId <- map["userId"]
        overLesson <- map["overLesson"]
        remark <- map["remark"]
        lessonId <- map["lessonId"]
        roomId <- map["roomId"]
        nextLessonStartTime <- map["nextLessonStartTime"]
        nextLessonEndTime <- map["nextLessonEndTime"]
        nextLessonName <- map["nextLessonName"]
        nextLessonId <- map["nextLessonId"]
    }
}
