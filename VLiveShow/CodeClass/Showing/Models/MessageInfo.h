//
//  MessageInfo.h
//  Demo3_Chat
//
//  Created by SXW on 16/2/24.
//  Copyright © 2016年 SXW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfo.h"
@interface MessageInfo : NSObject

@property (nonatomic, strong) UserInfo *userInfo;
//发送的消息
@property (nonatomic, copy) NSString *msg;

//用户名
@property (nonatomic, copy) NSString *userName;

//记录是否是“我”发送的消息
@property (nonatomic) BOOL isMine;

@end



