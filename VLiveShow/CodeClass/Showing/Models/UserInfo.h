//
//  UserInfo.h
//  VLS_BarrageViewDemo
//
//  Created by SXW on 16/5/17.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject
@property (nonatomic, assign) NSInteger userId;//用户id
@property (nonatomic, assign) NSInteger userLevel;//用户级别
@property (nonatomic, copy) NSString *userName;//用户名
@property (nonatomic, copy) NSString *userImage;//用户头像
@end
