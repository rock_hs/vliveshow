//
//  VLSVideoModel.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSVideoLayoutModel.h"

@implementation VLSVideoLayoutModel

+ (NSDictionary *)objectClassInArray
{
    return @{
             @"anchors" : [VLSVideoAnchorModel class],
             };
}

@end
