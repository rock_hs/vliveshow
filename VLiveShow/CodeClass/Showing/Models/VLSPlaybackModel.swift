//
//  VLSPlaybackModel.swift
//  VLiveShow
//
//  Created by VincentX on 16/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

import UIKit

class VLSPlaybackModel: NSObject
{
    var id: String?
    var title: String?
    var custom: String?
    var avatarUrl: String?
    var detailTitle: String?
    var videoUrl: String?
    var coverUrl: String?
    var duration: String?
}
