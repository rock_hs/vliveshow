//
//  VlSGiftModel.h
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    GiftShowTypeDefault = 0,
    GiftShowTypeCar     = 1,
    GiftShowTypePlan    = 2,
    GiftShowTypeBoat    = 3,
}GiftShowType;

@interface VlSGiftModel : NSObject

@property (nonatomic, assign) NSInteger giftId;
@property (nonatomic, strong) NSString *giftName;
@property (nonatomic, strong) NSString *giftImageName;
@property (nonatomic, assign) GiftShowType giftShowType;
//经验、等。。。


@end
