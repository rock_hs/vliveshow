//
//  VlSGift.m
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import "VlSGift.h"

@implementation VlSGift

+ (NSMutableArray *)getGiftList
{
    NSMutableArray *list = [[NSMutableArray alloc] initWithCapacity:0];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"GiftList" ofType:@"plist"];
    NSArray *arr = [[NSMutableArray alloc] initWithContentsOfFile:path];
    for (int i=0; i<[arr count]; i++) {
        NSDictionary *dict = [arr objectAtIndex:i];
        NSString *gid = [dict objectForKey:@"giftId"];
        NSString *gim = [dict objectForKey:@"giftImage"];
        NSString *gnm = [dict objectForKey:@"giftName"];
//        NSString *gtp = [dict objectForKey:@"giftShowType"];
        
        VlSGiftModel *model = [[VlSGiftModel alloc] init];
        model.giftId = gid.integerValue;
        model.giftName = gnm;
        model.giftImageName = gim;
        model.giftShowType = GiftShowTypeDefault;
        [list addObject:model];
    }

    return nil;
}

+ (VlSGiftModel *)getGiftWidthGiftId:(NSString *)giftId;
{
    NSMutableArray *arr = [self getGiftList];
    for (int i=0 ; i<[arr count]; i++) {
        VlSGiftModel *model = [arr objectAtIndex:i];
        if (model.giftId == giftId.integerValue) {
            return model;
        }
    }
    return nil;
}
@end
