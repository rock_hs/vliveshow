//
//  VLSSendC2CMsgModel.h
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSSendC2CMsgModel : NSObject
@property (nonatomic, assign) NSInteger toId;
@property (nonatomic, strong) NSString *message;

@end
