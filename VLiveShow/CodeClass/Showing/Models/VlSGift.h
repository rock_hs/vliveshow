//
//  VlSGift.h
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VlSGiftModel.h"

@interface VlSGift : NSObject

+ (NSMutableArray *)getGiftList;

+ (VlSGiftModel *)getGiftWidthGiftId:(NSString *)giftId;

@end
