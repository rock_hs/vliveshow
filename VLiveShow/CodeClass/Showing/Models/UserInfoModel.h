//
//  UserInfoModel.h
//  VLS_BarrageViewDemo
//
//  Created by SXW on 16/5/26.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import "BaseModel.h"

@interface UserInfoModel : BaseModel
//用户头像
@property (nonatomic, copy) NSString *userHeadImage;

//用户名
@property (nonatomic, copy) NSString *userName;
//性别
@property (nonatomic, copy) NSString *sex;
//个性签名
@property (nonatomic, copy) NSString *userContent;

//关注
@property (nonatomic, assign) NSInteger followNumber;
//粉丝
@property (nonatomic, assign) NSInteger fansNumber;
//好友
@property (nonatomic, assign) NSInteger friendNumber;

@end
