//
//  VLSVideoModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//  中转用的

#import <Foundation/Foundation.h>
#import "VLSVideoAnchorModel.h"

@interface VLSVideoLayoutModel : NSObject
@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) NSMutableArray *anchors;



@end
