//
//  VLSUserProfile.m
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import "VLSUserProfile.h"

@implementation VLSUserProfile

- (void)GetUserProfileFromTIMUserProfile:(TIMUserProfile *)userprofile
{
    if (userprofile) {
        self.userId = userprofile.identifier;
        self.userName = userprofile.nickname;
        self.userFaceURL = userprofile.faceURL;
    }

}

- (void)GetUserProfileFromDict:(NSDictionary *)dict
{
    if (dict) {
        self.userId = [[dict objectForKey:@"userId"] stringValue];
        NSString* nickName = [dict objectForKey:@"nickName"];
        if ([nickName isKindOfClass:[NSNull class]]) {
            nickName = @"";
        }
        self.userName = nickName;
        self.userFaceURL = [dict objectForKey:@"avatars"];
    }
}

@end
