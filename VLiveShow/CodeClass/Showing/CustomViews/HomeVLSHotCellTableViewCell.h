//
//  HomeVLSHotCellTableViewCell.h
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSLiveListModel.h"

@interface HomeVLSHotCellTableViewCell : UITableViewCell
/// 直播列表数据模型
@property (nonatomic, strong)VLSLiveListModel *liveListModel;
/// 用户头像
@property (nonatomic, strong)UIImageView *iconImageView;
/// 用户昵称
@property (nonatomic, strong)UILabel *nameLabel;
/// 地理位置图标
@property (nonatomic, strong)UIImageView *mapImageView;
/// 地址信息
@property (nonatomic, strong)UILabel *addressLabel;
/// 在线人数配图
@property (nonatomic, strong)UIImageView *countImageView;
/// 显示在线观看人数的label
@property (nonatomic, strong)UILabel *countLabel;
/// 封面 Imageview
@property (nonatomic, strong)UIImageView *coverImageView;
/// 显示直播状态的 Label
@property (nonatomic, strong)UILabel *playStatusLabel;
//直播状态背景图
@property (nonatomic, strong) UIImageView *playStatusBackground;
/// 主播标题
@property (nonatomic, strong)UILabel *titleLabel;
//  标签
@property (nonatomic, strong)UILabel *tags;
//  V标签
@property (nonatomic, strong)UIImageView *VImg;
//间隙分割线
@property (nonatomic, strong)UILabel *lines;
/// 开播时间
@property (nonatomic, strong)UILabel *timeLabel;

- (void)showTimeLableOnListWithModel:(VLSLiveListModel *)liveListModel;

@end
