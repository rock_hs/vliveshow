//
//  GiftView.m
//  SendGiftAnimation
//
//  Created by SXW on 16/5/19.
//  Copyright © 2016年 盛宣伟. All rights reserved.
//

#import "GiftView.h"

@implementation GiftView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroudImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
        self.headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        self.headImageView.layer.cornerRadius = 25;
//        self.headImageView.layer.masksToBounds = YES;
        self.headImageView.clipsToBounds = YES;
        self.giftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(200, 0, 50, 50)];
        self.giftImageView.layer.cornerRadius = 25;
        self.userName = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 100, 25)];
        self.userContent = [[UILabel alloc] initWithFrame:CGRectMake(60, 25, 100, 25)];
        
        self.userName.textColor = [UIColor whiteColor];
        self.userContent.font = [UIFont systemFontOfSize:12];
        self.userName.font = [UIFont systemFontOfSize:12];
        self.userContent.textColor = [UIColor greenColor];
        
        [self.backgroudImageView addSubview:self.headImageView];
        [self.backgroudImageView addSubview:self.giftImageView];
        [self.backgroudImageView addSubview:self.userName];
        [self.backgroudImageView addSubview:self.userContent];
        
        [self addSubview:self.backgroudImageView];
    }
    return self;
}

@end
