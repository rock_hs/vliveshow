//
//  OnLineNumberCollectionViewCell.m
//  balabala
//
//  Created by SuperGT on 16/5/12.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import "OnLineNumberCollectionViewCell.h"

@interface OnLineNumberCollectionViewCell ()

@property (nonatomic, weak) UIImageView *iconImg;


@end

@implementation OnLineNumberCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        _OnLineIcon = [[UIImageView alloc] init];
        _OnLineIcon.frame = CGRectMake(0, 0,self.frame.size.width, self.frame.size.height);
        _OnLineIcon.userInteractionEnabled = YES;
        _OnLineIcon.layer.masksToBounds = YES;
        _OnLineIcon.layer.cornerRadius = self.frame.size.width/2;
        _OnLineIcon.backgroundColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_OnLineIcon];
        _OnLineIcon.contentMode = UIViewContentModeScaleAspectFill;
    }
    return self;
}

-(void)setOnLineModel:(VLSUserProfile *)onLineModel{
//要替换掉
    [_OnLineIcon sd_setImageWithURL:[NSURL URLWithString:onLineModel.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
}


@end
