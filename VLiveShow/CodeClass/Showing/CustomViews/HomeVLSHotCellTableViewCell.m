//
//  HomeVLSHotCellTableViewCell.m
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HomeVLSHotCellTableViewCell.h"

@implementation HomeVLSHotCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self configUI];
        self.clipsToBounds = YES;
        
    }
    return self;
}
-(void)setLiveListModel:(VLSLiveListModel *)liveListModel
{
    //头像赋值
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:liveListModel.userCover] placeholderImage:ICON_PLACEHOLDER options:SDWebImageAllowInvalidSSLCertificates completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.iconImageView v_cornerRadiusWithImage:image cornerRadius:25 rectCornerType:UIRectCornerAllCorners];//调用UIBezier绘画圆角
    }];
    //标签赋值
    if (liveListModel.topics.count > 1 && ![liveListModel.topics[1] isEqualToString:@"<null>"]) {
        CGFloat width = [self widthOfString:liveListModel.topics[1] font:[UIFont systemFontOfSize:12] height:19];
        self.tags.text = liveListModel.topics[1];
        self.tags.sd_layout
        .leftSpaceToView(self.iconImageView, X_CONVERT(7))
        .topSpaceToView(self, Y_CONVERT(15))
        .widthIs(width + 18)
        .heightIs(Y_CONVERT(19));
        //标题赋值
        self.titleLabel.text = liveListModel.title;
        CGFloat widt = self.frame.size.width - 102 - self.tags.frame.size.width;
        self.titleLabel.sd_layout
        .leftSpaceToView(self.tags,X_CONVERT(7))
        .topSpaceToView(self, Y_CONVERT(14))
        .widthIs(widt + 10)
        .heightIs(Y_CONVERT(22));
    }else{
        self.tags.hidden = YES;
        //标题赋值
        self.titleLabel.text = liveListModel.title;
        CGFloat widt = self.frame.size.width - 102 - self.tags.frame.size.width;
        self.titleLabel.sd_layout
        .leftSpaceToView(self.iconImageView,X_CONVERT(7))
        .topSpaceToView(self, Y_CONVERT(14))
        .widthIs(widt + 10)
        .heightIs(Y_CONVERT(22));
    }

    // 数字千分位格式化
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSString *numStr = (liveListModel.courseId && [liveListModel.courseId length]) ? liveListModel.onlineNum : liveListModel.scNum;
    NSString *newAmount = [formatter stringFromNumber:[NSNumber numberWithInteger:[NSString stringWithFormat:@"%@", numStr].integerValue]];

    //主播昵称
    if (![liveListModel.hostNickName isEqualToString:@""]) {
        NSString *temLocStr = liveListModel.location;
        if ([liveListModel.location isEqualToString:@"定位关"]
            || [liveListModel.location isEqualToString:@"關閉定位"]
            || [liveListModel.location isEqualToString:@"定位關"]
            || [liveListModel.location isEqualToString:@"&&&"]
            || [liveListModel.location isEqualToString:@"(null)"]) {
            temLocStr = @"";
        }
        CGFloat locWidth = [self widthOfString:temLocStr font:[UIFont systemFontOfSize:12] height:19];
        CGFloat numWidth = [self widthOfString:newAmount font:[UIFont systemFontOfSize:SIZE_FONT_12] height:19];
        CGFloat width = [self widthOfString:liveListModel.hostNickName font:[UIFont systemFontOfSize:14] height:19];
        CGFloat temwidth = SCREEN_WIDTH - CGRectGetMaxX(self.iconImageView.frame)  - X_CONVERT(7) - X_CONVERT(10) - X_CONVERT(13) - X_CONVERT(3) - locWidth - X_CONVERT(7) - 1 - X_CONVERT(6) - X_CONVERT(13) - X_CONVERT(3) - numWidth - 12;
        width = MIN(width, temwidth);
        self.nameLabel.sd_layout
        .leftSpaceToView(self.iconImageView,X_CONVERT(7))
        .topSpaceToView(self.tags,Y_CONVERT(6))
        .widthIs(width)
        .heightIs(Y_CONVERT(19));
        self.nameLabel.text = liveListModel.hostNickName;
    }
    UILabel *lb = [self viewWithTag:1001113];
    self.mapImageView.hidden = YES;
    self.addressLabel.hidden = YES;
    lb.hidden = YES;
    //主播地理位置
    if ([liveListModel.location isEqualToString:@"定位关"]
        || [liveListModel.location isEqualToString:@"關閉定位"]
        || [liveListModel.location isEqualToString:@"定位關"]
        || [liveListModel.location isEqualToString:@"&&&"]
        || [liveListModel.location isEqualToString:@"(null)"]) {
        
        self.countImageView.sd_layout
        .leftSpaceToView(self.nameLabel,X_CONVERT(10))
        .topSpaceToView(self.titleLabel,Y_CONVERT(8))
        .widthIs(X_CONVERT(13))
        .heightIs(X_CONVERT(13));
        
        self.countLabel.sd_layout
        .leftSpaceToView(self.countImageView,X_CONVERT(3))
        .topSpaceToView(self.titleLabel,Y_CONVERT(8))
        .widthIs(X_CONVERT(100))
        .heightIs(X_CONVERT(13));
        
    }else{
        
        self.mapImageView.hidden = NO;
        self.addressLabel.hidden = NO;
        lb.hidden = NO;
        
        self.mapImageView.sd_layout
        .leftSpaceToView(self.nameLabel,X_CONVERT(10))
        .topSpaceToView(self.titleLabel,Y_CONVERT(8))
        .widthIs(X_CONVERT(13))
        .heightIs(X_CONVERT(13));
        
        CGFloat width = [self widthOfString:liveListModel.location font:[UIFont systemFontOfSize:12] height:19];
        self.addressLabel.sd_layout
        .leftSpaceToView(self.mapImageView,X_CONVERT(3))
        
        .topSpaceToView(self.titleLabel,Y_CONVERT(8))
        .widthIs(width)
        .heightIs(Y_CONVERT(13));
        self.addressLabel.text = liveListModel.location;
        
        lb.sd_layout
        .leftSpaceToView(self.addressLabel,X_CONVERT(7))
        .topEqualToView(self.addressLabel)
        .widthIs(1)
        .heightIs(Y_CONVERT(13));
        
        self.countImageView.sd_layout
        .leftSpaceToView(lb,X_CONVERT(6))
        .topSpaceToView(self.titleLabel,Y_CONVERT(8))
        .widthIs(X_CONVERT(13))
        .heightIs(X_CONVERT(13));
        
        self.countLabel.sd_layout
        .leftSpaceToView(self.countImageView,X_CONVERT(3))
        .topSpaceToView(self.titleLabel,Y_CONVERT(8))
        .widthIs(X_CONVERT(100))
        .heightIs(Y_CONVERT(13));
    }
    
    self.countLabel.text = newAmount;
    
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:liveListModel.roomCover] placeholderImage:nil options:SDWebImageAllowInvalidSSLCertificates];

    //添加V标签
    
    self.VImg.hidden = [liveListModel.priority intValue]<= 0;
    
    [self showTimeLableOnListWithModel:liveListModel];
    
}

- (void)showTimeLableOnListWithModel:(VLSLiveListModel *)liveListModel {
    // 子类重写
}

- (void)configUI
{
    [self buildIconImageView];
    [self buildTagsView];
    [self buildTitle];
    [self buildVImg];
    [self buildNickName];
    [self buildAddressPic];
    [self buildAddressLb];
    [self buildLine];
    [self buildOnLineImg];
    [self buildOnLineLb];
    [self buildCoverImageView];
    [self buildLines];
    [self buildLiveStatus];
    [self buildLiveStatusLabel];
    [self buildTimeLable];
}

//设置头像
- (void)buildIconImageView
{
    [self addSubview:self.iconImageView];
    self.iconImageView.sd_layout
    .leftSpaceToView(self, X_CONVERT(12))
    .topSpaceToView(self, Y_CONVERT(10))
    .widthIs(50)
    .heightIs(50);
}
//设置标签
- (void)buildTagsView
{
    [self addSubview:self.tags];
    self.tags.sd_layout
    .leftSpaceToView(self.iconImageView, X_CONVERT(7))
    .topSpaceToView(self, Y_CONVERT(14))
    .minWidthIs(0)
    .heightIs(Y_CONVERT(19));
}
//设置标题
- (void)buildTitle
{
    [self addSubview:self.titleLabel];
    self.titleLabel.sd_layout
    .leftSpaceToView(self.tags,X_CONVERT(11))
    .topSpaceToView(self,Y_CONVERT(14))
    .maxWidthIs(self.frame.size.width * 2 / 3)
    .heightIs(Y_CONVERT(20));
}
//V标签
- (void)buildVImg
{
    [self addSubview:self.VImg];
    self.VImg.sd_layout
    .rightSpaceToView(self,X_CONVERT(10))
    .topSpaceToView(self,0)
    .widthIs(X_CONVERT(17))
    .heightIs(Y_CONVERT(28));
}
//主播昵称
- (void)buildNickName
{
    [self addSubview:self.nameLabel];
    self.nameLabel.sd_layout
    .leftSpaceToView(self.iconImageView,X_CONVERT(7))
    .topSpaceToView(self.tags,Y_CONVERT(4))
    .minWidthIs(0)
    .heightIs(Y_CONVERT(20));
}
//定位图标
- (void)buildAddressPic
{
    [self addSubview:self.mapImageView];
    self.mapImageView.sd_layout
    .leftSpaceToView(self.nameLabel,X_CONVERT(10))
    .topSpaceToView(self.titleLabel,Y_CONVERT(7))
    .widthIs(X_CONVERT(13))
    .heightIs(X_CONVERT(13));
}
//地理位置标签
- (void)buildAddressLb
{
    [self addSubview:self.addressLabel];
    self.addressLabel.sd_layout
    .leftSpaceToView(self.mapImageView,X_CONVERT(3))
    .topSpaceToView(self.titleLabel,Y_CONVERT(7))
    .minWidthIs(0)
    .heightIs(Y_CONVERT(19));
}
//创建分割线
- (void)buildLine
{
    UILabel *lineLabel = [[UILabel alloc]init];
    [self addSubview:lineLabel];
    lineLabel.sd_layout
    .leftSpaceToView(self.nameLabel,self.addressLabel.frame.size.width + self.mapImageView.frame.size.width + 7)
    .topEqualToView(self.addressLabel)
    .widthIs(1)
    .heightIs(Y_CONVERT(19));
    lineLabel.tag = 1001113;
    lineLabel.backgroundColor = RGB16(COLOR_FONT_C1BEBE);
}
//在线人数图标
- (void)buildOnLineImg
{
    [self addSubview:self.countImageView];
    self.countImageView.sd_layout
    .leftSpaceToView(self.nameLabel,X_CONVERT(10))
    .topSpaceToView(self.titleLabel,Y_CONVERT(7))
    .widthIs(X_CONVERT(13))
    .heightIs(X_CONVERT(13));
}
//在线人数标签
- (void)buildOnLineLb
{
    [self addSubview:self.countLabel];
    self.countLabel.sd_layout
    .leftSpaceToView(self.countImageView,X_CONVERT(3))
    .topEqualToView(self.addressLabel)
    .minWidthIs(X_CONVERT(5))
    .heightIs(Y_CONVERT(19));
}
//显示封面
- (void)buildCoverImageView
{
    [self addSubview:self.coverImageView];
    self.coverImageView.sd_layout
    .leftSpaceToView(self,0)
    .topSpaceToView(self.iconImageView,Y_CONVERT(10))
    .rightSpaceToView(self,0)
    .heightIs([UIScreen mainScreen].bounds.size.width / 16 * 9);
}
//直播中标签
- (void)buildLiveStatus
{
    [self addSubview:self.playStatusBackground];
    self.playStatusBackground.sd_layout
    .rightSpaceToView(self,X_CONVERT(12))
    .topSpaceToView(self.iconImageView,Y_CONVERT(22))
    .widthIs(X_CONVERT(40))
    .heightIs(Y_CONVERT(20));
}
//直播中标签
- (void)buildLiveStatusLabel
{
    [self.playStatusBackground addSubview:self.playStatusLabel];
    self.playStatusLabel.sd_layout
    .centerXEqualToView(self.playStatusBackground)
    .centerYEqualToView(self.playStatusBackground)
    .widthIs(X_CONVERT(40))
    .heightIs(Y_CONVERT(20));
    self.playStatusLabel.text = @"直播";
    self.playStatusLabel.textAlignment = NSTextAlignmentCenter;
}

//间隙
- (void)buildLines
{
    self.lines = [[UILabel alloc]init];
    [self addSubview:self.lines];
    self.lines.sd_layout
    .leftSpaceToView(self,0)
    .rightSpaceToView(self,0)
    .topSpaceToView(self.coverImageView,0)
    .bottomSpaceToView(self, -1)
    .heightIs(Y_CONVERT(6));
    self.lines.backgroundColor = RGB16(COLOR_FONT_F4F4F4);
}

- (void)buildTimeLable {
    [self addSubview:self.timeLabel];
    self.timeLabel.sd_layout
    .rightSpaceToView(self, 10)
    .centerYEqualToView(self.countLabel)
    .widthIs(X_CONVERT(60))
    .heightIs(Y_CONVERT(20));
}
/*--------------------------------分割线（以下都是懒加载件 ）---------------------------------*/

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
    }
    return _iconImageView;
}
-(UILabel *)tags
{
    if (!_tags) {
        _tags = [[UILabel alloc]init];
        _tags.layer.cornerRadius = 10;
        _tags.layer.borderWidth = 1;
        _tags.layer.borderColor = RGB16(COLOR_FONT_C3A851).CGColor;
        _tags.clipsToBounds = YES;
        _tags.contentMode = UIViewContentModeScaleAspectFill;
        _tags.userInteractionEnabled = YES;
        _tags.backgroundColor = [UIColor clearColor];
        _tags.textColor = RGB16(COLOR_FONT_C3A851);
        _tags.font = [UIFont systemFontOfSize:12];
        _tags.textAlignment = NSTextAlignmentCenter;
    }
    return _tags;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont fontWithName:@"Helvetica Rounded MT Bold" size:15];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.numberOfLines = 1;
    }
    return _titleLabel;
}
- (UIImageView *)VImg
{
    if (!_VImg) {
        self.VImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_home_recommed"]];
    }
    return _VImg;
}
- (UILabel *)nameLabel
{
    if (!_nameLabel) {
        
        self.nameLabel = [[UILabel alloc] init];
        self.nameLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        self.nameLabel.backgroundColor = [UIColor clearColor];
        self.nameLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.nameLabel.text = @"";
        [self.addressLabel setSingleLineAutoResizeWithMaxWidth:100];
        
    }
    return _nameLabel;
}
- (UIImageView *)mapImageView {
    if (!_mapImageView) {
        self.mapImageView = [[UIImageView alloc] init];
        self.mapImageView.image = [UIImage imageNamed:@"ic_location"];
    }
    return _mapImageView;
}
- (UILabel *)addressLabel {
    if (!_addressLabel) {
        self.addressLabel = [[UILabel alloc] init];
        self.addressLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        self.addressLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        [self.addressLabel setSingleLineAutoResizeWithMaxWidth:100];
        
    }
    return _addressLabel;
}
- (UIImageView *)countImageView {
    if (!_countImageView) {
        self.countImageView = [[UIImageView alloc] init];
        self.countImageView.image = [UIImage imageNamed:@"ic_audcount"];
    }
    return _countImageView;
}

- (UILabel *)countLabel {
    if (!_countLabel) {
        self.countLabel = [[UILabel alloc] init];
        self.countLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        self.countLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
    }
    return _countLabel;
}
-(UIImageView *)coverImageView {
    if (!_coverImageView) {
        self.coverImageView = [[UIImageView alloc] init];
        self.coverImageView.image = [UIImage imageNamed:@"home_people"];
    }
    return _coverImageView;
}
-(UIImageView *)playStatusBackground{
    if (!_playStatusBackground) {
        self.playStatusBackground = [[UIImageView alloc] init];
        self.playStatusBackground.image = [UIImage imageNamed:@"ic_broadcast"];
    }
    return _playStatusBackground;
}
-(UILabel *)playStatusLabel{
    if (!_playStatusLabel) {
        self.playStatusLabel = [[UILabel alloc] init];
        self.playStatusLabel.textColor = [UIColor whiteColor];
        self.playStatusLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    }
    return _playStatusLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textColor = RGB16(COLOR_FONT_9B9B9B);
        _timeLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        _timeLabel.textAlignment = NSTextAlignmentRight;
    }
    return _timeLabel;
}

//根据文字计算宽度
-(CGFloat)widthOfString:(NSString *)string font:(UIFont *)font height:(CGFloat)height
{
    NSDictionary * dict=[NSDictionary dictionaryWithObject: font forKey:NSFontAttributeName];
    CGRect rect=[string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return rect.size.width;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
