//
//  VLSOrderView.h
//  VLiveShow
//
//  Created by tom.zhu on 2016/11/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VLSCourseModel;

static CGFloat const ORDERVIEW_HEIGHT = 337;

@protocol OrderViewDelegate <NSObject>
- (void)orderViewCloseClicked;
- (void)orderViewAttendanceClicked;
- (void)orderViewAudienceClicked;
- (void)orderViewPayClicked;
@end

@interface VLSOrderView : UIView
@property (nonatomic, strong, nonnull) VLSCourseModel *model;
@property (nonatomic, weak) id<OrderViewDelegate> delegate;
//更新balance
- (void)updateBalanc:(NSString*)newbalance;
@end
