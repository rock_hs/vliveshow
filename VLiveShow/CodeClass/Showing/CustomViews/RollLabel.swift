//
//  RollLabel.swift
//  VLiveShow
//
//  Created by vipabc on 16/10/19.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RollLabel: UIView {

    var firstLabel: UILabel?
    var secLabel: UILabel?
    var timer: Timer?
    var rect: CGRect?
    var rollSpeed: Float?            //滚动速度
    var isCanRoll: Bool = false     //默认不滚动
    var space: Float = 12           //滚动文字间隙
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.clipsToBounds = true
        
        secLabel = UILabel(frame: frame)
        secLabel?.textColor = UIColor.white
        secLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12))
        secLabel?.isHidden = true
        if let secLabel = secLabel {
            self.addSubview(secLabel)
        }
        
        //添加滚动文字栏
        firstLabel = UILabel(frame: frame)
        firstLabel?.textColor = UIColor.white
        firstLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_12))
        if let firstLabel = firstLabel {
            self.addSubview(firstLabel)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //文字滚动效果
    func timerAction() {
        guard var r = firstLabel?.frame else {
            return
        }
        guard var r1 = secLabel?.frame else {
            return
        }
        let customRollSpeed = CGFloat(r.width * 0.008)
        let newSpeed = rollSpeed <= 0 ? customRollSpeed : CGFloat(rollSpeed!)
        r.origin.x = r.origin.x - CGFloat(newSpeed)
        r1.origin.x = r1.origin.x - CGFloat(newSpeed)
        firstLabel?.frame = r
        secLabel?.frame = r1
        if -r1.origin.x > secLabel?.frame.width {
            secLabel?.frame = rect!
        }
        if -r.origin.x > firstLabel?.frame.width {
            firstLabel?.frame = rect!
        }
    }
    
    //开始滚动
    func starRoll() {
        if isCanRoll == true, let timer = timer, timer.isValid {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                timer.fireDate = Date()
            })
        }
    }
    
    //赋值
    func setText(_ text: String?, font: CGFloat = CGFloat(SIZE_FONT_12)) {
        var textSize = self.multilineTextSize(text, font: UIFont.systemFont(ofSize: font), maxSize: CGSize(width: CGFloat(MAXFLOAT), height: 10.0))
        if textSize.width > frame.width {
            isCanRoll = true    //可以滚动
            secLabel?.frame = CGRect(x: textSize.width + CGFloat(space), y: 0, width: textSize.width, height: frame.height)
            secLabel?.font = UIFont.systemFont(ofSize: font)
            secLabel?.text = text
            secLabel?.isHidden = false
            if let rect = secLabel?.frame {
                self.rect = rect
            }
            if timer == nil {
                timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(RollLabel.timerAction), userInfo: nil, repeats: true)
                RunLoop.current.add(timer!, forMode: RunLoopMode.commonModes)
                timer!.fireDate = Date.distantFuture
            }
        } else {
            textSize.width = frame.width
            secLabel?.isHidden = true
        }
        firstLabel?.frame = CGRect(x: 0, y: 0, width: textSize.width, height: frame.height)
        firstLabel?.font = UIFont.systemFont(ofSize: font)
        firstLabel?.text = text
        self.starRoll()
    }
    
    /**
     *  根据text获取相应的文字size
     */
    func multilineTextSize(_ text: String?,font: UIFont!,maxSize: CGSize,lineSpacing: CGFloat = 0) -> CGSize{
        if let text = text, text.characters.count > 0 {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = lineSpacing
            let textSize = (text as NSString).boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font, NSParagraphStyleAttributeName: style], context: nil).size
            return textSize
        }
        return CGSize.zero
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        timer?.invalidate()
        timer = nil
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
