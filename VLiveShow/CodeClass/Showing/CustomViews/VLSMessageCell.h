//
//  VLSMessageCell.h
//  Demo3_Chat
//
//  Created by SXW on 16/2/24.
//  Copyright © 2016年 SXW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSMessageViewModel.h"
#import "CTTextDisplayView.h"
@interface VLSMessageCell : UITableViewCell<CTTextDisplayViewDelegate>{
    
    //头像
    UIImageView *_headImageView;
    
    //用户名
    UILabel *_nameLabel;
    
    //聊天内容的背景图像
    UIImageView *_bubbleImageView;
    
    //聊天内容
    UILabel *_contentLabel;
    
}
//等级图标
@property (nonatomic, strong) UIImageView *levelImg;
//等级数字
@property (nonatomic, strong) UILabel *levelLabel;
@property (nonatomic, strong) UIView *Vie;
@property (nonatomic, strong) VLSMessageViewModel *msg;
@property (nonatomic, strong) CTTextDisplayView *CTtextView;
//@property (nonatomic, strong) MessageInfo *userName;
//- (void)addDataSource:(NSMutableArray *)array;


@end





