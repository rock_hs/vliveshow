//
//  BulletView.h
//  BulletForIOS
//
//  Created by SXW on 16/5/9.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

//move status
typedef NS_ENUM(NSInteger, CommentMoveStatus)
{
    MoveIn,
    Enter,
    MoveOut
};

@class BulletSettingDic;

@interface BulletView : UIView

@property (nonatomic, assign) float moveDuration;   //defaule is 5s,setting before startAnimation effect

@property (nonatomic, assign,readonly) float speed; //速度,这个速度在设置文字后自动生成，和屏幕宽度和文字长度有关。

@property (nonatomic, assign) float speedRate;      //速度比率，真实速度为speed＊speedRate；

@property (nonatomic, copy) void(^moveBlock)(CommentMoveStatus status);

@property (nonatomic, assign) NSInteger trajectory;//所在轨道编号

- (instancetype)initWithCommentDic:(BulletSettingDic *)commentDic;

- (void)reloadDataWithDic:(BulletSettingDic *)reloadDic;

- (void)startAnimation;

- (void)stopAnimation;

- (void)pauseAnimation;

- (void)resumeAnimation;

@end

@interface BulletSettingDic : NSObject
{
    NSMutableDictionary *_settingDic;
}
//设置字颜色
-(void)setBulletTextColor:(UIColor *)color;
-(UIColor *)bulletTextColor;

//设置背景颜色
-(void)setBulletBackgroundColor:(UIColor *)color;
-(UIColor *)bulletBackgroundColor;

//设置字体
-(void)setBulletTextFont:(UIFont *)font;
-(UIFont *)bulletTextFont;

/**
 *  自定义的视图 ⌚️2016年5月12日添加
 *
 */
//设置用户头像
-(void)setbulletImageView:(UIImageView *)imageView;
-(UIImageView*)bulletImageView;

//设置用户名
-(void)setbulletName:(NSString *)name;
-(NSString*)bulletName;

//设置内容
-(void)setbulletText:(NSString *)text;
-(NSString *)bulletText;

//设置高度
-(void)setBulletHeight:(CGFloat)height;
-(CGFloat)bulletHeight;

//设置动画时长
-(void)setBulletAnimationDuration:(float)duration;
-(float)bulletAnimationDuration;

//设置速度比率
-(void)setBulletAnimationSpeedRate:(float)speedRate;
-(float)bulletAnimationSpeedRate;

-(NSMutableDictionary *)settingDic;



@end
