//
//  VLSOrderView.m
//  VLiveShow
//
//  Created by tom.zhu on 2016/11/10.
//  Copyright © 2016年   . All rights reserved.
//

#import "VLSOrderView.h"
#import "VLiveShow-Swift.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface VLSOrderView ()
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderTitle;
@property (weak, nonatomic) IBOutlet UIButton *attendanceBtn;
@property (weak, nonatomic) IBOutlet UIButton *audienceBtn;
@property (weak, nonatomic) IBOutlet UILabel *recommendLabel;

@property (weak, nonatomic) IBOutlet UILabel *payTitle;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@end

@implementation VLSOrderView
- (void)setModel:(VLSCourseModel *)model {
    _model = model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:model.picTeacher] placeholderImage:ICON_PLACEHOLDER];
    
    NSArray* priceArr = [NSArray arrayWithObjects:[NSArray arrayWithObjects:self.model.priceAudience.stringValue, RGB16(COLOR_FONT_FF1130), [UIFont systemFontOfSize:SIZE_FONT_20], nil], [NSArray arrayWithObjects:LocalizedString(@"VDIAMONDS"), RGB16(COLOR_FONT_FF1130), [UIFont systemFontOfSize:SIZE_FONT_16], nil], nil];
    self.priceLabel.attributedText = [self CustomAttributeStrings:priceArr];
    NSString* remain = [LocalizedString(@"COURSE_DETAIL_NUM") stringByReplacingOccurrencesOfString:@"n" withString:model.numLessons.stringValue];
    [self.remainLabel setText:remain];
    self.balanceLabel.text = [NSString stringWithFormat:@"%@%@%@", LocalizedString(@"balance"), model.surplusDiamond, LocalizedString(@"VDIAMONDS")];
    
    if ([self.model.surplusParticipants intValue] <= 0) {//禁止点击嘉宾席
        self.attendanceBtn.enabled = NO;
        [self.attendanceBtn setTintColor:RGB16(COLOR_FONT_C7C7C7)];
        self.attendanceBtn.layer.borderColor = RGB16(COLOR_FONT_C7C7C7).CGColor;
    }
    if (![model.allowAudience boolValue]) {
        [self attendanceClicked:self.attendanceBtn];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path addArcWithCenter:self.headImageView.center radius:(self.headImageView.width_sd / 2) startAngle:0 endAngle:2 * M_PI clockwise:NO];
    CAShapeLayer *circleLayer = [CAShapeLayer new];
    circleLayer.path = path.CGPath;
    [circleLayer setFillColor:[[[UIColor purpleColor] colorWithAlphaComponent:.5f] CGColor]];
    [self.layer insertSublayer:circleLayer below:self.headImageView.layer];
    circleLayer.shadowOpacity = 1;
    circleLayer.shadowOffset = CGSizeMake(0, -2);
    circleLayer.shadowColor = RGB16(COLOR_BG_F4F4F4).CGColor;
    circleLayer.shadowRadius = -1;
    circleLayer.shouldRasterize = YES;
    circleLayer.rasterizationScale = [UIScreen mainScreen].scale;
    [circleLayer setNeedsDisplay];
    
    [self.attendanceBtn setTitle:LocalizedString(@"INVITE_GUEST_SEAT") forState:UIControlStateNormal];
    [self.attendanceBtn setTitleColor:RGB16(COLOR_FONT_9B9B9B) forState:UIControlStateNormal];
    [self.attendanceBtn setTitleColor:RGB16(COLOR_FONT_F12F47) forState:UIControlStateSelected];
    [self.attendanceBtn setTitle: LocalizedString(@"INVITE_GUEST_SEAT_IS_FULL") forState: UIControlStateDisabled];
    
    [self.audienceBtn setTitle:LocalizedString(@"INVITE_AUDIENCE_SEAT") forState:UIControlStateNormal];
    [self.audienceBtn setTitleColor:RGB16(COLOR_FONT_9B9B9B) forState:UIControlStateNormal];
    [self.audienceBtn setTitleColor:RGB16(COLOR_FONT_F12F47) forState:UIControlStateSelected];
    
    self.orderTitle.text = LocalizedString(@"buy_seats");
    self.recommendLabel.text = LocalizedString(@"COURSE_DETAIL_USER");
    self.payTitle.text = LocalizedString(@"payViaDiamond");
    [self.payBtn setTitle:[NSString stringWithFormat:@"%@  >", LocalizedString(@"RECHARGE")] forState:UIControlStateNormal];
    
    [self.audienceBtn setSelected:YES];
    self.audienceBtn.layer.borderColor = RGB16(COLOR_FONT_F12F47).CGColor;
    self.attendanceBtn.layer.borderColor = RGB16(COLOR_FONT_9B9B9B).CGColor;
    
    //变更样式 -update 2016-12-09 16:53:00 rock --begin
    [self.priceLabel setTextColor:RGB16(COLOR_FONT_FF1130)];
    self.priceLabel.font = [UIFont systemFontOfSize:SIZE_FONT_16];
    [self.remainLabel setTextColor:RGB16(COLOR_FONT_777777)];
    self.remainLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
    //购买席位
    [self.orderTitle setTextColor:RGB16(COLOR_FONT_4A4A4A)];
    self.orderTitle.font = [UIFont systemFontOfSize:SIZE_FONT_16];
    [self.recommendLabel setTextColor:RGB16(COLOR_FONT_777777)];
    self.recommendLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
    //使用v钻
    [self.payTitle setTextColor:RGB16(COLOR_FONT_4A4A4A)];
    self.payTitle.font = [UIFont systemFontOfSize:SIZE_FONT_16];
    [self.balanceLabel setTextColor:RGB16(COLOR_FONT_C7C7C7)];
    self.balanceLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
    //充值按钮
    [self.payBtn setTitleColor:RGB16(COLOR_FONT_53ABFF) forState:UIControlStateNormal];
    
    //变更样式 -update 2016-12-09 16:53:00 rock --end
}

- (void)orderViewDidChoosen:(UIButton*)button {
    self.attendanceBtn.selected = button == self.attendanceBtn;
    self.audienceBtn.selected = button == self.audienceBtn;
    UIColor *borderColor_SEL = RGB16(COLOR_FONT_F12F47);
    UIColor *borderColor_NOR = RGB16(COLOR_FONT_777777);
    NSArray *btnAry = @[self.attendanceBtn, self.audienceBtn];
    for (UIButton *btn in btnAry) {
        btn.selected = button == btn;
        btn.layer.borderColor = button == btn ? borderColor_SEL.CGColor : borderColor_NOR.CGColor;
    }
    if (button == self.attendanceBtn) {//席位联动效果
        NSString* vipText1 = [LocalizedString(@"COURSE_DETAIL_VIPUSER") substringWithRange:NSMakeRange(0, 2)];
        NSString* vipText2 = [LocalizedString(@"COURSE_DETAIL_VIPUSER") substringWithRange:NSMakeRange(3, LocalizedString(@"COURSE_DETAIL_VIPUSER").length - 3)];
        
        NSArray* vipArr = [NSArray arrayWithObjects:[NSArray arrayWithObjects:vipText1, RGB16(COLOR_FONT_777777), [UIFont systemFontOfSize:SIZE_FONT_14], nil], [NSArray arrayWithObjects:self.model.surplusParticipants.stringValue, RGB16(COLOR_FONT_FF1130), [UIFont systemFontOfSize:SIZE_FONT_18], nil], [NSArray arrayWithObjects:vipText2, RGB16(COLOR_FONT_777777), [UIFont systemFontOfSize:SIZE_FONT_14], nil], nil];
        self.recommendLabel.attributedText = [self CustomAttributeStrings:vipArr];
        //价格
        NSArray* priceArr = [NSArray arrayWithObjects:[NSArray arrayWithObjects:self.model.priceParticipant.stringValue, RGB16(COLOR_FONT_FF1130), [UIFont systemFontOfSize:SIZE_FONT_20], nil], [NSArray arrayWithObjects:LocalizedString(@"VDIAMONDS"), RGB16(COLOR_FONT_FF1130), [UIFont systemFontOfSize:SIZE_FONT_16], nil], nil];
        self.priceLabel.attributedText = [self CustomAttributeStrings:priceArr];
    } else {
        self.recommendLabel.text = LocalizedString(@"COURSE_DETAIL_USER");
        NSArray* priceArr = [NSArray arrayWithObjects:[NSArray arrayWithObjects:self.model.priceAudience.stringValue, RGB16(COLOR_FONT_FF1130), [UIFont systemFontOfSize:SIZE_FONT_20], nil], [NSArray arrayWithObjects:LocalizedString(@"VDIAMONDS"), RGB16(COLOR_FONT_FF1130), [UIFont systemFontOfSize:SIZE_FONT_16], nil], nil];
        self.priceLabel.attributedText = [self CustomAttributeStrings:priceArr];
    }
}

- (NSMutableAttributedString*)CustomAttributeStrings:(NSArray*)arr
{
    NSMutableAttributedString* mStr = [[NSMutableAttributedString alloc] init];
    for (NSArray* text in arr) {
        if (text.count == 3) {
            NSAttributedString* attrText = [[NSAttributedString alloc] initWithString:text[0] attributes:@{NSForegroundColorAttributeName: text[1], NSFontAttributeName: text[2]}];
            [mStr appendAttributedString:attrText];
        }
    }
    return mStr;
}

- (IBAction)closeDidClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(orderViewCloseClicked)]) {
        [self.delegate orderViewCloseClicked];
    }
}

- (IBAction)payClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(orderViewPayClicked)]) {
        [self.delegate orderViewPayClicked];
    }
}

- (IBAction)attendanceClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(orderViewAttendanceClicked)]) {
        [self.delegate orderViewAttendanceClicked];
    }
    [self orderViewDidChoosen:sender];
    if ([self.model.surplusParticipants intValue] <= 0) {
        [self audienceClicked:self.audienceBtn];
    }
}

- (IBAction)audienceClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(orderViewAudienceClicked)]) {
        [self.delegate orderViewAudienceClicked];
    }
    [self orderViewDidChoosen:sender];
}

- (void)updateBalanc:(NSString*)newbalance {
    self.balanceLabel.text = [NSString stringWithFormat:@"%@%@%@", LocalizedString(@"balance"), newbalance, LocalizedString(@"VDIAMONDS")];
}

@end
