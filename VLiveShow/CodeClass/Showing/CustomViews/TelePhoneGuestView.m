//
//  TelePhoneGuestView.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "TelePhoneGuestView.h"
#import "UIImage+colorImage.h"
#import "VLSTextField.h"
#import "SectionsViewController.h"
#import "AccountManager.h"
#import "VLSPhoneLoginViewController.h"
#import "AppDelegate.h"
#import "VLSNavigationController.h"
#import "VLSTabBarController.h"
#import "UIView+CurretVC.h"
#import "MBProgressHUD+Add.h"
#import "VLSCountryAreaManager.h"
#import "VLSCountryView.h"
#import "VLSUserTrackingManager.h"

@interface TelePhoneGuestView ()<SecondViewControllerDelegate, VLSCountryViewDelegate>{
    //获取到的键盘的高度
    CGFloat keyHeight;
}

@property (nonatomic, strong)VLSCountryView *countryView;
/// 手机号输入框
@property (nonatomic, strong)VLSTextField *phoneTF;
/// 验证码输入框
@property (nonatomic, strong)VLSTextField *verCodeTF;
/// 小手机图标
@property (nonatomic, strong)UIImageView *phoneIcon;

/// 选择国家
@property (nonatomic, strong)UILabel *countryLabel;

/// 去除空格后的手机号
@property (nonatomic, strong)NSString *phoneNum;
/// 警告图标
@property (nonatomic, strong)UIImageView *warningImageView;
/// 警告的提示语
@property (nonatomic, strong)UILabel *warningLabel;

/// 临时字符串
@property (nonatomic, copy)NSString *temStr;
/// 是否验证过手机号存在的标记
@property (nonatomic, assign)BOOL phoneExistFlag;

/// 提示框
@property (nonatomic, strong)MBProgressHUD *HUD;


@end


@implementation TelePhoneGuestView

- (instancetype)initTelePhoneVerifyWithFrame:(CGRect)frame
{
    if (self = [self initWithFrame:frame]) {
        [self TelePhoneVerify];
        
        // 加载子视图 textfield
        [self setupUI];
        
        //创建键盘将要显示的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardUp:) name:UIKeyboardWillShowNotification object:nil];
        //键盘将要收起的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDown:) name:UIKeyboardWillHideNotification object:nil];
        
    }
    return self;
}

- (void)setupUI {
    
    
    [self addSubview:self.countryView];
    self.countryView.sd_layout
    .leftSpaceToView(self,30)
    .rightSpaceToView(self, 30)
    .topSpaceToView(self.label, 10. / 667 * SCREEN_HEIGHT)
    .heightIs(40);
    
    [self addSubview:self.phoneTF];
    self.phoneTF.sd_layout
    .leftSpaceToView(self, 30)
    .rightSpaceToView(self, 30)
    .topSpaceToView(self.countryView, 10)
    .heightIs(40);
    
   
    [self addSubview:self.verCodeTF];
    self.verCodeTF.sd_layout
    .leftSpaceToView(self, 30)
    .rightSpaceToView(self, 30)
    .topSpaceToView(self.phoneTF, 10)
    .heightIs(40);
    
    [self.closeButton addTarget:self action:@selector(closeSelf) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 显示警告
- (void)showWarningWithString:(NSString *)str {
    
    self.warningLabel.text = str;
    
    [self addSubview:self.warningLabel];
    [self.warningLabel setSingleLineAutoResizeWithMaxWidth:SCREEN_WIDTH];
    self.warningLabel.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(self.verCodeTF, 10)
    .heightIs(30);
    
    [self addSubview:self.warningImageView];
    self.warningImageView.sd_layout
    .rightSpaceToView(self.warningLabel, 5)
    .centerYEqualToView(self.warningLabel)
    .widthIs(15)
    .heightIs(15);
    
}

#pragma mark - VLSCountryViewDelegate

- (void)selectCountryArea {
    
    SectionsViewController *stVC = [SectionsViewController new];
    stVC.delegate = self;
    // 获取当前view所在的控制器
    VLSTabBarController *vc = (VLSTabBarController *)[self getCurrentVC];
    
    [vc.viewControllers.firstObject pushViewController:stVC animated:YES];
}

#pragma mark - 隐藏警告
- (void)hideWarning {
    [self.warningLabel removeFromSuperview];
    [self.warningImageView removeFromSuperview];
}


- (void)closeSelf
{
    [UIAlertController showAlertInViewController:[self getCurrentVC] withTitle:nil message:LocalizedString(@"INVITE_CANCLE_SURE_TELEPHONE") cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"INVITE_CANCLE_APPLLY"),LocalizedString(@"INVITE_GOON_APPLY")]  tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if (buttonIndex - 2 == 0) {
            [self removeFromSuperview];
        }
    }];
    
    
}
- (void)TelePhoneVerify
{
    self.label.text = LocalizedString(@"INVITE_CONFIRMATION_PHONE");;
    [self.onLineButton setTitle:LocalizedString(@"INVITE_SUBMIT_VERCODE") forState:UIControlStateNormal];
    [self.onLineButton setBackgroundImage:[UIImage ImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateSelected];
    [self.onLineButton addTarget:self action:@selector(VerifyNumber) forControlEvents:UIControlEventTouchUpInside];
    
    //默认选中状态 更换背景颜色
    [self changeEnableStatu:NO];
}

- (void)VerifyNumber
{
    if (self.phoneExistFlag) {
        [self verifyCallBack];
    } else {
        [self phoneExists];
    }

}

- (void)verifyCallBack {
    // 网络请求验证手机号
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.verificationCode = self.verCodeTF.text;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
        NSString *showStr = [NSString stringWithFormat:@"%@ %@", LocalizedString(@"MINE_PHONENUMBER"), _phoneTF.text];
        [MBProgressHUD show:showStr detailText:LocalizedString(@"INVITE_VERIFY_SUCCESS") icon:@"broadcast_audience_view_verify_succeed" view: app.window];
        
        [AccountManager sharedAccountManager].account.mobilePhone = self.phoneNum;
        
        [self removeFromSuperview];
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {

            ManagerEvent *event = result;
            if (event.code == 20003) {
                self.verCodeTF.lineLabel.backgroundColor = [UIColor redColor];
                self.verCodeTF.textColor = [UIColor redColor];
                [self showWarningWithString:LocalizedString(@"VERCODE_ERROR")];
            }
        }
    };
    
    [[VLSUserTrackingManager shareManager] trackingRoomBindPhone:model bindType:@"request_link"] ;
    [AccountManager phoneBind:model callback:callback];
    
    [[[UIApplication sharedApplication].delegate window] endEditing:YES];
}

/**
 *  statu 为yes 代表button可以点击 为NO代表button不可点击
 */
- (void)changeEnableStatu:(BOOL)statu
{
    if (statu) {
        self.onLineButton.selected = NO;
        self.onLineButton.userInteractionEnabled = YES;
    }else{
        self.onLineButton.selected = YES;
        self.onLineButton.userInteractionEnabled = NO;
    }
}

#pragma mark - keyboard通知处理
- (void)keyboardUp:(NSNotification*)notify{
    NSValue *value = [notify.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    //得到键盘高度
    CGFloat tempHeight = value.CGRectValue.size.height;
    //为了解决反复拖动键盘上联想内容部分，需要判断keyHeight和tempHeight是否相同，然后实时记录键盘高度的变化
    if (keyHeight != tempHeight) {
        //修改contentView的高度
        CGRect contentFrame = self.superview.frame;
        contentFrame.origin.y -= tempHeight - keyHeight;
        self.superview.frame = contentFrame;
        keyHeight = tempHeight;
    }
}

- (void)keyboardDown:(NSNotification*)notifiy{
    //修改contentView的高度
    CGRect contentFrame = self.superview.frame;
    contentFrame.origin.y = SCREEN_HEIGHT - contentFrame.size.height;
    self.superview.frame = contentFrame;
    //回到初始状态
    keyHeight = 0;
}



#pragma mark - 验证按钮方法

- (void)verCodeAction:(id)sender {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
  
    if (![self checkVerCodeBtnEnableAndShowPhoneWarning]) return;
    
    // 验证码倒计时
    [self verCodeTime:sender];
    
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
        }
    };
    
    [AccountManager sendVerCode:model callback:callback];
    
}

- (BOOL)checkVerCodeBtnEnableAndShowPhoneWarning {
    
    BOOL flag = YES;
    
    if (self.phoneTF.text.length == 0) {
        
            [self showPhoneInputWrong];
            flag = NO;
        
    }
    
    if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
        if (self.phoneNum.length != 11) {
            
            [self showPhoneInputWrong];
               flag = NO;
        }
    } else if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
        if (self.phoneTF.text.length) {
            
            NSString *temStr = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *firstChar = [self.phoneTF.text substringToIndex:1];
                if ([firstChar isEqualToString:@"0"]) {
                    if (temStr.length != 10) {
                        [self showPhoneInputWrong];
                         flag = NO;
                    }
                } else {
                    if (temStr.length != 9) {

                        [self showPhoneInputWrong];
                         flag = NO;
                    }
            }
        }
    } else {
        if (self.phoneNum.length < 6) {
            [self showPhoneInputWrong];
             flag = NO;
        }
    }
    return flag;
}

- (void)showPhoneInputWrong {
    self.phoneTF.lineLabel.backgroundColor = [UIColor redColor];
    self.phoneTF.textColor = [UIColor redColor];
    [self showWarningWithString:LocalizedString(@"ALERT_MESSAGE_RIGHT_PHONENUM")];
}

#pragma mark - 点击发送验证码后的倒计时方法

- (void)verCodeTime:(id)sender {
    UIButton *verCodeButton = sender;
    __block int timeout=VERCODE_TIME; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){
            //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [verCodeButton setTitle:LocalizedString(@"VERCODE_SEND_AGAIN") forState:UIControlStateNormal];
                [verCodeButton setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateNormal];
                verCodeButton.userInteractionEnabled = YES;
                self.phoneIcon.image = [UIImage imageNamed:@"phone_red"];
            });
        }else{
            int seconds = timeout % VERCODE_TIME;
            NSString *strTime = [NSString stringWithFormat:@"%d", seconds];
            if ([strTime isEqualToString:@"0"]) {
                strTime = [NSString stringWithFormat:@"%d",VERCODE_TIME];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1];
                [verCodeButton setTitle:[NSString stringWithFormat:@"%@s%@",strTime, LocalizedString(@"VERCODE_SEND_LATER")] forState:UIControlStateNormal];
                [verCodeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                self.phoneIcon.image = [UIImage imageNamed:@"phone_gray"];
                [UIView commitAnimations];
                verCodeButton.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

#pragma mark - 验证手机号是否存在

- (void)phoneExists {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.phoneExistFlag = YES;
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if ([[result objectForKey:@"existPhone"] boolValue]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"ALERT_MESSAGE_LOGIN_AGAIN") preferredStyle:UIAlertControllerStyleAlert];
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                [self.phoneTF endEditing:YES];
                [self.verCodeTF endEditing:YES];
                
                VLSPhoneLoginViewController *phoneLoginVC = [[VLSPhoneLoginViewController alloc] init];
                phoneLoginVC.phoneNumber = self.phoneTF.text;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginAgain" object:nil];
                [(AppDelegate *)[UIApplication sharedApplication].delegate showLoginController];
                
            }];
            
            // 取消按钮
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"换个手机号" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.phoneTF.text = nil;
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];

            
            [[[UIApplication sharedApplication].delegate window].rootViewController presentViewController:alertController animated:YES completion:nil];

        } else {
            [self verifyCallBack];
        }
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
        }
    };
    
    [AccountManager phoneExists:model callback:callback];
}

- (void)phoneExistsOnly {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.phoneExistFlag = YES;
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if ([[result objectForKey:@"existPhone"] boolValue]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"ALERT_MESSAGE_LOGIN_AGAIN") preferredStyle:UIAlertControllerStyleAlert];
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                [self.phoneTF endEditing:YES];
                [self.verCodeTF endEditing:YES];
                
                VLSPhoneLoginViewController *phoneLoginVC = [[VLSPhoneLoginViewController alloc] init];
                phoneLoginVC.phoneNumber = self.phoneTF.text;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginAgain" object:nil];
                [(AppDelegate *)[UIApplication sharedApplication].delegate showLoginController];
                
            }];
            
            // 取消按钮
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"换个手机号" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.phoneTF.text = nil;
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            
            
            [[[UIApplication sharedApplication].delegate window].rootViewController presentViewController:alertController animated:YES completion:nil];
            
        }
    };
    callback.errorBlock = ^(id result){
        self.phoneExistFlag = NO;
        if ([result isKindOfClass:[ManagerEvent class]]) {
        }
    };
    
    [AccountManager phoneExists:model callback:callback];
}

#pragma mark - SecondViewControllerDelegate

- (void)secondData:(NSString *)data withCountryName:(NSString *)name{
    self.phoneExistFlag = NO;
    self.countryLabel.text = [NSString stringWithFormat:@"+%@", data];
    self.countryView.countryLabel.text = name;
    [VLSCountryAreaManager shareManager].area = self.countryLabel.text;
    [VLSCountryAreaManager shareManager].name = name;
    if ([[self.countryLabel.text substringFromIndex:1] isEqualToString:@"86"]) {
        if (self.phoneTF.text.length > 3 && self.phoneTF.text.length < 8) {
            self.phoneTF.text = [NSString stringWithFormat:@"%@ %@", [self.phoneTF.text substringToIndex:3], [self.phoneTF.text substringFromIndex:3]];
        } else if (self.phoneTF.text.length > 8) {
            self.phoneTF.text = [NSString stringWithFormat:@"%@ %@ %@", [self.phoneTF.text substringToIndex:3], [self.phoneTF.text substringWithRange:NSMakeRange(3, 4)], [self.phoneTF.text substringFromIndex:7]];
            
        }
        
    } else {
        self.phoneTF.text = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
    }
    
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];

}

#pragma mark - UITextField 输入判断

- (void)verCodeTFAction {
    
    if (self.verCodeTF.textColor == [UIColor redColor]) {
        self.verCodeTF.textColor = [UIColor blackColor];
        self.verCodeTF.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        [self hideWarning];
    }
    
    if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
        if (self.phoneNum.length == 11 && self.verCodeTF.text.length == 4) {
            
            [self changeEnableStatu:YES];
            
        } else {
            [self changeEnableStatu:NO];
        }
    } else {
        if (self.phoneNum.length > 0 && self.verCodeTF.text.length == 4) {
            
            [self changeEnableStatu:YES];
            
        } else {
            
            [self changeEnableStatu:NO];
        }
    }
    
//    // 验证码只能输入四位
//    if (self.verCodeTF.text.length > 4) {
//        self.verCodeTF.text = [self.verCodeTF.text substringToIndex:4];
//    }
}

- (void)phoneTFAction {
    
    if (self.phoneTF.textColor == [UIColor redColor]) {
        self.phoneTF.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        self.phoneTF.textColor = [UIColor blackColor];
        [self hideWarning];
        
    }
    
    if (self.phoneTF.text.length > 0 &&  self.phoneTF.text.length > self.temStr.length) {
        
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
            [self phoneFormatWithChinaMobile];
        }
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
            NSString *firstChar = [self.phoneTF.text substringToIndex:1];
            if ([firstChar isEqualToString:@"0"]) {
                if (self.phoneTF.text.length == 10) {
                    [self.phoneTF endEditing:YES];
                    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self phoneExistsOnly];
                }
//                if (self.phoneTF.text.length > 10) {
//                    self.phoneTF.text = [self.phoneTF.text substringToIndex:10];
//                    [self.phoneTF endEditing:YES];
//                }
            } else {
                if (self.phoneTF.text.length == 9) {
                    [self.phoneTF endEditing:YES];
                    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self phoneExistsOnly];
                }
//                if (self.phoneTF.text.length > 9) {
//                    [self.phoneTF endEditing:YES];
//                    self.phoneTF.text = [self.phoneTF.text substringToIndex:9];
//                }
            }
        }
    }
    self.temStr = self.phoneTF.text;
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
}


#pragma mark - 手机号格式化

- (void)phoneFormatWithChinaMobile {
    if (self.phoneTF.text.length == 3) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@ ",self.phoneTF.text];
        
    }
    
    if (self.phoneTF.text.length == 8) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@ ",self.phoneTF.text];
        
    }
    
    if (self.phoneTF.text.length == 13) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@",self.phoneTF.text];
        [self.phoneTF endEditing:YES];
        self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self phoneExistsOnly];
    }
    if (self.phoneTF.text.length > 13) {
        
        self.phoneTF.text = [self.phoneTF.text substringToIndex:13];
        [self.phoneTF endEditing:YES];
        
    }
}
#pragma mark - getters

- (UIImageView *)warningImageView {
    if (!_warningImageView) {
        
        self.warningImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"livebefore_warn"]];
        
    }
    return _warningImageView;
}

- (UILabel *)warningLabel {
    if (!_warningLabel) {
        self.warningLabel = [[UILabel alloc] init];
        self.warningLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.warningLabel.textColor = [UIColor redColor];
        self.warningLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _warningLabel;
}

- (VLSCountryView *)countryView {
    if (!_countryView) {
        self.countryView = [[VLSCountryView alloc] init];
        self.countryView.delegate = self;
    }
    return _countryView;
}


- (VLSTextField *)phoneTF {
    if (!_phoneTF) {
        // 创建国家选择按钮
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        self.countryLabel = [[UILabel alloc] init];
        self.countryLabel.font = [UIFont systemFontOfSize:15];
        self.countryLabel.text = [VLSCountryAreaManager shareManager].area;
        self.countryLabel.adjustsFontSizeToFitWidth = YES;
        [leftView addSubview:self.countryLabel];
        self.countryLabel.sd_layout
        .leftSpaceToView(leftView, 0)
        .topSpaceToView(leftView, 0)
        .bottomSpaceToView(leftView, 0)
        .widthIs(60);
        
        self.phoneTF = [[VLSTextField alloc] init];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
        self.phoneTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PHONE_NUMBER") attributes:attributes];
        self.phoneTF.keyboardType = UIKeyboardTypeNumberPad;
        self.phoneTF.leftView = leftView;
        self.phoneTF.leftViewMode = UITextFieldViewModeAlways;
        [self.phoneTF addTarget:self action:@selector(phoneTFAction) forControlEvents:UIControlEventEditingChanged];

    }
    return _phoneTF;
}


- (VLSTextField *)verCodeTF {
    if (!_verCodeTF) {
        
        // 创建验证码输入框左侧视图 View
        UILabel *codeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        codeLabel.text = LocalizedString(@"LOGIN_LABEL_VERCODE");
        codeLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
        
        // 创建验证码输入框右侧视图 View
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        self.phoneIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone_gray"]];
        [rightView addSubview:self.phoneIcon];
        self.phoneIcon.sd_layout
        .leftSpaceToView(rightView, 0)
        .topSpaceToView(rightView, 10)
        .bottomSpaceToView(rightView, 10)
        .widthIs(10);
        
        // 创建发送验证码按钮
        UIButton *verCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [verCodeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [verCodeButton setTitle:LocalizedString(@"VERCODE_SEND_VERCODE") forState: UIControlStateNormal];
        [verCodeButton addTarget:self action:@selector(verCodeAction:) forControlEvents:UIControlEventTouchUpInside];
        verCodeButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [rightView addSubview:verCodeButton];
        verCodeButton.sd_layout
        .leftSpaceToView(self.phoneIcon, 0)
        .rightSpaceToView(rightView, 0)
        .topSpaceToView(rightView, 0)
        .bottomSpaceToView(rightView, 0);
        
        // 创建验证码输入框
        self.verCodeTF = [[VLSTextField alloc] init];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
        self.verCodeTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PHONE_CODE") attributes:attributes];
        self.verCodeTF.keyboardType = UIKeyboardTypeNumberPad;
        self.verCodeTF.rightView = rightView;
        self.verCodeTF.leftView = codeLabel;
        self.verCodeTF.leftViewMode = UITextFieldViewModeAlways;
        self.verCodeTF.rightViewMode = UITextFieldViewModeAlways;
        [self.verCodeTF addTarget:self action:@selector(verCodeTFAction) forControlEvents:UIControlEventEditingChanged];
    }
    return _verCodeTF;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
