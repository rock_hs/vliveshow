//
//  closeRoomActionViewController.m
//  VLiveShow
//
//  Created by SuperGT on 16/7/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "closeRoomActionViewController.h"
#import "closeRoomActionView.h"
@interface closeRoomActionViewController ()

@property (nonatomic, strong) closeRoomActionView *actionView;

@end

@implementation closeRoomActionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.actionView];
}

- (closeRoomActionView *)actionView
{
    if (_actionView == nil) {
        _actionView = [[closeRoomActionView alloc]initWithFrame:self.view.bounds];
        [_actionView.cancleBtn addTarget:self action:@selector(cancle) forControlEvents:UIControlEventTouchUpInside];
        [_actionView.cancleGuestBtn addTarget:self action:@selector(cancleGuest) forControlEvents:UIControlEventTouchUpInside];

        [_actionView.closeRoom addTarget:self action:@selector(closeroom) forControlEvents:UIControlEventTouchUpInside];

    }
    return _actionView;
}

- (void)closeroom
{
//    [self cancle];
    if ([_delegate respondsToSelector:@selector(quitRoomAction)]) {
        [_delegate quitRoomAction];
    }
}

- (void)cancleGuest
{
    [self cancle];
    if ([_delegate respondsToSelector:@selector(cancleGuestAction)]) {
        [_delegate cancleGuestAction];
    }
    
}

- (void)cancle
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{

    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
