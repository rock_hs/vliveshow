//
//  VLSGiftShow.m
//  balabala
//
//  Created by SuperGT on 16/5/11.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import "VLSGiftShow.h"
#import "VLSGiftItem.h"

#define VLSH [UIScreen mainScreen].bounds.size.height
#define VLSW [UIScreen mainScreen].bounds.size.width

@interface VLSGiftShow ()
@property (nonnull,nonatomic,strong) UIScrollView *scrollview;
@property (nonatomic,strong) GiftShow giftBlock;
// 保存当前已选择的礼物
@property (nonnull,nonatomic,strong) VLSGiftItem *item;

@property (nonnull,nonatomic,strong) NSArray *images;


@end

@implementation VLSGiftShow

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        self.frame = CGRectMake(0, VLSH - 62, VLSW, 120);
        self.scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, VLSW, self.frame.size.height)];
        self.scrollview.pagingEnabled = YES;
//        self.backgroundColor = [UIColor blackColor];
//        self.scrollview.backgroundColor = [UIColor greenColor];
        [self addSubview:self.scrollview];
        
    }
    return self;
    
}

- (void)setGiftShowBlock:(GiftShow)block
{
    self.giftBlock = block;
}

- (void)setsetItemImages:(NSArray * _Nonnull)images;
{
    if (images.count > 0) {
        self.images = images;
        [self showGift];
    }else{
        
        NSLog(@"无数据");
    }
}

- (void)showGift
{
    NSInteger i = (self.images.count-1)/8;
    
    self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width*(i+1), self.scrollview.frame.size.height);
    
    //原始坐标
    CGFloat X = 0;
    CGFloat Y =0;
    for (int i = 0; i < self.images.count; i ++) {
        // 根据索引计算X坐标
        X = (i%4)*(VLSW/4)+(i/8)*VLSW;
        // 根据索引计算Y坐标
        Y = ((i/4)%2)*(self.frame.size.height/2);
        VLSGiftItem *giftitem = [[VLSGiftItem alloc]initWithFrame:CGRectMake(X, Y, VLSW/4, self.frame.size.height/2)];
        giftitem.tag = 1000+i;
        [giftitem setGiftImage:self.images[i] text:[NSString stringWithFormat:@"%ld",(long)giftitem.tag]];
        
        __weak VLSGiftShow *copy_self = self;
        
        [giftitem setgiftItemBlock:^(VLSGiftItem * _Nonnull item) {
            if (item.GiftSelect) {
                // 如果被选中，不做改动，保存在当前视图里
//                self.item = item;
                [item setGiftSelect:NO];
            }else{
                // 如果没有被选中，判断一下是否有保留的item
                if (self.item) {
                    // 如果有,替换
                    [self.item setGiftSelect:NO];
                    [item setGiftSelect:YES];
                    self.item = item;
                }else{
                    [item setGiftSelect:YES];
                    self.item = item;
                }
            }
            /**
             *  在这里可以再添加一些代码，比如设置发送button的可点击，保存当前是礼物类型，在点击事件中发送服务器
             *  也可以如下所示，将item.tag继续传递给上层view
             */
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            [dic setValue:self.images[i] forKey:@"imageName"];
            [dic setValue:[NSString stringWithFormat:@"%ld",(long)item.tag] forKey:@"imageTag"];
            
            copy_self.giftBlock(dic);
            
        }];
        
        [self.scrollview addSubview:giftitem];
        
    }
    
    [UIView animateWithDuration:1 animations:^{
        CGRect ANframe = self.frame;
        ANframe.origin.y -= 120;
        self.frame = ANframe;
        
    }];
}


@end
