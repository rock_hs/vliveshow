//
//  VLSgiftItem.h
//  balabala
//
//  Created by SuperGT on 16/5/11.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef enum GIFTTYPE (
//    HuaType
//);

@class VLSGiftItem;
typedef void  (^giftItemBlock) (VLSGiftItem * _Nonnull item);

@interface VLSGiftItem : UIView


//@property (nonnull,nonatomic,strong) UIImage *GiftIcon;
@property (nonnull,nonatomic,strong) UIButton *SelectButton;
//@property (nonatomic,assign) enum GIFTTYPE type;
@property (nonnull,nonatomic,strong) giftItemBlock giftItem;
@property (nonatomic) BOOL GiftSelect;


- (void)setGiftImage:(NSString * _Nonnull)image text:(NSString * _Nonnull)text;
- (void)setgiftItemBlock:(giftItemBlock _Nonnull)blcok;


@end
