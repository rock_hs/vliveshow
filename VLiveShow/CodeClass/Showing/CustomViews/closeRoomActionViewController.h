//
//  closeRoomActionViewController.h
//  VLiveShow
//
//  Created by SuperGT on 16/7/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, VLSprefersentStyle) {
    VLSprefersentAlter,
    VLSprefersentSheet
};

@protocol closeRoomDelegate <NSObject>

@optional
// 结束连线
- (void)cancleGuestAction;
// 退出房间
- (void)quitRoomAction;

@end

@interface closeRoomActionViewController : UIViewController

@property (nonatomic, weak) id<closeRoomDelegate> delegate;

@property (nonatomic, assign) VLSprefersentStyle prefersentStyle;

@end
