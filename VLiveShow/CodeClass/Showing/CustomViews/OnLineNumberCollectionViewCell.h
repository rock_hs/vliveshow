//
//  OnLineNumberCollectionViewCell.h
//  balabala
//
//  Created by SuperGT on 16/5/12.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSUserProfile.h"
@interface OnLineNumberCollectionViewCell : UICollectionViewCell

@property (nonnull,nonatomic, retain) VLSUserProfile *onLineModel;
// 在线的头像
@property (nonnull,nonatomic,strong) UIImageView *OnLineIcon;
@end
