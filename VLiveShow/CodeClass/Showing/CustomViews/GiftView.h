//
//  GiftView.h
//  SendGiftAnimation
//
//  Created by SXW on 16/5/19.
//  Copyright © 2016年 盛宣伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftView : UIView
/*用户头像*/
@property (nonatomic, strong) UIImageView *headImageView;
/*礼物图片*/
@property (nonatomic, strong) UIImageView *giftImageView;
/*用户名*/
@property (nonatomic, strong) UILabel *userName;
/*礼物内容*/
@property (nonatomic, strong) UILabel *userContent;

//背景模板
@property (nonatomic, strong) UIImageView *backgroudImageView;

@end
