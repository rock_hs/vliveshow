//
//  VLSAudiencedView.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAudiencesView.h"
#import "OnLineNumberCollectionViewCell.h"
#import "VLSHeartManager.h"

@interface VLSAudiencesView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,nonnull,strong) UICollectionView *collection;

@property (nonatomic,nonnull,strong) NSMutableArray *reloadArray;

@property (nonatomic,strong) NSTimer *Audientimer;

@property (nonatomic, assign) NSInteger maxAudiences;

@end

@implementation VLSAudiencesView
- (void)setOriention:(KLiveRoomOrientation)oriention {
    _oriention = oriention;
    if (self.oriention == KLiveRoomOrientation_LandscapeLeft || self.oriention == KLiveRoomOrientation_LandscapeRight) {
        self.maxAudiences = 21;
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.dataArray =[NSMutableArray array];
        [self addSubview:self.collection];
        self.collection.sd_layout
        .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
        if ([VLSHeartManager shareHeartManager].heartModel.liveMaxOnlineAvator) {
            self.maxAudiences = [VLSHeartManager shareHeartManager].heartModel.liveMaxOnlineAvator + 1;
        }else{
            self.maxAudiences = 11;
        }
    }
    return self;
}

- (void)setDataArray:(NSMutableArray *)dataArray
{
    _dataArray = dataArray;
//    [self.collection reloadData];
    self.Audientimer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(reloadcollection) userInfo:nil repeats:YES];
}

- (void)reloadcollection
{
    _reloadArray = [NSMutableArray arrayWithArray:self.dataArray];
    [self.collection reloadData];
}

- (void)insertDataArray:(NSArray *)array
{
    @synchronized (_dataArray) {
        VLSUserProfile *profile = array[0];
        
        [_dataArray enumerateObjectsUsingBlock:^(VLSUserProfile * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([profile.userId isEqualToString:obj.userId]) {
                [_dataArray removeObject:obj];
            }
        }];
        [_dataArray insertObject:profile atIndex:0];

        
        if (_dataArray.count > self.maxAudiences) {
            
            NSIndexSet *Deleset = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(self.maxAudiences, _dataArray.count-self.maxAudiences)];
            [_dataArray removeObjectsAtIndexes:Deleset];
        }
    }
    
    
}

- (void)deleteDataArray:(NSArray *)array
{
    @synchronized (_dataArray) {
        VLSUserProfile *profile = array[0];
        [_dataArray enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(VLSUserProfile * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([profile.userId isEqual:obj.userId]) {
                [_dataArray removeObject:obj];
            }
        }];
    }
}

#pragma delegate  dataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.reloadArray.count <= self.maxAudiences) {
        return self.reloadArray.count;
    }else{
        return self.maxAudiences;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    OnLineNumberCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestCell" forIndexPath:indexPath];
    cell.userInteractionEnabled = YES;
    
    if (indexPath.row == self.maxAudiences - 1) {
        [cell.OnLineIcon setImage:[UIImage imageNamed:@"broadcast-more"]];
        cell.userInteractionEnabled = NO;
        return cell;
    }
    
    VLSUserProfile *model = self.reloadArray[indexPath.row];
    [cell setOnLineModel:model];;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([_delegate respondsToSelector:@selector(audiencesViewSelect:)]) {
        VLSUserProfile *model = self.reloadArray[indexPath.row];
        [_delegate audiencesViewSelect:model];
    }
}

 -(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(28, 28);
}
- (UICollectionView *)collection
{
    if (_collection ==nil) {
        UICollectionViewFlowLayout *Flow = [[UICollectionViewFlowLayout alloc]init];
        
        _collection = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:Flow];
        Flow.itemSize = CGSizeMake(28,28);
        
        _collection.dataSource = self;
        _collection.delegate = self;
        _collection.backgroundColor = [UIColor clearColor];
        _collection.showsHorizontalScrollIndicator = NO;
//        _collection.pagingEnabled = YES;
        _collection.alwaysBounceHorizontal = YES;
        _collection.bounces = YES;
        Flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        Flow.minimumInteritemSpacing = 0;
        Flow.minimumLineSpacing = 5;
//        [self addSubview:self.collection];

        [_collection registerClass:[OnLineNumberCollectionViewCell class] forCellWithReuseIdentifier:@"TestCell"];
 
    }
    return _collection;
}

- (BOOL)checkisFullList
{
    if (self.dataArray.count >= self.maxAudiences) {
        return YES;
    }else{
        return NO;
    }
}

@end
