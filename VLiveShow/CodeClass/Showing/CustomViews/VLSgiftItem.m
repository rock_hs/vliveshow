//
//  VLSgiftItem.m
//  balabala
//
//  Created by SuperGT on 16/5/11.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import "VLSGiftItem.h"
#import "UIView+SDAutoLayout.h"

@interface VLSGiftItem()

@property (nonnull,nonatomic,strong) UIImageView *GiftIconView;
@property (nonnull,nonatomic,strong) UIImageView *SelectImageView;
@property (nonnull,nonatomic,strong) UILabel *OtherLabel;
@property (nonnull,nonatomic,strong) UILabel *jingyan;


@end

@implementation VLSGiftItem

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
//    frame.size.height = 30;
    self = [super initWithFrame:frame];
    if (self) {
        self.GiftIconView = [[UIImageView alloc]initWithFrame:CGRectZero];
        self.SelectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [UIImage imageNamed:@"lan-i_"];
//        self.SelectButton.frame = CGRectZero;
        [self.SelectButton setImage:nil forState:UIControlStateNormal];
        [self.SelectButton setImage:image forState:UIControlStateSelected];
        
        self.OtherLabel = [[UILabel alloc]initWithFrame:CGRectZero];
//        self.OtherLabel.adjustsFontSizeToFitWidth = YES;
        self.OtherLabel.font = [UIFont systemFontOfSize:11];
        self.OtherLabel.textAlignment = NSTextAlignmentCenter;
        //
        self.jingyan = [[UILabel alloc]initWithFrame:CGRectZero];
        self.jingyan.font = [UIFont systemFontOfSize:11];
        self.jingyan.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:self.jingyan];
        [self addSubview:self.OtherLabel];
        [self addSubview:self.GiftIconView];
        [self addSubview:self.SelectButton];
        
        
//        _GiftIconView.sd_layout
//        .centerXEqualToView(self)
//        .topSpaceToView(self,0)
//        .heightIs(33)
//        .widthIs(50);
//        
//        _SelectButton.sd_layout
//        .topEqualToView(_GiftIconView)
//        .rightSpaceToView(self,5)
//        .heightIs(15)
//        .widthIs(15);
//        
//        _OtherLabel.sd_layout
//        .topSpaceToView(_GiftIconView,1)
//        .centerXEqualToView(self)
//        .heightIs(10)
//        .widthRatioToView(self,1);
//        
//        _jingyan.sd_layout
//        .topSpaceToView(_OtherLabel,1)
//        .centerXEqualToView(self)
//        .heightIs(10)
//        .widthRatioToView(self,1);
        
    }
    NSLog(@"%s",__func__);

    return self;
    
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    static int i = 0;
    i = i + 1;
    NSLog(@"%d",i);
    NSLog(@"%s",__func__);
    
    _GiftIconView.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(self,0)
    .heightIs(33)
    .widthIs(50);
    
    _SelectButton.sd_layout
    .topEqualToView(_GiftIconView)
    .rightSpaceToView(self,5)
    .heightIs(15)
    .widthIs(15);
    
    _OtherLabel.sd_layout
    .topSpaceToView(_GiftIconView,1)
    .centerXEqualToView(self)
    .heightIs(10)
    .widthRatioToView(self,1);
    
    _jingyan.sd_layout
    .topSpaceToView(_OtherLabel,1)
    .centerXEqualToView(self)
    .heightIs(10)
    .widthRatioToView(self,1);
    
}

- (void)setgiftItemBlock:(giftItemBlock)giftItem
{
    self.giftItem = giftItem;
}

- (void)setGiftImage:(NSString *)image text:(NSString *)text
{
    [self.GiftIconView setImage:[UIImage imageNamed:image]];
    text = [NSString stringWithFormat:@"%@经验",text];
    self.jingyan.text = text;
    self.OtherLabel.text = @"100💎";
    [self layoutSubviews];
    
}

- (void)setGiftSelect:(BOOL)GiftSelect
{
    _GiftSelect = GiftSelect;
    self.SelectButton.selected = GiftSelect;

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    CGPoint point = [[touches anyObject] locationInView:self];
    NSLog(@"%@",[NSValue valueWithCGPoint:point]);
    if (CGRectContainsPoint(self.bounds, point)) {
        
        self.giftItem(self);
    }
    
}

@end
