//
//  VLSLRTableViewCell.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLRTableViewCell.h"

@implementation VLSLRTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext(); CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor); CGContextFillRect(context, rect); //上分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_FFFFFF).CGColor); CGContextStrokeRect(context, CGRectMake(5, -1, rect.size.width - 10, 1)); //下分割线
    CGContextSetStrokeColorWithColor(context, RGB16(COLOR_LINE_E2E2E2).CGColor); CGContextStrokeRect(context, CGRectMake(5, rect.size.height, rect.size.width - 10, 1));
}

//在该方法中实现自定义cell上相关控件的创建
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setSD_Layout];
    }
    return self;
}

- (void)setSD_Layout{
    
    _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 34, 34)];
    _headImageView.contentMode = UIViewContentModeScaleAspectFill;
    _headImageView.layer.masksToBounds = YES;
    _headImageView.layer.cornerRadius = 17;
    _headImageView.image = ICON_PLACEHOLDER;
    
    _titleLable = [[UILabel alloc] init];
    _titleLable.font = [UIFont systemFontOfSize:14];
    _titleLable.textColor = [UIColor blackColor];
    
    _timeLabel = [[UILabel alloc] init];
    _timeLabel.textColor = [UIColor lightGrayColor];
    _timeLabel.textAlignment = NSTextAlignmentRight;
    _timeLabel.font = [UIFont systemFontOfSize:13];
    
    _contentLabel = [[UILabel alloc] init];
    _contentLabel.font = [UIFont systemFontOfSize:14];
    _contentLabel.numberOfLines = 0;
    _contentLabel.textColor = RGB16(COLOR_FONT_999999);
    
    _contentImageView = [[UIImageView alloc] init];
    _contentImageView.backgroundColor = [UIColor lightGrayColor];
    
    _playImageView = [UIImageView new];
    _playImageView.image = [UIImage imageNamed:@"broadcast-news-play"];
    _playImageView.layer.cornerRadius = 15;
    _playImageView.clipsToBounds = YES;
    _playImageView.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:self.headImageView];
    [self.contentView addSubview:self.contentImageView];
    [self.contentView addSubview:self.titleLable];
    [self.contentView addSubview:self.contentLabel];
    [self.contentImageView addSubview:self.playImageView];
    
    
    _headImageView.sd_layout
    .leftSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,10)
    .heightIs(34)
    .widthIs(34);
    
    _contentImageView.sd_layout
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,15)
    .heightIs(60)
    .widthIs(100);
    
    _titleLable.sd_layout
    .leftSpaceToView(self.headImageView,10)
    .topSpaceToView(self.contentView,18)
    .rightSpaceToView(self.contentImageView,10)
    .heightIs(20);
    
    _timeLabel.sd_layout
    .leftSpaceToView(self.headImageView,10)
    .topSpaceToView(self.contentView,10)
    .widthIs(80)
    .heightIs(20);
    
    _contentLabel.sd_layout
    .leftSpaceToView(self.contentView,15)
    .rightSpaceToView(self.contentImageView,10)
    .topSpaceToView(self.headImageView,5)
    .bottomSpaceToView(self.contentView,5);
    
    _playImageView.sd_layout
    .centerYEqualToView(self.contentImageView)
    .centerXEqualToView(self.contentImageView)
    .heightIs(30)
    .widthIs(30);
    
    [self setupAutoHeightWithBottomViewsArray:@[self.contentImageView,self.contentLabel] bottomMargin:10];
}

#pragma mark - Model赋值实现
-(void)setModel:(VLSLiveRemindModel *)model{
    _model = model;
    //主播头像
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/v1/users/%@/avatars.png",BASE_URL,model.anchorID]] placeholderImage:ICON_PLACEHOLDER];
    self.titleLable.text = model.anchorName;//主播昵称
    self.contentLabel.text = model.content;//内容
    
    //时间
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yy/MM/dd";
    NSString *timeStr = [NSString compareCurrentTime:model.timestamp];
    
    //昵称
    NSMutableAttributedString *nameAttri = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",model.title]];
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@",timeStr]];
    [nameAttri appendAttributedString:string];
    
    // 修改富文本中的不同文字的样式
    [nameAttri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:10] range:NSMakeRange(model.title.length + 2, timeStr.length)];
    // 设置数字为浅灰色
    [nameAttri addAttribute:NSForegroundColorAttributeName value:RGB16(COLOR_FONT_B2B2B2) range:NSMakeRange(model.title.length + 2, timeStr.length)];
    _titleLable.attributedText = nameAttri;
    
    //直播图片
    [_contentImageView sd_setImageWithURL:[NSURL URLWithString:model.imageURL] placeholderImage:nil];
}



/**
 * 计算指定时间与当前的时间差
 * @param compareDate   某一指定时间
 * @return 多少(秒or分or天or月or年)+前 (比如，3天前、10分钟前)
 */
//-(NSString *) compareCurrentTime:(NSDate*) compareDate
////
//{
//    NSTimeInterval  timeInterval = [compareDate timeIntervalSinceNow];
//    timeInterval = -timeInterval;
//    long temp = 0;
//    NSString *result;
//    if (timeInterval < 60) {
//        result = [NSString stringWithFormat:@"刚刚"];
//    }
//    else if((temp = timeInterval/60) <60){
//        result = [NSString stringWithFormat:@"%ld分钟前",temp];
//    }
//    
//    else if((temp = temp/60) <24){
//        result = [NSString stringWithFormat:@"%ld小时前",temp];
//    }
//    
//    else if((temp = temp/24) <30){
//        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//        formatter.dateFormat = @"yy/MM/dd";
//        result = [formatter stringFromDate:_model.timestamp];
//    }
//    
//    else if((temp = temp/30) <12){
//        result = [NSString stringWithFormat:@"%ld个月前",temp];
//    }
//    else{
//        temp = temp/12;
//        result = [NSString stringWithFormat:@"%ld年前",temp];
//    }
//    
//    return  result;
//}


@end
