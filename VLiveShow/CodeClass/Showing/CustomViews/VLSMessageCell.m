//
//  VLSMessageCell.m
//  Demo3_Chat
//
//  Created by SXW on 16/2/24.
//  Copyright © 2016年 SXW. All rights reserved.
//

#import "VLSMessageCell.h"
//#import "VLSMessageManager.h"
#define random  arc4random() % 256 / 255.0

@implementation VLSMessageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    
}

//通过代码的方式创建cell的时候调用该方法
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createContent];
    }
    return self;
}

- (void)createContent{
    
    self.Vie = [[UIView alloc]initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH * 3 / 5, 1)];
    [self addSubview:self.Vie];
    self.CTtextView = [[CTTextDisplayView alloc] initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH * 3 / 5 - 10, 1)];
    self.CTtextView.tag = 100;
    self.CTtextView.delegate = self;
    [self addSubview:self.CTtextView];
/*
     self.levelLabel = [[UILabel alloc]init];
     [self.CTtextView addSubview:self.levelLabel];
     self.levelLabel.sd_layout
     .leftSpaceToView(self.CTtextView,15)
     .topSpaceToView(self.CTtextView,8)
     .heightIs(10)
     .widthIs(10);
     self.levelLabel.textColor = [UIColor whiteColor];
     self.levelLabel.font = [UIFont systemFontOfSize:8];
     self.levelLabel.textAlignment = NSTextAlignmentCenter;
*/
}

- (void)setMsg:(VLSMessageViewModel *)msg{
    _msg = msg;
    [self changeContentWithModel:msg];
}

- (void)changeContentWithModel:(VLSMessageViewModel *)msg{
    
    CTTextStyleModel * styleModel2 = [CTTextStyleModel new];
    styleModel2.font = [UIFont systemFontOfSize:14];
    styleModel2.faceSize = CGSizeMake(26,14);
    styleModel2.tagImgSize = CGSizeMake(2, 2);
    styleModel2.faceOffset = 3.0f;
    styleModel2.lineSpace = 3.0f;
    styleModel2.numberOfLines = -1;
    styleModel2.highlightBackgroundRadius = 10;
    styleModel2.highlightBackgroundAdjustHeight = 2;
    styleModel2.highlightBackgroundOffset = 3;
    styleModel2.autoHeight = NO;
    styleModel2.urlUnderLine = YES;
    styleModel2.emailUnderLine = YES;
    styleModel2.phoneUnderLine = YES;
    styleModel2.emailColor = [UIColor redColor];
    styleModel2.phoneColor = [UIColor greenColor];
    styleModel2.subjectColor = [UIColor blueColor];
    styleModel2.keyColor = _msg.nameColor;
    styleModel2.textColor = [UIColor whiteColor];
/* 等级需要的代码
    if (_msg.userLevel == 0) {
            NSString *s = [message substringWithRange:NSMakeRange(0, 4)];
            if ([s isEqualToString:@"zzzz"]) {
                NSRange range = NSMakeRange(0, 4);
                message =   [message stringByReplacingCharactersInRange:range withString:[NSString stringWithFormat:@""]];
            }
    }else{
        NSString *s = [message substringWithRange:NSMakeRange(0, 4)];
        if ([s isEqualToString:@"zzzz"]) {
            NSRange range = NSMakeRange(0, 4);
            message =   [message stringByReplacingCharactersInRange:range withString:[NSString stringWithFormat:@"[星星] "]];
    }
        self.levelLabel.text = [NSString stringWithFormat:@"%ld",_msg.userLevel];

    }
 */   
/**
     *  根据消息类型做出不同颜色样式
     TYPE_NORMALMESSAGE=10011,//普通消息
     TYPE_JOINOROUT,//进入
     TYPE_SUPERJOIN,//超级管理员
     TYPE_GIFT,//礼物消息
     TYPE_BARRAGE,//弹幕
     TYPE_GREENMESSAGE//绿色消息
     
     */
    switch (msg.types) {
            //普通消息
        case 10011:
            
            styleModel2.textColor = RGB16(COLOR_FONT_FFFFFF);
            
            styleModel2.keyColor = _msg.nameColor;
            
            styleModel2.font = [UIFont systemFontOfSize:14];
            
            self.Vie.backgroundColor = [UIColor colorWithRed:((float)((COLOR_FONT_000000 & 0xFF0000) >> 16))/255.0 green:((float)((COLOR_FONT_000000 & 0xFF00) >> 8))/255.0 blue:((float)(COLOR_FONT_000000 & 0xFF))/255.0 alpha:0.5f];
            
            break;
            //进入消息
        case 10012:
            
            styleModel2.textColor = RGB16(COLOR_FONT_FFFFFF);
            
            styleModel2.keyColor = RGB16(COLOR_FONT_FFFFFF);
            
            styleModel2.font = [UIFont systemFontOfSize:14];
            
            self.CTtextView.isShadow = YES;
            
            self.CTtextView.alhpa = 1;
            
            self.CTtextView.shadowColor = RGB16(COLOR_FONT_000000);
            
            self.Vie.backgroundColor = [UIColor clearColor];
            
            break;
            //绿色消息
        case 10016:
            
//            styleModel2.textColor = RGB16(COLOR_FONT_F9CB35);
            styleModel2.textColor = RGB16(COLOR_BG_FFDA62);
            styleModel2.keyColor = RGB16(COLOR_FONT_F9CB35);
            
            self.CTtextView.isShadow = YES;
            
            self.CTtextView.shadowColor = RGB16(COLOR_FONT_000000);
            
            self.CTtextView.alhpa = 0.5;
            
            self.Vie.backgroundColor = [UIColor clearColor];
            
            break;
            //超级管理员进入房间
        case 10013:
            styleModel2.textColor = [UIColor redColor];
            
            styleModel2.keyColor = [UIColor redColor];
            
            self.Vie.backgroundColor = [UIColor colorWithRed:((float)((COLOR_FONT_FFFFFF & 0xFF0000) >> 16))/255.0 green:((float)((COLOR_FONT_FFFFFF & 0xFF00) >> 8))/255.0 blue:((float)(COLOR_FONT_FFFFFF & 0xFF))/255.0 alpha:0.7f];
            styleModel2.faceSize = CGSizeMake(18,14);
            break;
            //敏感词消息
        case 10018:
            styleModel2.textColor = [UIColor redColor];
            
            styleModel2.keyColor = [UIColor redColor];
            
            styleModel2.font = [UIFont systemFontOfSize:14];
            
            self.Vie.backgroundColor = [UIColor colorWithRed:((float)((COLOR_FONT_FFFFFF & 0xFF0000) >> 16))/255.0 green:((float)((COLOR_FONT_FFFFFF & 0xFF00) >> 8))/255.0 blue:((float)(COLOR_FONT_FFFFFF & 0xFF))/255.0 alpha:0.7f];
            break;
        case 10019:
            styleModel2.textColor = RGB16(COLOR_FONT_FFFFFF);
            
            styleModel2.keyColor = _msg.nameColor;
            
            styleModel2.font = [UIFont systemFontOfSize:14];
            
            self.Vie.backgroundColor = [UIColor colorWithRed:((float)((COLOR_FONT_000000 & 0xFF0000) >> 16))/255.0 green:((float)((COLOR_FONT_000000 & 0xFF00) >> 8))/255.0 blue:((float)(COLOR_FONT_000000 & 0xFF))/255.0 alpha:0.5f];
            break;
            case TYPE_FORBIDSENDMSG:
        {
            styleModel2.textColor = [UIColor whiteColor];
            
            styleModel2.keyColor = [UIColor whiteColor];
            
            self.Vie.backgroundColor = [RGB16(COLOR_FONT_FF1130) colorWithAlphaComponent:.5f];
            styleModel2.faceSize = CGSizeMake(18,14);
        }
            break;
        default:
            
            break;
    }
    self.Vie.userInteractionEnabled = NO;
    self.CTtextView.text = _msg.message;
    self.CTtextView.styleModel = styleModel2;
    CGRect vieFrame = self.Vie.frame;
    vieFrame.size.width = _msg.width + 10;
    vieFrame.size.height = _msg.height - 5;
    self.Vie.frame = vieFrame;
    self.CTtextView.frame = CGRectMake(10, 0, _msg.width, _msg.height - 5);
    self.CTtextView.clipsToBounds = YES;
    self.CTtextView.backgroundColor = [UIColor clearColor];
    self.Vie.layer.cornerRadius = 8;
    
}

@end
