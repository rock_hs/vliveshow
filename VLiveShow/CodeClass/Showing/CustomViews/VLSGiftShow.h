//
//  VLSGiftShow.h
//  balabala
//
//  Created by SuperGT on 16/5/11.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^GiftShow)(NSMutableDictionary *_Nullable dic);

@interface VLSGiftShow : UIView



- (void)setGiftShowBlock:(GiftShow _Nonnull) block;

- (void)setsetItemImages:(NSArray * _Nonnull)images;

@end
