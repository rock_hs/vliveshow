//
//  UserCollectionViewCell.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "UserCollectionViewCell.h"
#import <SDAutoLayout/UIView+SDAutoLayout.h>


@interface UserCollectionViewCell ()

@property (nonatomic, strong) UIImageView *GT_Icon;


@end

@implementation UserCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.GT_Icon = [[UIImageView alloc]initWithFrame:CGRectZero];
        [self addSubview:self.GT_Icon];
        self.GT_status = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.GT_status setImage:nil forState:UIControlStateNormal];
        [self.GT_status setImage:[UIImage imageNamed:@"right02"] forState:UIControlStateSelected];
        [self addSubview:self.GT_status];
        self.GT_status.userInteractionEnabled = NO;
        
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.GT_Icon.sd_layout
    .topSpaceToView(self,10)
    .leftSpaceToView(self,10)
    .rightSpaceToView(self,10)
    .heightEqualToWidth();
    
    self.GT_status.sd_layout
    .rightSpaceToView(self,10)
    .bottomSpaceToView(self,10)
    .heightIs(21)
    .widthIs(21);
    
    [self updateLayout];
    
    
    
    CGFloat corner = self.GT_Icon.frame.size.width;
    self.GT_Icon.layer.cornerRadius = corner/2;
    self.GT_Icon.layer.masksToBounds = YES;
}

- (void)setOnLineModel:(VLSUserProfile *)onLineModel{
    //要替换掉
    [self.GT_Icon sd_setImageWithURL:[NSURL URLWithString:onLineModel.userFaceURL] placeholderImage:ICON_PLACEHOLDER];

    [self layoutSubviews];
}

@end
