//
//  UserView.h
//  animationTest
//
//  Created by sp on 16/5/18.
//  Copyright © 2016年 sp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserView : UIView
@property (nonatomic,strong)UIImageView *titleImageView;
@property (nonatomic,strong)UIImageView *flowerImageView;
@property (nonatomic,strong)UILabel *giftNameLabel;
@property (nonatomic,strong)UILabel * countLabel;
@end
