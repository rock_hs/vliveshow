//
//  closeRoomActionView.m
//  VLiveShow
//
//  Created by SuperGT on 16/7/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "closeRoomActionView.h"

#define cornerradius @5

@interface closeRoomActionView ()


@end

@implementation closeRoomActionView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
//        self.backgroundColor = [UIColor colorWithRed:70.f/255.f green:70.f/255.f blue:70.f/255 alpha:.2f];
        self.backgroundColor = [UIColor clearColor];
        
        /**
         *  ios8自带毛玻璃效果
         */
//        UIBlurEffect *beffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
//        
//        UIVisualEffectView *view = [[UIVisualEffectView alloc]initWithEffect:beffect];
//        
//        view.frame = self.bounds;
//        
//        [self addSubview:view];
        
        [self configer];
    }
    
    return self;
}
- (void)configer
{
    self.cancleBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.cancleBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.cancleBtn setTitle:LocalizedString(@"INVITE_CANCEL") forState:UIControlStateNormal];
    self.cancleBtn.backgroundColor = [UIColor whiteColor];
    self.cancleBtn.sd_cornerRadius = cornerradius;
    self.cancleBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.cancleBtn];
    
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    view.sd_cornerRadius = cornerradius;
    [self addSubview:view];
    
    
    self.cancleGuestBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.cancleGuestBtn setTitleColor:[UIColor colorWithRed:74.f/255.f green:74.f/255 blue:74.f/255.f alpha:1] forState:UIControlStateNormal];
    [self.cancleGuestBtn setTitle:LocalizedString(@"INVITE_END_GUEST_LINK") forState:UIControlStateNormal];
    self.cancleGuestBtn.backgroundColor = [UIColor whiteColor];
    self.cancleGuestBtn.titleLabel.font = [UIFont systemFontOfSize:14];
//    self.cancleGuestBtn.sd_cornerRadius = cornerradius;
    
    [view addSubview:self.cancleGuestBtn];
    
    self.closeRoom = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.closeRoom setTitleColor:[UIColor colorWithRed:74.f/255.f green:74.f/255 blue:74.f/255.f alpha:1] forState:UIControlStateNormal];
    [self.closeRoom setTitle:LocalizedString(@"QUIT_LIVE") forState:UIControlStateNormal];
    self.closeRoom.backgroundColor = [UIColor whiteColor];
    self.closeRoom.titleLabel.font = [UIFont systemFontOfSize:14];
//    self.closeRoom.sd_cornerRadius = cornerradius;
    [view addSubview:self.closeRoom];
    
    self.cancleBtn.sd_layout
    .widthRatioToView(self,260.f/320.f)
    .heightIs(47)
    .centerXEqualToView(self)
    .centerYIs(SCREEN_HEIGHT/2+50);
    
    view.sd_layout
    .centerXEqualToView(self)
    .bottomSpaceToView(self.cancleBtn,10)
    .heightIs(94)
    .widthRatioToView(self.cancleBtn,1);
    
    self.closeRoom.sd_layout
    .heightIs(47)
    .leftSpaceToView(view,0)
    .rightSpaceToView(view,0)
    .bottomSpaceToView(view,0);
    
    self.cancleGuestBtn.sd_layout
    .topSpaceToView(view,0)
    .leftSpaceToView(view,0)
    .rightSpaceToView(view,0)
    .heightIs(47);

    UIView *line = [[UIView alloc]initWithFrame:CGRectZero];
    line.backgroundColor = [UIColor colorWithRed:210/255.f green:210/255.f blue:210/255.f alpha:1];
    [view addSubview:line];
    line.sd_layout
    .widthRatioToView(view,1)
    .centerXEqualToView(view)
    .centerYEqualToView(view)
    .heightIs(.5f);
}



@end
