//
//  VLSHotCell.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VLSLiveListModel;

@interface VLSHotCell : UITableViewCell

/// 直播列表数据模型
@property (nonatomic, strong)VLSLiveListModel *liveListModel;

@end
