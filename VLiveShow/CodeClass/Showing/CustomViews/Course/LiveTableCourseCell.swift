//
//  LiveTableCourseCell.swift
//  VLiveShow
//
//  Created by VincentX on 09/11/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit
import FDStackView
import SDWebImage

enum LiveTableCourseModel
{
    case Course(VLSCourseModel)
    case Playback(VLSPlaybackModel)
}

class LiveTableCourseCell: UITableViewCell {
    
    @IBOutlet var avatarButton: UIButton!
    @IBOutlet var tagStackView: FDStackView!
    @IBOutlet var infoStackView: FDStackView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var coverImageView: UIImageView!
    @IBOutlet var anchorNameLabel: UILabel!
    @IBOutlet var livingLabel: UILabel!
    @IBOutlet var tagAndTitleSpacingConstraint: NSLayoutConstraint!
    @IBOutlet var tagStackViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var durationLabel: UILabel!
    
    var model: LiveTableCourseModel?
    {
        didSet
        {
            self.refreshUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        infoStackView.axis = .horizontal
        infoStackView.alignment = .center
        infoStackView.spacing = 4
        infoStackView.distribution = .equalCentering
        
        tagStackView.axis = .horizontal
        tagStackView.alignment = .center
        tagStackView.spacing = 2
        tagStackView.distribution = .fillProportionally

        resetLayout()
        refreshUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func resetLayout()
    {
        infoStackView.arrangedSubviews.forEach
        {
            infoStackView.removeArrangedSubview($0)
        }
        tagStackView.arrangedSubviews.forEach
        {
            tagStackView.removeArrangedSubview($0)
        }
    }
    
    func refreshUI()
    {
        resetLayout()
        guard let vm = self.model else
        {
            return
        }
        if case .Course(let courseModel) = vm
        {
            self.titleLabel.text = courseModel.courseName
            self.livingLabel.text = LocalizedString("LIVE_LIST_CELL_TYPE_BOOKING");
            self.livingLabel.backgroundColor = UIColor.tagYellow()
            self.anchorNameLabel.text = courseModel.teacherName
            
            let clockView =  LiveInfoAccessView.create()
            clockView.imageView.image = #imageLiteral(resourceName: "clock_gray")
            clockView.imageView.highlightedImage = #imageLiteral(resourceName: "clock_red")
            clockView.textLabel.text = ""
            let startDate = courseModel.startTime.doubleValue
            if startDate  > 0
            {
                clockView.textLabel.text = Date.dateStr(startDate, formatter: "YYYY年M月d日 HH:mm")
            }
            infoStackView.addArrangedSubview(clockView)
            if let coverImageURL = courseModel.picCover
            {
                self.coverImageView.sd_setImage(with: URL(string: coverImageURL))
            }
            if let picTeacher = courseModel.picTeacher
            {
                self.avatarButton.sd_setImage(with: URL(string: picTeacher), for: .normal, placeholderImage: #imageLiteral(resourceName: "headshot.png"))
            }
            var hasTags = false
            if let courseTypeName = courseModel.courseTypeName,
                !courseTypeName.isEmpty
            {
                hasTags = true
                self.labelFor(text: courseTypeName)
            }
            self.tagAndTitleSpacingConstraint.constant = hasTags ? 11 : 0
            self.tagStackViewWidthConstraint.isActive = !hasTags
            self.durationLabel.isHidden = true
        }
        else if case .Playback(let playback) = vm
        {
            self.titleLabel.text = playback.title
            self.livingLabel.text = _LS("LIVE_LIST_CELL_TYPE_PLAYBACK")
            self.livingLabel.backgroundColor = UIColor.tagPurple()
            self.anchorNameLabel.text = playback.detailTitle
            var hasTags = false
            if let tag = playback.custom
            {
                hasTags = true
                self.labelFor(text: tag)
            }
            self.tagAndTitleSpacingConstraint.constant = hasTags ? 11 : 0
            self.tagStackViewWidthConstraint.isActive = !hasTags
            if let coverImageURL = playback.coverUrl
            {
                self.coverImageView.sd_setImage(with: URL(string: coverImageURL))
            }
            if let avatarString = playback.avatarUrl,
                let avatarURL = URL(string: avatarString)
            {
                self.avatarButton.sd_setImage(with: avatarURL, for: .normal, placeholderImage: #imageLiteral(resourceName: "headshot.png"))
            }
            else
            {
                self.avatarButton.setImage(#imageLiteral(resourceName: "headshot.png"), for: .normal)
            }
            if let duration = playback.duration,
                !duration.isEmpty
            {
                self.durationLabel.isHidden = false
                self.durationLabel.text = duration
            }
            else
            {
                self.durationLabel.isHidden = true
            }
            
        }
    }
    
    func labelFor(text: String)
    {
        let tagLabel = UIExtLabel(frame: CGRect(x: 0, y: 0, width: 0, height: 20))
        tagLabel.font = UIFont.systemFont(ofSize: 10)
        tagLabel.textColor = UIColor.tagYellow()
        tagLabel.text = text
        tagLabel.textAlignment = .center
        tagLabel.layerMaskToBounds = true
        tagLabel.layerBorderColor = UIColor.tagYellow()
        tagLabel.layerBorderWidth = 1.0
        tagLabel.layerCornerRadius = tagLabel.frame.height / 2
        tagLabel.textLeftInsect = tagLabel.layerCornerRadius
        tagLabel.textRightInsect = tagLabel.layerCornerRadius
        tagStackView.addArrangedSubview(tagLabel)
    }
}

// For objective c
extension LiveTableCourseCell
{
    @objc var objcDataModel: AnyObject?
    {
        set
        {
            if let course = newValue as? VLSCourseModel
            {
                self.model = .Course(course)
            }
            else if let playback = newValue as? VLSPlaybackModel
            {
                self.model = .Playback(playback)
            }
        }
        
        get
        {
            guard  let m = self.model else
            {
                return nil
            }
            if case .Course(let course) = m
            {
                return course
            }
            else if case .Playback(let playback) = m
            {
                return playback
            }
            return nil
        }
    }
}
