//
//  LiveInfoAccessView.swift
//  VLiveShow
//
//  Created by VincentX on 09/11/2016.
//  Copyright © 2016 vliveshow. All rights reserved.
//

import UIKit
import SnapKit

class LiveInfoAccessView: UIView {
    
    lazy var imageView: UIImageView = UIImageView()
    lazy var textLabel: UILabel = UILabel()
    
    override var intrinsicContentSize: CGSize
    {
        let size = CGSize(width: imageView.intrinsicContentSize.width + textLabel.intrinsicContentSize.width + 12,
                          height: imageView.intrinsicContentSize.height)
        return size
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.imageView)
        self.addSubview(self.textLabel)
        self.configureUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureUI()
    }
    
    func configureUI()
    {
        self.textLabel.font = UIFont.systemFont(ofSize: 12)
        self.textLabel.textColor = UIColor.textGray()
        self.addSubview(self.imageView)
        self.addSubview(self.textLabel)
        configureLayout()
    }
    
    func configureLayout()
    {
        self.imageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(14)
            make.centerY.equalToSuperview()
        }
        self.textLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview()
            make.left.equalTo(self.imageView.snp.right).offset(7)
            make.top.bottom.equalToSuperview()
        }
    }
    
    static func create() -> LiveInfoAccessView
    {
        return LiveInfoAccessView(frame: CGRect.zero)
    }
}
