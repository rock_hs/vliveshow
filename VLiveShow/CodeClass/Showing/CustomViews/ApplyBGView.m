//
//  ApplyBGView.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "ApplyBGView.h"
#import "VLSMessageManager.h"
#import "VLSAVMultiManager.h"
#import "TelePhoneGuestView.h"
#import "VLSShowViewController.h"
#import "AccountManager.h"
#import "UIImage+colorImage.h"
#import "ManagerEvent.h"
#import "VLSAgoraVideoService.h"
#import "UIView+CurretVC.h"
#import "VLSUserTrackingManager.h"

#define Kwidth 50
#define imageMargin 80
#define rightHeight 40/250.f*(self.bounds.size.height)

@interface ApplyBGView ()

//@property (nonnull,nonatomic,copy) NSString *selfurl;
//@property (nonnull,nonatomic,copy) NSString *hosturl;



@property (nonnull,nonatomic,strong) UIImageView *selfIcon;

@property (nonnull,nonatomic,strong) UIImageView *hostIcon;

@property (nonnull,nonatomic,strong) CAShapeLayer *line;

@property (nonnull,nonatomic,strong) UIImageView *rightView;

@property (nonatomic, strong) UIButton *canclebtn;

@property (nonatomic, strong) UIButton *goonbtn;

@property (nonatomic, strong) VLSUserProfile *host;

@property (nonatomic, strong) TelePhoneGuestView *teleview;

@end

@implementation ApplyBGView

- (instancetype)initWithFrame:(CGRect)frame selfurl:(VLSUserProfile *)selfurl hosturl:(VLSUserProfile *)hosturl
{
    
    if(self = [self initWithFrame:frame]){
        self.host = hosturl;
        
        // 画虚线
        [self applyToAnchor];
        
        self.selfIcon = [[UIImageView alloc]initWithFrame:CGRectZero];
        self.selfIcon.contentMode = UIViewContentModeScaleAspectFill;
        self.selfIcon.layer.cornerRadius = Kwidth/2;
        self.selfIcon.clipsToBounds =  YES;
        [self addSubview:self.selfIcon];
        
        self.selfIcon.sd_layout
        .centerYEqualToView(self)
        .centerXIs(self.center.x - 88)
        .heightIs(Kwidth)
        .widthIs(Kwidth);
        
        [self.selfIcon sd_setImageWithURL:[NSURL URLWithString:selfurl.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
        
        
        self.hostIcon = [[UIImageView alloc]initWithFrame:CGRectZero];
        self.hostIcon.contentMode = UIViewContentModeScaleAspectFill;
        self.hostIcon.layer.cornerRadius = Kwidth/2;
        self.hostIcon.clipsToBounds = YES;
        self.hostIcon.layer.borderColor = [UIColor redColor].CGColor;
        self.hostIcon.layer.borderWidth = 1.0f;
        [self addSubview:self.hostIcon];
        
        self.hostIcon.sd_layout
        .centerYEqualToView(self)
        .centerXIs(self.center.x + 88)
        .heightIs(Kwidth)
        .widthIs(Kwidth);
        
        [self.hostIcon sd_setImageWithURL:[NSURL URLWithString:hosturl.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
        
        [self.closeButton addTarget:self action:@selector(hideSelf) forControlEvents:UIControlEventTouchUpInside];
        
        self.applyStatus = nonApply;
    };
    return self;
}


- (void)applyToAnchor
{
    self.label.text = LocalizedString(@"INVITE_INTERACTION");
    [self.onLineButton setTitle:LocalizedString(@"INVITE_GOTOCONNECT") forState:UIControlStateNormal];
    
    [self.onLineButton addTarget:self action:@selector(startApply) forControlEvents:UIControlEventTouchUpInside];
    

    if (self.line) {
        [self.line removeFromSuperlayer];
    }
    // 虚线
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(self.center.x - 88, self.bounds.size.height/2)];
    [path addQuadCurveToPoint:CGPointMake(self.center.x + 88, self.bounds.size.height/2) controlPoint:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2 - 80)];
    
    self.line = [CAShapeLayer layer];
    [self.line setFrame:self.bounds];
    [self.line setPath:path.CGPath];
    [self.line setStrokeColor:[UIColor colorWithRed:224.0f/255.0f green:30.0f/255.0f blue:22.0f/255.0f alpha:1.f].CGColor];
    [self.line setFillColor:[UIColor clearColor].CGColor];
    [self.line setLineWidth:1.0f];
    [self.line setLineDashPattern:
     [NSArray arrayWithObjects:[NSNumber numberWithInt:2],
      [NSNumber numberWithInt:2],nil]];
    [self.line setLineJoin:kCALineJoinRound];
    [self.layer addSublayer:self.line];
    
}

#pragma mark 点击开始请求
- (void)startApply
{
    if ([AccountManager sharedAccountManager].account.mobilePhone) {
        self.onLineButton.userInteractionEnabled = NO;
        
        // 开始动画
        [self startAnimation];

        ManagerCallBack *callback = [[ManagerCallBack alloc] init];
        callback.updateBlock = ^(id result){
            [self waitForconfirm];
            self.applyStatus = waitToConfirm;
        };
        callback.errorBlock = ^(id result){
            // 结束动画
            [self endAnimation];
            self.onLineButton.userInteractionEnabled = YES;
            /********************************申请记录已满**********************************/
            
            ManagerEvent *event = (ManagerEvent *)result;
            
            if (event.code == 20008) {
                [self guestEnough];
            }
            
        };
        if(self.isAcceptInvite){
            
            NSString *roomid = nil;
            if ([[VLSMessageManager sharedManager] getRoomId]) {
              
                roomid = [[VLSMessageManager sharedManager] getRoomId];
            }else{
            
                roomid = @"nil";
            }
            [[VLSUserTrackingManager shareManager] trackingAgreeGotoRoom:roomid anchorid:self.host.userId fromWhere:@"roomInvite_on"];
            
            [[VLSAVMultiManager sharedManager] guestAcceptInvite:self.host.userId callback:callback];
        }else{
            [[VLSAVMultiManager sharedManager] linkRequest:self.host.userId callback:callback];
        }
    }else{
        if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeRight || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft) {
            if ([self.delegate respondsToSelector:@selector(applyBGViewBindPhone)]) {
                [self.delegate applyBGViewBindPhone];
            }
        }else {
            self.teleview = [[TelePhoneGuestView alloc]initTelePhoneVerifyWithFrame:CGRectMake(0, SCREEN_HEIGHT,self.frame.size.width, self.frame.size.height)];
            [self addSubview:self.teleview];
            self.teleview.sd_layout
            .spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
            // 弹出动画
            [UIView animateWithDuration:.5f animations:^{
                self.teleview.frame = self.bounds;
            } completion:nil];
        }
    }
}

- (void)endSucc
{
    self.label.text = LocalizedString(@"INVITE_CONFIRMATION");
        
        // 添加成功动画
    self.rightView = [[UIImageView alloc]initWithFrame:CGRectZero];
    [self.rightView setImage:[UIImage imageNamed:@"host-choice"]];
    [self addSubview:self.rightView];
    self.rightView.sd_layout
    .centerXEqualToView(self)
    .centerYIs(self.bounds.size.height/2 - 40)
    .heightIs(31)
    .widthIs(31);
    CABasicAnimation *rightAn = [CABasicAnimation animationWithKeyPath:@"scale"];
    rightAn.duration = 1;
    rightAn.fromValue = @0.0f;
    rightAn.toValue = @1.0f;
    rightAn.delegate = self;
    rightAn.removedOnCompletion = NO;
    rightAn.fillMode = kCAFillModeForwards;
    [rightAn setValue:@"right" forKey:@"animationType"];
        
    [self.rightView.layer addAnimation:rightAn forKey:@"animationRight"];
    self.label.text = LocalizedString(@"INVITE_CONFIRMATION");
    [self guestYetChange];
}

- (void)endFail
{
    // 添加失败动画
    self.rightView = [[UIImageView alloc]initWithFrame:CGRectZero];
    [self.rightView setImage:[UIImage imageNamed:@"close"]];
    [self addSubview:self.rightView];
    self.rightView.sd_layout
    .centerXEqualToView(self)
    .centerYIs(self.bounds.size.height/2 - 40)
    .heightIs(21)
    .widthIs(21);
    CABasicAnimation *rightAn = [CABasicAnimation animationWithKeyPath:@"scale"];
    rightAn.duration = 1;
    rightAn.fromValue = @0.0f;
    rightAn.toValue = @1.0f;
    rightAn.delegate = self;
    rightAn.removedOnCompletion = NO;
    rightAn.fillMode = kCAFillModeForwards;
    [rightAn setValue:@"wrong" forKey:@"animationType"];
        
    [self.rightView.layer addAnimation:rightAn forKey:@"animationWrong"];
    

}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if ([[anim valueForKey:@"animationType"] isEqualToString:@"right"]){

//        [self hideSelf];
        self.label.text = LocalizedString(@"INVITE_CONFIRMATION");
        [self guestYetChange];
    }else{

        [self removeFromSuperview];
    }
    
}




/************************************请求已发送等待主播确认*******************************/

- (void)waitForconfirm
{
    // 结束动画
    [self endAnimation];
    
    self.label.text = LocalizedString(@"INVITE_WAIT_FOR_CONFIRMATION");
    // 取消button
    self.canclebtn = [UIButton buttonWithType:UIButtonTypeSystem];
    self.canclebtn.backgroundColor = [UIColor whiteColor];
    [self.canclebtn setBackgroundImage:[UIImage ImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
    [self.canclebtn setTitle:LocalizedString(@"INVITE_CANCLE_CONNECTION") forState:UIControlStateNormal];
    [self.canclebtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    self.canclebtn.titleLabel.font = [UIFont systemFontOfSize:14];
    self.canclebtn.layer.borderWidth = 0.5f;
    self.canclebtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.canclebtn addTarget:self action:@selector(cancleOnLine) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.canclebtn];
    self.canclebtn.sd_layout
    .leftEqualToView(self.onLineButton)
    .bottomSpaceToView(self,0)
    .heightRatioToView(self.onLineButton,1)
    .widthRatioToView(self.onLineButton,0.5);
    
    
    // 继续等待button
    self.goonbtn = [UIButton buttonWithType:UIButtonTypeSystem];
    self.goonbtn.backgroundColor = [UIColor whiteColor];
//    self.goonbtn.showsTouchWhenHighlighted = YES;
    [self.goonbtn setBackgroundImage:[UIImage ImageFromColor:[UIColor darkGrayColor]] forState:UIControlStateHighlighted];
    [self.goonbtn setTitle:LocalizedString(@"INVITE_CONTINUE_TO_WAIT") forState:UIControlStateNormal];
    [self.goonbtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    self.goonbtn.titleLabel.font = [UIFont systemFontOfSize:14];
    self.goonbtn.layer.borderWidth = 0.5f;
    self.goonbtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.goonbtn addTarget:self action:@selector(hideSelf) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self addSubview:self.goonbtn];
    self.goonbtn.sd_layout
    .leftSpaceToView(self.canclebtn,0)
    .bottomSpaceToView(self,0)
    .widthRatioToView(self.onLineButton,0.5)
    .heightRatioToView(self.onLineButton,1);
    
}



/************************************请求正在发送**********************************/
#pragma mark 开始虚线动画

- (void)startAnimation
{
    CABasicAnimation *basic = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    basic.fromValue = @0.0f;
    basic.toValue = @1.0f;
    basic.duration = 2.0f;
    basic.repeatCount = MAXFLOAT;
    basic.removedOnCompletion = NO;
    basic.fillMode = kCAFillModeForwards;
    [basic setValue:@"line" forKey:@"animationType"];
    [self.line addAnimation:basic forKey:@"animationLine"];
    
    self.onLineButton.alpha = 0.5;
}

#pragma mark 结束虚线动画,请求已发送

- (void)endAnimation
{
    [self.line removeAllAnimations];
    self.onLineButton.alpha = 1;
}

/**************************************申请记录已满***********************************************/

- (void)guestEnough
{
    // 嘉宾位已满
    // 创建错误提示图片并改变label
    //删除close以前绑定的事件，添加删除view事件
    [self.closeButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    [self.closeButton addTarget:self action:@selector(removewholeview) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.line removeFromSuperlayer];
    [self.hostIcon removeFromSuperview];
    [self.selfIcon removeFromSuperview];
    [self.onLineButton addTarget:self action:@selector(removewholeview) forControlEvents:UIControlEventTouchUpInside];
    [self.onLineButton setTitle:LocalizedString(@"INVITE_WAIT_FOR_TRY_AGAIN") forState:UIControlStateNormal];
    UILabel *oopslabel = [[UILabel alloc]initWithFrame:CGRectZero];
    oopslabel.font = [UIFont systemFontOfSize:14];
    oopslabel.textColor = [UIColor blackColor];
    oopslabel.textAlignment = NSTextAlignmentCenter;
    oopslabel.text = LocalizedString(@"INVITE_GUEST_FULL");
    oopslabel.lineBreakMode = NSLineBreakByWordWrapping;
    oopslabel.numberOfLines = 0;
    [self addSubview:oopslabel];
    oopslabel.sd_layout
    .centerXEqualToView(self)
    .bottomSpaceToView(self.onLineButton,10)
    .widthIs(200)
    .heightIs(46);
    
//    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectZero];
//    imageview.contentMode = UIViewContentModeScaleAspectFill;
//    [imageview setImage:[UIImage imageNamed:@"broadcast-audience view -line up"]];
//    [self addSubview:imageview];
//    imageview.sd_layout
//    .centerXEqualToView(self)
//    .bottomSpaceToView(oopslabel,20)
//    .widthIs(138)
//    .heightIs(71);
    
//    [oopslabel setSingleLineAutoResizeWithMaxWidth:200];

    
}



/****************************************主播已经成为嘉宾******************************************/

- (void)guestYetChange
{
    [self.canclebtn setTitle:LocalizedString(@"INVITE_CANCEL") forState:UIControlStateNormal];
    [self.goonbtn setTitle:LocalizedString(@"INVITE_IMMEDIATELY") forState:UIControlStateNormal];
    [self.canclebtn removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    [self.goonbtn removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    [self.canclebtn addTarget:self action:@selector(cancleguest) forControlEvents:UIControlEventTouchUpInside];
    [self.goonbtn addTarget:self action:@selector(uponImmediately) forControlEvents:UIControlEventTouchUpInside];
    [self performSelector:@selector(uponImmediately)];
}
#pragma mark 取消已经成为嘉宾的点击事件
- (void)cancleguest
{
    [UIAlertController showAlertInViewController:[self getCurrentVC] withTitle:LocalizedString(@"INVITE_END_GUEST_LINK") message:LocalizedString(@"INVITE_END_REQUEST_LINK") cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"INVITE_CANCEL"),LocalizedString(@"INVITE_SURE")]  tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if (buttonIndex - 2 == 1) {
            [self hideSelf];
            if ([_delegate respondsToSelector:@selector(fromApplyViewGuestCancelLink)]) {
                [_delegate fromApplyViewGuestCancelLink];
            }
        }
    }];
}
#pragma mark 取消连线事件

- (void)cancleOnLine
{
//    [UIAlertController showAlertInViewController:[self getCurrentVC] withTitle:LocalizedString(@"INVITE_END_GUEST_LINK") message:LocalizedString(@"INVITE_END_REQUEST_LINK") cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"INVITE_CANCEL"),LocalizedString(@"INVITE_SURE")]  tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
//        if (buttonIndex - 2 == 1) {
//    [self hideSelf];
//    if ([_delegate respondsToSelector:@selector(fromApplyViewGuestCancelLinkRequest)]) {
//        [_delegate fromApplyViewGuestCancelLinkRequest];
//    }
//        }
//    }];
    [UIAlertController showAlertInViewController:[self getCurrentVC] withTitle:nil message:LocalizedString(@"INVITE_CANCLE_SURE_TELEPHONE") cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"INVITE_CANCLE_APPLLY"),LocalizedString(@"INVITE_GOON_APPLY")]  tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if (buttonIndex - 2 == 0) {
            [self hideSelf];
            if ([_delegate respondsToSelector:@selector(fromApplyViewGuestCancelLinkRequest)]) {
                [_delegate fromApplyViewGuestCancelLinkRequest];
            }
        }
    }];
    
}

#pragma mark 立即上镜
- (void)uponImmediately
{
    
//    [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:NO];
//    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:NO];
    
    [self.goonbtn removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
//        [self waitForconfirm];
    };
    callback.errorBlock = ^(id result){
        
    };
    [[VLSAVMultiManager sharedManager] geneusOnCamera:self.host.userId callback:callback];

    self.hidden = YES;
    
    
    [self.goonbtn setTitle:LocalizedString(@"INVITE_DID_UP_GUEST") forState:UIControlStateNormal];
    [self.goonbtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.goonbtn addTarget:self action:@selector(didUponImmediately) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didUponImmediately
{
    [self hideSelf];
}


- (void)comeBackGuest
{
    
    [self waitForconfirm];
    
    self.label.text = LocalizedString(@"INVITE_CONFIRMATION");
    
    // 添加成功动画
    self.rightView = [[UIImageView alloc]initWithFrame:CGRectZero];
    [self.rightView setImage:[UIImage imageNamed:@"host-choice"]];
    [self addSubview:self.rightView];
    self.rightView.sd_layout
    .centerXEqualToView(self)
    .centerYIs(self.bounds.size.height/2 - 40)
    .heightIs(31)
    .widthIs(31);
    CABasicAnimation *rightAn = [CABasicAnimation animationWithKeyPath:@"scale"];
    rightAn.duration = 1;
    rightAn.fromValue = @0.0f;
    rightAn.toValue = @1.0f;
    rightAn.delegate = self;
    rightAn.removedOnCompletion = NO;
    rightAn.fillMode = kCAFillModeForwards;
    [rightAn setValue:@"right" forKey:@"animationType"];
    
    [self.rightView.layer addAnimation:rightAn forKey:@"animationRight"];
    self.label.text = LocalizedString(@"INVITE_CONFIRMATION");
    
    [self.canclebtn setTitle:LocalizedString(@"INVITE_CANCEL") forState:UIControlStateNormal];
    [self.canclebtn removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    [self.canclebtn addTarget:self action:@selector(cancleguest) forControlEvents:UIControlEventTouchUpInside];
    [self.goonbtn setTitle:LocalizedString(@"INVITE_DID_UP_GUEST") forState:UIControlStateNormal];
    [self.goonbtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.goonbtn addTarget:self action:@selector(didUponImmediately) forControlEvents:UIControlEventTouchUpInside];
}


//#pragma mark 调用remove代理
//
- (void)removewholeview
{
    [self removeFromSuperview];
//        if ([_delegate respondsToSelector:@selector(removeSelf)]) {
//        [_delegate removeSelf];
//    }
}

@end
