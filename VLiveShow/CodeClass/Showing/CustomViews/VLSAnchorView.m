//
//  AnchorView.m
//  balabala
//
//  Created by SuperGT on 16/5/12.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import "VLSAnchorView.h"
#import "VLSLiveHttpManager.h"
#import "ManagerCallBack.h"
#import "VLSMessageManager.h"
#import "UIImage+colorImage.h"
#import "NSString+UserFile.h"
#import "VLSPaoMaAnimation.h"
#import "VLiveShow-Swift.h"

#define imageH (self.frame.size.height - 1)
//#define BGColor [UIColor colorWithRed:255/255.f green:17/255.f blue:48/255.f alpha:1]

@interface VLSAnchorView ()

@property (nullable,nonatomic,strong) UIImageView *AnchorIcon;

@property (nonatomic, strong) RollLabel* labelscorll;

// 关注button
@property (nonatomic, strong) UIButton *button;

// 主播视图下的红色背景
@property (nonatomic, strong) UIView *anchorBGview;

@property (nonatomic, strong) UIImageView *hightimage;

@property (nonatomic, strong) UIImageView *lowimage;

//@property (nonatomic, strong) NSTimer *GTtimer;

@property (nonatomic, strong) dispatch_source_t timer;

@end

@implementation VLSAnchorView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        
        [self setupAnchor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(anchorTap)];
        [self addGestureRecognizer:tap];
    }
    return self;
}


- (void)setupAnchor
{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    
    self.AnchorIcon = [[UIImageView alloc]initWithFrame:CGRectMake(2, 2, imageH, imageH)];
    self.AnchorIcon.contentMode = UIViewContentModeScaleAspectFill;
    self.AnchorIcon.layer.masksToBounds = YES;
    self.AnchorIcon.layer.cornerRadius = imageH / 2;
    self.AnchorIcon.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.AnchorIcon.layer.borderColor = [UIColor colorFromHexRGB:@"FFFFFF"].CGColor;
    self.AnchorIcon.layer.borderWidth = 2;
    [self addSubview:self.AnchorIcon];
    self.AnchorIcon.sd_layout
    .leftSpaceToView(self,1)
    .centerYEqualToView(self)
    .widthIs(imageH)
    .heightIs(imageH);
    
    self.anchorBGview = [[UIView alloc] init];
    self.anchorBGview.backgroundColor = [UIColor colorFromHexRGB:@"FF1130"];
    [self addSubview:self.anchorBGview];
    self.anchorBGview.sd_cornerRadiusFromHeightRatio = @0.5;
    self.anchorBGview.layer.masksToBounds = YES;
    self.anchorBGview.sd_layout
    .leftSpaceToView(self.AnchorIcon,2)
    .rightSpaceToView(self,0)
    .centerYEqualToView(self.AnchorIcon)
    .heightIs(32);
    
//    self.labelscorll = [[LabelScorllView alloc] initWithFrame:CGRectZero font:SIZE_FONT_12];
    if (self.labelscorll == nil) {
        self.labelscorll = [[RollLabel alloc] initWithFrame: CGRectZero];
        [self setRoomName:_roomName];
    }
//    self.labelscorll.backgroundColor = [UIColor clearColor];
    [self addSubview:self.labelscorll];
    self.labelscorll.sd_layout
    .leftSpaceToView(self.AnchorIcon,15)
    .topSpaceToView(self,2)
    .heightIs(17);
    
    
    UIImageView *imagev = [[UIImageView alloc]initWithFrame:CGRectZero];
    imagev.contentMode = UIViewContentModeScaleAspectFill;
    [imagev setImage:[UIImage imageNamed:@"home_people"]];
    [self addSubview:imagev];
    imagev.sd_layout
    .leftEqualToView(self.labelscorll)
    .topSpaceToView(self.labelscorll,2)
    .heightIs(8)
    .widthIs(14);
    
    self.OnLineNumber = [[UILabel alloc]initWithFrame:CGRectZero];
    self.OnLineNumber.font = [UIFont systemFontOfSize:SIZE_FONT_10];
    self.OnLineNumber.text = @"0";
    self.OnLineNumber.textColor = [UIColor whiteColor];
    [self addSubview:self.OnLineNumber];
    self.OnLineNumber.sd_layout
    .leftSpaceToView(imagev,5)
    .centerYEqualToView(imagev)
    .heightIs(14);
    [self.OnLineNumber setSingleLineAutoResizeWithMaxWidth:60];
    [self updateLayout];
    
}

- (void)setupGuest
{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    
    self.anchorBGview = [[UIView alloc] init];
    self.anchorBGview.backgroundColor = [UIColor colorFromHexRGB:@"FF1130"];
    [self addSubview:self.anchorBGview];
    self.anchorBGview.sd_cornerRadiusFromHeightRatio = @0.5;
    self.anchorBGview.layer.masksToBounds = YES;
    self.anchorBGview.sd_layout
    .leftSpaceToView(self,2)
    .rightSpaceToView(self,0)
    .centerYEqualToView(self)
    .heightIs(32);
    
    
//    self.labelscorll = [[LabelScorllView alloc] initWithFrame:CGRectZero font:SIZE_FONT_12];
    if (self.labelscorll == nil) {
        self.labelscorll = [[RollLabel alloc] initWithFrame: CGRectZero];
        [self setRoomName:_roomName];
    }
//    self.labelscorll.backgroundColor = [UIColor clearColor];
    [self addSubview:self.labelscorll];
    self.labelscorll.sd_layout
    .leftSpaceToView(self,15)
    .topSpaceToView(self,2)
    .heightIs(17);
    
    
    
    UIImageView *imagev = [[UIImageView alloc]initWithFrame:CGRectZero];
    imagev.contentMode = UIViewContentModeScaleAspectFill;
    [imagev setImage:[UIImage imageNamed:@"home_people"]];
    [self addSubview:imagev];
    imagev.sd_layout
    .leftEqualToView(self.labelscorll)
    .topSpaceToView(self.labelscorll,2)
    .heightIs(8)
    .widthIs(14);
    
    self.OnLineNumber = [[UILabel alloc]initWithFrame:CGRectZero];
    self.OnLineNumber.font = [UIFont systemFontOfSize:SIZE_FONT_10];
    self.OnLineNumber.text = @"0";
    self.OnLineNumber.textColor = [UIColor whiteColor];
    [self addSubview:self.OnLineNumber];
    self.OnLineNumber.sd_layout
    .leftSpaceToView(imagev,5)
    .centerYEqualToView(imagev)
    .heightIs(14);
    [self.OnLineNumber setSingleLineAutoResizeWithMaxWidth:60];
    [self updateLayout];
    
}

- (void)setAnchorType:(AnchorViewType)anchorType
{
    _anchorType = anchorType;
    NSString *num = self.OnLineNumber.text;
    if (_anchorType == AnchorOnScreen) {
        [self setupAnchor];
    }else{
        [self setupGuest];
    }
    self.OnLineNumber.text = num;
    [self assemblingModel];
}

/**
 *  设置房间title
 */
- (void)setRoomName:(NSString *)roomName
{
    _roomName = roomName;
    [self.labelscorll setText: _roomName font: SIZE_FONT_12];
}

/**
 *  装配model，为了与设置model分离
 */
- (void)assemblingModel
{
    if (_AnchorIcon) {
        [_AnchorIcon sd_setImageWithURL:[NSURL URLWithString:_pmodel.userFaceURL] placeholderImage:ICON_PLACEHOLDER];
    }
    
//    if (_pmodel.userName) {
//        NSString *title = [NSString stringWithFormat:@"%@%@",_pmodel.userName,LocalizedString(@"INVITE_ROOM_TITLE")];
//        self.labelscorll.text = title;
//    }
    [self layoutWholeView];
}


-(void)setPmodel:(VLSUserProfile *)pmodel{
    _pmodel = pmodel;
    //    [self setupGuest];
    [self assemblingModel];
    
}

#pragma mark 主播类型

#pragma mark 观众类型

/**
 *  未关注
 */
- (void)notConcerned
{
    if (_anchorType == AnchorOnScreen) {
        // 添加关注button
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.button setTitle:LocalizedString(@"MINE_FOCUS") forState:UIControlStateNormal];
        //    [self.button setBackgroundColor:[UIColor redColor]];
        [self.button setBackgroundColor:[UIColor whiteColor]];
        [self.button setTitleColor:[UIColor colorFromHexRGB:@"FF1130"] forState:UIControlStateNormal];
        self.button.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.button];
        [self layoutWholeView];
    }else{
        
    }
    
#pragma mark 添加定时器5秒后消失
    
}

#pragma mark 自适应方法

- (void)layoutWholeView
{
    
    // 设置self宽度
    if (_anchorType == AnchorOnScreen) {
        
        if (self.button) {
            self.button.sd_layout
            .leftSpaceToView([self longerLabel],5)
            .centerYEqualToView(self)
            .widthIs(38)
            .heightIs(20);
            self.button.sd_cornerRadiusFromHeightRatio = @0.5f;
            
            // 根据子视图更改anchorview的宽度
            self.sd_layout.widthIs([self longerLabelwidths] + 55);
            
        }else{
            self.sd_layout.widthIs([self longerLabelwidths] + 15);
        }
        
    }else{
        
        self.sd_layout.widthIs([self longerLabelwidths] + 15);
        
        
    }
    
    [self updateLayout];
    
}


#pragma mark 定时器方法

- (void)dismissButton
{
    // remove button
    if (self.button) {
        [self.button removeFromSuperview];
        self.button = nil;
    }
    
    // 设置self宽度
    
    [UIView animateWithDuration:1.0f animations:^{
        self.sd_layout.widthIs([self longerLabelwidths] + 15);
        [self updateLayout];
    } completion:^(BOOL finished) {
        
    }];
    
    //    [self.GTtimer invalidate];
    //    self.GTtimer = nil;
}

#pragma mark button点击事件

- (void)buttonClick
{
    
    if ([_delegate respondsToSelector:@selector(focusAnchor)]) {
        [_delegate focusAnchor];
    }
    
    [self dismissButton];
}

- (void)anchorTap
{
    
    if ([_delegate respondsToSelector:@selector(anchoViewClicked:)]) {
        [_delegate anchoViewClicked:self];
    }
}


- (CGFloat)longerLabelwidths
{
    
    [self.labelscorll updateLayout];
    [self.OnLineNumber updateLayout];
    
    CGFloat Nothingwidth = self.labelscorll.frame.size.width;
    
    
    CGFloat NothinglabelX = self.labelscorll.frame.origin.x + Nothingwidth;
    CGFloat onLineNumberX = self.OnLineNumber.frame.origin.x + self.OnLineNumber.frame.size.width;
    return (NothinglabelX >= onLineNumberX)?NothinglabelX:onLineNumberX;
}

- (UIView *)longerLabel
{
    [self.labelscorll updateLayout];
    [self.OnLineNumber updateLayout];
    CGFloat Nothingwidth = self.labelscorll.frame.size.width;
    
    
    CGFloat NothinglabelX = self.labelscorll.frame.origin.x + Nothingwidth;
    CGFloat onLineNumberX = self.OnLineNumber.frame.origin.x + self.OnLineNumber.frame.size.width;
    return (NothinglabelX >= onLineNumberX)?self.labelscorll:self.OnLineNumber;
}

// 因为当前要显示的值在千分逗号以后，解析之后是一，所以需要有一个保存当前显示的值
- (void)setLookNum:(NSString *)lookNum
{
    _lookNum = lookNum;
    self.OnLineNumber.text = [NSString thousandpointsNum:[_lookNum integerValue]];
    NSLog(@"当前显示的值%@",_lookNum);
}

#pragma mark 定时器比较当前数字与显示的值
- (void)compareLabelTureNum:(NSString *)Num
{
    _tureNum = Num;
    if (_timer == nil) {
        uint64_t interval = (uint64_t)(0.5 * NSEC_PER_SEC);
        dispatch_queue_t queue = dispatch_queue_create("my queue", 0);
        _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
        dispatch_source_set_timer(_timer, dispatch_time(DISPATCH_TIME_NOW, 0), interval, 0);
        dispatch_source_set_event_handler(_timer, ^()
                                          {
                                              [self compareAction];
                                              
                                          });
        dispatch_resume(_timer);
    }
}

- (void)compareAction
{
    NSInteger labelNum = [_lookNum integerValue];
    NSString *wantNum = _tureNum;
    NSLog(@"目标值%@",_tureNum);
    NSInteger wantNumInt = [wantNum integerValue];
    if (labelNum <= wantNumInt) {
        // 递增
        if ((wantNumInt - labelNum) > 3 && (wantNumInt - labelNum) <= 10) {
            int random = [self randomint:0 to:(int)(wantNumInt - labelNum)];
            labelNum = labelNum + random;
        }else if((wantNumInt - labelNum) <= 3){
            labelNum = wantNumInt;
        }else if ((wantNumInt - labelNum) > 10){
            int random = [self randomint:0 to:(int)(wantNumInt - labelNum)/2];
            labelNum = labelNum + random;
        }
    }else{
        // 递减
        if ((labelNum - wantNumInt) > 3 && (labelNum - wantNumInt) <= 10) {
            int random = [self randomint:0 to:(int)(labelNum - wantNumInt)];
            labelNum = labelNum - random;
        }else if((labelNum - wantNumInt) <= 3){
            labelNum = wantNumInt;
        }else if ((labelNum - wantNumInt) > 10){
            int random = [self randomint:0 to:(int)(labelNum - wantNumInt)/2];
            labelNum = labelNum - random;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // code here
        self.lookNum = [NSString stringWithFormat:@"%ld",(long)labelNum];
        if ([_lookNum isEqualToString:_tureNum]) {
            _timer = nil;
        }
        if (labelNum > 1000) {
            [self layoutWholeView];
        }
    });
}

- (int)randomint:(int)from to:(int)to
{
    
    int random = (from + (arc4random() % (to-from + 1)));
    return random;
}

//更新关注按钮title
- (void)updateStatusFocusBt:(BOOL)isAttend
{
    if (isAttend) {
        [self notConcerned];
    } else {
        [self dismissButton];
    }
}

- (void)dealloc
{
    _timer = nil;
}

//- (void)restartAnimation
//{
//    [self.labelscorll restartTextScorll];
//}

@end
