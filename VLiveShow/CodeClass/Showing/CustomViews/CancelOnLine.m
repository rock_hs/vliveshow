//
//  CancelOnLine.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "CancelOnLine.h"
#import <SDAutoLayout/UIView+SDAutoLayout.h>
#import "UserCollectionViewCell.h"
#import "VLSUserProfile.h"
#import "OnLineViewModel.h"
#import "VLSMessageManager.h"



@interface CancelOnLine ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSArray *userData;
// 点击选择的成员
@property (nonatomic, strong) NSMutableArray *selectedUsers;

@property (nonatomic, assign) BOOL isAnchor;

@end

@implementation CancelOnLine

- (instancetype)initWithFrame:(CGRect)frame Users:(NSArray *)users isAnchor:(BOOL)type
{
    if (self = [self initWithFrame:frame]) {
        self.isAnchor = type;
        self.userData = users;
        self.collection = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self flowLayout]];
        self.collection.delegate = self;
        self.collection.dataSource = self;
        [self addSubview:self.collection];
        self.collection.backgroundColor = [UIColor whiteColor];
        self.collection.sd_layout
        .topSpaceToView(self.label,0)
        .leftSpaceToView(self,0)
        .rightSpaceToView(self,0)
        .bottomSpaceToView(self.onLineButton,0);
        
        /**
         *  下列的第一个方法方法，更新布局，此时view的frame就是真实的frame
            第二个方法是当了layout自动更新后的回调block返回真实的frame
         */
//        [self.collection updateLayout];
//        [self.collection setDidFinishAutoLayoutBlock:^(CGRect frame) {
//            
//        }];
        [self.collection registerClass:[UserCollectionViewCell class] forCellWithReuseIdentifier:@"IconIdentifier"];
        [self CancleOnline];
        self.selectedUsers = [NSMutableArray array];
//        for (VLSUserProfile *profile in self.userData) {
//            OnLineViewModel *Vmodel = [[OnLineViewModel alloc]init];
//            [Vmodel userProfileToViewModel:profile];
//            [self.selectedUsers addObject:Vmodel];
//        }
        
    }
    return self;
}

- (void)CancleOnline
{
    if (self.isAnchor) {
        self.label.text = @"申请连线的观众";
        [self.onLineButton setTitle:@"确定" forState:UIControlStateNormal];
        [self.onLineButton addTarget:self action:@selector(CancleOnLine) forControlEvents:UIControlEventTouchUpInside];
    }else{
        self.label.text = @"已连接主播";
        [self.onLineButton setTitle:@"取消连线" forState:UIControlStateNormal];
        [self.onLineButton addTarget:self action:@selector(CancleOnLine) forControlEvents:UIControlEventTouchUpInside];

    }
}
- (UICollectionViewFlowLayout *)flowLayout
{
    UICollectionViewFlowLayout *flowlayout = [[UICollectionViewFlowLayout alloc]init];
    flowlayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowlayout.minimumLineSpacing = 0.0f;
    flowlayout.minimumInteritemSpacing = 0.0f;
    
    return flowlayout;
}
#pragma mark 点击事件

- (void)CancleOnLine
{
    NSMutableArray *GT_array = [NSMutableArray array];
    for (NSIndexPath *indexPath in self.selectedUsers) {
        [GT_array addObject:self.userData[indexPath.row]];
    }
    if (self.isAnchor) {
        // 接收邀请API
        VLSUserProfile *users = GT_array[0];
        if (users) {
//            [[VLSMessageManager sharedManager] acceptLinkRequest:users.userId];
        }
    }else{
        // 取消连线API
        
    }
    
    
}

#pragma mark flowlayoutDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(collectionView.bounds.size.width/4, collectionView.bounds.size.height/2);
    NSLog(@"%@",[NSValue valueWithCGSize:size]);
    return size;
}

#pragma mark dalegate datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.userData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UserCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IconIdentifier" forIndexPath:indexPath];
    VLSUserProfile *profile = self.userData[indexPath.row];
    [cell setOnLineModel:profile];
    cell.userInteractionEnabled = YES;
    cell.alpha = 1.0f;
    if (self.selectedUsers.count != 0) {
        
        if ([self.selectedUsers containsObject:indexPath]) {
            cell.GT_status.selected = YES;
        }else{
            if (self.selectedUsers.count == 4) {
                cell.userInteractionEnabled = NO;
                cell.alpha = 0.5f;
            }
        
        }
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    UserCollectionViewCell *cell = (UserCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (cell.GT_status.selected) {
        cell.GT_status.selected = NO;
        [self.selectedUsers removeObject:indexPath];
    }else{
        cell.GT_status.selected = YES;
        [self.selectedUsers addObject:indexPath];
    }
    if (self.selectedUsers.count == 4 || self.selectedUsers.count == 3) {
        [collectionView reloadData];
    }
}

@end
