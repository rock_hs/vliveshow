//
//  CancelOnLine.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "GT_BGView.h"


@interface CancelOnLine : GT_BGView

@property (nonatomic, strong) UICollectionView *collection;

- (instancetype)initWithFrame:(CGRect)frame Users:(NSArray *)users isAnchor:(BOOL)type;

@end
