//
//  VLSBottomView.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBottomView.h"
#import "MessageShowView.h"
#import "VLSToolTipsView.h"
#import "VLSUtily.h"

static const CHATBUTTON_TAG = 4505;

@interface VLSBottomView ()

@property (nonatomic, strong) VLSToolTipsView *messageview;

@property (nonatomic, assign) BOOL isFirstLink;
@property (nonatomic, assign) ActionStatus actionStatus;
@property (nonatomic, strong) NSArray *maskArray;       //通过ActionStatus生成的mask，过滤静音、相机翻转、邮件通知、连线、礼物、分享功能
@property (nonatomic, assign) BOOL isForbidSendMsg;     //是否被禁言

@property (nonatomic, weak) UIButton* rateButton;
@end

@implementation VLSBottomView
- (NSArray *)maskArray {
    //TODO:don'trepeatyourself
    return [VLSUtily toBinarySystemWithDecimalSystem:self.actionStatus];
}

- (instancetype)initWithFrame:(CGRect)frame actionStatus:(ActionStatus)actionStatus
{
    if (self = [super initWithFrame:frame]) {
        self.isFirstLink = YES;
        self.actionStatus = actionStatus;
    }
    return self;
}

- (void)configBtn
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = CHATBUTTON_TAG;
    btn.frame = CGRectMake(12, 4, 36, 36);

    [btn setImage:[UIImage imageNamed:@"but_bc_chat"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"but_bc_chat"] forState:UIControlStateHighlighted];
    [self addSubview:btn];
    
    [btn addTarget:self action:@selector(btnMsgClick:) forControlEvents:UIControlEventTouchUpInside];
    NSArray *arr = [NSArray array];
    NSArray *selectArr = [NSArray array];
    
    NSMutableArray *fullFounction = [@[@"live_yincang", @"broadcast_host_par_view_mute",@"broadcast_Host_par_view_camera",@"broadcast-news",@"broadcast-bbg",@"but_bc_gift",@"share02", @"ic_fankui"] mutableCopy];
    NSMutableArray *fullSeletor = [@[@"live_xianshi", @"mic03",@"camera01@2x",@"letter01",@"ic_lianxian_tishi",@"but_bc_gift",@"share01", @"ic_fankui"] mutableCopy];
    /*
     AllShow = 0,
     GuestViewHidden = 1UL << 0,
     MuteHidden =  1UL << 1,
     CameraHidden = 1UL << 2,
     MailHidden = 1UL << 3,
     ConnectionHidden = 1UL << 4,
     GiftHidden = 1UL << 5,
     ShareHidden = 1UL << 6,
     */
    if (_actionType == ActionTypeLive) {
        if (self.actionStatus == ConnectionHidden) {
            self.actionStatus = GuestViewHidden | MuteHidden | CameraHidden | ConnectionHidden;
        }else {
            self.actionStatus = GuestViewHidden | MuteHidden | CameraHidden;
        }
    } else if (_actionType == ActionTypeAnchor){
        self.actionStatus = GuestViewHidden | GiftHidden | RateHidden;
    } else if (_actionType == ActionTypeGuest){
        if (self.actionStatus == ConnectionHidden) {
            self.actionStatus = ConnectionHidden;
        }else {
//            self.actionStatus = AllShow;
            self.actionStatus = GuestViewHidden;
        }
    } else if (_actionType == ActionTypeAdmin){
        self.actionStatus = GuestViewHidden | CameraHidden | GiftHidden | RateHidden;
    }else if (_actionType == ActionTypeLessonTeacher) {
        self.actionStatus = GuestViewHidden | GiftHidden | RateHidden;
    }else if (_actionType == ActionTypeLessonAttendance) {
        self.actionStatus = GuestViewHidden | MuteHidden | CameraHidden | RateHidden;
    }else if (_actionType == ActionTypeLessonAudience) {
        self.actionStatus = GuestViewHidden | MuteHidden | CameraHidden | ConnectionHidden | RateHidden;
    }else if (_actionType == ActionTypeNormalLandscape) {
        self.actionStatus = GuestViewHidden | MuteHidden | CameraHidden;
    }else if (_actionType == ActionTypeConnectedLandscape_Attendance) {
        self.actionStatus = AllShow;
    }else if (_actionType == ActionTypeConnectedLandscape_Audience) {
        self.actionStatus = MuteHidden | CameraHidden;
    }
    
    [self.maskArray enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [fullFounction removeObjectAtIndex:([obj integerValue] - idx)];
        [fullSeletor removeObjectAtIndex:([obj integerValue] - idx)];
    }];
    
    arr = (NSArray*)fullFounction;
    selectArr = (NSArray*)fullSeletor;
    
    CGFloat margin = 0.0f;
    if (arr.count >= 7 && SCREEN_WIDTH <= 320)
    {
        margin = 2.0f;
    }
    else if (arr.count >= 7) {
        margin = 7.0f;
    }else{
        margin = 16.0f;
    }
    
    UIButton *leftBtn = nil;
    for (int i = 0; i < arr.count; i++) {
      
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(SCREEN_WIDTH + i * (36+margin) - (36+margin) * arr.count + margin-12, 4, 36, 36);
        if (i == 0) {
            leftBtn = btn;
        }
        [btn setBackgroundImage:[UIImage imageNamed:[arr objectAtIndex:i]] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:[selectArr objectAtIndex:i]] forState:UIControlStateSelected];
        [self addSubview:btn];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 1 + i;
        
        NSInteger count = arr.count;
        
        if ([[arr objectAtIndex:i] isEqualToString: @"ic_fankui"])
        {
            self.rateButton = btn;
        }
        
        btn.sd_layout
//        .rightSpaceToView(self, i * (margin + 36) + margin)
        .rightSpaceToView(self, (count - i - 1) * (36 + margin) + margin)
        .topSpaceToView(self, 4)
        .widthIs(36)
        .heightIs(36);
    }
    [self setForbidSendMsg:self.isForbidSendMsg];
//    ActionTypeLessonLandscape,        //课程横屏模式
//    ActionTypeNormalLandscape,        //PC横屏模式
//    ActionTypeConnectedLandscape_Attendance,                 //横屏嘉宾连线模式
//    ActionTypeConnectedLandscape_Audience,       //横屏观众连线模式
    if (self.actionType == ActionTypeLessonLandscape || self.actionType == ActionTypeNormalLandscape || self.actionType == ActionTypeConnectedLandscape_Attendance || self.actionType == ActionTypeConnectedLandscape_Audience) {
        CGRect rect = btn.frame;
        rect.origin.x = CGRectGetMinX(leftBtn.frame) - 36 - margin;
        btn.frame = rect;
    }
}

//改变站内信的状态
- (void)changeMailIcon:(BOOL)isMail{

    for (UIButton *btn in self.subviews) {
        if (self.actionType == ActionTypeLive) {
            if (btn.tag == 1) {
                [self showRedIcon:isMail tag:1001 onButton:btn];
            }
        }else if (self.actionType == ActionTypeAnchor){
            if (btn.tag == 3) {
                [self showRedIcon:isMail tag:1003 onButton:btn];
            }
        }else if (self.actionType == ActionTypeAnchor){
            if (btn.tag == 2) {
                [self showRedIcon:isMail tag:1002 onButton:btn];
            }
        }
    }
}

- (void)showfriendicon:(BOOL)show;
{
    for (UIButton *button in self.subviews) {
        if (button.tag == 4) {
            [self showRedIcon:show tag:1004 onButton:button];
        }
    }
}

- (void)showVoiceIcon:(BOOL)show
{
    for (UIButton *button in self.subviews) {
        if (self.actionType == ActionTypeLessonLandscape || self.actionType == ActionTypeNormalLandscape || self.actionType == ActionTypeConnectedLandscape_Attendance || self.actionType == ActionTypeConnectedLandscape_Audience) {
            if (button.tag == 2) {
                [self showRedIcon:show tag:1001 onButton:button];
            }
        } else {
            if (button.tag == 1) {
                [self showRedIcon:show tag:1001 onButton:button];
            }
        }
    }
}

- (void)showRedIcon:(BOOL)show tag:(NSInteger)tag onButton:(UIButton *)btn
{
    if (show) {
        [btn dot:RGB16(COLOR_BG_FF1130) frame:CGRectMake(btn.bounds.size.width * 3/4, 0, 8, 8)];
    }else{
        [btn removeDot];
    }
}

- (void)changefriendStatus:(BOOL)isguest
{
    for (UIButton *button in self.subviews) {
        if (self.actionType == ActionTypeLive) {
            if (button.tag == 2) {
                button.selected = isguest;
            }
        }else if (self.actionType == ActionTypeAnchor){
            if (button.tag == 4) {
                button.selected = isguest;
            }
        }
     
    }
}

- (void)voiceEnable:(BOOL)enable
{
    for (UIButton *button in self.subviews) {
        if (button.tag == 1) {
            button.enabled = enable;
            if (enable) {
                [button setBackgroundImage:[UIImage imageNamed:@"broadcast_host_par_view_mute"] forState:UIControlStateNormal];
                button.hidden = NO;
            }else{
                button.hidden = YES;
            }
        }
    }
}


- (void)btnMsgClick:(UIButton *)sender
{
    if ([_delegate respondsToSelector:@selector(bottomMessageClicked)]) {
        [_delegate bottomMessageClicked];
    }
}

- (void)btnClick:(UIButton *)sender
{
    NSMutableArray *fullSeletor = [@[NSStringFromSelector(@selector(bottomGuestViewHiddenClicked:)),NSStringFromSelector(@selector(bottomVoiceClicked:)), NSStringFromSelector(@selector(bottomChangeCamera)), NSStringFromSelector(@selector(bottomMailClicked)), NSStringFromSelector(@selector(bottomFriendClicked:)), NSStringFromSelector(@selector(bottomGiftClicked)), NSStringFromSelector(@selector(bottomShareClicked)), NSStringFromSelector(@selector(bottomRateClicked))] mutableCopy];
    [self.maskArray enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [fullSeletor removeObjectAtIndex:([obj integerValue] - idx)];
    }];
    NSInteger index = (NSInteger)(sender.tag - 1);
    NSString *selStr = [fullSeletor objectAtIndex:index];
    SEL sel = NSSelectorFromString(selStr);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    if ([self.delegate respondsToSelector:sel]) {
        if ([selStr isEqualToString:NSStringFromSelector(@selector(bottomGuestViewHiddenClicked:))] || [selStr isEqualToString:NSStringFromSelector(@selector(bottomVoiceClicked:))] || [selStr isEqualToString:NSStringFromSelector(@selector(bottomFriendClicked:))])
            [self.delegate performSelector:sel withObject:sender];
        else
            [self.delegate performSelector:sel];
    }
#pragma clang diagnostic pop
}

- (void)setActionType:(ActionType)actionType {
    _actionType = actionType;
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    [self configBtn];
}


// 主播第一次開啟直播，彈出的氣泡
- (void)showisFirstShow
{
    if (self.messageview) {
        [self.messageview removeFromSuperview];
        self.messageview = nil;
    }
    
    // 判断是否是第一次开启直播
    if (![[NSUserDefaults standardUserDefaults] boolForKey:isFirstShow]) {
        return;
    }
    
    CGFloat frame = 0.0;
    for (UIButton *button in self.subviews) {
//        if (self.actionType == ActionTypeAnchor){
        if (self.actionType == ActionTypeAnchor || self.actionType == ActionTypeLessonTeacher){
            if (button.tag == 4) {
                CGPoint btnCenter = button.center;
                frame = btnCenter.x;
            }
        }
        
    }
    self.messageview = [[VLSToolTipsView alloc] initWithFrame:CGRectZero WithText:LocalizedString(@"ANCHOR_FIRST_SHOW") andAnchorPoint:frame tapBlock:^{
       [self.messageview removeFromSuperview];
    } autoDismiss:YES];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isFirstShow];
}

- (void)showisFirstWatch
{
    if (self.messageview) {
        [self.messageview removeFromSuperview];
        self.messageview = nil;
    }
    // 判断是否是第一次开启直播
    if (![[NSUserDefaults standardUserDefaults] boolForKey:isFirstWatch]) {
        return;
    }
    
    CGFloat frame = 0.0;
    for (UIButton *button in self.subviews) {
        if (self.actionType == ActionTypeLive){
            if (button.tag == 2) {
                CGPoint btnCenter = button.center;
                frame = btnCenter.x;
            }
        }else if (self.actionType == ActionTypeLessonAttendance) {
            if (button.tag == 2) {
                CGPoint btnCenter = button.center;
                frame = btnCenter.x;
            }
        }
        
    }
    self.messageview = [[VLSToolTipsView alloc] initWithFrame:CGRectZero WithText:LocalizedString(@"AUDICEN_FIRST_SHOW") andAnchorPoint:frame tapBlock:^{
        [self.messageview removeFromSuperview];
    } autoDismiss:YES];
    [self addSubview:self.messageview];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isFirstWatch];
}

- (void)isFirstRequestLink
{
    // 判断是否是第一次来到的请求
    if (self.isFirstLink == NO) {
        return;
    }
    
    if (self.messageview) {
        [self.messageview removeFromSuperview];
        self.messageview = nil;
    }
    CGFloat frame = 0.0;
    for (UIButton *button in self.subviews) {
        if (self.actionType == ActionTypeAnchor){
            if (button.tag == 4) {
                CGPoint btnCenter = button.center;
                frame = btnCenter.x;
            }
        }else if (self.actionType == ActionTypeLessonTeacher) {
            if (button.tag == 4) {
                CGPoint btnCenter = button.center;
                frame = btnCenter.x;
            }
        }
        
    }
    self.messageview = [[VLSToolTipsView alloc] initWithFrame:CGRectZero WithText:LocalizedString(@"ANCHOR_FIRST_REQUEST_LINK") andAnchorPoint:frame tapBlock:^{
        [self.messageview removeFromSuperview];
    } autoDismiss:YES];
    [self addSubview:self.messageview];
    self.isFirstLink = NO;
}
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
    if (view == nil) {
        CGPoint tempoint = [self.messageview convertPoint:point fromView:self];
        if (CGRectContainsPoint(self.messageview.bounds, tempoint))
        {
            view = self.messageview;
        }
    }
    return view;
}

- (void)checkMessageExistence
{
    if (self.messageview) {
        [self.messageview removeFromSuperview];
        self.messageview = nil;
    }
}

- (void)setForbidSendMsg:(BOOL)forbid {
    self.isForbidSendMsg = forbid;
    UIButton *chatBtn = [self viewWithTag:CHATBUTTON_TAG];
    [chatBtn setEnabled:!forbid];
    UIImage *image = forbid ? [UIImage imageNamed:@"unchat"] : [UIImage imageNamed:@"but_bc_chat"];
    [chatBtn setImage:image forState:UIControlStateNormal];
    [chatBtn setBackgroundImage:image forState:UIControlStateHighlighted];
}

- (void)enableRatingButton: (BOOL)enable
{
    self.rateButton.enabled = enable;
}

@end
