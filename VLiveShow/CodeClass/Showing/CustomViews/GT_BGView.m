//
//  GT_BGView.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "GT_BGView.h"
#import <SDAutoLayout/UIView+SDAutoLayout.h>


@implementation GT_BGView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // 连线直播主 label
        self.backgroundColor = [UIColor whiteColor];
        self.label = [[UILabel alloc]initWithFrame:CGRectZero];
        self.label.font = [UIFont systemFontOfSize:15];
        self.label.textColor = [UIColor darkGrayColor];
        self.label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.label];
        self.label.sd_layout
        .topSpaceToView(self,10)
        .leftSpaceToView(self,0)
        .rightSpaceToView(self,0)
        .heightIs(30);
        
        self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeButton setImage:[UIImage imageNamed:@"livebefore_close2@3x"] forState:UIControlStateNormal];
        self.closeButton.imageEdgeInsets = UIEdgeInsetsMake(0, 12, 12, 0);
        [self addSubview:self.closeButton];
        self.closeButton.sd_layout
        .topEqualToView(self.label)
        .rightSpaceToView(self,10)
        .heightIs(30)
        .widthEqualToHeight();
        
        
        
        self.onLineButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [self.onLineButton setBackgroundImage:[self ResizingModeStretch] forState:UIControlStateNormal];
        [self.onLineButton setBackgroundColor:RGB16(COLOR_BG_FF1130)];
        [self.onLineButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:self.onLineButton];
        self.onLineButton.sd_layout
        .bottomSpaceToView(self,0)
        .leftSpaceToView(self,0)
        .rightSpaceToView(self,0)
        .heightIs(50);
        
//        self.alpha = 0.9;
        self.hidden = NO;
    }
    return self;
}

- (UIImage *)ResizingModeStretch
{
    UIImage *image = [UIImage imageNamed:@"button04"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch];
    return image;
}

- (void)hideSelf
{
    
    self.hidden = YES;

}

- (void)setHidden:(BOOL)hidden
{
    if (hidden) {
        [UIView animateWithDuration:.5f animations:^{
            
            CGRect frame = self.frame;
            frame.origin.y = SCREEN_HEIGHT;
            self.frame = frame;
        } completion:^(BOOL finished) {
            [super setHidden:hidden];
        }];
    }else{
        
        [super setHidden:hidden];
        [UIView animateWithDuration:.4f animations:^{
            CGRect frame = self.frame;
            frame.origin.y = SCREEN_HEIGHT - frame.size.height;
            self.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
    }
}

@end
