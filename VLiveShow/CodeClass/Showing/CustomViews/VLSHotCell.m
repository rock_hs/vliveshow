//
//  VLSHotCell.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSHotCell.h"
#import "UIView+SDAutoLayout.h"
#import "VLSLiveListModel.h"

@interface VLSHotCell()
/// 用户头像
@property (nonatomic, strong)UIImageView *iconImageView;
/// 用户昵称
@property (nonatomic, strong)UILabel *nameLabel;
/// 地理位置图标
@property (nonatomic, strong)UIImageView *mapImageView;
/// 地址信息
@property (nonatomic, strong)UILabel *addressLabel;
/// 在线人数配图
@property (nonatomic, strong)UIImageView *countImageView;
/// 显示在线观看人数的label
@property (nonatomic, strong)UILabel *countLabel;
/// 封面 Imageview
@property (nonatomic, strong)UIImageView *coverImageView;
/// 显示直播状态的 Label
@property (nonatomic, strong)UILabel *playStatusLabel;
/// 主播标题
@property (nonatomic, strong)UILabel *titleLabel;
/// 播放按钮
@property (nonatomic, strong)UIImageView *playImageView;
/// 模糊效果
@property (nonatomic, strong)UIView *effectView;


@end

@implementation VLSHotCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self configUI];
    }
    return self;
}

- (void)configUI
{
    [self.contentView addSubview:self.coverImageView];
    [self.coverImageView addSubview:self.effectView];
    [self.coverImageView addSubview:self.iconImageView];
    [self.coverImageView addSubview:self.nameLabel];
    [self.coverImageView addSubview:self.mapImageView];
    [self.coverImageView addSubview:self.addressLabel];
    [self.coverImageView addSubview:self.playStatusLabel];
    [self.coverImageView addSubview:self.titleLabel];
  //  [self.coverImageView addSubview:self.playImageView];
    [self.coverImageView addSubview:self.countLabel];
    [self.coverImageView addSubview:self.countImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapIconImageView)];
    [self.iconImageView addGestureRecognizer:tap];
    
    // 添加背景图层
    self.effectView.sd_layout
    .topSpaceToView(self.coverImageView, 0)
    .leftSpaceToView(self.coverImageView, 0)
    .rightSpaceToView(self.coverImageView, 0)
    .bottomSpaceToView(self.coverImageView, 0);

    self.coverImageView.sd_layout
    .leftSpaceToView(self.contentView, 0)
    .topSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .bottomSpaceToView(self.contentView, 0);
    
    self.iconImageView.sd_layout
    .leftSpaceToView(self.coverImageView, 10)
    .topSpaceToView(self.coverImageView, 5)
    .widthIs(40)
    .heightIs(40);
    
    self.nameLabel.sd_layout
    .leftSpaceToView(self.iconImageView, 5)
    .centerYEqualToView(self.iconImageView)
    .widthIs(200)
    .heightIs(20);
    
    self.mapImageView.sd_layout
    .leftSpaceToView(self.coverImageView, 10)
    .bottomSpaceToView(self.coverImageView, 7)
    .widthIs(10)
    .heightIs(13);
    
    self.addressLabel.sd_layout
    .leftSpaceToView(self.mapImageView, 5)
    .bottomSpaceToView(self.coverImageView, 5)
    .heightIs(17);
    
    self.countImageView.sd_layout
    .leftSpaceToView(self.addressLabel, 15)
    .bottomSpaceToView(self.coverImageView, 7)
    .heightIs(12)
    .widthIs(20);
    
    self.countLabel.sd_layout
    .bottomSpaceToView(self.coverImageView, 3)
    .leftSpaceToView(self.countImageView, 5)
    .heightIs(20)
    .widthIs(150);
    
    self.playImageView.sd_layout
    .topSpaceToView(self.coverImageView, SCREEN_WIDTH * 9 / 32 - 25)
    .widthIs(50)
    .heightIs(50)
    .centerXIs(SCREEN_WIDTH / 2);
    
    self.titleLabel.sd_layout
    .leftSpaceToView(self.coverImageView, 10)
    .rightSpaceToView(self.coverImageView, 10)
    .bottomSpaceToView(self.mapImageView, 10)
    .autoHeightRatio(0);
    
    self.playStatusLabel.sd_layout
    .rightSpaceToView(self.coverImageView, 10)
    .centerYEqualToView(self.iconImageView)
    .widthIs(35)
    .heightIs(20);
    
}

#pragma mark - 点击用户头像跳转的页面

- (void)tapIconImageView {
    NSLog(@"%s", __func__);
}


- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        self.iconImageView = [[UIImageView alloc] initWithImage:ICON_PLACEHOLDER];
        self.iconImageView.layer.cornerRadius = 20;
        self.iconImageView.clipsToBounds = YES;
        self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.iconImageView.userInteractionEnabled = YES;
    }
    return _iconImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        
        self.nameLabel = [[UILabel alloc] init];
        self.nameLabel.textColor = [UIColor whiteColor];
        self.nameLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        self.nameLabel.text = @"";
        
    }
    return _nameLabel;
}

- (UIImageView *)mapImageView {
    if (!_mapImageView) {
        self.mapImageView = [[UIImageView alloc] init];
        self.mapImageView.image = [UIImage imageNamed:@"home_loction"];
    }
    return _mapImageView;
}

- (UILabel *)addressLabel {
    if (!_addressLabel) {
        self.addressLabel = [[UILabel alloc] init];
        self.addressLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        self.addressLabel.textColor = [UIColor whiteColor];
        self.addressLabel.textAlignment = NSTextAlignmentCenter;
        [self.addressLabel setSingleLineAutoResizeWithMaxWidth:100];
    }
    return _addressLabel;
}


- (UIImageView *)countImageView {
    if (!_countImageView) {
        self.countImageView = [[UIImageView alloc] init];
        self.countImageView.image = [UIImage imageNamed:@"home_people"];
    }
    return _countImageView;
}

- (UILabel *)countLabel {
    if (!_countLabel) {
        self.countLabel = [[UILabel alloc] init];
        self.countLabel.textColor = [UIColor whiteColor];
        self.countLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        
    }
    return _countLabel;
}


- (UIImageView *)playImageView {
    if (!_playImageView) {
        self.playImageView = [[UIImageView alloc] init];
        self.playImageView.image = [UIImage imageNamed:@"home_play"];
    }
    return _playImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_16];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}

- (UIImageView *)coverImageView {
    if (!_coverImageView) {
        self.coverImageView = [[UIImageView alloc] init];
        self.coverImageView.userInteractionEnabled = YES;
        self.coverImageView.contentMode = UIViewContentModeScaleToFill;
    }
    return _coverImageView;
}


- (UILabel *)playStatusLabel {
    if (!_playStatusLabel) {
        self.playStatusLabel = [[UILabel alloc] init];
        self.playStatusLabel.textColor = [UIColor whiteColor];
        self.playStatusLabel.textAlignment = NSTextAlignmentCenter;
        self.playStatusLabel.textColor = [UIColor whiteColor];
        self.playStatusLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
        self.playStatusLabel.layer.borderColor = [UIColor whiteColor].CGColor;
        self.playStatusLabel.layer.borderWidth = 1;
    }
    return _playStatusLabel;
}


- (UIView *)effectView {
    if (!_effectView) {
        self.effectView = [[UIView alloc] init];
        self.effectView.backgroundColor = [UIColor blackColor];
        self.effectView.alpha = 0.2;
    }
    return _effectView;
}

- (void)setLiveListModel:(VLSLiveListModel *)liveListModel {
    _liveListModel = liveListModel;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:_liveListModel.userCover] placeholderImage:ICON_PLACEHOLDER options:SDWebImageRefreshCached];
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:liveListModel.roomCover]];
    
    if (!liveListModel.hostNickName) {
        self.nameLabel.text = @"";
    } else {
    self.nameLabel.text = [NSString stringWithFormat:@"%@", liveListModel.hostNickName];
    }
    
    if (!liveListModel.location) {
        self.addressLabel.text = @"";
    } else {
    self.addressLabel.text = [NSString stringWithFormat:@"%@", liveListModel.location];
    }
    if (!liveListModel.title) {
        self.titleLabel.text = @"";
    } else {
    self.titleLabel.text = [NSString stringWithFormat:@"%@", liveListModel.title];
    }
    
    // 数字千分位格式化
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSString *newAmount = [formatter stringFromNumber:[NSNumber numberWithInteger:[NSString stringWithFormat:@"%@", liveListModel.scNum].integerValue]];
    self.countLabel.text = newAmount;
    
    if ([liveListModel.living isEqualToString:@"1"]) {
        
        self.playStatusLabel.text = LocalizedString(@"LIVEING");

    } else {
        
        self.playStatusLabel.text = @"已结束";
        
    }
    // 解决定位关的问题
    if ([self.addressLabel.text isEqualToString:@"定位关"]
        || [self.addressLabel.text isEqualToString:@"關閉定位"]
        || [self.addressLabel.text isEqualToString:@"定位關"]) {
        
        [self.addressLabel removeFromSuperview];
        [self.mapImageView removeFromSuperview];
        
        
        self.countImageView.sd_resetLayout
        .leftSpaceToView(self.coverImageView, 10)
        .bottomSpaceToView(self.coverImageView, 7)
        .heightIs(12)
        .widthIs(20);
        
        self.countLabel.sd_resetLayout
        .bottomSpaceToView(self.coverImageView, 3)
        .leftSpaceToView(self.countImageView, 5)
        .heightIs(20)
        .widthIs(150);
        
        self.titleLabel.sd_resetLayout
        .leftSpaceToView(self.coverImageView, 10)
        .rightSpaceToView(self.coverImageView, 10)
        .bottomSpaceToView(self.countImageView, 10)
        .autoHeightRatio(0);
        
        
    } else {
        
        [self.coverImageView addSubview:self.addressLabel];
        [self.coverImageView addSubview:self.mapImageView];
        
        self.mapImageView.sd_resetLayout
        .leftSpaceToView(self.coverImageView, 10)
        .bottomSpaceToView(self.coverImageView, 7)
        .widthIs(10)
        .heightIs(13);
        
        self.addressLabel.sd_resetLayout
        .leftSpaceToView(self.mapImageView, 5)
        .bottomSpaceToView(self.coverImageView, 5)
        .heightIs(17);
        
        self.countImageView.sd_resetLayout
        .leftSpaceToView(self.addressLabel, 15)
        .bottomSpaceToView(self.coverImageView, 7)
        .heightIs(12)
        .widthIs(20);
        
        self.countLabel.sd_resetLayout
        .bottomSpaceToView(self.coverImageView, 3)
        .leftSpaceToView(self.countImageView, 5)
        .heightIs(20)
        .widthIs(150);
        
        self.titleLabel.sd_resetLayout
        .leftSpaceToView(self.coverImageView, 10)
        .rightSpaceToView(self.coverImageView, 10)
        .bottomSpaceToView(self.mapImageView, 10)
        .autoHeightRatio(0);
        
    }
   
}

- (void)setFrame:(CGRect)frame {
    frame.size.height -= 6;
    [super setFrame:frame];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}



@end
