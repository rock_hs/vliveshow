//
//  VLSAudiencesView.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSUserProfile.h"
@protocol VLSAudiencesViewDelegate<NSObject>
- (void)audiencesViewSelect:(VLSUserProfile *)user;

@end
@interface VLSAudiencesView : UIView
@property (nonatomic,strong) NSMutableArray<VLSUserProfile *> *dataArray;
@property (nonatomic,assign) id<VLSAudiencesViewDelegate>delegate;
@property (nonatomic, assign) KLiveRoomOrientation oriention;
- (void)insertDataArray:(NSArray *)array;
- (void)deleteDataArray:(NSArray *)array;
- (BOOL)checkisFullList;
@end
