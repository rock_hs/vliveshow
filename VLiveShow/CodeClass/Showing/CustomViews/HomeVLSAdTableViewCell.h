//
//  HomeVLSAdTableViewCell.h
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSAdListModel.h"

@class HomeVLSAdTableViewCell;

@protocol CloseDelegate <NSObject>

-(void)closeAdCellWithIndex:(HomeVLSAdTableViewCell*)cell;

@end

@interface HomeVLSAdTableViewCell : UITableViewCell
//头像
@property (nonatomic, strong) UIImageView *icon;
//标题
@property (nonatomic, strong) UILabel     *titleLb;
//推广标签背景
@property (nonatomic, strong) UIImageView *flowImageView;
//推广label
@property (nonatomic, strong) UILabel     *flowLb;
//关闭叉号
@property (nonatomic, strong) UIButton    *closeBtn;
//副标题
@property (nonatomic, strong) UILabel     *subTitleLb;
//封面
@property (nonatomic, strong) UIImageView *coverImg;

@property (nonatomic, strong) VLSAdListModel *model;
//按钮点击事件
@property (nonatomic,assign) id <CloseDelegate> delegate;
@end
