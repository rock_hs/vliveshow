//
//  AnchorView.h
//  balabala
//
//  Created by SuperGT on 16/5/12.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSUserProfile.h"
typedef NS_ENUM(NSUInteger, AnchorViewType) {
    AnchorOnScreen,
    GuestOnScreen
};

@class VLSAnchorView;

@protocol VLSAnchorViewDelegate<NSObject>
@optional
//点击
- (void)anchoViewClicked:(VLSAnchorView *)anchorView;

- (void)focusAnchor;
@end

@interface VLSAnchorView : UIView

// 判断当前是主播还是观众
@property (nonatomic,assign) AnchorViewType anchorType;
@property (nonatomic, strong) VLSUserProfile *pmodel;
@property (nonatomic,nonnull,strong) UILabel *OnLineNumber;
@property (nonatomic,weak) id<VLSAnchorViewDelegate> delegate;
@property (nonatomic, copy) NSString *tureNum;
@property (nonatomic, strong) NSString *lookNum;
//直播房间名称
@property (nonatomic, strong) NSString* roomName;

- (void)notConcerned;

- (void)dismissButton;

- (void)layoutWholeView;

//- (void)restartAnimation;

- (void)compareLabelTureNum:(NSString *)Num;

//更新关注按钮title
- (void)updateStatusFocusBt:(BOOL)isAttend;

@end
