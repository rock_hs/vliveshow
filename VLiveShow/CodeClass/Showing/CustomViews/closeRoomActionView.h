//
//  closeRoomActionView.h
//  VLiveShow
//
//  Created by SuperGT on 16/7/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface closeRoomActionView : UIView


@property (nonatomic, strong) UIButton *cancleBtn;

@property (nonatomic, strong) UIButton *cancleGuestBtn;

@property (nonatomic, strong) UIButton *closeRoom;

@end
