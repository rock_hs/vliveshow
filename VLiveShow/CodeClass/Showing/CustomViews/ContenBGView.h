//
//  ContenBGView.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContenBGView : UIView

@property (nonnull,nonatomic,strong) UIImageView *selfIcon;

@property (nonnull,nonatomic,strong) UIImageView *hostIcon;

@property (nonnull,nonatomic,strong) CAShapeLayer *line;

@end
