//
//  HomeVLSAdTableViewCell.m
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HomeVLSAdTableViewCell.h"

@implementation HomeVLSAdTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self configUI];
    }
    return self;
}
-(void)setModel:(VLSAdListModel *)model{
    _model = model;
    [self.icon sd_setImageWithURL:[NSURL URLWithString:model.avatarUrl] placeholderImage:ICON_PLACEHOLDER options:SDWebImageAllowInvalidSSLCertificates completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.icon v_cornerRadiusWithImage:image cornerRadius:25 rectCornerType:UIRectCornerAllCorners];//调用UIBezier绘画圆角
    }];
    [self.coverImg sd_setImageWithURL:[NSURL URLWithString:model.coverUrl]];
    //标题赋值
    CGFloat width = [self widthOfString:model.title font:[UIFont systemFontOfSize:16] height:20];
    self.titleLb.sd_layout
    .leftSpaceToView(self.icon, X_CONVERT(7))
    .centerYEqualToView(self.icon)
    .widthIs(width)
    .heightIs(Y_CONVERT(21));
    self.titleLb.text = model.title;
    if (model.detailTitle != nil && ![model.detailTitle isEqualToString:@""]) {
        self.titleLb.sd_layout
        .leftSpaceToView(self.icon, X_CONVERT(7))
        .topSpaceToView(self, Y_CONVERT(14))
        .widthIs(width)
        .heightIs(Y_CONVERT(21));
        //副标题赋值
        CGFloat widthSub = [self widthOfString:model.detailTitle font:[UIFont systemFontOfSize:14] height:20];
        self.subTitleLb.sd_layout
        .leftSpaceToView(self.icon,X_CONVERT(7))
        .topSpaceToView(self.titleLb,Y_CONVERT(4))
        .widthIs(widthSub)
        .heightIs(Y_CONVERT(21));
        self.subTitleLb.text = model.detailTitle;
    }
    [self.closeBtn setImage:[UIImage imageNamed:@"ic_cancelHome"] forState:UIControlStateNormal];
    
    NSString* flagText = [model.custom length] > 0 ? model.custom : LocalizedString(@"LIVE_LIST_CELL_TYPE_ACTIVITY");

    self.flowLb.text = flagText;
    self.flowLb.sd_layout.widthIs(self.flowLb.intrinsicContentSize.width);
    self.flowImageView.sd_layout.widthIs(self.flowLb.intrinsicContentSize.width + 20)    ;
}

- (void)configUI
{
    [self buildIcon];
    [self buildTitle];
    
    // VLIVIOS-263: 不需要关闭按钮
    // 标签挪到下面去，底色变成蓝色
    //[self buildClickBtn];
    [self buildSubTitle];
    [self buildLine];
    [self buildCoverImg];
    [self buildFlowBackgroundImg];
    [self buildFlowLabel];
}
//头像icon
- (void)buildIcon
{
    [self addSubview:self.icon];
    self.icon.sd_layout
    .leftSpaceToView(self,X_CONVERT(12))
    .topSpaceToView(self,Y_CONVERT(10))
    .widthIs(50)
    .heightIs(50);
}
//标题
- (void)buildTitle
{
    [self addSubview:self.titleLb];
    self.titleLb.sd_layout
    .leftSpaceToView(self.icon,X_CONVERT(7))
//    .topSpaceToView(self,Y_CONVERT(14))
    .centerYEqualToView(self.icon)
    .widthIs(self.frame.size.width / 2)
    .heightIs(Y_CONVERT(20));
}
//推广背景框
- (void)buildFlowBackgroundImg
{
    [self addSubview:self.flowImageView];
    self.flowImageView.sd_layout
    .rightSpaceToView(self,X_CONVERT(10))
    .topEqualToView(_coverImg).offset(Y_CONVERT(10))
//    .centerYEqualToView(self.icon)
    .widthIs(X_CONVERT(36))
    .heightIs(Y_CONVERT(20));
}
//推广标签
- (void)buildFlowLabel
{
    [self addSubview:self.flowLb];
    self.flowLb.sd_layout
    //.rightSpaceToView(self,X_CONVERT(10))
    .topEqualToView(_coverImg).offset(Y_CONVERT(10))
//    .centerYEqualToView(self.icon)
    .widthIs(X_CONVERT(36))
    .centerXEqualToView(self.flowImageView)
    .heightIs(Y_CONVERT(20));
    
}
//关闭按钮
- (void)buildClickBtn
{
    self.closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.closeBtn];
    [self.closeBtn addTarget:self action:@selector(closeAd) forControlEvents:UIControlEventTouchUpInside];
    [self.closeBtn setImage:[UIImage imageNamed:@"ic_cancel"] forState:UIControlStateNormal];
    self.closeBtn.sd_layout
    .rightSpaceToView(self,X_CONVERT(12))
//    .topSpaceToView(self,Y_CONVERT(15))
    .centerYEqualToView(self.icon)
    .widthIs(X_CONVERT(20))
    .heightIs(X_CONVERT(20));
}
//关闭按钮点击事件代理
- (void)closeAd{
    
    if ([_delegate respondsToSelector:@selector(closeAdCellWithIndex:)]) {
        [_delegate closeAdCellWithIndex: self];
    }
}
//副标题
- (void)buildSubTitle
{
    [self addSubview:self.subTitleLb];
    self.subTitleLb.sd_layout
    .topSpaceToView(self.titleLb,Y_CONVERT(4))
    .leftSpaceToView(self.icon,X_CONVERT(7))
    .minWidthIs(1)
    .heightIs(Y_CONVERT(20));
}
-(void)buildLine
{
    UILabel *line = [[UILabel alloc]init];
    [self addSubview:line];
    line.sd_layout
    .leftSpaceToView(self,0)
    .bottomSpaceToView(self,0)
    .rightSpaceToView(self,0)
    .heightIs(Y_CONVERT(6));
    line.tag = 1001116;
    line.backgroundColor = RGB16(COLOR_FONT_F4F4F4);
}
-(void)buildCoverImg
{
    [self addSubview:self.coverImg];
    self.coverImg.sd_layout
    .leftSpaceToView(self,0)
    .rightSpaceToView(self,0)
    .topSpaceToView(self.icon,Y_CONVERT(10))
    //.heightIs([UIScreen mainScreen].bounds.size.width / 16 * 9 - 6);
    .heightIs(MIN(SCREEN_WIDTH, SCREEN_HEIGHT) / 16 * 9 - 6);
}
/*--------------------------------分割线（以下都是懒加载件 ）---------------------------------*/
-(UIImageView *)icon{
    if (!_icon) {
        _icon = [[UIImageView alloc] init];
    }
    return _icon;
}
-(UILabel *)titleLb{
    if (!_titleLb) {
        self.titleLb = [[UILabel alloc] init];
        self.titleLb.textColor = RGB16(COLOR_FONT_4A4A4A);
        self.titleLb.backgroundColor = [UIColor clearColor];
        self.titleLb.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
        self.titleLb.textAlignment = NSTextAlignmentLeft;
        self.titleLb.numberOfLines = 1;
        [self.titleLb setSingleLineAutoResizeWithMaxWidth:300];

    }
    return _titleLb;
}
-(UIImageView *)flowImageView{
    if (!_flowImageView) {
        self.flowImageView = [[UIImageView alloc]init];
        self.flowImageView.backgroundColor = RGB16(COLOR_BG_53ABFF);
        self.flowImageView.layer.masksToBounds = YES;
        self.flowImageView.layer.cornerRadius = Y_CONVERT(10);
        //self.flowImageView.image = [UIImage imageNamed:@"Rectangle43"];
    }
    return _flowImageView;
}
-(UILabel *)flowLb{
    if (!_flowLb) {
        self.flowLb = [[UILabel alloc]init];
        self.flowLb.textColor = [UIColor whiteColor];
        self.flowLb.textAlignment = NSTextAlignmentCenter;
        self.flowLb.font = [UIFont systemFontOfSize:12];
        self.flowLb.text = LocalizedString(@"LIVE_LIST_CELL_TYPE_ACTIVITY");
    }
    return _flowLb;
}
-(UILabel *)subTitleLb{
    if (!_subTitleLb) {
        self.subTitleLb = [[UILabel alloc]init];
        self.subTitleLb.textColor = RGB16(COLOR_FONT_9B9B9B);
        self.subTitleLb.font = [UIFont systemFontOfSize:14];
    }
    return _subTitleLb;
}
-(UIImageView *)coverImg{
    if (!_coverImg) {
        self.coverImg = [[UIImageView alloc]init];
        self.coverImg.image = [UIImage imageNamed:@"Ad_cover"];
    }
    return _coverImg;
}

//根据文字计算宽度
-(CGFloat)widthOfString:(NSString *)string font:(UIFont *)font height:(CGFloat)height
{
    NSDictionary * dict=[NSDictionary dictionaryWithObject: font forKey:NSFontAttributeName];
    CGRect rect=[string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return rect.size.width;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
