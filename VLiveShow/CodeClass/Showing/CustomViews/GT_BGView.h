//
//  GT_BGView.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GT_BGView : UIView

@property (nonnull, nonatomic, strong) UILabel *label;
@property (nonnull, nonatomic, strong) UIButton *onLineButton;
@property (nonnull, nonatomic, strong) UIButton *closeButton;

- (void)hideSelf;

@end
