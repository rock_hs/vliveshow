//
//  ApplyBGView.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GT_BGView.h"
@class VLSUserProfile;
@class ManagerEvent;
typedef void (^LineSucc)();
typedef void (^LineFail)();
typedef void (^CancleOnLine)();

typedef NS_ENUM(NSUInteger, ApplyStatus) {
    nonApply, // 未申请
    waitToConfirm, // 已申请，等待确认
    uponImmediately // 等待上镜
};;

@protocol ApplyBGViewDelegate <NSObject>

@optional
- (void)fromApplyViewGuestCancelLink;

- (void)fromApplyViewGuestCancelLinkRequest;

- (void)removeSelf;

- (void)errorToast:(ManagerEvent *)error;

- (void)applyBGViewBindPhone;

@end


@interface ApplyBGView :GT_BGView

@property (nonatomic, weak) id<ApplyBGViewDelegate> delegate;

@property (nonatomic, assign) ApplyStatus applyStatus;

@property (nonatomic, assign) BOOL isAcceptInvite;

- (instancetype)initWithFrame:(CGRect)frame selfurl:(VLSUserProfile *)selfurl hosturl:(VLSUserProfile *)hosturl;


- (void)startApply;
- (void)endSucc;
- (void)endFail;

- (void)comeBackGuest;

- (void)waitForconfirm;

@end
