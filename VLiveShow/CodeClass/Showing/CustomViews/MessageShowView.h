//
//  MessageShowView.h
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSTeamMessageModel.h"
@protocol CloseViewDelegate <NSObject>

-(void)closeCurrentView;
-(void)goToLiveRoom;

@end
/*调用方法
 MessageShowView *vie = [[MessageShowView alloc]initWithFrame:CGRectMake(350, 100, 1, 1) AndMessage:@"點擊可以與主播連線，成為嘉賓哦~" FourSecondDisapper:NO];
 [self.view addSubview:vie];
 
 UIImageView *pointed = [[UIImageView alloc]init];
 [self.view addSubview:pointed];
 pointed.sd_layout
 .leftSpaceToView(self.view,350)
 .topSpaceToView(vie,0)
 .heightIs(12)
 .widthIs(22);
 pointed.image = [UIImage imageNamed:@"ic_jiao"];

 */
@interface MessageShowView : UIView
//宽高务必给 286，55，否则图片会被拉伸

//消息标签
@property (nonatomic, strong) UILabel *messageLabel;
//背景图片
@property (nonatomic, strong) UIImageView *backgroundImg;
//头像图片
@property (nonatomic, strong) UIImageView *faceImg;
//指向尖图片
@property (nonatomic, strong) UIImageView *pointed;
//叉号
@property (nonatomic, strong) UIButton *clicked;
//透明button为了方便点击
@property (nonatomic ,strong) UIButton *clickImg;
//是否需要4秒后消失
@property (nonatomic) BOOL disappearOnFourSeconds;
//带头像的提示框按钮点击事件
@property (nonatomic,retain) id <CloseViewDelegate> delegate;
//不带头像初始化方法
-(instancetype)initWithFrame:(CGRect)frame AndMessage:(NSString *)message FourSecondDisapper:(BOOL)disapper;
//带头像初始化方法宽高给X_COVER(286)  Y_CONVER(55)
-(instancetype)initWithFrame:(CGRect)frame WithModel:(VLSTeamMessageModel *)model;
-(void)removeShowView;
//获取到当前宽度
+(CGFloat)widthWithMessage:(NSString *)message;
@end
