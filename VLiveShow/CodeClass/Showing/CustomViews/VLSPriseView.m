//
//  VLSPriseAnimation.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSPriseView.h"
@interface VLSPriseView ()
{
    NSInteger priseCount;
    NSMutableSet * recyclePool;
    NSMutableArray * array;
    CAShapeLayer * shapelayer;
    NSArray *imageArray;
    NSSet *timefuntions;
}

@end

@implementation VLSPriseView

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame: frame]) {
        array = @[].mutableCopy;
        recyclePool = [NSMutableSet set];
        
        imageArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"praise-01"],[UIImage imageNamed:@"praise-02"],[UIImage imageNamed:@"praise-03"],[UIImage imageNamed:@"praise-04"], nil];
        
        timefuntions = [NSSet setWithObjects:kCAMediaTimingFunctionLinear, kCAMediaTimingFunctionEaseIn,kCAMediaTimingFunctionEaseOut,kCAMediaTimingFunctionEaseInEaseOut,kCAMediaTimingFunctionDefault,nil];
    }
    return self;
}
- (void)showAnimation
{
    // 点击创建带有图片的layer,判断是否在recycle哩有layer
    CAShapeLayer *layer;
    if (recyclePool.count) {
        layer = [recyclePool anyObject];
        [recyclePool removeObject:layer];
    }else{
        layer = [self GreatImageLayer];
    }
    [self.layer addSublayer:layer];
    [self setanimation:layer];
    
    

}

//- (void)contentTouch
//{
//    if ([_delegagte respondsToSelector:@selector(priseViewTouch)]) {
//        [_delegagte priseViewTouch];
//    }
//}

//- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
//{
//
//}


#pragma mark - 点赞爱心
- (CAShapeLayer *)GreatImageLayer {
    
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.frame = CGRectMake(self.bounds.size.width/2, self.bounds.size.height/2,  24, 35);
    UIImage *image = imageArray[arc4random()%4];
    layer.contents = (__bridge id _Nullable)(image.CGImage);
    
    return layer;
}

- (void)setanimation:(CAShapeLayer *)layer {
    
    // 动画
    CAKeyframeAnimation *keyAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    keyAnimation.path = CFAutorelease([self CGPathMake]);
    
    // 透明动画
    CABasicAnimation *opAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opAnimation.toValue = @0;
    opAnimation.removedOnCompletion = NO;
    opAnimation.fillMode = kCAFillModeForwards;
    
    // 大小动画
    CABasicAnimation *scale = [CABasicAnimation animation];
    scale.keyPath = @"transform.scale";
    scale.toValue = @1;
    scale.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 0.1)];
    scale.duration = 0.5;
    
    // 组合动画
    //随机的组合时间
    CAAnimationGroup  *qipaoGroup = [CAAnimationGroup animation];
    qipaoGroup.duration = 4;
    qipaoGroup.repeatCount = 1;
    qipaoGroup.animations = @[scale,keyAnimation,opAnimation];
    qipaoGroup.timingFunction = [CAMediaTimingFunction functionWithName:[timefuntions anyObject]];
    qipaoGroup.delegate = self;
    qipaoGroup.removedOnCompletion = NO;
    qipaoGroup.fillMode = kCAFillModeForwards;
//    qipaoGroup.delegate = self;
//    [qipaoGroup setValue:@"qipao" forKey:@"animationType"];
    [layer addAnimation:qipaoGroup forKey:@"qipaoGroup"];
    
    [array addObject:layer];
}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    
    CAShapeLayer *layer = [array firstObject];
    [layer removeAllAnimations];
    [layer removeFromSuperlayer];
    [array removeObject:layer];
    [recyclePool addObject:layer];
    
}



- (int)randomint:(int)from to:(int)to
{
    
    int random = (from + (arc4random() % (to-from + 1)));
    return random;
}

- (CGMutablePathRef)CGPathMake {
    
    // 根据贝塞尔曲线画出路径
    //    UIBezierPath *path = [UIBezierPath bezierPath];
    
    //利用随机数
    int y = [self randomint:0 to:1];
    //获取第一个控制点
    int contolY;
    if (y == 0) {
        contolY = 1;
    }else{
        contolY = -1;
    }
    
    CGPoint StratPoint = CGPointMake(self.bounds.size.width - 64, self.bounds.size.height - 60);
    CGPoint EndPoint = CGPointMake(self.bounds.size.width - 64 + contolY*[self randomint:0 to:40], self.bounds.size.height - 60 - 260);
    
    CGPoint ControlPoint1 = CGPointMake(self.bounds.size.width - 64 - contolY*60, self.bounds.size.height - 60 - 35);
    CGPoint ControlPoint2 = CGPointMake(self.bounds.size.width - 64 + contolY*[self randomint:0 to:120], self.bounds.size.height - 60 - 145);
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    CGPathMoveToPoint(curvedPath, NULL, StratPoint.x, StratPoint.y);
    CGPathAddCurveToPoint(curvedPath, NULL, ControlPoint1.x, ControlPoint1.y, ControlPoint2.x, ControlPoint2.y, EndPoint.x, EndPoint.y);
    return curvedPath;
    
    
}
@end
