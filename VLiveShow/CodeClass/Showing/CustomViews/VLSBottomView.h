//
//  VLSBottomView.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/29.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
/*
typedef enum : NSUInteger {
    ActionTypeLive = 3, // 直播
    ActionTypeAnchor = 5, // 主播
    ActionTypeGuest = 6, // 嘉宾
    ActionTypeAdmin = 7, //管理员
    
    ActionTypeLessonTeacher,        //课程讲师
    ActionTypeLessonAttendance,     //课程预约连线嘉宾
    ActionTypeLessonAudience,       //课程预约观众
} ActionType;
*/
typedef NS_OPTIONS(NSInteger, ActionStatus) {
    AllShow = 0,
    GuestViewHidden = 1UL << 0,
    MuteHidden =  1UL << 1,
    CameraHidden = 1UL << 2,
    MailHidden = 1UL << 3,
    ConnectionHidden = 1UL << 4,
    GiftHidden = 1UL << 5,
    ShareHidden = 1UL << 6,
    RateHidden = 1UL << 7
};

@protocol VLSBottomViewDelegate<NSObject>

//消息按钮点击
- (void)bottomMessageClicked;

@optional
//切换镜头
- (void)bottomChangeCamera;
//声音点击
- (void)bottomVoiceClicked:(UIButton *)sender;
//分享点击
- (void)bottomShareClicked;

//礼物
- (void)bottomGiftClicked;

// 好友
- (void)bottomFriendClicked:(id)sender;

//站内信
- (void)bottomMailClicked;

// 评分
- (void)bottomRateClicked;

@end

@interface VLSBottomView : UIView

@property (nonatomic, assign) id<VLSBottomViewDelegate>delegate;

@property (nonatomic, assign)ActionType actionType;

- (instancetype)initWithFrame:(CGRect)frame actionStatus:(ActionStatus)actionStatus;
- (void)showfriendicon:(BOOL)show;
- (void)showVoiceIcon:(BOOL)show;

- (void)voiceEnable:(BOOL)enable;
//改变人头icon状态  红色／正常
- (void)changefriendStatus:(BOOL)isguest;

- (void)changeMailIcon:(BOOL)isMail;

- (void)isFirstRequestLink;

- (void)showisFirstWatch;

- (void)showisFirstShow;
- (void)checkMessageExistence;
//禁言用户
- (void)setForbidSendMsg:(BOOL)forbid;

- (void)enableRatingButton: (BOOL)enable;

@property (nonatomic, strong) UIImageView *imagviewMail;

@end
