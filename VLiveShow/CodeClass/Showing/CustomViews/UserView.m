//
//  UserView.m
//  animationTest
//
//  Created by sp on 16/5/18.
//  Copyright © 2016年 sp. All rights reserved.
//

#import "UserView.h"

@implementation UserView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.titleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        self.flowerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(200, 0, 50, 50)];
        self.countLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 100, 25)];
        self.giftNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 25, 100, 25)];
        self.layer.cornerRadius = 25;
        self.countLabel.font = [UIFont systemFontOfSize:12];
        self.giftNameLabel.font = [UIFont systemFontOfSize:12];
        self.countLabel.textColor = [UIColor whiteColor];
        self.giftNameLabel.textColor = [UIColor greenColor];
        [self addSubview:self.countLabel];
        [self addSubview:self.giftNameLabel];
        
        [self addSubview:self.titleImageView];
        [self addSubview:self.flowerImageView];
        self.flowerImageView.hidden = YES;
        self.backgroundColor = [UIColor grayColor];
        self.clipsToBounds = YES;
    
    }
    return self;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
