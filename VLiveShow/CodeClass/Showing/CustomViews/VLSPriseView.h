//
//  VLSPriseAnimation.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol VLSPriseViewDelegate<NSObject>
//@optional
//- (void)priseViewTouch;

@end

@interface VLSPriseView : UIView
@property (nonatomic, assign) BOOL isHost;
@property (nonatomic, assign) id <VLSPriseViewDelegate>delegagte;
- (void)showAnimation;

@end
