//
//  UserCollectionViewCell.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSUserProfile.h"


@interface UserCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIButton *GT_status;


-(void)setOnLineModel:(VLSUserProfile *)onLineModel;

@end
