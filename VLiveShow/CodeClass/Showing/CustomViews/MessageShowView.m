//
//  MessageShowView.m
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "MessageShowView.h"

@implementation MessageShowView

-(instancetype)initWithFrame:(CGRect)frame AndMessage:(NSString *)message FourSecondDisapper:(BOOL)disapper
{
    if (self = [super initWithFrame:frame]) {
        CGSize size = [self getWidthWithMessage:message];

        
        self.backgroundImg = [[UIImageView alloc]init];
        
        [self addSubview:self.backgroundImg];
        
        if (frame.origin.x + size.width + 28 >= SCREEN_WIDTH) {
            CGPoint poin = CGPointMake(SCREEN_WIDTH - size.width - 28, frame.origin.y);
            CGRect fram = CGRectMake(poin.x, poin.y, size.width + 28, Y_CONVERT(46));
            self.frame = fram;
        }
        
        self.backgroundImg.sd_layout
        .leftSpaceToView(self,0)
        .bottomSpaceToView(self,0)
        .heightIs(Y_CONVERT(34))
        .widthIs(size.width + 28);
        self.backgroundImg.image = [UIImage imageNamed:@"ic_kuang"];
        
        self.messageLabel = [[UILabel alloc]init];
        self.messageLabel.numberOfLines = 1;
        [self.backgroundImg addSubview:self.messageLabel];
        self.messageLabel.sd_layout
        .leftSpaceToView(self.backgroundImg,9)
        .heightIs(20)
        .widthIs(size.width)
        .topSpaceToView(self.backgroundImg,11);
        self.messageLabel.text = message;
        self.messageLabel.textColor = RGB16(COLOR_FONT_FFFFFF);
        self.messageLabel.font = [UIFont systemFontOfSize:15];
        self.disappearOnFourSeconds = disapper;
        [self hiddenOrNotOn4second];
        
    }
    return self;
}

+(CGFloat)widthWithMessage:(NSString *)message{
    CGRect contentRect = [message boundingRectWithSize:CGSizeMake(X_CONVERT(261), Y_CONVERT(46)) options:NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
    return contentRect.size.width + 28;
}

-(CGSize)getWidthWithMessage:(NSString *)message{
    
 CGRect contentRect = [message boundingRectWithSize:CGSizeMake(X_CONVERT(261), Y_CONVERT(46)) options:NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
    return contentRect.size;
}
-(void)hiddenOrNotOn4second
{
    if (self.disappearOnFourSeconds == YES) {
        [UIView animateWithDuration:4.0 animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }
}
-(void)removeShowView{
    [self.backgroundImg removeFromSuperview];
    self.backgroundImg = nil;
    [self.messageLabel removeFromSuperview];
    self.messageLabel = nil;
    [self removeFromSuperview];
}

-(instancetype)initWithFrame:(CGRect)frame WithModel:(VLSTeamMessageModel *)model{
    if (self = [super initWithFrame:frame]) {
        self.backgroundImg = [[UIImageView alloc]init];
        [self addSubview:self.backgroundImg];
        self.backgroundImg.sd_layout
        .leftSpaceToView(self,0)
        .bottomSpaceToView(self,0)
        .heightIs(Y_CONVERT(55))
        .widthIs(X_CONVERT(286));
        self.backgroundImg.image = [UIImage imageNamed:@"Rectangle87"];
        self.backgroundImg.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goToLiveRoom)];
        //设置同时触摸的手指个数
        tap.numberOfTouchesRequired = 1;
        //将手势识别器与视图建立联系
        [self.backgroundImg addGestureRecognizer:tap];
        
        self.faceImg = [[UIImageView alloc]init];
        [self addSubview:self.faceImg];
        self.faceImg.sd_layout
        .leftSpaceToView(self,X_CONVERT(6))
        .topSpaceToView(self,Y_CONVERT(5))
        .widthIs(X_CONVERT(36))
        .heightIs(Y_CONVERT(36));
        
        [self.faceImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/v1/users/%@/avatars.png",BASE_URL,model.anchorID]] placeholderImage:ICON_PLACEHOLDER];
        self.faceImg.layer.cornerRadius = 18;
        [self.faceImg.layer setMasksToBounds:YES];
        

        self.messageLabel = [[UILabel alloc]init];
        [self addSubview:self.messageLabel];
        self.messageLabel.sd_layout
        .leftSpaceToView(self.faceImg,X_CONVERT(8))
        .heightIs(Y_CONVERT(18))
        .widthIs(X_CONVERT(192))
        .centerYEqualToView(self.faceImg);
        self.messageLabel.text = [NSString stringWithFormat:@"%@的直播间邀请你成为嘉宾",model.anchorName];
        self.messageLabel.textColor = RGB16(COLOR_FONT_FFFFFF);
        self.messageLabel.font = [UIFont systemFontOfSize:14];
        
        
        self.clicked = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.clicked setBackgroundImage:[UIImage imageNamed:@"but_tos_close"] forState:UIControlStateNormal];
        [self.clicked setBackgroundImage:[UIImage imageNamed:@"but_ad._close"] forState:UIControlStateHighlighted];
        [self.backgroundImg addSubview:self.clicked];
        self.clicked.sd_layout
        .leftSpaceToView(self.messageLabel,X_CONVERT(19))
        .centerYEqualToView(self.messageLabel)
        .heightIs(Y_CONVERT(12))
        .widthIs(X_CONVERT(12));
        
        self.clickImg = [UIButton buttonWithType:UIButtonTypeCustom];
         [self.clickImg addTarget:self action:@selector(closeCurView) forControlEvents:UIControlEventTouchUpInside];
        [self.backgroundImg addSubview:self.clickImg];
        self.clickImg.sd_layout
        .leftSpaceToView(self.messageLabel,X_CONVERT(23))
        .centerYEqualToView(self.messageLabel)
        .heightIs(Y_CONVERT(20))
        .widthIs(X_CONVERT(20));
       

    }
    return self;

}
//进入直播间
-(void)goToLiveRoom{
    if (_delegate && [_delegate respondsToSelector:@selector(goToLiveRoom)]) {
        [_delegate goToLiveRoom];
    }
}
//关闭按钮点击事件代理
- (void)closeCurView{
    
    if (_delegate && [_delegate respondsToSelector:@selector(closeCurrentView)]) {
        [_delegate closeCurrentView];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
