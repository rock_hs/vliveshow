//
//  VLSMessageBoard.m
//  Demo3_Chat
//
//  Created by SXW on 16/5/16.
//  Copyright © 2016年 SXW. All rights reserved.
//

#import "VLSMessageBoard.h"
#import "VLSMessageCell.h"
#import "HTTPFacade.h"
#import "VLSHeartManager.h"
#define Random arc4random() % 20

#define MaxMessageNumber 100

@interface VLSMessageBoard ()<UITableViewDelegate,UITableViewDataSource,CTTextDisplayViewDelegate
>
{
    NSMutableDictionary *userColor;
}

@property (nonatomic, strong) UITableView *tableView;
@property (assign) CGFloat lastOffset;
@end

@implementation VLSMessageBoard

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _dataList =[NSMutableArray arrayWithCapacity:0];
        userColor = [[NSMutableDictionary alloc] initWithCapacity:0];
        [self addSubview:self.tableView];
//        //初始化渐变层
//        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
//        gradientLayer.frame= self.bounds;
//        //设置渐变颜色方向
//        gradientLayer.startPoint = CGPointMake(0, 0);
//        gradientLayer.endPoint = CGPointMake(0, 0.1);
//        
//        //设定颜色组
//        gradientLayer.colors = @[(__bridge id)[UIColor clearColor].CGColor,
//                                      (__bridge id)[UIColor blackColor].CGColor];
//        
//        //设定颜色分割点
//        gradientLayer.locations = @[@(0.01f) ,@(0.2f)];
//        self.layer.mask = gradientLayer;
    }
    
    return self;
}

#pragma mark 消息来源类型标注
- (void)addDataSource:(NSArray *)array
{
    
    /**
     *  当前所有消 息都在该数组中
     */
#ifdef DONOT_SHOW_MSG
    // 主播开播后如果遇到了大量的消息时，主播界面会卡住。
    // 这个只能作为一个临时的解决方式，给活动时候的主播定制的版本
    return;
#endif
    
#if 0
    [_dataList addObject:[array objectAtIndex:0]];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_dataList.count-1) inSection:0];
    
    [_tableView beginUpdates];
    
    [_tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    [_tableView endUpdates];
    
    self.tableView.scrollEnabled = NO;
    
    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:_dataList.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    self.tableView.scrollEnabled = YES;
    /**
     *  当到600条数据时删除前100条
     */
    if (_dataList.count >= 600) {
        
        NSArray *deleteArr = [_dataList subarrayWithRange:NSMakeRange(0, 100)];
        
        NSMutableArray *temp = [NSMutableArray arrayWithCapacity:0];
        
        for (int i=0; i<deleteArr.count; i++) {
            
            NSIndexPath *index =[NSIndexPath indexPathForRow:i inSection:0];
            
            [temp addObject:index];
            
        }
        //执行删除
        if (temp.count>0) {
            
            [_dataList removeObjectsInArray:deleteArr];
            
            [_tableView beginUpdates];
            
            [_tableView deleteRowsAtIndexPaths:temp withRowAnimation:UITableViewRowAnimationNone];
            
            [_tableView endUpdates];
            
        }
        
    }
#endif
    [self addDataSourceWithArray: array];
}

- (void)addDataSourceWithArray: (NSArray*)array
{
#ifdef DONOT_SHOW_MSG
    // 主播开播后如果遇到了大量的消息时，主播界面会卡住。
    // 这个只能作为一个临时的解决方式，给活动时候的主播定制的版本
    return;
#endif
    
    if (array.count == 0)
    {
        return;
    }
    if (array.count >= MaxMessageNumber)
    {
        [self.dataList removeAllObjects];
        NSInteger offset = array.count - MaxMessageNumber;
        array = [array subarrayWithRange: NSMakeRange(offset, MaxMessageNumber)];
    }
    else if (self.dataList.count + array.count > MaxMessageNumber)
    {
        NSInteger numberToRemove = self.dataList.count + array.count - MaxMessageNumber;
        [self.dataList removeObjectsInRange: NSMakeRange(0, numberToRemove)];
    }
    //NSInteger startIndex = self.dataList.count;
    [self.dataList addObjectsFromArray: array];
//    __block NSMutableArray* indexPathArray = [NSMutableArray array];
//    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
//    {
//        [indexPathArray addObject: [NSIndexPath indexPathForRow: idx + startIndex inSection: 0]];
//    }];
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath: [NSIndexPath indexPathForRow: self.dataList.count - 1 inSection: 0] atScrollPosition: UITableViewScrollPositionBottom animated: YES];
    
}

#pragma mark — UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  _dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VLSMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    cell.msg = _dataList[indexPath.row];
        
    cell.CTtextView.delegate = self;
    
    return cell;
}

#pragma mark - UITableViewDelegate
//计算行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VLSMessageViewModel *model = [_dataList objectAtIndex:indexPath.row];
    
    model.nameColor = [self getColor:model.user.userId];
    
    return model.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (UIColor *)getColor:(NSString *)userId
{
    NSArray *arr = @[ RGB16(COLOR_MSG_F8E71C),
                      RGB16(COLOR_MSG_A6D3FF),
                      RGB16(COLOR_MSG_F5A623),
                      RGB16(COLOR_MSG_7ED321),
                      RGB16(COLOR_MSG_50E3C2),
                      RGB16(COLOR_MSG_F4A7B1),
                      RGB16(COLOR_MSG_FF8B64),
                      RGB16(COLOR_MSG_AAE75C),
                      RGB16(COLOR_MSG_FF7995),
                      RGB16(COLOR_MSG_A1EAFF),
                      RGB16(COLOR_MSG_F4A5DF),
                      RGB16(COLOR_MSG_FF8BAD),
                      RGB16(COLOR_MSG_E4A2FF),
                      RGB16(COLOR_MSG_FFB000),
                      RGB16(COLOR_MSG_C3E419),
                      RGB16(COLOR_MSG_97F2F3),
                      RGB16(COLOR_MSG_DBED54),
                      RGB16(COLOR_MSG_C4E5A4),
                      RGB16(COLOR_MSG_F6F197),
                      RGB16(COLOR_MSG_47FEFF)];
    UIColor *color = nil;
    if (userId) {
        NSMutableArray *keys = [NSMutableArray arrayWithArray:userColor.allKeys];
        
        if ([keys containsObject:userId]) {
            color = [userColor objectForKey:userId];
        }else{
            
            color = arr[Random];
            [userColor setObject:color forKey:userId];
        }

    }
    return color;

}
/**
 *  滑动到最上面的cell alpha渐变
 *
 * 
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSArray *arr = [self.tableView visibleCells];
    UITableViewCell *cell = [arr firstObject];
    
    if (scrollView == self.tableView) {
        CGFloat y = scrollView.contentOffset.y;
        if (y > self.lastOffset) {
            cell.alpha = 1 - (self.tableView.contentOffset.y - cell.frame.origin.y) / (cell.frame.size.height - 20);

        }else{
            cell.alpha = 1;

        }
        self.lastOffset = y;
    }
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width+5, self.frame.size.height) style:UITableViewStylePlain];
        _tableView.userInteractionEnabled = YES;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorColor = [UIColor clearColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        //注册cell
        [_tableView registerClass:[VLSMessageCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
#pragma mark -CTTextDisplayViewDelegate
- (void)ct_textDisplayView:(CTTextDisplayView *)textDisplayView obj:(id)obj{
    
    if ([_delegate respondsToSelector:@selector(hideMsgStation)]) {
        [_delegate hideMsgStation];
    }
    NSString *str = [obj objectForKey:@"key"];
    if ([str isEqualToString:@"$"]) {
        UIView *cell;
        while (![cell isKindOfClass:[VLSMessageCell class]]) {
            cell = textDisplayView.superview;
        }
        NSIndexPath *indpath = [self.tableView indexPathForCell:(VLSMessageCell*)cell];
        
        VLSMessageViewModel *viewModel = [_dataList objectAtIndex:indpath.row];
        
        if ([_delegate respondsToSelector:@selector(messageBoardSelectUser:)]) {
            [_delegate messageBoardSelectUser:viewModel.user];
        }
        

    }else if ([str isEqualToString:@""])
        
    {
    
    }
    
    NSLog(@"key: %@  value: %@",obj[@"key"],obj[@"value"]);
}




@end
