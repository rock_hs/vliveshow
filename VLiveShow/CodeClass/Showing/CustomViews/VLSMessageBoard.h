//
//  VLSMessageBoard.h
//  Demo3_Chat
//
//  Created by SXW on 16/5/16.
//  Copyright © 2016年 SXW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLSMessageViewModel.h"

@protocol VLSMessageBoardDelegate <NSObject>

- (void)messageBoardSelectUser:(VLSUserProfile *)user;
- (void)hideMsgStation;

@end

@interface VLSMessageBoard : UIView
{

}
@property (nonatomic, strong) NSMutableArray *dataList;
@property (nonatomic, assign) id<VLSMessageBoardDelegate>delegate;
- (void)addDataSource:(NSArray *)array;

- (void)addDataSourceWithArray: (NSArray*)array;
@end
