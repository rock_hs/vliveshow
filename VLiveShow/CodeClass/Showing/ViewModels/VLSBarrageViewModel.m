//
//  VLSBarrageViewModel.m
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import "VLSBarrageViewModel.h"

@implementation VLSBarrageViewModel

- (instancetype)initWidthUser:(VLSUserProfile *)user message:(NSString *)message;
{
    if (self = [super init]) {
        self.user = user;
        self.userName = user.userName;
        self.userLevel = user.userLevel;
        self.message = message;
        
    }
    return self;
}

@end
