//
//  VLSLiveListModel.h
//  VLiveShow
//
//  Created by Cavan on 16/5/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseModel.h"
#import "AvatarsModel.h"
#import "VLSHotModel.h"
@interface VLSLiveListModel : VLSBaseModel
/// 主播头像
@property (nonatomic, copy)NSString *userCover;

@property (nonatomic, copy)NSString *roomCover;
/// 位置信息
@property (nonatomic, copy)NSString *location;
/// 真实的在线人数
@property (nonatomic, copy)NSString *onlineNum;
/// 观看过的人数
@property (nonatomic, copy)NSString *watchNum;
/// 计算出来的人数
@property (nonatomic, copy)NSString *cNum;
/// 新的人数
@property (nonatomic, copy)NSString *scNum;
/// 主播昵称
@property (nonatomic, copy)NSString *hostNickName;
/// 主播 ID
@property (nonatomic, strong)NSString *host;
// 直播间 ID
@property (nonatomic, strong)NSString *roomId;
/// 主播标题
@property (nonatomic, copy)NSString *title;
/// 直播状态
@property (nonatomic, copy)NSString *living;
//标签
@property (nonatomic, retain) NSArray *topics;
//置顶标志
@property (nonatomic, copy) NSNumber *priority;
@property (nonatomic, copy)NSString *updateTime;
@property (nonatomic, copy)NSNumber *createTime;
@property (nonatomic, copy) NSString *courseId;
@property (nonatomic, copy) NSString *lessonId;
//1竖2横 nil竖
@property (nonatomic, strong) NSNumber *orientation;

- (instancetype)initWidthHotModel:(VLSHotModel*)hotModel;

@end
