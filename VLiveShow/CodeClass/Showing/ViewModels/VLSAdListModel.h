//
//  VLSAdListModel.h
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSAdListModel : NSObject

//头像
@property (nonatomic, copy) NSString *avatarUrl;
//正标题
@property (nonatomic, copy) NSString *title;
//副标题
@property (nonatomic, copy) NSString *detailTitle;
//广告封面
@property (nonatomic, copy) NSString *coverUrl;
//广告链接
@property (nonatomic, copy) NSString *outerUrl;
//广告位置
@property (nonatomic, copy) NSString *position;
//广告ID
@property (nonatomic, copy) NSString *adID;
//是否是广告
@property (nonatomic      ) BOOL      isAd;

@property(nonatomic, copy) NSString* custom;

-(instancetype)initWithDic:(NSDictionary *)dic;

@end
