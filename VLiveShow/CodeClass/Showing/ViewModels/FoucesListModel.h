//
//  FoucesListModel.h
//  VLiveShow
//
//  Created by sp on 16/6/4.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoucesListModel : NSObject
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *username;
@property (nonatomic,copy)NSString *gender;
@property (nonatomic,copy)NSString *avatars;
@property (nonatomic,copy)NSString *intro;

@end
