//
//  VLSLiveInfoViewModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSLiveInfoModel.h"
#import "VLSVideoLayoutModel.h"

@interface VLSLiveInfoViewModel : NSObject

@property (nonatomic, copy)NSString *coverUrl;
/// 在线观看人数
@property (nonatomic, copy)NSString *onlineNum;
//观看人数
@property (nonatomic, copy)NSString *watchNum;

@property (nonatomic, copy)NSString *scNum;
/// 主播昵称VLSLiveInfoViewModel
@property (nonatomic, copy)NSString *hostNickName;
/// 主播 ID
@property (nonatomic, copy)NSString *host;
// 直播间 ID
@property (nonatomic, copy)NSString *roomId;
/// 主播标题
@property (nonatomic, copy)NSString *title;

@property (nonatomic, copy)NSString *totalGainTicket;

@property (nonatomic, strong) NSNumber *orientation;

/// 直播状态
@property (nonatomic, assign)BOOL living;
//布局
@property (nonatomic, strong)VLSVideoLayoutModel *layout;

@property (nonatomic, strong)NSArray *topics;
//直播间标签
@property (nonatomic, copy)NSString *roomMark;

@property (nonatomic, assign) BOOL rated;

- (instancetype) initWidthLiveInfoModel:(VLSLiveInfoModel *)model;
@end
