//
//  VLSLiveListModel.m
//  VLiveShow
//
//  Created by Cavan on 16/5/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveListModel.h"

@implementation VLSLiveListModel

- (instancetype)initWidthHotModel:(VLSHotModel*)hotModel
{
    if (self = [super init] ) {
        self.living = hotModel.living;
        self.host = [NSString stringWithFormat:@"%ld",(long)hotModel.host.integerValue];
        self.roomId = hotModel.roomId;
        self.courseId = hotModel.courseId;
        self.lessonId = hotModel.lessonId;
        self.title = hotModel.title;
        self.location = [NSString stringWithFormat:@"%@",hotModel.location];
        self.userCover = [NSString stringWithFormat:@"%@/v1/users/%@/avatars.png",BASE_URL,self.host];
        self.roomCover = hotModel.coverUrl;
        self.onlineNum = hotModel.onlineNum;
        self.watchNum = hotModel.watchNum;
        self.cNum = hotModel.cNum;
        self.scNum = hotModel.scNum;
        self.hostNickName = hotModel.hostNickName;
        self.updateTime = hotModel.updateTime;
        self.createTime = hotModel.createTime;
        self.topics = hotModel.topics;
        self.priority = hotModel.priority;
        self.orientation = hotModel.orientation;
    }
    return self;
}


@end
