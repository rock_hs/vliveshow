//
//  OnLineViewModel.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/12.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSUserProfile.h"

@interface OnLineViewModel : NSObject

@property (nonatomic, copy) NSString *indentifier;

@property (nonatomic, assign) BOOL selected;

- (void)userProfileToViewModel:(VLSUserProfile *)userprofile;


@end
