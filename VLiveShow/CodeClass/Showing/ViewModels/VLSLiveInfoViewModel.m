//
//  VLSLiveInfoViewModel.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveInfoViewModel.h"

@implementation VLSLiveInfoViewModel

- (instancetype) initWidthLiveInfoModel:(VLSLiveInfoModel *)model
{
    if (self = [super init]) {
        self.watchNum = model.watchNum;
        self.scNum = model.scNum;
        self.onlineNum = model.onlineNum;
        self.host = model.host;
        self.hostNickName = model.hostNickName;
        self.roomId = model.roomId;
        self.living = [model.living isEqualToString:@"1"]?YES:NO;
        self.coverUrl = model.coverUrl;
        self.layout = model.layout;
        self.totalGainTicket = model.totalGainTicket;
        self.title = model.title;
        self.topics = model.topics;
        self.orientation = model.orientation;
        self.rated = model.rated;
    }
    return self;
}


@end
