//
//  VLSAdListModel.m
//  VLiveShow
//
//  Created by 郭佳良 on 16/8/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAdListModel.h"

@implementation VLSAdListModel


-(instancetype)initWithDic:(NSDictionary *)dic{
    
    self = [super init];
    
    if (self) {
        
        [self setValuesForKeysWithDictionary:dic];
        
    }
    
    return self;
    
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"]) {
        self.adID = value;
    }
}

- (void)setNilValueForKey:(NSString *)key {
    
}


@end
