//
//  VLSVideoViewModel.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSVideoViewModel.h"

@implementation VLSVideoViewModel

- (instancetype)initViewModelWidthVideoAnchorModel:(VLSVideoAnchorModel *)anchorModel
{
    if (self = [super init]) {
        self.userId = anchorModel.userId;
        if ([anchorModel.userType isEqualToString:USER_TYPE_GUEST]) {
            self.userType = UserTypeGuest;
        }else if([anchorModel.userType isEqualToString:USER_TYPE_MASTER]) {
            self.userType = UserTypeHost;
        }else if([anchorModel.userType isEqualToString:USER_TYPE_SLAVE]) {
            self.userType = UserTypeSlave;
        }
        if ([anchorModel.video isEqualToString:VIDEO_OPENED]) {
            self.muteVideo = NO;
        }else if([anchorModel.video isEqualToString:VIDEO_CLOSED]){
            self.muteVideo = YES;
        }
        if ([anchorModel.voice isEqualToString:VOICE_MUTED]) {
            self.muteVoice = YES;
        }else if([anchorModel.voice isEqualToString:VOICE_OPENED]){
            self.muteVoice = NO;
        }else if([anchorModel.voice isEqualToString:VOICE_MUTEDS]){
            self.muteVoiceBySelf = YES;
            self.muteVoice = YES;
        }
        self.frame = CGRectZero;
    }
    return self;
}


@end
