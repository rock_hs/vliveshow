//
//  VLSMessageViewModel.m
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import "VLSMessageViewModel.h"
#import "CTTextDisplayView.h"

@implementation VLSMessageViewModel

- (instancetype)initWidthUser:(VLSUserProfile *)user message:(NSString *)message types:(NSInteger)types;
{

    if (self = [super init]) {
        self.user = user;
        self.userName = user.userName;
        self.userLevel = user.userLevel;
        self.types = types;
        /**
         *  对应消息的格式
         */
    NSString *language = [[NSLocale preferredLanguages] firstObject];
        switch (self.types) {
             //普通消息
            case 10011:
                self.message = [NSString stringWithFormat:@"${%@:} %@",self.userName,message];
                self.tempStr = [NSString stringWithFormat:@"%@: %@",self.userName,message];
                break;
            //进入
            case 10012:
                
                self.message = [NSString stringWithFormat:@"${%@} %@",self.userName,message];
                self.tempStr = [NSString stringWithFormat:@"%@ %@",self.userName,message];
                break;
             //绿色消息
            case 10016:
                
                self.message = [NSString stringWithFormat:@"%@",message];
                self.tempStr = self.message;
                break;
            //超级管理员进入房间
            case 10013:
//                if([language hasPrefix:@"en"]){
//                    self.message = [NSString stringWithFormat:@"[超管] Super Administrator is coming"];
//                }else{
//                    self.message = [NSString stringWithFormat:@"[超管] %@",LocalizedString(@"SUPER_COMEIN")];
//                }
                self.message = [NSString stringWithFormat:@"[超管] %@",LocalizedString(@"SUPER_COMEIN")];
                self.tempStr = self.message;
                break;
            //敏感词
            case 10018:
//                if([language hasPrefix:@"en"]){
//                    self.message = [NSString stringWithFormat:@"VLiveShow：Statement contains sensitive words."];
//                }else{
//                    self.message = [NSString stringWithFormat:@"%@",LocalizedString(@"CONTAIN_SENSITIVEWORD")];
//                }
                self.message = [NSString stringWithFormat:@"%@",LocalizedString(@"CONTAIN_SENSITIVEWORD")];
                self.tempStr = self.message;
                break;
            case 10019: { //点赞
                self.message = [NSString stringWithFormat:@"${%@} %@",self.userName,LocalizedString(@"LIVE_ANCHOR_SEND_PRAISE")];
                self.tempStr = [NSString stringWithFormat:@"%@ %@",self.userName,LocalizedString(@"LIVE_ANCHOR_SEND_PRAISE")];
            }
                break;
            case TYPE_FORBIDSENDMSG: {
                self.message = [NSString stringWithFormat:@"%@%@%@",LocalizedString(@"INVITE_ROOMID"), user.userName, LocalizedString(@"beendumbd")];
                self.tempStr = [NSString stringWithFormat:@"%@ %@",self.userName,LocalizedString(@"LIVE_ANCHOR_SEND_PRAISE")];
            }
                break;
            default:
                
                break;
                
        }
        for (int i = 0; i<[self.message length]; i++) {
            //截取字符串中的每一个字符
            NSString *s = [self.message substringWithRange:NSMakeRange(i, 1)];
            if ([s isEqualToString:@":"]) {
                NSRange range = NSMakeRange(i, 1);
                self.message =   [self.message stringByReplacingCharactersInRange:range withString:[NSString stringWithFormat:@"："]];
            }
        }

        self.height = [self getHeight];
        self.width = [self getWidth];
    }
    return self;
}

- (CGFloat)getHeight
{
    CTTextStyleModel * styleModel2 = [CTTextStyleModel new];
    styleModel2.font = [UIFont systemFontOfSize:14];
    styleModel2.faceSize = CGSizeMake(26,14);
    styleModel2.tagImgSize = CGSizeMake(2, 2);
    styleModel2.faceOffset = 3.0f;
    styleModel2.lineSpace = 3.0f;
    styleModel2.numberOfLines = -1;
    styleModel2.highlightBackgroundRadius = 10;
    styleModel2.highlightBackgroundAdjustHeight = 2;
    styleModel2.highlightBackgroundOffset = 3;
    styleModel2.autoHeight = NO;
    styleModel2.urlUnderLine = YES;
    styleModel2.emailUnderLine = YES;
    styleModel2.phoneUnderLine = YES;
    CGRect contentRect = [self.tempStr boundingRectWithSize:CGSizeMake(SCREEN_WIDTH / 5 * 3 - 10, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
    CGFloat rowHeight = [CTTextDisplayView getRowHeightWithText:self.tempStr rectSize:CGSizeMake(contentRect.size.width + X_CONVERT(15), CGFLOAT_MAX) styleModel:styleModel2];
    return rowHeight + 5;
}
-(CGFloat)getWidth
{
    CGRect contentRect = [self.tempStr boundingRectWithSize:CGSizeMake(SCREEN_WIDTH / 5 * 3 - 10, self.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
    if (self.types == 10013) {
        return contentRect.size.width - 10;
    }else{
        return contentRect.size.width + X_CONVERT(14);// + X_CONVERT(25);
}
}
@end
