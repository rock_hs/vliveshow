//
//  VLSCustomGiftViewModel.h
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VlSGiftModel.h"
#import "VLSUserProfile.h"

@interface VLSCustomGiftViewModel : NSObject
@property (nonatomic, strong) VlSGiftModel *giftModel;
@property (nonatomic, assign) GiftShowType giftShowType;
@property (nonatomic, strong) VLSUserProfile *userModel;

@property (nonatomic, strong) NSString *userImage;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *giftName;
@property (nonatomic, assign) NSInteger giftCount;
@property (nonatomic, assign) NSInteger userLevel;

@end
