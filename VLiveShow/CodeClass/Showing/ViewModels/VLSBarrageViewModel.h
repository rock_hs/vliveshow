//
//  VLSBarrageViewModel.h
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSUserProfile.h"

@interface VLSBarrageViewModel : NSObject

@property (nonatomic, strong) VLSUserProfile *user;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userImage;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) NSInteger userLevel;
- (instancetype)initWidthUser:(VLSUserProfile *)user message:(NSString *)message;

@end
