//
//  VLSNormalGiftViewModel.m
//  XMPPDEMO
//
//  Created by 李雷凯 on 16/5/19.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import "VLSNormalGiftViewModel.h"
#import "VlSGift.h"

@implementation VLSNormalGiftViewModel
- (instancetype)initWidthUser:(VLSUserProfile *)user gift:(VlSGiftModel *)gift ;
{
    if (self = [super init]) {
         self.user = user;
        self.giftModel = gift;
        self.userName = user.userName;
        self.userImage = user.userFaceURL;
        self.userLevel = user.userLevel;
        self.giftImageName = gift.giftImageName;
        self.message = [NSString stringWithFormat:@"送一个%@",gift.giftName];
    }
    return self;
}
@end
