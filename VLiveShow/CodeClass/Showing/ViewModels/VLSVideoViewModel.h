//
//  VLSVideoViewModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSVideoAnchorModel.h"

typedef enum {
    UserTypeHost     = 0,   //主播
    UserTypeSlave    = 1,   //副主播
    UserTypeGuest    = 2,   //嘉宾
}UserType;

@interface VLSVideoViewModel : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, assign) UserType userType;
@property (nonatomic, assign) BOOL muteVideo;
@property (nonatomic, assign) BOOL muteVoice;
@property (nonatomic, assign) BOOL showLoading;
@property (nonatomic, assign) BOOL muteVoiceBySelf;
@property (nonatomic, assign) BOOL isMainScreen;

@property (nonatomic, assign) CGRect frame;

- (instancetype)initViewModelWidthVideoAnchorModel:(VLSVideoAnchorModel *)anchorModel;

@end
