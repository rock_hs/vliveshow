//
//  VLSReConnectionManager.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSReConnectionManager : NSObject
+ (instancetype)sharedManager;
- (void)saveRoomID:(NSString *)roomid andAnchorID:(NSString *)anchorId;
- (void)deleteRoomIDandAnchorID;
- (NSString *)getRoomID;
- (NSString *)getAnchorID;
@end
