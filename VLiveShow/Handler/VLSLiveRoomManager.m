//
//  VLSLiveRoomManager.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveRoomManager.h"

@implementation VLSLiveRoomManager

+ (instancetype)sharedManager
{
    static VLSLiveRoomManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc ] init];
        
    });
    return _sharedManager;
}

@end
