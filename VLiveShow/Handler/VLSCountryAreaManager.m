//
//  VLSCountryAreaManager.m
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSCountryAreaManager.h"

@implementation VLSCountryAreaManager

+ (VLSCountryAreaManager *)shareManager {
    static VLSCountryAreaManager *countryArea = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        countryArea = [[VLSCountryAreaManager alloc] init];
        countryArea.name = LocalizedString(@"CHINA");
        countryArea.area = COUNTRY_AREA_CHINA;
    });
    return countryArea;
}



@end
