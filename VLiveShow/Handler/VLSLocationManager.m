//
//  VLSLocationManager.m
//  VLiveShow
//
//  Created by Cavan on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLocationManager.h"
#import "SNLocationManager.h"
#import "VLSUserTrackingManager.h"
@interface VLSLocationManager ()

@property (nonatomic, strong)SNLocationManager *locationManager;


@end

@implementation VLSLocationManager

+ (VLSLocationManager *)shareManager {
    static VLSLocationManager *locaManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        locaManager = [[VLSLocationManager alloc] init];
        
    });
    return locaManager;
}

-(void)loadLocation {
    self.locationManager = [[SNLocationManager alloc] init];
    [self.locationManager startUpdatingLocationWithSuccess:^(CLLocation *location, CLPlacemark *placemark) {
        self.locaAuthi = YES;
        self.locaStr = [NSString stringWithFormat:@"%@", placemark.locality];
        self.latitude = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
        self.longitude = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
//        self.geoLocation = [NSString stringWithFormat:@"%@,%@,%@", self.latitude, self.longitude, self.locaStr];
        NSLog(@"222222222%@, %@, %@", self.locaStr, self.latitude, self.longitude);

        [[VLSUserTrackingManager shareManager]  tackingLocation:[NSString stringWithFormat:@"%@", placemark.locality] longitude:[NSString stringWithFormat:@"%f", location.coordinate.longitude] latitude:[NSString stringWithFormat:@"%f", location.coordinate.latitude]];
        
    } andFailure:^(CLRegion *region, NSError *error) {
        self.locaStr = @"(null)";
        self.locaAuthi = NO;
    }];
}

@end
