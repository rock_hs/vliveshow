//
//  AccountManager.h
//  Stacks
//
//  Created by 李雷凯 on 15/10/30.
//  Copyright © 2015年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ManagerCallBack.h"
#import "HTTPFacade.h"
#import "VLSSelfInfoModel.h"
#import "VLSTenSigModel.h"
#import "AccountModel.h"
#import "ManagerEvent.h"
#import "DeviceModel.h"
@interface AccountManager : NSObject
@property(nonatomic,strong)AccountModel* account;

+(AccountManager*) sharedAccountManager;

// 验证码登录
+ (void)verCodeLogin:(AccountModel*)account callback:(ManagerCallBack*)callback;
// 密码登录
+(void)passwordLogin:(AccountModel*)account callback:(ManagerCallBack*)callback;
// 验证当前用户密码
+(void)verifyCurrentPwd:(AccountModel*)account callback:(ManagerCallBack*)callback;
+(void)verifyVerCode:(AccountModel*)account callback:(ManagerCallBack*)callback;
// 重新设置密码后登录
+(void)setNewPwdAndLogin:(AccountModel*)account callback:(ManagerCallBack*)callback;
// 第三方登录
+(void)thirdPlatformLogin:(AccountModel*)account callback:(ManagerCallBack*)callback;
//注册
+(void)phoneRegister:(AccountModel*)account callback:(ManagerCallBack*)callback;
+ (void)signUp:(AccountModel*)account callback:(ManagerCallBack*)callback;
+ (void)phoneExists:(AccountModel *)account callback:(ManagerCallBack *)callback;
//验证码
+ (void)sendVerCode:(AccountModel *)account callback:(ManagerCallBack *)callback;
//退出
+ (void)logOutCallback:(ManagerCallBack*)callback;
//上传个人信息
+ (void)postUserProfile:(AccountModel *)account callback:(ManagerCallBack *)callback;
//获取自己的信息
+ (void)getUserProfileCallback:(ManagerCallBack *)callback;
//腾讯签名
+ (void)getTencentSigCallback:(ManagerCallBack *)callback;
/** 获取心跳信息配置 */
+ (void)getHeartConfigCallback:(ManagerCallBack *)callback;
/** 获取主播标签列表 */
+ (void)getHostMarkList:(ManagerCallBack *)callback;
+ (void)setNewPsd:(AccountModel*)account callback:(ManagerCallBack *)callback;
+ (void)forgotPsd:(AccountModel*)account callback:(ManagerCallBack *)callback;
+ (void)getMyInfoCallback:(ManagerCallBack *)callback;

//手机号绑定
+ (void)phoneBind:(AccountModel*)account callback:(ManagerCallBack*)callback;

+(DeviceModel*)getDeviceInformation;
+ (BOOL)isPost;


-(void)loadAccount;
-(void)saveAccount;
- (void)signOut;

@end
