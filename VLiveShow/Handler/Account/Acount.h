//
//  Acount.h
//  VLiveShow
//
//  Created by SuperGT on 16/5/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Acount : NSObject

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *usersig;
@property (nonatomic, copy) NSString *token;
// token 时间戳
@property (nonatomic, copy) NSNumber *tokenTime;

// sig时间戳
@property (nonatomic, copy) NSNumber *SigTime;

@end
