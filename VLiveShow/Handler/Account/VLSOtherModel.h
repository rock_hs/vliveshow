//
//  VLSOtherModel.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/31.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSOtherModel : NSObject

@property (nonatomic, copy) NSString *viewprofile;//查看个人配置
@property (nonatomic, copy) NSString *settingchange;//系统配置开关
@property (nonatomic, copy) NSString *searchpeople;//搜索相关的人

@end
