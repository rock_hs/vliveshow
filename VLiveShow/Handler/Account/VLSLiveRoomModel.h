//
//  VLSLiveRoomModel.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/31.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSLiveRoomModel : NSObject

@property(nonatomic, copy) NSString *joinroom;//进入房间
@property(nonatomic, copy) NSString *quitroom;//离开房间
@property(nonatomic, copy) NSString *linkrequest;//申请连线
@property(nonatomic, copy) NSString *bindingphone;//绑定手机号
@property(nonatomic, copy) NSString *acceptlink;//主播同意
@property(nonatomic, copy) NSString *mute;//静音
@property(nonatomic, copy) NSString *unmute;//取消静音
@property(nonatomic, copy) NSString *uponImmediately;//立即上镜
@property(nonatomic, copy) NSString *changeactor;//切换屏幕
@property(nonatomic, copy) NSString *cancellink;//取消连线
@property(nonatomic, copy) NSString *invite;//主播邀请嘉宾
@property (nonatomic, copy) NSString *shareBefore;//直播前分享
@property (nonatomic, copy) NSString *shareOnLive;//直播中分享
@property (nonatomic, copy) NSString *shareAfter;//直播后分享
@property(nonatomic, copy) NSString *gift;//用户送礼物

@end
