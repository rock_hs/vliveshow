//
//  VLSJoinRoomModel.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/31.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSJoinRoomModel : NSObject

@property (nonatomic, strong) NSString *roomID;
@property (nonatomic, strong) NSString *joinType;

@end
