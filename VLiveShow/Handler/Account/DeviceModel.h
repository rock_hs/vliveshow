//
//  DeviceModel.h
//  VLiveShow
//
//  Created by sp on 16/7/5.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"
@interface DeviceModel : BaseModel
@property (nonatomic,copy)NSString * owner;//所有者名称
@property (nonatomic,copy)NSString * model;//设备的类型
@property (nonatomic,copy)NSString * localizedModel;//本地化版本
@property (nonatomic,copy)NSString * systemName;//获取当前运行的系统
@property (nonatomic,copy)NSString * systemVersion;//获取当前系统的版本
@property (nonatomic,copy)NSString * uuid;//唯一标示符
@property (nonatomic,copy)NSString * ScreenWidth;//屏宽
@property (nonatomic,copy)NSString * ScreenHeight;//屏高
@property (nonatomic,copy)NSString * carrierName;//运营商名称
@property (nonatomic,copy)NSString * radioAccessTechnology;//当前网络类型
@property (nonatomic,copy)NSString * idfa;//广告标示符
@property (nonatomic,copy)NSString * mac;//mac地址
@property (nonatomic,copy)NSString *token;

@property (nonatomic, copy)NSString *locaStr;//位置
@property (nonatomic, copy)NSString *latitude;//纬度
@property (nonatomic, copy)NSString *longitude;//经度
@property (nonatomic, copy)NSString *GEOLocation;//经纬度

@end
