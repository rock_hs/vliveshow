//
//  PostFileModel.h
//  Stacks
//
//  Created by 李雷凯 on 15/10/30.
//  Copyright © 2015年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostFileModel : NSObject

@property (nonatomic, strong)   NSString  *fileName;    //文件名字
@property (nonatomic, strong)   NSArray   *fileArray;   // image 的nsdata数据


@end
