//
//  AccountManager.m
//  Stacks
//
//  Created by 李雷凯 on 15/10/30.
//  Copyright © 2015年 LK. All rights reserved.
//

#import "AccountManager.h"
#import "ManagerUtil.h"
#import "SSKeychain.h"
#import <AdSupport/ASIdentifierManager.h>
#import "VLSHeartManager.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#import "VLiveShow-Swift.h"
#import "VLSUserTrackingManager.h"
#import "VLSLocationManager.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


#define ACCOUNT_UID         @"uid"
#define ACCOUNT_TOKEN       @"token"
#define ACCOUNT_HASBASE     @"hasBaseProfile"
#define ACCOUNT_LEVEL       @"level"
#define ACCOUNT_UNAME       @"username"
#define ACCOUNT_AVATARS     @"avatars"
#define ACCOUNT_MPHONE      @"mobilePhone"
#define ACCOUNT_DECODEMPHONE @"decodeMobilePhone"
#define ACCOUNT_NCNAME      @"nickName"
#define ACCOUNT_GENDER      @"gender"
#define ACCOUNT_TENDERSIG   @"tenderSig"
#define ACCOUNT_SUPERUSER   @"superUser"
#define ACCOUNT_ISHOST      @"isHOST"
#define ACCOUNT_TRIALHOST   @"trialHost"

#define ACCOUNT_AREA        @"area"
#define ACCOUNT_INTRO       @"intro"


@implementation AccountManager
+ (AccountManager *)sharedAccountManager {
    
    static AccountManager *sharedAccountManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAccountManager = [[self alloc] init];
    });
    return sharedAccountManager;
}
/** 手机号注册 */
+(void)phoneRegister:(AccountModel*)account callback:(ManagerCallBack*)callback
{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        NSDictionary *sigDict = [dict objectForKey:@"tencentSignature"];
        NSDictionary *infoDict = [dict objectForKey:@"userInfo"];
        NSString *token = [dict objectForKey:@"token"];
        VLSTenSigModel *sigModel = [VLSTenSigModel mj_objectWithKeyValues:sigDict];
        VLSSelfInfoModel *infoModel = [VLSSelfInfoModel mj_objectWithKeyValues:infoDict];
        AccountModel* account = [[AccountModel alloc] initAccountWidthSigModel:sigModel infoModel:infoModel token:token];
        
        ManagerEvent* event;
        if (account) {
            event = [ManagerEvent toast:nil title:LocalizedString(@"LOGIN_SUCCESS")];
            [AccountManager sharedAccountManager].account = account;
            [[AccountManager sharedAccountManager] saveAccount];
            [[VLSIAPManager sharedInstance] recoverUnfinishedReceipts];
            [[Crashlytics sharedInstance] setUserIdentifier: [NSString stringWithFormat: @"%ld", (long)account.uid]];
            [[VLSUserTrackingManager shareManager] trackingSignup:account];
        }
        return event;
        
    }];
    
    //[[VLSUserTrackingManager shareManager] trackingSignup:account];
    [HTTPFacade phoneRegister:account callback:http];
}

/** 验证码登录 */
+(void)verCodeLogin:(AccountModel*)account callback:(ManagerCallBack*)callback
{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        [[VLSUserTrackingManager shareManager] trackingLogin:account withLoginType:@"vcode"];
        
        NSDictionary *sigDict = [dict objectForKey:@"tencentSignature"];
        NSDictionary *infoDict = [dict objectForKey:@"userInfo"];
        NSString *token = [dict objectForKey:@"token"];
        VLSTenSigModel *sigModel = [VLSTenSigModel mj_objectWithKeyValues:sigDict];
        VLSSelfInfoModel *infoModel = [VLSSelfInfoModel mj_objectWithKeyValues:infoDict];
        AccountModel* account = [[AccountModel alloc] initAccountWidthSigModel:sigModel infoModel:infoModel token:token];
        
        ManagerEvent* event;
        if (account) {
            event = [ManagerEvent toast:nil title:LocalizedString(@"LOGIN_SUCCESS")];
            [AccountManager sharedAccountManager].account = account;
            [[AccountManager sharedAccountManager] saveAccount];
            [[VLSIAPManager sharedInstance] recoverUnfinishedReceipts];
            
            [[Crashlytics sharedInstance] setUserIdentifier: [NSString stringWithFormat: @"%ld", (long)account.uid]];
        }
        
        return event;
    }];
    
    [HTTPFacade verCodeLogin:account callback:http];
}

/** 密码登录 */
+(void)passwordLogin:(AccountModel*)account callback:(ManagerCallBack*)callback
{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        [[VLSUserTrackingManager shareManager] trackingLogin:account withLoginType:@"password"];
        
        NSDictionary *sigDict = [dict objectForKey:@"tencentSignature"];
        NSDictionary *infoDict = [dict objectForKey:@"userInfo"];
        NSString *token = [dict objectForKey:@"token"];
        VLSTenSigModel *sigModel = [VLSTenSigModel mj_objectWithKeyValues:sigDict];
        VLSSelfInfoModel *infoModel = [VLSSelfInfoModel mj_objectWithKeyValues:infoDict];
        AccountModel* account = [[AccountModel alloc] initAccountWidthSigModel:sigModel infoModel:infoModel token:token];
        
        ManagerEvent* event;
        if (account) {
            event = [ManagerEvent toast:nil title:LocalizedString(@"LOGIN_SUCCESS")];
            [AccountManager sharedAccountManager].account = account;
            [[AccountManager sharedAccountManager] saveAccount];
            [[VLSIAPManager sharedInstance] recoverUnfinishedReceipts];
            [[Crashlytics sharedInstance] setUserIdentifier: [NSString stringWithFormat: @"%ld", (long)account.uid]];
        }
        return event;
    }];
    
    [HTTPFacade passwordLogin:account callback:http];
}



/** 重新设置密码后登录 */
+(void)setNewPwdAndLogin:(AccountModel*)account callback:(ManagerCallBack*)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        AccountModel* result = [AccountModel mj_objectWithKeyValues:dict];
        ManagerEvent* event;
        if (result) {
            event = [ManagerEvent toast:nil title:LocalizedString(@"LOGIN_SUCCESS")];
        }
        return event;
    }];
    
    [HTTPFacade setNewPwdAndLogin:account callback:http];
}

/** 验证当前用户密码 */
+(void)verifyCurrentPwd:(AccountModel*)account callback:(ManagerCallBack*)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        AccountModel* result = [AccountModel mj_objectWithKeyValues:dict];
        ManagerEvent* event;
        if (result) {
//            event = [ManagerEvent toast:nil title:LocalizedString(@"LOGIN_SUCCESS")];
        }
        return event;
    }];
    
    [HTTPFacade verifyCurrentPwd:account callback:http];
}

/** 验证验证码是否正确 */
+(void)verifyVerCode:(AccountModel*)account callback:(ManagerCallBack*)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        AccountModel* result = [AccountModel mj_objectWithKeyValues:dict];
        ManagerEvent* event;
        if (result) {
            //            event = [ManagerEvent toast:nil title:LocalizedString(@"LOGIN_SUCCESS")];
        }
        return event;
    }];
    
    [HTTPFacade verifyVerCode:account callback:http];
}



/** 第三方登录 */
+(void)thirdPlatformLogin:(AccountModel*)account callback:(ManagerCallBack*)callback
{
    NSString* thirdpartyPlatform = account.thirdpartyPlatform;
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
     
        NSDictionary *sigDict = [dict objectForKey:@"tencentSignature"];
        NSDictionary *infoDict = [dict objectForKey:@"userInfo"];
        NSString *token = [dict objectForKey:@"token"];
        BOOL isExist = [dict objectForKey: @"exist"]? [[dict objectForKey: @"exist"] boolValue] : true;
        VLSTenSigModel *sigModel = [VLSTenSigModel mj_objectWithKeyValues:sigDict];
        VLSSelfInfoModel *infoModel = [VLSSelfInfoModel mj_objectWithKeyValues:infoDict];
        AccountModel* accountFromJSON = [[AccountModel alloc] initAccountWidthSigModel:sigModel infoModel:infoModel token:token];
        
        if (thirdpartyPlatform)
        {
            accountFromJSON.thirdpartyPlatform = thirdpartyPlatform;
            if (isExist)
            {
                [[VLSUserTrackingManager shareManager] trackingLogin: accountFromJSON withLoginType:[NSString stringWithFormat:@"%@", thirdpartyPlatform]];
            }
            else
            {
                [[VLSUserTrackingManager shareManager] trackingSignup: accountFromJSON];
            }
        }
        
        ManagerEvent* event;
        if (accountFromJSON) {
            event = [ManagerEvent toast:nil title:LocalizedString(@"LOGIN_SUCCESS")];
            [AccountManager sharedAccountManager].account = accountFromJSON;
            [[AccountManager sharedAccountManager] saveAccount];
            [[VLSIAPManager sharedInstance] recoverUnfinishedReceipts];
            [[Crashlytics sharedInstance] setUserIdentifier: [NSString stringWithFormat: @"%ld", (long)accountFromJSON.uid]];
        }
        return event;
    }];
    
    
//    if (account.thirdpartyPlatform) {
//        [[VLSUserTrackingManager shareManager] trackingLogin:account withLoginType:[NSString stringWithFormat:@"%@",account.thirdpartyPlatform]];
//    }
    
    [HTTPFacade thirdPlatformLogin:account callback:http];
}


/** 发送验证码 */
+ (void)sendVerCode:(AccountModel *)account callback:(ManagerCallBack *)callback
{
    HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
        ManagerEvent *event = [ManagerEvent toast:nil title:LocalizedString(@"VERCODE_SUCCESS")];
        return event;
    }];
    
    [HTTPFacade sendVerCode:account callback:http];
    
}

/** 手机号是否存在 */
+ (void)phoneExists:(AccountModel *)account callback:(ManagerCallBack *)callback
{
    HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
        
        //  ManagerEvent *event = [ManagerEvent toast:nil title:LocalizedString(@"VERCODE_SUCCESS")];
        return result;
    }];
    
    [HTTPFacade phoneExists:account callback:http];
    
}

+ (void)logOutCallback:(ManagerCallBack*)callback
{
    HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
        ManagerEvent *event;
        if (result) {
            event = [ManagerEvent toast:nil title:LocalizedString(@"SET_LOGIN_OUT_SUCCESS")];
            [[AccountManager sharedAccountManager] signOut];
        }
        return event;
    }];
    
    [HTTPFacade userLogout:http];
    
}
+(void)signUp:(AccountModel*)account callback:(ManagerCallBack*)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
                
        ManagerEvent* event= [ManagerEvent toast:nil title:LocalizedString(@"REGISTER_SUCCESS")];
        return event;
    }];
    
    [HTTPFacade signUp:account callback:http];
}

+ (void)postUserProfile:(AccountModel *)account callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        ManagerEvent* event= [ManagerEvent toast:nil title:LocalizedString(@"UPLOAD_SUCCESS")];
        return event;
    }];
    
    [HTTPFacade postSelfUserInfo:account callback:http];
}

+ (void)getUserProfileCallback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSDictionary * profile = [dict objectForKey:@"profile"];
        VLSSelfInfoModel *infoModel = [VLSSelfInfoModel mj_objectWithKeyValues:profile];
        AccountModel* account = [[AccountModel alloc] initAccountWidthSigModel:nil infoModel:infoModel token:nil];
        
        if (account) {
            [AccountManager sharedAccountManager].account = account;
            [[AccountManager sharedAccountManager] saveUserInfo:YES sig:NO];
        }
        return nil;
    }];
    [HTTPFacade getSelfUserInfoCallback:http];
}

+ (void)getTencentSigCallback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSDictionary *sigDict = [dict objectForKey:@"tencentSignature"];
        
        VLSTenSigModel *sigModel = [VLSTenSigModel mj_objectWithKeyValues:sigDict];
        AccountModel* account = [[AccountModel alloc] initAccountWidthSigModel:sigModel infoModel:nil token:nil];
        
        if (account) {
            
            [AccountManager sharedAccountManager].account = account;
            [[AccountManager sharedAccountManager] saveUserInfo:NO sig:YES];
        }
        return nil;
    }];
    [HTTPFacade getTenderSignCallback:http];
}
/** 获取心跳信息配置 */
+ (void)getHeartConfigCallback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        VLSHeartModel *result = [VLSHeartModel mj_objectWithKeyValues:dict[@"config"]];
        
        if (result) {
            
            [VLSHeartManager shareHeartManager].heartModel = result;
           
                       
            
            
        }
        return result;
    }];
    [HTTPFacade getHeartConfigCallback:http];
}

+ (void)getHostMarkList:(ManagerCallBack *)callback {
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
//        NSMutableArray *result = [VLSMarkModel mj_keyValuesArrayWithObjectArray:dict[@"list"]];
        
        return dict;
    }];
    [HTTPFacade getHostMarkListCallBack:http];
}


+ (void)getMyInfoCallback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        AccountModel* result = [AccountModel mj_objectWithKeyValues:dict];
        return result;
    }];
    [HTTPFacade getMyInfoCallback:http];
    
}


+ (void)forgotPsd:(AccountModel*)account callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        ManagerEvent* event= [ManagerEvent toast:nil title:LocalizedString(@"SEND_SUCCESS")];
        return event;
        
    }];
    [HTTPFacade forgotPsd:account callback:http];
}

/** 设置密码 */
+ (void)setNewPsd:(AccountModel*)account callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"SUCCESS")];
        return event;
    }];
    
    [[VLSUserTrackingManager shareManager] trackingResetpassword:account];
    [HTTPFacade setNewPassword:account callback:http];
}

//验证码绑定
+ (void)phoneBind:(AccountModel*)account callback:(ManagerCallBack*)callback{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        AccountModel* result = [AccountModel mj_objectWithKeyValues:dict];
        ManagerEvent* event;
        if (result) {
            [AccountManager sharedAccountManager].account = result;
            [[AccountManager sharedAccountManager] saveUserInfo:YES sig:NO];
        }
        return event;
    }];
    
    [HTTPFacade verCodeBind:account callback:http];
    
}

-(void)loadAccount
{
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:ACCOUNT_TOKEN];
    BOOL hasBase = [[NSUserDefaults standardUserDefaults] boolForKey:ACCOUNT_HASBASE];
    NSInteger uid = [[NSUserDefaults standardUserDefaults] integerForKey :ACCOUNT_UID];
    NSInteger level = [[NSUserDefaults standardUserDefaults] integerForKey:ACCOUNT_LEVEL];
    NSString *avatars = [[NSUserDefaults standardUserDefaults] objectForKey:ACCOUNT_AVATARS];
    NSString *mobilePhone = [[NSUserDefaults standardUserDefaults] objectForKey:ACCOUNT_MPHONE];
    NSString *gender = [[NSUserDefaults standardUserDefaults] objectForKey:ACCOUNT_GENDER];
    NSString *nickName = [[NSUserDefaults standardUserDefaults] objectForKey:ACCOUNT_NCNAME];
    NSString *tenderSig = [[NSUserDefaults standardUserDefaults] objectForKey:ACCOUNT_TENDERSIG];
    NSString *area = [[NSUserDefaults standardUserDefaults] objectForKey:ACCOUNT_AREA];
    NSString *intro = [[NSUserDefaults standardUserDefaults] objectForKey:ACCOUNT_INTRO];
    BOOL superUser = [[NSUserDefaults standardUserDefaults] boolForKey:ACCOUNT_SUPERUSER];
    BOOL isHost = [[NSUserDefaults standardUserDefaults] boolForKey:ACCOUNT_ISHOST];
    BOOL trialHost = [[NSUserDefaults standardUserDefaults] boolForKey:ACCOUNT_TRIALHOST];
    
    if (token) {
        AccountModel* account = [[AccountModel alloc] init];
        account.hasBaseProfile = hasBase;
        account.token = token;
        account.uid = uid;
        account.level = level;
        account.avatars = avatars;
        account.mobilePhone = mobilePhone;
        account.gender = gender;
        account.nickName = nickName;
        account.tenderSig = tenderSig;
        account.area = area;
        account.intro = intro;
        account.isSuperUser = superUser;
        account.isHost = isHost;
        account.trialHost = trialHost;
        
        self.account = account;
    }
}

-(void)saveAccount
{
    if (self.account) {
        [self saveUserInfo:YES sig:YES];
    }else {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_TOKEN];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_HASBASE];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_UID];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_LEVEL];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_AVATARS];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_UNAME];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_MPHONE];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_GENDER];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_NCNAME];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_TENDERSIG];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_AREA];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_INTRO];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_SUPERUSER];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_ISHOST];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACCOUNT_TRIALHOST];
        [self loadAccount];
        
    }
    
}

- (void)saveUserInfo:(BOOL)info sig:(BOOL)sig
{
    NSString *token = self.account.token;
    BOOL hasBase = self.account.hasBaseProfile;
    NSInteger uid = self.account.uid;
    NSInteger level = self.account.level;
    NSString *avatars = self.account.avatars;
    NSString *mobilePhone = self.account.mobilePhone;
    NSString *gender = self.account.gender;
    NSString *nickName = self.account.nickName;
    NSString *tenderSig = self.account.tenderSig;
    NSString *area = self.account.area;
    NSString *intro = self.account.intro;
    BOOL superUser = self.account.isSuperUser;
    BOOL isHost = self.account.isHost;
    BOOL trialHost = self.account.trialHost;
    
    if (info) {
        if (token && [token length]) {
            [[NSUserDefaults standardUserDefaults] setObject:token forKey:ACCOUNT_TOKEN];
        }
        if (uid) {
            [[NSUserDefaults standardUserDefaults] setInteger:uid forKey:ACCOUNT_UID];
        }
        if (level) {
            [[NSUserDefaults standardUserDefaults] setInteger:level forKey:ACCOUNT_LEVEL];
        }
        if (avatars) {
            [[NSUserDefaults standardUserDefaults] setObject:avatars forKey:ACCOUNT_AVATARS];
        }
        if (mobilePhone) {
            [[NSUserDefaults standardUserDefaults] setObject:mobilePhone forKey:ACCOUNT_MPHONE];
        }
        if (gender) {
            [[NSUserDefaults standardUserDefaults] setObject:gender forKey:ACCOUNT_GENDER];
        }
        if (nickName) {
            [[NSUserDefaults standardUserDefaults] setObject:nickName forKey:ACCOUNT_NCNAME];
        }
        if (area) {
            [[NSUserDefaults standardUserDefaults] setObject:area forKey:ACCOUNT_AREA];
        }if (intro) {
            [[NSUserDefaults standardUserDefaults] setObject:intro forKey:ACCOUNT_INTRO];
        }
        [[NSUserDefaults standardUserDefaults] setBool:hasBase forKey:ACCOUNT_HASBASE];
        [[NSUserDefaults standardUserDefaults] setBool:superUser forKey:ACCOUNT_SUPERUSER];
        [[NSUserDefaults standardUserDefaults] setBool:isHost forKey:ACCOUNT_ISHOST];
        [[NSUserDefaults standardUserDefaults] setBool:trialHost forKey:ACCOUNT_TRIALHOST];
    }
    if (sig) {
        if (tenderSig) {
            [[NSUserDefaults standardUserDefaults] setObject:tenderSig forKey:ACCOUNT_TENDERSIG];
        }
    }
    [self loadAccount];
    
}

- (void)signOut
{
    _account=nil;
    [self saveAccount];
    
}

+(DeviceModel*)getDeviceInformation
{
    DeviceModel *model = [[DeviceModel alloc] init];
    UIDevice *device = [UIDevice currentDevice];
    model.owner = device.name;       //获取设备所有者的名称
    model.model = device.model;      //获取设备的类别
    
    NSArray *languages = [NSLocale preferredLanguages];
    
    
    model.localizedModel = [languages objectAtIndex:0]; //获取手机语言
    model.systemName = device.systemName;   //获取当前运行的系统
    model.systemVersion = device.systemVersion;//获取当前系统的版本
    model.uuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    model.idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];//获取idfa
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    CGFloat scale = [UIScreen mainScreen].scale;
    CGFloat width = rect.size.width * scale;
    CGFloat height = rect.size.height * scale;
    model.ScreenWidth = [NSString stringWithFormat:@"%f",width];
    model.ScreenHeight = [NSString stringWithFormat:@"%f",height];
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [info subscriberCellularProvider];
    model.carrierName = [NSString stringWithFormat:@"%@",[carrier carrierName]];
    model.radioAccessTechnology = [[NSString alloc] initWithFormat:@"%@",info.currentRadioAccessTechnology];
    model.mac = [AccountManager getMacAddress];
    model.token = [AccountManager sharedAccountManager].account.token;
    
    model.locaStr = [VLSLocationManager shareManager].locaStr;
    model.longitude = [VLSLocationManager shareManager].longitude;
    model.latitude = [VLSLocationManager shareManager].latitude;
    model.GEOLocation = [NSString stringWithFormat:@"%@,%@",model.longitude,model.latitude];
    return model;
}
//获取手机mac
+ (NSString *)getMacAddress
{
    int                 mgmtInfoBase[6];
    char                *msgBuffer = NULL;
    size_t              length;
    unsigned char       macAddress[6];
    struct if_msghdr    *interfaceMsgStruct;
    struct sockaddr_dl  *socketStruct;
    NSString            *errorFlag = NULL;
    
    // Setup the management Information Base (mib)
    mgmtInfoBase[0] = CTL_NET;        // Request network subsystem
    mgmtInfoBase[1] = AF_ROUTE;       // Routing table info
    mgmtInfoBase[2] = 0;
    mgmtInfoBase[3] = AF_LINK;        // Request link layer information
    mgmtInfoBase[4] = NET_RT_IFLIST;  // Request all configured interfaces
    
    // With all configured interfaces requested, get handle index
    if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0)
        errorFlag = @"if_nametoindex failure";
    else
    {
        // Get the size of the data available (store in len)
        if (sysctl(mgmtInfoBase, 6, NULL, &length, NULL, 0) < 0)
            errorFlag = @"sysctl mgmtInfoBase failure";
        else
        {
            // Alloc memory based on above call
            if ((msgBuffer = malloc(length)) == NULL)
                errorFlag = @"buffer allocation failure";
            else
            {
                // Get system information, store in buffer
                if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, NULL, 0) < 0)
                    errorFlag = @"sysctl msgBuffer failure";
            }
        }
    }
    
    // Befor going any further...
    if (errorFlag != NULL)
    {
        NSLog(@"Error: %@", errorFlag);
        return errorFlag;
    }
    
    // Map msgbuffer to interface message structure
    interfaceMsgStruct = (struct if_msghdr *) msgBuffer;
    
    // Map to link-level socket structure
    socketStruct = (struct sockaddr_dl *) (interfaceMsgStruct + 1);
    
    // Copy link layer address data in socket structure to an array
    memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);
    
    // Read from char array into a string object, into traditional Mac address format
    NSString *macAddressString = [NSString stringWithFormat:@"%02x:%02x:%02x:%02x:%02x:%02x",
                                  macAddress[0], macAddress[1], macAddress[2],
                                  macAddress[3], macAddress[4], macAddress[5]];
    NSLog(@"Mac Address: %@", macAddressString);
    
    // Release the buffer memory
    free(msgBuffer);
    
    return macAddressString;
}

//判断设备信息有没有发生变化 用不用 传后台
+ (BOOL)isPost
{
    
    DeviceModel *model = [AccountManager getDeviceInformation];
    NSMutableArray *array = [DeviceModel searchWithWhere:nil];
    if (array.count>0) {
        DeviceModel *model1 = [array firstObject];
        NSString *token = [AccountManager sharedAccountManager].account.token;
        
        if (![model1.token isEqualToString:token]) {
            return YES;
        }else
        {
            
            if (![model.localizedModel isEqualToString:model1.localizedModel]
                ||![model.systemVersion isEqualToString:model1.systemVersion]
                ||![model.systemName isEqualToString:model1.systemName]
                ||![model.owner isEqualToString:model1.owner]
                ||![model.idfa isEqualToString:model1.idfa]
                ||![model.mac isEqualToString:model1.mac]
                ||![model.ScreenHeight isEqualToString:model1.ScreenHeight]
                ||![model.ScreenWidth isEqualToString:model1.ScreenWidth] || ![model.locaStr isEqualToString:model1.locaStr] || ![model.GEOLocation isEqualToString:model1.GEOLocation]) {
                
                [DeviceModel deleteWithWhere:[DeviceModel class]];
                
                return YES;
            }else
            {
                return NO;
            }
        }
    }else
    {
        return YES;
    }
}

@end
