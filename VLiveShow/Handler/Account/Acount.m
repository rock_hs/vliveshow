//
//  Acount.m
//  VLiveShow
//
//  Created by SuperGT on 16/5/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "Acount.h"
#import "NSString+UserFile.h"

@interface Acount ()<NSCoding>

@end

@implementation Acount

+ (instancetype)DefaultAcount
{
    static Acount *acount = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *filepath = [NSString UserFilePath];
        acount = [NSKeyedUnarchiver unarchiveObjectWithFile:filepath];
        if (!acount) {
            acount = [[Acount alloc]init];
        }
        
    });
    return acount;
}


#pragma mark 每次登录保存信息
- (void)saveLogin:(NSDictionary *)dict
{
    
    self.identifier = dict[@"id"];
    self.usersig = dict[@"sig"];
    self.token = dict[@"token"];
    self.SigTime = dict[@"expireTime"];
    self.tokenTime = dict[@"tokentime"];
    
    // 文件路径
    NSString *filepath = [NSString UserFilePath];
    BOOL result = [NSKeyedArchiver archiveRootObject:self toFile:filepath];
    NSLog(@"%d",result);
}

#pragma mark 判断是否登录状态
- (BOOL)isLogin
{
    return YES;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
//        self.identifier = [aDecoder decodeObjectForKey:KUserID];
//        self.token = [aDecoder decodeObjectForKey:Ktoken];
//        self.usersig = [aDecoder decodeObjectForKey:KUserSig];
//        self.SigTime = [aDecoder decodeObjectForKey:KSigtime];
//        self.tokenTime = [aDecoder decodeObjectForKey:Ktokentime];
    }
    return self;

}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
//    [aCoder encodeObject:self.identifier forKey:KUserID];
//    [aCoder encodeObject:self.token forKey:Ktoken];
//    [aCoder encodeObject:self.usersig forKey:KUserSig];
    
    
}
@end
