//
//  VLSTenSigModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/8/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSTenSigModel : NSObject
@property (nonatomic, strong) NSString *sig;              // 腾讯签名
@property (nonatomic, assign) NSInteger initTime;
@property (nonatomic, assign) NSInteger expireTime;              


@end
