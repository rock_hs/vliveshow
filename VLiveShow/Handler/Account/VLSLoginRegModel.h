//
//  VLSLoginRegModel.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/31.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSLoginRegModel : NSObject

@property(nonatomic, copy) NSString *signup;//注册
@property(nonatomic, copy) NSString *login;//登录
@property(nonatomic, copy) NSString *resetpassword;//重置密码
@property(nonatomic, copy) NSString *changeregion;//选择国家地区
@property(nonatomic, copy) NSString *closead;//关闭广告页

@end
