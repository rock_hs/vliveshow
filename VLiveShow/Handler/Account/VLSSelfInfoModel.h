//
//  VLSSelfInfoModel.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/8/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AvatarsModel.h"

@interface VLSSelfInfoModel : NSObject

@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, assign) NSInteger level;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *nickName;               // 昵称
@property (nonatomic, strong) NSString *gender;                 // 性别
@property (nonatomic, strong) NSString *intro;                  // 个性签名
@property (nonatomic, strong) NSString *experience;             // jingyan
@property (nonatomic, strong) NSString *area;                   // 定位位置

@property (nonatomic, strong) NSString *nationCode;             // 国家码
@property (nonatomic, strong) NSString *decodeMobilePhone;

@property (nonatomic, strong) NSString *signupType;

@property (nonatomic ,strong) AvatarsModel *avatars;        //解析头像
@property (nonatomic ,strong) AvatarsModel *showCover;      //解析头像


@property (nonatomic, assign) BOOL hasBaseProfile;              // 是否已经上传了部分信息
@property (nonatomic, assign) BOOL isHost;                      // 是否有權限
@property (nonatomic, assign) BOOL trialHost;                   // 是否有试播权限
@property (nonatomic, assign) BOOL isGuest;                     //
@property (nonatomic, assign) BOOL isSuperUser;                 // 超级用户
@property (nonatomic, assign) BOOL isAudiences;                 //
@property (nonatomic, assign) BOOL isSpecialGuest;              //

@end
