//
//  AccountModel.m
//  Stacks
//
//  Created by 李雷凯 on 15/10/27.
//  Copyright © 2015年 LK. All rights reserved.
//

#import "AccountModel.h"

@implementation AccountModel

//+ (NSDictionary *)replacedKeyFromPropertyName
//{
//    return @{@"uid" : @"id",
//             @"tenderSig" :@"sig",
//             };
//}

- (instancetype)initAccountWidthSigModel:(VLSTenSigModel *)sig infoModel:(VLSSelfInfoModel *)info token:(NSString *)token
{
    if (self = [super init]) {
        if (info) {
            self.uid = info.uid;
            self.level = info.level;
            self.nickName = info.nickName;
            self.mobilePhone = info.decodeMobilePhone;
            self.gender = info.gender;
            self.area = info.area;
            self.intro = info.intro;
            self.isSuperUser = info.isSuperUser;
            self.isHost = info.isHost;
            self.trialHost = info.trialHost;
            self.nationCode = info.nationCode;
            self.avatars = [NSString stringWithFormat:@"%@/v1/users/%ld/avatars.png",BASE_URL,(long)self.uid];
            if (info.showCover) {
                
            }
        }
        if (token) {
            self.token = token;
        }
        if (sig) {
            self.tenderSig = sig.sig;
        }
        
    }
    return self;
}

@end
