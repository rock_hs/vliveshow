//
//  AccountModel.h
//  Stacks
//
//  Created by 李雷凯 on 15/10/27.
//  Copyright © 2015年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MJExtension/MJExtension.h>
#import "PostFileModel.h"
#import "VLSTenSigModel.h"
#import "VLSSelfInfoModel.h"

@interface AccountModel : NSObject

@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, assign) NSInteger level;
@property (nonatomic, strong) NSString *avatars;
@property (nonatomic, strong) NSString *coverUrl;
@property (nonatomic, strong) NSString *mobilePhone;
@property (nonatomic, strong) NSString *gender;                 // 性别
@property (nonatomic, strong) NSString *nickName;               // 昵称
@property (nonatomic, assign) BOOL hasBaseProfile;              // 是否已经上传了部分信息
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *tenderSig;              // 腾讯签名
@property (nonatomic, strong) NSString *area;                   // 定位位置
@property (nonatomic, strong) NSString *nationCode;             // 国家码
@property (nonatomic, strong) NSString *intro;                  // 个性签名
@property (nonatomic, assign) BOOL isSuperUser;                 // 超级用户
@property (nonatomic, assign) BOOL isHost;                      // 是否有權限
@property (nonatomic, assign) BOOL trialHost;                   // 是否有试播权限
//主要作为上传用户信息用
@property (nonatomic, strong) PostFileModel *fileModel;
@property (nonatomic, strong) NSString *verificationCode;
@property (nonatomic, strong) NSString *password;               // 密码
@property (nonatomic, strong) NSString *resetPwd;               // 重设密码


@property (nonatomic, strong) NSString *thirdpartyIconUrl;      // 第三方账号头像 url
@property (nonatomic, strong) NSString *thirdpartyAccount;      // 第三方账号唯一标识
@property (nonatomic, strong) NSString *thirdpartyPlatform;     // 第三方平台(QQ,WX,WB)

//追踪登录方式
@property (nonatomic, strong) NSString *loginType;//登录方式
//追踪绑定位置
@property (nonatomic ,strong) NSString *bindType;
//加入房间的方式
@property (nonatomic, strong) NSString *joinType;

/** 新增 投票字段 */
@property (nonatomic, strong) NSNumber* residueVotes;   //剩余投票次数
@property (nonatomic, strong) NSNumber* totalVotes;     //总的投票数

- (instancetype)initAccountWidthSigModel:(VLSTenSigModel *)sig infoModel:(VLSSelfInfoModel *)info token:(NSString *)token;


@end
