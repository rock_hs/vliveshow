//
//  VLSPriseManager.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSPriseManager.h"

@interface VLSPriseManager (){
    NSInteger priseCount;
}

@property (nonatomic, strong) NSOperationQueue *priseQueue;

@property (nonatomic, strong) dispatch_source_t timer;

@end

@implementation VLSPriseManager

- (instancetype)init
{
    if (self = [super init]) {
//        self.priseQueue = [[NSOperationQueue alloc] init];
        priseCount = 0;
        self.maxPraise = 8;
        [self addPriseTimer];
    }
    return self;
}

- (void)addPriseTimer
{
    uint64_t interval = (uint64_t)(0.2 * NSEC_PER_SEC);
    dispatch_queue_t queue = dispatch_queue_create("SuperGT queue", 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer, dispatch_time(DISPATCH_TIME_NOW, 0), interval, 0);
    dispatch_source_set_event_handler(_timer, ^()
                                      {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [self compareStatic];
                                          });
                                          
                                          
                                      });
    dispatch_resume(_timer);
}

- (void)addOperationToQueue
{
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]initWithTarget:self selector:@selector(excuteOperation) object:nil];
    [self.priseQueue addOperation:operation];
}

- (void)excuteOperation
{
#ifdef DONOT_SHOW_MSG
    return;
#endif
    if (priseCount < self.maxPraise)
    {
        priseCount ++;
    }
    else
    {
        //NSLog(@"Skip prise");
    }
}

- (void)compareStatic
{
    if (priseCount > 0) {
        priseCount --;
        if ([_delegate respondsToSelector:@selector(showPriseAnimation)]) {
            [_delegate showPriseAnimation];

        }
    }
}
- (void)dealloc
{
    _timer = nil;
}

@end
