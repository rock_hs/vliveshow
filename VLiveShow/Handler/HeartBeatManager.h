//
//  HeartBeatManager.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HeartBeatManagerHeartBeatFailed @"HeartBeatManagerHeartBeatFailed"

@protocol VLSHaertBeatWithWatchNum <NSObject>

- (NSInteger)heartBeatWithWatchNum;

@end

@interface HeartBeatManager : NSObject

//@property (nonatomic, weak) id
@property (nonatomic, weak) id<VLSHaertBeatWithWatchNum> delegate;


+ (instancetype)shareManager;

- (void)startHeartBeat:(NSString *)roomid;
- (void)destoryHeartBeat;

@end
