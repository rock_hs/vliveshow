//
//  VLSGiftManager.h
//  VLiveShow
//
//  Created by LK on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GiftModel.h"

@protocol  VLSGiftManagerDelegate<NSObject>

/**
 *  收到常规的礼物
 *
 *  @param giftList VLSNormalGiftViewModel 数组
 */
- (void)receiveNormalGiftMessage:(GiftModel *)model;

/**
 *  收到自定义的礼物
 *
 *  @param giftList VLSCustomGiftViewModel 数组
 */
- (void)receiveCustomGiftMessage:(NSMutableArray *)giftList;

@end

@interface VLSGiftManager : NSObject

@property (nonatomic, weak) id <VLSGiftManagerDelegate>delegate;

@property (nonatomic,strong)NSString * timerStamp;
@property (nonatomic,assign)NSInteger currentCount;

+ (instancetype)sharedManager;


//发送礼物消息
- (void)sendGiftMessage:(GiftModel *)sendModel;

- (void)receiveGiftMessage:(NSArray *)giftMessage;


@end
