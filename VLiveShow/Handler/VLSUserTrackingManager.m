//
//  VLSUserTrackingManager.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSUserTrackingManager.h"
#import "AccountManager.h"
#import "VLSLiveHttpManager.h"
#import "TalkingData.h"
#import "TalkingDataAppCpa.h"
#import "VLSUserTrackContext.h"

@interface VLSUserTrackingManager()

@property (nonatomic, strong) AFHTTPSessionManager *manager;

@end
@implementation VLSUserTrackingManager

//获取 单例 对象
+ (instancetype)shareManager{ //最常用的单例创建
    static VLSUserTrackingManager *httpEngine = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        httpEngine = [[VLSUserTrackingManager alloc] init];
    });
    return httpEngine;
}

-(NSString*)getUserInfo{
    
    NSString *userid = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
    return userid;
}

- (void)getDeviceInfo{
    
    [AccountManager getDeviceInformation];
}

/****登录注册追踪****/
- (void)trackingSignup:(AccountModel *)model
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"signup" forKey:@"Action"];
    if (model.mobilePhone) {
        [dict setObject:model.mobilePhone forKey:@"phone"];
    }
    if (model.nationCode) {
        [dict setObject:model.nationCode forKey:@"code"];
    }
    if (model.uid)
    {
        [dict setObject: @(model.uid) forKey:@"userid"];
    }
    if (model.nickName)
    {
        [dict setObject: model.nickName forKey:@"nick_name"];
    }
    if ([model.thirdpartyPlatform length] > 0)
    {
        [dict setObject: model.thirdpartyPlatform forKey:@"signup_type"];
    }
    else
    {
        [dict setObject: @"phone" forKey:@"signup_type"];
    }
    
    [self saveTrackingInfo:dict];
    [TalkingData trackEvent: @"signup"];
    [TalkingDataAppCpa onRegister: [NSString stringWithFormat: @"%ld", (long)model.uid]];
    
}

- (void)trackingLogin:(AccountModel *)model withLoginType:(NSString *)loginType{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"login" forKey:@"Action"];
    [dict setObject:loginType forKey:@"logintype"];
    if (model.mobilePhone) {
        [dict setObject:model.mobilePhone forKey:@"phone"];
    }
    if (model.thirdpartyAccount) {
        [dict setObject:model.thirdpartyAccount forKey:@"thirdid"];
    }
    [self saveTrackingInfo:dict];
    [TalkingData trackEvent: @"login"];
    [TalkingDataAppCpa onLogin: [NSString stringWithFormat: @"%ld", (long)model.uid]];
}

- (void)trackingResetpassword:(AccountModel *)model{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"resetpassword" forKey:@"Action"];
    if (model.mobilePhone) {
        [dict setObject:model.mobilePhone forKey:@"phone"];
    }
    [self saveTrackingInfo:dict];
}

/****直播间追踪****/
-(VLSUserTrackContext*)trackingJoinroom:(VLSJoinRoomModel *)model{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"joinroom" forKey:@"Action"];
    if (model.roomID) {
        [dict setObject:model.roomID forKey:@"roomid"];
    }
    if (model.joinType) {
        [dict setObject:model.joinType forKey:@"joinType"];
    }
    VLSUserTrackContext* context = [[VLSUserTrackContext alloc] init];
    [dict setObject: [context.idenetifier UUIDString] forKey: @"actionuuid"];
    
    [self saveTrackingInfo:dict];
    [TalkingData trackPageBegin: @"in_room"];
    [TalkingDataAppCpa onCustEvent1];
    
    return context;
}

- (void)trackingQuitroom:(NSString *)leaveType context: (VLSUserTrackContext*)context
{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"quitroom" forKey:@"Action"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }
    [dict setObject:leaveType forKey:@"leaveType"];
    if (context != nil)
    {
        [dict setObject: [context.idenetifier UUIDString] forKey: @"actionuuid"];
    }
    [self saveTrackingInfo:dict];
    [TalkingData trackPageEnd: @"in_room"];
}

- (void)trackingRoomInterruption: (NSString*)type context: (VLSUserTrackContext*)context
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"Action"] = @"roominterruption";
    if ([type length] > 0)
    {
        dict[@"interruptiontype"] = type;
    }
    if ([[VLSMessageManager sharedManager] getRoomId])
    {
        dict[@"roomid"] = [[VLSMessageManager sharedManager] getRoomId];
    }
    if (context != nil)
    {
        dict[@"actionuuid"] = [context.idenetifier UUIDString];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingBindingphone:(AccountModel *)model bindType:(NSString *)bindType{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"bindingphone" forKey:@"Action"];
    [dict setObject:bindType forKey:@"bindType"];
    if (model.mobilePhone) {
        [dict setObject:model.mobilePhone forKey:@"phone"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingRoomBindPhone:(AccountModel *)model bindType:(NSString *)bindType{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"bindingphone" forKey:@"Action"];
    [dict setObject:bindType forKey:@"bindType"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }
    if (model.mobilePhone) {
        [dict setObject:model.mobilePhone forKey:@"phone"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingLinkrequest:(VLSAVInteractionModel *)model{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"linkrequest" forKey:@"Action"];
    if (model.toAccount) {
        [dict setObject:model.toAccount forKey:@"to"];
    }
    if (model.roomId) {
        [dict setObject:model.roomId forKey:@"roomid"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingAcceptlink:(VLSAVInteractionModel *)model{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"acceptlink" forKey:@"Action"];
    if (model.roomId) {
        [dict setObject:model.roomId forKey:@"roomid"];
    }
    if (model.toAccount) {
        [dict setObject:model.toAccount forKey:@"to"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingMute:(NSString*)isMute{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"mute" forKey:@"Action"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }
    [dict setObject:isMute forKey:@"mute"];
    [self saveTrackingInfo:dict];
}

- (void)trackingGuestMute:(NSString *)mute{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"guestmute" forKey:@"Action"];
    [dict setValue:mute forKey:@"mute"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingUponImmediately{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"uponImmediately" forKey:@"Action"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingChangeactor:(NSString *)uid{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"changeactor" forKey:@"Action"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }

    [self saveTrackingInfo:dict];
}

- (void)trackingCancellink:(VLSAVInteractionModel *)model {

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"cancellink" forKey:@"Action"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }
    if (model.action) {
        [dict setObject:model.action forKey:@"cancelType"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingInvite:(VLSAVInteractionModel *)model{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"invite" forKey:@"Action"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }
    if (model.toAccount) {
        [dict setObject:model.toAccount forKey:@"to"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingGift:(GiftModel *)model{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"gift" forKey:@"Action"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }
    if (model.giftID) {
        [dict setObject:model.giftID forKey:@"giftid"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingShareStatus:(NSString *)shareStatus platform:(NSString *)platform roomID:(NSString *)roomid{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"share" forKey:@"Action"];
    [dict setObject:shareStatus forKey:@"shareStatus"];
    [dict setObject:platform forKey:@"platform"];
    [dict setObject:roomid forKey:@"roomid"];
    [self saveTrackingInfo:dict];
}

- (void)trackingAgreeGotoRoom:(NSString *)roomid anchorid:(NSString *)anchorid fromWhere:(NSString *)where{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:roomid forKey:@"roomid"];
    [dict setObject:anchorid forKey:@"anchorid"];
    [dict setObject:where forKey:@"from_where"];
    [self saveTrackingInfo:dict];
}

/****我的界面追踪****/
- (void)trackingViewprofile:(NSString *)userID{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"viewprofile" forKey:@"Action"];
    [dict setObject:userID forKey:@"userID"];
    [self saveTrackingInfo:dict];
}


- (void)trackingChangeProfile:(NSString *)setString status:(NSString *)status{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"changeprofile" forKey:@"Action"];
    [dict setObject:setString forKey:@"change"];
    [dict setObject:status forKey:@"status"];
    [self saveTrackingInfo:dict];
}

- (void)tackingLocation:(NSString *)location longitude:(NSString *)longitude latitude:(NSString *)latitude{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"getlocation" forKey:@"Action"];
    [dict setObject:location forKey:@"location"];
    [dict setObject:longitude forKey:@"longitude"];
    [dict setObject:latitude forKey:@"latitude"];
    [self saveTrackingInfo:dict];
}

- (void)trackingUserExitLiveRoom:(NSString *)leave{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:leave forKey:@"Action"];
    [self saveTrackingInfo:dict];
}

- (void)trackingSendSensitive:(NSString *)sensitive{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"sendsensitive" forKey:@"Action"];
    [dict setObject:sensitive forKey:@"sensitiveword"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingOpenAPPTime:(NSString *)timeStr backOrFore:(NSString *)isOrBack{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"openapp" forKey:@"Action"];
    [dict setObject:timeStr forKey:@"opentime"];
    [dict setObject:isOrBack forKey:@"where"];
    [self saveTrackingInfo:dict];
}

//用户关注
- (void)trackingFoucswithBeFocusedUserid:(NSString *)beFocusedUserid action:(NSString *)action{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:action forKey:@"Action"];
    [dict setObject:beFocusedUserid forKey:@"target_userid"];
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        [dict setObject:[[VLSMessageManager sharedManager] getRoomId] forKey:@"roomid"];
        [dict setObject:@"room" forKey:@"where"];
    }else{
    
        [dict setObject:@"mine" forKey:@"where"];
    }
    [self saveTrackingInfo:dict];
}

- (void)trackingUserAction:(NSString *)targetUserid action:(NSString *)action{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:action forKey:@"Action"];
    [dict setObject:targetUserid forKey:@"target_userid"];
    [dict setObject:@"mine" forKey:@"where"];
    [self saveTrackingInfo:dict];
}


-(void)trackingWelcomeApp{

    if (![AccountManager isPost]) {
        
    } else {
        
        ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
        callback.updateBlock = ^(id result) {
            DeviceModel *deviceModel = [AccountManager getDeviceInformation];
            // 存数据库
            [deviceModel saveToDB];
            
        };
        callback.errorBlock = ^(id result) {
        };
        
        [VLSLiveHttpManager postWelcomeDeviceInfo:callback];
    }
}

- (void)trackingActivate
{
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    
    DeviceModel *deviceModel = [AccountManager getDeviceInformation];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue: @"ios" forKey: @"device_os"];
    [dict setValue: [NSBundle mainBundle].bundleIdentifier forKey: @"ads_source"];
    if ([deviceModel.owner length] > 0)
    {
        [dict setValue: deviceModel.owner forKey: @"device_name"];
    }
    if ([deviceModel.systemVersion length] > 0)
    {
        [dict setValue: deviceModel.systemVersion forKey: @"device_os_version"];
    }
    if (deviceModel.idfa == nil || [deviceModel.idfa isEqualToString: @"00000000-0000-0000-0000-000000000000"])
    {
        [dict setValue: @"" forKey: @"idfa"];
    }
    else
    {
        [dict setValue: deviceModel.idfa forKey: @"idfa"];
    }
    if ([AccountManager sharedAccountManager].account.uid > 0)
    {
        NSString *str = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
        [dict setObject:str forKey:@"userid"];
    }
    
    callback.updateBlock = ^(id result) {
        NSLog(@"result: %@", result);
    };
    callback.errorBlock = ^(id result) {
        NSLog(@"result: %@", result);
    };
    
    [VLSLiveHttpManager postDeviceActivateTrack: dict callback: callback];
    [TalkingData trackEvent: @"activate"];

}

//post请求
-(void)saveTrackingInfo:(NSDictionary *)dict{
    DeviceModel *deviceModel = [AccountManager getDeviceInformation];
    NSMutableDictionary *dic = [deviceModel mj_keyValues];
    [dic addEntriesFromDictionary:dict];
    if ([AccountManager sharedAccountManager].account.uid > 0)
    {
        NSString *str = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
        [dic setObject:str forKey:@"userid"];
    }
    
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
    };
    callback.errorBlock = ^(id result)
    {
    };
    
    [VLSLiveHttpManager postUserTrackingInfo:dic callBcak:callback];
}

- (void)trackingEnterIntoBackground
{
    [TalkingData trackEvent: @"enter_background"];
}

- (void)trackingTerminate
{
    [TalkingData trackEvent: @"terminate"];
}

- (void)trackingIAP:(AccountModel *)model orderId: (NSString*)orderId amount: (NSInteger)amount currencyType: (NSString*)currencyType payType: (NSString*)payType
{
    [TalkingData trackEvent: @"iap"];
    [TalkingDataAppCpa onPay: [NSString stringWithFormat:@"%ld",[AccountManager sharedAccountManager].account.uid]  withOrderId: orderId withAmount: (int)amount withCurrencyType: currencyType withPayType: payType];
}

- (VLSUserTrackContext*)trackingLinkBegin:(VLSAVInteractionModel *)model
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"Action"] = @"linkbegin";
    if ([[VLSMessageManager sharedManager] getRoomId]) {
        dict[@"roomid"] = [[VLSMessageManager sharedManager] getRoomId];
    }
    if ([model.fromAccount length] > 0)
    {
        dict[@"from"] = model.fromAccount;
    }
    if ([model.toAccount length] > 0)
    {
        dict[@"host"] = model.toAccount;
    }
    VLSUserTrackContext* context = [[VLSUserTrackContext alloc] init];
    dict[@"actionuuid"] = [context.idenetifier UUIDString];

    [self saveTrackingInfo:dict];
    return context;
}

- (void)trackingLinkEnd: (VLSAVInteractionModel *)model context: (VLSUserTrackContext*)context
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"Action"] = @"linkend";
    if ([[VLSMessageManager sharedManager] getRoomId])
    {
        dict[@"roomid"] = [[VLSMessageManager sharedManager] getRoomId];
    }
    if ([model.fromAccount length] > 0)
    {
        dict[@"from"] = model.fromAccount;
    }
    if ([model.toAccount length] > 0)
    {
        dict[@"host"] = model.toAccount;
    }
    if (context != nil)
    {
        dict[@"actionuuid"] = [context.idenetifier UUIDString];
    }
    
    [self saveTrackingInfo:dict];
}

- (void)trackingClickWithAction: (NSString*)action extraInfo: (NSDictionary*)extraInfo
{
    if ([action length] == 0)
    {
        return;
    }
    if ([[AccountManager sharedAccountManager].account.token length] == 0)
    {
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"Action"] = action;
    if (extraInfo)
    {
        [dict addEntriesFromDictionary: extraInfo];
    }
    [self saveTrackingInfo:dict];
    [TalkingDataAppCpa onCustEvent2];
}
@end
