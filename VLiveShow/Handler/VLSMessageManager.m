//
//  VLSMessageManager.m
//  XmppDemo
//
//  Created by 李雷凯 on 16/5/17.
//  Copyright © 2016年 LK. All rights reserved.
//

#import "VLSMessageManager.h"
#import "AccountManager.h"
#import "VLSDBHelper.h"
#import "VLSTeamMessageModel.h"
#import "VLSLiveRemindModel.h"
#import "VLSFansModel.h"
#import "AppDelegate+CurretVC.h"
#import "VLSBaseViewController.h"
#import "ManagerEvent.h"
#import "MBProgressHUD+Add.h"
#import "VLiveShow-Swift.h"

/*
enum {
    TYPE_NORMALMESSAGE=10011,//普通消息
    TYPE_JOINOROUT,//进入
    TYPE_SUPERJOIN,//超级管理员
    TYPE_GIFT,//礼物消息
    TYPE_BARRAGE,//弹幕
    TYPE_GREENMESSAGE,//绿色消息
    TYPE_FIRSTJOINROOMMESSAGE,//刚进入直播间时的系统公告
    TYPE_SYSTEMMESSAGE,//系统红色公告字体
    TYPE_SEND_PRAISE,   //点亮了屏幕
    TYPE_FORBIDSENDMSG, //禁止发言
    TYPE_FORBIDSENDMSG_CANCEL,  //取消禁止发言
};
*/
@interface VLSMessageManager ()<IMManagerDelegate>
{
    VLSVideoViewModel *mainGuestViewModel; //嘉宾在主屏；
}
@end

@implementation VLSMessageManager

+ (instancetype)sharedManager
{
    static VLSMessageManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[VLSMessageManager alloc ] init];
        
    });
    //     [VLSIMManager sharedInstance].delegate = (id<IMManagerDelegate>)_sharedManager;
    return _sharedManager;
}

- (instancetype)init
{
    [VLSIMManager sharedInstance].delegate = self;
    _normalMessages = [[NSMutableArray alloc] initWithCapacity:0];
    //    [self initMessagrTimer];
    return self;
    
}

#pragma mark - 暂时注释掉，后续继续完成，主要消息优化，每秒钟限制UI显示的数量（文本消息）

- (void)initMessagrTimer
{
    
    // 获得队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    // 创建一个定时器(dispatch_source_t本质还是个OC对象)
    _messageTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    // 设置定时器的各种属性（几时开始任务，每隔多长时间执行一次）
    // GCD的时间参数，一般是纳秒（1秒 == 10的9次方纳秒）
    // 何时开始执行第一个任务
    // dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC) 比当前时间晚3秒
    dispatch_time_t start = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
    uint64_t interval = (uint64_t)(0.06 * NSEC_PER_SEC);
    dispatch_source_set_timer(self.messageTimer, start, interval, 0);
    // 设置回调
    dispatch_source_set_event_handler(self.messageTimer, ^{
        NSLog(@"------------%@", [NSThread currentThread]);
        NSMutableArray *temp = [NSMutableArray arrayWithCapacity:0];
        if (self.normalMessages.count > 0) {
            for (int i=0; i<self.normalMessages.count; i++) {
                if (i<4) {
                    [temp addObject:[self.normalMessages objectAtIndex:i]];
                }
            }
            if (temp.count>0) {
                //通知主线程刷新
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.normalMessages removeObjectsInArray:temp];
                    //回调或者说是通知主线程刷新，
                    NSLog(@"callback");
                    if ([_delegate respondsToSelector:@selector(receiveNormalMessage:)]) {
                        [_delegate receiveNormalMessage:temp];
                    }
                });
                
            }
            
        }
        // 取消定时器
        //        dispatch_cancel(self.messageTimer);
        //        self.messageTimer = nil;
    });
    // 启动定时器
    dispatch_resume(self.messageTimer);
    
}

- (NSString *)getRoomId
{
    return [VLSIMManager sharedInstance].RoomID;
}

//首次布局
- (void)initFirstAVroom:(NSMutableArray *)array
{
    self.liveAnchors = array;
    if ([_delegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
        [_delegate receiveUpdateLiveLayout:self.liveAnchors];
    }
}

//根据用户ID判断用户是否正在直播
- (BOOL)checkUserIsLiving:(NSString *)userId
{
    for (VLSVideoViewModel *model in self.liveAnchors) {
        if ([userId isEqualToString:model.userId]) {
            return YES;
        }
    }
    return NO;
    
}

//根据用户ID判断用户是否是主播
- (BOOL)checkUserIsAnchor:(NSString *)userId
{
    for (VLSVideoViewModel *model in self.liveAnchors) {
        if (model.userType == UserTypeHost) {
            if ([userId isEqualToString:model.userId]) {
                return YES;
            }
        }
    }
    return NO;
}
//根据用户ID判断用户是否是嘉宾
- (BOOL)checkUserIsGuests:(NSString *)userId
{
    for (VLSVideoViewModel *model in self.liveAnchors) {
        if ([model.userId isEqualToString:userId]) {
            if (![self checkUserIsAnchor:userId]) {
                return YES;
                
            }
            
        }
        
    }
    
    return NO;
}
//根据用户ID判断用户是否是观众
- (BOOL)checkUserIsAudience:(NSString *)userId
{
    for (VLSVideoViewModel *model in self.liveAnchors) {
        if ([model.userId isEqualToString:userId]) {
            return NO;
        }
    }
    return YES;
}
//根据用户ID判断用户是否是自己
- (BOOL)checkUserIsSelf:(NSString *)userId
{
    if ([userId isKindOfClass:[NSNumber class]]) {
        userId = [NSString stringWithFormat:@"%@",(NSNumber *)userId];
    }
    if ([userId isEqualToString:self.host.userId]) {
        return YES;
    }
    return NO;
}

- (BOOL)checkUserIsVoiceMute:(NSString *)userId
{
    for (VLSVideoViewModel *model in self.liveAnchors) {
        if ([model.userId isEqualToString:userId]) {
            if (model.muteVoice) {
                return YES;
            }else {
                return NO;
            }
        }
    }
    return NO;
}
//监测主播显示声音小红点
- (BOOL)checkAnchorShouldShowVoiceRedPoint
{
    for (VLSVideoViewModel *model in self.liveAnchors) {
        if (model.muteVoice) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)checkUserIsVoiceMuteBySelf:(NSString *)userId
{
    for (VLSVideoViewModel *model in self.liveAnchors) {
        if ([model.userId isEqualToString:userId]) {
            if(model.muteVoiceBySelf){
                return YES;
            }
        }
    }
    return NO;
}

/**
 *  登录成功之后调用此方法回去自己的资料，设置faceuurl和nicknema也会调用
 */
- (void)getSelfProfile
{
    [[VLSIMManager sharedInstance] getSelfProfile:^(TIMUserProfile *profile) {
        VLSUserProfile *userprofile = [[VLSUserProfile alloc]init];
        [userprofile GetUserProfileFromTIMUserProfile:profile];
        self.host = userprofile;
        NSLog(@"获取个人信息成功%@",userprofile);
    } fail:^(int code, NSString *msg) {
        NSLog(@"获取个人信息失败%d:%@",code,msg);
    }];
}

//
- (void)getAnchorProfile:(NSString *)anchorId succ:(VLSGetProfileSucc)succ;
{
    [[VLSIMManager sharedInstance] getUserProfile:anchorId succ:^(VLSUserProfile *profile) {
        self.anchor = profile;
        succ (profile);
    } fail:^(int code, NSString *msg) {
        
    }];
}

/**
 *  查询userID的资料
 *  userID装配成数组，使用TIMFriend查询资料
 *  @param userId 成功回调
 *  @param succ   失败回调
 */
- (void)getUserProfile:(NSString*)userId succ:(VLSGetProfileSucc)succ
{
    [[VLSIMManager sharedInstance] getUserProfile:userId succ:^(VLSUserProfile *profile) {
        succ(profile);
    } fail:^(int code, NSString *msg) {
        
    }];
}

/**
 *  登录操作，自动从偏好设置中取得参数登录，在操作失败的统一回调里，也会调用此方法，
 */
- (void)IMLogin
{
    
    
    NSString *identifier =[NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
    NSString *usersig = [AccountManager sharedAccountManager].account.tenderSig;
    
    if (identifier && usersig) {
        
        [self loginIMidentifier:identifier userSig:usersig succ:^{
            [self getSelfProfile];
            
//            NSLog(@"登录成功");
        } fail:^(int code, NSString *msg) {
//            NSLog(@"%d:%@",code,msg);
            if (code == 6208) { //代表账号已在另外设备上登录
                [(AppDelegate *)[UIApplication sharedApplication].delegate ShowAlterView:LocalizedString(@"CHECKING_LOGIN")];
            }
        }];
        
    }else{
        return;
    }
}

/**
 *  进行登录（包括第三方登录操作）使用这个方法
 */
- (void)loginAndIMLogin
{
    
    
    NSString *identifier =[NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
    NSString *usersig = [AccountManager sharedAccountManager].account.tenderSig;
    
    if (identifier && usersig) {
        
        [self loginIMidentifier:identifier userSig:usersig succ:^{
            [self getSelfProfile];
            
            NSLog(@"登录成功");
        } fail:^(int code, NSString *msg) {
            NSLog(@"%d:%@",code,msg);
            AppDelegate *appdele = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appdele showLoginController];
        }];
        
    }else{
        return;
    }
}

//登录IM
- (void)loginIMidentifier:(NSString *)identifier userSig:(NSString *)userSig succ:(VLSSucc)succ fail:(VLSFail)fail
{
    [[VLSIMManager sharedInstance] loginWithidentifier:identifier accountType:@"" userSig:userSig succ:^{
        succ();
    } fail:^(int code, NSString *msg) {
        fail(code, msg);
    }];
}

/**
 *  主播创建直播间
 *
 *  @param RoomName 描述
 *  @param RoomID   RoomID
 *  @param succ     成功回调
 *  @param fail     失败回调
 */
- (void)CreatAVChatRoomGroup:(NSString *)RoomName RoomID:(NSString *)RoomID succ:(VLSCreateGroupSucc)succ fail:(VLSFail)fail
{
    [[VLSIMManager sharedInstance] CreatAVChatRoomGroup:RoomName RoomID:RoomID succ:^(NSString *groupId) {
        succ(groupId);
    } fail:^(int code, NSString *msg) {
        fail(code,msg);
    }];
}

/**
 *  主播解散直播间
 *  @param succ 成功回调
 *  @param fail 失败回调
 */
- (void)deleteAVChartRoomGroupsucc:(VLSSucc)succ fail:(VLSFail)fail
{
    [[VLSIMManager sharedInstance] DeleteGroupSucc:^{
        succ();
    } fail:^(int code, NSString *msg) {
        fail(code, msg);
    }];
}

/**
 *  主播设置管理员
 */
- (void)setAdminToGtoup:(NSString *)userId
{
    NSLog(@"%@被授予了管理员身份",userId);
}
/**
 *  主播取消管理员
 */
- (void)cancleAdminToGtoup:(NSString *)userId
{
    NSLog(@"%@被取消了管理员身份",userId);
}
/**
 *  有人进入房间时推送的消息
 */
- (void)sendJoinRoom
{
    int num;
    if ([AccountManager sharedAccountManager].account.isSuperUser) {
        num = 2;
    }else{
        num = 1;
    }
    
    NSString *str = [NSString stringWithFormat:@"%d&%d",VLS_JOIN_ROOM,num];
    [self sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        
    } fail:^(int code, NSString *msg) {
        
    }];
    
}
/**
 *  有人退出房间时推送的消息
 */
- (void)sendQuitRoomsucc:(VLSSucc)succ fail:(VLSFail)fail
{
    NSString *str = [NSString stringWithFormat:@"%d&%d",VLS_QUIT_ROOM,1];
    [self sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        succ();
    } fail:^(int code, NSString *msg) {
        fail(code,msg);
    }];
    
}

/**
 *  观众加入到直播间
 *  首先加入到直播间，成功之后发送加入直播间消息，消息发送成功之后，因为自己不会收到消息，成功之后调用代理成员加入方法刷新成员列表
 *  @param RoomID RoomID
 *  @param succ   成功回调
 *  @param fail   失败回调
 */
- (void)joinGroupRoomID:(NSString *)RoomID succ:(VLSSucc)succ fail:(VLSFail)fail
{
    [[VLSIMManager sharedInstance] JoinGroupRoomID:RoomID Succ:^{
        succ();
        [self sendJoinRoom];
        
    } fail:^(int code, NSString *msg) {
        fail(code,msg);
    }];
}

/**
 *  观众退出直播间
 *  首先广播我要退出直播间，消息发送成功之后（因为自己不会收到消息，成功之后调用代理成员退出方法刷新成员列表）首先返回直播间列表，然后退出直播间操作不管成功与否
 *  @param succ 成功回调
 *  @param fail 失败回调
 */
- (void)quitAVChatRoomGroupsucc:(VLSSucc)succ fail:(VLSFail)fail
{
    [self sendQuitRoomsucc:^{
        
    } fail:^(int code, NSString *msg) {
        
    }];
    [[VLSIMManager sharedInstance] QuitAVChatRoomGroupsucc:^{
        _delegate = nil;
        succ();
    } fail:^(int code, NSString *msg) {
        fail(code,msg);
    }];
    
    
}

- (void)sendBlack:(BOOL)black toUser:(NSString *)toUserId
{
    NSString *str = [NSString stringWithFormat:@"%d&%d",VLS_BLACK,black?1:0];
    [self sendC2CMsg:str to:toUserId type:SendMsgTypeCustom succ:^{
        
    } fail:^(int code, NSString *msg) {
        
    }];
}

//发送私信w
- (void)sendC2CMessage:(VLSSendC2CMsgModel *)sendModel;
{
    
    //   NSString *message = sendModel.message;
    
}
//发送群消息
- (void)sendGroupMessage:(VLSSendGroupMsgModel *)sendModel success:(void (^)(id result))success error:(void (^)(int code, NSString *msg))error
{
    NSString *message = sendModel.message;
    [self sendGroupMsg:message type:SendMsgTypeText succ:^{
        success(nil);
    } fail:^(int code, NSString *msg) {
        error(code, msg);
    }];
    
}
//发送礼物消息
- (void)sendGiftMessage:(VLSSendGiftModel *)sendModel;
{
    NSInteger giftId = sendModel.giftId;
    NSInteger giftCount  = sendModel.giftCount;
    NSString *str = [NSString stringWithFormat:@"%d&%ld&%ld&%@&%@",VLS_GIFT,(long)giftId,(long)giftCount,self.anchor.userId,[self getRoomId]];
    [self sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        
    } fail:^(int code, NSString *msg) {
        
    }];
    
}
//发送弹幕消息
- (void)sendBarrageMessage:(VLSSendGroupMsgModel *)sendModel;
{
    NSString *message = sendModel.message;
    NSString *str = [NSString stringWithFormat:@"%d&%@",VLS_TM,message];
    [self sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        
    } fail:^(int code, NSString *msg) {
        
    }];
}

- (void)sendPraiseMessage:(NSInteger)count success:(void(^)())success fail:(void(^)(int code, NSString *msg))fail
{
    NSString *str = [NSString stringWithFormat:@"%d&1000&%ld&%@&%@",VLS_PRAISE,(long)count,self.anchor.userId,[self getRoomId]];
    [self sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        success();
    } fail:^(int code, NSString *msg) {
        fail(code, msg);
    }];
}

- (void)sendPraiseMessage:(NSInteger)count sendModel:(VLSSendGroupMsgModel *)sendModel
{
    NSString *str = [NSString stringWithFormat:@"%d&%@",VLS_SEND_PRAISE, sendModel.message];
    [self sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        
    } fail:^(int code, NSString *msg) {
        
    }];
//    //用户点赞完成，及时在弹幕屏显示
//    TIMTextElem * textElem = [[TIMTextElem alloc] init];
//    [textElem setText:sendModel.message];
//    [self onReceiveGroupNormalMessage:textElem senderProfile:self.host messageType:TYPE_SEND_PRAISE];
}

/**
 *  获取成员列表
 *
 *  @param succ 成功会掉
 *  @param fail 失败回调
 */
- (void)getMenberListsucc:(VLSGroupMemberSucc)succ fail:(VLSFail)fail
{
    [[VLSIMManager sharedInstance] getMenberListsucc:^(NSArray *members) {
        
        NSMutableArray *array = [NSMutableArray array];
        for (TIMUserProfile *User in members)
        {
            VLSUserProfile *userprofile = [[VLSUserProfile alloc]init];
            [userprofile GetUserProfileFromTIMUserProfile:User];
            [array addObject:userprofile];
        }
        succ(array);
    } fail:^(int code, NSString *msg) {
        fail(code,msg);
    }];
    
}
/**
 *  获取当前用户是否登录状态
 *
 *  @return 登录则返回用户ID，失败则返回nil
 */
- (BOOL)GetLoginState
{
    return [[VLSIMManager sharedInstance] GetLoginState];
}

/**
 *  IM退出操作
 */
- (void)logoutIM
{
    return [[VLSIMManager sharedInstance]logoutIM];
}
/**
 *  设置昵称
 *
 *  @param name void
 */
- (void)setNikename:(NSString *)name
{
    [[VLSIMManager sharedInstance] setNikename:name succ:^{
        [self getSelfProfile];
    } fail:^(int code, NSString *msg) {
        
    }];
}

/**
 *  设置头像URL
 *
 *  @param faceUrl void
 */
- (void)setFaceUrl:(NSString *)faceUrl
{
    [[VLSIMManager sharedInstance] setFaceUrl:faceUrl succ:^{
        [self getSelfProfile];
    } fail:^(int code, NSString *msg) {
        
    }];
}

- (void)getGroupInfoSucc:(VLSGroupInfoSucc)succ fail:(VLSFail)fail
{
    [[VLSIMManager sharedInstance] getGroupInfoSucc:^(VLSGroupInfo *info) {
        succ (info);
    } fail:^(int code, NSString *msg) {
        
    }];
}

- (void)sendUpdateVTickets:(NSString *)vTicket succ:(VLSSucc)succ fail:(VLSFail)fail
{
    NSLog(@"====== send update ticket: %@", [NSDate date]);
    NSString *str = [NSString stringWithFormat:@"%d&%@",VLS_VTICKETS_UPDATED, vTicket];
    [self sendGroupMsg: str type: SendMsgTypeCustom succ: succ fail: fail];
}


#pragma mark - 收到信息
- (void)receiveNewMessage:(NSArray *)messages
{
    NSMutableArray* groupMessages = [NSMutableArray array];
    NSString* roomId = kGetRoomId;
    for(TIMMessage* msg in messages)
    {
        if (msg.getConversation.getType == TIM_C2C)
        {
            [self onReceiveC2C:msg];
        } else if ([msg.getConversation.getReceiver isEqualToString:roomId] || [msg.getConversation.getReceiver isEqualToString:@""]) {//解决上一个直播间IM消息异常退出，导致进入下一个直播间引起多个IM消息混淆bug
            [groupMessages addObject: msg];
        }
    }
    if (groupMessages.count > 0)
    {
        [self onReceiveGroupMessages: groupMessages];
    }
}



#pragma mark - 发送信息
- (void)sendC2CMsg:(NSString *)msg to:(NSString *)userId type:(SendMsgType)type succ:(VLSSucc)succ fail:(VLSFail)fail;
{
    TIMCustomElem *elem = [[TIMCustomElem alloc] init];
    elem.data = [msg dataUsingEncoding:NSUTF8StringEncoding];
    
    TIMMessage *timMsg = [[TIMMessage alloc] init];
    [timMsg addElem:elem];
    
    [[VLSIMManager sharedInstance] sendC2CChatMessage:timMsg receiver:userId succ:^{
        NSLog(@"申请成功");
        succ();
    } fail:^(int code, NSString *msg) {
        NSLog(@"申请失败");
        fail(code, msg);
    }];
    
}
/**
 *  发送群组消息
 *  如果发送普通消息，首先调用代理方法显示在view中，再发送广播消息以提高用户体验。如果发送自定义消息，如加入退出礼物弹幕，广播成功以后，调用代理方法显示在view上
 *  @param msg  消息内容
 *  @param type 消息类型：自定义，普通
 *  @param succ 成功回调
 *  @param fail 失败回调
 */
- (void)sendGroupMsg:(NSString *)msg type:(SendMsgType)type succ:(VLSSucc)succ fail:(VLSFail)fail
{
    TIMMessage * message = [[TIMMessage alloc] init];
    if (type == SendMsgTypeText) {
        TIMTextElem * textElem = [[TIMTextElem alloc] init];
        [textElem setText:msg];
        [message addElem:textElem];
        
    }else if (type == SendMsgTypeCustom){
        TIMCustomElem *customElem = [[TIMCustomElem alloc] init];
        [customElem setData:[msg dataUsingEncoding:NSUTF8StringEncoding]];
        [message addElem:customElem];
    }
    if (type == SendMsgTypeText) {
        [self onReceiveGroupNormalMessage:(TIMTextElem *)[message getElem:0] senderProfile:self.host];
    }
    //针对群组消息设置优先级
    TIMMessage* newMessage = [VLSIMExtendManager setPriorityWithType:type msg:msg imMessage:message];
    
    [[VLSIMManager sharedInstance] sendChatRoomMessage:newMessage succ:^{
        if (type == SendMsgTypeCustom){
            // 如果是发送点赞消息成功，则不回调
            NSArray *dataarray = [msg componentsSeparatedByString:VLS_SEPERATOR];
            int cmd = [dataarray[0] intValue];
            if (cmd == VLS_PRAISE) {
                return ;
            }
            [self onReceiveGroupCustomMessage:(TIMCustomElem *)[message getElem:0] senderProfile:self.host generatedNormalMessage: nil];
        }
        succ();
        NSLog(@"发送群组消息成功");
    } fail:^(int code, NSString *msg) {
        fail(code,msg);
        NSLog(@"发送群组消息失败");
    }];
}

#pragma mark - 解析群组信息

- (void)onReceiveGroup:(TIMMessage *)msg
{
    //发送人信息
    TIMUserProfile *senderProfile = [msg GetSenderProfile];
    NSString *userId = senderProfile.identifier;
    NSString *nikeName = senderProfile.nickname;
    NSString *faceURL = senderProfile.faceURL;
    
    VLSUserProfile * user = [[VLSUserProfile alloc] init];
    user.userId = userId;
    user.userName = nikeName;
    user.userFaceURL = faceURL;
    
    for(int index=0;index<[msg elemCount];index++)
    {
        TIMElem* elem = [msg getElem:index];
        
        if ([elem isKindOfClass:[TIMGroupTipsElem class]]) {
            //在群内的群事件
            TIMGroupTipsElem * tips_elem = (TIMGroupTipsElem * )elem;
            [self onReceiveGroupTipsMessage:tips_elem senderProfile:user];
        }else if ([elem isKindOfClass:[TIMGroupSystemElem class]]){
            //群系统消息
            TIMGroupSystemElem *sys_elem = (TIMGroupSystemElem *)elem;
            [self onReceiveGroupSystemMessage:sys_elem senderProfile:user];
        }else if([elem isKindOfClass:[TIMTextElem class]]){
            //文本消息
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSLog(@"dispatch_async");
                TIMTextElem* textElem = (TIMTextElem*)elem;
                [self onReceiveGroupNormalMessage:textElem senderProfile:user];
            });
        }
        else if([elem isKindOfClass:[TIMCustomElem class]]){
            //
            TIMCustomElem* customElem = (TIMCustomElem*)elem;
            [self onReceiveGroupCustomMessage:customElem senderProfile:user generatedNormalMessage: nil];
        }
        
    }
}

- (void)onReceiveGroupMessages: (NSArray<TIMMessage*>*)messages
{
    __block NSMutableArray<VLSMessageViewModel*>* textMessageArray = [NSMutableArray<VLSMessageViewModel*> array];
    
    [messages enumerateObjectsUsingBlock:^(TIMMessage * _Nonnull msg, NSUInteger idx, BOOL * _Nonnull stop)
    {
        
        TIMUserProfile *senderProfile = [msg GetSenderProfile];
        NSString *userId = senderProfile.identifier;
        NSString *nikeName = senderProfile.nickname;
        NSString *faceURL = senderProfile.faceURL;
        
        VLSUserProfile * user = [[VLSUserProfile alloc] init];
        user.userId = userId;
        user.userName = nikeName;
        user.userFaceURL = faceURL;
        
        for(int index=0;index<[msg elemCount];index++)
        {
            TIMElem* elem = [msg getElem:index];
            
            if ([elem isKindOfClass:[TIMGroupTipsElem class]]) {
                //在群内的群事件
                TIMGroupTipsElem * tips_elem = (TIMGroupTipsElem * )elem;
                [self onReceiveGroupTipsMessage:tips_elem senderProfile:user];
            }else if ([elem isKindOfClass:[TIMGroupSystemElem class]]){
                //群系统消息
                TIMGroupSystemElem *sys_elem = (TIMGroupSystemElem *)elem;
                [self onReceiveGroupSystemMessage:sys_elem senderProfile:user];
            }else if([elem isKindOfClass:[TIMTextElem class]]){
                //文本消息
                TIMTextElem* textElem = (TIMTextElem*)elem;
                if (user.userId) {
                    NSString *message = textElem.text;
                    VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:message types:TYPE_NORMALMESSAGE];
                    [textMessageArray addObject: messageVM];
                }
            }
            else if([elem isKindOfClass:[TIMCustomElem class]]){
                //
                TIMCustomElem* customElem = (TIMCustomElem*)elem;
                VLSMessageViewModel *messageVM;
                [self onReceiveGroupCustomMessage:customElem senderProfile:user generatedNormalMessage: &messageVM];
                if (messageVM)
                {
                    [textMessageArray addObject: messageVM];
                }
                
                NSString *str = [[NSString alloc] initWithData:customElem.data encoding:NSUTF8StringEncoding];
                NSArray *ary = [str componentsSeparatedByString:VLS_SEPERATOR];
                if (ary.count > 0) {
                    NSString *type = [ary firstObject];
                    if ([type integerValue] == VLS_GIFT) {
                        //礼物IM
                        user.userId = [ary objectAtIndex:2];
                        user.userFaceURL = [ary objectAtIndex:4];
                        user.userName = [ary objectAtIndex:5];
                        NSString *message = [ary objectAtIndex:6];
                        VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:message types:TYPE_NORMALMESSAGE];
                        
                        [textMessageArray addObject: messageVM];
                    }
                }
                
            }
        }
        
    }];

    if ([_delegate respondsToSelector: @selector(receiveNormalMessage:)])
    {
        [_delegate receiveNormalMessage: textMessageArray];
    }
}


#pragma mark - GroupNormalMessage
- (void)onReceiveGroupNormalMessage:(TIMTextElem *)elem senderProfile:(VLSUserProfile*)user
{
    [self onReceiveGroupNormalMessage:elem senderProfile:user messageType:TYPE_NORMALMESSAGE];
}

#pragma mark - Custom GroupNormalMessage
- (void)onReceiveGroupNormalMessage:(TIMTextElem *)elem senderProfile:(VLSUserProfile*)user messageType:(NSInteger) type
{
    if (user.userId) {
        NSString *message = elem.text;
        VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:message types:type];
        //        [self.normalMessages addObject:messageVM];
        dispatch_async(dispatch_get_main_queue(), ^{
            //            [self.normalMessages removeObjectsInArray:temp];
            //回调或者说是通知主线程刷新，
            NSLog(@"callback");
            if ([_delegate respondsToSelector:@selector(receiveNormalMessage:)]) {
                [_delegate receiveNormalMessage:[[NSMutableArray alloc] initWithObjects:messageVM, nil]];
            }
        });
    }
}

#pragma mark - GroupCustomMessage
- (void)onReceiveGroupCustomMessage:(TIMCustomElem *)elem senderProfile:(VLSUserProfile*)user generatedNormalMessage: (VLSMessageViewModel**)generatedNormalMessage
{
    //自定义消息
    NSString* data = [[NSString alloc] initWithData:elem.data encoding:NSUTF8StringEncoding];
    NSArray* array = [data componentsSeparatedByString:VLS_SEPERATOR];
    if(array.count == 0){
        return;
    }
    //NSLog(@"%@",elem.data);
    int cmd = [array[0] intValue];
    
    switch (cmd) {
        case VLS_GIFT:
            //礼物
            [[VLSGiftManager sharedManager] receiveGiftMessage:array];
            //            if(array.count == 4){
            //                NSString * giftCount = array[2];
            //                NSString * giftId = array[1];
            //                NSMutableArray *giftArray = [NSMutableArray arrayWithCapacity:0];
            //                VlSGiftModel *giftModel = [VlSGift getGiftWidthGiftId:giftId];
            //                if (giftModel.giftShowType == GiftShowTypeDefault) {
            //                    //小礼物
            //                    VLSNormalGiftViewModel *vm = [[VLSNormalGiftViewModel alloc] initWidthUser:user gift:giftModel];
            //                    VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:vm.message types:TYPE_GIFT];
            //                    for (int i = 0; i<giftCount.integerValue; i++) {
            //                        [giftArray addObject:vm];
            //                    }
            ////                    [self.normalMessages addObject:messageVM];
            //                    if ([_delegate respondsToSelector:@selector(receiveNormalMessage:)]) {
            //                        [_delegate receiveNormalMessage:[[NSMutableArray alloc] initWithObjects:messageVM, nil]];
            //                    }
            //                    if ([_delegate respondsToSelector:@selector(receiveNormalGiftMessage:)]) {
            //                        [_delegate receiveNormalGiftMessage:giftArray];
            //                    }
            //                }else{
            //                    //大礼物
            //
            //                }
            //            }
            break;
        case VLS_PRAISE:
            //赞
            if(array.count == 5){
                NSInteger count = [array[2] integerValue];
                if ([_delegate respondsToSelector:@selector(receivePariseMessage:)]) {
                    [_delegate receivePariseMessage:count];
                }
            }
            break;
        case VLS_TM:
            //弹幕
            if (array.count == 2) {
                NSString *message = array[1];
                VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:message types:TYPE_BARRAGE];
                VLSBarrageViewModel *barrageVM = [[VLSBarrageViewModel alloc] initWidthUser:user message:message];
                //                [self.normalMessages addObject:messageVM];
                
                if (generatedNormalMessage == nil)
                {
                    if ([_delegate respondsToSelector:@selector(receiveNormalMessage:)]) {
                        [_delegate receiveNormalMessage:[[NSMutableArray alloc] initWithObjects:messageVM, nil]];
                    }
                }
                else
                {
                    *generatedNormalMessage = messageVM;
                }

                if ([_delegate respondsToSelector:@selector(receiveBarrageMessage:)]) {
                    [_delegate receiveBarrageMessage:[[NSMutableArray alloc] initWithObjects:barrageVM, nil]];
                }
            }
            break;
        case VLS_JOIN_ROOM:{
            if (_delegate) {
                if ([_delegate respondsToSelector:@selector(receiveAddMembers:isRoot:)]) {
                    [_delegate receiveAddMembers:@[user] isRoot:NO];
                }
            }
            NSInteger type;
            BOOL discardMessage = NO;
            if (array.count>1)
            {
                
                if ([array[1] isEqualToString:@"1"]
                    || [self.lessonId length] > 0 //即使是超管，进入课程房间也不提示超管身份
                    ) {
                    
                    type = TYPE_JOINOROUT;
                    
                    // 课程直播不显示“XXX进来了”
                    discardMessage = self.disableJoinRoomMessage;
                }else{
                    
                    type = TYPE_SUPERJOIN;
                    
                }
                
            }else
            {
                NSLog(@"数据格式异常");
            }
            
            if (!discardMessage)
            {
                VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:LocalizedString(@"COMEIN") types:type];
                //            [self.normalMessages addObject:messageVM];
                if (generatedNormalMessage == nil)
                {
                    if ([_delegate respondsToSelector:@selector(receiveNormalMessage:)]) {
                        [_delegate receiveNormalMessage:[[NSMutableArray alloc] initWithObjects:messageVM, nil]];
                    }
                }
                else
                {
                    *generatedNormalMessage = messageVM;
                }
            }

        }
            break;
        case VLS_QUIT_ROOM:{
            /**
             *  当走到这段代码的时候，有两种情况，一种是别人退出，一种是自己退出，自己退出时走完这段代码pop视图控制器，然后退出群组操作不管成功与否
             */
            if (_delegate) {
                if ([_delegate respondsToSelector:@selector(receiveDeleteMembers:)]) {
                    [_delegate receiveDeleteMembers:@[user]];
                }
            }
        }
            break;
            
            
        case VLS_LIVE_COUNT:{
            
            if (_delegate) {
                //在线人数
                if (array[1]) {
                    if ([_delegate respondsToSelector:@selector(receiveUpdateMemberCount:)]) {
                        [_delegate receiveUpdateMemberCount:array[1]];
                    }
                }
                // V票
//                if (array[2]) {
//                    if ([_delegate respondsToSelector:@selector(receiveUpdateTicket:)]) {
//                        [_delegate receiveUpdateTicket:array[2]];
//                    }
//                }
                
            }
        }
            break;
        case VLS_ROOT_JOIN_ROOM:{
            
            if (!user.userId || !user.userName) {
                // 如果机器人是未注册的，不再回调
                return;
            }
            
            if (_delegate) {
                if ([_delegate respondsToSelector:@selector(receiveAddMembers:isRoot:)]) {
                    [_delegate receiveAddMembers:@[user] isRoot:YES];
                }
            }
            
            VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:LocalizedString(@"COMEIN") types:TYPE_JOINOROUT];
            //[self.normalMessages addObject:messageVM];
            if (generatedNormalMessage == nil)
            {
                if ([_delegate respondsToSelector:@selector(receiveNormalMessage:)]) {
                    [_delegate receiveNormalMessage:[[NSMutableArray alloc] initWithObjects:messageVM, nil]];
                }
            }
            else
            {
                *generatedNormalMessage = messageVM;
            }

        }
            break;
        case VLS_SYSTERM_MSG:{
            
            if (array.count>=2) {
                NSString *msg = array[1];
                if (msg) {
                    VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:msg types:TYPE_GREENMESSAGE];
                    [self.normalMessages addObject:messageVM];
                    
                    if ([_delegate respondsToSelector:@selector(receiveNormalMessage:)]) {
                        [_delegate receiveNormalMessage:[[NSMutableArray alloc] initWithObjects:messageVM, nil]];
                    }
                }
            }
        }
            break;
            
        case VLS_GUEST_CANCEL:{
            //100+
            //嘉宾取消连线 （嘉宾成功连线）
            BOOL contain = NO;
            for (int i=0; i<self.liveAnchors.count; i++) {
                VLSVideoViewModel *model = [self.liveAnchors objectAtIndex:i];
                if ([model.userId isEqualToString:user.userId]) {
                    [self.liveAnchors removeObject:model];
                    contain = YES;
                }
            }
            //自己取消
            if ([self checkUserIsSelf:user.userId]) {
                if ([_delegate respondsToSelector:@selector(receiveGuestCancelLink)]) {
                    [VLSAVMultiManager sharedManager].linkStatus = UserLinkStatusAudience;
                    [_delegate receiveGuestCancelLink];
                    
                }
            }
            if ([_delegate respondsToSelector:@selector(receiveGuestCancelLinkRequest:)]) {
                [_delegate receiveGuestCancelLinkRequest:user];
            }
            if (contain) {
                [self checkMainGuestWasCancel];
                if ([_delegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
                    [_delegate receiveUpdateLiveLayout:self.liveAnchors];
                }
            }
        }
            break;
        case VLS_ANCHOR_CANCEL:{
            //主播取消连线消息
            NSString *str = [array objectAtIndex:1];
            NSArray *ids = [str componentsSeparatedByString:@","];
            BOOL contain = NO;
            NSMutableArray *temp = [NSMutableArray arrayWithCapacity:0];
            for (int i=0; i<self.liveAnchors.count; i++) {
                VLSVideoViewModel *model = [self.liveAnchors objectAtIndex:i];
                if ([ids containsObject:model.userId]) {
                    [temp addObject:model];
                    contain = YES;
                }
            }
            [self.liveAnchors removeObjectsInArray:temp];
            
            //自己被取消连线
            if ([ids containsObject:self.host.userId]) {
                if ([_delegate respondsToSelector:@selector(receiveLinkRequestCancel)]) {
                    [VLSAVMultiManager sharedManager].linkStatus = UserLinkStatusAudience;
                    [_delegate receiveLinkRequestCancel];
                }
            }
            for (NSString *uid in ids) {
                VLSUserProfile *userprofile = [[VLSUserProfile alloc] init];
                userprofile.userId = uid;
                if ([_delegate respondsToSelector:@selector(receiveGuestCancelLinkRequest:)]) {
                    [_delegate receiveGuestCancelLinkRequest:userprofile];
                }
            }
            
            if (contain) {
                [self checkMainGuestWasCancel];
                if ([_delegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
                    [_delegate receiveUpdateLiveLayout:self.liveAnchors];
                }
            }
        }
            break;
        case VLS_ANCHOR_REPLACE:{
            //替换位置消息
            NSString *fromId = [array objectAtIndex:1];
            NSString *toId = [array objectAtIndex:2];
            NSInteger index1 = 0;
            NSInteger index2 = 0;
            for (int i=0; i<self.liveAnchors.count; i++) {
                VLSVideoViewModel *model = [self.liveAnchors objectAtIndex:i];
                if ([model.userId isEqualToString:fromId]) {
                    index1 = i;
                }
                if ([model.userId isEqualToString:toId]) {
                    index2 = i;
                }
            }
            [self.liveAnchors exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
            
            VLSVideoViewModel *viewModel = [self.liveAnchors firstObject];
            if (viewModel.userType == UserTypeHost) {
                mainGuestViewModel = nil;
                if ([_delegate respondsToSelector:@selector(receiveUpdataAnchorView:)]) {
                    [_delegate receiveUpdataAnchorView:nil];
                }
            }else{
                mainGuestViewModel = viewModel;
                if ([_delegate respondsToSelector:@selector(receiveUpdataAnchorView:)]) {
                    [_delegate receiveUpdataAnchorView:viewModel];
                }
            }
            if ([_delegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
                [_delegate receiveUpdateLiveLayout:self.liveAnchors];
            }
        }
            break;
        case VLS_ANCHOR_MUTE:{
            //主播静音消息
            NSString *str1 = [array objectAtIndex:1];
            NSString *str2 = [array objectAtIndex:2];
            NSArray *ids = [str1 componentsSeparatedByString:@","];
            NSArray *statues = [str2 componentsSeparatedByString:@","];
            for (VLSVideoViewModel *model in self.liveAnchors) {
                if ([ids containsObject:model.userId]) {
                    model.muteVoiceBySelf = NO;
                    model.muteVoice = [[statues objectAtIndex:[ids indexOfObject:model.userId]] boolValue];
                    if ([self checkUserIsSelf:model.userId]) {
                        if ([_delegate respondsToSelector:@selector(receiveAnchorMuteMic:)]) {
                            [_delegate receiveAnchorMuteMic:model.muteVoice];
                        }
                    }
                }
            }
            if ([_delegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
                [_delegate receiveUpdateLiveLayout:self.liveAnchors];
            }
        }
            break;
        case VLS_GUEST_MUTE_SELF:{
            //嘉宾静音消息
            BOOL mute = [[array objectAtIndex:1] boolValue];
            for (VLSVideoViewModel *model in self.liveAnchors) {
                if ([model.userId isEqualToString:user.userId]) {
                    model.muteVoice = mute;
                    model.muteVoiceBySelf = YES;
                }
            }
            if ([_delegate respondsToSelector:@selector(receiveGuestMuteSelf:)]) {
                [_delegate receiveGuestMuteSelf:mute];
            }
            if ([_delegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
                [_delegate receiveUpdateLiveLayout:self.liveAnchors];
            }
        }
            break;
        case VLS_GUEST_ON_CAMERA:{
            //嘉宾立即上镜
            BOOL canOnCamera = YES;
            for (VLSVideoViewModel *model in self.liveAnchors) {
                if ([model.userId isEqualToString:user.userId]) {
                    canOnCamera = NO;
                }
            }
            if (canOnCamera) {
                VLSVideoViewModel *model = [[VLSVideoViewModel alloc] init];
                model.userId = user.userId;
                model.userType = UserTypeGuest;
                model.muteVideo = NO;
                model.muteVoice = NO;
                model.showLoading = YES;
                model.muteVoiceBySelf = NO;
                [self.liveAnchors addObject:model];
                if ([self checkUserIsSelf:user.userId]) {
                    if ([_delegate respondsToSelector:@selector(receiveLinkOnCamera:)]) {
                        [VLSAVMultiManager sharedManager].linkStatus = UserLinkStatusOnCamera;
                        [_delegate receiveLinkOnCamera:user];
                    }
                }
                if ([_delegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
                    [_delegate receiveUpdateLiveLayout:self.liveAnchors];
                }
            }
        }
            break;
            
        case VLS_VTICKETS_UPDATED:
            if (_delegate && array.count > 0 && [_delegate respondsToSelector: @selector(receiveUpdateTicket:)])
            {
                [_delegate receiveUpdateTicket: array[1]];
            }
            break;
        case VLS_SEND_PRAISE:
            //点亮了屏幕
            if(array.count == 2){
                if ([_delegate respondsToSelector:@selector(receivePariseMessage:)]) {
                    [_delegate receivePariseMessage:1];
                }
                VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:array[1] types:TYPE_SEND_PRAISE];
                if ([_delegate respondsToSelector:@selector(receiveNormalMessage:)]) {
                    [_delegate receiveNormalMessage:[[NSMutableArray alloc] initWithObjects:messageVM, nil]];
                }
            }
            break;
        case VLS_FORBIDSENDMSG: {
            //被禁言
            if (array.count >= 4 && [_delegate respondsToSelector:@selector(receiveNormalMessage:)] && [_delegate respondsToSelector:@selector(receiveForbidSendMsg:)]) {
                user.userId = array[2];
                user.userName = array[3];
                VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:user message:nil types:TYPE_FORBIDSENDMSG];
                [_delegate receiveNormalMessage:[[NSMutableArray alloc] initWithObjects:messageVM, nil]];
                [_delegate receiveForbidSendMsg:@[messageVM]];
            }
        }
            break;
        case VLS_FORBIDSENDMSG_CANCEL: {
            //禁言取消
            if (array.count > 0 && [_delegate respondsToSelector:@selector(receiveForbidSendMsgCancel:)]) {
                [_delegate receiveForbidSendMsgCancel:array];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark - ReceiveC2C
- (void)onReceiveC2C:(TIMMessage *)msg
{
    //发送人信息
    TIMUserProfile *senderProfile = [msg GetSenderProfile];
    NSString *userId = senderProfile.identifier;
    NSString *nikeName = senderProfile.nickname;
    NSString *faceURL = senderProfile.faceURL;
    
    VLSUserProfile * user = [[VLSUserProfile alloc] init];
    user.userId = msg.sender;
    // 未处理C2C文本消息
    for(int index = 0; index < [msg elemCount]; index++)
    {
        TIMElem *elem = [msg getElem:index];
        if([elem isKindOfClass:[TIMTextElem class]])
        {
            //               //消息
            //               TIMTextElem *textElem = (TIMTextElem *)elem;
            //               NSString *msgText = textElem.text;
        }
        // 只处理C2C自定义消息，不处理其他类型聊天消息
        if([elem isKindOfClass:[TIMCustomElem class]])
        {// 自定义消息
            //               [self onRecvC2CSender:profile customMsg:(TIMCustomElem *)elem];
            TIMCustomElem* customElem = (TIMCustomElem*)elem;
            NSString* data = [[NSString alloc] initWithData:customElem.data encoding:NSUTF8StringEncoding];
            NSArray* array = [data componentsSeparatedByString:VLS_SEPERATOR];
            if(array.count == 0){
                return;
            }
            int cmd = [array[0] intValue];
            switch (cmd) {
                case VLS_BLACK:{
                    //拉黑
                    if ([_delegate respondsToSelector:@selector(receiveSendToBlacklist)]){
                        [_delegate receiveSendToBlacklist];
                    }
                }
                    break;
                case VLS_ANCHOR_REJECT:{
                    //拒绝消息 101+
                    
                }
                    break;
                case VLS_GUEST_REQUEST:{
                    //收到连线请求
                    if ([_delegate respondsToSelector:@selector(receiveLinkRequest:)]){
                        [_delegate receiveLinkRequest:user];
                    }
                }
                    break;
                case VLS_GUEST_RQT_CANCEL:{
                    //取消连线 （嘉宾连线等待）
                    if ([_delegate respondsToSelector:@selector(receiveGuestCancelLinkRequest:)]){
                        [_delegate receiveGuestCancelLinkRequest:user];
                    }
                }
                    break;
                case VLS_ANCHOR_ACCEPT:{
                    //主播接受连线
                    if ([_delegate respondsToSelector:@selector(receiveLinkRequestAccept)]) {
                        [VLSAVMultiManager sharedManager].linkStatus = UserLinkStatusApplySuccess;
                        [_delegate receiveLinkRequestAccept];
                    }
                }
                    break;
                    
                case VLS_TEAM:{
                    // 收到团队信息通知
                    // 201&之后的字符串为字典
                    NSString *dictstr = [data substringFromIndex:4];
                    NSDictionary *dict = [VLSMessageManager parseJSONStringToNSDictionary:dictstr];
                    
                    [VLSDBHelper ThreadSaveTeamModelToDB:dict TIMMessage:msg elem:customElem completeHandler:^(BOOL isSave) {
                        if (isSave) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (!self.newMail) {
                                    self.newMail = YES;
                                }
                                if ([_delegate respondsToSelector:@selector(receiveMail)]) {
                                    [_delegate receiveMail];
                                }
                            });
                        }
                    }];
                }
                    break;
                    
                case VLS_ANCHOR_INVITE:{
                    //主播邀请嘉宾
                    //显示有提示图标
                    if ([_delegate respondsToSelector:@selector(receiveMail)]) {
                        [_delegate receiveMail];
                    }
                    if ([_delegate respondsToSelector:@selector(anchoInviteWithAnchoPicture:NickName:)]) {
                        [_delegate anchoInviteWithAnchoPicture:faceURL NickName:nikeName];
                    }
                    
                    
                }
                    break;
                case VLS_LIVEREMIND:{
                    // 收到开播提醒
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:LIVEREMIND]) {
                        break ;
                    }
                    
                    NSString *dictstr = [data substringFromIndex:4];
                    NSDictionary *dict = [VLSMessageManager parseJSONStringToNSDictionary:dictstr];
                    [VLSDBHelper ThreadSaveLiveModelToDB:dict TIMMessage:msg elem:customElem completeHandler:^(BOOL isSave) {
                        if (isSave) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (!self.newMail) {
                                    self.newMail = YES;
                                }
                                if ([_delegate respondsToSelector:@selector(receiveMail)]) {
                                    [_delegate receiveMail];
                                }
                            });
                        }
                    }];
                    
                }
                    break;
                case VLS_FANS:{
                    // 收到粉丝关注提醒
                    NSString *dictstr = [data substringFromIndex:4];
                    NSDictionary *dict = [VLSMessageManager parseJSONStringToNSDictionary:dictstr];
                    [VLSDBHelper ThreadSaveFansModelToDB:dict TIMMessage:msg elem:customElem completeHandler:^(BOOL isSave) {
                        if (isSave) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (!self.newMail) {
                                    self.newMail = YES;
                                }
                                if ([_delegate respondsToSelector:@selector(receiveMail)]) {
                                    [_delegate receiveMail];
                                }
                            });
                            
                        }
                        
                    }];
                    
                }
                    break;
                case VLS_ACCOUNT_CLOSURE: {//账号封停
                    NSString* msg = @"";
                    if (array.count == 2) {
                        NSDictionary* content = [VLSMessageManager parseJSONStringToNSDictionary:array[1]];
                        msg = [content objectForKey:@"content"];
                    }
                    [(AppDelegate*)[UIApplication sharedApplication].delegate accountClosure:msg];
                }
                    break;
                default:
                    break;
                    
            }
        }
    }
    
}



#pragma mark - GroupTipsMessage
- (void)onReceiveGroupTipsMessage:(TIMGroupTipsElem *)elem senderProfile:(VLSUserProfile*)user
{
    switch ([elem type]) {
        case TIM_GROUP_TIPS_TYPE_INVITE:{
            //当有用户加入群组时（包括申请入群和被邀请入群），群组内会由系统发出通知，开发者可选择展示样式。可以更新群成员列表。
            //入群的用户列表
            //            NSArray *userList = [elem userList];
            //            NSLog(@"入群:%@",userList);
            //            if ([_delegate respondsToSelector:@selector(receiveAddMembers:)]) {
            //                [_delegate receiveAddMembers:userList];
            //            }
        }
            break;
        case TIM_GROUP_TIPS_TYPE_QUIT_GRP:{
            //当有用户主动退群时，群组内会由系统发出通知。可以选择更新群成员列表。
            //退出用户identifier
            NSString *identifier =[elem opUser];
            NSLog(@"离开群:%@",identifier);
            if ([_delegate respondsToSelector:@selector(receiveDeleteMembers:)]) {
                [_delegate receiveDeleteMembers:nil];
            }
        }
            break;
        case TIM_GROUP_TIPS_TYPE_KICKED:{
            //当有用户被踢时，群组内会由系统发出通知。可以选择更新群成员列表。
            //被踢用户identifier
            NSString *identifier =[elem opUser];
            NSLog(@"被踢出群:%@",identifier);
            if ([_delegate respondsToSelector:@selector(receiveDeleteMembers:)]) {
                [_delegate receiveDeleteMembers:nil];
            }
        }
            break;
        case TIM_GROUP_TIPS_TYPE_SET_ADMIN:{
            //当有用户被设置为管理员身份时，群组内会由系统发出通知。如果界面有显示是否管理员，此时可更新管理员标识。
            //被操作人列表
            NSArray *userList = [elem userList];
            NSLog(@"设置管理员:%@",userList);
            if ([_delegate respondsToSelector:@selector(receiveUpdataMembers:)]) {
                [_delegate receiveUpdataMembers:userList];
            }
        }
            break;
        case TIM_GROUP_TIPS_TYPE_CANCEL_ADMIN:{
            //当有用户被取消管理员身份时，群组内会由系统发出通知。如果界面有显示是否管理员，此时可更新管理员标识。
            //被操作人列表
            NSArray *userList = [elem userList];
            NSLog(@"取消管理员:%@",userList);
            if ([_delegate respondsToSelector:@selector(receiveUpdataMembers:)]) {
                [_delegate receiveUpdataMembers:userList];
            }
        }
            break;
        default:
            NSLog(@"ignore type");
            break;
    }
}

#pragma mark - GroupSystemMessage
- (void)onReceiveGroupSystemMessage:(TIMGroupSystemElem *)elem senderProfile:(VLSUserProfile*)user
{
    //    NSString *groupId = elem.group;
    
    switch ([elem type]) {
        case TIM_GROUP_SYSTEM_DELETE_GROUP_TYPE:{
            //当群被解散时，全员会收到解散群消息。
            //群被解散
            //
            NSString *user = elem.user;
            NSString *msg = elem.msg;
            NSString *roomId = elem.group;
            NSLog(@"群被解散:%@ %@",user,msg);
            if ([roomId isEqualToString:[self getRoomId]]) {
                if (_delegate) {
                    if ([_delegate respondsToSelector:@selector(receiveDeleteGroup)]) {
                        [_delegate receiveDeleteGroup];
                    }
                }
            }
        }
            break;
        case TIM_GROUP_SYSTEM_CREATE_GROUP_TYPE:{
            //当群创建时，创建者会收到创建群消息。
            //当调用创建群方法成功回调后，即表示创建成功，此消息主要为多终端同步，如果有在其他终端登录，做为更新群列表的时机，本终端可以选择忽略。
            //创建群消息（创建者能够收到）
            
        }
            break;
        case TIM_GROUP_SYSTEM_GRANT_ADMIN_TYPE:{
            //当用户被设置为管理员时，可收到被设置管理员的消息通知，当用户被取消管理员时，可收到取消通知，可提示用户。
            //设置管理员(被设置者接收)
            //群ID
            NSString * group = elem.group;
            NSString * user = elem.user;
            NSLog(@"设置管理员:%@ %@",group,user);
            if ([_delegate respondsToSelector:@selector(receiveSetAdminToGroup)]) {
                [_delegate receiveSetAdminToGroup];
            }
            
        }
            break;
        case TIM_GROUP_SYSTEM_CANCEL_ADMIN_TYPE:{
            //取消管理员(被取消者接收)
            //群ID
            NSString * group = elem.group;
            NSString *user = elem.user;
            NSLog(@"取消管理员:%@ %@",group,user);
            if ([_delegate respondsToSelector:@selector(receiveCancleAdminToGroup)]) {
                [_delegate receiveCancleAdminToGroup];
            }
        }
            break;
        case TIM_GROUP_SYSTEM_ADD_GROUP_REQUEST_TYPE:{
            //申请
            //当有用户申请加群时，群管理员会收到申请加群消息，可展示给用户，由用户决定是否同意对方加群，如果同意，调用accept方法，拒绝调用refuse方法。
            //申请加群请求（只有管理员会收到）
            //申请人、信息
            NSString *user = elem.user;
            NSString *msg = elem.msg;
            NSLog(@"申请加群请求:%@  %@",user,msg);
            
            
        }
            break;
        case TIM_GROUP_SYSTEM_ADD_GROUP_ACCEPT_TYPE:{
            //申请
            // 当管理员同意加群请求时，申请人会收到同意入群的消息，当管理员拒绝时，收到拒绝入群的消息。
            //申请加群被同意（只有申请人能够收到）
            //处理请求的管理员identifier
            NSString *user = elem.user;
            NSString *msg = elem.msg;
            NSLog(@"申请加群被同意:%@  %@",user,msg);
            
            
        }
            break;
        case TIM_GROUP_SYSTEM_ADD_GROUP_REFUSE_TYPE:{
            //申请
            //当管理员同意加群请求时，申请人会收到同意入群的消息，当管理员拒绝时，收到拒绝入群的消息。
            //申请加群被拒绝（只有申请人能够收到）
            //处理请求的管理员identifier
            NSString *user = elem.user;
            NSString *msg = elem.msg;
            NSLog(@"申请加群被拒绝:%@  %@",user,msg);
            
        }
            break;
        case TIM_GROUP_SYSTEM_INVITED_TO_GROUP_TYPE:{
            //邀请
            //当用户被邀请加入群组时，该用户会收到邀请消息，注意：创建群组时初始成员无需邀请即可入群。
            //邀请入群通知(被邀请者能够收到)
            NSLog(@"邀请");
            
        }
            break;
        case TIM_GROUP_SYSTEM_INVITE_TO_GROUP_REQUEST_TYPE:{
            //邀请
            //当有用户被邀请群时，该用户会收到邀请入群消息，可展示给用户，由用户决定是否同意入群，如果同意，调用accept方法，拒绝调用refuse方法。
            //邀请入群请求(被邀请者接收)
            //邀请人identifier
            NSString *user = elem.user;
            NSString *msg = elem.msg;
            NSLog(@"邀请入群请求:%@  %@",user,msg);
            
        }
            break;
        case TIM_GROUP_SYSTEM_INVITE_TO_GROUP_ACCEPT_TYPE:{
            //邀请
            
            //当被邀请者同意入群请求时，申请人会收到同意入群的消息，当管理员拒绝时，收到拒绝入群的消息。
            //邀请加群被同意(只有发出邀请者会接收到)
            //处理请求的用户identifier
            NSString *user = elem.user;
            NSString *msg = elem.msg;
            NSLog(@"邀请加群被同意:%@  %@",user,msg);
            
        }
            break;
        case TIM_GROUP_SYSTEM_INVITE_TO_GROUP_REFUSE_TYPE:{
            //邀请
            
            //当被邀请者同意入群请求时，申请人会收到同意入群的消息，当管理员拒绝时，收到拒绝入群的消息。
            //邀请加群被拒绝(只有发出邀请者会接收到)
            //处理请求的用户identifier
            NSString *user = elem.user;
            NSString *msg = elem.msg;
            NSLog(@"邀请加群被拒绝:%@  %@",user,msg);
            
        }
            break;
        case TIM_GROUP_SYSTEM_KICK_OFF_FROM_GROUP_TYPE:{
            //当用户被管理员踢出群组时，申请人会收到被踢出群的消息。
            //被管理员踢出群（只有被踢的人能够收到）
            NSString *user = elem.user;
            NSLog(@"被管理员踢出群:%@",user);
            
        }
            break;
        case TIM_GROUP_SYSTEM_QUIT_GROUP_TYPE:{
            //当用户主动退出群组时，该用户会收到退群消息，只有退群的用户自己可以收到。
            //当用户调用QuitGroup时成功回调返回，表示已退出成功，此消息主要为了多终端同步，其他终端可以做为更新群列表的时机，本终端可以选择忽略。
            //主动退群（主动退群者能够收到）
            NSLog(@"主动退群");
            
        }
            break;
        case TIM_GROUP_SYSTEM_REVOKE_GROUP_TYPE:{
            //当群组被系统回收时，全员可收到群组被回收消息。
            //群已被回收(全员接收)
            //群ID
            NSString * group = elem.group;
            NSLog(@"群已被回收:%@",group);
        }
            break;
        default:
            NSLog(@"ignore type");
            break;
    }
}




- (VLSVideoViewModel *)checkSelfInViewModels:(NSMutableArray *)viewModels
{
    for (VLSVideoViewModel *viewModel in viewModels) {
        if ([[VLSMessageManager sharedManager].host.userId isEqualToString:viewModel.userId]) {
            return viewModel;
        }
    }
    return nil;
}

//检查主屏的嘉宾是否被取消连线
- (void)checkMainGuestWasCancel
{
    if (mainGuestViewModel) {
        if (![[VLSMessageManager sharedManager] checkUserIsGuests:mainGuestViewModel.userId]) {
            if ([_delegate respondsToSelector:@selector(receiveUpdataAnchorView:)]) {
                [_delegate receiveUpdataAnchorView:nil];
            }
        }
    }
}

#pragma mark -操作失败的通知回调
- (void)receiveFailMessage:(NSString *)msg code:(int)code
{
    
#ifdef DEBUG
    [[VLSUserTrackingManager shareManager] trackingSendSensitive:msg];
//    [MBProgressHUD showError:msg toView:nil];
#endif
    if (code == 80001) {
        // 敏感词汇
        VLSMessageViewModel *messageVM = [[VLSMessageViewModel alloc] initWidthUser:nil message:@"V来秀：您当前的发言包含敏感词汇" types:TYPE_SYSTEMMESSAGE];
        
        if ([_delegate respondsToSelector:@selector(receiveNormalMessage:)]) {
            [_delegate receiveNormalMessage:[[NSMutableArray alloc] initWithObjects:messageVM, nil]];
        }
        
    }
    
}

+(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString {
    NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    return responseJSON;
}

@end
