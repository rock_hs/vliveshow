//
//  VLSPriseManager.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/10.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VLSPriseManagerDelegate <NSObject>

- (void)showPriseAnimation;

@end

@interface VLSPriseManager : NSObject

@property (nonatomic, weak) id<VLSPriseManagerDelegate> delegate;

- (void)addOperationToQueue;
- (void)excuteOperation;

// Default is 8
@property(nonatomic, assign) NSInteger maxPraise;
@end
