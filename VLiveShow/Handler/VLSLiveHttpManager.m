//
//  VLSLiveHttpManager.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLiveHttpManager.h"
#import "ManagerUtil.h"
#import "VLSRecomModel.h"
#import "VLSSeachResultModel.h"
#import "VLSUserTrackingManager.h"
#import "VLiveShow-Swift.h"

@implementation VLSLiveHttpManager


+ (void)getLiveList:(NSInteger)page callback:(ManagerCallBack *)callback
     WithSizeNumber:(NSInteger)sizeNumber
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *actorList = [dict objectForKey:@"list"];
        NSArray *list = [VLSHotModel mj_objectArrayWithKeyValuesArray:actorList];
        NSMutableArray *temp = [NSMutableArray arrayWithCapacity:0];
        for (int i=0 ; i<list.count; i++) {
            VLSLiveListModel *model = [[VLSLiveListModel alloc] initWidthHotModel:[list objectAtIndex:i]];
            [temp addObject:model];
        }
        return temp;
    }];
    [HTTPFacade getLiveList:page size:sizeNumber callback:http];
}

+ (void)getAdListCallback:(ManagerCallBack *)callback
{

    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *actorList = [dict objectForKey:@"list"];
        NSMutableArray *temp = [NSMutableArray arrayWithCapacity:0];
        for (int i=0 ; i<actorList.count; i++) {
            VLSAdListModel *model = [[VLSAdListModel alloc] initWithDic:actorList[i]];
            [temp addObject:model];
        }
        return temp;
    }];
    [HTTPFacade getAdCallback:http];
}

+ (void)getCourseListCallback: (ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *actorList = [dict objectForKey:@"list"];
        NSMutableArray *temp = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary* itemDict in actorList)
        {
            VLSCourseModel* course = [VLSCourseModel mj_objectWithKeyValues: itemDict];
            course.courseId = itemDict[@"id"];
            [temp addObject: course];
        }
        return temp;
    }];
    [HTTPFacade getCourseList:http];
}

+ (void)getLiveInfo:(NSString* )roomId callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        VLSLiveInfoModel *model = [VLSLiveInfoModel mj_objectWithKeyValues:dict];
        
        VLSLiveInfoViewModel *viewModel = [[VLSLiveInfoViewModel alloc] initWidthLiveInfoModel:model];
        if (![[dict objectForKey:@"topics"] isKindOfClass:[NSNull class]]) {
            NSArray * markArray = [dict objectForKey:@"topics"];
            
            if (markArray.count>=2) {
                viewModel.roomMark = [markArray objectAtIndex:1];
            }
        }
        return viewModel;
    }];
    [HTTPFacade getLiveInfo:roomId callback:http];

}

+ (void)getUserInfo:(NSString *)userId callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        NSDictionary *profile = [dict objectForKey:@"profile"];
        
        VLSUserBaseInfoModel *model = [VLSUserBaseInfoModel mj_objectWithKeyValues:profile];
        
        VLSUserInfoViewModel *viewModel = [[VLSUserInfoViewModel alloc] initWidthUserInfoModel:model];
        
        return viewModel;
    }];
    [HTTPFacade getUserInfo:userId Callback:http];
}
+(void)getuserInformation:(NSString*)fromUser Touser:(NSString*)touser callback:(ManagerCallBack *)callback
{

    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        NSDictionary *profile = [dict objectForKey:@"profile"];
        
        VLSUserBaseInfoModel *model = [VLSUserBaseInfoModel mj_objectWithKeyValues:profile];
        
        VLSUserInfoViewModel *viewModel = [[VLSUserInfoViewModel alloc] initWidthUserInfoModel:model];
        
        return viewModel;
    }];
    [HTTPFacade getuserInformation:fromUser Touser:touser callback:http];


}
+ (void)createLive:(VLSCreateLiveModel *)createLive callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    [HTTPFacade createLive:createLive callback:http];
}

+ (void)destroyLive:(NSString *)liveShowId callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return nil;
    }];
    [HTTPFacade destroyLive:liveShowId callback:http];
}

+ (void)RDKForRoomId:(NSString *)roomId callBack:(ManagerCallBack *)callback{
  
    HttpCallBack * http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
      return result;
    }];
  
    [HTTPFacade RDKForRoomId:roomId callBack:http];
  
}

+ (void)getAttentList:(NSInteger)page userid:(NSInteger)userid callback:(ManagerCallBack *)callback
{

    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        NSArray *attentList = [dict objectForKey:@"list"];
        NSArray *list = [MineModel mj_objectArrayWithKeyValuesArray:attentList];
        return list;
    }];
    [HTTPFacade getAttentList:page size:10 userid:userid callback:http];
}

+ (void)getFocusList:(NSInteger)page userid:(NSInteger)userid callback:(ManagerCallBack *)callback {
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
    
        return dict;
    }];
    [HTTPFacade getFocusList:page size:10 userid:userid callback:http];
}

+ (void)getFanstList:(NSInteger)page userid:(NSInteger)userid callback:(ManagerCallBack *)callback;
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *attentList = [dict objectForKey:@"list"];
        NSArray *list = [MineModel mj_objectArrayWithKeyValuesArray:attentList];
        return list;
    }];
    [HTTPFacade getFansList:page size:10 userid:userid callback:http];
}

+ (void)focusToUser:(NSString *)userId callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade focusToUser:userId callback:http];

}
+ (void)cancelFocusToUser:(NSString *)userId callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade cancelfocusToUser:userId callback:http];
}
//加入黑名单
+ (void)addBlack:(NSString*)usertoUID callback:(ManagerCallBack *)callback
{
    
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade addBlack:usertoUID callback:http];

}
//取消黑名单
+ (void)cancelBlack:(NSString*)usertoUID callback:(ManagerCallBack *)callback
{

    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade cancelBlack:usertoUID callback:http];


}
+(void)getuserBlackList:(NSInteger)page  callback:(ManagerCallBack*)callback
{

    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        NSArray *attentList = [dict objectForKey:@"list"];
        
        NSArray *list = [MineModel mj_objectArrayWithKeyValuesArray:attentList];
        return list;
    }];
    [HTTPFacade getuserBlackList:page size:10 callback:http];
}

//是否关注
+ (void)getrelationShip:(NSString*)usertoUID callBack:(ManagerCallBack *)callback{

    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return dict;
    }]; 
    [HTTPFacade getRelationShip:usertoUID callback:http];
}

+ (void)postLiveInfo:(NSString *)roomId watchNum:(NSInteger)watchNum callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    [HTTPFacade postLiveInfo:roomId watchNum:watchNum callback:http];
}

+ (void)postReport:(NSString*)userToID callback:(ManagerCallBack*)callback type:(NSString*)type content:(NSString*)content remark:(NSString*)remark
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade postReport:userToID callback:http type:type content:content remark:remark];


}

+(void)getRoomId:(NSString*)userID callback:(ManagerCallBack*)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        NSString * roomid = [dict objectForKey:@"room_id"];
        MyLog(@"roomid = %@", roomid);
        return roomid;
    }];
    [HTTPFacade getRoomId:userID callback:http];
}
+ (void)getIsAttent:(NSString *)usertoUID callBack:(ManagerCallBack *)callback{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return dict;
    }];
    [HTTPFacade IsAttent:usertoUID callback:http];
}
+ (void)postDeviceInformation:(ManagerCallBack *)callback
{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade postDeviceInformation:http];
}

+ (void)postWelcomeDeviceInfo:(ManagerCallBack *)callback{

    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade postWelcomeInfo:http];
}

//上传是否接收直播提醒消息
+ (void)postLiveNotice:(NSString*)isReceiveOrNot callback:(ManagerCallBack *)callback{

    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        [[VLSUserTrackingManager shareManager] trackingChangeProfile:@"livenotice" status:isReceiveOrNot];
        
        return dict;
    }];
    [HTTPFacade postLiveNoticeInformation:isReceiveOrNot callback:http];
}

+ (void)postLiveInvite:(NSString*)isReceiveOrNot callback:(ManagerCallBack *)callback{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        [[VLSUserTrackingManager shareManager] trackingChangeProfile:@"invitenotice" status:isReceiveOrNot];
        return dict;
    }];
    [HTTPFacade postLiveInviteInformation:isReceiveOrNot callback:http];
}

//获得是否接收直播消息提醒
+ (void)getLiveNotice:(ManagerCallBack *)callback{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    [HTTPFacade getLiveNoticeInformation:nil callback:http];
}

+ (void)getGiftList:(ManagerCallBack *)callback{
  
  HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
    return result;
  }];
  [HTTPFacade getGiftList:http];
  
}

+ (void)sendGift:(GiftModel *)model callback:(ManagerCallBack *)callback{
  
  HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
    return result;
  }];
  [HTTPFacade sendGift:model callback:http];
  
}

// 用户跟踪
+ (void)shareTrackingLiveBefore:(NSString *)platform RoomID:(NSString *)roomid callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade shareTrackingLiveBefore:platform RoomID:roomid callback:http];
}
+ (void)shareTrackingLiveAfter:(NSString *)platform RoomId:(NSString *)roomid callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade shareTrackingLiveAfter:platform RoomId:roomid callback:http];
}
+ (void)shareTrackingLiveProcess:(NSString *)platform RoomId:(NSString *)roomid callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        
        return nil;
    }];
    [HTTPFacade shareTrackingLiveProcess:platform RoomId:roomid callback:http];
}
+ (void)getEndPageInfo:(NSString*)roomID callback:(ManagerCallBack *)callback
{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
        
    }];
    [HTTPFacade getEndPageInfo:roomID callback:http];
}

+ (void)getRecommedLists:(ManagerCallBack *)callback {
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *lists = [dict objectForKey:@"list"];
        
        NSArray *modelLists = [VLSRecomModel mj_objectArrayWithKeyValuesArray:lists];
        return modelLists;
        
//        NSArray *modelLists = [VLSRecommendModel mj_objectArrayWithKeyValuesArray:lists];
//        NSMutableArray *recomLists = [NSMutableArray array];
//        for (VLSRecommendModel *model  in modelLists) {
//            VLSRecommendViewModel *recomViewModel = [[VLSRecommendViewModel alloc] initWithRecommendModel:model];
//            [recomLists addObject:recomViewModel];
//            
//        }
//        return recomLists;
    }];
    [HTTPFacade getRecommedLists:http];
}

+ (void)getAudiuesInfo:(NSString *)roomid size:(NSInteger)size callback:(ManagerCallBack *)callback
{
    HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *actorList = [dict objectForKey:@"audiences"];
        NSMutableArray *temp = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in actorList) {
            VLSUserProfile *model = [[VLSUserProfile alloc]init];
            [model GetUserProfileFromDict:dict];
            [temp addObject:model];
        }
        
        return temp;
    }];
    [HTTPFacade getAudiuesInfo:roomid size:size callback:http];
}
+ (void)getWatchNum:(NSString *)roomid callback:(ManagerCallBack *)callback
{
    HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
        return [result objectForKey:@"tscNum"];
    }];
    [HTTPFacade getWatchNum:roomid callback:http];
}
+ (void)getAllRelationshipPage:(NSInteger)page size:(NSInteger)size roomid:(NSString *)roomid callback:(ManagerCallBack *)callback
{
    HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *aboutPeople = [dict objectForKey:@"list"];
        NSArray *list = [VLSAnchorAboutModel mj_objectArrayWithKeyValuesArray:aboutPeople];
        NSMutableArray *temp = [NSMutableArray arrayWithCapacity:0];
        for (VLSAnchorAboutModel *model in list) {
            VLSInviteGuestModel *inviteModel = [[VLSInviteGuestModel alloc] init];
            [inviteModel getFromAnchorAboutModel:model];
            [temp addObject:inviteModel];
        }
        return temp;
    }];
    [HTTPFacade getAllRelationshipPage:page size:size roomid:roomid callback:http];
}

+ (void)getSearchRelationshipPage:(NSInteger)page size:(NSInteger)size keyword:(NSString *)keyword callback:(ManagerCallBack *)callback;
{
    HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *aboutPeople = [dict objectForKey:@"list"];
        NSArray *list = [VLSAnchorSearchModel mj_objectArrayWithKeyValuesArray:aboutPeople];
        NSMutableArray *temp = [NSMutableArray arrayWithCapacity:0];
        for (VLSAnchorSearchModel *model in list) {
            VLSInviteGuestModel *inviteModel = [[VLSInviteGuestModel alloc] init];
            [inviteModel getFromAnchorAboutSearchModel:model];
            [temp addObject:inviteModel];
        }
        return temp;
    }];
    [HTTPFacade getSearchRelationshipPage:page size:size keyword:keyword callback:http];
}
+ (void)getRankingList:(ManagerCallBack*)callback userID:(NSString*)userid
{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    [HTTPFacade getRankingList:http userID:userid];
}

+ (void)searchUserWithPage:(NSInteger)page size:(NSInteger)size keyword:(NSString *)keyword callback:(ManagerCallBack *)callback {
    HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *seachLists = [dict objectForKey:@"list"];
        NSMutableArray *results = [VLSSeachResultModel mj_objectArrayWithKeyValuesArray:seachLists];
        
        return results;
//        NSMutableArray *searchResults = [NSMutableArray array];
//        for (MineModel *model in results) {
//            VLSRecommendViewModel *recomViewModel = [[VLSRecommendViewModel alloc] initWithUserInfoModel:model];
//            [searchResults addObject:recomViewModel];
//            
//        }
//        return searchResults;
    }];
    [HTTPFacade searchUserWithPage:page size:size keyword:keyword callback:http];
}

//上传tracking
+(void)postUserTrackingInfo:(NSDictionary*)dict callBcak:(ManagerCallBack *)callback{
    
    HttpCallBack *http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
        return nil;
    }];
    [HTTPFacade postTrackingInformation:dict callBack:http];
}

+ (void)postDeviceActivateTrack: (NSDictionary *)param callback: (ManagerCallBack *)callback
{
    HttpCallBack *httpCallback = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
        return nil;
    }];
    [HTTPFacade postDeviceActivateTrack: param callBack: httpCallback];
}

+ (void)getChannelContents: (NSString*)channelId page: (NSInteger)page number: (NSInteger)number callback: (ManagerCallBack *)callback
{
    HttpCallBack *httpCallback = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary *result) {
        return result;
    }];
    [HTTPFacade getChannelDetails: channelId pageNumber: page pageSize: number callback: httpCallback];
}

@end
