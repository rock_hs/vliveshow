//
//  VLSDBHelper.m
//  VLiveShow
//
//  Created by SuperGT on 16/7/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSDBHelper.h"
#import "BaseModel.h"
#import "VLSFansModel.h"
#import "VLSLiveRemindModel.h"
#import "VLSTeamMessageModel.h"
#import "VLSVLiveViewModel.h"
#import "AccountManager.h"

@interface VLSDBHelper ()

@property (nonatomic, strong) NSMutableArray *messageArr;

@end

@implementation VLSDBHelper

+ (instancetype)defaultHelper
{
    static VLSDBHelper *helper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (helper == nil) {
            helper = [[VLSDBHelper alloc] init];
        
            [helper loadMessageArr];
        }
    });
    return helper;
}

- (void)loadMessageArr
{
    self.messageArr = [NSMutableArray arrayWithCapacity:0];
    
    VLSVLiveViewModel *model1 = [[VLSVLiveViewModel alloc] init];
    VLSVLiveViewModel *model2 = [[VLSVLiveViewModel alloc] init];
    VLSVLiveViewModel *model3=  [[VLSVLiveViewModel alloc] init];

    [self.messageArr addObject:model1];
    [self.messageArr addObject:model2];
    [self.messageArr addObject:model3];
}

+ (NSMutableArray *)sortComparator:(NSMutableArray *)arr
{
    [arr sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        BaseModel *model1 = obj1;
        BaseModel *model2 = obj2;
        
        NSDate *date1 = [model1 valueForKey:@"timestamp"];
        NSDate *date2 = [model2 valueForKey:@"timestamp"];
        
        switch ([date1 compare:date2]) {
            case NSOrderedSame:
                return NSOrderedSame;
                break;
            case NSOrderedDescending:
                return NSOrderedAscending;
                break;
            case NSOrderedAscending:
                return NSOrderedDescending;
                break;
            default:
                break;
        }
    }];
    
    return arr;
}

+ (void)updateFansModelFromDB
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        BOOL updated = [VLSFansModel updateToDBWithSet:@"isread = 1" where:nil];
        if (updated) {
            NSLog(@"更新成功");
        }
//        return updated;
    });
}

+ (void)updateLiveModelFromDB
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        BOOL updated = [VLSLiveRemindModel updateToDBWithSet:@"isread = 1" where:nil];
        if (updated) {
            NSLog(@"更新成功");
        }
    });

}

+ (void)updateTeamModelFromDB
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        BOOL updated = [VLSTeamMessageModel updateToDBWithSet:@"isread = 1" where:nil];
        if (updated) {
            NSLog(@"更新成功");
        }
    });
}

+ (void)teamSearchWithPage:(NSInteger)page pageSize:(NSInteger)pageSize completeHandler:(complete)block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray *arrData = [VLSTeamMessageModel searchWithWhere:nil orderBy:[NSString stringWithFormat:@"%@ desc",[VLSTeamMessageModel getPrimaryKey]] offset:page*pageSize count:pageSize];
        
        NSMutableArray *sortArr = [self sortComparator:arrData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            block(sortArr);
        });
    });
}

+ (void)LiveSearchWithPage:(NSInteger)page pageSize:(NSInteger)pageSize completeHandler:(complete)block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray *arrData = [VLSLiveRemindModel searchWithWhere:nil orderBy:[NSString stringWithFormat:@"%@ desc",[VLSLiveRemindModel getPrimaryKey]] offset:page*pageSize count:pageSize];
        
        NSMutableArray *sortArr = [self sortComparator:arrData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            block(sortArr);
        });
    });
}

+ (void)fansSearchWithPage:(NSInteger)page pageSize:(NSInteger)pageSize completeHandler:(complete)block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray *arrData = [VLSFansModel searchWithWhere:nil orderBy:[NSString stringWithFormat:@"%@ desc",[VLSFansModel getPrimaryKey]] offset:page*pageSize count:pageSize];
        
        NSMutableArray *sortArr = [self sortComparator:arrData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            block(sortArr);
        });
    });
}

- (void)getAllMessageArrFromDB:(complete)block
{
    //1.创建队列组
    dispatch_group_t group = dispatch_group_create();
    //2.创建队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    
    dispatch_group_async(group, queue, ^{
        [self getTeamMessageModel];
    });
    
    
    dispatch_group_async(group, queue, ^{
        [self getLiveModel];
        
    });
    
    
    dispatch_group_async(group, queue, ^{
        [self getFansModel];
        
    });
    
    //4.都完成后会自动通知
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        block(_messageArr);
        
    });
}

- (void)getTeamMessageModel
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dict setValue:[NSNumber numberWithBool:NO] forKey:@"isread"];
    NSInteger arrCount = [VLSTeamMessageModel rowCountWithWhere:dict];
    NSMutableArray *arr = [VLSTeamMessageModel searchWithWhere:nil orderBy:@"rowid desc" offset:0 count:1];
    
    VLSTeamMessageModel *vTeamModel = [arr firstObject];
    VLSVLiveViewModel *model = [[VLSVLiveViewModel alloc] init];
    model.title = LocalizedString(@"VTEAM");
    model.imageName = @"ic_official";
    model.content = vTeamModel.desc;
    model.messageNumber = arrCount;
    [_messageArr replaceObjectAtIndex:0 withObject:model];
}

- (void)getLiveModel
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dict setValue:[NSNumber numberWithBool:NO] forKey:@"isread"];
    NSInteger arrCount = [VLSLiveRemindModel rowCountWithWhere:dict];
    
    NSMutableArray *arr = [VLSLiveRemindModel searchWithWhere:nil orderBy:@"rowid desc" offset:0 count:1];
    
    VLSLiveRemindModel *vLiveModel = [arr firstObject];
    
    VLSVLiveViewModel *model = [[VLSVLiveViewModel alloc] init];
    model.title = LocalizedString(@"LIVENOTICE");
    model.imageName = @"ic_kaibotixing";
    model.content = vLiveModel.desc;
    model.messageNumber = arrCount;
    [_messageArr replaceObjectAtIndex:1 withObject:model];
    
}

- (void)getFansModel
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dict setValue:[NSNumber numberWithBool:NO] forKey:@"isread"];
    NSInteger arrCount = [VLSFansModel rowCountWithWhere:dict];
    
    NSMutableArray *arr = [VLSFansModel searchWithWhere:nil orderBy:@"rowid desc" offset:0 count:1];
    VLSFansModel *vFansModel = [arr firstObject];
    
    VLSVLiveViewModel *model = [[VLSVLiveViewModel alloc] init];
    model.title = LocalizedString(@"FANS");
    model.imageName = @"ic_fans";
    model.content = vFansModel.desc;
    model.messageNumber = arrCount;
    [_messageArr replaceObjectAtIndex:2 withObject:model];
    
}



+ (void)ThreadSaveFansModelToDB:(NSDictionary *)dict TIMMessage:(TIMMessage *)msg elem:(TIMCustomElem *)elem completeHandler:(operation)operation
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 耗时的操作
        if (!dict) {
            operation(NO);
            return;
        }
        VLSFansModel *model = [[VLSFansModel alloc]init];
        model.nickName = dict[@"nickname"];
        model.UserID = dict[@"userID"];
        model.desc = elem.desc;
        model.isread = NO;
        model.timestamp = msg.timestamp;
        
        // 判断是否是自己向自己发送
        NSNumber *hostID = [NSNumber numberWithInteger:[AccountManager sharedAccountManager].account.uid];
        if ([model.UserID isEqual:hostID]) {
            operation(NO);
            return;
        }
        
        // 搜索数据库是否包含这条消息
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
        [dict setValue:model.UserID forKey:@"UserID"];
        NSInteger count = [VLSFansModel rowCountWithWhere:dict];
        if (count > 0) {
            operation(NO);
            return;
        }
        operation(YES);
        [model saveToDB];
    });
}

+ (void)ThreadSaveLiveModelToDB:(NSDictionary *)dict TIMMessage:(TIMMessage *)msg elem:(TIMCustomElem *)elem completeHandler:(operation)operation
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 耗时的操作
        if (!dict) {
            operation(NO);
            return;
        }
        VLSLiveRemindModel *model = [[VLSLiveRemindModel alloc]init];
        model.anchorID = dict[@"anchorID"];
        model.anchorName = dict[@"anchorName"];
        model.title = dict[@"title"];
        model.imageURL = dict[@"imageURL"];
        model.RoomID = dict[@"roomID"];
        model.content = dict[@"content"];
        model.desc = elem.desc;
        model.isread = NO;
        model.timestamp = msg.timestamp;
        // 判断是否是自己向自己发送
        NSNumber *hostID = [NSNumber numberWithInteger:[AccountManager sharedAccountManager].account.uid];
        if ([model.anchorID isEqual:hostID]) {
            operation(NO);
            return;
        }
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
        [dict setValue:model.RoomID forKey:@"RoomID"];
        NSInteger count = [VLSLiveRemindModel rowCountWithWhere:dict];
        if (count > 0) {
            operation(NO);
            return;
        }
        operation(YES);
        [model saveToDB];
    });
}

+ (void)ThreadSaveTeamModelToDB:(NSDictionary *)dict TIMMessage:(TIMMessage *)msg elem:(TIMCustomElem *)elem completeHandler:(operation)operation
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 耗时的操作
        if (!dict) {
            operation(NO);
            return;
        }
        VLSTeamMessageModel *model = [[VLSTeamMessageModel alloc]init];
        model.title = dict[@"title"];
        model.content = dict[@"content"];
        model.imageURL = dict[@"imageURL"];
        model.anchorID = [dict[@"anchorID"] stringValue];
        model.anchorName = dict[@"anchorName"];
        model.roomID = dict[@"roomID"];
        model.desc = elem.desc;
        model.isread = NO;
        model.timestamp = msg.timestamp;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
        
        if (model.roomID) {
            // 如果嘉宾邀请的情况下，改变过滤条件
            NSString *hostID = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
            if ([model.anchorID isEqual:hostID]) {
                operation(NO);
                return;
            }
            [dict setValue:model.roomID forKey:@"roomID"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"showMsgWindow" object:model];
            NSInteger count = [VLSTeamMessageModel rowCountWithWhere:dict];
            if (count > 0) {                
                operation(NO);
                return;
            }
        }else{
            [dict setValue:model.content forKey:@"content"];
            NSInteger count = [VLSTeamMessageModel rowCountWithWhere:dict];
            if (count > 0) {
                operation(NO);
                return;
            }
        }
    
        operation(YES);
        [model saveToDB];
    });
    
}

+ (void)UpdateTeamDBandReload:(complete)complete
{
    //1.创建队列组
    dispatch_group_t group = dispatch_group_create();
    //2.创建队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    
    dispatch_group_async(group, queue, ^{
        [self updateTeamModelFromDB];
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        complete(nil);
        
    });

}
+ (void)UpdateLiveDBandReload:(complete)complete
{
    //1.创建队列组
    dispatch_group_t group = dispatch_group_create();
    //2.创建队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    
    dispatch_group_async(group, queue, ^{
        [self updateLiveModelFromDB];
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        complete(nil);
        
    });
    
}
+ (void)UpdateFansDBandReload:(complete)complete
{
    //1.创建队列组
    dispatch_group_t group = dispatch_group_create();
    //2.创建队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    
    dispatch_group_async(group, queue, ^{
        [self updateFansModelFromDB];
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        [[VLSDBHelper defaultHelper] getAllMessageArrFromDB:^(NSMutableArray *arr) {
            complete(arr);
        }];
        
    });
    
}

+ (void)DeleteAllDB
{
    [VLSTeamMessageModel deleteWithWhere:nil];
    [VLSLiveRemindModel deleteWithWhere:nil];
    [VLSFansModel deleteWithWhere:nil];
}

@end
