//
//  VLSReConnectionManager.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/25.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSReConnectionManager.h"
#import "VLSLiveListModel.h"
@implementation VLSReConnectionManager

+ (instancetype)sharedManager
{
    static VLSReConnectionManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc ] init];
        
    });
    return _sharedManager;
}

- (void)saveRoomID:(NSString *)roomid andAnchorID:(NSString *)anchorId
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    [userdef setObject:roomid forKey:ReconnectionRoomID];
    [userdef setObject:anchorId forKey:ReconnectionAnchorID];
}

- (void)deleteRoomIDandAnchorID
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    [userdef removeObjectForKey:ReconnectionRoomID];
    [userdef removeObjectForKey:ReconnectionAnchorID];
}

- (NSString *)getRoomID
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *roomid = [userdef objectForKey:ReconnectionRoomID];
    return roomid;
}

- (NSString *)getAnchorID
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *anchorID = [userdef objectForKey:ReconnectionAnchorID];
    return anchorID;
}


@end
