 //
//  VLSAVMultiManager.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSAVMultiManager.h"
#import "ManagerUtil.h"
#import "HTTPFacade.h"
#import "VLSMessageManager.h"
#import "VLSMuteModel.h"
#import "VLSUserTrackingManager.h"


@interface VLSAVMultiManager()
//@property (nonatomic)
@end

@implementation VLSAVMultiManager

+ (instancetype)sharedManager
{
    static VLSAVMultiManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc ] init];
        
    });
    return _sharedManager;
}
#pragma mark - 初始化直播间主屏的model
- (void)initFirstAVroomAnchorId:(NSString *)anchorId
{
    VLSVideoViewModel *model = [[VLSVideoViewModel alloc] init];
    model.userId = anchorId;
    model.userType = UserTypeHost;
    model.muteVideo = NO;
    model.muteVoice = NO;
    model.muteVoiceBySelf = NO;

    [[VLSMessageManager sharedManager] initFirstAVroom:[[NSMutableArray alloc] initWithObjects:model, nil]];
}

//观众申请连线主播
- (void)linkRequest:(NSString *)anchorId callback:(ManagerCallBack *)callback
{
    //发送消息
    NSString *str = [NSString stringWithFormat:@"%d&1",VLS_GUEST_REQUEST];
    [[VLSMessageManager sharedManager] sendC2CMsg:str to:anchorId type:SendMsgTypeCustom succ:^{
        NSLog(@"发送连线主播消息成功");
        self.linkStatus = UserLinkStatusApplying;
    } fail:^(int code, NSString *msg) {
        
    }];
    
    //发送请求
    VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
    model.action = GUEST_REQUEST;
    model.fromAccount = [VLSMessageManager sharedManager].host.userId;
    model.toAccount = anchorId;
    model.roomId = [[VLSMessageManager sharedManager] getRoomId];
    NSLog(@"%@",[[VLSMessageManager sharedManager] getRoomId]);
    [self interactionPost:model callback:callback];
    
    [[VLSUserTrackingManager shareManager] trackingLinkrequest:model];
}
//嘉宾接收邀请
- (void)guestAcceptInvite:(NSString *)anchorId callback:(ManagerCallBack *)callback
{
    //发送消息
    NSString *str = [NSString stringWithFormat:@"%d&1",VLS_GUEST_REQUEST];
    [[VLSMessageManager sharedManager] sendC2CMsg:str to:anchorId type:SendMsgTypeCustom succ:^{
        NSLog(@"发送连线主播消息成功");
        self.linkStatus = UserLinkStatusApplying;
    } fail:^(int code, NSString *msg) {
        
    }];
    
    //发送请求
    VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
    model.action = GUEST_ACCEPT_INVITE;
    model.fromAccount = [VLSMessageManager sharedManager].host.userId;
    model.toAccount = anchorId;
    model.roomId = [[VLSMessageManager sharedManager] getRoomId];
    NSLog(@"%@",[[VLSMessageManager sharedManager] getRoomId]);
    [self interactionPost:model callback:callback];
    
    
    
    
    [[VLSUserTrackingManager shareManager] trackingLinkrequest:model];

}

//观众取消连线（未连接成功之前）
- (void)linkRequestCancel:(NSString *)anchorId callback:(ManagerCallBack *)callback
{
    //发送消息
    NSString *str = [NSString stringWithFormat:@"%d&1",VLS_GUEST_RQT_CANCEL];
    [[VLSMessageManager sharedManager] sendC2CMsg:str to:anchorId type:SendMsgTypeCustom succ:^{
        NSLog(@"观众取消连线（未连接成功之前）成功");
        self.linkStatus = UserLinkStatusAudience;
    }fail:^(int code, NSString *msg) {
        
    }];
    
    //发送请求
    VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
    model.action = GUEST_REQ_CANCEL;
    model.fromAccount = [VLSMessageManager sharedManager].host.userId;
    model.toAccount = anchorId;
    model.roomId = [[VLSMessageManager sharedManager] getRoomId];
    NSLog(@"%@",[[VLSMessageManager sharedManager] getRoomId]);
    [self interactionPost:model callback:callback];
    
    [[VLSUserTrackingManager shareManager] trackingCancellink:model];
}

//嘉宾取消连线（连线成功之后）
- (void)guestCancel:(NSString *)anchorId callback:(ManagerCallBack *)callback
{
    //发送消息
    NSString *str = [NSString stringWithFormat:@"%d&1",VLS_GUEST_CANCEL];
    [[VLSMessageManager sharedManager] sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        NSLog(@"发送嘉宾取消连线（连线成功之后）成功");
        self.linkStatus = UserLinkStatusAudience;
        
    } fail:^(int code, NSString *msg) {
        
    }];
    
    //发送请求
    VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
    model.action = GUEST_CANCEL;
    model.fromAccount = [VLSMessageManager sharedManager].host.userId;
    model.toAccount = anchorId;
    model.roomId = [[VLSMessageManager sharedManager] getRoomId];
    [self interactionPost:model callback:callback];
    
    [[VLSUserTrackingManager shareManager] trackingCancellink:model];
}

//主播接通连线
- (void)anchorAcceptLinkRequest:(NSArray *)requestIds callback:(ManagerCallBack *)callback
{
    NSMutableArray *anchors = [VLSMessageManager sharedManager].liveAnchors;
    NSMutableArray *tempIds = [NSMutableArray arrayWithArray:requestIds];

    for (VLSVideoViewModel *model in anchors) {
        if (model.userType != UserTypeHost) {
            if ([tempIds containsObject:model.userId]) {
                [tempIds removeObject:model.userId];
            }
        }
    }
    if (tempIds.count >0) {
        [self anchorAccept:tempIds callback:callback];
    }

    
//    NSMutableArray *tempIds = [NSMutableArray arrayWithArray:requestIds];
//    NSMutableArray *anchors = [VLSMessageManager sharedManager].liveAnchors;
//    NSMutableArray *removeIds =[NSMutableArray arrayWithCapacity:0];
//
//    for (VLSVideoViewModel *model in anchors) {
//        if (model.userType != UserTypeHost) {
//            if ([tempIds containsObject:model.userId]) {
//                [tempIds removeObject:model.userId];
//            }else{
//                [removeIds addObject:model.userId];
//            }
//        }
//    }
//
//    if (removeIds.count == 0 && tempIds.count == 0) {
//        ManagerEvent* event = [ManagerEvent toast:nil title:@"..."];
//        event.code = 0000;
//        callback.errorBlock(event);
//    }
//    //quxiao
//    if (removeIds.count >0) {
//        [self anchorCancelLink:removeIds callback:callback];
//    }
//    //jieshou
//    if (tempIds.count >0) {
//        [self anchorAccept:tempIds callback:callback];
//    }
}

- (void)anchorAccept:(NSArray *)requestIds callback:(ManagerCallBack *)callback
{
    for (int i=0; i<requestIds.count; i++) {
        NSString *requestId = [requestIds objectAtIndex:i];
        //发送消息
        NSString *str = [NSString stringWithFormat:@"%d&1",VLS_ANCHOR_ACCEPT];
        [[VLSMessageManager sharedManager] sendC2CMsg:str to:requestId type:SendMsgTypeCustom succ:^{
            NSLog(@"发送主播接受连线成功");
        } fail:^(int code, NSString *msg) {
            
        }];
    }
    //发送请求
    VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
    model.action = ANCHOR_ACCEPT;
    model.fromAccount = [VLSMessageManager sharedManager].host.userId;
    model.toAccount = [self getStringSpliceWidthArray:requestIds];
    model.roomId = [[VLSMessageManager sharedManager] getRoomId];
    [self interactionPost:model callback:callback];
    
    [[VLSUserTrackingManager shareManager] trackingAcceptlink:model];
}

//主播取消连线
- (void)anchorCancelLink:(NSArray *)requestIds callback:(ManagerCallBack *)callback
{
    if (requestIds.count>0) {
        //发送消息
        NSString *str = [NSString stringWithFormat:@"%d&%@",VLS_ANCHOR_CANCEL,[self getStringSpliceWidthArray:requestIds]];
        [[VLSMessageManager sharedManager] sendGroupMsg:str type:SendMsgTypeCustom succ:^{
            NSLog(@"发送嘉宾取消连线（连线成功之后）成功");
        } fail:^(int code, NSString *msg) {
            
        }];
        //发送请求
        VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
        model.action = ANCHOR_CANCEL;
        model.fromAccount = [VLSMessageManager sharedManager].host.userId;
        model.toAccount = [self getStringSpliceWidthArray:requestIds];
        model.roomId = [[VLSMessageManager sharedManager] getRoomId];
        [self interactionPost:model callback:callback];
        
        [[VLSUserTrackingManager shareManager] trackingCancellink:model];
    }
}

//主播切换嘉宾位置
- (void)anchorReplacePositionFrom:(NSString *)uId1 to:(NSString *)uId2 callback:(ManagerCallBack *)callback
{
    //发送消息
    NSString *str = [NSString stringWithFormat:@"%d&%@&%@",VLS_ANCHOR_REPLACE,uId1,uId2];
    [[VLSMessageManager sharedManager] sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        NSLog(@"发送交换位置成功");
    } fail:^(int code, NSString *msg) {
        
    }];
    
    [[VLSUserTrackingManager shareManager] trackingChangeactor:uId1];
}

//主播禁(取消)麦
- (void)anchorMuteModels:(NSArray *)muteModels callback:(ManagerCallBack *)callback
{
    NSMutableArray *anchors = [VLSMessageManager sharedManager].liveAnchors;
    NSMutableArray *ids = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *statues = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *tempModels = [NSMutableArray arrayWithCapacity:0];
    
    for (VLSVideoViewModel *anchor in anchors) {
        for (VLSMuteModel *model in muteModels) {
            if ([model.userId isEqualToString:anchor.userId]) {
                if (model.muteVoice == anchor.muteVoice&& model.muteVoiceBySelf == anchor.muteVoiceBySelf) {
                    
                }else{
                    [tempModels addObject:model];
                }
            }
        }
    }
    for (VLSMuteModel *model in tempModels) {
        NSString *uid= model.userId;
        NSString *statue = model.muteVoice?@"1":@"0";
        [ids addObject:uid];
        [statues addObject:statue];
    }
    
    NSString *str = [NSString stringWithFormat:@"%d&%@&%@",VLS_ANCHOR_MUTE,[self getStringSpliceWidthArray:ids],[self getStringSpliceWidthArray:statues]];
    [[VLSMessageManager sharedManager] sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        NSLog(@"发送嘉宾取消连线（连线成功之后）成功");
    } fail:^(int code, NSString *msg) {
        
    }];
    [[VLSUserTrackingManager shareManager] trackingMute:str];
}

//嘉宾自己禁麦
- (void)geneusMuteSelf:(BOOL)mute callback:(ManagerCallBack *)callback
{
    NSString *boolStr = nil;
    //发送消息
    NSString *str = [NSString stringWithFormat:@"%d&%d",VLS_GUEST_MUTE_SELF,mute];
    [[VLSMessageManager sharedManager] sendGroupMsg:str type:SendMsgTypeCustom succ:^{
        NSLog(@"发送自己静音成功");
        callback.updateBlock(@"success");
    } fail:^(int code, NSString *msg) {
        
    }];
    
    if (mute == YES) {
        boolStr = @"YES";
    }else{
    
        boolStr = @"NO";
    }
    [[VLSUserTrackingManager shareManager] trackingGuestMute:boolStr];
}

//邀请嘉宾
/**
 *  只需要写入点击事件即可
 */
-(void)inviteHonoredGuestWithInviteIds:(NSArray *)inviteIds callback:(ManagerCallBack *)callback{
    
    //发送请求
    VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
    model.action = ANCHOR_INVITE;
    model.fromAccount = [VLSMessageManager sharedManager].host.userId;
    model.toAccount = [self getStringSpliceWidthArray:inviteIds];
    model.roomId = [[VLSMessageManager sharedManager] getRoomId];
    [self interactionPost:model callback:callback];
    
    [[VLSUserTrackingManager shareManager] trackingInvite:model];
}
//嘉宾立即上镜
- (void)geneusOnCamera:(NSString *)anchorId callback:(ManagerCallBack *)callback
{
    NSMutableArray *anchors = [VLSMessageManager sharedManager].liveAnchors;
    BOOL canOnCamera = YES;
    if (anchors.count < 5) {
        for (VLSVideoViewModel *anchor in anchors) {
            if ([[VLSMessageManager sharedManager] checkUserIsSelf:anchor.userId]) {
                canOnCamera = NO;
            }
        }
    }else{
        callback.errorBlock(@"嘉宾席已满,请稍候再试");
    }
    if (canOnCamera) {
        //发送消息
        NSString *str = [NSString stringWithFormat:@"%d&1",VLS_GUEST_ON_CAMERA];
        [[VLSMessageManager sharedManager] sendGroupMsg:str type:SendMsgTypeCustom succ:^{
            NSLog(@"发送立即上镜成功");
            callback.updateBlock(@"success");
        } fail:^(int code, NSString *msg) {
            
        }];
        
        [[VLSUserTrackingManager shareManager] trackingUponImmediately];
    }
    
}

//当主播暂时离开时候， 自己获取发送布局的权限，自己离开时候要发送当前布局给后台
- (void)guestLeaveWhenAnchorLeaveMoment
{
    NSMutableArray *anchors = [VLSMessageManager sharedManager].liveAnchors;
    for (int i=0; i<anchors.count; i++) {
        VLSVideoViewModel *model = [anchors objectAtIndex:i];
        if ([model.userId isEqualToString:[VLSMessageManager sharedManager].host.userId]) {
            [anchors removeObject:model];
        }
    }
    [VLSMessageManager sharedManager].liveAnchors = anchors;
    ManagerCallBack *callback = [[ManagerCallBack alloc]init];
    callback.updateBlock = ^(id result){
        MyLog(@"主播暂离，嘉宾向后台发送布局成功");
    };
    callback.errorBlock = ^(id result){

    };
    [[VLSAVMultiManager sharedManager] anchorSendLayoutToBackgroungCallback:callback];
}

//主播发送布局给后台
- (void)anchorSendLayoutToBackgroungCallback:(ManagerCallBack *)callback
{
    NSMutableArray *anchors = [VLSMessageManager sharedManager].liveAnchors;
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
    for (VLSVideoViewModel *anchor in anchors) {
        VLSVideoAnchorModel *model = [[VLSVideoAnchorModel alloc] initWidthViewModel:anchor];
        [array addObject:model];
    }
    VLSVideoLayoutModel *layoutModel = [[VLSVideoLayoutModel alloc] init];
    layoutModel.roomId = [[VLSMessageManager sharedManager] getRoomId];
    layoutModel.anchors = array;
    NSDictionary *layoutDict = [layoutModel mj_JSONObject];
    
    VLSAVInteractionModel *model = [[VLSAVInteractionModel alloc] init];
    model.fromAccount = [VLSMessageManager sharedManager].host.userId;
    model.roomId = [[VLSMessageManager sharedManager] getRoomId];
    model.action = ANCHOR_UPD_LAYOUT;
    model.toAccount = @"1";
    model.layout = layoutDict;
    [self interactionPost:model callback:callback];

}

- (NSString *)getStringSpliceWidthArray:(NSArray *)array
{
    NSString *tempString = [array componentsJoinedByString:@","];
    
    return tempString;
}

- (void)interactionPost:(VLSAVInteractionModel*)model callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return nil;
    }];
    [HTTPFacade avInteraction:model callback:http];

}
//获取请求连线的观众列表
- (void)getLinkRequestListCallback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        NSArray *attentList = [VLSLinkRequestModel mj_objectArrayWithKeyValuesArray:[dict objectForKey:@"linkReqestList"]];
        return attentList;
//        NSArray *list = [ mj_objectArrayWithKeyValuesArray:attentList];
    }];
    [HTTPFacade avRequestListRoomId:[[VLSMessageManager sharedManager] getRoomId] callback:http];
}



@end
