//
//  VLSLiveRoomManager.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/23.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSLiveInfoViewModel.h"
@interface VLSLiveRoomManager : NSObject
@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) VLSLiveInfoViewModel *liveInfo;

+ (instancetype)sharedManager;

//- (void)joinRoom;
//
//- (void)leaveRoom;

@end
