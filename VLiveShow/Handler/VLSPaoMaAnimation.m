//
//  VLSPaoMaAnimation.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSPaoMaAnimation.h"

@implementation VLSPaoMaAnimation

+ (void)AnimationPaoMaWithLabel:(UILabel *)label
{
    
    
    CGRect labelframe = label.frame;
    
    CGRect superframe = label.superview.frame;
    
    labelframe.origin.x = superframe.size.width;
    
    label.frame = labelframe;
    
    
    CGPoint fromPoint = CGPointMake(superframe.size.width + labelframe.size.width/2, superframe.size.height/2);
    UIBezierPath *movePath = [UIBezierPath bezierPath];
    [movePath moveToPoint:fromPoint];
    [movePath addLineToPoint:CGPointMake(-labelframe.size.width/2, superframe.size.height/2)];
    
    CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    moveAnim.path = movePath.CGPath;
    moveAnim.removedOnCompletion = YES;
    
    moveAnim.duration = labelframe.size.width * 0.08f;
    moveAnim.repeatCount = MAXFLOAT;
    
    [label.layer addAnimation:moveAnim forKey:nil];
}

@end
