//
//  VLSLocationManager.h
//  VLiveShow
//
//  Created by Cavan on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSLocationManager : NSObject


@property (nonatomic, strong)NSString *locaStr;
@property (nonatomic, strong)NSString *latitude;
@property (nonatomic, strong)NSString *longitude;
@property (nonatomic, assign)BOOL locaAuthi;

+ (VLSLocationManager *)shareManager;
- (void)loadLocation;

@end
