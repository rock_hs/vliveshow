//
//  VLSIMManage.m
//  TXDemo
//
//  Created by SuperGT on 16/5/23.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import "VLSIMManager.h"
#import <ImSDK/ImSDK.h>


static unsigned int CloundSDK = CLOUND_SDK;
static NSString * const acountType = CLOUND_ACCOUNT_TYPE;

@interface VLSIMManager ()<TIMMessageListener>
//@property (nonnull,nonatomic,strong) TIMManager *manager;
@property (nonatomic,strong) TIMConversation *conversation;
//@property (nonnull,nonatomic,strong) TIMGroupManager *GroupManager;

@end

@implementation VLSIMManager

+ (instancetype)sharedInstance
{
     static VLSIMManager *VLManager = nil;
     static dispatch_once_t onceToken;
     dispatch_once(&onceToken, ^{
          VLManager = [[VLSIMManager alloc]init];
          
     });
     return VLManager;
}

- (instancetype)init
{
    self.manager = [TIMManager sharedInstance];
    [self.manager setMessageListener:self];
    [self.manager initLogSettings:NO logPath:@"log"];
//    [self.manager setLogLevel:TIM_LOG_NONE];
     [self.manager initSdk:CloundSDK];
     
     return self;
}

- (void)setRoomID:(NSString *)RoomID
{
     if (RoomID) {
          self.conversation = [self.manager getConversation:TIM_GROUP receiver:RoomID];
     }else{
          self.conversation = nil;
     }
     _RoomID = RoomID;
}

- (void)loginWithidentifier:(NSString *)iden accountType:(NSString *)accout userSig:(NSString *)Sig succ:(VLSLoginSucc)succ fail:(VLSFail)fail
{
    
     NSAssert(Sig, @"没有签名");
     TIMLoginParam *param = [[TIMLoginParam alloc]init];
     param.identifier = iden;
     param.accountType = acountType;
     param.userSig = Sig;
     param.sdkAppId = CloundSDK;
     param.appidAt3rd = [NSString stringWithFormat:@"%d",CloundSDK];
     
     [self.manager login:param succ:^{
         succ();
     } fail:^(int code, NSString *msg) {
         [_delegate receiveFailMessage:msg code:code];
         fail(code,msg);
     }];

}
// v2.0_future

- (void )getSelfProfile:(TIMGetProfileSucc)succ fail:(TIMFail)fail
{
    
    [[TIMFriendshipManager sharedInstance] GetSelfProfile:^(TIMUserProfile *profile) {
        succ(profile);
        
    } fail:^(int code, NSString *msg) {
        NSLog(@"%d:%@",code,msg);
        [_delegate receiveFailMessage:msg code:code];

    }];

}

- (BOOL)GetLoginState
{
    if ([self.manager getLoginStatus] == 1 || [self.manager getLoginStatus] == 2) {
        return YES;
    } else {
        return NO;
    }
}

- (void)logoutIM
{
    [self.manager logout:^{
        
        
    } fail:^(int code, NSString *msg) {
        
    }];
}

- (void)CreatAVChatRoomGroup:(NSString *)RoomName RoomID:(NSString *)RoomID succ:(VLSCreateGroupSucc)succ fail:(VLSFail)fail
{
     
    [[TIMGroupManager sharedInstance]CreateGroup:@"AVChatRoom" members:nil groupName:RoomName groupId:RoomID succ:^(NSString *groupId) {
        self.RoomID = groupId;
        succ(@"创建成功");
    } fail:^(int code, NSString *msg) {
        fail(code,msg);
        [_delegate receiveFailMessage:msg code:code];

    }];
}
/**
 *  禁言系统回调
 *
 *  @param identifier 
 *  @param stime
 *  @param succ
 *  @param fail
 */
- (void)ModifyGroupMemberInfoSetSilenceUser:(NSString*)identifier stime:(uint32_t)stime succ:(VLSSucc)succ fail:(VLSFail)fail
{
     [[TIMGroupManager sharedInstance] ModifyGroupMemberInfoSetSilence:self.RoomID user:identifier stime:stime succ:^{
          succ();
         
     } fail:^(int code, NSString *msg) {
          fail(code,msg);
         [_delegate receiveFailMessage:msg code:code];

     }];

}
- (void)DeleteGroupSucc:(VLSSucc)succ fail:(VLSFail)fail
{
     [[TIMGroupManager sharedInstance] DeleteGroup:self.RoomID succ:^{
//          self.RoomID = nil;
          succ();
     } fail:^(int code, NSString *msg) {
          fail(code,msg);
         [_delegate receiveFailMessage:msg code:code];
     }];
}


- (void)JoinGroupRoomID:(NSString *)RoomID Succ:(VLSSucc)succ fail:(VLSFail)fail
{
     [[TIMGroupManager sharedInstance]JoinGroup:RoomID msg:nil succ:^{
          self.RoomID = RoomID;
          succ();
          
     } fail:^(int code, NSString *msg) {
          fail(code,msg);
         [_delegate receiveFailMessage:msg code:code];

     }];
}

- (void)QuitAVChatRoomGroupsucc:(VLSSucc)succ fail:(VLSFail)fail
{
     [[TIMGroupManager sharedInstance] QuitGroup:self.RoomID succ:^{
          succ();
//          self.RoomID = nil;
     } fail:^(int code, NSString *msg) {
          fail(code,msg);
         
        [_delegate receiveFailMessage:msg code:code];
         
     }];
    
}

- (void)sendC2CChatMessage:(TIMMessage *)message receiver:(NSString *)receiver succ:(VLSSucc)succ fail:(VLSFail)fail;
{
     TIMConversation *conv = [[TIMManager sharedInstance] getConversation:TIM_C2C receiver:receiver];
      [conv sendMessage:message succ:^{
           succ();
      } fail:^(int code, NSString *msg) {
           fail(code,msg);
          [_delegate receiveFailMessage:msg code:code];

      }];
     
}


- (void)sendChatRoomMessage:(TIMMessage *)ChatMsg succ:(VLSSucc)succ fail:(VLSFail)fail
{
     [self.conversation sendMessage:ChatMsg succ:^{
          succ();
     } fail:^(int code, NSString *msg) {
          fail(code,msg);
         [_delegate receiveFailMessage:msg code:code];

     }];

}

- (void)onNewMessage:(NSArray *) msgs
{
     if ([_delegate respondsToSelector:@selector(receiveNewMessage:)]) {
          [self.delegate receiveNewMessage:msgs];
     }
}

- (void)setNikename:(NSString *)name succ:(VLSSucc)succ fail:(VLSFail)fail
{
    if ([name isKindOfClass:[NSNull class]]) {
        return;
    }
    [[TIMFriendshipManager sharedInstance] SetNickname:name succ:^{
        NSLog(@"nikename success");
        succ();
    } fail:^(int code, NSString *msg) {
        [_delegate receiveFailMessage:msg code:code];

    }];
}
- (void)setFaceUrl:(NSString *)url succ:(VLSSucc)succ fail:(VLSFail)fail
{
    if ([url isKindOfClass:[NSNull class]]) {
        return;
    }
    [[TIMFriendshipManager sharedInstance] SetFaceURL:url succ:^{
        NSLog(@"SetFaceURL success");
        succ();
    } fail:^(int code, NSString *msg) {
        [_delegate receiveFailMessage:msg code:code];

    }];
}

- (void)getUserProfile:(NSString*)userId succ:(VLSGetProfileSucc)succ fail:(VLSFail)fail;
{
    [[TIMFriendshipManager sharedInstance] GetUsersProfile:@[userId] succ:^(NSArray *friends) {
        NSMutableArray *array = [NSMutableArray arrayWithArray:friends];
        if (array.count == 1) {
            TIMUserProfile * user = [array objectAtIndex:0];
            VLSUserProfile * pro = [[VLSUserProfile alloc] init];
            [pro GetUserProfileFromTIMUserProfile:user];
            succ(pro);
        }
        
    } fail:^(int code, NSString *msg) {
        
        fail(code, msg);
    }];

}

- (void)getMenberListsucc:(VLSGroupMemberSucc)succ fail:(VLSFail)fail;
{
     [[TIMGroupManager sharedInstance] GetGroupMembers:self.RoomID succ:^(NSArray *members) {
          NSMutableArray *array = [NSMutableArray array];
          for (TIMGroupMemberInfo *mem in members)
          {
              if (mem.role == TIM_GROUP_MEMBER_ROLE_SUPER) {
                  
              }else{
                  if (array.count < 10) {
                      [array addObject:[mem member]];
                  }
              }
          }
          
          [[TIMFriendshipManager sharedInstance] GetUsersProfile:array succ:^(NSArray *friends) {
               NSMutableArray *array = [NSMutableArray array];
               for (TIMUserProfile *u in friends)
               {
                    [array addObject:u];
                    
               }
               succ(array);
               
          } fail:^(int code, NSString *msg) {

               fail(code, msg);
              [_delegate receiveFailMessage:msg code:code];


          }];
     } fail:^(int code, NSString *msg) {
          fail(code, msg);
         [_delegate receiveFailMessage:msg code:code];

     }];
}

//获取成员个数
- (void)getGroupInfoSucc:(VLSGroupInfoSucc)succ fail:(VLSFail)fail
{
    
    [[TIMGroupManager sharedInstance] GetGroupInfo:@[self.RoomID] succ:^(NSArray *arr) {
        TIMGroupInfo *info = [arr objectAtIndex:0];
        VLSGroupInfo *vinfo = [[VLSGroupInfo alloc] init];
        vinfo.memberNum = info.memberNum;
        vinfo.owner = info.owner;
        vinfo.group = info.group;
        succ (vinfo);
    } fail:^(int code, NSString *msg) {
        fail (code , msg);
    }];
}


@end
