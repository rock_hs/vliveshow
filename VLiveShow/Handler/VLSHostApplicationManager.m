//
//  VLSHostApplicationManager.m
//  VLiveShow
//
//  Created by Davis on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSHostApplicationManager.h"
#import "HTTPFacade.h"
#import "ManagerUtil.h"

@implementation VLSHostApplicationManager

#pragma mark - host application
/**
 *  获取申请状态
 *
 *  @param callback 回调
 */
+ (void)getHostApplicationStatusCallback:(ManagerCallBack *)callback
{
    
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    
    [HTTPFacade getHostApplicationStatusCallback:http];
    
}

/**
 *  确认申请审批结果
 *
 *  @param applicationId applicationId description
 *  @param callback      应答
 */
+ (void)postHostApplicationConfirm:(NSString *)applicationId
                          callback: (ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    
    [HTTPFacade postHostApplicationConfirm:applicationId callback:http];
}

/**
 *  获取主播申请资料
 *
 *  @param callback 应答
 */
+ (void)getHostApplicationCallback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    
    [HTTPFacade getHostApplicationCallback:http];
}

/**
 *  上传主播申请图片
 *
 *  @param image    imageData
 *  @param callback 应答
 */
+ (void)postHostApplicationImage:(NSData *)image imageName:(NSString*)imageName callback: (ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    [HTTPFacade postHostApplicationImage:image imageName:imageName callback:http];
    
}

/**
 *  提交主播申请资料
 *
 *  @param callback 应答
 */
+ (void)postHostApplication:(NSDictionary *)param callback:(ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    [HTTPFacade postHostApplication:param callback:http];
}

/**
 *  删除主播申请图片
 *
 *  @param image    imageURL
 *  @param callback 应答
 */
+ (void)deleteHostApplicationImage:(NSString *)imageURL callback: (ManagerCallBack *)callback
{
    HttpCallBack* http = [ManagerUtil defaultHttpCallBack:callback process:^id(NSDictionary* dict){
        return dict;
    }];
    [HTTPFacade deleteHostApplicationImage:imageURL callback:http];
}

@end
