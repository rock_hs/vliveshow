//
//  VLSUserTrackContext.m
//  VLiveShow
//
//  Created by VincentX on 10/24/16.
//  Copyright © 2016 vliveshow. All rights reserved.
//

#import "VLSUserTrackContext.h"

@implementation VLSUserTrackContext

- (instancetype)init
{
    return [self initWithUUID: NULL];
}

- (_Nonnull instancetype)initWithUUID: (NSUUID* _Nullable) uuid
{
    self = [super init];
    if (self)
    {
        if (uuid != nil)
        {
            self.idenetifier = uuid;
        }
        else
        {
            self.idenetifier = [NSUUID UUID];
        }
    }
    return self;
}

@end
