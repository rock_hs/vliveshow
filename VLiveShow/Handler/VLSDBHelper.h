//
//  VLSDBHelper.h
//  VLiveShow
//
//  Created by SuperGT on 16/7/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^complete)(NSMutableArray *arr);

typedef void(^operation)(BOOL isSave);

@interface VLSDBHelper : NSObject

+ (instancetype)defaultHelper;

- (void)getAllMessageArrFromDB:(complete)block;

+ (void)updateTeamModelFromDB;

+ (void)updateLiveModelFromDB;

+ (void)updateFansModelFromDB;

+ (void)teamSearchWithPage:(NSInteger)page pageSize:(NSInteger)pageSize completeHandler:(complete)block;

+ (void)LiveSearchWithPage:(NSInteger)page pageSize:(NSInteger)pageSize completeHandler:(complete)block;

+ (void)fansSearchWithPage:(NSInteger)page pageSize:(NSInteger)pageSize completeHandler:(complete)block;

+ (void)ThreadSaveFansModelToDB:(NSDictionary *)dict TIMMessage:(TIMMessage *)msg elem:(TIMCustomElem *)elem completeHandler:(operation)operation;

+ (void)ThreadSaveLiveModelToDB:(NSDictionary *)dict TIMMessage:(TIMMessage *)msg elem:(TIMCustomElem *)elem completeHandler:(operation)operation;

+ (void)ThreadSaveTeamModelToDB:(NSDictionary *)dict TIMMessage:(TIMMessage *)msg elem:(TIMCustomElem *)elem completeHandler:(operation)operation;

+ (void)UpdateTeamDBandReload:(complete)complete;

+ (void)UpdateLiveDBandReload:(complete)complete;

+ (void)UpdateFansDBandReload:(complete)complete;

+ (void)DeleteAllDB;

@end
