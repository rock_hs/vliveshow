//
//  VLSHostApplicationManager.h
//  VLiveShow
//
//  Created by Davis on 16/8/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSHostApplicationManager : NSObject




#pragma mark - host application
/**
 *  获取申请状态
 *
 *  @param callback 应答
 */
+ (void)getHostApplicationStatusCallback:(ManagerCallBack *)callback;

/**
 *  确认申请审批结果
 *
 *  @param applicationId applicationId description
 *  @param callback      应答
 */
+ (void)postHostApplicationConfirm:(NSString *)applicationId
                          callback:(ManagerCallBack *)callback;

/**
 *  获取主播申请资料
 *
 *  @param callback 应答
 */
+ (void)getHostApplicationCallback:(ManagerCallBack *)callback;

/**
 *  上传主播申请图片
 *
 *  @param image    imageData
 *  @param callback 应答
 */
+ (void)postHostApplicationImage:(NSData *)image imageName:(NSString*)imageName callback: (ManagerCallBack *)callback;



/**
 *  提交主播申请资料
 *
 *  @param callback 应答
 */
+ (void)postHostApplication:(NSDictionary *)param callback:(ManagerCallBack *)callback;


/**
 *  删除主播申请图片
 *
 *  @param image    imageURL
 *  @param callback 应答
 */
+ (void)deleteHostApplicationImage:(NSString *)imageURL callback: (ManagerCallBack *)callback;

@end
