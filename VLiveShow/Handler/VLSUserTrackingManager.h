//
//  VLSUserTrackingManager.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceModel.h"
#import "AccountModel.h"
#import "VLSAVInteractionModel.h"
#import "VLSJoinRoomModel.h"
#import "GiftModel.h"

@class VLSUserTrackContext;

@interface VLSUserTrackingManager : NSObject

//单例对象
+ (instancetype)shareManager;

/****登录注册追踪****/
//注册
- (void)trackingSignup:(AccountModel *)model;
//登录
- (void)trackingLogin:(AccountModel *)model withLoginType:(NSString *)loginType;
//重设
- (void)trackingResetpassword:(AccountModel *)model;

/****直播间追踪****/
//进入房间
- (VLSUserTrackContext*)trackingJoinroom:(VLSJoinRoomModel *)model;
//离开房间
- (void)trackingQuitroom:(NSString *)leaveType context: (VLSUserTrackContext*)context;

- (void)trackingRoomInterruption: (NSString*)type context: (VLSUserTrackContext*)context;
//绑定手机号
- (void)trackingBindingphone:(AccountModel *)model bindType:(NSString *)bindType;
- (void)trackingRoomBindPhone:(AccountModel *)model bindType:(NSString *)bindType;
//申请连线
- (void)trackingLinkrequest:(VLSAVInteractionModel *)model;
//主播同意申请
- (void)trackingAcceptlink:(VLSAVInteractionModel *)model;
//主播(取消)静音
- (void)trackingMute:(NSString*)isMute;
//嘉宾(取消)静音
- (void)trackingGuestMute:(NSString *)mute;
//立即上镜
- (void)trackingUponImmediately;
//切换主屏
- (void)trackingChangeactor:(NSString *)uid;
//取消连线
- (void)trackingCancellink:(VLSAVInteractionModel *)model;
//邀请嘉宾
- (void)trackingInvite:(VLSAVInteractionModel *)model;
//送礼物
- (void)trackingGift:(GiftModel *)model;
//分享
- (void)trackingShareStatus:(NSString *)shareStatus platform:(NSString *)platform roomID:(NSString *)roomid;
//嘉宾同意邀请
- (void)trackingAgreeGotoRoom:(NSString *)roomid anchorid:(NSString *)anchorid fromWhere:(NSString *)where;
/****我的界面追踪****/
//查看profile
- (void)trackingViewprofile:(NSString *)userID;
//改变配置
- (void)trackingChangeProfile:(NSString *)setString status:(NSString *)status;

//获取用户信息
-(NSString*)getUserInfo;
//获取设备信息
- (void)getDeviceInfo;

//获取位置信息
- (void)tackingLocation:(NSString *)location longitude:(NSString *)longitude latitude:(NSString *)latitude;
//敏感词追踪
- (void)trackingSendSensitive:(NSString *)sensitive;
//退出app
- (void)trackingUserExitLiveRoom:(NSString *)leave;

- (void)trackingOpenAPPTime:(NSString *)timeStr backOrFore:(NSString *)isOrBack;

- (void)trackingWelcomeApp;

// New activating api
- (void)trackingActivate;

//补充tracking记录

//关注、拉黑、举报
- (void)trackingFoucswithBeFocusedUserid:(NSString *)beFocusedUserid action:(NSString *)action;

//关注、拉黑、举报
- (void)trackingUserAction:(NSString *)targetUserid action:(NSString *)action;

- (void)trackingEnterIntoBackground;

- (void)trackingTerminate;

- (void)trackingIAP:(AccountModel *)model orderId: (NSString*)orderId amount: (NSInteger)acount currencyType: (NSString*)currencyType payType: (NSString*)payType;


- (VLSUserTrackContext*)trackingLinkBegin:(VLSAVInteractionModel *)model;
- (void)trackingLinkEnd: (VLSAVInteractionModel *)model context: (VLSUserTrackContext*)context;

- (void)trackingClickWithAction: (NSString*)action extraInfo: (NSDictionary*)extraInfo;

@end


