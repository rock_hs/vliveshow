//
//  VLSGiftManager.m
//  VLiveShow
//
//  Created by LK on 16/8/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSGiftManager.h"
#import "VLSUserTrackingManager.h"
#import "VLSLiveHttpManager.h"
#import "VLiveShow-Swift.h"

@interface VLSGiftManager ()
@end

@implementation VLSGiftManager

+ (instancetype)sharedManager
{
  static VLSGiftManager *_sharedManager = nil;
  static dispatch_once_t onceToken;
  
  dispatch_once(&onceToken, ^{
    _sharedManager = [[self alloc ] init];
    
  });
  return _sharedManager;
}

/* 发送礼物消息 */
- (void)sendGiftMessage:(GiftModel *)sendModel
{
  
  sendModel.timerStamp = self.timerStamp;
  sendModel.currentCount = self.currentCount;
  ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
  callback.updateBlock = ^(NSDictionary* result)
  {
     NSLog(@"result -- %@",result);
  };
  callback.errorBlock = ^(id result)
  {
    NSLog(@"result -- %@",result);
  };
  
  [VLSLiveHttpManager sendGift:sendModel callback:callback];
  
//  NSString *str = [NSString stringWithFormat:@"%d&%@&%@&%@&%@&%@&%ld&%@&%@&%ld&%@&%ld",VLS_GIFT,sendModel.userID,sendModel.giftID,sendModel.headImage,sendModel.name,sendModel.giftName,(long)self.currentCount,self.timerStamp,sendModel.giftImage,(long)sendModel.giftType,sendModel.folderName,(long)sendModel.isLocal];
//  [[VLSMessageManager sharedManager] sendGroupMsg:str type:SendMsgTypeCustom succ:^{
//    
//  } fail:^(int code, NSString *msg) {
//    
//  }];
    
//    [[VLSUserTrackingManager shareManager] trackingGift:sendModel];
}

/* 接收礼物 */
- (void)receiveGiftMessage:(NSArray *)giftMessage
{
  
  NSMutableString * UidAGiftId = [[NSMutableString alloc] init];
  if ([_delegate respondsToSelector:@selector(receiveNormalGiftMessage:)]) {

    GiftModel * model = [[GiftModel alloc] init];
    for (int i = 1 ; i < giftMessage.count; i ++) {
      
      switch (i) {
        case 1:
          
          break;
        case 2:
          model.userID = giftMessage[i];
          break;
        case 3:
          model.giftID = giftMessage[i];
          break;
        case 4:
          model.headImage = giftMessage[i];
          break;
        case 5:
          model.name = giftMessage[i];
          break;
        case 6:
          model.giftName = giftMessage[i];
          break;
        case 7:
          model.currentCount = [giftMessage[i] integerValue];
          break;
        case 8:
          model.timerStamp = giftMessage[i];
          [UidAGiftId appendString:[NSString stringWithFormat:@"%@%@%@",model.giftID,model.userID,model.timerStamp]];
          [[GiftCountModel sharedGiftCountModel].GiftArr addObject:UidAGiftId];
          break;
        case 9:
          model.giftImage = giftMessage[i];
          break;
        case 10:
          model.giftType = [giftMessage[i] integerValue];
          break;
        case 11:
          model.folderName = giftMessage[i];
          break;
        case 12:
          model.isLocal = [giftMessage[i] integerValue];
          break;
        case 13:
          model.aspectRatio = [giftMessage[i] integerValue];
          break;
        case 14:
          model.giftThirdImage = giftMessage[i];
          break;
        case 15:
          model.playOnceTime = [giftMessage[i] integerValue];
          break;
        case 16:
          model.repeatTimes = [giftMessage[i] integerValue];
          break;
        default:
          break;
          
      }
      
    }
    
    if ([model.userID integerValue] == [AccountManager sharedAccountManager].account.uid) {
      
      VLSSendGroupMsgModel *msg = [[VLSSendGroupMsgModel alloc] init];
      msg.message = model.giftName;
//      [[VLSMessageManager sharedManager] sendGroupMessage:msg];
      [AccountManager sharedAccountManager].account.diamondBalance = [giftMessage[1] longLongValue];
      
    }
      

    [_delegate receiveNormalGiftMessage:model];
    
  }
}
@end
