
//
//  HeartBeatManager.m
//  VLiveShow
//
//  Created by SuperGT on 16/6/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "HeartBeatManager.h"
#import "ManagerCallBack.h"
#import "VLSLiveHttpManager.h"
#import "VLSHeartManager.h"
#import "NSString+UserFile.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface HeartBeatManager ()

@property (nonatomic, strong) dispatch_source_t timer;

@end

@implementation HeartBeatManager

+ (instancetype)shareManager
{
    static HeartBeatManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[HeartBeatManager alloc]init];
    });
    return manager;
}

- (void)startHeartBeat:(NSString *)roomid
{
    NSString *timeoutstr = [VLSHeartManager shareHeartManager].heartModel.keepAliveInterval;
    
    if (!timeoutstr) {
        timeoutstr = @"20";
    }
    CGFloat timeout = [timeoutstr doubleValue];
  
    uint64_t interval = (uint64_t)(timeout * NSEC_PER_SEC);
    dispatch_queue_t queue = dispatch_queue_create("my queue", 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer, dispatch_time(DISPATCH_TIME_NOW, 0), interval, 0);
        dispatch_source_set_event_handler(_timer, ^()
    {
        NSLog(@"Timer %@", [NSThread currentThread]);
        [self sendHeartBeatRequest:roomid];
        
    });
    dispatch_resume(_timer);
}

- (void)sendHeartBeatRequest:(NSString *)roomid
{
    ManagerCallBack *callBack =[[ManagerCallBack alloc] init];
    callBack.updateBlock = ^(id result){
        NSLog(@"fasong ");
    };
    callBack.errorBlock = ^(id result){
        
        NSLog(@"fasong error");
        if ([result isKindOfClass: [ManagerEvent class]])
        {
            ManagerEvent* event = (ManagerEvent*)result;
            NSInteger uid = [AccountManager sharedAccountManager].account.uid;
            DeviceModel* model = [AccountManager getDeviceInformation];
            NSDictionary* dict = [model mj_keyValues];
            [[Crashlytics sharedInstance] recordError: event.error withAdditionalUserInfo: @{@"userid": @(uid), @"device": dict}];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName: HeartBeatManagerHeartBeatFailed object: self userInfo: @{}];
    };
    
    NSString *strNum = [NSString stringWithFormat:@"%ld",(long)[_delegate heartBeatWithWatchNum]];
    NSString *count = [NSString calculatedSum:strNum];
    
    
    [VLSLiveHttpManager postLiveInfo:roomid watchNum:[count integerValue] callback:callBack];

}

- (void)destoryHeartBeat
{
    self.timer = nil;
}

@end
