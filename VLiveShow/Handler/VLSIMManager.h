//
//  VLSIMManage.h
//  TXDemo
//
//  Created by SuperGT on 16/5/23.
//  Copyright © 2016年 GaoTong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VlSSendGroupMsgModel.h"
#import "VLSSendC2CMsgModel.h"
#import "VLSSendGiftModel.h"

#import "VLSBarrageViewModel.h"
#import "VLSMessageViewModel.h"
#import "VLSCustomGiftViewModel.h"
#import "VLSNormalGiftViewModel.h"
#import "VLSGroupInfo.h"
#import "VlSGift.h"
#import <ImSDK/ImSDK.h>
#import "VLSUserTrackingManager.h"
/////////////////////////////////////////////////////////
///  回调Block
/////////////////////////////////////////////////////////

/**
 *  获取消息回调
 *
 *  @param NSArray 消息列表
 */
typedef void (^VLSGetMsgSucc)(NSArray * msgs);

/**
 *  一般操作成功回调
 */
typedef void (^VLSSucc)();

/**
 *  操作失败回调
 *
 *  @param code 错误码
 *  @param msg  错误描述，配合错误码使用，如果问题建议打印信息定位
 */
typedef void (^VLSFail)(int code, NSString * msg);

/**
 *  登陆成功回调
 */
typedef void (^VLSLoginSucc)();

/**
 *  获取资源
 *
 *  @param NSData 资源二进制
 */
typedef void (^VLSGetResourceSucc)(NSData * data);



/**
 *  群创建成功
 *
 *  @param NSString 群组Id
 */
typedef void (^VLSCreateGroupSucc)(NSString * groupId);

/**
 *  群成员列表回调
 *
 *  @param NSArray 群成员列表
 */
typedef void (^VLSGroupMemberSucc)(NSArray * members);

/**
 *  群成员列表回调（分页使用）
 *
 *  @param NSArray 群成员（TIMGroupMemberInfo*）列表
 */
typedef void (^VLSGroupMemberSuccV2)(uint64_t nextSeq, NSArray * members);

/**
 *  群列表回调
 *
 *  @param NSArray 群列表
 */
typedef void (^VLSGroupInfoSucc)(VLSGroupInfo * info);


/**
 *  日志回调
 *
 *  @param lvl      输出的日志级别
 *  @param NSString 日志内容
 */
typedef void (^VLSLogFunc)(TIMLogLevel lvl, NSString * msg);


/**
 *  获取资料回调
 *
 *  @param profile 资料
 */
typedef void (^VLSGetProfileSucc)(VLSUserProfile * profile);



#pragma mark 消息代理
@protocol IMManagerDelegate <NSObject>

/**
 *  代理对象实现代理方法，当收到新消息时回调
 *
 *  @param message message 新消息
 */
- (void)receiveNewMessage:(NSArray *)messages;

- (void)receiveFailMessage:(NSString *)msg code:(int)code;

@end



@interface VLSIMManager : NSObject

/**
 *  收到新消息代理对象
 */

@property (nonatomic, assign) id<IMManagerDelegate> delegate;


@property (nonatomic, strong) TIMManager *manager;

// 聊天室设置room，初始化会话
@property (nonatomic,copy) NSString *RoomID;


/**
 *  实例化manager
 *
 *  @return 初始化TIMManager,GroupManager等等
 */
+ (instancetype)sharedInstance;


/**
 *  应该在登录注册成功之后，IM用户登录
 *
 *  @param iden   用户ID
 *  @param accout 用户类型
 *  @param Sig    用户签名
 *  @param succ   成功回调
 *  @param fail   失败回调
 */
- (void)loginWithidentifier:(NSString *)iden accountType:(NSString *)accout userSig:(NSString *)Sig succ:(TIMLoginSucc)succ fail:(TIMFail)fail;


#pragma mark 主播创建，禁言，解散接口
/**
 *  创建聊天室
 *
 *  @param RoomName 群名称
 *  @param succ     成功回调
 *  @param fail     失败回调
 */
- (void)CreatAVChatRoomGroup:(NSString *)RoomName RoomID:(NSString *)RoomID succ:(VLSCreateGroupSucc)succ fail:(VLSFail)fail;

/**
 *  禁言
 *
 *  @param identifier 用户ID
 *  @param stime      禁言时间
 *  @param succ       成功回调
 *  @param fail       失败回调
 */

- (void)ModifyGroupMemberInfoSetSilenceUser:(NSString*)identifier stime:(uint32_t)stime succ:(TIMSucc)succ fail:(TIMFail)fail;
/**
 *  解散群组
 *
 *  @param succ 成功回调
 *  @param fail 失败回调
 */

- (void)DeleteGroupSucc:(TIMSucc)succ fail:(TIMFail)fail;

#pragma mark 观众加入，退出接口

/**
 *  加入
 *
 *  @param RoomID 群ID
 *  @param succ   成功回调
 *  @param fail   失败回调
 */

- (void)JoinGroupRoomID:(NSString *)RoomID Succ:(TIMSucc)succ fail:(TIMFail)fail;

/**
 *  退出
 *
 *  @param succ 成功回调
 *  @param fail 失败回调
 */

- (void)QuitAVChatRoomGroupsucc:(TIMSucc)succ fail:(TIMFail)fail;

#pragma mark 发送消息接口

/**
 *  发送消息
 *
 *  @param ChatMsg 消息
 *  @param succ    成功回调
 *  @param fail    失败回调
 */

- (void)sendChatRoomMessage:(TIMMessage *)ChatMsg succ:(TIMSucc)succ fail:(TIMFail)fail;

- (void)sendC2CChatMessage:(TIMMessage *)message receiver:(NSString *)receiver succ:(TIMSucc)succ fail:(TIMFail)fail;

- (void)getMenberListsucc:(VLSGroupMemberSucc)succ fail:(VLSFail)fail;

- (void)getUserProfile:(NSString*)userId succ:(VLSGetProfileSucc)succ fail:(VLSFail)fail;

- (void)setNikename:(NSString *)name succ:(VLSSucc)succ fail:(VLSFail)fail;
- (void)setFaceUrl:(NSString *)url succ:(VLSSucc)succ fail:(VLSFail)fail;
#pragma mark 获取群信息（人数等）
- (void)getGroupInfoSucc:(VLSGroupInfoSucc)succ fail:(VLSFail)fail;

#pragma mark 获取当前登录的用户，如果未登录返回为nil，否则返回用户id
- (BOOL)GetLoginState;
//获取自己的信息
- (void )getSelfProfile:(TIMGetProfileSucc)succ fail:(TIMFail)fail;

#pragma mark 退出
- (void)logoutIM;

@end
