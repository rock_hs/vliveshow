//
//  VLSUserTrackContext.h
//  VLiveShow
//
//  Created by VincentX on 10/24/16.
//  Copyright © 2016 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSUserTrackContext : NSObject
@property(nonnull, nonatomic, strong) NSUUID* idenetifier;
- (_Nonnull instancetype)initWithUUID: ( NSUUID* _Nullable )uuid;
@end
