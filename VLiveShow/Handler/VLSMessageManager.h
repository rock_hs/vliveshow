//
//  VLSMessageManager.h
//  XmppDemo
//
//  Created by 李雷凯 on 16/5/17.
//  Copyright © 2016年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSIMManager.h"
#import "VLSGiftManager.h"
#import "VLSSendGroupMsgModel.h"
#import "VLSSendC2CMsgModel.h"
#import "VLSSendGiftModel.h"

#import "VLSBarrageViewModel.h"
#import "VLSMessageViewModel.h"
#import "VLSCustomGiftViewModel.h"
#import "VLSNormalGiftViewModel.h"
#import "VLSUserProfile.h"
#import "VlSGift.h"

#import "VLSVideoViewModel.h"
#import "VLSVideoLayoutModel.h"
#import "VLSVideoAnchorModel.h"

#import "VLSUserInfoViewModel.h"
#import "VLSAVMultiManager.h"
#import "VLSUserTrackingManager.h"
//自定义消息命令
#define VLS_SEPERATOR @"&"
//礼物  VLS_GIFT&1001&10
#define VLS_GIFT              0
//赞
#define VLS_PRAISE            1
//弹幕
#define VLS_TM                2
//加入房间
#define VLS_JOIN_ROOM         10
//退出房间
#define VLS_QUIT_ROOM         11
//秀票
#define VLS_TICKET            30
//目标人数
#define VLS_LIVE_COUNT        31
// 机器人加入直播间
#define VLS_ROOT_JOIN_ROOM    32
//拉黑
#define VLS_BLACK             40
//系统消息
#define VLS_SYSTERM_MSG       50



#define VLS_GUEST_REQUEST       100     //嘉宾申请连线
#define VLS_ANCHOR_REJECT       101
#define VLS_ANCHOR_CANCEL       102
#define VLS_ANCHOR_ACCEPT       103
#define VLS_ANCHOR_REPLACE      104
#define VLS_ANCHOR_MUTE         105
#define VLS_GUEST_MUTE_SELF     106
#define VLS_GUEST_RQT_CANCEL    107
#define VLS_GUEST_CANCEL        108
#define VLS_GUEST_ON_CAMERA     109
#define VLS_ANCHOR_INVITE       111

#define VLS_FORBIDSENDMSG           121
#define VLS_FORBIDSENDMSG_CANCEL    122

#define VLS_INIT                110

#define VLS_VTICKETS_UPDATED    115

#define VLS_TEAM                201
#define VLS_LIVEREMIND          202
#define VLS_FANS                203

//账号封停
#define VLS_ACCOUNT_CLOSURE     205
//点亮屏幕
#define VLS_SEND_PRAISE         120

typedef NS_ENUM(NSUInteger) {
    TYPE_NORMALMESSAGE=10011,//普通消息
    TYPE_JOINOROUT,//进入
    TYPE_SUPERJOIN,//超级管理员
    TYPE_GIFT,//礼物消息
    TYPE_BARRAGE,//弹幕
    TYPE_GREENMESSAGE,//绿色消息
    TYPE_FIRSTJOINROOMMESSAGE,//刚进入直播间时的系统公告
    TYPE_SYSTEMMESSAGE,//系统红色公告字体
    TYPE_SEND_PRAISE,   //点亮了屏幕
    TYPE_FORBIDSENDMSG, //禁止发言
    TYPE_FORBIDSENDMSG_CANCEL,  //取消禁止发言
}MESSAGETYPE;

typedef enum {
    SendMsgTypeText     = 0,
    SendMsgTypeCustom   = 1,
    
}SendMsgType;

@protocol VLSMessageManagerDelegate<NSObject>
@optional
/**
 *  收到常规的消息
 *
 *  @param messageList
 */
- (void)receiveNormalMessage:(NSMutableArray *)messageList;

/**
 *  收到自定义的礼物
 *
 *  @param giftList VLSCustomGiftViewModel 数组
 */
- (void)receiveCustomGiftMessage:(NSMutableArray *)giftList;

/**
 *  收到弹幕消息
 *
 *  @param barrageList VLSBarrageViewModel 数组
 */
- (void)receiveBarrageMessage:(NSMutableArray *)barrageList;
/**
 *  收到赞消息
 *
 *  @param count  数量
 */
- (void)receivePariseMessage:(NSInteger)count;

/**
 *  收到秀票更新
 */
- (void)receiveUpdateTicket:(NSString *)count;

/**
 *  收到直播间人数更新
 */
- (void)receiveUpdateMemberCount:(NSString *)count;


/**
 *  新增群成员
 *
 *  @param userList  users
 */
- (void)receiveAddMembers:(NSArray *)userList isRoot:(BOOL)root;

/**
 *  删除群成员
 *
 *  @param userList  users
 */
- (void)receiveDeleteMembers:(NSArray *)userList;

/**
 *  更新群成员
 *
 *  @param userList  users
 */
- (void)receiveUpdataMembers:(NSArray *)userList;

/**
 *  收到解散群的群的消息
 *
 *  @param userList  users
 */
- (void)receiveDeleteGroup;

/**
 *  收到设置管理员的群消息
 */
- (void)receiveSetAdminToGroup;

/**
 *  收到取消管理员的群消息
 */
- (void)receiveCancleAdminToGroup;

/**
 *  收到禁言的群消息
 */
- (void)receiveForbidSendMsg:(NSArray *)userList;

/**
 *  收到取消禁言的群消息
 */
- (void)receiveForbidSendMsgCancel:(NSArray *)userList;

@optional

/**
 *  主播收到嘉宾静音
 */
- (void)receiveGuestMuteSelf:(BOOL)mute;


/**
 *  主播收到申请连线请求取消
 */
- (void)receiveGuestCancelLinkRequest:(VLSUserProfile *)user;

/**
 *  主播收到申请连线请求
 */
- (void)receiveLinkRequest:(VLSUserProfile *)user;

/**
 *  收到立即上镜
 */
- (void)receiveLinkOnCamera:(VLSUserProfile *)user;

/**
 *  嘉宾收到禁麦
 */
- (void)receiveAnchorMuteMic:(BOOL)mute;
/**
 *  观众收到被主播拉黑
 */
- (void)receiveSendToBlacklist;

/**
 *  观众收到申请连线成功
 */
- (void)receiveLinkRequestAccept;

/**
 *  观众收到申请连线拒绝
 */
- (void)receiveLinkRequestReject;
/**
 *  观众收到申请连线取消
 */
- (void)receiveLinkRequestCancel;
/**
 *  主播邀请嘉宾
 */
- (void)anchoInviteWithAnchoPicture:(NSString *)url NickName:(NSString *)Name;
/**
 *  收到申请连线成功更新布局
 *
 *  @param liveAnchors VLSVideoViewModel 数组
 */
- (void)receiveUpdateLiveLayout:(NSMutableArray *)liveAnchors;

/**
 *  更新房间名部分信息显示
 *
 *
 */
- (void)receiveUpdataAnchorView:(VLSVideoViewModel*)viewModel;

/**
 *  观众收到自己取消连线
 */
- (void)receiveGuestCancelLink;

/**
 *  用户收到推送消息
 */
- (void)receiveMail;

@end



@interface VLSMessageManager : NSObject

@property (nonatomic, weak) id <VLSMessageManagerDelegate>delegate;
@property (nonatomic, strong) VLSUserProfile *host;     //自己的IM资料
@property (nonatomic, strong) VLSUserProfile *anchor;   //主播的IM资料
@property (nonatomic, assign) BOOL selfIsGuest;   //自己是嘉宾
@property (nonatomic, assign) BOOL selfIsVoiceMute;   //自己被静音
@property (nonatomic, assign) BOOL newMail;

@property (nonatomic, assign) BOOL disableJoinRoomMessage;

@property (nonatomic, strong) dispatch_source_t messageTimer;
@property (nonatomic, strong) NSMutableArray<VLSMessageViewModel *> *normalMessages;  //常规文本消息数组
@property (nonatomic, strong) NSMutableArray<VLSVideoViewModel *> *liveAnchors;  //嘉宾
@property (nonatomic, strong) NSString* lessonId;
+ (instancetype)sharedManager;
- (NSString *)getRoomId;

- (void)setNikename:(NSString *)name;
- (void)setFaceUrl:(NSString *)faceUrl;

//首次布局
- (void)initFirstAVroom:(NSMutableArray *)array;
//根据用户ID判断用户是否正在直播
- (BOOL)checkUserIsLiving:(NSString *)userId;
//根据用户ID判断用户是否是主播
- (BOOL)checkUserIsAnchor:(NSString *)userId;
//根据用户ID判断用户是否是嘉宾
- (BOOL)checkUserIsGuests:(NSString *)userId;
//根据用户ID判断用户是否是观众
- (BOOL)checkUserIsAudience:(NSString *)userId;
//根据用户ID判断用户是否是自己
- (BOOL)checkUserIsSelf:(NSString *)userId;
//监测主播显示声音小红点
- (BOOL)checkAnchorShouldShowVoiceRedPoint;


- (BOOL)checkUserIsVoiceMute:(NSString *)userId;
- (BOOL)checkUserIsVoiceMuteBySelf:(NSString *)userId;


- (void)getSelfProfile;
//获取主播信息
- (void)getAnchorProfile:(NSString *)anchorId succ:(VLSGetProfileSucc)succ;

//发送拉黑消息
- (void)sendBlack:(BOOL)black toUser:(NSString *)toUserId;

//发送私信
- (void)sendC2CMessage:(VLSSendC2CMsgModel *)sendModel;

- (void)sendC2CMsg:(NSString *)msg to:(NSString *)userId type:(SendMsgType)type succ:(VLSSucc)succ fail:(VLSFail)fail;

- (void)sendGroupMsg:(NSString *)msg type:(SendMsgType)type succ:(VLSSucc)succ fail:(VLSFail)fail;


//发送群消息
//- (void)sendGroupMessage:(VLSSendGroupMsgModel *)sendModel;
- (void)sendGroupMessage:(VLSSendGroupMsgModel *)sendModel success:(void (^)(id result))success error:(void (^)(int code, NSString *msg))error;

//发送弹幕消息
- (void)sendBarrageMessage:(VLSSendGroupMsgModel *)sendModel;

//发送礼物消息
- (void)sendGiftMessage:(VLSSendGiftModel *)sendModel;

//发送点赞消息
//- (void)sendPraiseMessage:(NSInteger)count;
- (void)sendPraiseMessage:(NSInteger)count success:(void(^)())success fail:(void(^)(int code, NSString *msg))fail;
//发送点亮屏幕消息
- (void)sendPraiseMessage:(NSInteger)count sendModel:(VLSSendGroupMsgModel *)sendModel;

// 获取登录状态，有则返回ID，没有返回nil
- (BOOL)GetLoginState;
// 登出
- (void)logoutIM;
/**
 *  自动登录，userdeault中取得id和sig登录
 */
- (void)IMLogin;
//登录IM
- (void)loginIMidentifier:(NSString *)identifier userSig:(NSString *)userSig succ:(VLSSucc)succ fail:(VLSFail)fail;
//主播创建聊天室
- (void)CreatAVChatRoomGroup:(NSString *)RoomName RoomID:(NSString *)RoomID succ:(VLSCreateGroupSucc)succ fail:(VLSFail)fail;
//主播解散聊天室
- (void)deleteAVChartRoomGroupsucc:(VLSSucc)succ fail:(VLSFail)fail;

//主播设置管理员
- (void)setAdminToGtoup:(NSString *)userId;

//主播取消管理员
- (void)cancleAdminToGtoup:(NSString *)userId;

- (void)getMenberListsucc:(VLSGroupMemberSucc)succ fail:(VLSFail)fail;

- (void)getUserProfile:(NSString*)userId succ:(VLSGetProfileSucc)succ;

//群组信息
- (void)getGroupInfoSucc:(VLSGroupInfoSucc)succ fail:(VLSFail)fail;

//观众加入聊天室
- (void)joinGroupRoomID:(NSString *)RoomID succ:(VLSSucc)succ fail:(VLSFail)fail;
//观众退出聊天室
- (void)quitAVChatRoomGroupsucc:(VLSSucc)succ fail:(VLSFail)fail;

- (void)sendUpdateVTickets:(NSString *)RoomID succ:(VLSSucc)succ fail:(VLSFail)fail;

+(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString;



@end
