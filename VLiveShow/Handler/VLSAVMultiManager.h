//
//  VLSAVMultiManager.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/6/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLSAVInteractionModel.h"
#import "VLSVideoLayoutModel.h"
#import "VLSVideoAnchorModel.h"
#import "VLSLinkRequestModel.h"
#import "HTTPFacade.h"
#define GUEST_REQUEST       @"GUEST_REQUEST"
#define GUEST_REQ_CANCEL    @"GUEST_REQUEST_CANCEL"
#define GUEST_CANCEL        @"GUEST_CANCEL"

#define ANCHOR_ACCEPT       @"HOST_ACCEPT"
#define ANCHOR_CANCEL       @"HOST_CANCEL"
#define ANCHOR_REPLACE      @"HOST_REPLACE"
#define ANCHOR_MUTE         @"HOST_MUTE"
#define ANCHOR_REJECT       @"HOST_REJECT"
#define GUEST_MUTE_SELF     @"MUTE_SELF"
#define GUEST_MUTE_SELF     @"MUTE_SELF"
#define ANCHOR_UPD_LAYOUT   @"UPDATE_LAYOUT"
#define ANCHOR_INVITE       @"HOST_INVITE_GUEST"
#define GUEST_ON_CAMERA     @"GUEST_ON_CAMERA"
#define GUEST_ACCEPT_INVITE @"GUEST_ACCEPT_INVITATION"

#define MSG_C2C             @"C2C"
#define MSG_GROUP           @"GROUP"

typedef NS_ENUM(NSInteger, UserLinkStatus) {
    UserLinkStatusAudience,
    UserLinkStatusApplying,
    UserLinkStatusApplySuccess,
    UserLinkStatusOnCamera
};

@interface VLSAVMultiManager : NSObject
@property (nonatomic, assign) UserLinkStatus linkStatus;
+ (instancetype)sharedManager;
#pragma mark - 初始化直播间主屏的model
- (void)initFirstAVroomAnchorId:(NSString *)anchorId;

//邀请嘉宾
- (void)inviteHonoredGuestWithInviteIds:(NSArray *)inviteIds callback:(ManagerCallBack *)callback;

//观众申请连线主播
- (void)linkRequest:(NSString *)anchorId callback:(ManagerCallBack *)callback;

//嘉宾接收邀请
- (void)guestAcceptInvite:(NSString *)anchorId callback:(ManagerCallBack *)callback;

//观众取消连线（未连接成功之前）
- (void)linkRequestCancel:(NSString *)anchorId callback:(ManagerCallBack *)callback;

//嘉宾取消连线（连接成功后）
- (void)guestCancel:(NSString *)anchorId callback:(ManagerCallBack *)callback;

//主播接通连线
- (void)anchorAcceptLinkRequest:(NSArray *)requestIds callback:(ManagerCallBack *)callback;

//主播取消连线
- (void)anchorCancelLink:(NSArray *)requestIds callback:(ManagerCallBack *)callback;

//主播切换嘉宾位置
- (void)anchorReplacePositionFrom:(NSString *)userId to:(NSString *)userId callback:(ManagerCallBack *)callback;

//主播禁(取消)麦
- (void)anchorMuteModels:(NSArray *)muteModels callback:(ManagerCallBack *)callback;

//嘉宾自己禁麦
- (void)geneusMuteSelf:(BOOL)mute callback:(ManagerCallBack *)callback;

//嘉宾立即上镜
- (void)geneusOnCamera:(NSString *)anchorId callback:(ManagerCallBack *)callback;

//当主播暂时离开时候，自己离开时候，获取发送布局的权限，要发送当前布局给后台
- (void)guestLeaveWhenAnchorLeaveMoment;

//主播发送布局给后台
- (void)anchorSendLayoutToBackgroungCallback:(ManagerCallBack *)callback;

//获取请求连线的观众列表
- (void)getLinkRequestListCallback:(ManagerCallBack *)callback;


@end
