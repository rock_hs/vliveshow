//
//  VLSLiveHttpManager.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/28.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ManagerCallBack.h"
#import "HTTPFacade.h"
#import "VLSUserBaseInfoModel.h"
#import "VLSUserInfoViewModel.h"
#import "VLSLiveListModel.h"
#import "MineModel.h"
#import "VLSHotModel.h"
#import "VLSLiveInfoModel.h"
#import "VLSLiveInfoViewModel.h"
#import "VLSGuestModel.h"
#import "RankingModel.h"
#import "VLSAnchorAboutModel.h"
#import "VLSInviteGuestModel.h"
#import "VLSAnchorSearchModel.h"


#import "VLSAdListModel.h"
@interface VLSLiveHttpManager : NSObject
//获取直播列表
+ (void)getLiveList:(NSInteger)page callback:(ManagerCallBack *)callback
     WithSizeNumber:(NSInteger)sizeNumber;
//获取直播间广告列表
+ (void)getAdListCallback:(ManagerCallBack *)callback;
//获取直播信息

+ (void)getCourseListCallback: (ManagerCallBack *)callback;

+ (void)getLiveInfo:(NSString* )roomId callback:(ManagerCallBack *)callback;

//获取成员信息
+ (void)getUserInfo:(NSString *)userId callback:(ManagerCallBack *)callback;
//创建
+ (void)createLive:(VLSCreateLiveModel *)createLive callback:(ManagerCallBack *)callback;
//退出
+ (void)destroyLive:(NSString *)liveShowId callback:(ManagerCallBack *)callback;
//获取动态key
+ (void)RDKForRoomId:(NSString *)roomId callBack:(ManagerCallBack *)callback;
//关注列表
+ (void)getAttentList:(NSInteger)page userid:(NSInteger)userid callback:(ManagerCallBack *)callback;
// 获取关注人员的直播情况列表
+ (void)getFocusList:(NSInteger)page userid:(NSInteger)userid callback:(ManagerCallBack *)callback;

+ (void)getFanstList:(NSInteger)page userid:(NSInteger)userid callback:(ManagerCallBack *)callback;
//关注某人
+ (void)focusToUser:(NSString *)userId callback:(ManagerCallBack *)callback;
+ (void)cancelFocusToUser:(NSString *)userId callback:(ManagerCallBack *)callback;
//黑名单列表
+ (void)getuserBlackList:(NSInteger)page  callback:(ManagerCallBack*)callback;
//取消黑名单
+ (void)cancelBlack:(NSString*)usertoUID callback:(ManagerCallBack *)callback;
//加入黑名单
+ (void)addBlack:(NSString*)usertoUID callback:(ManagerCallBack *)callback;
//上传直播的心跳信息
+ (void)postLiveInfo:(NSString *)roomId watchNum:(NSInteger)watchNum callback:(ManagerCallBack *)callback;
//判断是否关注
+ (void)getrelationShip:(NSString*)usertoUID callBack:(ManagerCallBack *)callback;
//用户举报
+ (void)postReport:(NSString*)userToID callback:(ManagerCallBack*)callback type:(NSString*)type content:(NSString*)content remark:(NSString*)remark;
+(void)getRoomId:(NSString*)userID callback:(ManagerCallBack*)callback;
+ (void)getIsAttent:(NSString *)usertoUID callBack:(ManagerCallBack *)callback;
+(void)getuserInformation:(NSString*)fromUser Touser:(NSString*)touser callback:(ManagerCallBack *)callback;
// 用户跟踪
+ (void)shareTrackingLiveBefore:(NSString *)platform RoomID:(NSString *)roomid callback:(ManagerCallBack *)callback;
+ (void)shareTrackingLiveAfter:(NSString *)platform RoomId:(NSString *)roomid callback:(ManagerCallBack *)callback;
+ (void)shareTrackingLiveProcess:(NSString *)platform RoomId:(NSString *)roomid callback:(ManagerCallBack *)callback;

//传设备信息
+ (void)postDeviceInformation:(ManagerCallBack *)callback;
//welcome首次打开未注册
+ (void)postWelcomeDeviceInfo:(ManagerCallBack *)callback;

//上传是否接收提醒
+ (void)postLiveNotice:(NSString*)isReceiveOrNot callback:(ManagerCallBack *)callback;
+ (void)postLiveInvite:(NSString*)isReceiveOrNot callback:(ManagerCallBack *)callback;
+ (void)getLiveNotice:(ManagerCallBack *)callback;
//获取v票排行榜列表
+ (void)getRankingList:(ManagerCallBack*)callback userID:(NSString*)userid;
+ (void)getEndPageInfo:(NSString*)roomID callback:(ManagerCallBack *)callback;

//  获取观众列表
+ (void)getAudiuesInfo:(NSString *)roomid size:(NSInteger)size callback:(ManagerCallBack *)callback;
+ (void)getWatchNum:(NSString *)roomid callback:(ManagerCallBack *)callback;
+ (void)getRecommedLists:(ManagerCallBack *)callback;
// 获取跟主播相关的人
+ (void)getAllRelationshipPage:(NSInteger)page size:(NSInteger)size roomid:(NSString *)roomid callback:(ManagerCallBack *)callback;
// 搜索相关的人
+ (void)getSearchRelationshipPage:(NSInteger)page size:(NSInteger)size keyword:(NSString *)keyword callback:(ManagerCallBack *)callback;

// 根据关键词搜索用户(首页和关注页面的搜索)
+ (void)searchUserWithPage:(NSInteger)page size:(NSInteger)size keyword:(NSString *)keyword callback:(ManagerCallBack *)callback;
/* 获取礼物列表 */
+ (void)getGiftList:(ManagerCallBack *)callback;
/* 发送礼物 */
+ (void)sendGift:(GiftModel *)model callback:(ManagerCallBack *)callback;
+(void)postUserTrackingInfo:(NSDictionary*)dict callBcak:(ManagerCallBack *)callback;

+ (void)postDeviceActivateTrack: (NSDictionary *)param callback: (ManagerCallBack *)callback;

+ (void)getChannelContents: (NSString*)channelId page: (NSInteger)page number: (NSInteger)number callback: (ManagerCallBack *)callback;
@end
