//
//  VLSCountryAreaManager.h
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VLSCountryAreaManager : NSObject

@property (nonatomic, strong)NSString *area;
@property (nonatomic, strong)NSString *name;
+ (VLSCountryAreaManager *)shareManager;

@end
