//
//  NSDate.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import Foundation

var dateFormatter = DateFormatter()

extension Calendar {
    static func shared() -> Calendar {
        let calender = (Locale.current as NSLocale).object(forKey: NSLocale.Key.calendar)
        return calender as! Calendar
    }
}

extension Date {
    //时间戳转dateString
    static func dateStr(_ timestamp: Double, formatter: String = "yyyy年MM月dd日 HH:mm") -> String {
        self.setServiceFormatter(formatter)
        let serviceDate = Date(timeIntervalSince1970: TimeInterval(timestamp/1000))
        //当地时区
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: serviceDate)
    }
    
    //时间戳转date
    static func date(_ timestamp: Double, formatter: String = "yyyy-MM-dd HH:mm:ss") -> Date {
        let dateStr = self.dateStr(timestamp, formatter: formatter)
        if let date = dateFormatter.date(from: dateStr) {
            return date
        }
        return Date()
    }
    
    //dateString转date
    static func date(_ str: String, formatter: String = "yyyy年MM月dd日 HH:mm") -> Date {
        self.setServiceFormatter(formatter)
        if let date = dateFormatter.date(from: str) {
            return date
        }
        return Date()
    }
    
    //date转string
    static func date(_ date: Date, formatter: String = "yyyy-MM-dd") -> String {
        self.setServiceFormatter(formatter)
        //当地时区
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: date)
    }
    
    //service时区
    static func setServiceFormatter(_ formatter: String) {
        dateFormatter.dateFormat = formatter
        if let serviceTimeZone = NSTimeZone(name: "Asia/Shanghai") { //服务器时区
            dateFormatter.timeZone = serviceTimeZone as TimeZone!
        }
    }
    
    //比较两时间相距多久
    static func dateInterval(_ from: Date, to: Date) -> Int {
        let dateTuple = Date.dateComponents(from, to: to)
        var day = dateTuple.2
        if dateTuple.3 > 0 || dateTuple.4 > 0 || dateTuple.5 > 0 {
            day = day + 1
        }
        return day
    }
    
    //获取两时间相差多少秒（忽略年月日）
    static func dateIntervalSeconds(_ from: Date, to: Date) -> CLongLong {
        let dateTuple = Date.dateComponents(from, to: to)
        let seconds = dateTuple.3 * 60 * 60 + dateTuple.4 * 60 + dateTuple.5
        return CLongLong(seconds)
    }
    
    //获取两时间相差多少时间（忽略年月日）
    static func dateIntervalDateStr(_ from: Date, to: Date) -> String {
        let dateTuple = Date.dateComponents(from, to: to)
        var hourStr = String(dateTuple.3)
        if dateTuple.3 >= 0 && dateTuple.3 < 10 {
            hourStr = "0" + hourStr
        }
        var minuteStr = String(dateTuple.4)
        if dateTuple.4 >= 0 && dateTuple.4 < 10 {
            minuteStr = "0" + minuteStr
        }
        var secondStr = String(dateTuple.5)
        if dateTuple.5 >= 0 && dateTuple.5 < 10 {
            secondStr = "0" + secondStr
        }
        let dateStr = hourStr + ":" + minuteStr + ":" + secondStr
        return dateStr
    }
    
    //比较两时间相距多久components
    static func dateComponents(_ from: Date, to: Date) -> (Int, Int, Int, Int, Int, Int) {
        let components = Calendar.shared().dateComponents([.year, .month, .day, .hour, .minute, .second], from: from, to: to)
        let years = components.year
        let months = components.month
        let days = components.day
        let hours = components.hour
        let minutes = components.minute
        let seconds = components.second
        if let year = years, let month = months, let day = days, let hour = hours, let minute = minutes, let second = seconds {
            return (year, month, day, hour, minute, second)
        }
        return (0, 0, 0, 0, 0, 0)
    }
    
    //根据日期判断星期几
    static func weekday(from date: Date) -> String {
        let weekdays = ["日", "一", "二", "三", "四", "五", "六"]
        let component = Calendar.shared().dateComponents([.weekday], from: date)
        return StringSafty(ObjectForKeySafety(NSArraySafty(weekdays), index: IntSafty(component.weekday) - 1))
    }
    
    //根据日期判断几月几号
    static func day(from date: Date) -> String {
        let components = Calendar.shared().dateComponents([.month, .day], from: date)
        if components.day == 1 {//月初
            return StringSafty(String(format: LocalizedString("COURSE_WEEK_DAY"), IntSafty(components.month), IntSafty(components.day)))
        } else {
            return StringSafty(IntSafty(components.day))
        }
    }
    
//    //根据日期判断是否为当天
//    static func curday(from date: Date, to )
}
