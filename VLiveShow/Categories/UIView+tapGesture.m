//
//  UIView+tapGesture.m
//  VLiveShow
//
//  Created by tom.zhu on 10/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

#import "UIView+tapGesture.h"

@implementation UIView (tapGesture)
- (void)addGestureRecognizer:(SEL)sel target:(id)target {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:sel];
    [self addGestureRecognizer:tap];
}
@end
