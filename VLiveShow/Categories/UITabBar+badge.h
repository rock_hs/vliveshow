//
//  UITabBar+badge.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/8/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBar (badge)

- (void)showBadgeOnItemIndex:(int)index;   //显示小红点

- (void)hideBadgeOnItemIndex:(int)index; //隐藏小红点

- (void)removeBadgeOnItemIndex:(int)index;
@end
