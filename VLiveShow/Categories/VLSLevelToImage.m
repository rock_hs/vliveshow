//
//  VLSLevelToImage.m
//  sdAutoTable
//
//  Created by LK on 16/8/18.
//  Copyright © 2016年 LK. All rights reserved.
//

#import "VLSLevelToImage.h"

@implementation VLSLevelToImage

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (UIImage *)getLevelImageWidthLevel:(NSString *)level
{
    NSString *imageName ;
    NSInteger number = level.integerValue;
    if (1 <= number && number <= 9) {
        imageName = @"lv_1-9";
    }else if (10 <= number && number <= 19){
        imageName = @"lv_10-19";
    }else if (20 <= number && number <= 29){
        imageName = @"lv_20-29";
    }else if (30 <= number && number <= 39){
        imageName = @"lv_30-39";
    }else if (40 <= number && number <= 49){
        imageName = @"lv_40-49";
    }else if(50 <= number){
        imageName = @"lv_50";
    }else{
    }
    return [self drawText:level onImage:[UIImage imageNamed:imageName]];
}


+ (UIImage*)drawText:(NSString*)text onImage:(UIImage*)img{
    //开启位图上下文
    
    UIGraphicsBeginImageContextWithOptions(img.size, NO, 0);
    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
    NSInteger number = text.integerValue;
    if (1 <= number && number <= 9) {
        [text drawInRect:CGRectMake(5+img.size.width/2, 2, 30, img.size.height) withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10],NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }else{
        [text drawInRect:CGRectMake(0+img.size.width/2, 2, 30, img.size.height) withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10],NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }
    UIImage*currentImage=UIGraphicsGetImageFromCurrentImageContext();
    //关闭位图上下文
    UIGraphicsEndImageContext();
    return currentImage;
}

@end
