//
//  UIImage+colorImage.h
//  VLiveShow
//
//  Created by SuperGT on 16/6/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (colorImage)

+ (UIImage *)ImageFromColor:(UIColor *)color;
+ (UIImage *)ResizingModeStretch:(UIImage *)image;
- (UIImage *)fixOrientation:(UIImage *)aImage;
+ (UIImage *)v_setRadius:(CGFloat)radius image:(UIImage *)image size:(CGSize)size borderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth backgroundColor:(UIColor *)backgroundColor rectCornerType:(UIRectCorner)rectCornerType;

@end
