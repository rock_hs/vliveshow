//
//  UINavigationController+Rotate.m
//  VLiveShow
//
//  Created by tom.zhu on 2016/10/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "UINavigationController+Rotate.h"

@implementation UINavigationController (Rotate)
-(NSUInteger)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate {
    return [self.topViewController shouldAutorotate];
}
@end
