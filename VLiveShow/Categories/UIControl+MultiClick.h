//
//  UIControl+MultiClick.h
//  VLiveShow
//
//  Created by rock on 2016/12/26.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (MultiClick)

- (NSTimeInterval)cs_acceptEventInterval;

- (void)setCs_acceptEventInterval:(NSTimeInterval)cs_acceptEventInterval;

- (void)cs_sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event;

- (BOOL)cs_acceptEventIntervalFlag;

- (void)setCs_acceptEventIntervalFlag:(BOOL)cs_acceptEventIntervalFlag;

@end
