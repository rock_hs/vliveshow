//
//  UIColor+RZHexRGB.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/3/15.
//  Copyright © 2016年 SXW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RZHexRGB)

/**
 *  获取颜色
 *
 *  @param inColorString RGB颜色值
 *
 *  @return UIcolor对象
 */
+ (UIColor *)colorFromHexRGB:(NSString *)inColorString;

@end
