//
//  UIAlertController+Custom.h
//  VLiveShow
//
//  Created by Davis on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^UIAlertControllerTapBlock) (UIAlertController * __nonnull controller, UIAlertAction * __nonnull action, NSInteger buttonIndex);


@interface UIAlertController (Custom)

+ (nonnull instancetype)customAlertShowInViewController:(nonnull UIViewController *)viewController
                                              withTitle:(nullable NSString *)title
                                                message:(nullable NSString *)message
                                      cancelButtonTitle:(nullable NSString *)cancelButtonTitle
                                 destructiveButtonTitle:(nullable NSString *)destructiveButtonTitle
                                               tapBlock:(nullable UIAlertControllerTapBlock)tapBlock;

@end
