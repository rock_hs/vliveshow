//
//  UIViewController+Rotation.m
//  VLiveShow
//
//  Created by tom.zhu on 2016/10/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "UIViewController+Rotation.h"
#import "VLSShowViewController.h"

@implementation UIViewController (Rotation)
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    NSUInteger ret = 0;
    if (![self isKindOfClass:[VLSShowViewController class]]) {
        return UIInterfaceOrientationMaskPortrait;
    }
    return ret;
}

-(BOOL)shouldAutorotate {
    if ([self isKindOfClass:[VLSShowViewController class]]) {
        return YES;
    }
    return NO;
}

@end
