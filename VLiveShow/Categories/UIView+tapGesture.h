//
//  UIView+tapGesture.h
//  VLiveShow
//
//  Created by tom.zhu on 10/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (tapGesture)
- (void)addGestureRecognizer:(SEL)sel target:(id)target;
@end
