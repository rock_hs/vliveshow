//
//  VLSLevelToImage.h
//  sdAutoTable
//
//  Created by LK on 16/8/18.
//  Copyright © 2016年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface VLSLevelToImage : NSObject

+ (UIImage *)getLevelImageWidthLevel:(NSString *)level;


@end
