//
//  NSObject.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/9.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import Foundation

extension NSObject{
    
    func initializeWithDictionary(_ item: NSDictionary?){
        if item == nil {return}
        let mirror = Mirror(reflecting: self)
        for case let (label,_) in mirror.children{
            if item![label!] != nil{
                let mi = Mirror(reflecting: item![label!]!)
                if mi.subjectType == NSNull.classForCoder() {
                    self.setValue("", forKey: label!)
                }else{
                    self.setValue(item![label!]!, forKey: label!)
                }
            }
        }
    }
}
