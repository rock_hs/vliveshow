//
//  UIAlertController+Custom.m
//  VLiveShow
//
//  Created by Davis on 16/8/18.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "UIAlertController+Custom.h"

@implementation UIAlertController (Custom)

+ (nonnull instancetype)customAlertShowInViewController:(nonnull UIViewController *)viewController
                                              withTitle:(nullable NSString *)title
                                                message:(nullable NSString *)message
                                      cancelButtonTitle:(nullable NSString *)cancelButtonTitle
                                 destructiveButtonTitle:(nullable NSString *)destructiveButtonTitle
                                               tapBlock:(nullable UIAlertControllerTapBlock)tapBlock
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    __weak UIAlertController *controller = alertController;

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        tapBlock(controller,action,0);
        
    }];
    
    UIAlertAction *destructiveAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        tapBlock(controller,action,1);

    }];


    
    [alertController addAction:cancelAction];
    [alertController addAction:destructiveAction];

    
    [destructiveAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    [destructiveAction setValue:[UIFont boldSystemFontOfSize:14] forKey:@"titleFont"];
    
    [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    [viewController presentViewController:alertController animated:YES completion:nil];
    
    return alertController;
}
@end
