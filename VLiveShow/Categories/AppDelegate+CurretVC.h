//
//  AppDelegate+CurretVC.h
//  VLiveShow
//
//  Created by SuperGT on 16/7/7.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (CurretVC)

+ (UIViewController *)getCurrentVC;

@end
