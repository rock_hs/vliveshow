//
//  NSString+UserFile.h
//  VLiveShow
//
//  Created by SuperGT on 16/5/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UserFile)

+ (NSString *)UserFilePath;
+ (NSString *)thousandpointsNum:(NSInteger)num;
+ (NSString *) compareCurrentTime:(NSDate*) compareDate;
+ (NSString *)calculatedSum:(NSString *)watchNum;
@end
