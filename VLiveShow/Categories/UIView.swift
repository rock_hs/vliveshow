//
//  UIView.swift
//  VLiveShow
//
//  Created by vipabc on 2016/11/8.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

import Foundation

typealias UIViewClickHandler = (_ view: UIView) -> Void

class CustomView: NSObject {
    var clickHandler: UIViewClickHandler?
    
    init(clickHandler: UIViewClickHandler?){
        self.clickHandler = clickHandler
    }
    
//    func copyWithZone(zone: NSZone?) -> AnyObject {
//        return CustomView(clickHandler: self.clickHandler)
//    }
}

extension UIView {
    fileprivate struct AssociatedKeys {
        static var ViewClickKey = "viewClickKey"
    }
    
    /*****************************设置View的单击事件监控********************************/
    func handleClick(handler: @escaping UIViewClickHandler) {
        self.isUserInteractionEnabled = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIView.viewClick))
        self.addGestureRecognizer(tap)
        objc_setAssociatedObject(self, &AssociatedKeys.ViewClickKey, CustomView(clickHandler: handler), objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC)
    }
    
    func viewClick() {
        let callBack: CustomView? = objc_getAssociatedObject(self, &AssociatedKeys.ViewClickKey) as? CustomView
        if let callBack = callBack, let handler = callBack.clickHandler {
            handler(self)
        }
    }
    
    func clickAction(target: AnyObject, action: Selector) {
        self.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: target, action: action)
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    //添加小红点
    func dot(_ backgroundColor: UIColor = UIColor.red, frame: CGRect = CGRect.zero) {
        let dotView = UIView().then {
            if frame == CGRect.zero {
                $0.frame = CGRect(x: self.bounds.width - 4, y: -4, width: 8, height: 8)
            } else {
                $0.frame = frame
            }
            $0.tag = 8888
            $0.layer.cornerRadius = 4
            $0.backgroundColor = backgroundColor
        }
        self.addSubview(dotView)
    }
    
    func removeDot() {
        //按照tag值进行移除
        for view in self.subviews {
            if view.tag == 8888 {
                view.removeFromSuperview()
            }
        }
    }
}
