//
//  userRoleJudge.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/2.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface userRoleJudge : NSObject

+ (BOOL)userRoleJudge:(NSInteger)role compare:(NSInteger)integer;

@end
