//
//  NSString+UserFile.m
//  VLiveShow
//
//  Created by SuperGT on 16/5/30.
//  Copyright © 2016年 vliveshow. All rights reserved.
//
#define KFilePath @"User.plist"

#import "NSString+UserFile.h"
#import "VLSHeartModel.h"
#import "VLSHeartManager.h"

@implementation NSString (UserFile)

+ (NSString *)UserFilePath
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filepath = [path stringByAppendingPathComponent:KFilePath];
    NSLog(@"%@",filepath);
    return filepath;
    
}

+ (NSString *)thousandpointsNum:(NSInteger)num
{
//    NSInteger num = [self.contentView.anchor.OnLineNumber.text integerValue] + userList.count;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    
    formatter.numberStyle =NSNumberFormatterDecimalStyle;
    NSNumber *number = [NSNumber numberWithInteger:num];
    
    NSString *newAmount = [formatter stringFromNumber:number];
    return newAmount;
}

+ (NSString *)calculatedSum:(NSString *)watchNum
{
    
    NSInteger firstNum = 0;
    NSInteger secondNum = 0;
    NSInteger thirdNum = 0;
    NSInteger fourNum = 0;
    NSInteger peoples = [watchNum integerValue];
    NSInteger sum = 0;
    NSArray *config = [VLSHeartManager shareHeartManager].heartModel.headcount;


    if (!config) {
    
      
        if (peoples <= 10) {
            firstNum = peoples;
        }else if(peoples <= 10000 && peoples >10){
            secondNum = (peoples - 10)*20 + 10;
        }else if(peoples > 10000 && peoples <= 100000){
            thirdNum = (peoples - 10000)*10 + 10 + 9990*20;
        }else if(peoples > 100000 && peoples < 1000000){
            fourNum = (peoples - 100000) + 90000*10 + 9990*20 +10;
        }
    }else{
        
        for (int i = 0; i < config.count; i++) {
            NSDictionary *dict = config[i];
            NSInteger endzone = [[dict objectForKey:@"end"] integerValue];
            NSInteger startzone = [[dict objectForKey:@"start"] integerValue];
            NSInteger multi = [[dict objectForKey:@"multiplier"] integerValue];
            if (peoples <= endzone && peoples > startzone) {
                sum = (peoples - startzone) * multi;
                for (int j = 0; j < i; j++) {
                    NSDictionary *dict = config[j];
                    NSInteger endzone = [[dict objectForKey:@"end"] integerValue];
                    NSInteger startzone = [[dict objectForKey:@"start"] integerValue];
                    NSInteger multi = [[dict objectForKey:@"multiplier"] integerValue];
                    sum += (endzone - startzone)*multi;
                }
            }
        }
    }
    sum += firstNum + secondNum + thirdNum + fourNum;

    return [NSString stringWithFormat:@"%ld",(long)sum];
}

/**
 * 计算指定时间与当前的时间差
 * @param compareDate   某一指定时间
 * @return 多少(秒or分or天or月or年)+前 (比如，3天前、10分钟前)
 */
+ (NSString *) compareCurrentTime:(NSDate*) compareDate
//
{
    NSTimeInterval  timeInterval = [compareDate timeIntervalSinceNow];
    timeInterval = -timeInterval;
    long temp = 0;
    NSString *result;
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"%@",LocalizedString(@"JUSTNOW")];
    }
    else if((temp = timeInterval/60) <60){
        result = [NSString stringWithFormat:@"%ld%@",temp,LocalizedString(@"MINUTESAGO")];
    }
    
    else if((temp = temp/60) <24){
        result = [NSString stringWithFormat:@"%ld%@",temp,LocalizedString(@"HOURSAGO")];
    }
    
    else if((temp = temp/24) <30){
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        formatter.dateFormat = @"yy/MM/dd";
        result = [formatter stringFromDate:compareDate];
    }
    
    else if((temp = temp/30) <12){
        result = [NSString stringWithFormat:@"%ld%@",temp,LocalizedString(@"MONTHSAGO")];
    }
    else{
        temp = temp/12;
        result = [NSString stringWithFormat:@"%ld%@",temp,LocalizedString(@"YEARSAGO")];
    }
    
    return  result;
}

@end
