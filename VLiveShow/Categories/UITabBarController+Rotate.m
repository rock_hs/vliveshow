//
//  UITabBarController+Rotate.m
//  VLiveShow
//
//  Created by tom.zhu on 2016/10/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "UITabBarController+Rotate.h"

@implementation UITabBarController (Rotate)
-(NSUInteger)supportedInterfaceOrientations {
    return [self.selectedViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate {
    return [self.selectedViewController shouldAutorotate];
}
@end
