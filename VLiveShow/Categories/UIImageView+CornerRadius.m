//
//  UIImageView+CornerRadius.m
//  VLiveShow
//
//  Created by vipabc on 16/10/17.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "UIImageView+CornerRadius.h"

@implementation UIImageView (CornerRadius)

/**
 * @brief clip the cornerRadius with image, UIImageView must be setFrame before, no off-screen-rendered
 */
- (void)v_cornerRadiusWithImage:(UIImage *)image cornerRadius:(CGFloat)cornerRadius rectCornerType:(UIRectCorner)rectCornerType {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);//创建画板
    CGContextRef context = UIGraphicsGetCurrentContext();//获取画布
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:rectCornerType cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
    CGContextAddPath(context, path.CGPath);//设置画布路径（显示的内容）
    CGContextClip(context);//截取路径
    [image drawInRect:self.bounds];//将画板的内容画进image(image不会显示的)
    self.image = UIGraphicsGetImageFromCurrentImageContext();//获取画布上的内容并赋值
    UIGraphicsEndImageContext();//关闭制图
}

@end
