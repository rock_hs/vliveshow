//
//  PAUIViewLayout.swift
//  PAViewPager
//
//  Created by VincentX on 3/7/16.
//
//

import UIKit

extension UIView
{
    enum Direction: Int
    {
        case vertical
        case horizontal
        case both
    }
    
    func fillParent(_ direction: Direction) -> [NSLayoutConstraint]
    {
        guard let superview = self.superview else
        {
            return []
        }
        var constraints: [NSLayoutConstraint] = []
        self.translatesAutoresizingMaskIntoConstraints = false
        if direction == .vertical || direction == .both
        {
            let vConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view": self])
            superview.addConstraints(vConstraints)
            constraints.append(contentsOf: vConstraints)
        }
        if direction == .horizontal || direction == .both
        {
            let hConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view": self])
            superview.addConstraints(hConstraints)
            constraints.append(contentsOf: constraints)
        }
        return constraints
    }
}
