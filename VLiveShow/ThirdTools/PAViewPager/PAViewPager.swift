//
//  PAViewPager.swift
//  PAViewPagerDemo
//
//  Created by VincentX on 2/23/16.
//
//

import Foundation
import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


@objc public protocol PAViewPagerDelegate: NSObjectProtocol
{
    func numberOfPageInViewPager(_ viewPager: PAViewPager) -> Int
    @objc optional func viewPager(_ viewPager: PAViewPager, reusableIdentifierForIndex: Int)-> String
    func viewPager(_ viewPager: PAViewPager, resuableView: UIView?, viewForIndex: Int) -> UIView
    
    @objc optional func viewPager(_ viewPager: PAViewPager, titleForIndex: Int) -> String
    
    @objc optional func viewPager(_ viewPager: PAViewPager, reusableIdentifierForTitleViewIndex: Int) -> String
    @objc optional func viewPager(_ viewPager: PAViewPager, resuableTitleView: UIView?, titleViewForIndex: Int) -> UIView
    
    @objc optional func viewPager(_ viewPager: PAViewPager, willPreloadViewAtIndex: Int, animated: Bool) -> Void
    @objc optional func viewPager(_ viewPager: PAViewPager, willShowViewAtIndex: Int, animated: Bool) -> Void
    @objc optional func viewPager(_ viewPager: PAViewPager, didShowViewAtIndex: Int, previousIndex: Int, animated: Bool) -> Void
}

open class PAViewPager: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    public enum AnimationWhenTappingTab
    {
        case none
        case adjacent
        case all
    }
    
    public enum TabPosition
    {
        case top
        case bottom
    }
    
    public enum TabSpacingStyle
    {
        case equalWidth
        case equalSpacing
    }
    
    // MARK: Public properties
    open var tabSelectedBackgroundColor: UIColor = UIColor.orange
    {
        didSet
        {
            self.selectionIndicatorView.backgroundColor = tabSelectedBackgroundColor
        }
    }
    
    open var titleColor: UIColor = UIColor.black
    open var titleFont: UIFont = UIFont.systemFont(ofSize: 14)
    open var selectedTitleColor: UIColor = UIColor.white
    open var animatedScrollWhenTappingTab: AnimationWhenTappingTab = .adjacent
    open var needAnimateSelectionIndictor: Bool = true
    
    open var allowScroll = true
    {
        didSet
        {
            self.contentCollectionView.isScrollEnabled = allowScroll
        }
    }
    
    open var allowTabScroll = false
    {
        didSet
        {
            self.tabCollectionView.isScrollEnabled = allowScroll
        }
    }
    
    open var tabHeight: CGFloat  = 44
    {
        didSet
        {
            tabHeightConstraint.constant = tabHeight
            self.invalidateLayout()
        }
    }
    
    open var tabWidth: CGFloat = 0
    
    open var tabMinimumWidth: CGFloat = 0
    
    open var tabLeftOffset: CGFloat = 0
    {
        didSet
        {
            if let flowLayout = self.tabCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
            {
                flowLayout.sectionInset = UIEdgeInsets(top: 0, left: tabLeftOffset, bottom: 0, right: 0)
                flowLayout.invalidateLayout()
            }
        }
    }
    
    open var tabRightOffset: CGFloat = 0
        {
        didSet
        {
            if let flowLayout = self.tabCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
            {
                flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -tabRightOffset)
                flowLayout.invalidateLayout()
            }
        }
    }
    
    open var tabPosition: TabPosition = .top
    {
        didSet
        {
            if self.verticalLayoutConstraints.count > 0
            {
                self.removeConstraints(self.verticalLayoutConstraints)
            }
            self.tabView.translatesAutoresizingMaskIntoConstraints = false
            self.contentCollectionView.translatesAutoresizingMaskIntoConstraints = false
            var layoutStr = "V:|[tab(\(tabHeight))]-(0)-[content]|"
            if tabPosition == .bottom
            {
                layoutStr = "V:|[content]-(0)-[tab(\(tabHeight))]|"
            }
            
            let vConstraints = NSLayoutConstraint.constraints(withVisualFormat: layoutStr, options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["tab": tabView, "content": contentCollectionView])
            self.tabHeightConstraint = vConstraints[1]
            self.verticalLayoutConstraints = vConstraints
            self.addConstraints(vConstraints)
            self.layoutIfNeeded()
        }
    }
    
    open var tabBackgroundColor: UIColor = UIColor.lightGray
    {
        didSet
        {
            self.tabView.backgroundColor = tabBackgroundColor
        }
    }
    
    open var tabInset: UIEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
    {
        didSet
        {
            self.tabCollectionView.contentInset = tabInset
        }
    }
    
    // MARK: SelectionIndicator parameters
    open var selectionIndicatorHeight: CGFloat = 4
    {
        didSet
        {
            self.selectionIndicatorVerticalConstraints[1].constant = selectionIndicatorHeight
        }
    }
    
    open var selectionIndicatorY: CGFloat = 40 {
        didSet
        {
            self.selectionIndicatorVerticalConstraints[0].constant = selectionIndicatorY
        }
    }
    
    open var selectionIndicatorWidth: CGFloat = 0 {
        didSet
        {
            self.selectionIndicatorVerticalConstraints[1].constant = selectionIndicatorWidth
            adjustSelectionIndicator()
        }
    }
    
    open var allowBounces:Bool = true
    {
        didSet
        {
            self.contentCollectionView.bounces = allowBounces
        }
    }
    
    open var tabSpacingStyle: TabSpacingStyle = .equalWidth
    {
        didSet
        {
            
        }
    }

    
    open var selectionIndicatorView: UIView
    
    // MARK: Private variables
    var tabCollectionView: UICollectionView
    var contentCollectionView: UICollectionView
    var seperatorView: UIView
    let titleCellIndentifier = "tabTitleCell"
    let kCommonTag = 1001
    var numberOfItems:Int = 0
    var tabDequeueDictionary:[String:Bool] = [:]
    var contentDequeueDictionary: [String:Bool] = [:]
    var tabHeightConstraint: NSLayoutConstraint!
    
    var selectionIndicatorVerticalConstraints: [NSLayoutConstraint] = []
    var selectionIndicatorHorizontalConstraints: [NSLayoutConstraint] = []
    var verticalLayoutConstraints: [NSLayoutConstraint] = []
    open var tabView: UIView
    var realTabWidth: CGFloat = 0
    
    fileprivate var _selectedIndex:Int = -1

    @IBOutlet weak var delegate: PAViewPagerDelegate?
    {
        didSet
        {
            self.tabCollectionView.delegate = self
            self.tabCollectionView.dataSource = self
            self.contentCollectionView.delegate = self
            self.contentCollectionView.dataSource = self
        }
    }
    
    override public init(frame: CGRect) {
        let contentlayout = UICollectionViewFlowLayout()
        let tabLayout = UICollectionViewFlowLayout()
        self.tabView = UIView(frame: CGRect.zero)
        self.selectionIndicatorView = UIView(frame: CGRect.zero)
        self.tabCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: tabLayout)
        self.contentCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: contentlayout)
        self.seperatorView = UIView()
        super.init(frame: frame)
        self.initViews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        let contentlayout = UICollectionViewFlowLayout()
        let tabLayout = UICollectionViewFlowLayout()
        self.tabView = UIView(frame: CGRect.zero)
        self.selectionIndicatorView = UIView(frame: CGRect.zero)
        self.tabCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: tabLayout)
        self.contentCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: contentlayout)
        self.seperatorView = UIView()
        super.init(coder: aDecoder)
        self.initViews()
    }
    
    func initViews()
    {
        if let tabLayout = tabCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        {
            tabLayout.scrollDirection = .horizontal
            tabLayout.minimumInteritemSpacing = 0
            tabLayout.sectionInset = UIEdgeInsets.zero
            tabLayout.minimumLineSpacing = 0
        }
        self.tabCollectionView.backgroundColor = UIColor.clear
        self.tabCollectionView.showsHorizontalScrollIndicator = false
        self.tabCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: titleCellIndentifier)
        self.tabView.backgroundColor = self.tabBackgroundColor

        if let contentLayout = contentCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        {
            contentLayout.scrollDirection = .horizontal
            contentLayout.minimumInteritemSpacing = 0
            contentLayout.sectionInset = UIEdgeInsets.zero
            contentLayout.minimumLineSpacing = 0
        }
        self.contentCollectionView.isPagingEnabled = true
        self.contentCollectionView.showsHorizontalScrollIndicator = false
        self.contentCollectionView.backgroundColor = UIColor.clear
        self.selectionIndicatorView = UIView()
        tabView.addSubview(tabCollectionView)
        tabView.addSubview(seperatorView)
        tabView.addSubview(selectionIndicatorView)

        self.addSubview(contentCollectionView)
        self.addSubview(tabView)
        self.addConstraintsToSubviews()
        self.selectionIndicatorView.backgroundColor = tabSelectedBackgroundColor
    }
    
    open func setSelectedIndex(_ index: Int, animated: Bool)
    {
        self.setSelectedIndex(index, animated: animated, scrollTab: false)
    }
    
    open func setSelectedIndex(_ index: Int, animated: Bool, scrollTab: Bool)
    {
        if index < numberOfItems && index != _selectedIndex
        {
            let indexPath = IndexPath(row: index, section: 0)
            self.tabCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
            self.collectionView(tabCollectionView, didDeselectItemAt: IndexPath(row: _selectedIndex, section: 0))
            self.collectionView(tabCollectionView, didSelectItemAt: indexPath)
            self.contentCollectionView.scrollToItem(at: indexPath, at: .left, animated: animated && allowScroll)
            adjustSelectionIndicator(needAnimateSelectionIndictor)
            preloadNextPage()
            
//            var needScroll = true
//            if let index = self.tabCollectionView.indexPathsForVisibleItems.index(of: indexPath),
//                index != self.tabCollectionView.indexPathsForVisibleItems.count
//            {
//                needScroll = false
//            }
            
            if scrollTab
            {
                self.tabCollectionView.scrollToItem(at: indexPath, at: (indexPath.row < _selectedIndex) ? .left : .right, animated: animated && allowScroll)
            }
        }
    }
    
    open func selectedIndex()-> Int
    {
        return _selectedIndex
    }
    
    func reloadData()
    {
        guard let delegate = self.delegate else
        {
            return
        }
        numberOfItems = delegate.numberOfPageInViewPager(self)
        self.tabCollectionView.reloadData()
        self.contentCollectionView.reloadData()
        if numberOfItems == 0
        {
            _selectedIndex = -1
        }
        if _selectedIndex > numberOfItems || (_selectedIndex == -1 && numberOfItems > 0)
        {
            self.setSelectedIndex(0, animated: false)
        }
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
        self.tabCollectionView.collectionViewLayout.invalidateLayout()
        self.contentCollectionView.collectionViewLayout.invalidateLayout()
        
        if (_selectedIndex < numberOfItems && _selectedIndex != -1)
        {
            let indexPath = IndexPath(row: _selectedIndex, section: 0)
            if let indexes = self.tabCollectionView.indexPathsForSelectedItems
            {
                if indexes.count > 0 && indexes[0].row == _selectedIndex
                {
                    //return
                }
            }
            self.tabCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
            self.contentCollectionView.scrollToItem(at: indexPath, at: .left, animated: false)
            adjustSelectionIndicator(false)
            return
        }
    }
    
    func addConstraintsToSubviews()
    {
        self.tabCollectionView.fillParent(.both)
        self.tabView.fillParent(.horizontal)
        self.contentCollectionView.fillParent(.horizontal)
        self.tabPosition = .top
        self.seperatorView.fillParent(.horizontal)
        
        self.selectionIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        let vConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(self.selectionIndicatorY)-[selectionView(\(self.selectionIndicatorHeight))]", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["selectionView": self.selectionIndicatorView])
        let hConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[selectionView(\(self.selectionIndicatorWidth))]", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["selectionView": self.selectionIndicatorView])
        self.selectionIndicatorVerticalConstraints = vConstraints
        self.selectionIndicatorHorizontalConstraints = hConstraints
        self.addConstraints(vConstraints)
        self.addConstraints(hConstraints)
        
        let seratorConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:[seperatorView(1)]|", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["seperatorView": self.seperatorView])
        self.tabView.addConstraints(seratorConstraints)
        self.layoutIfNeeded()
    }
    
    func setupVerticalLayout(_ tabPosition: TabPosition)
    {
        
    }
    
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return numberOfItems
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == self.contentCollectionView
        {
            return cellForContentView(indexPath)
        }
        return cellForTab(indexPath)
        
    }
    
    open func invalidateLayout()
    {
        self.tabCollectionView.collectionViewLayout.invalidateLayout()
        self.contentCollectionView.collectionViewLayout.invalidateLayout()
        self.layoutIfNeeded()
    }
    
    fileprivate func cellForContentView(_ indexPath: IndexPath) -> UICollectionViewCell
    {
        var indentifier = "cell"
        guard let delegate = self.delegate else
        {
            return self.contentCollectionView.dequeueReusableCell(withReuseIdentifier: indentifier, for: indexPath)
        }
        
        if delegate.responds(to: #selector(PAViewPagerDelegate.viewPager(_:reusableIdentifierForIndex:)))
        {
            indentifier = delegate.viewPager!(self, reusableIdentifierForIndex: indexPath.row)
            if contentDequeueDictionary[indentifier] == nil
            {
                self.contentCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: indentifier)
                contentDequeueDictionary[indentifier] = true
            }
        }
        let cell = self.contentCollectionView.dequeueReusableCell(withReuseIdentifier: indentifier, for: indexPath)
        if delegate.responds(to: #selector(PAViewPagerDelegate.viewPager(_:resuableView:viewForIndex:)))
        {
            let view = delegate.viewPager(self, resuableView: cell.contentView.viewWithTag(kCommonTag), viewForIndex: indexPath.row)
            view.tag = kCommonTag
            if view.superview != cell.contentView
            {
                let subviews = cell.contentView.subviews.map({ (view) -> UIView in
                    return view
                })
                subviews.forEach({ (view) -> () in
                    view.removeFromSuperview()
                })
                cell.contentView.addSubview(view)
                view.fillParent(.both)
            }
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == tabCollectionView
        {
            if tabWidth > 0
            {
                self.realTabWidth = tabWidth
            }
            else if (tabMinimumWidth > 0)
            {
                self.realTabWidth = max(tabMinimumWidth, CGFloat( Float(self.frame.width) / Float(numberOfItems)))
            }
            else
            {
                self.realTabWidth = (CGFloat(self.frame.width) - self.tabLeftOffset - self.tabRightOffset) / CGFloat(numberOfItems)
            }
            return CGSize(width: self.realTabWidth, height: tabHeight)
            
        }
        else
        {
            return CGSize(width: self.frame.width, height: self.frame.height - tabHeight)
        }
    }
    
    // MARK: UICollectionViewDelegate
    open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == tabCollectionView
        {
            if (_selectedIndex == indexPath.row)
            {
                return
            }
            
            let offset = abs(indexPath.row - _selectedIndex)
            var animated = false
            switch(animatedScrollWhenTappingTab)
            {
            case .none:
                animated = false
                
            case .all:
                animated = true
            case .adjacent:
                animated = (offset == 1)
            }
            if let delegate = self.delegate
            {
                if delegate.responds(to: #selector(PAViewPagerDelegate.viewPager(_:willShowViewAtIndex:animated:)))
                {
                    delegate.viewPager!(self, willShowViewAtIndex: indexPath.row, animated: animated)
                }
            }
            let oldIndex = _selectedIndex
            _selectedIndex = indexPath.row
            if let cell = self.tabCollectionView.cellForItem(at: indexPath)
            {
                if delegate != nil && delegate!.responds(to: #selector(PAViewPagerDelegate.viewPager(_:titleForIndex:)))
                {
                    if let titleLabel = cell.contentView.viewWithTag(kCommonTag) as? UILabel
                    {
                        titleLabel.textColor = self.selectedTitleColor
                    }
                }
            }
            if let lastCell = self.tabCollectionView.cellForItem(at: IndexPath(row: oldIndex, section: 0))
            {
                if delegate != nil && delegate!.responds(to: #selector(PAViewPagerDelegate.viewPager(_:titleForIndex:)))
                {
                    if let titleLabel = lastCell.contentView.viewWithTag(kCommonTag) as? UILabel
                    {
                        titleLabel.textColor = self.titleColor
                    }
                }
            }
            
            contentCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: animated && allowScroll)
            adjustSelectionIndicator(needAnimateSelectionIndictor)
            if let delegate = self.delegate, delegate.responds(to: #selector(PAViewPagerDelegate.viewPager(_:didShowViewAtIndex:previousIndex:animated:)))
            {
                delegate.viewPager!(self, didShowViewAtIndex: indexPath.row, previousIndex: oldIndex, animated: animated)
            }
            preloadNextPage()
        }
    }
    
    open func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        if collectionView == tabCollectionView
        {
            if let cell = self.tabCollectionView.cellForItem(at: indexPath)
            {
                if delegate != nil && delegate!.responds(to: #selector(PAViewPagerDelegate.viewPager(_:titleForIndex:)))
                {
                    if let titleLabel = cell.contentView.viewWithTag(kCommonTag) as? UILabel
                    {
                        titleLabel.textColor = self.titleColor
                    }
                }
            }
        }
    }
    
    // MARK: UIScrollViewDelegate
    open func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == contentCollectionView
        {
            let i = scrollView.contentOffset.x / scrollView.frame.width
            self.setSelectedIndex(Int(i), animated: true)
        }
    }
    
    open func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if scrollView == contentCollectionView && !decelerate
        {
            scrollViewDidEndDecelerating(scrollView)
        }
    }
    
    open func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tabCollectionView
        {
            self.adjustSelectionIndicator()
        }
    }

    
    
    fileprivate func cellForTab(_ indexPath: IndexPath) -> UICollectionViewCell
    {
        var indentifier = "cell"
        guard let delegate = self.delegate else
        {
            return self.tabCollectionView.dequeueReusableCell(withReuseIdentifier: indentifier, for: indexPath)
        }
        if delegate.responds(to: #selector(PAViewPagerDelegate.viewPager(_:titleForIndex:)))
        {
            let cell = self.tabCollectionView.dequeueReusableCell(withReuseIdentifier: titleCellIndentifier, for: indexPath)
            cell.backgroundColor = UIColor.clear
            cell.backgroundView = UIView()
//            cell.selectedBackgroundView = UIView(frame: CGRectZero)
//            cell.selectedBackgroundView?.backgroundColor = self.tabSelectedBackgroundColor
            var titleLabel = cell.contentView.viewWithTag(kCommonTag) as? UILabel
            if titleLabel == nil
            {
                titleLabel = UILabel()
                titleLabel?.textAlignment = .center
                cell.contentView.addSubview(titleLabel!)
                titleLabel!.tag = kCommonTag
                titleLabel?.fillParent(.both)
            }
            let title = delegate.viewPager!(self, titleForIndex: indexPath.row)
            titleLabel!.text = title
            titleLabel!.font = self.titleFont
            titleLabel!.textColor = self.titleColor
            if indexPath.row == _selectedIndex
            {
                titleLabel!.textColor = self.selectedTitleColor
            }
            return cell
        }
        if delegate.responds(to: #selector(PAViewPagerDelegate.viewPager(_:reusableIdentifierForTitleViewIndex:)))
        {
            indentifier = delegate.viewPager!(self, reusableIdentifierForTitleViewIndex: indexPath.row)
            if tabDequeueDictionary[indentifier] == nil
            {
                self.tabCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: indentifier)
                tabDequeueDictionary[indentifier] = true
            }
            let cell = self.tabCollectionView.dequeueReusableCell(withReuseIdentifier: indentifier, for: indexPath)
            let view = cell.contentView.viewWithTag(kCommonTag)
            let tabView = delegate.viewPager!(self, resuableTitleView: view, titleViewForIndex: indexPath.row)
            if tabView.superview != cell.contentView
            {
                let subviews = cell.contentView.subviews.map({ (view) -> UIView in
                    return view
                })
                subviews.forEach({ (view) -> () in
                    view.removeFromSuperview()
                })
                cell.contentView.addSubview(tabView)
                tabView.fillParent(.both)
            }
            return cell
        }
        return self.tabCollectionView.dequeueReusableCell(withReuseIdentifier: indentifier, for: indexPath)
    }
    
    fileprivate func adjustSelectionIndicator(_ animated: Bool = false)
    {
        var width = self.selectionIndicatorWidth
        if width == 0
        {
            width = self.realTabWidth
        }
//        if let cell = self.tabCollectionView.cellForItemAtIndexPath(NSIndexPath(forRow: self.selectedIndex(), inSection: 0))
//        {
//            if let superview = cell.superviewf
//            {
//                let x = superview.convertPoint(cell.center, toView: self.tabView).x
//                let block = {
//                    self.selectionIndicatorHorizontalConstraints[0].constant = x - width / 2
//                    self.layoutIfNeeded()
//                }
//                if animated
//                {
//                    self.layoutIfNeeded()
//                    UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: block, completion: nil)
//                }
//                else
//                {
//                    block()
//                }
//            }
//        }
        
        let block = {
            let segWith = self.realTabWidth
            self.selectionIndicatorHorizontalConstraints[0].constant = (CGFloat(self.selectedIndex()) + 0.5) * segWith - width / 2 - self.tabCollectionView.contentOffset.x
            self.layoutIfNeeded()
        }
        if animated
        {
            self.layoutIfNeeded()
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions(), animations: block, completion: nil)
        }
        else
        {
            block()
        }
        
        self.selectionIndicatorHorizontalConstraints[1].constant = width
    }
    
    fileprivate func preloadNextPage()
    {
        
        if self.selectedIndex() + 1 < self.numberOfItems
        {
            if let delegate = self.delegate
            {
                if delegate.responds(to: #selector(PAViewPagerDelegate.viewPager(_:willPreloadViewAtIndex:animated:)))
                {
                    delegate.viewPager!(self, willPreloadViewAtIndex: self.selectedIndex() + 1, animated: false)
                }
            }
        }
    }
}

public extension PAViewPager
{
    public func setAsNormalTabBarStyle(_ tabPosition: TabPosition)
    {
        self.allowScroll = false
        self.tabWidth = 0
        self.tabLeftOffset = 0
        self.tabRightOffset = 0
        self.tabPosition = tabPosition
        self.animatedScrollWhenTappingTab = .none
    }
    
    public func setAsViewPagerStyle(_ tabPosition: TabPosition)
    {
        self.allowScroll = true
        self.tabWidth = 0
        self.tabLeftOffset = 0
        self.tabRightOffset = 0
        self.tabPosition = tabPosition
        self.animatedScrollWhenTappingTab = .adjacent
    }
}
