//
//  PAViewPagerViewController.swift
//  PAViewPagerDemo
//
//  Created by VincentX on 3/14/16.
//
//

import UIKit

open class PAViewPagerViewController: UIViewController, PAViewPagerDelegate {
    
    open var viewPager: PAViewPager
    
    @objc open var viewPagerConstraints: [NSLayoutConstraint]?
    
    open var selectedViewController: UIViewController?
    {
        get
        {
            let index = self.viewPager.selectedIndex()
            if index >= 0 && index < subViewControllers.count
            {
                return subViewControllers[index]
            }
            return nil
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.viewPager = PAViewPager(frame: CGRect(x: 0, y: 0, width: 0, height: 44))
        super.init(coder: aDecoder)
        self.edgesForExtendedLayout = UIRectEdge()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.viewPager = PAViewPager(frame: CGRect.zero)
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    open var subViewControllers:[UIViewController] = []
    {
        didSet(oldValues)
        {
            removeOldChildren(oldChilren: oldValues)
            subViewControllers.forEach { (vc) -> () in
                vc.willMove(toParentViewController: self)
                self.addChildViewController(vc)
                vc.pagerViewController = self
                vc.didMove(toParentViewController: self)
                
            }
            viewPager.reloadData()
        }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    deinit {
        removeOldChildren(oldChilren: self.subViewControllers)
    }
        
    open func numberOfPageInViewPager(_ viewPager: PAViewPager) -> Int
    {
        return self.subViewControllers.count
    }
    
    open func viewPager(_ viewPager: PAViewPager, reusableIdentifierForIndex: Int)-> String
    {
        return "controller"
    }
    
    open func viewPager(_ viewPager: PAViewPager, resuableView: UIView?, viewForIndex: Int) -> UIView
    {
        return self.subViewControllers[viewForIndex].view
    }
    
    open func viewPager(_ viewPager: PAViewPager, titleForIndex: Int) -> String
    {
        if let title = self.subViewControllers[titleForIndex].title
        {
            return title
        }
        return ""
    }
    
    open func viewPager(_ viewPager: PAViewPager, willShowViewAtIndex: Int, animated: Bool) -> Void
    {
        if viewPager.selectedIndex() >= 0 && viewPager.selectedIndex() < self.subViewControllers.count
        {
            let oldVC = self.subViewControllers[viewPager.selectedIndex()]
            oldVC.viewWillDisappear(animated)
        }
        let newVC = self.subViewControllers[willShowViewAtIndex]
        if !newVC.isViewLoaded
        {
            // Force vc to bload
            _ = newVC.view.frame
        }
        newVC.viewWillAppear(animated)
        
    }
    
    open func viewPager(_ viewPager: PAViewPager, didShowViewAtIndex: Int, previousIndex:Int, animated: Bool) -> Void
    {
        if previousIndex >= 0 && previousIndex < self.subViewControllers.count
        {
            let oldVC = self.subViewControllers[previousIndex]
            oldVC.viewDidDisappear(animated)
        }
        let newVC = self.subViewControllers[didShowViewAtIndex]
        newVC.viewDidAppear(animated)
    }
    
    open func viewPager(_ viewPager: PAViewPager, willPreloadViewAtIndex: Int, animated: Bool) -> Void
    {
        if willPreloadViewAtIndex >= self.subViewControllers.count
        {
            return
        }
        let newVC = self.subViewControllers[willPreloadViewAtIndex]
        if !newVC.isViewLoaded
        {
            // Force vc to bload
            _ = newVC.view.frame
        }
    }
    
    fileprivate func setup()
    {
        self.view.addSubview( self.viewPager)
        self.viewPagerConstraints = self.viewPager.fillParent(.both)
        self.viewPager.delegate = self
        self.viewPager.setAsNormalTabBarStyle(PAViewPager.TabPosition.top)
        self.viewPager.allowScroll = true
    }
    
    func removeOldChildren(oldChilren: [UIViewController])
    {
        oldChilren.forEach { (vc) -> () in
            vc.willMove(toParentViewController: nil)
            vc.removeFromParentViewController()
            vc.pagerViewController = nil
            vc.didMove(toParentViewController: nil)
        }
    }
}


extension UIViewController
{
    static let pagerViewKey = UnsafePointer<Int>(bitPattern: 0)

    weak var pagerViewController: PAViewPagerViewController?
    {
        get
        {
            let item = objc_getAssociatedObject(self, UIViewController.pagerViewKey) as? PAViewPagerViewController
            
            return item
        }

        set
        {
            objc_setAssociatedObject(self, UIViewController.pagerViewKey, newValue, .OBJC_ASSOCIATION_ASSIGN)

        }
    }
}
