//
//  AnimOperation.m
//  LiveAnimation
//
//  Created by lllll-xy on 16/7/20.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "AnimOperation.h"
#import "UIView+LYAdd.h"
#import "BarrageService.h"

@interface AnimOperation ()

@property (nonatomic, getter = isExecuting) BOOL executing;

@end

@implementation AnimOperation

@synthesize finished = _finished;
@synthesize executing = _executing;

- (instancetype)init
{
  
  self = [super init];
  if (self) {
    
    _executing = NO;
    _finished  = NO;
    
  }
  return self;
  
}

- (void)start {

  if ([self isCancelled]) {
    self.finished = YES;
    return;
  }
  self.executing = YES;
  
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{

    [[BarrageService shareBarrageService] giftQueueControlForView:_listView withGiftModel:_giftModel Completed:^{
      self.finished =YES;
    }];
    [BarrageService shareBarrageService].operation = self;
    
  }];
  
}

#pragma mark -  KVO
- (void)setExecuting:(BOOL)executing
{
  [self willChangeValueForKey:@"isExecuting"];
  _executing = executing;
  [self didChangeValueForKey:@"isExecuting"];
}

- (void)setFinished:(BOOL)finished
{
  [self willChangeValueForKey:@"isFinished"];
  _finished = finished;
  [self didChangeValueForKey:@"isFinished"];
}

@end
