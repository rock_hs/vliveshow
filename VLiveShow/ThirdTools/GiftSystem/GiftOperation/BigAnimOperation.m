//
//  BigAnimOperation.m
//  LiveAnimation
//
//  Created by lllll-xy on 16/8/4.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "BigAnimOperation.h"

@interface BigAnimOperation ()

@property (nonatomic, getter = isFinished)  BOOL finished;
@property (nonatomic, getter = isExecuting) BOOL executing;

@end

@implementation BigAnimOperation

@synthesize finished = _finished;
@synthesize executing = _executing;

- (instancetype)init
{
  
  self = [super init];
  if (self) {
    
    _executing = NO;
    _finished  = NO;
    
  }
  return self;
  
}

- (void)start {
  
  if ([self isCancelled]) {
    self.finished = YES;
    return;
  }
  self.executing = YES;
  
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{

    [[BigGiftService shareBigGiftService] bigGiftQueueControlForView:_fromView withGiftModel:self.giftModel Completed:^{
      self.finished = YES;
    }];
    
  }];
  
}

#pragma mark -  KVO

- (void)setExecuting:(BOOL)executing
{
  [self willChangeValueForKey:@"isExecuting"];
  _executing = executing;
  [self didChangeValueForKey:@"isExecuting"];
}

- (void)setFinished:(BOOL)finished
{
  [self willChangeValueForKey:@"isFinished"];
  _finished = finished;
  [self didChangeValueForKey:@"isFinished"];
}

@end
