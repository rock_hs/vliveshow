//
//  BigAnimOperation.h
//  LiveAnimation
//
//  Created by lllll-xy on 16/8/4.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BigGiftService.h"

@interface BigAnimOperation : NSOperation

@property (nonatomic,strong) UIView *fromView;
@property (nonatomic,strong) GiftModel * giftModel;

@end
