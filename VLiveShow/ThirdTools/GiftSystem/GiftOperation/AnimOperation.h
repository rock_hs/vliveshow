//
//  AnimOperation.h
//  LiveAnimation
//
//  Created by lllll-xy on 16/7/20.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GiftModel.h"
#import "LYLiveGiftBarrageView.h"

@interface AnimOperation : NSOperation

@property (nonatomic,strong) LYLiveGiftBarrageView *model;
@property (nonatomic,strong) GiftModel *giftModel;
@property (nonatomic,assign) NSInteger index;
@property (nonatomic,strong) UIView *listView;

@property (nonatomic, getter = isFinished)  BOOL finished;

@end
