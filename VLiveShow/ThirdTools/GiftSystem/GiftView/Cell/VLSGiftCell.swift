//
//  VLSGiftCell.swift
//  VLiveShow
//
//  Created by rock on 2017/2/18.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

class VLSGiftCell: UICollectionViewCell {
    var giftImageView: UIImageView!
    var giftPriceBt: UIButton!
    var activity: UIActivityIndicatorView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.borderColor = UIColor.white.withAlphaComponent(0.2).cgColor
        self.layer.borderWidth = 0.3
        
        giftPriceBt = UIButton().then {
            $0.setImage(#imageLiteral(resourceName: "gift_diamond"), for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_15))
            $0.isEnabled = false
        }
        self.addSubview(giftPriceBt)
        giftPriceBt.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self)
            make.height.equalTo(20)
        }
        
        giftImageView = UIImageView().then {
            $0.contentMode = .scaleAspectFit
            $0.isUserInteractionEnabled = true
        }
        self.addSubview(giftImageView)
        giftImageView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self)
            make.bottom.equalTo(giftPriceBt.snp.top)
        }
        activity = UIActivityIndicatorView().then {
            $0.activityIndicatorViewStyle = .white
        }
        self.addSubview(activity)
        activity.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(self)
        }
    }
    
    func setNewItem(_ item: VLSGiftModel) {
        activity.startAnimating()
        if let giftURL = URL(string: StringSafty(item.giftModel?.giftImage)) {
            giftImageView.sd_setImage(with: giftURL, completed: {[weak self] (image, error, cache, url) in
                self?.giftImageView.image = image
                self?.activity.stopAnimating()
                self?.activity.hidesWhenStopped = true
            })
        }
        giftPriceBt.setTitle(StringSafty(item.giftModel?.giftPrice), for: .normal)
        if item.isSelected {
            self.layer.borderColor = HEX(COLOR_BG_FF1130).cgColor
            self.layer.borderWidth = 1
        } else {
            self.layer.borderColor = UIColor.white.withAlphaComponent(0.2).cgColor
            self.layer.borderWidth = 0.3
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
