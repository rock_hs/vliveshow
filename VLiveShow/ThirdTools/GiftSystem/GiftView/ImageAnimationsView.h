//
//  ImageAnimationsView.h
//  LiveAnimation
//
//  Created by lllll-xy on 16/8/5.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiftModel.h"

typedef enum : NSUInteger {
  
  SixAnimation,
  JumpAnimation,
  PraiseAnimation,
  LauseAnimation,
  PickNoseAnimation,
  rocketsAnimation,
  
} BigGiftEnum;


@interface ImageAnimationsView : UIView

@property (nonatomic,strong)GiftModel *giftModel;

@property (nonatomic,assign) float animationDuration;

@property (nonatomic,assign) float ViewanimationDuration;

@property (nonatomic,assign) float animationRepeatCount;

- (void)startAnimationAndCompleted:(void(^)())completed;

@end
