//
//  DrawCircleProgressButton.h
//  LiveAnimation
//
//  Created by lllll-xy on 16/8/10.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DrawCircleProgressButton;

@protocol CircleProgressButtonDelegate <NSObject>

- (void)AnimationCompleted;

@end

typedef void(^DrawCircleProgressBlock)(void);

@interface DrawCircleProgressButton : UIButton

//set track color
@property (nonatomic,strong)UIColor    *trackColor;

//set progress color
@property (nonatomic,strong)UIColor    *progressColor;

//set track background color
@property (nonatomic,strong)UIColor    *fillColor;

//set progress line width
@property (nonatomic,assign)CGFloat    lineWidth;

//set progress duration
@property (nonatomic,assign)CGFloat    animationDuration;

//set delegate duration
@property (nonatomic,weak) id<CircleProgressButtonDelegate> delegate;

/**
 *  set complete callback
 *
 *  @param lineWidth line width
 *  @param block     block
 *  @param duration  time
 */
- (void)startAnimationDuration:(CGFloat)duration withBlock:(DrawCircleProgressBlock )block;

- (void)resetTimer;

- (void)stopTimer;

@end
