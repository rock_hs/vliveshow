//
//  DrawCircleProgressButton.m
//  LiveAnimation
//
//  Created by lllll-xy on 16/8/10.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "DrawCircleProgressButton.h"

#define degreesToRadians(x) ((x) * M_PI / 180.0)

@interface DrawCircleProgressButton()

@property (nonatomic, strong) CAShapeLayer *trackLayer;
@property (nonatomic, strong) CAShapeLayer *progressLayer;
@property (nonatomic, strong) UIBezierPath *bezierPath;
@property (nonatomic, copy)   DrawCircleProgressBlock myBlock;
@property (strong, nonatomic) UILabel *countLabel;

@end

dispatch_source_t _timer;

@implementation DrawCircleProgressButton

- (instancetype)initWithFrame:(CGRect)frame
{
  
  if (self == [super initWithFrame:frame]) {
    
//    self.backgroundColor = [UIColor clearColor];
    
    [self.layer addSublayer:self.trackLayer];
    
    UILabel * countLabel = [[UILabel alloc] initWithFrame:self.bounds];
    countLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:frame.size.width/3.7];
    countLabel.numberOfLines = 0;
    countLabel.textColor = [UIColor colorWithRed:255/256.0 green:181/256.0 blue:165/256.0 alpha:1];
    countLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:countLabel];
      self.countLabel = countLabel;
  }
  return self;
  
}

-(void)startTime
{
  
  __weak typeof (self)bself = self;
  if (_animationDuration > 0) {
    __block NSInteger timeout = _animationDuration; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
      if(timeout<=0){ //倒计时结束，关闭
        dispatch_source_cancel(_timer);
  
        dispatch_async(dispatch_get_main_queue(), ^{

          bself.hidden = YES;
          if ([self.delegate respondsToSelector:@selector(AnimationCompleted)]) {
            [self.delegate AnimationCompleted];
          }

        });
      }else{

        NSInteger seconds = timeout % 60;
        dispatch_async(dispatch_get_main_queue(), ^{
          self.countLabel.text = [NSString stringWithFormat:@"%@        %@",@(seconds).stringValue,LocalizedString(@"EVEN_SEND")];
        });
        
        timeout--;
        
      }
      
    });
    
    dispatch_resume(_timer);
    
  }
  
}

- (void)resetTimer{
    if (_timer) {
        dispatch_source_cancel(_timer);
        [self.progressLayer removeAllAnimations];
        [self.progressLayer removeFromSuperlayer];
        self.progressLayer = nil;
    }
  [self startAnimationDuration:self.animationDuration withBlock:nil];
  
}

- (void)stopTimer{
  
  if (_timer) {
    dispatch_source_cancel(_timer);
    [self.progressLayer removeAllAnimations];
    [self.progressLayer removeFromSuperlayer];
    self.progressLayer = nil;
  }

}

- (UIBezierPath *)bezierPath
{
  
  if (!_bezierPath) {
    
    CGFloat width = CGRectGetWidth(self.frame)/2.0f;
    CGFloat height = CGRectGetHeight(self.frame)/2.0f;
    CGPoint centerPoint = CGPointMake(width, height);
    float radius = CGRectGetWidth(self.frame)/2;
    
    _bezierPath = [UIBezierPath bezierPathWithArcCenter:centerPoint
                                                 radius:radius
                                             startAngle:degreesToRadians(-90)
                                               endAngle:degreesToRadians(270)
                                              clockwise:YES];
  }
  return _bezierPath;
  
}

- (CAShapeLayer *)trackLayer
{
  
  if (!_trackLayer) {
    
    _trackLayer = [CAShapeLayer layer];
    _trackLayer.frame = self.bounds;
    _trackLayer.fillColor = self.fillColor.CGColor ? self.fillColor.CGColor : [UIColor redColor].CGColor ;
    _trackLayer.lineWidth = self.lineWidth ? self.lineWidth : 4.0f;
    _trackLayer.strokeColor = self.trackColor.CGColor ? self.trackColor.CGColor : [UIColor whiteColor].CGColor ;
    _trackLayer.strokeStart = 0.f;
    _trackLayer.strokeEnd = 1.f;
    
    _trackLayer.path = self.bezierPath.CGPath;
    
  }
  return _trackLayer;
  
}

- (CAShapeLayer *)progressLayer
{
  
  if (!_progressLayer) {
    
    _progressLayer = [CAShapeLayer layer];
    _progressLayer.frame = self.bounds;
    _progressLayer.fillColor = [UIColor clearColor].CGColor;
    _progressLayer.lineWidth = self.lineWidth ? self.lineWidth : 2.f;
    _progressLayer.lineCap = kCALineCapRound;
    _progressLayer.strokeColor = self.progressColor.CGColor ? self.progressColor.CGColor  : [UIColor blackColor].CGColor;
    _progressLayer.strokeStart = 1.f;
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    pathAnimation.duration = self.animationDuration;
    pathAnimation.fromValue = @(1.0);
    pathAnimation.toValue = @(0.0);
    pathAnimation.removedOnCompletion = YES;
    pathAnimation.delegate = self;
    [_progressLayer addAnimation:pathAnimation forKey:nil];
    
    _progressLayer.path = _bezierPath.CGPath;
    
  }
  return _progressLayer;
  
}

#pragma mark -- CAAnimationDelegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
  if (flag) {
//    self.myBlock();
  }
}

#pragma mark ---

- (void)startAnimationDuration:(CGFloat)duration withBlock:(DrawCircleProgressBlock )block{
  
  self.myBlock = block;
  self.animationDuration = duration;
  [self.layer addSublayer:self.progressLayer];
  [self startTime];
  
}

@end
