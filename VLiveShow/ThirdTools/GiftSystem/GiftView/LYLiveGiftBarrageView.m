//
//  LYLiveGiftBarrageView.m
//  GlobalLive
//
//  Created by Louis on 16/7/9.
//  Copyright © 2016年 GuanCloud. All rights reserved.
//

#import "LYLiveGiftBarrageView.h"
#import "UIView+LYAdd.h"

@interface LYLiveGiftBarrageView ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@end

@implementation LYLiveGiftBarrageView

+ (instancetype)barrageWithAvatar:(NSString *)avatar nickName:(NSString *)nickName content:(NSString *)content giftIcon:(NSString *)giftIcon {
  
  LYLiveGiftBarrageView *barrageView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(LYLiveGiftBarrageView.class) owner:self options:nil][0];
  barrageView.avatarImageView.image = [UIImage imageNamed:avatar];
  barrageView.avatarImageView.layer.cornerRadius = barrageView.avatarImageView.width / 2;
  barrageView.avatarImageView.layer.borderWidth = 1;
  barrageView.avatarImageView.layer.borderColor = [UIColor colorWithRed:75/255.0 green:211/255.0 blue:179/255.0 alpha:1].CGColor;
  barrageView.nickNameLabel.text = nickName;
  barrageView.contentLabel.text = content;
  barrageView.giftImageView.image = [UIImage imageNamed:giftIcon];
  return barrageView;
  
}

- (void)startAnimatingCompleted:(void(^)())completed{
  
  [self setView];
//  if (self.model.giftType == 1) {
//    
//    self.giftImageView.hidden = YES;
//    self.skLabel.hidden = YES;
//    
//  }else
//  {
//    self.giftImageView.hidden = NO;
//    self.skLabel.hidden = NO;
//  }
    //动态礼物连送\
    静态礼物以及动态礼物都显示小图 并且 显示连送次数
    self.giftImageView.hidden = NO;
    self.skLabel.hidden = NO;
    
  _timer = [NSTimer timerWithTimeInterval:0.7 target:self selector:@selector(fire) userInfo:nil repeats:YES];
  [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
  
  self.x = -self.width;
  
  [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    
    self.x = 12;
    self.alpha = 1;
    
  } completion:^(BOOL finished) {
    
    CGFloat giftImageX = _giftImageView.x;
    _giftImageView.x = -_giftImageView.width;
    
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
      
      _giftImageView.x = giftImageX;
      _giftImageView.alpha = 1;
      
      if (completed) {
        completed();
      }
      
    } completion:^(BOOL finished) {
    }];
    
  }];
  
}

- (void)setView{
  
  [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/v1/users/%@/avatars.png",BASE_URL,self.model.userID]]];
  self.avatarImageView.layer.cornerRadius = self.avatarImageView.width / 2;
  self.nickNameLabel.text = _model.name;
  self.contentLabel.text = _model.giftName;
  self.giftImageView.alpha = 0;
  [self.giftImageView sd_setImageWithURL:[NSURL URLWithString:_model.giftImage]];
  
}

- (void)fire{
  
  if (_animCount > 0) {
    
    _animCount --;
    _Count ++;
    _lastTiemr = 0;
    _iSOPEN = YES;
    
    self.skLabel.text = [NSString stringWithFormat:@"x %ld",(long)_Count];
    
    [self.skLabel startAnimWithDuration:0.25 Complete:^{
      
      if (_animCount == 0 && _SuperIsReady == YES) {
        
        self.isReady = YES;
        
      }
      
    }];
    
  }else{
    
    if (!self.SuperIsReady) {
      
      _lastTiemr += 0.8;
      if (_lastTiemr >= 2.0) {
        [self end];
      }
      
    }else{
      
      self.isReady = YES;
      
    }
    
  }
  
}

- (void)end{
  
  [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    self.y = -64;
    self.alpha = 0;
    
  } completion:^(BOOL finished) {
    
    NSLog(@"完成");
    self.animCount = 0;
    self.Count = 0;
    [_timer invalidate];
    _timer = nil;
    _iSOPEN = NO;
    [self removeFromSuperview];
    
  }];
  
}

- (void)startCountAddAnimatingCompleted:(void(^)())completed{
  
  completed();
  
}

- (ShakeLabel *)skLabel{
  
  if (!_skLabel) {
    
    _skLabel =  [[ShakeLabel alloc] init];
    _skLabel.font = [UIFont systemFontOfSize:18.5];
    _skLabel.borderColor = [UIColor colorWithRed:195/256.0 green:168/256.0 blue:81 /256.0 alpha:1];
    _skLabel.textColor = [UIColor colorWithRed:246/256.0 green:234/256.0 blue:24 /256.0 alpha:1];
    _skLabel.textAlignment = NSTextAlignmentCenter;
    
    _skLabel.frame = CGRectMake(_bgImage.frame.origin.x + _bgImage.frame.size.width + 9,0, 50, 53);
    [self addSubview:_skLabel];
    
  }
  return _skLabel;
  
}

- (void)startCompleted:(void(^)())completed{
  
  self.x = -self.width;
  [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    self.x = 0;
    self.alpha = 1;
  } completion:^(BOOL finished) {
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
      
    } completion:^(BOOL finished) {
      if (completed) {
        completed();
      }
    }];
    
  }];
  
  _timer = [NSTimer timerWithTimeInterval:0.7 target:self selector:@selector(fire) userInfo:nil repeats:YES];
  [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
  
}

@end
