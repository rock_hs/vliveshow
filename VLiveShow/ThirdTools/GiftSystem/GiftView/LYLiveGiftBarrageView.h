//
//  LYLiveGiftBarrageView.h
//  GlobalLive
//
//  Created by Louis on 16/7/9.
//  Copyright © 2016年 GuanCloud. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShakeLabel.h"
#import "GiftModel.h"

@interface LYLiveGiftBarrageView : UIView

@property (nonatomic,weak)NSString *userID;
@property (nonatomic,weak)NSString *giftID;
@property (nonatomic,assign)BOOL isNews;

@property (nonatomic,strong)GiftModel *model;

@property (nonatomic,assign) BOOL iSOPEN;
@property (nonatomic,assign) BOOL isReady;
@property (nonatomic,assign) BOOL SuperIsReady;

@property (nonatomic,strong) ShakeLabel *skLabel;

@property (nonatomic,assign) NSInteger animCount;

@property (nonatomic,assign) NSInteger Count;

@property (nonatomic,strong) NSTimer *timer;

@property (nonatomic,assign) float lastTiemr;

/* 暂时不用此方法 */
+ (instancetype)barrageWithAvatar:(NSString *)avatar nickName:(NSString *)nickName content:(NSString *)content giftIcon:(NSString *)giftIcon;

- (void)startAnimatingCompleted:(void(^)())completed;

- (void)startCountAddAnimatingCompleted:(void(^)())completed;

@end
