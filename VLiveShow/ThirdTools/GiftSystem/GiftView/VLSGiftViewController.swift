//
//  VLSGiftViewController.swift
//  VLiveShow
//
//  Created by rock on 2017/2/18.
//  Copyright © 2017年 vliveshow. All rights reserved.
//

import UIKit

@objc protocol VLSGiftViewControllerDelegate {
    func sendGift(_ model: GiftModel)
    func chargeVDiamond()
}

class VLSGiftViewController: VLSCollectionViewController {

    weak var delegate: VLSGiftViewControllerDelegate?
    var footerView: VLSGiftFooterView!
    var preIndex: NSInteger = 0
    var itemCount_Column: NSInteger = 0
    var itemCount_Row: NSInteger = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        footerView = VLSGiftFooterView().then {
            $0.delegate = self
        }
        self.view.addSubview(footerView)
        if UIScreen.main.applicationFrame.width > UIScreen.main.applicationFrame.height {//横屏
            itemCount_Row = 1
            itemCount_Column = 7
            footerView.snp.makeConstraints { (make) in
                make.right.bottom.equalTo(self.view)
                make.size.equalTo(CGSize(width: 110, height: 80))
            }
            collectionView.snp.makeConstraints { (make) in
                make.top.bottom.equalTo(footerView)
                make.left.equalTo(self.view)
                make.right.equalTo(footerView.snp.left)
            }
            collectionView.isPagingEnabled = false
        } else {//竖屏
            itemCount_Row = 2
            itemCount_Column = 5
            footerView.snp.makeConstraints { (make) in
                make.left.right.bottom.equalTo(self.view)
                make.height.equalTo(48)
            }
            collectionView.snp.makeConstraints { (make) in
                make.left.right.equalTo(self.view)
                make.bottom.equalTo(footerView.snp.top)
                make.height.equalTo(160)
            }
            collectionView.isPagingEnabled = true
        }
        let flowLayout = VLSCollectionViewFlowLayout().then {
            $0.itemCount_Column = itemCount_Column
            $0.itemCount_Row = itemCount_Row
            $0.scrollDirection = .horizontal
        }
        collectionView.collectionViewLayout = flowLayout
        collectionView.register(VLSGiftCell.self, forCellWithReuseIdentifier: "giftCell")
        collectionView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        viewModel.itemClass = GiftModel.self
        
        let tap = UITapGestureRecognizer().then {
            $0.addTarget(self, action: #selector(VLSGiftViewController.disappear))
        }
        let backView = UIView().then {
            $0.backgroundColor = UIColor.clear
            $0.addGestureRecognizer(tap)
        }
        self.view.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.bottom.equalTo(collectionView.snp.top)
        }
    }
    
    func disappear() {
        self.view.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.reloadView()
    }
    
    func reloadView() {
        viewModel.dataSource.removeAllObjects()
        for i in 0..<GiftModel.getGiftPlist().count {
            if let model = ObjectForKeySafety(NSArraySafty(GiftModel.getGiftPlist()), index: i) as? GiftModel {
                let giftModel = VLSGiftModel()
                giftModel.giftModel = model
                if preIndex == i {
                    giftModel.isSelected = true
                }
                viewModel.dataSource.add(giftModel)
            }
        }
        collectionView.reloadData()
        let pageNum = Int(viewModel.dataSource.count / (itemCount_Column * itemCount_Row)) + (viewModel.dataSource.count % (itemCount_Column * itemCount_Row) > 0 ? 1 : 0)
        footerView.numberOfPages(num: pageNum)
        footerView.vdiamondLab.text = "\(AccountManager.shared().account.diamondBalance)"
        footerView.remainVDiamond = AccountManager.shared().account.diamondBalance
    }
    
}

extension VLSGiftViewController: VLSGiftFooterViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / CGFloat(itemCount_Column), height: collectionView.frame.height / CGFloat(itemCount_Row))
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "giftCell", for: indexPath)
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) as? VLSGiftModel {
            if let cell = cell as? VLSGiftCell {
                cell.setNewItem(item)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if preIndex != indexPath.row {
            if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: preIndex) as? VLSGiftModel {
                item.isSelected = false
            }
        }
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: indexPath.row) as? VLSGiftModel {
            if let model = item.giftModel {
                if model.giftType == 1, model.isLocal == 0 {//动态礼物
                    let basePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
                    var filePath = basePath.appending(String(format: "/%@.gif", StringSafty(model.folderName)))
                    if model.gifResource == "" {
                        filePath = basePath.appending(String(format: "/%@", StringSafty(model.folderName)))
                    }
                    if FileManager.default.fileExists(atPath: filePath) {
                        footerView.sendStatus(true)
                    } else {
                        self.showHUD(LocalizedString("DYNMAIC_GIFT_DOWNLOAD"))
                        footerView.sendStatus(false)
                    }
                } else {
                    footerView.sendStatus(true)
                }
                item.isSelected = true
                self.collectionView.reloadData()
                preIndex = indexPath.row
                footerView.removeCircle()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = Int(fabs(scrollView.contentOffset.x)/scrollView.frame.width)
        footerView.currentPage(num: index)
    }
    
    func sendGift() {
        if let item = ObjectForKeySafety(NSArraySafty(viewModel.dataSource), index: preIndex) as? VLSGiftModel {
            if let model = item.giftModel {
                if model.giftPrice > footerView.remainVDiamond {
                    self.tips()
                    if let _ = footerView.circleBt {
                        footerView.removeCircle()
                    }
                } else {
                    footerView.remainVDiamond -= model.giftPrice
                }
                delegate?.sendGift(model)
                footerView.vdiamondLab.text = "\(footerView.remainVDiamond)"
            }
        }
    }
    
    func tips() {
        let alertVC = UIAlertController(title: LocalizedString("BALANCE_NOTENOUGH"), message: LocalizedString("CLICK_TORECHARGE"), preferredStyle: .alert)
        let action = UIAlertAction(title: LocalizedString("INVITE_SURE"), style: .default, handler: { [weak self] action in
            self?.chargeVDiamond()
        })
        let cancelAction = UIAlertAction(title: LocalizedString("INVITE_CANCEL"), style: .cancel, handler: nil)
        alertVC.addAction(action)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func chargeVDiamond() {
        self.disappear()
        delegate?.chargeVDiamond()
    }

}

@objc protocol VLSGiftFooterViewDelegate {
    func sendGift()
    func chargeVDiamond()
}

class VLSGiftFooterView: UICollectionReusableView {
    var pageControl: UIPageControl!
    var chargeBt: UIButton!
    var vdiamondLab: UILabel!
    var sendBt: UIButton!
    var circleBt: DrawCircleProgressButton!
    var remainVDiamond: NSInteger = AccountManager.shared().account.diamondBalance
    weak var delegate: VLSGiftFooterViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        vdiamondLab = UILabel().then {
            $0.textColor = HEX(COLOR_FONT_FFFFFF)
            $0.text = "\(AccountManager.shared().account.diamondBalance)"
            $0.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
            $0.textAlignment = .center
            $0.sizeToFit()
            $0.adjustsFontSizeToFitWidth = true
        }
        self.addSubview(vdiamondLab)
        chargeBt = UIButton().then {
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_16))
            $0.setTitleColor(UIColor(red: 195/256.0, green: 168/256.0, blue: 51/256.0, alpha: 1), for: .normal)
            $0.setTitle(LocalizedString("RECHARGE")+">", for: .normal)
            $0.addTarget(self, action: #selector(VLSGiftFooterView.chargeVDiamond), for: .touchUpInside)
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
        }
        self.addSubview(chargeBt)
        sendBt = UIButton().then {
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_17))
            $0.setTitleColor(HEX(COLOR_FONT_FFFFFF), for: .normal)
            $0.setTitle(LocalizedString("SENDGift"), for: .normal)
            $0.addTarget(self, action: #selector(VLSGiftFooterView.sendGift), for: .touchUpInside)
            $0.backgroundColor = HEX(COLOR_BG_FF1130)
            $0.layer.cornerRadius = 2
        }
        self.addSubview(sendBt)
        
        if UIScreen.main.applicationFrame.width > UIScreen.main.applicationFrame.height {//横屏
            sendBt.snp.makeConstraints { (make) in
                make.centerX.equalTo(self)
                make.top.equalTo(10)
                make.size.equalTo(CGSize(width: 64, height: 36))
            }
            chargeBt.snp.makeConstraints { (make) in
                make.top.equalTo(sendBt.snp.bottom)
                make.bottom.right.equalTo(self)
                make.width.equalTo(50)
            }
            vdiamondLab.snp.makeConstraints { (make) in
                make.top.bottom.equalTo(chargeBt)
                make.left.equalTo(10)
                make.right.equalTo(chargeBt.snp.left)
            }
        } else {//竖屏
            pageControl = UIPageControl().then {
                $0.currentPageIndicatorTintColor = HEX(COLOR_FONT_C6C6C6)
                $0.pageIndicatorTintColor = HEX(COLOR_FONT_4A4A4A)
                $0.currentPage = 0
            }
            self.addSubview(pageControl)
            pageControl.snp.makeConstraints { (make) in
                make.top.equalTo(6)
                make.left.right.equalTo(self)
                make.height.equalTo(10)
            }
            vdiamondLab.snp.makeConstraints { (make) in
                make.top.bottom.equalTo(self)
                make.left.equalTo(10)
            }
            chargeBt.snp.makeConstraints { (make) in
                make.top.bottom.equalTo(vdiamondLab)
                make.left.equalTo(vdiamondLab.snp.right)
                make.width.equalTo(70)
            }
            sendBt.snp.makeConstraints { (make) in
                make.centerY.equalTo(self)
                make.right.equalTo(-10)
                make.size.equalTo(CGSize(width: 64, height: 36))
            }
        }
    }
    
    func sendStatus(_ status: Bool) {
        sendBt.isEnabled = status
        if status {
            sendBt.backgroundColor = HEX(COLOR_BG_FF1130)
        } else {
            sendBt.backgroundColor = UIColor.lightGray
        }
    }
    
    func numberOfPages(num: Int) {
        if let pageControl = self.pageControl {
            pageControl.numberOfPages = num
        }
    }
    
    func currentPage(num: Int) {
        if let pageControl = self.pageControl {
            pageControl.currentPage = num
        }
    }
    
    func chargeVDiamond() {
        delegate?.chargeVDiamond()
    }
    
    func sendGift() {
        self.initCircleBt()
        self.setTimerStamp()
        sendBt.isHidden = true
        circleBt.isHidden = false
        circleBt.resetTimer()
        delegate?.sendGift()
    }
    
    func resetProgress() {
        VLSGiftManager.shared().currentCount = +1
        circleBt.resetTimer()
        delegate?.sendGift()
    }
    
    func setTimerStamp() {
        VLSGiftManager.shared().currentCount = 1
        VLSGiftManager.shared().timerStamp = "\(Date().timeIntervalSince1970)"
    }
    
    func removeCircle() {
        if let circleBt = self.circleBt {
            self.setTimerStamp()
            circleBt.stopTimer()
            circleBt.isHidden = true
            sendBt.isHidden = false
        }
    }
    
    func initCircleBt() {
        if circleBt == nil {
            circleBt = DrawCircleProgressButton(frame: CGRect(x: 0, y: 0, width: 75, height: 75)).then {
                $0.lineWidth = 4
                $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(SIZE_FONT_14))
                $0.setCs_acceptEventIntervalFlag(true)
                $0.delegate = self
                $0.addTarget(self, action: #selector(VLSGiftFooterView.resetProgress), for: .touchUpInside)
                $0.animationDuration = 5
                $0.isHidden = true
            }
            self.superview?.addSubview(circleBt)
            if UIScreen.main.applicationFrame.width > UIScreen.main.applicationFrame.height {//横屏
                circleBt.snp.makeConstraints { (make) in
                    make.center.equalTo(self.snp.center)
                    make.size.equalTo(CGSize(width: 75, height: 75))
                }
            } else {//竖屏
                circleBt.snp.makeConstraints { (make) in
                    make.bottom.right.equalTo(self).offset(-6)
                    make.size.equalTo(CGSize(width: 75, height: 75))
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VLSGiftFooterView: CircleProgressButtonDelegate {
    func animationCompleted() {
        circleBt.isHidden = true
        sendBt.isHidden = false
    }
}

class VLSGiftModel: NSObject {
    var isSelected: Bool = false
    var giftModel: GiftModel?
}
