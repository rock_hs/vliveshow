//
//  ImageAnimationsView.m
//  LiveAnimation
//
//  Created by lllll-xy on 16/8/5.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "ImageAnimationsView.h"
#import "UIView+LYAdd.h"
#import <YYImage/YYAnimatedImageView.h>
#import <YYImage/YYImage.h>

static YYImage *GIFIMAGE;
static NSString *GIFID;

@interface ImageAnimationsView ()

@property (nonatomic,strong)UIImageView * ImageV;
@property (nonatomic,strong)NSMutableArray * ImageArr;
@property (nonatomic, strong) YYAnimatedImageView *animatedImageView;
@property (nonatomic, copy) void (^completeBlock)();
@end

@implementation ImageAnimationsView
- (void)setGiftModel:(GiftModel *)giftModel {
    _giftModel = giftModel;
    if ([self hasGifResource]) {
        self.animatedImageView = [[YYAnimatedImageView alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height)];
        [self addSubview:self.animatedImageView];
        [self.animatedImageView addObserver:self forKeyPath:@"currentAnimatedImageIndex" options:NSKeyValueObservingOptionNew context:nil];
    }
}

- (void)dealloc {
    [self.animatedImageView removeObserver:self forKeyPath:@"currentAnimatedImageIndex"];
}

- (instancetype)initWithFrame:(CGRect)frame{
  self = [super initWithFrame:frame];
    if (self) {
        if ([self hasGifResource]) {
            self.animatedImageView = [[YYAnimatedImageView alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height)];
            self.animatedImageView.center = self.center;
            [self addSubview:self.animatedImageView];
            [self.animatedImageView addObserver:self forKeyPath:@"currentAnimatedImageIndex" options:NSKeyValueObservingOptionNew context:nil];
        }else {
            _ImageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height)];
            _ImageV.center = self.center;
            _ImageV.contentMode = UIViewContentModeScaleAspectFit;
            [self addSubview:_ImageV];
            _animationDuration = 4;
            _animationRepeatCount = 1;
            _ViewanimationDuration = _animationDuration;
        }
  }
  return self;
}

-(void)startAnimationAndCompleted:(void (^)())completed {
    self.completeBlock = completed;
    if ([self hasGifResource]) {
        GIFIMAGE = [self gifImage];
        self.animatedImageView.image = GIFIMAGE;
    }else {
        _ImageV.animationImages = self.ImageArr;
        _ImageV.animationDuration = _animationDuration;
        _ImageV.animationRepeatCount = _animationRepeatCount;
        [_ImageV startAnimating];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(_ViewanimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (completed) {
                [_ImageV stopAnimating];
                [_ImageArr removeAllObjects];
                _ImageArr = nil;
                completed();
            }
        });
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    NSUInteger count = 0;
    if ([keyPath isEqualToString:@"currentAnimatedImageIndex"]) {
        count = [GIFIMAGE animatedImageFrameCount];
        NSUInteger currentCount = [[[change allValues] lastObject] unsignedIntegerValue];
        if (count - 1 == currentCount) {
            [self.animatedImageView stopAnimating];
            [self.animatedImageView removeFromSuperview];
            if (self.completeBlock) {
                self.completeBlock();
            }
        }
    }
}

- (BOOL)hasGifResource {
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([path count] > 0) ? [path objectAtIndex:0] : nil;
    BOOL res = [[NSFileManager defaultManager] fileExistsAtPath:[basePath stringByAppendingString:[NSString stringWithFormat:@"/%@.gif", self.giftModel.folderName]]];
    return res;
}

-(NSMutableArray *)ImageArr{
  if (!_ImageArr) {
    _ImageArr = [NSMutableArray array];
    NSArray * arr = [self getFolderCount:self.giftModel.isLocal FolderName:self.giftModel.folderName];
    if (arr.count > 0) {
      for (int i = 1; i <= arr.count; i ++) {
        if (self.giftModel.isLocal) {
          NSString *thumbnailFile = [self getBundlePath];
          NSBundle *bundle = [NSBundle bundleWithPath:thumbnailFile];
          [_ImageArr addObject:[UIImage imageWithContentsOfFile:[bundle pathForResource:[NSString stringWithFormat:@"%@%d",self.giftModel.folderName,i] ofType:@"png"]]];
        }else{
            UIImage* image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@%d.png",[self getCachesPath],self.giftModel.folderName,i]];
            if (image != nil) {
                [_ImageArr addObject:image];
            }
        }
      }
    }
  }
  return _ImageArr;
}

- (YYImage*)gifImage {
    YYImage *image = [YYImage new];
    NSString *path = [[self getCachesPath] stringByAppendingString:@".gif"];
    if ([self.giftModel.giftID isEqualToString:GIFID]) {
        if (GIFIMAGE) {
            image = GIFIMAGE;
        }else {
            GIFIMAGE = [YYImage imageWithContentsOfFile:path];
            image = GIFIMAGE;
        }
    }else {
        GIFIMAGE = [YYImage imageWithContentsOfFile:path];
        image = GIFIMAGE;
        GIFID = self.giftModel.giftID;
    }
    /*
    if (![self.giftModel.giftID isEqualToString:GIFID] && [self hasGifResource]) {
        NSString *path = [[self getCachesPath] stringByAppendingString:[@"/" stringByAppendingString:@".gif"]];
        GIFIMAGE = [YYImage imageWithContentsOfFile:path];
        image = GIFIMAGE;
        GIFID = self.giftModel.giftID;
    }else {
        image = GIFIMAGE;
    }
    */
    return image;
}

- (NSArray *)getFolderCount:(BOOL)isLocal FolderName:(NSString *)folderName{
  
  if (isLocal) {
    
    NSString *imagepath = [self getBundlePath];
    
    NSLog(@"Path to Images: %@", imagepath);
    
    NSArray * paths = [NSBundle pathsForResourcesOfType: @"png" inDirectory:imagepath];
    NSMutableArray * allImageNames = [[NSMutableArray alloc] init];
    
    for ( NSString * path in paths )
    {
      if ( [[path lastPathComponent] hasPrefix: @"AQ"] )
        continue;
      
      [allImageNames addObject: [path lastPathComponent]];
      
    }
    
    return allImageNames;
    
  }else{
    
    NSFileManager * fm = [NSFileManager defaultManager];
    NSString * url = [self getCachesPath];
    NSMutableArray * allImageNames = [[NSMutableArray alloc] init];
    
    if ([fm fileExistsAtPath:url]) {
      
      for ( NSString * path in [fm subpathsAtPath:url])
      {
        if ( ![[path pathExtension] isEqualToString:@"png"] )
          continue;
        
        [allImageNames addObject: [path lastPathComponent]];
        
      }

    }
    
    return allImageNames;
  }

}

- (NSString * )getCachesPath{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([path count] > 0) ? [path objectAtIndex:0] : nil;
    if (self.giftModel.gifResource > 0) return basePath;
    else return [basePath stringByAppendingString:[NSString stringWithFormat:@"/%@",self.giftModel.folderName]];
}

- (NSString *)getBundlePath{
  return [NSString stringWithFormat:@"%@/%@.bundle", [[NSBundle mainBundle] resourcePath],self.giftModel.folderName];
}

@end
