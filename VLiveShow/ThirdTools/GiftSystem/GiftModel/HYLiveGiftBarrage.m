//
//  HYLiveGiftBarrage.m
//  LiveAnimation
//
//  Created by lllll-xy on 16/7/22.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "HYLiveGiftBarrage.h"
#import "AnimOperation.h"
#import "BigAnimOperation.h"

@interface HYLiveGiftBarrage ()

@property (nonatomic,assign)int index;
@property (nonatomic, weak) UIView *barrageSuperView;
@property (nonatomic, weak) UIView *giftSuperView;

@property (nonatomic,strong) NSOperationQueue *queue; // 全局动画队列
@property (nonatomic,strong) NSOperationQueue *BigGiftQueue; // 全局动态动画队列

@end

@implementation HYLiveGiftBarrage

- (instancetype)initBarrageToView:(UIView *)barrageSuperView giftToView:(UIView *)giftSuperView {
  
  self = [super init];
  if (self) {
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.maxConcurrentOperationCount = 1;
    
    NSOperationQueue *bigGiftQueue = [[NSOperationQueue alloc] init];
    bigGiftQueue.maxConcurrentOperationCount = 1;
    
      _BigGiftQueue = bigGiftQueue;
      _queue = queue;
      _index = 0;
      self.barrageSuperView = barrageSuperView;
      self.giftSuperView = giftSuperView;
    
  }
  return self;
  
}

- (void)addBarrageModel:(GiftModel *)barrageModel{

  AnimOperation *op = [[AnimOperation alloc] init];
  op.listView = self.barrageSuperView;
  op.giftModel = barrageModel;
  [_queue addOperation:op];
  
}

- (void)addGiftEnum:(GiftModel *)bigGiftModel{
  
  BigAnimOperation *op = [[BigAnimOperation alloc] init];
  op.fromView = self.giftSuperView;
  op.giftModel = bigGiftModel;
  [_BigGiftQueue addOperation:op];
  
}

- (void)stopAllOperation{
  
  [_queue cancelAllOperations];
  [_BigGiftQueue cancelAllOperations];
  
}

@end
