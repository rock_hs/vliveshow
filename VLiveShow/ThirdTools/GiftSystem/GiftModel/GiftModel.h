//
//  GiftModel.h
//  LiveAnimation
//
//  Created by lllll-xy on 16/7/20.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftModel : NSObject

/*!
 @brief 用户ID
 */
@property (nonatomic,copy) NSString *userID;

/*!
 @brief 房间ID
 */
@property (nonatomic,copy) NSString *roomID;

/*!
 @brief 礼物ID
 */
@property (nonatomic,copy) NSString *giftID;

/*!
 @brief 动态图片的比例
 */
@property (nonatomic,assign) float aspectRatio;

/*!
 @brief 头像
 */
@property (nonatomic,strong) NSString *headImage;

/*!
 @brief 礼物图片
 */
@property (nonatomic,strong) NSString *giftImage;

/*!
 @brief 礼物图片
 */
@property (nonatomic,strong) NSString *giftThirdImage;

/*!
 @brief 送礼物者
 */
@property (nonatomic,copy) NSString *name;

/*!
 @brief 礼物名称
 */
@property (nonatomic,copy) NSString *giftName;

/*!
 @brief 礼物个数
 */
@property (nonatomic,assign) NSInteger giftCount;

/*!
 @brief 当前数
 */
@property (nonatomic,assign) NSInteger currentCount;

/*!
 @brief 时间戳
 */
@property (nonatomic,copy) NSString * timerStamp;

/*!
 @brief 礼物价格
 */
@property (nonatomic,assign) NSInteger giftPrice;

/*!
 @brief 礼物Type 1:动态礼物   0:静态礼物
 */
@property (nonatomic,assign) NSInteger giftType;

/*!
 @brief 礼物单位
 */
@property (nonatomic,copy) NSString *giftUnit;

/*!
 @brief 礼物经验
 */
@property (nonatomic,copy) NSString *experience;

/*!
 @brief 礼物表情包下载路劲
 */
@property (nonatomic,copy) NSString *resource;

/*!
 @brief gif表情包下载路劲
 */
@property (nonatomic,copy) NSString *gifResource;

/*!
 @brief 礼物文件夹名称
 */
@property (nonatomic,copy) NSString *folderName;

/*!
 @brief 是否是本地图片(即上线时自带)
 */
@property (nonatomic,assign) NSInteger isLocal;

/*!
 @brief 礼物属性数组
 */
@property (nonatomic,copy) NSArray *attrs;

/*!
 @brief 重复次数
 */
@property (nonatomic,assign) NSInteger repeatTimes;

/*!
 @brief 播放一次的时间
 */
@property (nonatomic,assign) NSInteger playOnceTime;

@property (nonatomic, copy) void (^saveGiftdataCompletionBlock)();

/* 获取礼物列表 */
+ (NSArray *)getGiftPlist;

/* 设置礼物列表 */
- (void)setGiftPlist:(NSArray *)giftList;

/* 删除礼物列表 */
- (void)deletedGiftImg:(NSArray *)arr;

+ (int)getFolderImageCountForPath:(NSString *)basePath FolderName:(NSString *)folderName;

@end

@interface GiftCountModel : NSObject

@property (nonatomic,copy) NSMutableArray *GiftArr;

+ (GiftCountModel*)sharedGiftCountModel;

+ (NSInteger)getSameCount:(NSString *)UidAGiftID;

@end
