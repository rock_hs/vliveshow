
//
//  GiftModel.m
//  LiveAnimation
//
//  Created by lllll-xy on 16/7/20.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "GiftModel.h"
#import "HTTPFacade.h"
#import "SSZipArchive.h"

@interface GiftModel ()<SSZipArchiveDelegate>

@end

@implementation GiftModel

+ (NSArray *)getGiftPlist{
  
  NSArray *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
  //获取完整路径
  NSString *documentsPath = [path objectAtIndex:0];
  NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"VLSGift.plist"];
  NSMutableArray * giftList = [[NSMutableArray alloc] initWithContentsOfFile:plistPath];
  NSMutableArray * array = [[NSMutableArray alloc] init];

  for (NSDictionary * dict in giftList) {

    GiftModel * model = [[GiftModel alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    [array addObject:model];
  }
  
  return array;
  
}

- (void)setGiftPlist:(NSArray *)giftList{
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    //获取完整路径
    NSString *documentsPath = [path objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"VLSGift.plist"];
    NSMutableArray *  giftListArr = [[NSMutableArray alloc] init];
    
    for (NSDictionary * dict in giftList) {
        
        if (![[dict objectForKey:@"resource"] isEqualToString:@""]) {
            [self downloadImageZip:[dict objectForKey:@"resource"] folderName:[dict objectForKey:@"folderName"]];
        }
        if (![[dict objectForKey:@"gifResource"] isEqualToString:@""]) {
            [self downloadGif:[dict objectForKey:@"gifResource"] folderName:[dict objectForKey:@"folderName"]];
        }
        
        NSMutableDictionary * giftDict = [[NSMutableDictionary alloc] init];
        [giftDict setObject:[dict objectForKey:@"folderName"] forKey:@"folderName"];
        [giftDict setObject:[dict objectForKey:@"giftType"] forKey:@"giftType"];
        [giftDict setObject:[dict objectForKey:@"id"] forKey:@"giftID"];
        [giftDict setObject:[dict objectForKey:@"name"] forKey:@"giftName"];
        [giftDict setObject:[dict objectForKey:@"thumb"] forKey:@"giftImage"];
        [giftDict setObject:[dict objectForKey:@"thumb3x"] forKey:@"giftThirdImage"];
        [giftDict setObject:[dict objectForKey:@"diamond"] forKey:@"giftPrice"];
        [giftDict setObject:[dict objectForKey:@"resource"] forKey:@"resource"];
        [giftDict setObject:[dict objectForKey:@"isLocal"] forKey:@"isLocal"];
        [giftDict setObject:[dict objectForKey:@"aspectRatio"] forKey:@"aspectRatio"];
        [giftDict setObject:[dict objectForKey:@"playOnceTime"] forKey:@"playOnceTime"];
        [giftDict setObject:[dict objectForKey:@"repeatTimes"] forKey:@"repeatTimes"];
        if ([[dict allKeys] containsObject:@"gifResource"]) {
            [giftDict setObject:[dict objectForKey:@"gifResource"] forKey:@"gifResource"];
        }
        
        NSMutableArray * attrsArr = [[NSMutableArray alloc] init];
        NSArray * giftAttrsArr =[dict objectForKey:@"attrs"];
        
        for (NSDictionary * attrsDict in giftAttrsArr) {
            
            NSMutableDictionary * giftAttrsDict = [[NSMutableDictionary alloc] init];
            [giftAttrsDict setObject:[attrsDict objectForKey:@"i18n"] forKey:@"i18n"];
            [giftAttrsDict setObject:[attrsDict objectForKey:@"name"] forKey:@"name"];
            [giftAttrsDict setObject:[attrsDict objectForKey:@"unit"] forKey:@"unit"];
            [attrsArr addObject:giftAttrsDict];
            
        }
        
        [giftDict setObject:attrsArr forKey:@"attrs"];
        
        [giftListArr addObject:giftDict];
        
    }
    
//    [giftListArr writeToFile:plistPath atomically:YES];
    if ([giftListArr writeToFile:plistPath atomically:YES]) {
        if (self.saveGiftdataCompletionBlock) {
            self.saveGiftdataCompletionBlock();
        }
    }
    NSLog(@"%d",[giftListArr writeToFile:plistPath atomically:YES]);
}

- (void)downloadImageZip:(NSString *)url folderName:(NSString *)folderName{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([path count] > 0) ? [path objectAtIndex:0] : nil;
        if (![[NSFileManager defaultManager] fileExistsAtPath:[basePath stringByAppendingString:[NSString stringWithFormat:@"/%@",folderName]]] || [self.class getFolderImageCountForPath:basePath FolderName:folderName] <= 0 ) {
            NSString * urlS = [basePath stringByAppendingString:[NSString stringWithFormat:@"/%@.zip",folderName]];
            [HTTPFacade download:url folderName:folderName completion:^() {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                    [SSZipArchive unzipFileAtPath:urlS toDestination:basePath delegate:self];
                });
            }];
        };
    });
    /*
  NSArray *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
  NSString *basePath = ([path count] > 0) ? [path objectAtIndex:0] : nil;
  if (![[NSFileManager defaultManager] fileExistsAtPath:[basePath stringByAppendingString:[NSString stringWithFormat:@"/%@",folderName]]] || [self.class getFolderImageCountForPath:basePath FolderName:folderName] <= 0 ) {
    NSString * urlS = [basePath stringByAppendingString:[NSString stringWithFormat:@"/%@.zip",folderName]];
    [HTTPFacade download:url folderName:folderName completion:^() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           //[SSZipArchive unzipFileAtPath:urlS toDestination:basePath delegate:self];
        });
    }];
  };
     */
}

- (void)downloadGif:(NSString *)url folderName:(NSString *)folderName{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([path count] > 0) ? [path objectAtIndex:0] : nil;
        if (![[NSFileManager defaultManager] fileExistsAtPath:[basePath stringByAppendingString:[NSString stringWithFormat:@"/%@.gif",folderName]]]) {
            [HTTPFacade downGifgiftLoad:url folderName:folderName completion:^{
                
            }];
        };
    });
    /*
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([path count] > 0) ? [path objectAtIndex:0] : nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:[basePath stringByAppendingString:[NSString stringWithFormat:@"/%@.gif",folderName]]]) {
        [HTTPFacade downGifgiftLoad:url folderName:folderName completion:^{
     
        }];
    };
    */
}

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath{
  
  NSLog(@"解压成功%@",path);
  [self deleteFolder:path];
  
}

- (void)deleteFolder:(NSString *)path{

  NSFileManager *fm = [NSFileManager defaultManager];
  
  if ([fm removeItemAtPath:path error:nil] == NO) {
    NSLog(@"删除失败");
  }else{
    NSLog(@"删除成功");
  }
  
}

- (void)deletedGiftImg:(NSArray *)arr{

  for (NSDictionary * dict in arr) {

    if ([[dict objectForKey:@"giftType"] integerValue] == 1) {
      
      NSFileManager *fm = [NSFileManager defaultManager];
      if ([[dict objectForKey:@"isLocal"] integerValue] == YES) {
        
        if ([fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@.bundle", [[NSBundle mainBundle] resourcePath],[dict objectForKey:@"folderName"]] error:nil]) {
          NSLog(@"删除成功");
        }
        
      }else{
        
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([path count] > 0) ? [path objectAtIndex:0] : nil;
        
        if ([fm removeItemAtPath:[basePath stringByAppendingString:[NSString stringWithFormat:@"/%@",[dict objectForKey:@"folderName"]]] error:nil]) {
            if (self.saveGiftdataCompletionBlock) {
                self.saveGiftdataCompletionBlock();
            }
          NSLog(@"删除成功");
        }
      }
      
    }
    
  }
  
}

+ (int)getFolderImageCountForPath:(NSString *)basePath FolderName:(NSString *)folderName{

  NSFileManager *fm = [NSFileManager defaultManager];
  int count = 0;
  for ( NSString * path in [fm subpathsAtPath:[basePath stringByAppendingString:[NSString stringWithFormat:@"/%@",folderName]]])
  {
    
    if ( ![[path pathExtension] isEqualToString:@"png"] )
      continue;
    count++;
    
  }
  
  return count;
  
}

@end

@implementation GiftCountModel

+ (GiftCountModel *)sharedGiftCountModel {
  
  static GiftCountModel *sharedGiftCountModel = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedGiftCountModel = [[self alloc] init];
  });
  return sharedGiftCountModel;
  
}

- (NSMutableArray *)GiftArr{
  
  if (!_GiftArr) {
    _GiftArr = [[NSMutableArray alloc] init];
  }
  return _GiftArr;
  
}

+ (NSInteger)getSameCount:(NSString *)UidAGiftID{
  
  NSInteger count = 0;
  
  @autoreleasepool {
    
    for (NSString * str in [GiftCountModel sharedGiftCountModel].GiftArr) {
      if ([str isEqualToString:UidAGiftID]) {
        count ++;
      }
    }
    
  }
  
  return count;
  
}

@end
