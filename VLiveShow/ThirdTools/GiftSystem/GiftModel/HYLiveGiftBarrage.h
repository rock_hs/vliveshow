//
//  HYLiveGiftBarrage.h
//  LiveAnimation
//
//  Created by lllll-xy on 16/7/22.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LYLiveGiftBarrageView.h"
#import "GiftModel.h"
#import "BigAnimOperation.h"

@interface HYLiveGiftBarrage : NSObject

- (instancetype)initBarrageToView:(UIView *)barrageSuperView giftToView:(UIView *)giftSuperView;

- (void)addBarrageModel:(GiftModel *)barrageModel;
- (void)addGiftEnum:(GiftModel *)bigGiftModel;

- (void)stopAllOperation;

@end
