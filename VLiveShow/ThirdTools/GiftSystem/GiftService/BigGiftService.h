//
//  BigGiftService.h
//  LiveAnimation
//
//  Created by lllll-xy on 16/8/4.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageAnimationsView.h"
#import "GiftModel.h"

@interface BigGiftService : NSObject

@property (nonatomic,strong)UIView * fromView;

+ (BigGiftService *)shareBigGiftService;

/*
 @pragma 动画线程控制
 @param fromView - 显示视图
 @param bigGiftEnum - 播放什么礼物
 */
- (void)bigGiftQueueControlForView:(UIView *)fromView withGiftModel:(GiftModel *)bigGiftModel Completed:(void(^)())completed;

@end
