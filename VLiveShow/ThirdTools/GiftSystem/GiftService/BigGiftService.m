//
//  BigGiftService.m
//  LiveAnimation
//
//  Created by lllll-xy on 16/8/4.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "BigGiftService.h"
#import "UIView+LYAdd.h"

@interface BigGiftService ()

@property (nonatomic,strong)ImageAnimationsView * animationsView;
@property (nonatomic,assign) GiftModel * giftModel;
@end

static BigGiftService* service;
static dispatch_once_t onceToken;

@implementation BigGiftService

#pragma mark - 单例

+ (BigGiftService *)shareBigGiftService{
  
  dispatch_once(&onceToken, ^{
    
    service = [[BigGiftService alloc]init];
    
  });
  
  return service;
  
}

- (void)bigGiftQueueControlForView:(UIView *)fromView withGiftModel:(GiftModel *)bigGiftModel Completed:(void(^)())completed{
  
  self.fromView = fromView;
  _giftModel = bigGiftModel;
  [self.fromView addSubview:self.animationsView];
  
//    float width = MIN(SCREEN_WIDTH, SCREEN_HEIGHT);
//    _animationsView.frame = CGRectMake(0, 0, width, width * bigGiftModel.aspectRatio);
//    _animationsView.center = self.fromView.center;//CGPointMake(self.fromView.width / 2, self.fromView.height / 2);
    
//  _animationsView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH * bigGiftModel.aspectRatio);
//  _animationsView.center = CGPointMake(self.fromView.width / 2, self.fromView.height / 2);
  
  self.animationsView.giftModel = self.giftModel;
  self.animationsView.animationDuration = self.giftModel.playOnceTime / 1000.0;
  self.animationsView.animationRepeatCount = self.giftModel.repeatTimes;
  self.animationsView.ViewanimationDuration = self.giftModel.playOnceTime / 1000.0 * self.giftModel.repeatTimes;
  [self startAndCompleted:^{
    completed();
  }];
  
}

- (void)startAndCompleted:(void(^)())completed{

  __weak typeof(self) ws = self;
  [self.animationsView startAnimationAndCompleted:^{
    
    [ws.animationsView removeFromSuperview];
    ws.animationsView = nil;
    completed();
    
  }];
  
}

-(ImageAnimationsView *)animationsView{
  
  if (!_animationsView) {
      float width = MIN(SCREEN_WIDTH, SCREEN_HEIGHT);
//      _animationsView = [[ImageAnimationsView alloc] initWithFrame:CGRectMake(0,0, self.fromView.width, self.fromView.width)];
      _animationsView = [[ImageAnimationsView alloc] initWithFrame:CGRectMake(0,0, width, width)];
      _animationsView.center = self.fromView.center;//CGPointMake(self.fromView.width / 2, self.fromView.height / 2);

  }
  return _animationsView;
  
}

@end
