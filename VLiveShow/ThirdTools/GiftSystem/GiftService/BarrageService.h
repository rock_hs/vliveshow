//
//  BarrageService.h
//  LiveAnimation
//
//  Created by lllll-xy on 16/7/26.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiftModel.h"
#import "AnimOperation.h"
#import "LYLiveGiftBarrageView.h"

typedef enum : NSUInteger {
  
  GIFTADD,
  GIFTHAVEONE,
  GIFTHAVETWO,
  GIFTNOTFOUNT,
  
} GiftEnum;

@interface BarrageService : NSObject

@property (nonatomic,strong) GiftModel *model;
@property (nonatomic,assign) GiftEnum giftEnum;

@property (nonatomic,assign)AnimOperation * operation;

@property (nonatomic,strong)LYLiveGiftBarrageView * firstGiftBarrage;
@property (nonatomic,strong)LYLiveGiftBarrageView * secondGiftBarrage;

@property (nonatomic,strong)LYLiveGiftBarrageView * saveView;

@property (nonatomic,assign) BOOL isHaveReady;

@property (nonatomic,strong)UIView * fromView;

+ (BarrageService *)shareBarrageService;

/* 
 @pragma 动画线程控制
 @param fromView - 显示视图
 @param model - 当前队列的中礼物的model
 */
- (void)giftQueueControlForView:(UIView *)fromView withGiftModel:(GiftModel *)model Completed:(void(^)())completed;

@end
