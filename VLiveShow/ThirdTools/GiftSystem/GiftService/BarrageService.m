//
//  BarrageService.m
//  LiveAnimation
//
//  Created by lllll-xy on 16/7/26.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "BarrageService.h"
#import "VLSMessageBoard.h"
#import "HYLiveGiftBarrage.h"

#define Height SCREEN_HEIGHT - 180 * SCREEN_WIDTH / 320 - 100

static BarrageService* service;
static dispatch_once_t onceToken;

@interface BarrageService ()

@end

@implementation BarrageService

#pragma mark - 单例

+ (BarrageService *)shareBarrageService{
  
  dispatch_once(&onceToken, ^{
    
    service = [[BarrageService alloc]init];
    
  });
  
  return service;
  
}

- (void)giftQueueControlForView:(UIView *)fromView withGiftModel:(GiftModel *)model Completed:(void(^)())completed{
  
  self.model = model;
  self.fromView = fromView;
  
  if ([self searchBarrageView:fromView]) {
    
    if (self.giftEnum == GIFTHAVEONE) {
      
      if ([_model.userID isEqualToString:_firstGiftBarrage.model.userID] && [_model.giftID isEqualToString:_firstGiftBarrage.model.giftID]) {
        
        self.firstGiftBarrage.model = model;

        [self.firstGiftBarrage startCountAddAnimatingCompleted:^{
          self.firstGiftBarrage.animCount += 1;
          completed();
        }];
        
      }else if([_saveView.model.userID isEqualToString:_secondGiftBarrage.model.userID] && [_saveView.model.giftID isEqualToString:_secondGiftBarrage.model.giftID]){
        
        self.secondGiftBarrage.model = model;
 
        [self.secondGiftBarrage startCountAddAnimatingCompleted:^{
          self.secondGiftBarrage.animCount += 1;
          completed();
        }];
        
      }
      
    }
    
  }else{
    
    if (self.giftEnum == GIFTNOTFOUNT) {
      NSLog(@"NOTFOUNT");
      [self initgiftBarrageView:self.firstGiftBarrage];
      self.firstGiftBarrage.model = model;
      self.firstGiftBarrage.iSOPEN = YES;
      self.firstGiftBarrage.frame = CGRectMake(-fromView.frame.size.width / 2, [self searchChatView], fromView.frame.size.width / 2, 53);
      [fromView addSubview:_firstGiftBarrage];
      [self.firstGiftBarrage startAnimatingCompleted:^{
        completed();
      }];
      
    }else if (self.giftEnum == GIFTHAVEONE){
       NSLog(@"1111111");
      if (self.firstGiftBarrage.iSOPEN == YES) {
        
        [self initgiftBarrageView:self.secondGiftBarrage];
        self.secondGiftBarrage.model = model;
        
        self.secondGiftBarrage.frame = CGRectMake(-fromView.frame.size.width / 2, [self searchChatView] - 55, fromView.frame.size.width / 2, 53);
        [fromView addSubview:_secondGiftBarrage];
        [self.secondGiftBarrage startAnimatingCompleted:^{
          completed();
        }];

      }else{
        
        [self initgiftBarrageView:self.firstGiftBarrage];
        self.firstGiftBarrage.model = model;
        
        self.firstGiftBarrage.frame = CGRectMake(-fromView.frame.size.width / 2, [self searchChatView], fromView.frame.size.width / 2, 53);
        [fromView addSubview:_firstGiftBarrage];
        [self.firstGiftBarrage startAnimatingCompleted:^{
          completed();
        }];
        
      }
      
    }else{

      _isHaveReady = YES;
      _firstGiftBarrage.SuperIsReady = YES;
      _secondGiftBarrage.SuperIsReady = YES;
      _firstGiftBarrage.lastTiemr = 0;
      _secondGiftBarrage.lastTiemr = 0;
      
    }
    
  }
  
}

#pragma mark - search

- (BOOL)searchBarrageView:(UIView *)fromView{
  
  int count = 0;
  for (UIView * subview in [fromView subviews]) {
    
    if([subview isKindOfClass:LYLiveGiftBarrageView.class]){
      
      count ++;
      LYLiveGiftBarrageView * giftView = (LYLiveGiftBarrageView *)subview;
        //动态礼物连送
        if ([giftView.model.giftID isEqualToString:_model.giftID] && [giftView.model.userID isEqualToString:_model.userID] && giftView.isReady == NO ) {// && _model.giftType != 1) {

        _saveView = giftView;
        _giftEnum = GIFTHAVEONE;
        return YES;
        
      }
      
    }
    
  }
  
  switch (count) {
    case 0:
      _giftEnum = GIFTNOTFOUNT;
      break;
    
    case 1:
      _giftEnum = GIFTHAVEONE;
      break;
    
    case 2:
      _giftEnum = GIFTHAVETWO;
      break;
      
    default:
      break;
  }
  
  return NO;
  
}

#pragma mark - 初始化视图(giftBarrage)

- (void)initgiftBarrageView:(LYLiveGiftBarrageView *)giftBarrageView{
  
  [giftBarrageView.timer invalidate];
  giftBarrageView.timer = nil;
  [giftBarrageView.skLabel removeFromSuperview];
  giftBarrageView.skLabel = nil;
  giftBarrageView.animCount = 1;
  giftBarrageView.Count = 0;
  if (self.model.currentCount > 1) {
    giftBarrageView.Count = self.model.currentCount - 1;
  }
  
}

#pragma mark - KVO控制等待中的队列

- (void)addObserver{
  
  [self.firstGiftBarrage addObserver:self forKeyPath:@"isReady" options:NSKeyValueObservingOptionNew context:nil];
  [self.secondGiftBarrage addObserver:self forKeyPath:@"isReady" options:NSKeyValueObservingOptionNew context:nil];
  
}

#pragma mark - KVODelegate

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{

  if (_isHaveReady == YES  && [[change objectForKey:@"new"] intValue] == YES) {
    
    self.isHaveReady = NO;
    self.secondGiftBarrage.SuperIsReady = NO;
    self.firstGiftBarrage.SuperIsReady = NO;
    LYLiveGiftBarrageView * readyBarrageView = object;
    [self initgiftBarrageView:readyBarrageView];
    readyBarrageView.iSOPEN = NO;
    readyBarrageView.isReady = NO;
    readyBarrageView.model = self.model;
    [readyBarrageView startAnimatingCompleted:nil];
    self.operation.finished = YES;
    
  }
  
}

#pragma mark - Getter

- (LYLiveGiftBarrageView *)firstGiftBarrage{
  
  if (!_firstGiftBarrage) {
    
    _firstGiftBarrage = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(LYLiveGiftBarrageView.class) owner:self options:nil][0];
    _firstGiftBarrage.iSOPEN = NO;
    _firstGiftBarrage.animCount = 1;
    _firstGiftBarrage.Count = 0;
    [self addObserver];
    
  }
  
  return _firstGiftBarrage;
  
}

- (LYLiveGiftBarrageView *)secondGiftBarrage{
  
  if (!_secondGiftBarrage) {
    
    _secondGiftBarrage = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(LYLiveGiftBarrageView.class) owner:self options:nil][0];
    _secondGiftBarrage.iSOPEN = NO;
    _secondGiftBarrage.animCount = 1;
    _secondGiftBarrage.Count = 0;
    [self addObserver];
    
  }
  
  return _secondGiftBarrage;
  
}

- (float)searchChatView{
  
  for (UIView * subview in [self.fromView subviews]) {
    
    if([subview isKindOfClass:VLSMessageBoard.class]){
    
      return CGRectGetMinY(subview.frame) - 90;
      
    }
    
  }

  return Height;
}

@end
