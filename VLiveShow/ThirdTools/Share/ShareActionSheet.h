//
//  ShareActionSheet.h
//  VLiveShow
//
//  Created by lllll-xy on 16/6/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShareActionSheet;

@protocol ShareActionSheetDelegate <NSObject>

/**
 *  点击的分享按键
 *
 *  @param actionSheet <#actionSheet description#>
 *  @param buttonIndex <#buttonIndex description#>
 */
-(void)xs_actionSheet:(ShareActionSheet *)actionSheet clickedButtonIndex:(NSInteger)index;

@end

@interface ShareActionSheet : UIButton

/**
 *  @preface 初始化frame,delegate对象,图片和标题
 *
 *  @param frame
 *  @param delegate 
 *  @param images  图片数组
 *  @param titles  标题数组
 */
-(instancetype)initWithFrame:(CGRect)frame delegate:(id<ShareActionSheetDelegate>)delegate images:(NSArray *)images;

@property(assign,nonatomic)id<ShareActionSheetDelegate> delegate;

/*
 *  @param showView 获取显示的视图
 */
-(void)showInView:(UIView *)showView;

/*
 *  判断分享按钮对应的应用是否有安装
 */
+ (BOOL)isInstallation:(NSInteger)btnIndex;

- (void)dismiss;

@end
