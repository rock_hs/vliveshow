//
//  ShareActionSheet.m
//  VLiveShow
//
//  Created by lllll-xy on 16/6/27.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "ShareActionSheet.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>

@interface ShareActionSheet(){
  
  UIView * _showContentView;
  UIButton * _cancleButton;
  CGRect _frame;
  NSArray * _images;
  NSArray * _titles;
  
}

@end

@implementation ShareActionSheet

-(instancetype)initWithFrame:(CGRect)frame delegate:(id<ShareActionSheetDelegate>)delegate images:(NSArray *)images{
  
  if (self = [super init]) {
    
    self.delegate = delegate;
    _frame = frame;
    self.backgroundColor = [UIColor clearColor];
    [self addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];

    _showContentView = [[UIView alloc] init];
    _showContentView.backgroundColor = [UIColor whiteColor];
    _showContentView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH,CGRectGetHeight(_frame));
    
    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, SCREEN_WIDTH, 15)];
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.text = LocalizedString(@"LIVE_SHARE");
    titleLabel.textColor = [UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [_showContentView addSubview:titleLabel];
    [self addSubview:_showContentView];
      _showContentView.sd_layout
      .leftSpaceToView(self, 0)
      .rightSpaceToView(self, 0);
      titleLabel.sd_layout
      .widthIs(SCREEN_WIDTH);
      
    _cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cancleButton setFrame:CGRectMake(0, CGRectGetHeight(_showContentView.frame) - 108 / 2, SCREEN_WIDTH , 108 / 2)];
    _cancleButton.layer.cornerRadius = 4;
    _cancleButton.layer.masksToBounds = YES;
    _cancleButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [_cancleButton setTitleColor:[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1] forState:UIControlStateNormal];
    [_cancleButton setTitle:LocalizedString(@"CANCLE") forState:UIControlStateNormal];
    [_cancleButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    [_showContentView addSubview:_cancleButton];
    
    _images = [[NSArray alloc]initWithArray:images];
    [self setShareBtn];
    
    UIView * separateLine = [[UIView alloc]initWithFrame:CGRectMake(0, 207.5, SCREEN_WIDTH, 1)];
    separateLine.backgroundColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1];
    [_showContentView addSubview:separateLine];
  }
  
  return self;
  
}

- (void)setShareBtn{
  for (int i = 0 ; i < _images.count; i++) {
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake((i + 1) * (SCREEN_WIDTH - 225) / 6 + i * 45, 99.3, 45, 45)];
    [button setImage:[UIImage imageNamed:_images[i]] forState:UIControlStateNormal];
    button.tag = i + 200;
    [button addTarget:self action:@selector(clickFunc:) forControlEvents:UIControlEventTouchUpInside];
    [_showContentView addSubview:button];
  }
}

- (void)showInView:(UIView *)showView{

  self.frame = [UIApplication sharedApplication].keyWindow.bounds;
  
  [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.sd_layout
    .widthIs(SCREEN_WIDTH);
  
  [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
    
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
    _showContentView.frame = CGRectMake(0, SCREEN_HEIGHT - CGRectGetHeight(_frame), SCREEN_WIDTH,CGRectGetHeight(_frame));
    
  } completion:nil];
  
}

- (void)dismiss{
  
  [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
    
    self.backgroundColor = [UIColor clearColor];
    _showContentView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH,CGRectGetHeight(_frame));
      
  } completion:^(BOOL finished) {
    
    [self removeFromSuperview];

  }];
  
}

- (void)clickFunc:(UIButton *)btn{
  
  if ([self.delegate respondsToSelector:@selector(xs_actionSheet:clickedButtonIndex:)]) {
    
    if ([self.class isInstallation:(btn.tag - 200)]) {
      [self.delegate xs_actionSheet:self clickedButtonIndex:btn.tag - 200];
    }else{
      [self.delegate xs_actionSheet:self clickedButtonIndex:403];
    }
    
  }
  
}

+ (BOOL)isInstallation:(NSInteger)btnIndex{
  
  if (btnIndex == 0 || btnIndex == 1) {
    
    if (![QQApiInterface isQQInstalled]) {
      return NO;
    }
    
  }else if (btnIndex == 4){
    
    return YES;
    
  }else{
    
    if (![WXApi isWXAppInstalled]) {
      return NO;
    }
    
  }
  return YES;
  
}

@end
