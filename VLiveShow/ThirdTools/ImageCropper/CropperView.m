//
//  CropperView.m
//  VIBABPCUT
//
//  Created by lllll-xy on 16/6/21.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "CropperView.h"

@interface CropperView ()<UIScrollViewDelegate>

@property (strong,nonatomic)OverlayView * overlayView;

/* 裁剪框的frame */
@property (nonatomic, assign)CGRect circularFrame;

/* 当前图片Frame */
@property (nonatomic, assign)CGRect currentFrame;

/* Image view */
@property (nonatomic, strong) UIImageView *imageView;

@end

static CGSize minSize = {40, 40};

@implementation CropperView

#pragma mark - init

- (instancetype)initWithFrame:(CGRect)frame
                        image:(UIImage *)image andCropperViewControllerStyle:(CropperStyle)style{
  
  self = [super initWithFrame:frame];
  
  if (self) {
    
    self.backgroundColor = [UIColor blackColor];
    self.image = image;

    self.imageView = [[UIImageView alloc] initWithImage:image];

    CGRect rect;
    rect.size = [self getImageSizeForPreview:image];
    rect.origin = CGPointMake((frame.size.width - rect.size.width) / 2.0, (frame.size.height - rect.size.height) / 2.0);
    
    self.imageView.frame = rect;
    
    [self addSubview:_imageView];

    self.overlayView = [[OverlayView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.overlayView.style = style;
    
    float circularHeight;
    
    if (style == SquareStyle) {
      circularHeight = SCREEN_WIDTH;
    }else
      circularHeight = PROPORTIONHEIGHT;
    
    self.circularFrame = CGRectMake(_overlayView.topLeftPoint.x, _overlayView.topLeftPoint.y, SCREEN_WIDTH, circularHeight);
    
    [self addSubview:_overlayView];
    [self addAllGesture];
    
  }
  
  return self;
  
}

#pragma mark - Cropper

- (UIImage *)clipImage{
  
  CGRect squareFrame = self.circularFrame;
  CGFloat scaleRatio = self.imageView.frame.size.width / self.image.size.width;
  CGFloat x = (squareFrame.origin.x - self.imageView.frame.origin.x) / scaleRatio;
  CGFloat y = (squareFrame.origin.y - self.imageView.frame.origin.y) / scaleRatio;
  CGFloat w = squareFrame.size.width / scaleRatio;
  CGFloat h = squareFrame.size.height / scaleRatio;
  CGRect myImageRect = CGRectMake(x, y, w, h);
  UIImage *image = [self.image copy];
  image = [self croppIngimageByImageName:image toRect:myImageRect];
  return image;
  
}

- (UIImage *)croppIngimageByImageName:(UIImage *)imageToCrop toRect:(CGRect)rect
{
  
  CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
  UIImage *cropped = [UIImage imageWithCGImage:imageRef];
  CGImageRelease(imageRef);
  return cropped;
  
}

#pragma mark - GestureRecognizer

- (void)addAllGesture
{
  
  //捏合手势
  UIPinchGestureRecognizer * pinGesture = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinGesture:)];
  [self addGestureRecognizer:pinGesture];
  //拖动手势
  UIPanGestureRecognizer * panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanGesture:)];
  [self addGestureRecognizer:panGesture];
  
}

#pragma mark - GestureRecognizerAction

- (void)handlePinGesture:(UIPinchGestureRecognizer *)pinGesture
{
  
  UIView *view = self.imageView;
  
  CGPoint point = [pinGesture locationInView:self];
  
  if(pinGesture.state == UIGestureRecognizerStateBegan || pinGesture.state == UIGestureRecognizerStateChanged){

    CGFloat vectorX,vectorY;
    vectorX = (point.x - view.center.x) * pinGesture.scale;
    vectorY = (point.y - view.center.y) * pinGesture.scale;
    view.transform = CGAffineTransformScale(view.transform, pinGesture.scale, pinGesture.scale);
    
    [view setCenter:(CGPoint){(point.x - vectorX) , (point.y - vectorY)}];
    pinGesture.scale = 1;
    
  }else if(pinGesture.state == UIGestureRecognizerStateEnded){
    
    [self handleScaleOverflowWithPoint:point];
    CGPoint newCenter = [self handleBorderOverflow];
    self.imageView.center = newCenter;
    
  }
  
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)panGesture
{
  
  UIView * view = _imageView;
  
  if(panGesture.state == UIGestureRecognizerStateBegan || panGesture.state == UIGestureRecognizerStateChanged)
  {
    
    CGPoint translation = [panGesture translationInView:view.superview];
    [view setCenter:CGPointMake(view.center.x + translation.x, view.center.y + translation.y)];
    
    [panGesture setTranslation:CGPointZero inView:view.superview];
    
  }
  
  else if ( panGesture.state == UIGestureRecognizerStateEnded)
  {
    
    CGRect currentFrame = view.frame;
    //向右滑动 并且超出裁剪范围后
    if(currentFrame.origin.x >= self.circularFrame.origin.x)
    {
      currentFrame.origin.x =self.circularFrame.origin.x;
    }
    //向下滑动 并且超出裁剪范围后
    if(currentFrame.origin.y >= self.circularFrame.origin.y)
    {
      currentFrame.origin.y = self.circularFrame.origin.y;
    }
    //向左滑动 并且超出裁剪范围后
    if(currentFrame.size.width + currentFrame.origin.x < self.circularFrame.origin.x + self.circularFrame.size.width)
    {
      
      CGFloat movedLeftX =fabs(currentFrame.size.width + currentFrame.origin.x -(self.circularFrame.origin.x + self.circularFrame.size.width));
      currentFrame.origin.x += movedLeftX;
      
    }
    //向上滑动 并且超出裁剪范围后
    if(currentFrame.size.height+currentFrame.origin.y < self.circularFrame.origin.y + self.circularFrame.size.height)
    {
      
      CGFloat moveUpY =fabs(currentFrame.size.height + currentFrame.origin.y -(self.circularFrame.origin.y + self.circularFrame.size.height));
      currentFrame.origin.y += moveUpY;
      
    }
    
    [UIView animateWithDuration:0.2 animations:^{
      [view setFrame:currentFrame];
    }];
    
  }
  
}

/**
 * 处理图片移出裁剪框的情况
 */
- (CGPoint)handleBorderOverflow{
  
  CGPoint rightTop, leftBottom, newCenter;
  newCenter = self.imageView.center;
  CGRect clippingRect = self.circularFrame;
  UIView *view = self.imageView;
  rightTop.x = clippingRect.origin.x + clippingRect.size.width - view.frame.size.width/2;
  rightTop.y = clippingRect.origin.y + clippingRect.size.height - view.frame.size.height/2;
  leftBottom.x = clippingRect.origin.x + view.frame.size.width/2;
  leftBottom.y = clippingRect.origin.y + view.frame.size.height/2;
  
  //图片中心点超出x方向的最大值和最小值
  //
  if(view.center.x < rightTop.x){
    newCenter.x = rightTop.x;
  }else if(view.center.x > leftBottom.x){
    newCenter.x = leftBottom.x;
  }
  
  //图片中心点超出y方向的最大值和最小值
  //
  if(view.center.y < rightTop.y){
    newCenter.y = rightTop.y;
  }else if(view.center.y > leftBottom.y){
    newCenter.y = leftBottom.y;
  }
  
  return newCenter;
  
}

/**
 * 处理图片放缩小于裁剪框的尺寸
 */
- (CGRect)handleScaleOverflowWithPoint:(CGPoint)point{
  
  CGRect clippingRect = self.circularFrame;
  UIView *view = self.imageView;
  CGRect frame = view.frame;
  
  if(view.frame.size.width <= view.frame.size.height && frame.size.width < clippingRect.size.width){
    
    float scale = clippingRect.size.width / frame.size.width;
    
    CGFloat vectorX,vectorY;
    
    vectorX = (point.x - view.center.x)*scale;
    vectorY = (point.y - view.center.y)*scale;
    view.transform = CGAffineTransformScale(view.transform, scale, scale);
    
    [view setCenter:(CGPoint){(point.x - vectorX) , (point.y - vectorY)}];
    
  }
  
  if(view.frame.size.width > view.frame.size.height && frame.size.height < clippingRect.size.height){
    
    float scale = clippingRect.size.height / frame.size.height;
    CGFloat vectorX,vectorY;
    
    vectorX = (point.x - view.center.x)*scale;
    vectorY = (point.y - view.center.y)*scale;
    view.transform = CGAffineTransformScale(view.transform, scale, scale);
    
    [view setCenter:(CGPoint){(point.x - vectorX) , (point.y - vectorY)}];
   
  }
  
  return view.frame;
  
}

#pragma mark - tools

- (CGSize)getImageSizeForPreview:(UIImage *)image {
  
  CGFloat maxWidth = self.frame.size.width, maxHeight = self.frame.size.height;
  
  CGSize size = image.size;
  
  if (size.width > maxWidth) {
    size.height *= (maxWidth / size.width);
    size.width = maxWidth;
  }
  
  if (size.height > maxHeight) {
    size.width *= (maxHeight / size.height);
    size.height = maxHeight;
  }
  
  if (size.width < minSize.width) {
    size.height *= (minSize.width / size.width);
    size.width = minSize.width;
  }
  
  if (size.height < minSize.height) {
    size.width *= (minSize.height / size.height);
    size.height = minSize.height;
  }
  
  return size;
  
}

@end
