//
//  OverlayView.m
//  VIBABPCUT
//
//  Created by lllll-xy on 16/6/21.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "OverlayView.h"

@implementation OverlayView

- (id)initWithFrame:(CGRect)frame {
  
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor clearColor];
  }
  return self;
  
}

#pragma mark - 绘制

-(void)drawRect:(CGRect)rect{
  
  CGContextRef contextRef = UIGraphicsGetCurrentContext();
  CGContextSetShouldAntialias(contextRef, YES);
  
  CGContextSetFillColorWithColor(contextRef, [UIColor colorWithWhite:0 alpha:0.5].CGColor);
  CGContextAddRect(contextRef, CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height));
  CGContextFillPath(contextRef);
  [self createClearRectWithContextRef:contextRef];
  
}

#pragma mrak - 透明区域

- (void)createClearRectWithContextRef:(CGContextRef)contextRef{
  
  CGContextSaveGState(contextRef);

  CGContextMoveToPoint(contextRef, self.topLeftPoint.x, self.topLeftPoint.y);
  CGContextAddLineToPoint(contextRef, self.topRightPoint.x, self.topRightPoint.y);
  CGContextAddLineToPoint(contextRef, self.bottomRightPoint.x, self.bottomRightPoint.y);
  CGContextAddLineToPoint(contextRef, self.bottomLeftPoint.x, self.bottomLeftPoint.y);
  CGContextAddLineToPoint(contextRef, self.topLeftPoint.x, self.topLeftPoint.y);

  CGContextClip(contextRef);

  CGContextClearRect(contextRef, CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT));

  CGContextRestoreGState(contextRef);
  
  [self createGridWithContextRef:contextRef];
  
  CGContextStrokePath(contextRef);
  
}

#pragma - 边框

- (void)createGridWithContextRef:(CGContextRef)contextRef {
  
  CGContextSetStrokeColorWithColor(contextRef, [UIColor whiteColor].CGColor);
  CGContextSetLineWidth(contextRef,1.5f);
  CGContextSetShouldAntialias(contextRef, NO);
  
  CGPoint from, to;
  
  for (int i = 0 ; i < 4; i ++) {
    
    switch (i) {
      case 0:
        from = self.topLeftPoint;
        to = self.topRightPoint;
        break;
      case 1:
        to = self.bottomLeftPoint;
        break;
      case 2:
        from = self.bottomRightPoint;
        break;
      case 3:
        to = self.topRightPoint;
        break;
      default:
        break;
    }
    
    CGContextMoveToPoint(contextRef, from.x, from.y);
    CGContextAddLineToPoint(contextRef, to.x, to.y);
    
  }
  
  CGContextDrawPath(contextRef, kCGPathStroke);
  CGContextGetInterpolationQuality(contextRef);
  
}

- (CGPoint)topLeftPoint{
  
  if (_style != SquareStyle) {
    return CGPointMake(0, SCREEN_HEIGHT / 2 - PROPORTIONHEIGHT / 2);
  }
  return CGPointMake(0, SCREEN_HEIGHT / 2 - SCREEN_WIDTH / 2);
  
}

- (CGPoint)topRightPoint{
  
  if (_style != SquareStyle) {
    return CGPointMake(SCREEN_WIDTH, SCREEN_HEIGHT / 2 - PROPORTIONHEIGHT / 2);
  }
  return CGPointMake(SCREEN_WIDTH, SCREEN_HEIGHT / 2 - SCREEN_WIDTH / 2);
  
}

- (CGPoint)bottomLeftPoint{
  
  if (_style != SquareStyle) {
    return CGPointMake(0, SCREEN_HEIGHT / 2 + PROPORTIONHEIGHT / 2);
  }
  return CGPointMake(0, SCREEN_HEIGHT / 2 + SCREEN_WIDTH / 2);
  
}

- (CGPoint)bottomRightPoint{
  
  if (_style != SquareStyle) {
    return CGPointMake(SCREEN_WIDTH, SCREEN_HEIGHT / 2 + PROPORTIONHEIGHT / 2);
  }
  return CGPointMake(SCREEN_WIDTH, SCREEN_HEIGHT / 2 + SCREEN_WIDTH / 2);
  
}

@end
