//
//  OverlayView.h
//  VIBABPCUT
//
//  Created by lllll-xy on 16/6/21.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>

#define PROPORTIONHEIGHT  [[UIScreen mainScreen] bounds].size.width * 9 / 16

typedef NS_ENUM(NSInteger,CropperStyle){
  
  SquareStyle = 0, /* 正方形 */
  ProportionStyle, /* 16:9 */
  
};

@interface OverlayView : UIView

#pragma mark - Properties

/*!
 @brief 裁剪框左上角的点
 */
@property (readonly) CGPoint topLeftPoint;

/*!
 @brief 裁剪框右上角的点
 */
@property (readonly) CGPoint topRightPoint;

/*!
 @brief 裁剪框左下角的点
 */
@property (readonly) CGPoint bottomLeftPoint;

/*!
 @brief 裁剪框右下角的点
 */
@property (readonly) CGPoint bottomRightPoint;

@property (nonatomic,assign)CropperStyle style;

@end
