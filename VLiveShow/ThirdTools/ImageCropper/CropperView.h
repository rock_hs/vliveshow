//
//  CropperView.h
//  VIBABPCUT
//
//  Created by lllll-xy on 16/6/21.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OverlayView.h"

@interface CropperView : UIView

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong)UIImageView *result;

// 初始化
- (instancetype)initWithFrame:(CGRect)frame
                        image:(UIImage *)image andCropperViewControllerStyle:(CropperStyle)style;

- (UIImage *)clipImage;

@end
