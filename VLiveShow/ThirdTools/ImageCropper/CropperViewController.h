//
//  CropperViewController.h
//  VIBABPCUT
//
//  Created by lllll-xy on 16/6/21.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CropperView.h"

@protocol CropperViewControllerDelegate <NSObject>

/*
 *  @pragma 返回裁剪完成的图片
 */
- (void)AfterCuttingThePictures:(UIImage *)image;

@end

@interface CropperViewController : UIViewController

/*
 *  @pragma 初始化视图image 和裁剪方式
 *  image - 图片
 *  style - 裁剪方式
 */
- (id)initWithImage:(UIImage *)image andStyle:(CropperStyle)style;

@property (weak,nonatomic)id<CropperViewControllerDelegate> delegate;

@end
