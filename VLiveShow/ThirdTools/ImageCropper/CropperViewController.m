//
//  CropperViewController.m
//  VIBABPCUT
//
//  Created by lllll-xy on 16/6/21.
//  Copyright © 2016年 lllll-xy. All rights reserved.
//

#import "CropperViewController.h"

@interface CropperViewController ()

/*
 *  style - 裁剪方式
 */
@property (assign,nonatomic)CropperStyle style;

/*
 *  imageCropperView - 裁剪视图
 */
@property (nonatomic, strong) CropperView *imageCropperView;

@end

@implementation CropperViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
}



- (id)initWithImage:(UIImage *)image andStyle:(CropperStyle)style{

  self = [super init];
  
  if (self) {
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.style = style;
    
    self.imageCropperView = [[CropperView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) image:image andCropperViewControllerStyle:style];
    [self.view addSubview:_imageCropperView];
    
    [self CreatBtn];

  }
  
  return self;
  
}

#pragma mark - UI

- (void)CreatBtn{
  
  UIButton * cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
  [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
  cancelBtn.frame = CGRectMake(20, SCREEN_HEIGHT - 55, 40, 20);
  [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [cancelBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:cancelBtn];
  
  UIButton * clipBtn = [UIButton buttonWithType:UIButtonTypeSystem];
  [clipBtn setTitle:@"选取" forState:UIControlStateNormal];
  clipBtn.frame = CGRectMake(SCREEN_WIDTH - 60, SCREEN_HEIGHT - 55, 40, 20);
  [clipBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [clipBtn addTarget:self action:@selector(clip:) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:clipBtn];
  
}

#pragma mark - BtnAction

- (void)cancel:(UIButton *)btn{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)clip:(UIButton *)btn{
  
  if ([self.delegate respondsToSelector:@selector(AfterCuttingThePictures:)]) {
    [self.delegate AfterCuttingThePictures:[self.imageCropperView clipImage]];
  }
  [self dismissViewControllerAnimated:YES completion:nil];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
