//
//  AdvertisementViewController.h
//  VLiveShow
//
//  Created by tom.zhu on 10/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvertisementModel.h"

@interface AdvertisementViewController : UIViewController
/*
 * 广告页消失
 */
@property (nonatomic, copy) void(^skipBlock)();

+ (BOOL)storeSplashData:(AdvertisementModel*)model;
+ (AdvertisementModel*)getSplashData;
@end
