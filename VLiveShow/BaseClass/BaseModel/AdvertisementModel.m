//
//  AdvertisementModel.m
//  VLiveShow
//
//  Created by tom.zhu on 11/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

#import "AdvertisementModel.h"
#import <objc/runtime.h>
#import <SDWebImage/SDWebImageDownloader.h>

@interface CoopenImage () <NSCoding>
@property (nonatomic, strong, readwrite) UIImage *image;
@end

@implementation CoopenImage
- (UIImage *)image {
//    NSString *path_image = [self getImagePath];
//    NSFileManager *fm = [NSFileManager defaultManager];
//    if (![fm fileExistsAtPath:path_image] ||
//        ![[NSBundle pathsForResourcesOfType:@"data" inDirectory:path_image] count]) {
//        //图片文件存在则不再下载
//        return nil;
//    }
//     
//    NSBundle *bundle = [NSBundle bundleWithPath:path_image];
////    UIImage *image = [UIImage imageWithContentsOfFile:[bundle pathForResource:@"" ofType:@"data"]];
//    NSData *data = [NSData dataWithContentsOfFile:[bundle pathForResource:@"" ofType:@"data"]];
//    UIImage *image = [[UIImage alloc] initWithData:data];
//    return image;
    NSString *imgName = [self getImageName];
    NSString *path_image = [[self getImagePath] stringByAppendingString:[NSString stringWithFormat:@"/%@.png", imgName]];
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:path_image];
    return image;
}

- (void)setId_:(NSString *)id_ {
    _id_ = id_;
    if (self.imgUrl) {
     [self saveImage];
    }
}

- (void)setImgUrl:(NSString *)imgUrl {
    _imgUrl = imgUrl;
    if (self.id_) {
        [self saveImage];
    }
}

- (instancetype)initWithDictionary:(NSDictionary*)dic {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

- (void)saveImage {
    NSString *path_image = [self getImagePath];
    NSFileManager *fm = [NSFileManager defaultManager];
    
//    if ([fm fileExistsAtPath:path_image] &&
//        [[NSBundle pathsForResourcesOfType:@"data" inDirectory:path_image] count]) {
//            //图片文件存在则不再下载
//            return;
//        }
    
    if (self.imgUrl) {
        NSURL *url = [NSURL URLWithString:self.imgUrl];
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:url options:SDWebImageDownloaderAllowInvalidSSLCertificates | SDWebImageDownloaderContinueInBackground progress:nil
         completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
             BOOL res = [fm createDirectoryAtPath:path_image withIntermediateDirectories:YES attributes:nil error:nil];
             if (res) {
//                 NSString *imgName = [self.imgUrl.lastPathComponent substringToIndex:(self.imgUrl.lastPathComponent.length - 4)];
//                 imgName = [imgName stringByAppendingString:@".data"];
//                 NSData *imageData = UIImagePNGRepresentation(image);
//                 [NSKeyedArchiver archiveRootObject:imageData toFile:[NSString stringWithFormat:@"%@/%@", path_image, imgName]];
                 
                 NSString *imgName = [self getImageName];
                 NSData *imageData = UIImagePNGRepresentation(image);
                 [fm createFileAtPath:[path_image stringByAppendingString:[NSString stringWithFormat:@"/%@.png", imgName]] contents:imageData attributes:nil];
             }
        }];
    }
}

- (NSString*)getImageName {
    NSString *imgName = [self.imgUrl.lastPathComponent substringToIndex:(self.imgUrl.lastPathComponent.length - 4)];
    return imgName;
//    return [imgName stringByAppendingString:@".data"];
}

- (NSString*)getImagePath {
    NSString *path_cache = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject;
    return  [path_cache stringByAppendingString:[NSString stringWithFormat:@"/%@", self.id_]];
}

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    unsigned int count;
    Ivar *vars = class_copyIvarList(self.class, &count);
    for (int i = 0 ; i < count; i++) {
        Ivar certainVar = vars[i];
        NSString *name = [NSString stringWithUTF8String:ivar_getName(certainVar)];
        [aCoder encodeObject:object_getIvar(self, certainVar) forKey:name];
    }
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        unsigned int count;
        Ivar *vars = class_copyIvarList(self.class, &count);
        for (int i = 0 ; i < count; i++) {
            Ivar certainVar = vars[i];
            NSString *instanceVariableName = [NSString stringWithUTF8String:ivar_getName(certainVar)];
            NSString *propertyName = [instanceVariableName substringFromIndex:1];
            //set
            NSString *upperCase = [propertyName stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[propertyName substringToIndex:1] uppercaseString]];
            NSString *selName = [[@"set" stringByAppendingString:upperCase] stringByAppendingString:@":"];
            SEL sel = NSSelectorFromString(selName);
            [self performSelector:sel withObject:[aDecoder decodeObjectForKey:instanceVariableName]];
        }
    }
    return self;
}

@end


@interface AdvertisementModel () <NSCoding>
@property (nonatomic, strong, readwrite) NSMutableArray<CoopenImage*> *coopenImages;
@end

@implementation AdvertisementModel
- (void)setId_:(NSString *)id_ {
    _id_ = id_;
    if (self.coopenImages) {
        [self.coopenImages enumerateObjectsUsingBlock:^(CoopenImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.id_= id_;
        }];
    }
}

- (void)setCoopenImages:(NSMutableArray<CoopenImage *> *)coopenImages {
    _coopenImages = coopenImages;
    if (self.id_) {
        [coopenImages enumerateObjectsUsingBlock:^(CoopenImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.id_ = self.id_;
        }];
    }
}

- (instancetype)initWithDictionary:(NSDictionary*)dic {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"coopenImage"]) {
        NSArray *arr = value;
        if (arr) {
            self.coopenImages = @[].mutableCopy;
            [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.coopenImages addObject:[[CoopenImage alloc] initWithDictionary:obj]];
            }];
            self.coopenImages = self.coopenImages;
        }
    }else if ([key isEqualToString:@"id"]) {
        self.id_ = value;
    }
}

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    unsigned int count;
    Ivar *vars = class_copyIvarList(self.class, &count);
    for (int i = 0 ; i < count; i++) {
        Ivar certainVar = vars[i];
        NSString *name = [NSString stringWithUTF8String:ivar_getName(certainVar)];
        [aCoder encodeObject:object_getIvar(self, certainVar) forKey:name];
    }
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        unsigned int count;
        Ivar *vars = class_copyIvarList(self.class, &count);
        for (int i = 0 ; i < count; i++) {
            Ivar certainVar = vars[i];
            NSString *instanceVariableName = [NSString stringWithUTF8String:ivar_getName(certainVar)];
            NSString *propertyName = [instanceVariableName substringFromIndex:1];
            //runtime
            //objc_setAssociatedObject(self, [propertyName UTF8String], [aDecoder decodeObjectForKey:instanceVariableName], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            
            //set
            NSString *upperCase = [propertyName stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[propertyName substringToIndex:1] uppercaseString]];
            NSString *selName = [[@"set" stringByAppendingString:upperCase] stringByAppendingString:@":"];
            SEL sel = NSSelectorFromString(selName);
            [self performSelector:sel withObject:[aDecoder decodeObjectForKey:instanceVariableName]];
        }
    }
    return self;
}

@end
