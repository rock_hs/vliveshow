//
//  AdvertisementModel.h
//  VLiveShow
//
//  Created by tom.zhu on 11/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CoopenImage : NSObject
@property (nonatomic, strong) NSString *imgUrl;
@property (nonatomic, strong) NSString *resolutionRatio;
@property (nonatomic, strong) NSString *id_;
@property (nonatomic, strong, readonly) UIImage *image;   //通过imgUrl下载保存在本地 保存在cache/imgUrl/
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end

@interface AdvertisementModel : NSObject
@property (nonatomic, readonly) NSMutableArray<CoopenImage*> *coopenImages;
@property (nonatomic, strong) NSNumber *createTime;
@property (nonatomic, strong) NSNumber *endValidTime;
@property (nonatomic, strong) NSString *id_;
@property (nonatomic, strong) NSNumber *isDeleted;
@property (nonatomic, strong) NSNumber *isDisabled;
@property (nonatomic, strong) NSString *startValidTime;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *updateTime;
- (instancetype)initWithDictionary:(NSDictionary*)dic;
@end
