//
//  VLSLoginBaseViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "VLSCountryView.h"
#import "VLSPhoneInputView.h"
#import "VLSVerCodeInputView.h"

@interface VLSLoginBaseViewController : VLSBaseViewController

@property (nonatomic, strong)UIButton *nextButton;
@property (nonatomic, strong)VLSCountryView *countryView;
@property (nonatomic, strong)VLSPhoneInputView *phoneInputView;
@property (nonatomic, strong)VLSVerCodeInputView *verCodeInputView;

- (void)nextBtnEnable:(BOOL)enable;
- (void)showWarningWithString:(NSString *)str;
- (void)hideWarning;
- (BOOL)clickNextBtnAndVerifyPhoneExists;
- (void)phoneNumberExisted;

@end
