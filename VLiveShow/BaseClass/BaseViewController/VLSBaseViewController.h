//
//  VLSBaseViewController.h
//  VLiveShow
//
//  Created by 李雷凯 on 16</5/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "ManagerEvent.h"
#import <UMSocialCore/UMSocialCore.h>
#import "VLSUserTrackingManager.h"
#import "VLSJoinRoomModel.h"
@class AppDelegate;
@class ManagerEvent;

static const int K_LANDSCAPEVIEWWIDTH = 290;

@interface VLSBaseViewController : UIViewController
@property (nonatomic) BOOL isLoading;
@property (nonatomic, strong)MBProgressHUD *HUD;// loading对象
///
@property (nonatomic, strong)AppDelegate *app;

- (void)leftItemAction:(id)sender;

//- (void)configUI;
// 显示loading
- (void)showProgressHUD;
// 隐藏loading
- (void)hideProgressHUDAfterDelay:(NSTimeInterval)delay;
// 显示带文字的loading
- (void)showProgressHUDWithStr:(NSString *)str dimBackground:(BOOL)dim;

- (void)toastHUDWithStr:(NSString *)str dimBackground:(BOOL)dim;

- (void)showProgressHUD: (NSString*)text;

- (void)showHUD:(NSString*)text;

- (void)success:(ManagerEvent*)event;
- (void)error:(ManagerEvent*)event;

- (void)ShowAlterView:(NSString *)str;
- (void)showWarnAlterview:(NSString *)str;

- (void)showAlterviewController:(NSString *)str;

- (void)getUserInfo;

- (void)shareToPlatform:(NSInteger)platformType messageObject:(NSDictionary*)dic completion:(void(^)(NSDictionary* item))complete;

- (void)shareTrackingLiveBefore:(NSString *)roomID Index:(NSInteger)index;
- (void)shareTrackingLiveAfter:(NSString *)roomID Index:(NSInteger)index;
- (void)shareTrackingLiveProcess:(NSString *)roomID Index:(NSInteger)index;




- (UIAlertController *)showNoTapBlockAlertControllerInVC:(UIViewController *)vc withTitle:(NSString *)title message:(NSString *)message onlyCancelButton:(NSString *)buttonTitle;

- (UIAlertController *)showAlertControllerInVC:(UIViewController *)vc withTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray *)buttonTitles tapBlock:(UIAlertControllerCompletionBlock)tapBlock;
- (void)setNavigationBarRedMethod;
// 等待子类重写的方法
- (void)anchorQuiteRoom;
- (void)audienceQuiteRoom;
- (void)loadRequestWebViewWithString:(NSString*)pathFile;

@end
