//
//  VLSNavigationController.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSNavigationController : UINavigationController
/**
 *  改变支持的旋转方向
 *
 *  @param interfaceOrientation
 */
- (void)changeSupportedInterfaceOrientations:(UIInterfaceOrientationMask)interfaceOrientationMask;
@end
