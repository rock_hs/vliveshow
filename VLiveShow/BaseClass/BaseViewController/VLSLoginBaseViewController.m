//
//  VLSLoginBaseViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/8/22.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLoginBaseViewController.h"
#import "SectionsViewController.h"
#import "VLSNavigationController.h"
#import "VLSCountryAreaManager.h"
#import "MBProgressHUD+Add.h"

@interface VLSLoginBaseViewController ()<VLSCountryViewDelegate, SecondViewControllerDelegate, VLSPhoneInputViewDelegate, VLSVerCodeInputViewDelegate>

@property (nonatomic, strong)UIImageView *warningImageView;
@property (nonatomic, strong)UILabel *warningLabel;

@end

@implementation VLSLoginBaseViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.phoneInputView.phoneTF.text.length) {
        
        NSString *temStr = self.phoneInputView.normalPhoneNum;
        NSString *firstChar = [temStr substringToIndex:1];
        if ([self.phoneInputView.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
            if ([firstChar isEqualToString:@"0"]) {
                if (temStr.length == 10) {
                    [self nextBtnEnable:YES];
                    [self phoneNumberExisted];
                } else {
                    [self nextBtnEnable:NO];
                }
            } else {
                if (temStr.length == 9) {
                    [self nextBtnEnable:YES];
                    [self phoneNumberExisted];
                } else {
                    [self nextBtnEnable:NO];
                }
            }
        } else if ([self.phoneInputView.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
            if (temStr.length == 11) {
                [self nextBtnEnable:YES];
                [self phoneNumberExisted];
            } else {
                [self nextBtnEnable:NO];
            }
        } else {
             [self nextBtnEnable:YES];
        }
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB16(COLOR_BG_FFFFFF);

    [self.view addSubview:self.nextButton];

}

- (void)nextAction:(UIButton *)button {
    
}

- (void)nextBtnEnable:(BOOL)enable {
    self.nextButton.userInteractionEnabled = enable;
    if (enable) {
        self.nextButton.backgroundColor = [UIColor redColor];
        self.nextButton.layer.borderWidth = 0.;
        [self.nextButton setTitleColor:RGB16(COLOR_FONT_FFFFFF) forState:UIControlStateNormal];
    } else {
        self.nextButton.layer.borderWidth = 1.;
        self.nextButton.layer.cornerRadius = 20. / 375. * SCREEN_WIDTH;
        self.nextButton.layer.masksToBounds = YES;
        self.nextButton.layer.borderColor = [UIColor grayColor].CGColor;
        self.nextButton.backgroundColor = [UIColor whiteColor];
        [self.nextButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    
}

#pragma mark - 显示警告
- (void)showWarningWithString:(NSString *)str {
    
    self.warningLabel.text = str;
    
    [self.view addSubview:self.warningLabel];
    [self.warningLabel setSingleLineAutoResizeWithMaxWidth:SCREEN_WIDTH];
    self.warningLabel.sd_layout
    .centerXEqualToView(self.view)
    .bottomSpaceToView(self.nextButton, 30)
    .heightIs(30);
    
    [self.view addSubview:self.warningImageView];
    self.warningImageView.sd_layout
    .rightSpaceToView(self.warningLabel, 5)
    .centerYEqualToView(self.warningLabel)
    .widthIs(15)
    .heightIs(15);
    
}

#pragma mark - 隐藏警告
- (void)hideWarning {
    [self.warningLabel removeFromSuperview];
    [self.warningImageView removeFromSuperview];
}

#pragma mark - VLSPhoneInputViewDelegate

- (void)inputPhoneNumberWithNextBtnEnable:(BOOL)enable {
    [self nextBtnEnable:enable];
}
- (void)hidePhoneInputWarning {
    [self hideWarning];
}
- (void)phoneNumberExisted {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"MINE_PHONE_HAS_REGEIST") preferredStyle:UIAlertControllerStyleAlert];
    // 确定按钮
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"BIND_INPUT_AGAIN") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self nextBtnEnable:NO];
        self.phoneInputView.phoneTF.text = nil;
        [self.phoneInputView.phoneTF endEditing:YES];
        [self.verCodeInputView.verCodeTF endEditing:YES];
        
        
    }];
    
    // 取消按钮
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"LIVEBEFORE_BACK") style:UIAlertActionStyleCancel handler:nil];
    [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    [okAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (BOOL)clickNextBtnAndVerifyPhoneExists {
    __block BOOL flag;
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneInputView.normalPhoneNum;
    model.nationCode = [self.phoneInputView.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        flag = [[result objectForKey:@"existPhone"] boolValue];
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            
        }
    };
    
    [AccountManager phoneExists:model callback:callback];
    return flag;
}

#pragma mark - VLSVerCodeInputViewDelegate
- (void)inputVerCode:(NSString *)verCodeStr {
   
}

-(void)hideVerCodeInputWarning {
    [self hideWarning];
}
-(void)sendVerCode:(UIButton *)button {
    
    if (self.phoneInputView.normalPhoneNum.length != 11 && [self.phoneInputView.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
        
        [self.phoneInputView inputWrongPhoneNumber];
        [self showWarningWithString:LocalizedString(@"ALERT_MESSAGE_RIGHT_PHONENUM")];
        
        return;
    }
    
    // 验证码倒计时
    
    [self verCodeTime:button];
    
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneInputView.normalPhoneNum;
    model.nationCode = [self.phoneInputView.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [MBProgressHUD showSuccess:LocalizedString(@"LOGIN_ALERT_VERCODE_SEND") toView:self.view];
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager sendVerCode:model callback:callback];

}

- (void)verCodeTime:(id)sender {
//    self.isSend = YES;
    __block int timeout=VERCODE_TIME; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){
            //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示
                [self.verCodeInputView verCodeBtnStateWithTimeFinished];
            });
        }else{
            int seconds = timeout % VERCODE_TIME;
            NSString *strTime = [NSString stringWithFormat:@"%d", seconds];
            if ([strTime isEqualToString:@"0"]) {
                strTime = [NSString stringWithFormat:@"%d",VERCODE_TIME];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1];
                [self.verCodeInputView verCodeBtnStateWithTimeRuning:strTime];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}


#pragma mark - VLSCountryViewDelegate

- (void)selectCountryArea {
    SectionsViewController *stVC = [SectionsViewController new];
    stVC.delegate = self;
    [self.navigationController pushViewController:stVC animated:YES];
}

#pragma mark - SecondViewControllerDelegate

- (void)secondData:(NSString *)data withCountryName:(NSString *)name{
    
    self.phoneInputView.countryLabel.text = [NSString stringWithFormat:@"+%@", data];
    self.countryView.countryLabel.text = name;
    [VLSCountryAreaManager shareManager].area = self.phoneInputView.countryLabel.text;
    [VLSCountryAreaManager shareManager].name = name;
    
    [self selectCountryAreaAndFormatPhoneNum];
}

- (void)selectCountryAreaAndFormatPhoneNum {
    
    if ([[self.phoneInputView.countryLabel.text substringFromIndex:1] isEqualToString:@"86"]) {
        
        if (self.phoneInputView.phoneTF.text.length > 3 && self.phoneInputView.phoneTF.text.length < 8) {
            
            self.phoneInputView.phoneTF.text = [NSString stringWithFormat:@"%@ %@", [self.phoneInputView.phoneTF.text substringToIndex:3], [self.phoneInputView.phoneTF.text substringFromIndex:3]];
            
        } else if (self.phoneInputView.phoneTF.text.length > 8) {
            
            self.phoneInputView.phoneTF.text = [NSString stringWithFormat:@"%@ %@ %@", [self.phoneInputView.phoneTF.text substringToIndex:3], [self.phoneInputView.phoneTF.text substringWithRange:NSMakeRange(3, 4)], [self.phoneInputView.phoneTF.text substringFromIndex:7]];
        }
        
    } else {
        
        self.phoneInputView.phoneTF.text = [self.phoneInputView.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
    }
    
    self.phoneInputView.normalPhoneNum = [self.phoneInputView.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.phoneInputView.phoneTF endEditing:YES];
    [self.verCodeInputView.verCodeTF endEditing:YES];
}


#pragma mark - getters

- (UIButton *)nextButton {
    if (!_nextButton) {
        self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.nextButton setTitle:LocalizedString(@"LOGIN_BUTTON_NEXT") forState:UIControlStateNormal];
        self.nextButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
        [self nextBtnEnable:NO];
        [self.nextButton addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _nextButton;
}

- (VLSCountryView *)countryView {
    if (!_countryView) {
        self.countryView = [[VLSCountryView alloc] init];
        self.countryView.delegate = self;
    }
    return _countryView;
}

- (VLSPhoneInputView *)phoneInputView {
    if (!_phoneInputView) {
        self.phoneInputView = [[VLSPhoneInputView alloc] init];
        self.phoneInputView.delegate = self;
    }
    return _phoneInputView;
}

- (VLSVerCodeInputView *)verCodeInputView {
    if (!_verCodeInputView) {
        self.verCodeInputView = [[VLSVerCodeInputView alloc] init];
        self.verCodeInputView.delegate = self;
    }
    return _verCodeInputView;
}

- (UIImageView *)warningImageView {
    if (!_warningImageView) {
        
        self.warningImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"livebefore_warn"]];
        
    }
    return _warningImageView;
}

- (UILabel *)warningLabel {
    if (!_warningLabel) {
        self.warningLabel = [[UILabel alloc] init];
        self.warningLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.warningLabel.textColor = [UIColor redColor];
        self.warningLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _warningLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
