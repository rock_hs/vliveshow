//
//  VLSLiveBaseViewController.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "VLSAgoraVideoService.h"
#import "VLSLiveHttpManager.h"
#import "VLSAVMultiManager.h"
#import "VLSLiveInfoViewModel.h"
#import "VLSReConnectionManager.h"
//#import "VLSLiveListModel.h"

@interface VLSMultiLiveBaseViewController : VLSBaseViewController<VLSAgoraVideoServiceDelegate>
{
}
//加载过来的数据
@property (nonatomic, strong) VLSLiveInfoViewModel *liveInfo;   // 房间信息
@property (nonatomic, strong) NSMutableArray *menberList;   // 观众列表
@property (nonatomic, strong) VLSUserProfile *anchorProfile;// 主播profile
//@property (nonatomic, strong) VLSUserProfile *anchorInfo;   // 用户信息
//@property (nonatomic, strong) VLSLiveListModel *liveListModel;

@property (nonatomic, strong) NSString *courseId;
@property (nonatomic, strong) NSString *lessonId;

@property (nonatomic, assign) BOOL isReconnection;

// 后台推送的在线人数或者观众列表数量
@property (nonatomic, strong) NSString *tureWatch;

// 主播开启直播间获取到的票数
@property (nonatomic, strong) NSString *anchorTickets;

// 记录的票数
@property (nonatomic, strong) NSString *tureTickets;

// 向后台推送的cNum人数
@property (nonatomic, assign) NSInteger cNum;

// 是否已经请求观众列表成功
@property (nonatomic, assign) BOOL isAudiences;

//当前用户是否是观众
@property (nonatomic, assign) BOOL isSelfAttendance;

//check用
@property (nonatomic, assign) BOOL selfIsAnchor;

//+++++++++++++++++++必传参数
@property (nonatomic, strong) NSString *roomId;
@property (nonatomic, strong) NSString *anchorId;
//1竖2横 nil竖
//@property (nonatomic, strong) NSNumber *orientation;
@property (nonatomic, assign) KLiveRoomOrientation orientation;
//课程直播间 用户类型
@property(nonatomic, assign) ActionType actionType;
// 直播前先开始检查网络
// 无网不进
// wifi：直接进
// 移动网：提示，用户选择性进行
- (void)checkNetWorkBeforeLive;

// 添加网络监听,只在直播的过程中监听，创建房间过程中不监听
// 进入直播成功后监听
//- (void)addNetwokChangeListner;

//加入房间
- (void)joinRoom;
//加入成功
- (void)joinRoomSuccess;


//主播退出
- (void)anchorQuiteRoom;

- (void)audienceQuiteRoom: (BOOL)needQuit completeBlock: (void (^)(BOOL)) completeBlock;
//...
- (void)audienceQuiteRoom;
//...
- (void)guestQuiteRoom;
- (void)guestEndLink;
//嘉宾取消连线
- (void)guestCancelLink;
//发送布局
- (void)sendLayoutToBack;
//嘉宾取消连线请求（未成为嘉宾)
- (void)guestCancelLinkRequest;

- (void)reloadLiveWidthRoomId:(NSString *)roomId anchorId:(NSString *)anchorId;
// 刷新观众人数
- (void)reloadMemberNumber;
//刷新观众列表
- (void)reloadMemberList;
//刷新主播
- (void)reloadAnchorInfo;
//刷新room信息
- (void)reloadRoomInfo;
//没有关注主播
- (void)notAttendAnchor;

- (void)reloadVideoView;

//触摸可以用来点击屏幕时候隐藏某些view等
- (void)touchOnLiveBaseView:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
//嘉宾取消连线成功（更新view等操作）
- (void)guestCancelLinkSuccess;
//嘉宾取消连线请求（未成为嘉宾）成功（更新view等操作）
- (void)guestCancelLinkRequestSuccess;
//主播暂时离开
- (void)anchorOffLine;
//主播回来了
- (void)anchorIsBack;
//嘉宾暂时离开
- (void)guestOffLineUid:(NSString *)uid;
//嘉宾回来了
- (void)guestIsBackUid:(NSString *)uid;

// 网络类型有变化
- (void)onNetworkChanged:(NSNotification*)notifiy;

// 网络断开与重联
- (void)onNetworkConnected;

//alert
- (void)showSendBlackAlert;
//嘉宾退出弹框
- (void)showGuestQuitRoomAlertTapBlock:(UIAlertControllerCompletionBlock)tapBlock;
//主播退出弹框
- (void)showAnchorQuiteRoomAlertTapBlock:(UIAlertControllerCompletionBlock)tapBlock;

- (void)loadRoomInfo;

@end
