//
//  VLSAgoraVideoService.m
//  AgoraDemo
//
//  Created by xax on 16/5/12.
//  Copyright © 2016年 apple. All rights reserved.
//

#import "VLSAgoraVideoService.h"
#import <videoprp/AgoraYuvEnhancerObjc.h>
#import <AgoraRtcEngineKit/AgoraRtcEngineKit.h>
#import "VLSHeartManager.h"

@interface VLSAgoraVideoService()<AgoraRtcEngineDelegate>

@property (nonatomic, strong) AgoraRtcEngineKit *agoraKit;
@property (nonatomic, strong) AgoraYuvEnhancerObjc *yuvEnhancer; //美颜相关的类；
@property (nonatomic, strong) NSMutableArray* array;

@end
@implementation VLSAgoraVideoService

+ (instancetype)sharedInstance
{
    static VLSAgoraVideoService *agoraServer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        agoraServer = [[VLSAgoraVideoService alloc]init];
        
    });
    return agoraServer;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //直播模式初始化
        //self.agoraKit = [AgoraRtcEngineKit sharedEngineWithVendorKey:VENDORKEY applicationCategory:AgoraRtc_ApplicationCategory_LiveBroadcasting delegate:self];
        self.agoraKit = [AgoraRtcEngineKit sharedEngineWithAppId: VENDORKEY delegate: self];
        [self.agoraKit setChannelProfile: AgoraRtc_ChannelProfile_LiveBroadcasting];
        [self.agoraKit enableDualStreamMode: YES];
        [self.agoraKit setVideoQualityParameters: NO];
        self.yuvEnhancer = [[AgoraYuvEnhancerObjc alloc] init];
//        NSInteger tag = [VLSHeartManager shareHeartManager].heartModel.liveVideoResolution;
        NSInteger tag = [[VLSHeartManager shareHeartManager].heartModel.broadcastProfile integerValue];
        if (tag) {
            [self.agoraKit setVideoProfile: tag swapWidthAndHeight: YES];
        }else{
            [self.agoraKit setVideoProfile:AgoraRtc_VideoProfile_360P_9 swapWidthAndHeight: YES];
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cachesDir = [paths objectAtIndex:0];
        int res = [self.agoraKit setLogFile:cachesDir];
        [self.agoraKit leaveChannel:^(AgoraRtcStats *stat) {}];
        self.videoSessions = [NSMutableArray arrayWithCapacity:0];
        
        NSMutableArray* array = [NSMutableArray arrayWithCapacity:0];
        self.array = array;
//        self.beautyLevel = 1;
    }
    return self;
}

- (void)setOrientation:(KLiveRoomOrientation)orientation {
    if (_orientation == orientation) {
        return;
    }
    _orientation = orientation;
    if (orientation == KLiveRoomOrientation_LandscapeRight || orientation == KLiveRoomOrientation_LandscapeLeft) {
        NSInteger tag = [[VLSHeartManager shareHeartManager].heartModel.broadcastProfile integerValue];
        if (tag) {
            [self.agoraKit setVideoProfile:tag swapWidthAndHeight: NO];
        }else{
            [self.agoraKit setVideoProfile:AgoraRtc_VideoProfile_360P_9 swapWidthAndHeight: NO];
        }
    }else if (orientation == KLiveRoomOrientation_Portrait || orientation == KLiveRoomOrientation_Unknown) {
        NSInteger tag = [[VLSHeartManager shareHeartManager].heartModel.broadcastProfile integerValue];
        if (tag) {
            [self.agoraKit setVideoProfile:tag swapWidthAndHeight: YES];
        }else{
            [self.agoraKit setVideoProfile:AgoraRtc_VideoProfile_360P_9 swapWidthAndHeight: YES];
        }
    }
}

- (void)setEnableBeauty:(BOOL)enableBeauty
{
    _enableBeauty = enableBeauty;
    if (_enableBeauty) {
        [self.yuvEnhancer turnOn];
    }else{
        [self.yuvEnhancer turnOff];
    }
}

- (void)setBeautyLevel:(NSInteger)beautyLevel
{
    _beautyLevel = beautyLevel;
    //亮度；
    self.yuvEnhancer.lighteningFactor = 1.0;
    //平滑度
    self.yuvEnhancer.smoothness = 15;
}

- (void)joinChannel
{
  
    __weak typeof(self) weakSelf = self;
  
    [self.agoraKit joinChannelByKey:_channelKey channelName:self.channel info:nil uid:self.uid joinSuccess:^(NSString *channel, NSUInteger uid, NSInteger elapsed) {
    
      /*
       * 非直播不开启录制服务
       */
      if (self.userType != AGUserTypeDefault)
      {
        [weakSelf.agoraKit startRecordingService:_RDK];
//          [weakSelf.agoraKit setEnableSpeakerphone:YES];
//          NSError* error = nil;
//          [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error: &error];
//          [[AVAudioSession sharedInstance] setActive:YES error:&error];

     }
     else
     {
//         [weakSelf.agoraKit setEnableSpeakerphone:YES];

//         NSError* error = nil;
//         [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error: &error];
//         [[AVAudioSession sharedInstance] setActive:YES error:&error];
     }
    }];
  
//    __weak typeof(self) weakSelf = self;
//    [self.agoraKit joinChannelByKey:nil channelName:self.channel info:nil uid:self.uid joinSuccess:^(NSString *channel, NSUInteger uid, NSInteger elapsed) {
//        [weakSelf.agoraKit setEnableSpeakerphone:YES];
//    }];
}

- (void)setUpVideo
{
    if ([[VLSHeartManager shareHeartManager].heartModel.enableH264Profile boolValue])
    {
        [self.agoraKit setParameters: @"{\"che.video.h264_profile\":66}"];
    }
    //[self.agoraKit setDefaultAudioRouteToSpeakerphone: YES];

    [self.agoraKit enableVideo];
    [self.agoraKit enableDualStreamMode: YES];
    [self.agoraKit setVideoQualityParameters: NO];
    [self enablePreRotate];
    /*
    if (self.userType == AGUserTypeDefault) {
        //设置观众角色
        [self.agoraKit setClientRole:AgoraRtc_ClientRole_Audience withKey: @""];
        [self setLocalVideoMute:YES];
    }else if(self.userType == AGUserTypeAnchor || self.userType == AGUserTypeAnchorBate){
        //设置直播角色
        [self.agoraKit setClientRole:AgoraRtc_ClientRole_Broadcaster withKey: @""];
        [self setLocalVideoMute:NO];
    }*/
    [self alternateUserType:self.userType];
}

- (void)alternateUserType:(AGUserType)type {
    self.userType = type;
    if (self.userType == AGUserTypeDefault) {
        //设置观众角色
        [self.agoraKit setClientRole:AgoraRtc_ClientRole_Audience withKey: @""];
        [self setLocalVideoMute:YES];
    }else if(self.userType == AGUserTypeAnchor || self.userType == AGUserTypeAnchorBate){
        //设置直播角色
        [self.agoraKit setClientRole:AgoraRtc_ClientRole_Broadcaster withKey: @""];
        [self setLocalVideoMute:NO];
    }
}

- (void)setupAudio
{
    [self.agoraKit setDefaultAudioRouteToSpeakerphone: YES];
}

- (void)enablePreRotate {
    if (self.agoraKit) {
        [self.agoraKit setParameters:@"{\"che.video.enablePreRotation\":true}"];
    }
}

- (VLSVideoItem *)createLocalVideoOnly
{
    self.userType = AGUserTypeAnchor;
    [self setUpVideo];
    [self setupAudio];
    VLSVideoSession *video = [[VLSVideoSession alloc] initSessionUid:0];
    video.videoView.model = nil;
    
    [self.agoraKit setupLocalVideo:video.canvas];
    [self.agoraKit startPreview];
    [self enablePreRotate];
    self.enableBeauty = YES;
    CGFloat light = [VLSHeartManager shareHeartManager].heartModel.liveVideoLighteningFactor;
    if (light) {
        self.yuvEnhancer.lighteningFactor = light;
    }else{
        self.yuvEnhancer.lighteningFactor = 1;
    }
    
    return video.videoView;
}

- (VLSVideoItem *)createLocalVideo
{
    VLSVideoSession *video = [self videoSessionOfUid:0];
    //SDK状态改为直播状态
    [self alternateUserType:AGUserTypeAnchor];
    return video.videoView;
}

- (VLSVideoItem *)createRemoteVideoOfUid:(NSInteger)uid
{
    VLSVideoSession *video = [self videoSessionOfUid:uid];
    if (![[VLSMessageManager sharedManager] checkUserIsSelf:[NSString stringWithFormat:@"%ld",(long)uid]]) {
        [self.agoraKit setupRemoteVideo:video.canvas];
    }
    return video.videoView;
}

- (VLSVideoSession *)videoSessionOfUid:(NSInteger) uid;
{
    if ([self fetchSessionOfUid:uid]) {
        return [self fetchSessionOfUid:uid];
    }else{
        VLSVideoSession * newSession = [[VLSVideoSession alloc] initSessionUid:uid];
        if (uid == 0) {
            
            //创建本地视频
            if ([[VLSMessageManager sharedManager] checkUserIsGuests:[VLSMessageManager sharedManager].host.userId]){
                //嘉宾的话需要重新设置一下直播角色，初始状态没有视频，立即上镜后显示
                [VLSAgoraVideoService sharedInstance].userType = AGUserTypeAnchor;
                [[VLSAgoraVideoService sharedInstance] setUpVideo];
                [[VLSAgoraVideoService sharedInstance] setupAudio];
//                [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:YES];
                [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:YES];
            }
            [self.agoraKit setupLocalVideo:newSession.canvas];
            [self.agoraKit startPreview];
            [self enablePreRotate];
            [[VLSAgoraVideoService sharedInstance] joinChannel];
            [self setEnableBeauty:YES];
        }
        [self.videoSessions addObject:newSession];
        return newSession;
    }
}

- (VLSVideoSession *)fetchSessionOfUid:(NSInteger)uid
{
    for (VLSVideoSession *session in self.videoSessions) {
        if (session.uid == uid) {
            return session;
        }
    }
    return nil;
}



#pragma mark - AgoraRtcEngineKit的delegate
//
- (void)rtcEngine:(AgoraRtcEngineKit *)engine firstRemoteVideoDecodedOfUid:(NSUInteger)uid size:(CGSize)size elapsed:(NSInteger)elapsed
{
    VLSVideoViewModel *model = [[VLSVideoViewModel alloc] init];
    model.userId = [NSString stringWithFormat:@"%lu", (unsigned long)uid];
    if (uid == [VLSMessageManager sharedManager].anchor.userId.intValue) {
        model.userType = UserTypeHost;
    } else {
        model.userType = UserTypeGuest;
    }
    model.muteVideo = NO;
    model.muteVoice = NO;
    model.showLoading = NO;
    model.muteVoiceBySelf = NO;
    [self.array addObject:model];
    VLSVideoSession *session = [self fetchSessionOfUid:uid];
    if (session == nil) {
        session = [self videoSessionOfUid:uid];
        if (![[VLSMessageManager sharedManager] checkUserIsSelf:[NSString stringWithFormat:@"%ld",(long)uid]]) {
            [self.agoraKit setupRemoteVideo:session.canvas];
        }
    }
    if (!session.videoView.loadingView.hidden) {
        session.videoView.loadingView.hidden = YES;
    }
    if (self.array.count > self.videoSessions.count) {//只有当im没收到时，才已agora流为准
        if ([_messagedelegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
            [_messagedelegate receiveUpdateLiveLayout:self.array];
        }
    }
    __weak typeof(self) weakSelf = self;
    if ([weakSelf.delegate respondsToSelector:@selector(agoraListenerFirstRemoteVideoDecodedOfUid:)]) {
        [weakSelf.delegate agoraListenerFirstRemoteVideoDecodedOfUid:uid];
    }
}
//开始直播
- (void)rtcEngine:(AgoraRtcEngineKit *)engine firstLocalVideoFrameWithSize:(CGSize)size elapsed:(NSInteger)elapsed
{
    //    self.videoMainView.frame = self.view.frame;
    NSLog(@"local video display");
    //    if let _ = videoSessions.first {
    //        updateInterfaceWithSessions(videoSessions, animation: false)
    //    }
    __weak typeof(self) weakSelf = self;
    if ([weakSelf.delegate respondsToSelector:@selector(agoraListenerFirstLocalVideoFrameWithSize:)]) {
        [weakSelf.delegate agoraListenerFirstLocalVideoFrameWithSize:size];
    }
}

//监听有用户加入，可以在此处获取到全部用户的uid
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didJoinedOfUid:(NSUInteger)uid elapsed:(NSInteger)elapsed
{
    __weak typeof(self) weakSelf = self;
    if ([weakSelf.delegate respondsToSelector:@selector(agoraListenerDidJoinedOfUid:)]) {
        [weakSelf.delegate agoraListenerDidJoinedOfUid:uid];
    }
}
//再次加入
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didJoinChannel:(NSString *)channel withUid:(NSUInteger)uid elapsed:(NSInteger)elapsed
{

}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didApiCallExecute:(NSString*)api error:(NSInteger)error{
  
  switch (error) {
    case 0:
      NSLog(@"成功成功成功");
      break;
    case 1:
      NSLog(@"请求失败");
      break;
    case 2:
      NSLog(@"参数无效");
    case 3:
      NSLog(@"未就绪");
    case 7:
      NSLog(@"录音服务器IP未初始化");
    case 10:
      NSLog(@"操作超时");
    default:
      break;
  }
  
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didRejoinChannel:(NSString *)channel withUid:(NSUInteger)uid elapsed:(NSInteger)elapsed
{

}

//监听有用户掉线，此方法内可以监听嘉宾离开 要移除
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOfflineOfUid:(NSUInteger)uid reason:(AgoraRtcUserOfflineReason)reason
{
    __weak typeof(self) weakSelf = self;
    
    if (reason == AgoraRtc_UserOffline_Dropped) {
        if ([[VLSMessageManager sharedManager] checkUserIsLiving:[NSString stringWithFormat:@"%lu",(unsigned long)uid]]) {//直播掉线通知
            if ([weakSelf.delegate respondsToSelector:@selector(agoraListenerDidOfflineOfUid:)]) {
                [weakSelf.delegate agoraListenerDidOfflineOfUid:uid];
            }
        }
    }
    if (reason == AgoraRtc_UserOffline_Quit || AgoraRtc_UserOffline_Dropped) {
        [self.array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            VLSVideoViewModel* model = obj;
            if (model.userId.intValue == uid && model.userType == UserTypeGuest) {
                [self.array removeObject:model];
                *stop = YES;
            }
        }];
    }
//    if (reason == AgoraRtc_UserOffline_Quit) { //只有当用户退出时，才更新updateLayout
//        if ([_messagedelegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
//            [_messagedelegate receiveUpdateLiveLayout:self.array];
//        }
//    }
}

//监听有用户停止/发生视频回调，此方法内可以监听嘉宾
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didVideoMuted:(BOOL)muted byUid:(NSUInteger)uid
{
#pragma mark - 在直播间内嘉宾二次连线加载中bug临时性修复，还有优化空间；。。。解决：当嘉宾被取消或被取消连线时，重新设定观众角色等操作，再次操作时可以避免
    if (!muted) {
        VLSVideoSession *session = [self fetchSessionOfUid:uid];
        if (!session.videoView.loadingView.hidden) {
            session.videoView.loadingView.hidden = YES;
        }
    }

    __weak typeof(self) weakSelf = self;
    if ([weakSelf.delegate respondsToSelector:@selector(agoraListenerDidVideoMuted:byUid:)]) {
        [weakSelf.delegate agoraListenerDidVideoMuted:muted byUid:uid];
    }
}

////用户音频静音回调
//- (void)rtcEngine:(AgoraRtcEngineKit *)engine didAudioMuted:(BOOL)muted byUid:(NSUInteger)uid {
//    
//    [self.array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        VLSVideoViewModel* model = obj;
//        if (model.userId.intValue == uid) {
//            model.muteVoice = muted;
//            [self.array replaceObjectAtIndex:idx withObject:model];
//            *stop = YES;
//        }
//    }];
//    if ([_messagedelegate respondsToSelector:@selector(receiveUpdateLiveLayout:)]) {
//        [_messagedelegate receiveUpdateLiveLayout:self.array];
//    }
//}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine lastmileQuality:(AgoraRtcQuality)quality {
    if ([self.delegate respondsToSelector:@selector(agoraListenerNetworkQuality:)]) {
        [self.delegate agoraListenerNetworkQuality:quality];
    }
}

- (void)rtcEngineConnectionDidLost:(AgoraRtcEngineKit *)engine
{
    
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine reportRtcStats:(AgoraRtcStats*)stats
{
    
}

//跟服务器断连
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOccurError:(AgoraRtcErrorCode)errorCode
{
  
    if (errorCode == AgoraRtc_Error_InvalidAppId) {
        
    }else if (errorCode == AgoraRtc_Error_ChannelKeyExpired){
      
      /* 动态key失效的时候需要更新重新设置*/
//      [self.agoraKit renewChannelDynamicKey:<#(NSString *)#>];
      
    }
  
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOccurWarning:(AgoraRtcWarningCode)warningCode
{
    NSLog(@"Agora warning: %ld", warningCode);
}
//
- (void)rtcEngine:(AgoraRtcEngineKit *)engine remoteVideoStats:(AgoraRtcRemoteVideoStats*)stats
{
//    NSLog(@"remoteVideoStats:%@",stats);
    
}

//
//
//- (void)showVideoOnView:(UIView *)view muteAVStream:(BOOL)mute uid:(NSInteger)uid
//{
//    [self.agoraKit enableVideo];
//    //后面如果有嘉宾模式可以改动这里参数为no
//    [self.agoraKit muteLocalVideoStream:mute];
//    [self.agoraKit muteLocalAudioStream:mute];
//    [self showVideoOnView:view uid:uid];
//}
//
//#pragma mark priviate 检测cancas是否已经被创建过
//- (AgoraRtcVideoCanvas *)getCanvasWidthUid:(NSUInteger)uid view:(UIView *)view
//{
//    for ( AgoraRtcVideoCanvas *canvas in self.videoCanvasList) {
//        if (uid ==canvas.uid) {
//            return canvas;
//        }
//    }
//    AgoraRtcVideoCanvas *canvas = [self createVideoCanvesUid:uid view:view];
//    [self.videoCanvasList addObject:canvas];
//    return canvas;
//
//}
//
//- (AgoraRtcVideoCanvas *)createVideoCanvesUid:(NSInteger)uid view:(UIView *)view
//{
//    AgoraRtcVideoCanvas *canvas= [[AgoraRtcVideoCanvas alloc] init];
//    canvas.renderMode = AgoraRtc_Render_Hidden;
//    canvas.uid = uid;
//    canvas.view = view;
//    return canvas;
//}
//
#pragma mark -- 视频控制
- (void)setFullScreenRemoteVideoTypeOfUid:(NSInteger)uid
{
    [self.agoraKit setRemoteVideoStream:uid type:AgoraRtc_VideoStream_High];

}

- (void)setSmallScreenRemoteVideoTypeOfUid:(NSInteger)uid
{
    NSInteger tag = [VLSHeartManager shareHeartManager].heartModel.subAgoraRtcVideoStream;
    if (tag) {
        [self.agoraKit setRemoteVideoStream:uid type:tag];

    }else{
        [self.agoraKit setRemoteVideoStream:uid type:2];
    }


}

- (void)leaveChannel
{
    [self leaveChannel: nil];
}

- (void)leaveChannel: (void(^)(BOOL))completeBlock
{
    [self.agoraKit leaveChannel:^(AgoraRtcStats *stat)
    {
        if (completeBlock)
        {
            completeBlock(YES);
        }
    }];
    [self.videoSessions removeAllObjects];
    [self.agoraKit setupLocalVideo:nil];
    [self.agoraKit stopPreview];
    [self.array removeAllObjects];
}

//停止录播,把key清空
- (void)stopRecording{
  
    [self.agoraKit stopRecordingService:_RDK];
    self.RDK = nil;
    self.channelKey = nil;
  
}

//开启静音(主播／嘉宾可能会用)
- (void)setLocalAudioMute:(BOOL)mute
{
    [self.agoraKit muteLocalAudioStream:mute];
}

//开启视频(主播／嘉宾可能会用)
- (void)setLocalVideoMute:(BOOL)mute
{
    [self.agoraKit muteLocalVideoStream:mute];
}

//开启远端音频
- (void)setRemoteAudioWithUid:(NSInteger)uid mute:(BOOL)mute
{
    [self.agoraKit muteRemoteAudioStream:uid mute:mute];
}

- (void)muteAllRemoteVideo:(BOOL)mute
{
    [self.agoraKit muteAllRemoteVideoStreams:mute];
}


//开启远端视频
- (void)setRemoteVideoWithUid:(NSInteger)uid mute:(BOOL)mute
{
    [self.agoraKit muteRemoteAudioStream:uid mute:mute];
}

//开放扬声器
- (void)setSpeakerYes:(BOOL)yes
{
    if (self.agoraKit.isSpeakerphoneEnabled) {
        [self.agoraKit setEnableSpeakerphone:yes];
    }
}

//切换摄像头
- (void)switchCamera
{
    [self.agoraKit switchCamera];
}
// 开始预览
- (void)startPreview
{
    [self.agoraKit startPreview];
    [self enablePreRotate];
}

// 暂停预览
- (void)stopPreview
{
    [self.agoraKit stopPreview];
}
// 启用网络检测
- (void)enableNetworkTest
{
    [self.agoraKit enableLastmileTest];
}
// 暂停网络检测
- (void)disableNetworkTest
{
    [self.agoraKit disableLastmileTest];
}

//推流音视频与否
- (void)setMuteLocalAVStream:(BOOL)mute
{
    [self.agoraKit muteLocalVideoStream:mute];
    [self.agoraKit muteLocalAudioStream:mute];
}

//推流音视频与否
- (void)endVLSVideoLivingEndBlock:(void(^)(AgoraRtcStats *stat))block
{
    [self.agoraKit leaveChannel:^(AgoraRtcStats *stat) {
        block(stat);
    }];
}

- (void)setRemoteRenderMode:(NSUInteger)uid mode:(AgoraRtcRenderMode)mode {
    [self.agoraKit setRemoteRenderMode:uid mode:mode];
}

@end
