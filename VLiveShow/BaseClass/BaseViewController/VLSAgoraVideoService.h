//
//  VLSAgoraVideoService.h
//  AgoraDemo
//
//  Created by xax on 16/5/12.
//  Copyright © 2016年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "VLSVideoSession.h"
#import "VLSMessageManager.h"

typedef enum {
    AGUserTypeDefault     = 0,
    AGUserTypeAnchor      = 1,
    AGUserTypeAnchorBate  = 2,
}AGUserType;

@protocol VLSAgoraVideoServiceDelegate <NSObject>

@optional
//取到第一个远程视频编码
- (void)agoraListenerFirstRemoteVideoDecodedOfUid:(NSUInteger)uid;
//第一帧本地视频size
- (void)agoraListenerFirstLocalVideoFrameWithSize:(CGSize)size;
//监听有用户加入,可以判断 uid是否在视频列表中，如果存在，调用createRemoteVideoOfUid
- (void)agoraListenerDidJoinedOfUid:(NSUInteger)uid;
//监听有用户掉线，此方法内可以监听嘉宾离开
- (void)agoraListenerDidOfflineOfUid:(NSUInteger)uid;
//监听有用户停止/发生视频回调，此方法内可以监听嘉宾
- (void)agoraListenerDidVideoMuted:(BOOL)muted byUid:(NSUInteger)uid;
//监听有用户回来了
- (void)agoraListenerDidBackOfUid:(NSUInteger)uid;
- (void)agoraListenerNetworkQuality:(NSUInteger)quality;
@end

@interface VLSAgoraVideoService : NSObject<VLSMessageManagerDelegate>

@property (nonatomic, weak) id<VLSAgoraVideoServiceDelegate> delegate;
@property (nonatomic, weak) id<VLSMessageManagerDelegate> messagedelegate;
@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, strong) NSString *channel;
@property (nonatomic, assign) AGUserType userType;
@property (nonatomic, strong) UIView *fatherView;
@property (nonatomic, assign) BOOL enableBeauty;
@property (nonatomic, assign) NSInteger beautyLevel;
@property (nonatomic, strong) NSMutableArray *videoSessions;

@property (nonatomic, assign) KLiveRoomOrientation orientation;

/*  必传参数
 *  @param PDK 录制的动态KEY
 *  @param channelKey 加入频道的动态KEY
 */
@property (nonatomic, strong) NSString * RDK;
@property (nonatomic, strong) NSString *channelKey;

+ (instancetype)sharedInstance;
//++++++++++++++美颜++++++++++++++



- (void)setUpVideo;
- (void)setupAudio;
- (VLSVideoItem *)createLocalVideo;
- (VLSVideoItem *)createLocalVideoOnly;

- (VLSVideoItem *)createRemoteVideoOfUid:(NSInteger)uid;


- (void)setLocalAudioMute:(BOOL)mute;
- (void)setLocalVideoMute:(BOOL)mute;
- (void)setRemoteAudioWithUid:(NSInteger)uid mute:(BOOL)mute;
- (void)setRemoteVideoWithUid:(NSInteger)uid mute:(BOOL)mute;

//
- (void)muteAllRemoteVideo:(BOOL)mute;
- (void)enableNetworkTest;
- (void)disableNetworkTest;
- (void)startPreview;
- (void)stopPreview;
- (void)joinChannel;
- (void)leaveChannel;
- (void)leaveChannel: (void(^)(BOOL))completeBlock;

- (void)switchCamera;
- (void)endVLSVideoLivingEndBlock:(void(^)(AgoraRtcStats *stat))block;
- (void)stopRecording;

- (void)setFullScreenRemoteVideoTypeOfUid:(NSInteger)uid;

- (void)setSmallScreenRemoteVideoTypeOfUid:(NSInteger)uid;
- (void)setRemoteRenderMode:(NSUInteger)uid mode:(AgoraRtcRenderMode)mode;
//切换用户类型（直播状态/非直播状态\
调用agoraSDK改变setClientRole
- (void)alternateUserType:(AGUserType)type;
@end
