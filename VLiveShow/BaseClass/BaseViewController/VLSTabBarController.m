//
//  VLSTabBarController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/11.
//  Copyright © 2016年 vliveshow. All rights reserved.

#import "VLSTabBarController.h"
#import "VLSMineViewController.h"
#import "VLSNavigationController.h"
#import "VLSLiveViewController.h"
#import "VLSAnchorTitleViewController.h"
#import "VLSPermissionLiveViewController.h"
#import "VLSFocusViewController.h"
#import "VLSMessageViewController.h"

#import "HostApplicationViewController.h"
#import "UITabBar+badge.h"
#import "LiveBeforeTestViewController.h"
#import "VLSLittleNavigationController.h"

#import "VLSShowViewController.h"
#import "VLSTeamMessageModel.h"
#import "VLSAdminViewController.h"
#import "VLSLiveViewController.h"
#import "VLSFocusViewController.h"
#import "VLSMessageViewController.h"
#import "VLSUserTrackingManager.h"
#import "VLiveShow-Swift.h"
#import "AdvertisementModel.h"
#import "AdvertisementViewController.h"

@interface VLSTabBarController ()<UITabBarControllerDelegate, VLSMessageAlertViewControllerDelegate>{

    VLSTeamMessageModel *vlstmodel;
    UIImageView *faceImg;
    UILabel *messageLabel;
    UIButton *clickImg;
    UIButton *clicked;
}

//邀请的数组数据
@property (nonatomic, strong) NSMutableArray *modelArray;

@property (nonatomic) BOOL disappearOnFourSeconds;

//消息提示框
@property (nonatomic, strong) VLSMessageAlertViewController* messageAlertVC;

@end

@implementation VLSTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tabBar setTintColor:[UIColor darkGrayColor]];
    self.delegate = self;
    
    [self tabControllers];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMsgWindow:) name:@"showMsgWindow" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideTipWindow:) name:@"hideTip" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideTipWindow:) name:@"hideview" object:nil];

    [self requestSplashData];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //iOS8 改变status orientation 为landscape tabbar的frame会莫名改变
    CGRect rect = self.tabBar.frame;
    rect.size.width = MIN(SCREEN_WIDTH, SCREEN_HEIGHT);
    self.tabBar.frame = rect;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - 请求广告屏数据
- (void)requestSplashData {
    NSString *path = @"/liveshow/splash/screen";
    WS(ws)
    [HTTPFacade getPath:path param:@{} successBlock:^(id  _Nonnull object) {
        AdvertisementModel *model = [[AdvertisementModel alloc] initWithDictionary:[object[@"data"] firstObject]];
        [AdvertisementViewController storeSplashData:model];
    } errorBlcok:^(ManagerEvent * _Nonnull error) {
        
    }];
}

- (void)showMsgWindow:(NSNotification*)notification{
    
    vlstmodel = notification.object;

    dispatch_async(dispatch_get_main_queue(),^(void){
        [self anchoInviteWithAnchoPicture:vlstmodel];
    });
}

- (void)hideTipWindow:(NSNotification *)notification{

    dispatch_async(dispatch_get_main_queue(),^(void){
        if (self.messageAlertVC) {
            [self.messageAlertVC removeFromParentViewController];
            self.messageAlertVC = nil;
        }
    });
}

#pragma mark - 主播邀请嘉宾展示在该页
- (void)anchoInviteWithAnchoPicture:(VLSTeamMessageModel*)mmodel
{
    UIViewController *vc = [self getCurrentVC];
    if ([vc isKindOfClass:[VLSHomeViewPagerViewController class]] || [vc isKindOfClass:[VLSLiveViewController class]] || [vc isKindOfClass:[VLSFocusViewController class]] || [vc isKindOfClass:[HostApplicationViewController class]] || [vc isKindOfClass:[VLSMessageViewController class]] || [vc isKindOfClass:[VLSMineViewController class]] || [vc isKindOfClass:[VLSMessageViewController class]] || [vc isKindOfClass:[BookingCourseViewController class]]) {
        if (self.messageAlertVC) {
            [self.messageAlertVC removeFromParentViewController];
            self.messageAlertVC = nil;
        }
        VLSMessageAlertViewController* messageAlertVC = [[VLSMessageAlertViewController alloc] init];
        messageAlertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        messageAlertVC.delegate = self;
        messageAlertVC.anchorId = mmodel.anchorID;
        messageAlertVC.avatorUrl = [NSString stringWithFormat:@"%@/v1/users/%@/avatars.png",BASE_URL,mmodel.anchorID];
        messageAlertVC.nickname = mmodel.anchorName;
        messageAlertVC.content = mmodel.title;
        [self presentViewController:messageAlertVC animated:YES completion:^{
            [UIView animateWithDuration:0.25 animations:^{
                messageAlertVC.view.backgroundColor = [UIColor colorWithWhite:0.25 alpha:0.25];
            }];
        }];
        self.messageAlertVC = messageAlertVC;
    }
}

//点击跳转直播间\
课程主播没有邀请权限，所以不用判断课程和用户的预约关系
-(void)goToLiveRoom{
//    [self.tabBar hideBadgeOnItemIndex:3];
    VLSShowViewController *show = [[VLSShowViewController alloc] init];
    show.roomId = vlstmodel.roomID;
    show.anchorId = vlstmodel.anchorID;
    show.isFromMailInvite = YES;
    
    [[VLSUserTrackingManager shareManager] trackingAgreeGotoRoom:vlstmodel.roomID anchorid:vlstmodel.anchorID fromWhere:@"tabbarInvite"];
    
    show.hidesBottomBarWhenPushed = YES;
    self.selectedIndex = 0;
    UINavigationController *vc = self.viewControllers[0];
    [vc pushViewController:show animated:YES];
}
- (UIViewController *)getCurrentVC{
    
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    //app默认windowLevel是UIWindowLevelNormal，如果不是，找到UIWindowLevelNormal的
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    //    如果是present上来的appRootVC.presentedViewController 不为nil
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else{
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        //        UINavigationController * nav = tabbar.selectedViewController ; 上下两种写法都行
        result=nav.childViewControllers.lastObject;
        
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
        if ([result isKindOfClass:[UIWindow class]]) {//解决切换账号第三方登录获取不到当前viewController
            if ([((UIWindow*)result).rootViewController isKindOfClass:[VLSTabBarController class]]) {
                if ([((VLSTabBarController*)(((UIWindow*)result).rootViewController)).selectedViewController isKindOfClass:[VLSNavigationController class]]) {
                    result = ((VLSNavigationController*)((VLSTabBarController*)(((UIWindow*)result).rootViewController)).selectedViewController).visibleViewController;
                }
            }
        }
    }
    return result;
}
- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    [super setSelectedIndex:selectedIndex];
}

- (void)tabControllers
{
    
    VLSHomeViewPagerViewController *liveVC = [[VLSHomeViewPagerViewController alloc] init];
    VLSNavigationController *liveNav = [[VLSNavigationController alloc] initWithRootViewController:liveVC];
    
    liveNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:LocalizedString(@"TABBAR_TITLE_HOME") image:[UIImage imageNamed:@"tabbar_home_nor"] selectedImage:[[UIImage imageNamed:@"tabbar_home_choose"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    VLSFocusViewController *focusVC = [[VLSFocusViewController alloc] init];
    VLSNavigationController *focusNav = [[VLSNavigationController alloc] initWithRootViewController:focusVC];
    
    focusNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:LocalizedString(@"TABBAR_TITLE_ATTENTION") image:[UIImage imageNamed:@"tabbar_follow_nor"] selectedImage:[[UIImage imageNamed:@"tabbar_follow_choose"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    HostApplicationViewController *permissionVC = [[HostApplicationViewController alloc] init];
    UINavigationController *permissionNav = [[UINavigationController alloc] initWithRootViewController:permissionVC];
    
    permissionNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:LocalizedString(@"TABBAR_TITLE_LIVE") image:[UIImage imageNamed:@"tabbar_live_nor"] selectedImage:[[UIImage imageNamed:@"tabbar_live_choose"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    VLSScheduleViewController* bookingVC = [[VLSScheduleViewController alloc] init];
    VLSNavigationController *mailNav = [[VLSNavigationController alloc] initWithRootViewController:bookingVC];
    
    mailNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:LocalizedString(@"TABBAR_TITLE_PLAN") image:[UIImage imageNamed:@"cart01"] selectedImage:[[UIImage imageNamed:@"cart02"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    VLSMineViewController *mine = [[VLSMineViewController alloc] init];
    VLSNavigationController *mineNav = [[VLSNavigationController alloc] initWithRootViewController:mine];
    
    mineNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:LocalizedString(@"TABBAR_TITLE_MINE") image:[UIImage imageNamed:@"tabbar_wo_nor"] selectedImage:[[UIImage imageNamed:@"tabbar_wo_choose"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // tabbaritem 图片居中
    CGFloat offset = 2.0;
    self.viewControllers = @[liveNav, focusNav, permissionNav, mailNav, mineNav];
    for (UITabBarItem *item in self.tabBar.items) {
        item.imageInsets = UIEdgeInsetsMake(offset, 0, -offset, 0);
    }
    
    //tabbarItem标题颜色变化
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:RGB16(COLOR_FONT_9B9B9B), NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor redColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
}



- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {

    NSString* tabName = @"home";
    NSInteger tabIndex = tabBarController.selectedIndex;
    if (tabBarController.selectedIndex == 1) {
        VLSNavigationController *focusNav = self.childViewControllers[1] ;
        [focusNav popToRootViewControllerAnimated:YES];
        
        tabName = @"follow";
    }
    if (tabBarController.selectedIndex == 2) {
        if ([AccountManager sharedAccountManager].account.isHost
            || [AccountManager sharedAccountManager].account.trialHost) {
            
            LiveBeforeTestViewController* liveVC = [[LiveBeforeTestViewController alloc] init];
            liveVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            VLSLittleNavigationController* anchorNav = [[VLSLittleNavigationController alloc] initWithRootViewController:liveVC];
            [self presentViewController:anchorNav animated:YES completion:nil];
            tabBarController.selectedIndex = 0;
       
        }
        viewController.tabBarController.tabBar.hidden = NO;
        tabName = @"live";
    }
    if (tabBarController.selectedIndex == 3) {
        tabName = @"plan";
    }
    if (tabBarController.selectedIndex == 4)
    {
        tabName = @"mine";
    }
    [[VLSUserTrackingManager shareManager] trackingClickWithAction: @"ClickMainTab" extraInfo: @{@"tabName": tabName, @"tabIndex": @(tabIndex)}];
}

-(NSMutableArray *)modelArray{
    if (!_modelArray) {
        self.modelArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _modelArray;
}

#pragma mark -- VLSMessageAlertViewControllerDelegate
- (void)eventHandle
{
    [self goToLiveRoom];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showMsgWindow" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
