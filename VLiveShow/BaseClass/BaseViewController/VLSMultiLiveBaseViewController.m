//
//  VLSLiveBaseViewController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/7/1.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSMultiLiveBaseViewController.h"
#import "closeRoomActionViewController.h"
#import "HeartBeatManager.h"
#import "NSString+UserFile.h"
#import "VLSHeartManager.h"

#import "blackView.h"
#import "VLSGiftManager.h"

@interface VLSMultiLiveBaseViewController ()<VLSMessageManagerDelegate,VLSAgoraVideoServiceDelegate,closeRoomDelegate,VLSHaertBeatWithWatchNum,VLSGiftManagerDelegate>
{
    NSMutableArray *offLineIds;
}
@end

@implementation VLSMultiLiveBaseViewController


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].idleTimerDisabled=YES;//不自动锁屏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.navigationController.viewControllers.count<2) {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
    [UIApplication sharedApplication].idleTimerDisabled=NO;//自动锁屏
}

- (void)viewDidLoad
{
    
    [self addNetwokChangeListner];

    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    offLineIds = [[NSMutableArray alloc] initWithCapacity:0];
    NSInteger userId = [AccountManager sharedAccountManager].account.uid;
    self.cNum = 0;
    
    /** 保存房间roomId，针对直播间IM roomId比较，防止IM混淆 */
    kSaveRoomId(self.roomId)
    
    if ([self.anchorId isEqualToString:[NSString stringWithFormat:@"%ld",(long)userId]]) {
        self.selfIsAnchor = YES;
        /************************发送心跳包*********************************/
        [[HeartBeatManager shareManager] startHeartBeat:self.roomId];
        [HeartBeatManager shareManager].delegate = self;
    }
    [self configur];
//    [self checkNetWorkBeforeLive];
    [VLSMessageManager sharedManager].lessonId = self.lessonId;
}

- (void)reloadLiveWidthRoomId:(NSString *)roomId anchorId:(NSString *)anchorId;
{
   
    if (![self.roomId isEqualToString:roomId]) {
        [[VLSMessageManager sharedManager] quitAVChatRoomGroupsucc:^{
            self.anchorId = anchorId;
            self.roomId = roomId;
            
            [self configur];
        } fail:^(int code, NSString *msg) {
            self.anchorId = anchorId;
            self.roomId = roomId;
            
            [self configur];
        }];
        [VLSMessageManager sharedManager].delegate = nil;
        [[VLSAgoraVideoService sharedInstance] leaveChannel];
        
        
    }
}


#pragma mark - 配置
- (void)configur
{
    if ([TIMManager sharedInstance].getLoginStatus == 2) {//im正在登陆中
        [self performSelector:@selector(configur) withObject:nil afterDelay:2];
    } else {
        [self subconfigur];
    }
}

- (void)subconfigur
{
    // 进来之后直接赋值防止VLSMessageManager内取不到roomid
    if (self.roomId) {
        [VLSIMManager sharedInstance].RoomID = self.roomId;
    }
    [VLSMessageManager sharedManager].delegate = self;
    [[VLSMessageManager sharedManager].liveAnchors removeAllObjects];
    [VLSAgoraVideoService sharedInstance].uid = [AccountManager sharedAccountManager].account.uid;
    [VLSAgoraVideoService sharedInstance].channel = self.roomId;
    [VLSAgoraVideoService sharedInstance].delegate = self;
    [VLSGiftManager sharedManager].delegate = self;
    
    [self loadAnchorInfo];
    if (self.selfIsAnchor) {
        [VLSAgoraVideoService sharedInstance].userType = AGUserTypeAnchor;
        [[VLSAgoraVideoService sharedInstance] setUpVideo];
        [[VLSAgoraVideoService sharedInstance] setupAudio];
        if (self.isReconnection) {
            [self RDKForRoomID:self.roomId result:^(NSDictionary *keyDict) {
                NSString *channelKey = [keyDict objectForKey:@"channel-key"];
                NSString *rdk = [keyDict objectForKey:@"record-key"];
                [VLSAgoraVideoService sharedInstance].RDK = rdk;
                [VLSAgoraVideoService sharedInstance].channelKey = channelKey;
                [[VLSAgoraVideoService sharedInstance] joinChannel];
                [self loadRoomInfo];
                [self loadAudiences];
            }];
        }else{
            [self loadRoomInfo];
        }
        
    }else{
#warning 如果channelKey获取不了直播是会灰屏的,在这里做相应操作.....
        [self RDKForRoomID:self.roomId result:^(NSDictionary *keyDict) {
            if (keyDict) {
                NSString *channelKey = [keyDict objectForKey:@"channel-key"];
                [VLSAVMultiManager sharedManager].linkStatus = UserLinkStatusAudience;
                [VLSAgoraVideoService sharedInstance].userType = AGUserTypeDefault;
                [VLSAgoraVideoService sharedInstance].channelKey = channelKey;
                [[VLSAgoraVideoService sharedInstance] setUpVideo];
                [[VLSAgoraVideoService sharedInstance] setupAudio];
                [[VLSAgoraVideoService sharedInstance] joinChannel];
                [self requestisAttend];
                [self loadRoomInfo];
            }
        }];
    }
    if (!self.isReconnection) {
        [[VLSAVMultiManager sharedManager] initFirstAVroomAnchorId:self.anchorId];
    }
}


#pragma mark 加入直播间
- (void)joinRoom
{
    // 首先判断是否是登录状态
    if ([[VLSMessageManager sharedManager] GetLoginState]) {
        // 观众加入到直播间
        if (self.roomId) {
//            [self showProgressHUDWithStr:LocalizedString(@"PLEASE_WAIT") dimBackground:YES];
//            //课程直播间不显示进入房间信息
//            if (self.actionType == ActionTypeLessonAudience || self.actionType == ActionTypeLessonAttendance) {
//                return;
//            }
            [[VLSMessageManager sharedManager] joinGroupRoomID:self.roomId succ:^{
                NSLog(@"加入房间成功");
                
                if (!self.anchorProfile) {
                    [self loadAnchorInfo];
                }
                [self joinRoomSuccess];
                [self loadAudiences];
                
            } fail:^(int code, NSString *msg) {
                [self performSelector:@selector(joinRoom) withObject:nil afterDelay:2];//2秒钟之后继续加入房间
            }];
        }else{
            
        }
    }else{
        NSString *identifier =[NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
        NSString *usersig = [AccountManager sharedAccountManager].account.tenderSig;
        
        if (identifier && usersig) {
            
            [[VLSMessageManager sharedManager] loginIMidentifier:identifier userSig:usersig succ:^{
                [[VLSMessageManager sharedManager] getSelfProfile];
                [self joinRoom];
                NSLog(@"登录成功");
            } fail:^(int code, NSString *msg) {
                NSLog(@"%d:%@",code,msg);
            }];
            
        }else{
            return;
        }
    }
    
}

#pragma mark -退出直播间操作

//主播退出
- (void)anchorQuiteRoom
{
    [[VLSReConnectionManager sharedManager] deleteRoomIDandAnchorID];
    //停止发送心跳包
    [[HeartBeatManager shareManager]destoryHeartBeat];
    /**
     *  主播解散直播间，就不在收到消息，所以设置代理为空
     */
    [VLSMessageManager sharedManager].delegate = nil;
    ManagerCallBack *callBack =[[ManagerCallBack alloc] init];
    callBack.updateBlock = ^(id result){
        
    };
    callBack.errorBlock = ^(id result){
        
        [self error:result];
    };
    [VLSLiveHttpManager destroyLive:_roomId callback:callBack];

    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:YES];
    [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:YES];
    [[VLSAgoraVideoService sharedInstance] stopPreview];
    [[VLSAgoraVideoService sharedInstance] stopRecording];
    [[VLSAgoraVideoService sharedInstance] leaveChannel];

    
}
//...
- (void)audienceQuiteRoom
{
    [self audienceQuiteRoom: YES completeBlock: nil];
}

- (void)audienceQuiteRoom: (BOOL)needQuit completeBlock: (void (^)(BOOL)) completeBlock
{
    [VLSMessageManager sharedManager].delegate = nil;
    [[VLSMessageManager sharedManager]quitAVChatRoomGroupsucc:^{
        

        NSLog(@"退出房间成功");
    } fail:^(int code, NSString *msg) {
        if (completeBlock)
        {
            completeBlock(NO);
        }
        NSLog(@"%d:%@",code,msg);
    }];
    
    
    [[VLSAgoraVideoService sharedInstance] leaveChannel: ^(BOOL success)
    {
        if (completeBlock)
        {
            completeBlock(YES);
        }
    }];
    [VLSAgoraVideoService sharedInstance].channelKey = nil;
    
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    //群解散的时候把弹出来的控制器都dismiss
    //    [self.presentedViewController dismissViewControllerAnimated:NO completion:^{
    //       [self.navigationController popViewControllerAnimated:((self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) ? NO : YES)];
    //    }];
    
    if (needQuit)
    {
        if (self.presentedViewController) {
            [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
                [self.navigationController popViewControllerAnimated:((self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) ? NO : YES)];
            }];
        }else {
            [self.navigationController popViewControllerAnimated:((self.orientation == KLiveRoomOrientation_LandscapeRight || self.orientation == KLiveRoomOrientation_LandscapeLeft) ? NO : YES)];
        }
    }
    else
    {
        if (self.presentedViewController)
        {
            [self.presentedViewController dismissViewControllerAnimated: NO completion: nil];
        }
    }
}

//...
- (void)guestQuiteRoom
{
    
    [self audienceQuiteRoom];
    [self guestCancelLink];
    
    [VLSMessageManager sharedManager].selfIsGuest = NO;
    [VLSMessageManager sharedManager].selfIsVoiceMute = NO;
    [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:YES];
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:YES];

}

- (void)guestEndLink
{
    [self guestCancelLink];
    
    [VLSMessageManager sharedManager].selfIsGuest = NO;
    [VLSMessageManager sharedManager].selfIsVoiceMute = NO;
    [[VLSAgoraVideoService sharedInstance] setLocalVideoMute:YES];
    [[VLSAgoraVideoService sharedInstance] setLocalAudioMute:YES];
    
}

#pragma mark 嘉宾取消连线请求（未成为嘉宾之前）
- (void)guestCancelLinkRequest
{
    // 取消联系请求
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [self guestCancelLinkRequestSuccess];
    };
    callback.errorBlock = ^(id result){
        [self error:result];
        
    };
    NSString *anchorID = [VLSMessageManager sharedManager].anchor.userId;
    [[VLSAVMultiManager sharedManager] linkRequestCancel:anchorID callback:callback];
}

#pragma mark - VLSGiftManagerDelegate

- (void)receiveNormalGiftMessage:(GiftModel *)model{

}

#pragma mark 嘉宾取消连线功能
- (void)guestCancelLink
{
    ManagerCallBack *callback = [[ManagerCallBack alloc]init];
    callback.updateBlock = ^(id result){
        MyLog(@"取消连线成功");
        if (self) {
            [self guestCancelLinkSuccess];
        }
    };
    callback.errorBlock = ^(id result){
        [self error:result];
    };
    
    NSString *anchorID = [VLSMessageManager sharedManager].anchor.userId;
    [[VLSAVMultiManager sharedManager] guestCancel:anchorID callback:callback];
}
/*
#pragma mark - 加载房间信息
- (void)loadRoomInfo
{
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if (result) {
            
            self.liveInfo = result;
            
            [self reloadRoomInfo];
            if (self.liveInfo.layout) {
                [self reloadVideoView];
            }
        }
        if (!self.liveInfo.living) {
            [self receiveDeleteGroup];
        }
        // 房间信息的v票
        
    };
    callback.errorBlock = ^(id result){
        
    };
    [VLSLiveHttpManager getLiveInfo:_roomId callback:callback];
}
*/

#pragma mark - 加载观众列表
- (void)loadAudiences
{

    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    
    callback.updateBlock = ^(id result){
        self.menberList = result;
        self.isAudiences = YES;
        [self reloadMemberList];
    };
    callback.errorBlock = ^(id result){
        
    };
    NSInteger liveMaxOnlineAvator;
    if ([VLSHeartManager shareHeartManager].heartModel.liveMaxOnlineAvator) {
        liveMaxOnlineAvator = [VLSHeartManager shareHeartManager].heartModel.liveMaxOnlineAvator + 1;
    }else{
        liveMaxOnlineAvator = 11;
    }
    
    [VLSLiveHttpManager getAudiuesInfo:self.roomId size:liveMaxOnlineAvator callback:callback];

}

#pragma mark - 获取主播信息
- (void)loadAnchorInfo
{    
    [[VLSMessageManager sharedManager] getUserProfile:self.anchorId succ:^(VLSUserProfile *profile) {
        [VLSMessageManager sharedManager].anchor = profile;
        self.anchorProfile = profile;
        [self reloadAnchorInfo];
    }];

}

#pragma mark 根据返回的结果有三种情况 1.已关注，2.黑名单，3.未关注

- (void)requestisAttend
{
    ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result)
    {
        
        NSDictionary *dic = result;
        NSAssert(dic[@"is_attended"], @"关注API返回字典没有该属性");
        NSString *isattend = [dic objectForKey:@"is_attended"];
        NSString *isblacklist = [dic objectForKey:@"is_blacklist_by"];
        NSString *blacklist = [dic objectForKey:@"is_blacklist"];
        if ([isattend isEqualToString:@"Y"] && [isblacklist isEqualToString:@"N"]) {
            // 已关注
            [self joinRoom];
        }else if([isattend isEqualToString:@"N"] && [isblacklist isEqualToString:@"N"]){
            // 未关注
            [self notAttendAnchor];
            [self joinRoom];
        }else if([isblacklist isEqualToString:@"Y"]){
            // 黑名单
            blackView *black = [[blackView alloc]initWithFrame:self.view.bounds];
            /**
             *  关闭agora视频直播
             */
            [[VLSAgoraVideoService sharedInstance] leaveChannel];
            [VLSAgoraVideoService sharedInstance].channelKey = nil;
            black.clickBlock = ^{
//                [self.navigationController popViewControllerAnimated:YES];
                [self audienceQuiteRoom];
                
            };
            [self.view addSubview:black];
        }
        if ([blacklist isEqualToString:@"Y"]){
            [[VLSAgoraVideoService sharedInstance] leaveChannel];
            [VLSAgoraVideoService sharedInstance].channelKey = nil;
            [self.navigationController popViewControllerAnimated:YES];
        }
    };
    callback.errorBlock = ^(id result){
    };
    [VLSLiveHttpManager getrelationShip:self.anchorId callBack:callback];
    
}

- (void)sendLayoutToBack
{
    ManagerCallBack *callback = [[ManagerCallBack alloc]init];
    callback.updateBlock = ^(id result){
        MyLog(@"向后台发送布局成功");
    };
    callback.errorBlock = ^(id result){
        [self error:result];
    };
    [[VLSAVMultiManager sharedManager] anchorSendLayoutToBackgroungCallback:callback];
}


#pragma mark -VLSAgoraVideoServiceDelegate

- (void)agoraListenerFirstLocalVideoFrameWithSize:(CGSize)size
{
    
}

- (void)agoraListenerDidJoinedOfUid:(NSUInteger)uid
{
    NSString *userId = [NSString stringWithFormat:@"%lu",(unsigned long)uid];
    if ([offLineIds containsObject:userId]) {
        if ([[VLSMessageManager sharedManager] checkUserIsLiving:userId]) {
            if ([[VLSMessageManager sharedManager] checkUserIsAnchor:userId]) {
                [self anchorIsBack];
            }else{
                [self guestIsBackUid:userId];
            }
        }else{
            [offLineIds removeObject:userId];
        }
    }
}

//掉线
- (void)agoraListenerDidOfflineOfUid:(NSUInteger)uid
{
    NSString *userId = [NSString stringWithFormat:@"%lu",(unsigned long)uid];
    [offLineIds addObject:userId];
    if ([[VLSMessageManager sharedManager] checkUserIsAnchor:userId]) {
        [self anchorOffLine];
    }else{
        [self guestOffLineUid:userId];
    }
}

- (void)agoraListenerDidVideoMuted:(BOOL)muted byUid:(NSUInteger)uid
{
    
}

#pragma mark VLSHaertBeatWithWatchNum

- (NSInteger)heartBeatWithWatchNum
{
    return self.cNum;
}


#pragma mark - 等待子类继承

- (void)reloadVideoView
{
    NSMutableArray *temp =[NSMutableArray arrayWithCapacity:0];
    for (VLSVideoAnchorModel *model in self.liveInfo.layout.anchors) {
        VLSVideoViewModel *vmodel = [[VLSVideoViewModel alloc] initViewModelWidthVideoAnchorModel:model];
        [temp addObject:vmodel];
    }
    [[VLSMessageManager sharedManager] initFirstAVroom:temp];
}

- (void)reloadMemberNumber
{

}

- (void)reloadMemberList
{
    
}

- (void)reloadAnchorInfo
{
    
}

- (void)reloadRoomInfo
{
}

- (void)touchOnLiveBaseView:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
}
- (void)notAttendAnchor
{
    
}

- (void)guestCancelLinkSuccess
{
    
}

- (void)guestCancelLinkRequestSuccess
{

}

- (void)anchorOffLine
{

}

- (void)anchorIsBack
{

}
- (void)guestOffLineUid:(NSString *)uid
{
    
}

- (void)guestIsBackUid:(NSString *)uid
{

}

- (void)joinRoomSuccess
{

}

//收回键盘等操作
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
    
    [self touchOnLiveBaseView:touches withEvent:event];
}

#pragma mark - 开始先做网络监测
- (void)checkNetWorkBeforeLive
{
    BOOL isWIfi = [AFNetworkReachabilityManager sharedManager].isReachableViaWiFi;
    BOOL iswan = [AFNetworkReachabilityManager sharedManager].isReachableViaWWAN;
    BOOL is3G = [[NSUserDefaults standardUserDefaults] boolForKey:@"is3G"];
    if (isWIfi) {
        
    }else if (is3G == NO&&isWIfi == NO) {
        if (iswan == YES) {
            [self showNetworkAlert];
        }
    }else{
        
        
    }
}
// 添加网络监听,只在直播的过程中监听，创建房间过程中不监听
// 进入直播成功后监听
- (void)addNetwokChangeListner
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNetworkChanged:) name:@"4G" object:nil];

}

// 网络类型有变化
- (void)onNetworkChanged:(NSNotification*)notifiy
{
    
    NSString *object = notifiy.object;
    if ([object isEqualToString:@"wifi"]) {

        
    }else if ([object isEqualToString:@"is4G"]) {
        
        [self showNetworkAlert];
    
    }else if ([object isEqualToString:@"nocontect"]) {
       
        [self showNoNetworkAlert];
    }
}

// 网络断开与重联
- (void)onNetworkConnected
{

}


#pragma mark 警告弹出视图

- (void)showAnchorQuiteRoomAlertTapBlock:(UIAlertControllerCompletionBlock)tapBlock
{
    [UIAlertController showAlertInViewController:self withTitle:LocalizedString(@"LIVE_ANCHOR_ALERT_CLOSE")  message:LocalizedString(@"LIVE_ANCHOR_ALERT_CLOSE_SURE") cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"INVITE_CANCEL"),LocalizedString(@"YES"),] tapBlock:tapBlock];
}

- (void)showGuestQuitRoomAlertTapBlock:(UIAlertControllerCompletionBlock)tapBlock
{
    [UIAlertController showActionSheetInViewController:self withTitle:nil message:nil cancelButtonTitle:LocalizedString(@"INVITE_CANCEL") destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"INVITE_GOTOCONNECT_END"),LocalizedString(@"QUIT_LIVE")] popoverPresentationControllerBlock:nil tapBlock:tapBlock];
    
}

- (void)showWarnAlertview:(NSString *)str
{
    [self showNoTapBlockAlertControllerInVC:self withTitle:LocalizedString(@"ALERT_TITLE_WARING") message:str onlyCancelButton:LocalizedString(@"INVITE_CANCEL")];

}

- (void)showSendBlackAlert
{
    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:0];
    [self showNoTapBlockAlertControllerInVC:vc withTitle:LocalizedString(@"ALERT_TITLE_WARING") message:LocalizedString(@"INVITE_OUT_ROOM") onlyCancelButton:LocalizedString(@"LIVEBEFORE_I_KONWEN")];
}

//弹出提示信息
- (void)showNetworkAlert
{
    VLSMessageManager *manager = [VLSMessageManager sharedManager];

    if ([manager checkUserIsAnchor:manager.host.userId]) {
        [UIAlertController showAlertInViewController:self withTitle:LocalizedString(@"LIVEBEFORE_REMIND") message:LocalizedString(@"LIVEBEFORE_ANCHORNO_WIFI") cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"LIVEBEFORE_I_KONWEN")] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
            
                    }];

        
    }else if([manager checkUserIsGuests:manager.host.userId]) {
        [UIAlertController showAlertInViewController:self withTitle:LocalizedString(@"LIVEBEFORE_REMIND") message:LocalizedString(@"LIVEBEFORE_ANCHORNO_WIFI") cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"LIVEBEFORE_I_KONWEN")] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
            
        }];
    }else{
        
        [UIAlertController showAlertInViewController:self withTitle:LocalizedString(@"LIVEBEFORE_REMIND") message:LocalizedString(@"LIVEBEFORE_ANCHORNO_WIFI") cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"LIVEBEFORE_I_KONWEN")] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
            
        }];

        
    }
}


- (void)RDKForRoomID:(NSString *)roomId result:(void (^)(NSDictionary *keyDict))success{
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    
    callback.updateBlock = ^(id result){
        
        success(result);
        
    };
    
    callback.errorBlock = ^(id result){
        
        success(nil);
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [VLSLiveHttpManager RDKForRoomId:roomId callBack:callback];
    
}
- (void)showNoNetworkAlert
{
    [UIAlertController showAlertInViewController:self withTitle:LocalizedString(@"ALERT_NETWORK_ERROR") message:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@[LocalizedString(@"LIVEBEFORE_I_KONWEN")] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        }];
}



- (void)dealloc
{
    [[HeartBeatManager shareManager] destoryHeartBeat];
    [VLSMessageManager sharedManager].delegate = nil;
}

@end
