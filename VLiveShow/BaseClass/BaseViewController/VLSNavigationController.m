//
//  VLSNavigationController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSNavigationController.h"

@interface VLSNavigationController ()<UIGestureRecognizerDelegate>
@property (nonatomic, assign) UIInterfaceOrientationMask interfaceOrientationMask;
@end

@implementation VLSNavigationController
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self changeSupportedInterfaceOrientations:UIInterfaceOrientationMaskPortrait];
    }
    return self;
}

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    if (self = [super initWithRootViewController:rootViewController]) {
        [self changeSupportedInterfaceOrientations:UIInterfaceOrientationMaskPortrait];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置导航条item字体颜色
    [self.navigationBar setTranslucent:NO];

    [self.navigationBar setTintColor:RGB16(COLOR_FONT_4A4A4A)];
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:  @{NSFontAttributeName:[UIFont systemFontOfSize:18],
                                                                                              NSForegroundColorAttributeName:RGB16(COLOR_FONT_4A4A4A)},NSForegroundColorAttributeName,nil]];
    [self.navigationBar setBackgroundImage:[UIImage ImageFromColor:[UIColor whiteColor]] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];

    [self.navigationBar setTitleTextAttributes:
  @{NSFontAttributeName:[UIFont systemFontOfSize:18],
    NSForegroundColorAttributeName:RGB16(COLOR_FONT_4A4A4A)}];
    
}

#pragma mark - Orientation
- (void)changeSupportedInterfaceOrientations:(UIInterfaceOrientationMask)interfaceOrientationMask {
    self.interfaceOrientationMask = interfaceOrientationMask;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return self.interfaceOrientationMask;
}

@end
