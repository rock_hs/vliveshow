//
//  VLSTabBarController.h
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSTabBarController : UITabBarController

@property (nonatomic, strong) UIView *testView;

@end
