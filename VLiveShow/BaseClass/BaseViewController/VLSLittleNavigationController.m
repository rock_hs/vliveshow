//
//  VLSLittleNavigationController.m
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/6.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSLittleNavigationController.h"
#import "VLSLiveNoViewController.h"
@interface VLSLittleNavigationController ()

@end

@implementation VLSLittleNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    //设置导航条item字体颜色
    [self.navigationBar setTranslucent:NO];
    
    [self.navigationBar setTintColor:[UIColor blackColor]];
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor grayColor],NSForegroundColorAttributeName,nil]];
    [self.navigationItem.backBarButtonItem setBackButtonTitlePositionAdjustment:UIOffsetMake(-100, -100) forBarMetrics:UIBarMetricsDefault];
    for (UIViewController *vc in self.viewControllers) {
        vc.view.frame = self.view.frame;
    }
//    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"button04.png"] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
