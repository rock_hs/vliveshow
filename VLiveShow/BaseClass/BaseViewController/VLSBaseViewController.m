//
//  VLSBaseViewController.m
//  VLiveShow
//
//  Created by 李雷凯 on 16/5/11.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "AccountManager.h"
#import "VLSIMManager.h"
#import "VLSMessageManager.h"
#import "VLSLoginViewController.h"
#import "AppDelegate.h"
#import "VLSPhoneLoginViewController.h"
#import "MBProgressHUD+Add.h"
#import "VLSDBHelper.h"
#import "VLSLiveHttpManager.h"
#import "VLSAnchorViewController.h"
#import "VLSShowViewController.h"
#import "VLSTabBarController.h"
#import "VLiveShow-Swift.h"

@interface VLSBaseViewController ()<TIMUserStatusListener,TIMConnListener,UIGestureRecognizerDelegate>

@end

@implementation VLSBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    _app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (self.navigationController.viewControllers.count > 1) {
        
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(leftItemAction:)];
        self.navigationItem.leftBarButtonItem = leftItem;
    }
    
    
    // 关键行
    //    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}
- (void)leftItemAction:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (void)setNavigationBarRedMethod {
    
    self.navigationController.navigationBar.tintColor = RGB16(COLOR_BG_FFFFFF);
    self.navigationController.navigationBar.backgroundColor = RGB16(COLOR_BG_FF1130);//[UIColor redColor];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage ImageFromColor:[UIColor redColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_17],
       NSForegroundColorAttributeName:RGB16(COLOR_FONT_FFFFFF)}];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear: animated];
    [[VLSIMManager sharedInstance].manager setUserStatusListener: nil];
    [[VLSIMManager sharedInstance].manager setConnListener: nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[VLSIMManager sharedInstance].manager setUserStatusListener:self];
    [[VLSIMManager sharedInstance].manager setConnListener:self];
}


// 显示loading
- (void)showProgressHUD {
    self.isLoading = YES;
    [self showProgressHUDWithStr:nil dimBackground:NO];
}
// 隐藏loading
- (void)hideProgressHUDAfterDelay:(NSTimeInterval)delay {
    self.isLoading = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.HUD != nil) {
            [self.HUD hide:YES];
            [self.HUD removeFromSuperview];
        }
    });
    
}
// 显示带文字的loading
- (void)showProgressHUDWithStr:(NSString *)str dimBackground:(BOOL)dim
{
    if (_HUD == nil) {
        self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
        
    }
    
    self.HUD.dimBackground = dim;
    if (str.length == 0) {
        self.HUD.labelText = nil;
    } else {
        self.HUD.labelText = str;
    }
    // 显示loading
    [self.view addSubview:_HUD];
    [self.HUD show:YES];
}

- (void)showProgressHUD: (NSString*)text
{
    self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:true];
    self.HUD.labelText = text;
//    self.HUD.dimBackground = true;
    [self.HUD hide:true afterDelay:1.0];
}

-(void)success:(ManagerEvent*)event
{
    ManagerEventType type = event.type;
    switch (type) {
        case ManagerEventToast:
        {
            NSString* title = event.title;
            NSString* info = event.info;
            MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = title;
            hud.detailsLabelText = info;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
            
        }
            break;
            
        default:
            break;
    }
}

-(void)error:(ManagerEvent*)event
{
    ManagerEventType type = event.type;
    switch (type) {
        case ManagerEventToast:
        {
            NSString* title = event.title;
            NSString* info = event.info;
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = title;
            hud.detailsLabelText = info;
            hud.removeFromSuperViewOnHide = YES;
            hud.yOffset = -90;
            [hud hide:YES afterDelay:2];
        }
            break;
            
        case ManagerEventExpired:
        {
            if ([[AccountManager sharedAccountManager].account.token length] > 0)
            {
                [self.navigationController popToRootViewControllerAnimated:YES];
                [[AccountManager sharedAccountManager] signOut];
                [(AppDelegate *)[UIApplication sharedApplication].delegate  showLoginController];
            }
        }
            break;
        case ManagerEventAlert:
        {
            NSString* title = event.title;
            NSString* info = event.info;
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:info message:title preferredStyle:UIAlertControllerStyleAlert];
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"LOGIN_PAGE_LOGIN") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                VLSPhoneLoginViewController *phoneLoginVC = [[VLSPhoneLoginViewController alloc] init];
                [self.navigationController pushViewController:phoneLoginVC animated:YES];
                
            }];
            // 取消按钮
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleCancel handler: nil];
            
            [alertController addAction:okAction];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
}

- (void)toastHUDWithStr:(NSString *)str dimBackground:(BOOL)dim
{
    if (_HUD == nil) {
        self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
        
    }
    self.HUD.dimBackground = dim;
    self.HUD.labelText = str;
    self.HUD.mode = MBProgressHUDModeText;
    [self.HUD show:YES];
}

- (void)showHUD:(NSString*)text
{
    MBProgressHUD * HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.mode = MBProgressHUDModeText;
    HUD.margin = 10.f;
    HUD.yOffset = 15.f;
    HUD.removeFromSuperViewOnHide = YES;
    HUD.detailsLabelText = text;
    HUD.detailsLabelFont = [UIFont systemFontOfSize:15];
    [HUD hide:YES afterDelay:1.5];
}

#pragma mark 用户状态

/**
 *  踢下线通知
 */
- (void)onForceOffline
{
    [self ShowAlterView:LocalizedString(@"CHECKING_LOGIN")];
}

/**
 *  断线重连失败
 */
- (void)onReConnFailed:(int)code err:(NSString*)err
{
    //    [self ShowAlterView:@"重连失败"];
}

/**
 *  用户登录的userSig过期，需要重新登录
 */
- (void)onUserSigExpired
{
    //    [self ShowAlterView:@"检测到签名过期"];
}
#pragma mark 网络连接监听
/**
 *  网络连接成功
 */
- (void)onConnSucc
{
    //    [self ShowAlterView:@"网络重连"];
}

/**
 *  网络连接失败
 *
 *  @param code 错误码
 *  @param err  错误描述
 */
- (void)onConnFailed:(int)code err:(NSString*)err
{
    //    [self ShowAlterView:err];
}

/**
 *  网络连接断开（断线只是通知用户，不需要重新登陆，重连以后会自动上线）
 *
 *  @param code 错误码
 *  @param err  错误描述
 */
- (void)onDisconnect:(int)code err:(NSString*)err
{
    //    [self ShowAlterView:err];
}


#pragma mark 获取用户信息然后获取腾讯签名然后IM登录

- (void)getUserInfo {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
//        [self getTencentSig];
        
        
    };
    
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                AppDelegate *appdele = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appdele showLoginController];
            });
        }
    };
    [AccountManager getUserProfileCallback:callback];
}

#pragma mark - 获取腾讯云签名

- (void)getTencentSig {
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if (![[VLSMessageManager sharedManager] GetLoginState]) {
            [[VLSMessageManager sharedManager] IMLogin];//重连IM
        }
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                AppDelegate *appdele = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appdele showLoginController];
            });
        }
    };
    [AccountManager getTencentSigCallback:callback];
    
    
}



#pragma mark alterview
// 踢下线通知
- (void)ShowAlterView:(NSString *)str
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:str preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:LocalizedString(@"INVITE_CANCEL") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([self isKindOfClass:[VLSAnchorViewController class]]) {
            //            [self dismissViewControllerAnimated:YES completion:nil];
            [self anchorQuiteRoom];
            
        }else if([self isKindOfClass:[VLSShowViewController class]]){
            [self audienceQuiteRoom];
        }
        
#pragma mark 登录界面
        [(AppDelegate *)[UIApplication sharedApplication].delegate  showLoginController];
        
    }];
    
    UIAlertAction *LoginAction = [UIAlertAction actionWithTitle:LocalizedString(@"LOGIN_AGAIN") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (![[VLSMessageManager sharedManager] GetLoginState]) {
            [[VLSMessageManager sharedManager] IMLogin];//重连IM
        }
    }];
    
    [alertController addAction:cancleAction];
    [alertController addAction:LoginAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showAlterviewController:(NSString *)str{
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:nil message:str preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    });
    
}

- (UIAlertController *)showNoTapBlockAlertControllerInVC:(UIViewController *)vc withTitle:(NSString *)title message:(NSString *)message onlyCancelButton:(NSString *)buttonTitle;
{
    UIAlertController *alertController = [UIAlertController showAlertInViewController:vc withTitle:title message:message cancelButtonTitle:nil destructiveButtonTitle:buttonTitle otherButtonTitles:nil tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        
        
    }];
    return alertController;
}

- (UIAlertController *)showNoTapBlockAlertControllerInVC:(UIViewController *)vc withTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray *)buttonTitles
{
    UIAlertController *alertController = [self showAlertControllerInVC:vc withTitle:title message:message buttonTitles:buttonTitles tapBlock:nil];
    return alertController;
}

- (UIAlertController *)showAlertControllerInVC:(UIViewController *)vc withTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray *)buttonTitles tapBlock:(UIAlertControllerCompletionBlock)tapBlock
{
    UIAlertController *alertController = [UIAlertController showAlertInViewController:vc withTitle:title message:message cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:buttonTitles tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        
    }];
    return alertController;
}


#pragma mark - 分享处理
//分享
- (void)shareToPlatform:(NSInteger)platformType messageObject:(NSDictionary*)dic completion:(void(^)(NSDictionary* item))complete
{
    NSString* userID = [dic objectForKey: @"userID"];
    NSString* roomID = [dic objectForKey: @"roomID"];
    NSURLComponents* urlComp = [NSURLComponents componentsWithString: SHARE_THTML_URL];
    urlComp.queryItems = @[];
    if ([dic objectForKey:@"urlString"] != nil) {
        urlComp = [NSURLComponents componentsWithString: [dic objectForKey:@"urlString"]];
    }
    if (userID.length > 0) {
        urlComp.queryItems = [urlComp.queryItems arrayByAddingObject: [NSURLQueryItem queryItemWithName: @"userId" value: userID]];
    }
    if (roomID.length > 0) {
        urlComp.queryItems = [urlComp.queryItems arrayByAddingObject: [NSURLQueryItem queryItemWithName: @"roomId" value: roomID]];
    }
    urlComp.queryItems = [urlComp.queryItems arrayByAddingObject: [NSURLQueryItem queryItemWithName: @"source" value: [self platformIndex:platformType]]];
    VLSShareModel* shareModel = [VLSShareModel new];
    shareModel.title = [dic objectForKey:@"title"] == nil ? LocalizedString(@"SHARE_HTML_URL_TITLE") : [dic objectForKey:@"title"];
    if ([[dic objectForKey:@"overrideTitleInWXTimeline"] boolValue]) {
        shareModel.title = [dic objectForKey:@"content"] == nil ? LocalizedString(@"SHARE_HTML_URL_TITLE") : [dic objectForKey:@"content"];
    }
    shareModel.content = [dic objectForKey:@"content"];
    if ([dic objectForKey:@"coverImageURL"] != nil && ![[dic objectForKey:@"coverImageURL"] isEqualToString:@""]) {
        NSURL* coverImageURL = [NSURL URLWithString:[dic objectForKey:@"coverImageURL"]];
        shareModel.thumbImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:coverImageURL]];
    } else if ([dic objectForKey:@"coverImage"] != nil) {
        shareModel.thumbImage = [dic objectForKey:@"coverImage"];
    }
    shareModel.webpageUrl = urlComp.string;
    [VLSShareManager share:platformType messageObject:shareModel currentViewController: self complete: ^(NSDictionary* item) {
        complete(item);
    }];
}


- (void)setHidesBottomBarWhenPushed:(BOOL)hidesBottomBarWhenPushed
{
    
    [super setHidesBottomBarWhenPushed:hidesBottomBarWhenPushed];
    if (hidesBottomBarWhenPushed == YES) {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"hideview" object:nil];
        
    }else{
    }
    NSLog(@"+++++++++++++++++++++++++%d",self.tabBarController.tabBar.hidden);
    
}

- (void)showWarnAlterview:(NSString *)str
{
    
}  

- (void)shareTrackingLiveBefore:(NSString *)roomID Index:(NSInteger)index
{
    NSString *platform = [self platformIndex:index];
    // 发送分享跟踪
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    
    callback.errorBlock = ^(id result){
        
    };
    [VLSLiveHttpManager shareTrackingLiveBefore:platform RoomID:roomID callback:callback];
}
- (void)shareTrackingLiveAfter:(NSString *)roomID Index:(NSInteger)index
{
    NSString *platform = [self platformIndex:index];
    
    // 发送分享跟踪
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    
    callback.errorBlock = ^(id result){
        
    };
    [VLSLiveHttpManager shareTrackingLiveAfter:platform RoomId:roomID callback:callback];
}
- (void)shareTrackingLiveProcess:(NSString *)roomID Index:(NSInteger)index
{
    NSString *platform = [self platformIndex:index];
    // 发送分享跟踪
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    
    callback.errorBlock = ^(id result){
        
    };
    [[VLSUserTrackingManager shareManager] trackingShareStatus:@"LiveProcess" platform:platform roomID:roomID];

    [VLSLiveHttpManager shareTrackingLiveProcess:platform RoomId:roomID callback:callback];
}

- (NSString *)platformIndex:(NSInteger)index
{
    NSString *platform;
    
    switch (index) {
        case 0:{
            platform = QQPlatform;
        }
            break;
        case 1:{
            platform = QQZPlatform;
        }
            break;
        case 2:{
            platform = WeChatPlatform;
        }
            break;
        case 3:{
            platform = MomentsPlatform;
        }
            break;
        case 4:{
            platform = SinaPlatform;
        }
            break;
        default:
            break;
    }
    return platform;
}
// 等待子类重写
- (void)anchorQuiteRoom
{
    
}
- (void)audienceQuiteRoom
{

}

- (void)loadRequestWebViewWithString:(NSString*)pathFile{
    
    UIWebView *webView = [[UIWebView alloc] init];
    webView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    NSURL *url = [[NSBundle mainBundle] URLForResource:pathFile withExtension:nil];
    NSURLRequest *requeset = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requeset];
    [self.view addSubview:webView];
}
@end
