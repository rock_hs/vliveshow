//
//  VLSLittleNavigationController.h
//  VLiveShow
//
//  Created by 盛宣伟 on 16/7/6.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLSLittleNavigationController : UINavigationController
@property (nonatomic,strong)UIViewController *fatherVC;

@end
