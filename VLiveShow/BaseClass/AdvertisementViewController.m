    //
//  AdvertisementViewController.m
//  VLiveShow
//
//  Created by tom.zhu on 10/01/2017.
//  Copyright © 2017 vliveshow. All rights reserved.
//

#import "AdvertisementViewController.h"
#import "UIView+tapGesture.h"
#import "Macro.h"
#import "HTTPFacade+get.h"
#import "VLSUtily.h"

static NSTimeInterval TIMEINTERVAL = 1;
static int BEGIN = 4;
static int IPHONE6_HEIGHT = 667;

@interface AdvertisementViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *advertiseImageView;
@property (weak, nonatomic) IBOutlet UIButton *countBtn;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, copy) void (^block)();
@property (nonatomic, strong) AdvertisementModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *sloganImageView;

@end

@implementation AdvertisementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    self.model = [self.class getSplashData];
    
    [self.advertiseImageView addGestureRecognizer:@selector(adDidClicked) target:self];
    WS(ws)
    self.block = ^{
        [ws.timer invalidate];
        if (ws.skipBlock) {
            ws.skipBlock();
        }
    };
    self.timer = [NSTimer scheduledTimerWithTimeInterval:TIMEINTERVAL target:self selector:@selector(countdown) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    [self.timer fire];
    
    CoopenImage *coopenImage = (CoopenImage*)[self.model.coopenImages firstObject];
    self.advertiseImageView.image = coopenImage.image;
}

- (void)configUI {
    NSString *languageCurrent = [VLSUtily currentLanguageString];
    if ([languageCurrent isEqualToString:@"zh-hant"]) {
        //繁体
        self.sloganImageView.image = [UIImage imageNamed:@"icon_slogan_tr"];
    }else {
        self.sloganImageView.image = [UIImage imageNamed:@"icon_slogan_sm"];
    }
}

- (void)countdown {
    if (BEGIN--) {
        NSString *str = [NSString stringWithFormat:@"%ds %@",BEGIN + 1, NSLocalizedString(@"skip", nil)];
        [self.countBtn setTitle:str forState:UIControlStateNormal];
    }else  {
        [self.timer invalidate];
        self.timer = nil;
        self.block();
    }
}

- (void)adDidClicked {
    
}

- (IBAction)countDownBtnDidClicked:(UIButton *)sender {
    if (self.skipBlock) {
        [self.timer invalidate];
        self.timer = nil;
        self.skipBlock();
    }
}

+ (AdvertisementModel*)getSplashData {
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[self.class getSplashPath]];
}

+ (BOOL)storeSplashData:(AdvertisementModel*)model {
    BOOL res = [NSKeyedArchiver archiveRootObject:model toFile:[self.class getSplashPath]];
    return  res;
    return [NSKeyedArchiver archiveRootObject:model toFile:[self.class getSplashPath]];
}

+ (NSString*)getSplashPath {
    NSString *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject;
    NSString *pathStr = [path stringByAppendingString:@"/splash.data"];
    return pathStr;
}

@end
