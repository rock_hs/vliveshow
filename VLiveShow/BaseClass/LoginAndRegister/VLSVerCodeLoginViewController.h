//
//  VLSVerCodeLoginViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"
#import "VLSTextField.h"
#import "AccountManager.h"
#import "VLSMessageManager.h"
#import "SectionsViewController.h"
#import "VLSPhoneRegisterViewController.h"
#import "VLSNavigationController.h"
#import "VLSCountryView.h"

@interface VLSVerCodeLoginViewController : VLSBaseViewController<SecondViewControllerDelegate, VLSCountryViewDelegate>

@property (nonatomic, strong)VLSCountryView *countryView;

/// 从上个页面穿过来的手机号
@property (nonatomic, copy)NSString *phoneNumber;
/// 验证码输入框
@property (nonatomic, strong)VLSTextField *verCodeTF;
/// 手机号输入框
@property (nonatomic, strong)VLSTextField *phoneTF;
/// 小手机
@property (nonatomic, strong)UIImageView *phoneIcon;
/// 登录按钮
@property (nonatomic, strong)UIButton *loginButton;
/// 选择国家
@property (nonatomic, strong)UILabel *countryLabel;
/// 去除空格后的手机号
@property (nonatomic, strong)NSString *phoneNum;
/// 警告图标
@property (nonatomic, strong)UIImageView *warningImageView;
/// 警告的提示语
@property (nonatomic, strong)UILabel *warningLabel;
/// 临时字符串
@property (nonatomic, copy)NSString *temStr;
/// 是否已经发送了验证码
@property (nonatomic, assign)BOOL isSend;
/// 标记是否打开国家区号控制器
@property (nonatomic, assign)BOOL flag;
/// 是否验证过手机号存在的标记
@property (nonatomic, assign)BOOL phoneExistFlag;

- (void)showWarningWithString:(NSString *)str;
- (void)hideWarning;
- (void)phoneExists;
- (void)phoneExistsOnly;

@end
