//
//  VLSUserInfoViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

/************************ 设置用户信息界面 UI *****************************/

#import "VLSUserInfoViewController.h"
#import "VLSPhotoSelectView.h"
#import "AccountManager.h"
#import "AppDelegate.h"
#import "VLSMessageManager.h"


@interface VLSUserInfoViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate, VLSPhotoSelectViewDelegate>

/// 照片选择器
@property (nonatomic, strong)UIImagePickerController *imagePicker;
/// 用户头像
@property (nonatomic, strong)UIImageView *iconImageView;
/// 用户昵称
@property (nonatomic, strong)UITextField *nickNameTF;
/// 男
@property (nonatomic, strong)UIButton *maleButton;
/// 女
@property (nonatomic, strong)UIButton *femaleButton;
/// 性别
@property (nonatomic, copy)NSString *gender;
/// 上分割线
@property (nonatomic, strong)UILabel *upLine;
/// 登录按钮
@property (nonatomic, strong)UIButton *loginButton;
/// 相册选取通用组件
@property (nonatomic, strong)VLSPhotoSelectView *photoSelectView;

@end

@implementation VLSUserInfoViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;

}
- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupUI];

}

- (void)setupUI {
    
    // 创建返回按钮
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"login_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout
    .topSpaceToView(self.view, 10)
    .leftSpaceToView(self.view, 0)
    .widthIs(40)
    .heightIs(50);
    
    // 创建用户头像 Imageview
    self.iconImageView = [[UIImageView alloc] initWithImage:ICON_PLACEHOLDER];
    self.iconImageView.userInteractionEnabled = YES;
    self.iconImageView.layer.cornerRadius = 40;
    self.iconImageView.clipsToBounds = YES;
    [self.view addSubview:self.iconImageView];
    self.iconImageView.sd_layout
    .topSpaceToView(self.view, 60)
    .centerXEqualToView(self.view)
    .widthIs(80)
    .heightIs(80);
    
    // 添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.iconImageView addGestureRecognizer:tap];
    
    
    UILabel *label = [[UILabel alloc] init];
    label.text = LocalizedString(@"LOGIN_SET_ICON");
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:13];
    label.textColor = [UIColor grayColor];
    [self.view addSubview:label];
    label.sd_layout
    .topSpaceToView(self.iconImageView, 10)
    .centerXEqualToView(self.view)
    .heightIs(20)
    .widthIs(100);
    
    // 昵称输入框
    self.nickNameTF = [[UITextField alloc] init];
    self.nickNameTF.tintColor = [UIColor grayColor];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"LOGIN_SET_NICKNAME")];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [str length])];
    self.nickNameTF.attributedPlaceholder = str;
    self.nickNameTF.textAlignment = NSTextAlignmentCenter;
    [self.nickNameTF addTarget:self action:@selector(nickNameTFAction) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:self.nickNameTF];
    self.nickNameTF.sd_layout
    .topSpaceToView(label, 60)
    .centerXEqualToView(self.view)
    .widthIs(200)
    .heightIs(20);
    
    // 创建分割线
    self.upLine = [[UILabel alloc] init];
    self.upLine.backgroundColor = [UIColor grayColor];
    [self.view addSubview:self.upLine];
    self.upLine.sd_layout
    .topSpaceToView(self.nickNameTF, 10)
    .rightSpaceToView(self.view, 20)
    .leftSpaceToView(self.view, 20)
    .heightIs(0.5);
    
    // 创建竖线
    UILabel *middleLine = [[UILabel alloc] init];
    middleLine.backgroundColor = [UIColor grayColor];
    [self.view addSubview:middleLine];
    middleLine.sd_layout
    .topSpaceToView(self.upLine, 10)
    .centerXEqualToView(self.view)
    .heightIs(30)
    .widthIs(0.5);
    
    // 创建性别 button
    self.maleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.maleButton setImage:[UIImage imageNamed:@"login_male_gray"] forState:UIControlStateNormal];
    [self.maleButton setImage:[UIImage imageNamed:@"login_male_blue"] forState:UIControlStateSelected];
    [self.maleButton addTarget:self action:@selector(maleAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.maleButton];
    self.maleButton.sd_layout
    .topSpaceToView(self.upLine, 15)
    .rightSpaceToView(middleLine, 60)
    .heightIs(20)
    .widthIs(50);
    
    self.femaleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.femaleButton setImage:[UIImage imageNamed:@"login_female_gray"] forState:UIControlStateNormal];
    [self.femaleButton setImage:[UIImage imageNamed:@"login_female_red"] forState:UIControlStateSelected];
    [self.femaleButton addTarget:self action:@selector(femaleAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.femaleButton];
    self.femaleButton.sd_layout
    .topSpaceToView(self.upLine, 15)
    .leftSpaceToView(middleLine, 60)
    .heightIs(20)
    .widthIs(50);
    
    // 创建分割线
    UILabel *downLine = [[UILabel alloc] init];
    downLine.backgroundColor = [UIColor grayColor];
    [self.view addSubview:downLine];
    downLine.sd_layout
    .topSpaceToView(self.maleButton, 20)
    .rightSpaceToView(self.view, 20)
    .leftSpaceToView(self.view, 20)
    .heightIs(0.5);
    
    // 创建下一步按钮
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginButton setTitle:LocalizedString(@"LOGIN_BUTTON_LOGIN") forState:UIControlStateNormal];
    self.loginButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [self nextActionDeny];
    self.loginButton.userInteractionEnabled = YES;
    [self.loginButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.loginButton];
    self.loginButton.sd_layout
    .topSpaceToView(downLine, 50)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(40);
    
}
#pragma mark - 返回按钮

- (void)backAction:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 选择性别
- (void)maleAction {
    self.gender = @"MALE";
    self.maleButton.selected = !self.maleButton.selected;
    self.femaleButton.selected = NO;
}
- (void)femaleAction {
    self.gender = @"FEMALE";
    self.femaleButton.selected = !self.femaleButton.selected;
    self.maleButton.selected = NO;
}

#pragma mark - 头像选择手势
- (void)tapAction {
    
    [self.view addSubview:self.photoSelectView];
    
    [self.photoSelectView showPhotoView:YES animation:YES];
}

#pragma mark - 点击登录

- (void)loginAction:(id)sender {
    
    if (self.nickNameTF.text.length > 0) {
        
        [self showProgressHUDWithStr:LocalizedString(@"ALERT_SAVEING") dimBackground:YES];
        [self uploadUserInfo];
        
    } else {
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PLACEHODER_NICKNAME")];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, [str length])];
        self.nickNameTF.attributedPlaceholder = str;
        self.upLine.backgroundColor = [UIColor redColor];
        
    }
    
}

#pragma mark - 上传用户信息

- (void)uploadUserInfo {
    
    NSString *nickName = @"";
    nickName = [NSString stringWithFormat:@"%@", self.nickNameTF.text];
    NSData *data = [NSData data];

    data = UIImageJPEGRepresentation(self.iconImageView.image, 0.2);
    PostFileModel *fileModel = [[PostFileModel alloc] init];
    fileModel.fileName = [NSUUID UUID].UUIDString;
    fileModel.fileArray = @[data];
    AccountModel *accountModel = [[AccountModel alloc] init];
    accountModel.nickName = nickName;
    accountModel.gender =  self.gender;
    accountModel.fileModel = fileModel;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.loadBlock = ^(id result){
        
    };
    callback.updateBlock = ^(id result){
        
        [self hideProgressHUDAfterDelay:0];
        [self success:result];
        [self getUserInfo];
        [[VLSMessageManager sharedManager] getSelfProfile];
        
        [((AppDelegate *)[UIApplication sharedApplication].delegate) showTabBarController];
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self hideProgressHUDAfterDelay:0];
            [self error:result];
        }
    };
    
    [AccountManager postUserProfile:accountModel callback:callback];
    
    
}


#pragma mark - VLSPhotoSelectViewDelegate

- (void)jumpToSystemPhotoAlbum {
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];

}

- (void)jumpToSystemCamera {
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void)closePhotoSelectView {
    [self.photoSelectView removeFromSuperview];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.photoSelectView removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
  
    UIImage *image = info[UIImagePickerControllerEditedImage];
    self.iconImageView.image = image;
    [self.photoSelectView removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
  
}

#pragma mark - UITextFieldDelegate 输入框判断

- (void)nickNameTFAction {
    if (self.nickNameTF.text.length > 0) {
        
        [self nextActionEnable];
        self.upLine.backgroundColor = [UIColor grayColor];
    } else {
        [self nextActionDeny];
    }
}


- (void)nextActionEnable {
    [self.loginButton setBackgroundColor:[UIColor redColor]];
    self.loginButton.layer.borderWidth = 0.;
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.loginButton.userInteractionEnabled = YES;
}

- (void)nextActionDeny {
    [self.loginButton setBackgroundColor:RGB16(COLOR_BG_FFFFFF)];
    self.loginButton.layer.cornerRadius = 20. / 375. * SCREEN_WIDTH;
    self.loginButton.layer.masksToBounds = YES;
    self.loginButton.layer.borderWidth = 1.;
    self.loginButton.layer.borderColor = [UIColor grayColor].CGColor;
    [self.loginButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    self.loginButton.userInteractionEnabled = NO;
}


#pragma mark - getters


- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        self.imagePicker = [[UIImagePickerController alloc] init];
        self.imagePicker.delegate = self;
        self.imagePicker.allowsEditing = YES;
    }
    return _imagePicker;
}

-(UIView *)photoSelectView{
    
    if (!_photoSelectView) {
        
        _photoSelectView = [[VLSPhotoSelectView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _photoSelectView.delegate = self;
        _photoSelectView.photoLabel.text = LocalizedString(@"LOGIN_SET_ICON");
        [_photoSelectView showPhotoView:NO animation:NO];
        
    }
    return _photoSelectView;
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.nickNameTF resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
