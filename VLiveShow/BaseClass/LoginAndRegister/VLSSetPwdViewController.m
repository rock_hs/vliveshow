//
//  VLSSetPwdViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

/************************ 设置密码界面 UI *****************************/

#import "VLSSetPwdViewController.h"

#import "VLSUserInfoViewController.h"
#import "VLSTextField.h"
#import "AccountManager.h"

@interface VLSSetPwdViewController ()<UITextFieldDelegate>

/// 密码输入框
@property (nonatomic, strong)VLSTextField *pwdTextField;
/// 下一步按钮
@property (nonatomic, strong)UIButton *nextButton;

@end

@implementation VLSSetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = LocalizedString(@"LOGIN_SET_PWD");
    self.view.backgroundColor = [UIColor whiteColor];
   
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home_back"] style:UIBarButtonItemStylePlain target:self action:@selector(leftItemAction:)];
    self.navigationItem.leftBarButtonItem = leftItem;

    [self setupUI];
    

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNavigationBarRedMethod];
    self.nextButton.userInteractionEnabled = YES;
    self.navigationController.navigationBarHidden = NO;
}


- (void)setupUI {
    // 创建提示 Label
    UILabel *pwdLabel = [[UILabel alloc] init];
    pwdLabel.text = LocalizedString(@"LOGIN_SET_PWD_WARING");
    pwdLabel.font = [UIFont systemFontOfSize:14];
    pwdLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:pwdLabel];
    pwdLabel.sd_layout
    .topSpaceToView(self.view, 30)
    .centerXEqualToView(self.view)
    .widthIs(200)
    .heightIs(30);
    
    // 创建密码输入框的右侧视图
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(0, 0, 40, 20)];
    [rightButton setImage:[UIImage imageNamed:@"login_eyes_closed"] forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"login_eyes_open"] forState:UIControlStateSelected];
    [rightButton addTarget:self action:@selector(eyesAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    // 设置密码输入框
    self.pwdTextField = [[VLSTextField alloc] init];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
    self.pwdTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PLACEHODER_PWD") attributes:attributes];
    self.pwdTextField.rightView = rightButton;
    self.pwdTextField.rightViewMode = UITextFieldViewModeAlways;
    [self.pwdTextField addTarget:self action:@selector(pwdTextFieldAction) forControlEvents:UIControlEventEditingChanged];
    self.pwdTextField.delegate = self;
    self.pwdTextField.secureTextEntry = YES;
    self.pwdTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.pwdTextField.keyboardType = UIKeyboardTypeASCIICapable;
    self.pwdTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.view addSubview:self.pwdTextField];
    self.pwdTextField.sd_layout
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(pwdLabel, 30)
    .heightIs(40);
    
    
    // 创建下一步按钮
    self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.nextButton setTitle:LocalizedString(@"LOGIN_BUTTON_NEXT") forState:UIControlStateNormal];
    self.nextButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
    [self nextActionDeny];
    [self.nextButton addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.nextButton];
    self.nextButton.sd_layout
    .topSpaceToView(self.pwdTextField, 100)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(40);
    
    
}

- (void)leftItemAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)eyesAction:(id)sender{
    
    UIButton *button = sender;
    
    self.pwdTextField.secureTextEntry = button.selected;

    button.selected = !button.selected;
    [self.pwdTextField endEditing:YES];
    
}

- (void)nextAction:(id)sender {
    
    self.nextButton.userInteractionEnabled = NO;
    
    AccountModel *model = [[AccountModel alloc] init];
    
    model.password = self.pwdTextField.text;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
//            [self success:result];
            VLSUserInfoViewController *userInfoVC = [[VLSUserInfoViewController alloc] init];
            [self.navigationController pushViewController:userInfoVC animated:YES];
        }
        
    };
    callback.errorBlock = ^(id result){
        
        self.nextButton.userInteractionEnabled = YES;

        if ([result isKindOfClass:[ManagerEvent class]]) {
            
//            [self error:result];
            
        }
        
    };
    [AccountManager setNewPsd:model callback:callback];
    
}


- (void)pwdTextFieldAction {
    

    if (self.pwdTextField.text.length > 5 && self.pwdTextField.text.length < 21) {
        [self nextActionEnable];
        
    } else {
    
        [self nextActionDeny];
        
    }
    
    self.pwdTextField.text = [self.pwdTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];


}

- (void)nextActionEnable {
    [self.nextButton setBackgroundColor:[UIColor redColor]];
    self.nextButton.layer.borderWidth = 0.;
    [self.nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.nextButton.userInteractionEnabled = YES;
}

- (void)nextActionDeny {
    [self.nextButton setBackgroundColor:RGB16(COLOR_BG_FFFFFF)];
    self.nextButton.layer.cornerRadius = 20. / 375. * SCREEN_WIDTH;
    self.nextButton.layer.masksToBounds = YES;
    self.nextButton.layer.borderWidth = 1.;
    self.nextButton.layer.borderColor = [UIColor grayColor].CGColor;
    [self.nextButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    self.nextButton.userInteractionEnabled = NO;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.pwdTextField)
    {
        if (textField.secureTextEntry)
        {
            [textField insertText:self.pwdTextField.text];
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.pwdTextField endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
