//
//  VLSSetNewPwdViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/6/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"

@class AccountModel;

@interface VLSSetNewPwdViewController : VLSBaseViewController

/// 从上个页面拿到手机号、区号、验证码
@property (nonatomic, strong)AccountModel *model;

@end
