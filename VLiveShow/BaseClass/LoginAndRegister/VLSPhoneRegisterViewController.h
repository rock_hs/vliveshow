//
//  VLSPhoneRegisterViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"

@interface VLSPhoneRegisterViewController : VLSBaseViewController

/// 从上个页面传过来的手机号
@property (nonatomic, copy)NSString *phoneNumber;

@end
