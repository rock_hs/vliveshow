//
//  VLSSetNewPwdViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/6/15.
//  Copyright © 2016年 vliveshow. All rights reserved.
//
/************************ 设置新密码界面 UI *****************************/

#import "VLSSetNewPwdViewController.h"
#import "VLSTextField.h"
#import "AccountManager.h"
#import "AppDelegate.h"

@interface VLSSetNewPwdViewController ()<UITextFieldDelegate>

@property (nonatomic, strong)VLSTextField *passwordTF;
@property (nonatomic, strong)UIButton *loginButton;

@property (nonatomic, strong)UIImageView *warningImageView;
@property (nonatomic, strong)UILabel *warningLabel;

@end

@implementation VLSSetNewPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = LocalizedString(@"LOGIN_SET_NEW_PWD");
    
    
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNavigationBarRedMethod];
    self.navigationItem.leftBarButtonItem = nil;
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationItem.backBarButtonItem setTitle:@""];
}

- (void)setupUI {
    
    // 创建密码输入框的右侧视图
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(0, 0, 40, 20)];
    [rightButton setImage:[UIImage imageNamed:@"login_eyes_closed"] forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"login_eyes_open"] forState:UIControlStateSelected];
    [rightButton addTarget:self action:@selector(eyesAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // 创建密码输入框
    self.passwordTF = [[VLSTextField alloc] init];
    self.passwordTF.rightView = rightButton;
    self.passwordTF.rightViewMode = UITextFieldViewModeAlways;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
    self.passwordTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PLACEHODER_PWD") attributes:attributes];
    [self.passwordTF addTarget:self action:@selector(pwdTextFieldAction) forControlEvents:UIControlEventEditingChanged];
    self.passwordTF.delegate = self;
    self.passwordTF.secureTextEntry = YES;
    self.passwordTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.passwordTF.keyboardType = UIKeyboardTypeASCIICapable;
    self.passwordTF.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.view addSubview:self.passwordTF];
    self.passwordTF.sd_layout
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(self.view, 30)
    .heightIs(40);
    
    // 创建下一步按钮
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginButton setTitle:LocalizedString(@"LOGIN_BUTTON_SUBMIT_LOGIN") forState:UIControlStateNormal];
    self.loginButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
    [self nextActionDeny];
    [self.loginButton addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    self.loginButton.userInteractionEnabled = NO;
    [self.view addSubview:self.loginButton];
    self.loginButton.sd_layout
    .topSpaceToView(self.passwordTF, 100)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(40);
    

}


#pragma mark - 显示警告
- (void)showWarningWithString:(NSString *)str {
    
    self.warningLabel.text = str;
    
    [self.view addSubview:self.warningLabel];
    [self.warningLabel setSingleLineAutoResizeWithMaxWidth:SCREEN_WIDTH];
    self.warningLabel.sd_layout
    .centerXEqualToView(self.view)
    .bottomSpaceToView(self.loginButton, 30)
    .heightIs(30);
    
    [self.view addSubview:self.warningImageView];
    self.warningImageView.sd_layout
    .rightSpaceToView(self.warningLabel, 5)
    .centerYEqualToView(self.warningLabel)
    .widthIs(15)
    .heightIs(15);
    
}

#pragma mark - 隐藏警告
- (void)hideWarning {
    [self.warningLabel removeFromSuperview];
    [self.warningImageView removeFromSuperview];
}


- (void)eyesAction:(id)sender{
    
    UIButton *button = sender;
    self.passwordTF.secureTextEntry = button.selected;
    button.selected = !button.selected;
    [self.passwordTF endEditing:YES];
    
}

#pragma mark - 提交并登陆

- (void)loginAction {
    
    if (self.passwordTF.text.length < 6) {
        [self showWarningWithString:LocalizedString(@"LOGIN_PAGE_PWD_SHORT")];
        self.passwordTF.lineLabel.backgroundColor = [UIColor redColor];
        self.passwordTF.textColor = [UIColor redColor];
        return;
    }
    
    self.model.password = self.passwordTF.text;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        

        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"PASSWORD_LOGIN_AGAIN") preferredStyle:UIAlertControllerStyleAlert];
        // 确定按钮
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"LOGIN_AGAIN") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [((AppDelegate *)[UIApplication sharedApplication].delegate) showLoginController];

        }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    [AccountManager setNewPsd:self.model callback:callback];
}


- (void)pwdTextFieldAction {
    
    if (self.passwordTF.textColor == [UIColor redColor]) {
        self.passwordTF.textColor = [UIColor blackColor];
        self.passwordTF.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        [self hideWarning];
    }
    
   
    if (self.passwordTF.text.length > 5 && self.passwordTF.text.length < 21) {
        [self nextActionEnable];
        
    } else {
        
        [self nextActionDeny];
        
    }
    
    self.passwordTF.text = [self.passwordTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];

    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.passwordTF)
    {
        if (textField.secureTextEntry)
        {
            [textField insertText:self.passwordTF.text];
        }
    }
}

- (void)nextActionEnable {
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.loginButton setBackgroundColor:[UIColor redColor]];
    self.loginButton.layer.borderWidth = 0.;
    self.loginButton.userInteractionEnabled = YES;
}

- (void)nextActionDeny {
    [self.loginButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.loginButton setBackgroundColor:RGB16(COLOR_BG_FFFFFF)];
    self.loginButton.layer.cornerRadius = 20. / 375. * SCREEN_WIDTH;
    self.loginButton.layer.borderWidth = 1.;
    self.loginButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.loginButton.layer.masksToBounds = YES;
    self.loginButton.userInteractionEnabled = NO;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.passwordTF endEditing:YES];
}

#pragma mark - getters

- (UIImageView *)warningImageView {
    if (!_warningImageView) {
        
        self.warningImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"livebefore_warn"]];
        
    }
    return _warningImageView;
}

- (UILabel *)warningLabel {
    if (!_warningLabel) {
        self.warningLabel = [[UILabel alloc] init];
        self.warningLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.warningLabel.textColor = [UIColor redColor];
        self.warningLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _warningLabel;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
