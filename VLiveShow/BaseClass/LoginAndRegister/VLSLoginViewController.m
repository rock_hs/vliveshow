//
//  VLSLoginViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/5/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

/************************ 登录欢迎页界面 UI *****************************/

#import "VLSLoginViewController.h"
#import "VLSPhoneLoginViewController.h"
#import "VLSPhoneRegisterViewController.h"
#import "UIView+SDAutoLayout.h"
#import "AppDelegate.h"
#import "AccountManager.h"
#import "WeiboUser.h"
#import "VLSServiceViewController.h"
#import "MBProgressHUD+Add.h"
#import "VLiveShow-Swift.h"

// 每个登录按钮的宽度
#define kButtonWidth SCREEN_WIDTH / 10

@interface VLSLoginViewController ()
{
    UIImageView *backgroundImageView;
    UIImageView *bgView;
    CGRect forebackImgViewFrame;
    CGRect foreBgViewFrame;
}

@end

@implementation VLSLoginViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = YES;
    backgroundImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT);
    bgView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT);
    [self startFirstAnimation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appHasGoneInForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appHasGoneInBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
   
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [bgView.layer removeAllAnimations];
    [backgroundImageView.layer removeAllAnimations];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

}

- (void)appHasGoneInForeground {
    [self startFirstAnimation];
}
- (void)appHasGoneInBackground {
    [bgView.layer removeAllAnimations];
    [backgroundImageView.layer removeAllAnimations];
    backgroundImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT);
    bgView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT);
}
- (void)startFirstAnimation {

    [UIView animateWithDuration:15 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        backgroundImageView.frame = CGRectMake(0, -0.8 * SCREEN_HEIGHT, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT);
    } completion:^(BOOL finished) {
        if (finished) {
            [self startSecondAnimation];

        }

    }];
    
}

- (void)startSecondAnimation {
    
    [UIView animateWithDuration:15 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        backgroundImageView.frame = CGRectMake(0, -1.8 * SCREEN_HEIGHT, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT);
        bgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT);
    } completion:^(BOOL finished) {
        if (finished) {
            backgroundImageView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT);
            UIImageView *tempImgView = [[UIImageView alloc] init];
            tempImgView = bgView;
            bgView = backgroundImageView;
            backgroundImageView = tempImgView;
            [self startFirstAnimation];
        }
    }];
  
}

#pragma mark - 配置 UI 界面

- (void)configUI {

    
    // 创建背景图
    backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT)];
    backgroundImageView.userInteractionEnabled = YES;
    backgroundImageView.image = [UIImage imageNamed:@"iPhone6"];
    [self.view addSubview:backgroundImageView];
    
    bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 1.8 * SCREEN_HEIGHT)];
    bgView.userInteractionEnabled = YES;
    bgView.image = [UIImage imageNamed:@"iPhone6"];
    [self.view addSubview:bgView];
    
    // 创建协议按钮
    UIButton *protocolButton = [UIButton buttonWithType:UIButtonTypeCustom];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"PROTOCAL_BUTTON")];
    [str addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(8, [str length] - 8)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [str length])];
    [protocolButton setAttributedTitle:str forState:UIControlStateNormal];
    protocolButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [protocolButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    protocolButton.tag = 1001;
    [self.view addSubview:protocolButton];
    protocolButton.sd_layout
    .bottomSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 80)
    .leftSpaceToView(self.view, 80)
    .heightIs(20);
    
    // 创建 Sina 按钮
    UIButton *sinaButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sinaButton setImage:[UIImage imageNamed:@"login_sina_white"] forState:UIControlStateNormal];
    [sinaButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    sinaButton.tag = 1002;
    sinaButton.highlighted = NO;
    [self.view addSubview:sinaButton];
    sinaButton.sd_layout
    .bottomSpaceToView(protocolButton, 10)
    .centerXEqualToView(self.view)
    .widthIs(kButtonWidth)
    .heightIs(kButtonWidth);
    
    // 创建 WeChat 按钮
    UIButton *weCahtButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [weCahtButton setImage:[UIImage imageNamed:@"login_wechat_white"] forState:UIControlStateNormal];
    [weCahtButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    weCahtButton.tag = 1003;
    weCahtButton.highlighted = NO;
    [self.view addSubview:weCahtButton];
    weCahtButton.sd_layout
    .bottomSpaceToView(protocolButton, 10)
    .rightSpaceToView(sinaButton, 30)
    .heightIs(kButtonWidth)
    .widthIs(kButtonWidth);
    
    // 创建 QQ 按钮
    UIButton *qqButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [qqButton setImage:[UIImage imageNamed:@"login_qq_white"] forState:UIControlStateNormal];
    [qqButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    qqButton.tag = 1004;
    qqButton.highlighted = NO;
    [self.view addSubview:qqButton];
    qqButton.sd_layout
    .bottomSpaceToView(protocolButton, 10)
    .leftSpaceToView(sinaButton, 30)
    .widthIs(kButtonWidth)
    .heightIs(kButtonWidth);
    
    
    // 创建登录方式的 Label
    UILabel *loginMethodLabel = [[UILabel alloc] init];
    loginMethodLabel.text = LocalizedString(@"LOGIN_WAY");
    loginMethodLabel.font = [UIFont systemFontOfSize:12];
    loginMethodLabel.textColor = [UIColor whiteColor];
    loginMethodLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:loginMethodLabel];
    loginMethodLabel.sd_layout
    .bottomSpaceToView(sinaButton, 10)
    .centerXEqualToView(self.view)
    .widthIs(100)
    .heightIs(30);
    
    // 创建登录方式左侧的横线
    UILabel *leftLine = [[UILabel alloc] init];
    leftLine.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:leftLine];
    leftLine.sd_layout
    .centerYEqualToView(loginMethodLabel)
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(loginMethodLabel, 10)
    .heightIs(0.3);
    
    // 创建登录方式右侧的横线
    UILabel *rightLine = [[UILabel alloc] init];
    rightLine.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:rightLine];
    rightLine.sd_layout
    .centerYEqualToView(loginMethodLabel)
    .leftSpaceToView(loginMethodLabel, 10)
    .rightSpaceToView(self.view, 30)
    .heightIs(0.3);
    
    
    // 创建注册按钮
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [registerButton setTitle:LocalizedString(@"LOGIN_PAGE_REGISTER") forState:UIControlStateNormal];
    registerButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [registerButton setBackgroundImage:[UIImage imageNamed:@"button_write"] forState:UIControlStateNormal];
    [registerButton setBackgroundImage:[UIImage imageNamed:@"button_write"] forState:UIControlStateHighlighted];
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [registerButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    registerButton.highlighted = NO;
    registerButton.tag = 1006;
    [self.view addSubview:registerButton];
    registerButton.sd_layout
    .bottomSpaceToView(loginMethodLabel, 70 / 667. *SCREEN_HEIGHT)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(kButtonWidth);
    
    // 创建手机号登录按钮
    UIButton *phoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [phoneButton setTitle:LocalizedString(@"LOGIN_PAGE_PHONELOGIN") forState:UIControlStateNormal];
    phoneButton.titleLabel.font = [UIFont systemFontOfSize:15];

    [phoneButton setBackgroundColor:[UIColor redColor]];
    [phoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [phoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    phoneButton.layer.cornerRadius = 20. / 375. * SCREEN_WIDTH;
    phoneButton.layer.masksToBounds = YES;
    [phoneButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    phoneButton.tag = 1005;
    phoneButton.highlighted = NO;
    [self.view addSubview:phoneButton];
    phoneButton.sd_layout
    .bottomSpaceToView(registerButton, 20)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(kButtonWidth);
    
    // 创建LOGO背景图
    UIImageView *logoImageView = [[UIImageView alloc] init];
    logoImageView.contentMode = UIViewContentModeCenter;
    logoImageView.userInteractionEnabled = YES;
    logoImageView.image = [UIImage imageNamed:@"logo@2x"];
    [self.view addSubview:logoImageView];
    logoImageView.sd_layout
    .centerXEqualToView(self.view)
    .bottomSpaceToView(phoneButton, 100 / 667. * SCREEN_HEIGHT)
    .widthIs(80)
    .heightIs(150);
    
}



#pragma mark - 按钮点击方法

- (void)loginAction:(UIButton *)button {
    
    switch (button.tag) {
            // 协议按钮
        case 1001:
        {
            VLSServiceViewController *serviceVC = [[VLSServiceViewController alloc] init];
            [self.navigationController pushViewController:serviceVC animated:YES];
            
        }
            break;
            // Sina 按钮
        case 1002:
        {
            [self login:UMSocialPlatformType_Sina];
        }
            break;
            // WeChat 按钮
        case 1003:
        {
            [self login:UMSocialPlatformType_WechatSession];
        }
            break;
            // QQ 按钮
        case 1004:
        {
            [self login:UMSocialPlatformType_QQ];
        }
            break;
            // 手机号登录按钮
        case 1005:
        {
            VLSPhoneLoginViewController *phoneLoginVC = [[VLSPhoneLoginViewController alloc] init];
            [self.navigationController pushViewController:phoneLoginVC animated:YES];
        }
            break;
            // 注册按钮
        case 1006:
        {
            VLSPhoneRegisterViewController *phoneRegisterVC = [[VLSPhoneRegisterViewController alloc] init];
            [self.navigationController pushViewController:phoneRegisterVC animated:YES];
        }
            break;
            
    }
    
}

#pragma mark - 登录的回调方法

- (void)login:(UMSocialPlatformType)platformType {
    [VLSShareManager getAuthWithUserInfo:platformType complete: ^(UMSocialUserInfoResponse* item) {
        NSString* platform;
        switch (platformType) {
            case UMSocialPlatformType_Sina:
                platform = @"WB";
                break;
            case UMSocialPlatformType_WechatSession:
                platform = @"WX";
                break;
            case UMSocialPlatformType_QQ:
                platform = @"QQ";
                break;
            default:
                break;
        }
        NSString* openid = item.openid != nil ? item.openid : item.uid;
        [self postThirdInfoToServerWithThirdpartyPlatform:platform thirdpartyAccount:openid thirdpartyIconUrl:item.iconurl gender:item.gender nickName:item.name];
    }];
}

#pragma mark - 将第三方登录信息回传服务器

- (void)postThirdInfoToServerWithThirdpartyPlatform:(NSString *)platform thirdpartyAccount:(NSString *)account thirdpartyIconUrl:(NSString *)iconUrl gender:(NSString *)gender nickName:(NSString *)nickName {
    __weak typeof(self) weakSelf = self;
    AccountModel *model = [[AccountModel alloc] init];
    model.thirdpartyPlatform = platform;
    model.thirdpartyAccount = account;
    model.thirdpartyIconUrl = iconUrl;
    model.gender = gender;
    model.nickName = nickName;
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        //        [self getUserInfo];
        if (![[VLSMessageManager sharedManager] GetLoginState]) {
            [[VLSMessageManager sharedManager] IMLogin];//重连IM
        }
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self success:result];
        }
        // 登录成功，跳转到直播列表
        [((AppDelegate *)[UIApplication sharedApplication].delegate) showTabBarController];
                
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [weakSelf error:result];
        }
    };
    
    [AccountManager thirdPlatformLogin:model callback:callback];
    
}


#pragma mark - 获取心跳信息配置
- (void)getHeartConfig {
    __weak typeof(self) weakSelf = self;
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [weakSelf error:result];
        }
    };
    [AccountManager getHeartConfigCallback:callback];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


- (void)dealloc
{
    
}

@end
