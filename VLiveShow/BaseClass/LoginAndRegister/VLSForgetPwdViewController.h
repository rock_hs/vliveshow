//
//  VLSForgetPwdViewController.h
//  VLiveShow
//
//  Created by Cavan on 16/6/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "VLSBaseViewController.h"

@class VLSTextField;

@interface VLSForgetPwdViewController : VLSBaseViewController

/// 从上个页面穿过来的手机号
@property (nonatomic, copy)NSString *phoneNumber;

@end
