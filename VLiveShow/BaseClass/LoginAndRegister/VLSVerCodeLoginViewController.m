//
//  VLSVerCodeLoginViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/6/14.
//  Copyright © 2016年 vliveshow. All rights reserved.
//
/****************验证码登录界面 UI *********************/

#import "VLSVerCodeLoginViewController.h"
#import "VLSCountryAreaManager.h"
#import "AppDelegate.h"

@interface VLSVerCodeLoginViewController ()

@end

@implementation VLSVerCodeLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = LocalizedString(@"LOGIN_LABEL_CERCODE_LOGIN_OTHER");
       
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home_back"] style:UIBarButtonItemStylePlain target:self action:@selector(leftItemAction:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    [self setupUI];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNavigationBarRedMethod];
    if (self.phoneTF.text.length) {
        
        NSString *temStr = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *firstChar = [self.phoneTF.text substringToIndex:1];
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
            if ([firstChar isEqualToString:@"0"]) {
                if (temStr.length == 10) {
                    [self phoneExistsOnly];
                }
            } else {
                if (temStr.length == 9) {
                    [self phoneExistsOnly];
                }
            }
        }
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
            if (temStr.length == 11) {
                [self phoneExistsOnly];
            }
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated{

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

#pragma mark - 点击导航栏左侧返回按钮

- (void)leftItemAction:(id)sender {
    
    if (self.verCodeTF.text.length == 0 && self.isSend) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
        // 确定按钮
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }];
        // 等待按钮
        UIAlertAction *waitAction = [UIAlertAction actionWithTitle:LocalizedString(@"WAIT") style:UIAlertActionStyleDestructive handler:nil];
        
        NSString *message = [NSString stringWithFormat:@"%@\n%@", LocalizedString(@"ALERT_MESSAGE_VERCODE_DEALY"), LocalizedString(@"ALERT_MESSAGE_VERCODE_DEALY_RESEND")];
        NSMutableAttributedString *alertMessageStr = [[NSMutableAttributedString alloc] initWithString:message];
        [alertMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:SIZE_FONT_16] range:NSMakeRange(0, alertMessageStr.length)];
        [alertMessageStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, alertMessageStr.length)];
        [alertController setValue:alertMessageStr forKey:@"attributedMessage"];
        
        [okAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        [alertController addAction:okAction];
        [alertController addAction:waitAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}

- (void)setupUI {
    
    [self.view addSubview:self.countryView];
    self.countryView.sd_layout
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(self.view, 20)
    .heightIs(40);
    
   
    [self.view addSubview:self.phoneTF];
    self.phoneTF.sd_layout
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(self.countryView, 20)
    .heightIs(40);
    
    
   
    [self.view addSubview:self.verCodeTF];
    self.verCodeTF.sd_layout
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(self.phoneTF, 20)
    .heightIs(40);
    
    // 创建登录按钮
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginButton setTitle:LocalizedString(@"LOGIN_PAGE_LOGIN") forState:UIControlStateNormal];
    self.loginButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [self nextActionDeny];
    [self.loginButton addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.loginButton];
    self.loginButton.sd_layout
    .topSpaceToView(self.verCodeTF, Y_CONVERT(75))
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(40);
    
    
}

#pragma mark - VLSCountryViewDelegate

-(void)selectCountryArea {
    _flag = YES;
    SectionsViewController *stVC = [SectionsViewController new];
    stVC.delegate = self;
    [self.navigationController pushViewController:stVC animated:YES];

}

#pragma mark - 显示警告
- (void)showWarningWithString:(NSString *)str {
    
    self.warningLabel.text = str;
    
    [self.view addSubview:self.warningLabel];
    [self.warningLabel setSingleLineAutoResizeWithMaxWidth:SCREEN_WIDTH];
    self.warningLabel.sd_layout
    .centerXEqualToView(self.view)
    .bottomSpaceToView(self.loginButton, 30)
    .heightIs(30);
    
    [self.view addSubview:self.warningImageView];
    self.warningImageView.sd_layout
    .rightSpaceToView(self.warningLabel, 5)
    .centerYEqualToView(self.warningLabel)
    .widthIs(15)
    .heightIs(15);
    
}

#pragma mark - 隐藏警告
- (void)hideWarning {
    [self.warningLabel removeFromSuperview];
    [self.warningImageView removeFromSuperview];
}


#pragma mark - 验证按钮方法

/**
 *  业务逻辑：
 *  1、判断手机号位数是否正确，不正确给出提示
 *  2、如果正确，发送验证码按钮开始倒计时并进行网络请求，弹出验证码
 */

- (void)verCodeAction:(id)sender {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
  
    
    if (![self checkVerCodeBtnEnableAndShowPhoneWarning]) return;

  
    
    // 验证码倒计时
    ManagerEvent *event = [ManagerEvent toast:LocalizedString(@"LOGIN_ALERT_VERCODE_SEND") title:nil];
    [self error:event];
    [self verCodeTime:sender];
    
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager sendVerCode:model callback:callback];
    
    
}


- (BOOL)checkVerCodeBtnEnableAndShowPhoneWarning {
    
    BOOL flag = YES;
    
    if (self.phoneTF.text.length == 0) {
        
        [self showPhoneInputWrong];
        flag = NO;
        
    }
    
    if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
        if (self.phoneNum.length != 11) {
            
            [self showPhoneInputWrong];
            flag = NO;
        }
    } else if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
        if (self.phoneTF.text.length) {
            
            NSString *temStr = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *firstChar = [self.phoneTF.text substringToIndex:1];
            if ([firstChar isEqualToString:@"0"]) {
                if (temStr.length != 10) {
                    [self showPhoneInputWrong];
                    flag = NO;
                }
            } else {
                if (temStr.length != 9) {
                    
                    [self showPhoneInputWrong];
                    flag = NO;
                }
            }
        }
    } else {
        if (self.phoneNum.length < 6) {
            [self showPhoneInputWrong];
            flag = NO;
        }
    }
    return flag;
}

- (void)showPhoneInputWrong {
    self.phoneTF.lineLabel.backgroundColor = [UIColor redColor];
    self.phoneTF.textColor = [UIColor redColor];
    [self showWarningWithString:LocalizedString(@"ALERT_MESSAGE_RIGHT_PHONENUM")];
}


#pragma mark - 点击发送验证码后的倒计时方法

- (void)verCodeTime:(id)sender {
    self.isSend = YES;
    UIButton *verCodeButton = sender;
    __block int timeout=VERCODE_TIME; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){
            //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [verCodeButton setTitle:LocalizedString(@"VERCODE_SEND_AGAIN") forState:UIControlStateNormal];
                [verCodeButton setTitleColor:RGB16(COLOR_FONT_FF1130) forState:UIControlStateNormal];
                verCodeButton.userInteractionEnabled = YES;
                self.phoneIcon.image = [UIImage imageNamed:@"phone_red"];
            });
        }else{
            int seconds = timeout % VERCODE_TIME;
            NSString *strTime = [NSString stringWithFormat:@"%d", seconds];
            if ([strTime isEqualToString:@"0"]) {
                strTime = [NSString stringWithFormat:@"%d",VERCODE_TIME];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1];
                [verCodeButton setTitle:[NSString stringWithFormat:@"%@s%@",strTime, LocalizedString(@"VERCODE_SEND_LATER")] forState:UIControlStateNormal];
                [verCodeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                self.phoneIcon.image = [UIImage imageNamed:@"phone_gray"];
                [UIView commitAnimations];
                verCodeButton.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

#pragma mark - 验证手机号是否存在

- (void)phoneExists {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.phoneExistFlag = YES;
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if (![[result objectForKey:@"existPhone"] boolValue]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"ALERT_MESSAGE_GO_REGISTER") preferredStyle:UIAlertControllerStyleAlert];
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"LOGIN_PAGE_REGISTER") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                [self.phoneTF endEditing:YES];
                [self.verCodeTF endEditing:YES];
                
                VLSPhoneRegisterViewController *phoneRegisterVC = [[VLSPhoneRegisterViewController alloc] init];
                [self.navigationController pushViewController:phoneRegisterVC animated:YES];
                
            }];
            // 取消按钮
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleCancel handler:nil];
            [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
            
            [alertController addAction:okAction];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            [self loginCallBack];
        }
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager phoneExists:model callback:callback];
}

- (void)phoneExistsOnly {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.phoneExistFlag = YES;
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if (![[result objectForKey:@"existPhone"] boolValue]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"ALERT_MESSAGE_GO_REGISTER") preferredStyle:UIAlertControllerStyleAlert];
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"LOGIN_PAGE_REGISTER") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                [self.phoneTF endEditing:YES];
                [self.verCodeTF endEditing:YES];
                
                VLSPhoneRegisterViewController *phoneRegisterVC = [[VLSPhoneRegisterViewController alloc] init];
                phoneRegisterVC.phoneNumber = self.phoneTF.text;
                [self.navigationController pushViewController:phoneRegisterVC animated:YES];
                
            }];
            // 取消按钮
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleCancel handler:nil];
            [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
            
            [alertController addAction:okAction];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    };
    callback.errorBlock = ^(id result){
        self.phoneExistFlag = NO;
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager phoneExists:model callback:callback];
}

#pragma mark - 登录按钮

- (void)loginAction {
    
    
    if (self.phoneNum.length != 11 && [self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
        
        self.phoneTF.lineLabel.backgroundColor = [UIColor redColor];
        self.phoneTF.textColor = [UIColor redColor];
        [self showWarningWithString:LocalizedString(@"ALERT_MESSAGE_RIGHT_PHONENUM")];
        
        
        return;
        
    }
    
    if (self.phoneExistFlag) {
        [self loginCallBack];
    } else {
    [self phoneExists];
    }
}

- (void)loginCallBack {
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.verificationCode = self.verCodeTF.text;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self success:result];
        }
        
        //        [self getUserInfo];
        
        if (![[VLSMessageManager sharedManager] GetLoginState]) {
            [[VLSMessageManager sharedManager] IMLogin];//重连IM
        }
        
        [((AppDelegate *)[UIApplication sharedApplication].delegate) showTabBarController];
        
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            ManagerEvent *event = result;
            if (event.code == 20003) {
                self.verCodeTF.lineLabel.backgroundColor = [UIColor redColor];
                self.verCodeTF.textColor = [UIColor redColor];
                [self showWarningWithString:LocalizedString(@"VERCODE_ERROR")];
            } else {
                [self error:result];
            }
        }
    };
    [AccountManager verCodeLogin:model callback:callback];

}


#pragma mark - SecondViewControllerDelegate

- (void)secondData:(NSString *)data withCountryName:(NSString *)name{
    self.phoneExistFlag = NO;
    self.countryLabel.text = [NSString stringWithFormat:@"+%@", data];
    self.countryView.countryLabel.text = name;
    [VLSCountryAreaManager shareManager].area = self.countryLabel.text;
    [VLSCountryAreaManager shareManager].name = name;
    
    if ([[self.countryLabel.text substringFromIndex:1] isEqualToString:@"86"]) {
        if (self.phoneTF.text.length > 3 && self.phoneTF.text.length < 8) {
            self.phoneTF.text = [NSString stringWithFormat:@"%@ %@", [self.phoneTF.text substringToIndex:3], [self.phoneTF.text substringFromIndex:3]];
        } else if (self.phoneTF.text.length > 8) {
            self.phoneTF.text = [NSString stringWithFormat:@"%@ %@ %@", [self.phoneTF.text substringToIndex:3], [self.phoneTF.text substringWithRange:NSMakeRange(3, 4)], [self.phoneTF.text substringFromIndex:7]];
            
        }
        
    } else {
        self.phoneTF.text = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
    }
    
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}

#pragma mark - UITextField 输入判断

- (void)verCodeTFAction {
    if (self.verCodeTF.textColor == [UIColor redColor]) {
        self.verCodeTF.textColor = [UIColor blackColor];
        self.verCodeTF.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        [self hideWarning];
    }
    
    
    if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
        if (self.phoneNum.length == 11 && self.verCodeTF.text.length == 4) {
            [self nextActionEnable];
            
        } else {
            [self nextActionDeny];
        }
    } else {
        if (self.phoneNum.length > 0 && self.verCodeTF.text.length == 4) {
            [self nextActionEnable];
            
        } else {
            [self nextActionDeny];
        }
    }
    
    //    // 验证码只能输入四位
    //    if (self.verCodeTF.text.length > 4) {
    //        self.verCodeTF.text = [self.verCodeTF.text substringToIndex:4];
    //    }
}

- (void)phoneTFAction {
    
    if (self.phoneTF.textColor == [UIColor redColor]) {
        self.phoneTF.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        self.phoneTF.textColor = [UIColor blackColor];
        [self hideWarning];
        
    }
    
    if (self.phoneTF.text.length > 0 &&  self.phoneTF.text.length > self.temStr.length) {
        
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
            [self phoneFormatWithChinaMobile];
        }
        
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
            NSString *firstChar = [self.phoneTF.text substringToIndex:1];
            if ([firstChar isEqualToString:@"0"]) {
                if (self.phoneTF.text.length == 10) {
                    [self.phoneTF endEditing:YES];
                    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self phoneExistsOnly];
                }
//                if (self.phoneTF.text.length > 10) {
//                    self.phoneTF.text = [self.phoneTF.text substringToIndex:10];
//                    [self.phoneTF endEditing:YES];
//                }
            } else {
                if (self.phoneTF.text.length == 9) {
                    [self.phoneTF endEditing:YES];
                    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self phoneExistsOnly];
                }
//                if (self.phoneTF.text.length > 9) {
//                    [self.phoneTF endEditing:YES];
//                    self.phoneTF.text = [self.phoneTF.text substringToIndex:9];
//                }
            }
        }
    }
    self.temStr = self.phoneTF.text;
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
}

#pragma mark - 手机号格式化

- (void)phoneFormatWithChinaMobile {
    if (self.phoneTF.text.length == 3) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@ ",self.phoneTF.text];
        
    }
    
    if (self.phoneTF.text.length == 8) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@ ",self.phoneTF.text];
        
    }
    
    if (self.phoneTF.text.length == 13) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@",self.phoneTF.text];
        [self.phoneTF endEditing:YES];
        self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self phoneExistsOnly];
    }
    if (self.phoneTF.text.length > 13) {
        
        self.phoneTF.text = [self.phoneTF.text substringToIndex:13];
        [self.phoneTF endEditing:YES];
        
    }
}

- (void)nextActionEnable {
  
    [self.loginButton setBackgroundColor:[UIColor redColor]];
    self.loginButton.layer.borderWidth = 0.;
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.loginButton.userInteractionEnabled = YES;
}

- (void)nextActionDeny {
     [self.loginButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.loginButton setBackgroundColor:RGB16(COLOR_BG_FFFFFF)];
    self.loginButton.layer.cornerRadius = 20. / 375. * SCREEN_WIDTH;
    self.loginButton.layer.borderWidth = 1.;
    self.loginButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.loginButton.layer.masksToBounds = YES;
    self.loginButton.userInteractionEnabled = NO;
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.verCodeTF resignFirstResponder];
    [self.phoneTF resignFirstResponder];
}

#pragma mark - getters

- (UIImageView *)warningImageView {
    if (!_warningImageView) {
        
        self.warningImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"livebefore_warn"]];
        
    }
    return _warningImageView;
}

- (UILabel *)warningLabel {
    if (!_warningLabel) {
        self.warningLabel = [[UILabel alloc] init];
        self.warningLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.warningLabel.textColor = [UIColor redColor];
        self.warningLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _warningLabel;
}

- (VLSCountryView *)countryView {
    if (!_countryView) {
        self.countryView = [[VLSCountryView alloc] init];
        self.countryView.delegate = self;
    }
    return _countryView;
}


- (VLSTextField *)phoneTF {
    if (!_phoneTF) {
        // 创建国家选择按钮
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        self.countryLabel = [[UILabel alloc] init];
        self.countryLabel.font = [UIFont systemFontOfSize:15];
        self.countryLabel.text = [VLSCountryAreaManager shareManager].area;
        [leftView addSubview:self.countryLabel];
        self.countryLabel.sd_layout
        .leftSpaceToView(leftView, 0)
        .topSpaceToView(leftView, 0)
        .bottomSpaceToView(leftView, 0)
        .widthIs(60);
        
        
        // 创建手机号输入框
        self.phoneTF = [[VLSTextField alloc] init];
        self.phoneTF.text = self.phoneNumber;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
        self.phoneTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PAGE_INPUTPHONE") attributes:attributes];
        self.phoneTF.keyboardType = UIKeyboardTypeNumberPad;
        self.phoneTF.leftView = leftView;
        self.phoneTF.leftViewMode = UITextFieldViewModeAlways;
        [self.phoneTF addTarget:self action:@selector(phoneTFAction) forControlEvents:UIControlEventEditingChanged];
    }
    return _phoneTF;
}

- (VLSTextField *)verCodeTF {
    if (!_verCodeTF) {
        
        // 创建验证码输入框左侧视图 View
        UILabel *codeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        codeLabel.text = LocalizedString(@"LOGIN_LABEL_VERCODE");
        codeLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
        
        // 创建验证码输入框右侧视图 View
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        self.phoneIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone_gray"]];
        [rightView addSubview:self.phoneIcon];
        self.phoneIcon.sd_layout
        .leftSpaceToView(rightView, 0)
        .topSpaceToView(rightView, 10)
        .bottomSpaceToView(rightView, 10)
        .widthIs(10);
        
        // 创建发送验证码按钮
        UIButton *verCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [verCodeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [verCodeButton setTitle:LocalizedString(@"VERCODE_SEND_VERCODE") forState: UIControlStateNormal];
        [verCodeButton addTarget:self action:@selector(verCodeAction:) forControlEvents:UIControlEventTouchUpInside];
        verCodeButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [rightView addSubview:verCodeButton];
        verCodeButton.sd_layout
        .leftSpaceToView(self.phoneIcon, 0)
        .rightSpaceToView(rightView, 0)
        .topSpaceToView(rightView, 0)
        .bottomSpaceToView(rightView, 0);
        
        // 创建验证码输入框
        self.verCodeTF = [[VLSTextField alloc] init];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
        self.verCodeTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PHONE_CODE") attributes:attributes];
        self.verCodeTF.keyboardType = UIKeyboardTypeNumberPad;
        self.verCodeTF.rightView = rightView;
        self.verCodeTF.leftView = codeLabel;
        self.verCodeTF.leftViewMode = UITextFieldViewModeAlways;
        self.verCodeTF.rightViewMode = UITextFieldViewModeAlways;
        [self.verCodeTF addTarget:self action:@selector(verCodeTFAction) forControlEvents:UIControlEventEditingChanged];
    }
    return _verCodeTF;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
