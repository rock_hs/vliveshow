//
//  VLSPhoneLoginViewController.m
//  VLiveShow
//
//  Created by Cavan on 16/5/13.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

/************************ 手机号登录界面 UI *****************************/

#import "VLSPhoneLoginViewController.h"

#import "VLSVerCodeLoginViewController.h"
#import "VLSForgetPwdViewController.h"
#import "UIView+SDAutoLayout.h"
#import "VLSTabBarController.h"
#import "VLSMessageManager.h"
#import "AccountManager.h"
#import "VLSTextField.h"
#import "AppDelegate.h"
#import "SectionsViewController.h"
#import "VLSPhoneRegisterViewController.h"
#import "VLSNavigationController.h"
#import "VLSCountryAreaManager.h"
#import "VLSCountryView.h"


#define kSpace 20

@interface VLSPhoneLoginViewController ()<SecondViewControllerDelegate, UITextFieldDelegate, VLSCountryViewDelegate>

@property (nonatomic, strong)VLSCountryView *countryView;
/// 手机号输入框
@property (nonatomic, strong)VLSTextField *phoneTF;
/// 密码输入框
@property (nonatomic, strong)VLSTextField *pwdTF;
/// 登录按钮
@property (nonatomic, strong)UIButton *loginButton;
/// 选择国家
@property (nonatomic, strong)UILabel *countryLabel;
/// 去除空格后的手机号
@property (nonatomic, strong)NSString *phoneNum;

/// 临时字符串
@property (nonatomic, copy)NSString *temStr;

/// 警告图标
@property (nonatomic, strong)UIImageView *warningImageView;
/// 警告的提示语
@property (nonatomic, strong)UILabel *warningLabel;
/// 标记是否打开国家区号控制器
@property (nonatomic, assign)BOOL flag;
/// 是否验证过手机号存在的标记
@property (nonatomic, assign)BOOL phoneExistFlag;

@end

@implementation VLSPhoneLoginViewController


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.phoneTF.text && !_flag) {
        self.phoneTF.text = nil;
        self.pwdTF.text = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNavigationBarRedMethod];
    [self.navigationController.navigationBar setTranslucent: NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage ImageFromColor:[UIColor redColor]] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
    self.countryLabel.text = [VLSCountryAreaManager shareManager].area;
    self.countryView.countryLabel.text = [VLSCountryAreaManager shareManager].name;
    if (self.phoneTF.text.length) {
        
        NSString *temStr = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *firstChar = [self.phoneTF.text substringToIndex:1];
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
            if ([firstChar isEqualToString:@"0"]) {
                if (temStr.length == 10) {
                    [self phoneExistsOnly];
                }
            } else {
                if (temStr.length == 9) {
                    [self phoneExistsOnly];
                }
            }
        }
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
            if (temStr.length == 11) {
                [self phoneExistsOnly];
            }
        }
    }
   
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    

    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = LocalizedString(@"LOGIN_PAGE_PHONELOGIN");
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"LOGIN_PAGE_REGISTER") style:UIBarButtonItemStylePlain target:self action:@selector(registerAction)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home_back"] style:UIBarButtonItemStylePlain target:self action:@selector(leftItemAction:)];
    self.navigationItem.leftBarButtonItem = leftItem;

    
    [self configUI];
    
    
}

#pragma mark - 界面基础配置

- (void)configUI {
    
    
    [self.view addSubview:self.countryView];
    self.countryView.sd_layout
    .leftSpaceToView(self.view, kSpace)
    .rightSpaceToView(self.view, kSpace)
    .topSpaceToView(self.view, 20)
    .heightIs(40);
    
    [self.view addSubview:self.phoneTF];
    self.phoneTF.sd_layout
    .leftSpaceToView(self.view, kSpace)
    .rightSpaceToView(self.view, kSpace)
    .topSpaceToView(self.countryView, kSpace)
    .heightIs(40);
    
   
     self.pwdTF.sd_layout
    .leftSpaceToView(self.view, kSpace)
    .rightSpaceToView(self.view, kSpace)
    .topSpaceToView(self.phoneTF, kSpace)
    .heightIs(40);
    
    
    [self.view addSubview:self.loginButton];
    self.loginButton.sd_layout
    .topSpaceToView(self.pwdTF, 4 * kSpace)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .heightIs(40);
    
    // 通过验证码登录
    UIButton *verCodeLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [verCodeLoginButton setTitle:LocalizedString(@"LOGIN_LABEL_CERCODE_LOGIN") forState: UIControlStateNormal];
    [verCodeLoginButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [verCodeLoginButton addTarget:self action:@selector(verCodeLoginAction) forControlEvents:UIControlEventTouchUpInside];
    verCodeLoginButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
    [self.view addSubview:verCodeLoginButton];
    verCodeLoginButton.sd_layout
    .leftSpaceToView(self.view, 20)
    .topSpaceToView(self.loginButton, 10)
    .widthIs(100)
    .heightIs(40);
    
    // 忘记密码按钮
    UIButton *forgetPwdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [forgetPwdButton setTitle:LocalizedString(@"LOGIN_LABEL_FORGET_PWD") forState: UIControlStateNormal];
    [forgetPwdButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [forgetPwdButton addTarget:self action:@selector(forgetPwdAction) forControlEvents:UIControlEventTouchUpInside];
    forgetPwdButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_12];
    [self.view addSubview:forgetPwdButton];
    forgetPwdButton.sd_layout
    .rightSpaceToView(self.view, 10)
    .topSpaceToView(self.loginButton, 10)
    .widthIs(100)
    .heightIs(40);
    
    
}

#pragma mark - VLSCountryViewDelegate

- (void)selectCountryArea {
    _flag = YES;
    SectionsViewController *stVC = [SectionsViewController new];
    stVC.delegate = self;
    [self.navigationController pushViewController:stVC animated:YES];
}

#pragma mark - 是否开启明文显示

- (void)eyesAction:(id)sender{
    
    UIButton *button = sender;
    self.pwdTF.secureTextEntry = button.selected;
    button.selected = !button.selected;
    [self.pwdTF endEditing:YES];
    
    
}

#pragma mark - 导航栏右侧登录按钮

- (void)registerAction {
    NSInteger count = self.navigationController.viewControllers.count;
    if ([[self.navigationController.viewControllers objectAtIndex:(count - 2)] isKindOfClass:[VLSPhoneRegisterViewController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
    VLSPhoneRegisterViewController *phoneRegisterVC = [[VLSPhoneRegisterViewController alloc] init];
    phoneRegisterVC.phoneNumber = self.phoneTF.text;
    [self.navigationController pushViewController:phoneRegisterVC animated:YES];
    }
}

#pragma mark - 导航栏左侧返回按钮

- (void)leftItemAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 显示警告
- (void)showWarningWithString:(NSString *)str {
    
    self.warningLabel.text = str;
    
    [self.view addSubview:self.warningLabel];
    [self.warningLabel setSingleLineAutoResizeWithMaxWidth:SCREEN_WIDTH];
    self.warningLabel.sd_layout
    .centerXEqualToView(self.view)
    .bottomSpaceToView(self.loginButton, 30)
    .heightIs(30);
    
    [self.view addSubview:self.warningImageView];
    self.warningImageView.sd_layout
    .rightSpaceToView(self.warningLabel, 5)
    .centerYEqualToView(self.warningLabel)
    .widthIs(15)
    .heightIs(15);
    
}

#pragma mark - 隐藏警告
- (void)hideWarning {
    [self.warningLabel removeFromSuperview];
    [self.warningImageView removeFromSuperview];
}



#pragma mark - 登录按钮方法

/**
 *  业务逻辑：
 *  1、根据手机号和验证码发送网络请求
 *  2、如果请求成功，存储服务器返回的 Token，并且进行一次网络请求获取用户信息
 *  3、根据请求数据，判断该用户是否第一次登陆（有没有修改过用户信息）
 *  4、如果是第一次登陆，跳转到修改用户信息界面 VLSRegisterViewController
 *  5、如果已经修改过用户信息，直接跳转到直播列表界面
 *  6、如果登陆请求失败，给出网络请求失败的提示
 */

- (void)loginAction {

    if (self.phoneExistFlag) {
        [self loginCallBack];
    } else {
        [self phoneExists];
    }
}

- (void)loginCallBack {
    self.loginButton.userInteractionEnabled = NO;
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.password = self.pwdTF.text;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        //        [self getUserInfo];
        
        if (![[VLSMessageManager sharedManager] GetLoginState]) {
            [[VLSMessageManager sharedManager] IMLogin];//重连IM
        }
        
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self success:result];
        }
        
        [((AppDelegate *)[UIApplication sharedApplication].delegate) showTabBarController];
        
    };
    callback.errorBlock = ^(id result){
        self.loginButton.userInteractionEnabled = YES;
        if ([result isKindOfClass:[ManagerEvent class]]) {
            ManagerEvent *event = result;
            if (event.code == 20005) {
                self.pwdTF.lineLabel.backgroundColor = [UIColor redColor];
                self.pwdTF.textColor = [UIColor redColor];
                [self showWarningWithString:LocalizedString(@"LOGIN_PAGE_PWD_ERROR")];
            } else {
                [self error:result];
            }
        }
    };
    [AccountManager passwordLogin:model callback:callback];

}

#pragma mark - 验证手机号是否存在

- (void)phoneExists {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.phoneExistFlag = YES;
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        if (![[result objectForKey:@"existPhone"] boolValue]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"ALERT_MESSAGE_GO_REGISTER") preferredStyle:UIAlertControllerStyleAlert];
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                [self.phoneTF endEditing:YES];
                [self.pwdTF endEditing:YES];
                
                VLSPhoneRegisterViewController *phoneRegisterVC = [[VLSPhoneRegisterViewController alloc] init];
                phoneRegisterVC.phoneNumber = self.phoneTF.text;
                [self.navigationController pushViewController:phoneRegisterVC animated:YES];
                
            }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleCancel handler:nil];
            [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            [self loginCallBack];
        }
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager phoneExists:model callback:callback];

}

- (void)phoneExistsOnly {
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.phoneExistFlag = YES;
    AccountModel *model = [[AccountModel alloc] init];
    model.mobilePhone = self.phoneNum;
    model.nationCode = [self.countryLabel.text substringFromIndex:1];
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        if (self.pwdTF.text.length>=6&self.pwdTF.text.length<=20) {
            [self nextActionEnable];
        }
        if (![[result objectForKey:@"existPhone"] boolValue]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"ALERT_MESSAGE_GO_REGISTER") preferredStyle:UIAlertControllerStyleAlert];
            // 确定按钮
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                [self.phoneTF endEditing:YES];
                [self.pwdTF endEditing:YES];
                
                VLSPhoneRegisterViewController *phoneRegisterVC = [[VLSPhoneRegisterViewController alloc] init];
                phoneRegisterVC.phoneNumber = self.phoneTF.text;
                [self.navigationController pushViewController:phoneRegisterVC animated:YES];
                
            }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleCancel handler:nil];
            [cancelAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
           
        }
    };
    callback.errorBlock = ^(id result){
        self.phoneExistFlag = NO;
        if ([result isKindOfClass:[ManagerEvent class]]) {
            [self error:result];
        }
    };
    
    [AccountManager phoneExists:model callback:callback];
    
}


#pragma mark - 通过验证码登录

- (void)verCodeLoginAction {
    
    // 跳转到发送验证码界面
    VLSVerCodeLoginViewController *verCodeVC = [[VLSVerCodeLoginViewController alloc] init];
    verCodeVC.phoneNumber = self.phoneTF.text;
    [self.navigationController pushViewController:verCodeVC animated:YES];
    
    
}



#pragma mark - 忘记密码 

- (void)forgetPwdAction {
    VLSForgetPwdViewController *forgetPwdVC = [[VLSForgetPwdViewController alloc] init];
    forgetPwdVC.phoneNumber = self.phoneTF.text;
    [self.navigationController pushViewController:forgetPwdVC animated:YES];
}


#pragma mark - SecondViewControllerDelegate

- (void)secondData:(NSString *)data withCountryName:(NSString *)name{
    self.phoneExistFlag = NO;
    self.countryLabel.text = [NSString stringWithFormat:@"+%@", data];
    self.countryView.countryLabel.text = name;
    [VLSCountryAreaManager shareManager].area = self.countryLabel.text;
    [VLSCountryAreaManager shareManager].name = name;
    if ([[self.countryLabel.text substringFromIndex:1] isEqualToString:@"86"]) {
        if (self.phoneTF.text.length > 3 && self.phoneTF.text.length < 8) {
            self.phoneTF.text = [NSString stringWithFormat:@"%@ %@", [self.phoneTF.text substringToIndex:3], [self.phoneTF.text substringFromIndex:3]];
        } else if (self.phoneTF.text.length > 8) {
            self.phoneTF.text = [NSString stringWithFormat:@"%@ %@ %@", [self.phoneTF.text substringToIndex:3], [self.phoneTF.text substringWithRange:NSMakeRange(3, 4)], [self.phoneTF.text substringFromIndex:7]];
            
        }
        
    } else {
        self.phoneTF.text = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
    }
    
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (void)pwdTFAction {
    
    if (self.pwdTF.textColor == [UIColor redColor]) {
        self.pwdTF.textColor = [UIColor blackColor];
        self.pwdTF.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        [self hideWarning];
    }
    
    
    if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
        if ((self.phoneNum.length == 11
             || self.phoneNumber.length == 13)
            && self.pwdTF.text.length > 5
            && self.pwdTF.text.length < 21
            ) {
            [self nextActionEnable];
            
        } else {
            [self nextActionDeny];
        }
        
    } else {
        if (self.phoneNum.length > 0
            && self.pwdTF.text.length > 5
            && self.pwdTF.text.length < 21
            ) {
            [self nextActionEnable];
            
        } else {
            [self nextActionDeny];
        }
        
    }
    
    self.pwdTF.text = [self.pwdTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];

    
}


- (void)phoneTFAction {

    [self nextActionDeny];

    if (self.phoneTF.textColor == [UIColor redColor]) {
        self.phoneTF.lineLabel.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.000];
        self.phoneTF.textColor = [UIColor blackColor];
        [self hideWarning];
        
    }
    
    if (self.phoneTF.text.length > 0 &&  self.phoneTF.text.length > self.temStr.length) {
        
        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_CHINA]) {
            [self phoneFormatWithChinaMobile];
        }

        if ([self.countryLabel.text isEqualToString:COUNTRY_AREA_TW]) {
            NSString *firstChar = [self.phoneTF.text substringToIndex:1];
            if ([firstChar isEqualToString:@"0"]) {
                if (self.phoneTF.text.length == 10) {
                    [self.phoneTF endEditing:YES];
                    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self phoneExistsOnly];
                }
//                if (self.phoneTF.text.length > 10) {
//                    self.phoneTF.text = [self.phoneTF.text substringToIndex:10];
//                    [self.phoneTF endEditing:YES];
//                }
            } else {
                if (self.phoneTF.text.length == 9) {
                    [self.phoneTF endEditing:YES];
                    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [self nextActionDeny];
                    [self phoneExistsOnly];
                }
//                if (self.phoneTF.text.length > 9) {
//                    [self.phoneTF endEditing:YES];
//                    self.phoneTF.text = [self.phoneTF.text substringToIndex:9];
//                }
            }
        }

    }
    self.temStr = self.phoneTF.text;
    self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];

}

#pragma mark - 手机号格式化

- (void)phoneFormatWithChinaMobile {
    if (self.phoneTF.text.length == 3) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@ ",self.phoneTF.text];
        
    }
    
    if (self.phoneTF.text.length == 8) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@ ",self.phoneTF.text];
        
    }
    
    if (self.phoneTF.text.length == 13) {
        
        self.phoneTF.text = [NSString stringWithFormat:@"%@",self.phoneTF.text];
        [self.phoneTF endEditing:YES];
        self.phoneNum = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self phoneExistsOnly];
    }
    if (self.phoneTF.text.length > 13) {
        
        self.phoneTF.text = [self.phoneTF.text substringToIndex:13];
        [self.phoneTF endEditing:YES];
        
    }
}

- (void)nextActionEnable {
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.loginButton setBackgroundColor:[UIColor redColor]];
    self.loginButton.layer.borderWidth = 0.;
    self.loginButton.userInteractionEnabled = YES;
}

- (void)nextActionDeny {
    [self.loginButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.loginButton setBackgroundColor:RGB16(COLOR_BG_FFFFFF)];
    self.loginButton.layer.cornerRadius = 20. / 375. * SCREEN_WIDTH;
    self.loginButton.layer.borderWidth = 1.;
    self.loginButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.loginButton.layer.masksToBounds = YES;
    self.loginButton.userInteractionEnabled = NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.pwdTF)
    {
        if (textField.secureTextEntry)
        {
            [textField insertText:self.pwdTF.text];
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.pwdTF resignFirstResponder];
    [self.phoneTF resignFirstResponder];
}
#pragma mark - getters

- (UIImageView *)warningImageView {
    if (!_warningImageView) {
        
        self.warningImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"livebefore_warn"]];
        
    }
    return _warningImageView;
}

- (UILabel *)warningLabel {
    if (!_warningLabel) {
        self.warningLabel = [[UILabel alloc] init];
        self.warningLabel.font = [UIFont systemFontOfSize:SIZE_FONT_14];
        self.warningLabel.textColor = [UIColor redColor];
        self.warningLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _warningLabel;
}

- (VLSCountryView *)countryView {
    if (!_countryView) {
        self.countryView = [[VLSCountryView alloc] init];
        self.countryView.delegate = self;    }
    return _countryView;
}

- (VLSTextField *)phoneTF {
    if (!_phoneTF) {
        // 创建国家选择按钮
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        self.countryLabel = [[UILabel alloc] init];
        self.countryLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
        self.countryLabel.text = [VLSCountryAreaManager shareManager].area;
        [leftView addSubview:self.countryLabel];
        self.countryLabel.sd_layout
        .leftSpaceToView(leftView, 0)
        .topSpaceToView(leftView, 0)
        .bottomSpaceToView(leftView, 0)
        .widthIs(60);
        
        // 创建手机号输入框
        self.phoneTF = [[VLSTextField alloc] init];
        self.phoneTF.text = self.phoneNumber;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
        self.phoneTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PAGE_INPUTPHONE") attributes:attributes];
        self.phoneTF.keyboardType = UIKeyboardTypeNumberPad;
        self.phoneTF.leftView = leftView;
        self.phoneTF.leftViewMode = UITextFieldViewModeAlways;
        [self.phoneTF addTarget:self action:@selector(phoneTFAction) forControlEvents:UIControlEventEditingChanged];

    }
    return _phoneTF;
}


- (VLSTextField *)pwdTF {
    if (!_pwdTF) {
        
        // 创建验证码输入框左侧视图 View
        UILabel *pwdLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        pwdLabel.text = LocalizedString(@"LOGIN_PWD");
        pwdLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
        
        // 创建密码输入框的右侧视图
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton setFrame:CGRectMake(0, 0, 40, 20)];
        [rightButton setImage:[UIImage imageNamed:@"login_eyes_closed"] forState:UIControlStateNormal];
        [rightButton setImage:[UIImage imageNamed:@"login_eyes_open"] forState:UIControlStateSelected];
        [rightButton addTarget:self action:@selector(eyesAction:) forControlEvents:UIControlEventTouchUpInside];
        
        // 创建密码输入框
        self.pwdTF = [[VLSTextField alloc] init];
        self.pwdTF.rightView = rightButton;
        self.pwdTF.leftView = pwdLabel;
        self.pwdTF.delegate = self;
        self.pwdTF.secureTextEntry = YES;
        self.pwdTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.pwdTF.keyboardType = UIKeyboardTypeASCIICapable;
        self.pwdTF.autocorrectionType = UITextAutocorrectionTypeNo;
        self.pwdTF.rightViewMode = UITextFieldViewModeAlways;
        self.pwdTF.leftViewMode = UITextFieldViewModeAlways;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:SIZE_FONT_15]};
        self.pwdTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedString(@"LOGIN_PAGE_PASSWORD") attributes:attributes];
        [self.pwdTF addTarget:self action:@selector(pwdTFAction) forControlEvents:UIControlEventEditingChanged];
        [self.view addSubview:self.pwdTF];
    }
    return _pwdTF;
}

- (UIButton *)loginButton {
    if (!_loginButton) {
        // 创建登录按钮
        self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.loginButton.titleLabel.font = [UIFont systemFontOfSize:SIZE_FONT_15];
       
        [self.loginButton setTitle:LocalizedString(@"LOGIN_PAGE_LOGIN") forState:UIControlStateNormal];
        [self nextActionDeny];
        [self.loginButton addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
        self.loginButton.userInteractionEnabled = NO;
    }
    return _loginButton;
}

@end
