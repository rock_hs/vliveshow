//
//  Macro.h
//  VLiveShow
//
//  Created by Cavan on 16/5/16.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#ifndef Macro_h
#define Macro_h

#if TARGET_VERSION_LITE == 0

// Test Settings
#define BASE_URL                @"http://124.172.174.187:8888/vliveshow-api-app"
#define REDIRECT_URL            @"http://www.vipabc.com"
#define SHARE_THTML_URL         @"http://124.172.174.187:8080/share/preview.html"
#define SHARE_SINA_URL          @"http://www.vliveshow.com"

#define VENDORKEY               @"69299c1a456340419e7d0dcfb533afa7"
#define CLOUND_SDK              1400009962
#define CLOUND_ACCOUNT_TYPE     @"5276"
#define BUSID                   1825
#define APPKEY_UMENG            @"5745101fe0f55ab82600052c"
#define WX_APPID                @"wx99ef2c5818d88878"
#define WX_APPSCRECT            @"b16a162e54a501b091a2600e25f503d7"
#define QQ_APPID                @"1105390061"
#define QQ_APPKEY               @"0jyZlonpAxVY284k"
#define SINA_APPKEY             @"833573700"
#define SINA_APPSCRECT          @"7193164878ef187b70273529b9a59321"

#define TALKINGDATA_APP_ID      @"0424A6CEDE37DCB8D61F84B0475F768B"
#define TALKINGDATA_ADTRACK_ID  @""

#elif TARGET_VERSION_LITE == 1

// Production Settings
#define BASE_URL                @"http://www.vliveshow.com/vliveshow-api-app"
#define REDIRECT_URL            @"http://www.vipabc.com"
#define SHARE_THTML_URL         @"http://www.vliveshow.com/share/preview.html"
#define SHARE_SINA_URL          @"http://www.vliveshow.com"

#define VLIVESHOW_HOST          @"http://www.vliveshow.com"

// agaro 配置
#define VENDORKEY               @"a6cbb42220114d30a517d652ab3e179c"
// 腾讯云配置
#define CLOUND_SDK              1400011613
#define CLOUND_ACCOUNT_TYPE     @"6110"
#define BUSID                   1831
// Umeng AppKey
#define APPKEY_UMENG            @"579194c2e0f55ae95f000e2d"

#define WX_APPID                @"wx88846c1764d627a5"
#define WX_APPSCRECT            @"612a590ce0ac96c27c871d61f7325eec"

#define QQ_APPID                @"1105390061"
#define QQ_APPKEY               @"0jyZlonpAxVY284k"
#define SINA_APPKEY             @"833573700"
#define SINA_APPSCRECT          @"7193164878ef187b70273529b9a59321"

// Talking Data 配置
#ifdef MINI_VERSION
#define TALKINGDATA_APP_ID      @"A1D1D4CBF87043849EBAF9BE32BFFE62"
#define TALKINGDATA_ADTRACK_ID  @"506a9ad884084edb9cbb890f088091af"
#else
#define TALKINGDATA_APP_ID      @"BBC58023A6CCD542250E1B08B5B905B5"
#define TALKINGDATA_ADTRACK_ID  @"4596188be3ed410d90c357c40720ac3c"
#endif

#elif TARGET_VERSION_LITE == 2

// Dev Settings
#define BASE_URL                @"http://dev.vliveshow.com/vliveshow-api-app"
#define REDIRECT_URL            @"http://www.vipabc.com"
#define SHARE_THTML_URL         @"http://dev.vliveshow.com/share/preview.html"
#define SHARE_SINA_URL          @"http://www.vliveshow.com"
#define VLIVESHOW_HOST          @"http://dev.vliveshow.com"


#define VENDORKEY               @"06c59bc35aee40fd952e9fd7eb34a3b3"
#define CLOUND_SDK              1400012474
#define CLOUND_ACCOUNT_TYPE     @"6504"
#define BUSID                   1825
#define APPKEY_UMENG            @"5745101fe0f55ab82600052c"
#define WX_APPID                @"wx99ef2c5818d88878"
#define WX_APPSCRECT            @"b16a162e54a501b091a2600e25f503d7"
#define QQ_APPID                @"1105390061"
#define QQ_APPKEY               @"0jyZlonpAxVY284k"
#define SINA_APPKEY             @"833573700"
#define SINA_APPSCRECT          @"7193164878ef187b70273529b9a59321"

#define TALKINGDATA_APP_ID      @"0424A6CEDE37DCB8D61F84B0475F768B"
#define TALKINGDATA_ADTRACK_ID  @""

#elif TARGET_VERSION_LITE == 3
#define BASE_URL                @"http://staging.vliveshow.com/vliveshow-api-app"
#define REDIRECT_URL            @"http://www.vipabc.com"
#define SHARE_THTML_URL         @"http://staging.vliveshow.com/share/preview.html"
#define SHARE_SINA_URL          @"http://www.vliveshow.com"
#define VLIVESHOW_HOST          @"http://staging.vliveshow.com"


#define VENDORKEY               @"06c59bc35aee40fd952e9fd7eb34a3b3"
#define CLOUND_SDK              1400012392
#define CLOUND_ACCOUNT_TYPE     @"6472"
#define BUSID                   3353


#define APPKEY_UMENG            @"5745101fe0f55ab82600052c"
#define WX_APPID                @"wx99ef2c5818d88878"
#define WX_APPSCRECT            @"b16a162e54a501b091a2600e25f503d7"
#define QQ_APPID                @"1105390061"
#define QQ_APPKEY               @"0jyZlonpAxVY284k"
#define SINA_APPKEY             @"833573700"
#define SINA_APPSCRECT          @"7193164878ef187b70273529b9a59321"

#define TALKINGDATA_APP_ID      @"0424A6CEDE37DCB8D61F84B0475F768B"
#define TALKINGDATA_ADTRACK_ID  @""

#endif

// 语言国际化设置
#define LocalizedString(key)    [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:nil]

//获取屏幕 宽度、高度、比例等设备信息
#define IS_IPAD                 (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE               (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA               ([[UIScreen mainScreen] scale] >= 2.0)
#define SCREEN_WIDTH            ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT           ([UIScreen mainScreen].bounds.size.height)
#define SCALE                   ([UIScreen mainScreen].scale)
#define SCREEN_MAX_LENGTH       (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH       (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SC_WIDTH                MIN(SCREEN_WIDTH, SCREEN_HEIGHT)
#define SC_HEIGHT                MAX(SCREEN_WIDTH, SCREEN_HEIGHT)
#define IS_IPHONE_4_OR_LESS     (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5             (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6             (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P            (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


//状态栏、导航栏、标签栏高度
#define STATUS_HEIGHT           ([UIApplication sharedApplication].statusBarFrame.size.height)
#define NAVIGATIONBAR_HEIGHT    (self.navigationController.navigationBar.frame.size.height)
#define TABBAR_HEIGHT           (self.tabBarController.tabBar.frame.size.height)


// 验证码倒计时
#define VERCODE_TIME            60
#define COUNTRY_AREA_CHINA      @"+86"
#define COUNTRY_AREA_TW         @"+886"
#define LIVE_BEFORE_TITLE_COUNT 20

// 字体大小
#define SIZE_FONT_8             8
#define SIZE_FONT_9             9
#define SIZE_FONT_10            10
#define SIZE_FONT_11            11
#define SIZE_FONT_12            12
#define SIZE_FONT_13            13
#define SIZE_FONT_14            14
#define SIZE_FONT_15            15
#define SIZE_FONT_16            16
#define SIZE_FONT_17            17
#define SIZE_FONT_18            18
#define SIZE_FONT_19            19
#define SIZE_FONT_20            20
#define SIZE_FONT_28            28
#define SIZE_FONT_24            24
#define SIZE_FONT_32            32
#define SIZE_FONT_34            34

//字体颜色
#define COLOR_FONT_9B9B9B       0x9B9B9B
#define COLOR_FONT_4A4A4A       0x4A4A4A
#define COLOR_FONT_FF1130       0xFF1130
#define COLOR_FONT_FFFFFF       0xFFFFFF
#define COLOR_FONT_C3A851       0xC3A851
#define COLOR_FONT_000000       0x000000
#define COLOR_FONT_FF4429       0xFF4429
#define COLOR_FONT_FF784A       0xFF784A
#define COLOR_FONT_979797       0x979797
#define COLOR_FONT_C3A851       0xC3A851
#define COLOR_FONT_777777       0x777777
#define COLOR_FONT_C1BEBE       0xC1BEBE
#define COLOR_FONT_999999       0x999999
#define COLOR_FONT_CCCCCC       0xCCCCCC
#define COLOR_FONT_222222       0x222222
#define COLOR_FONT_B2B2B2       0xB2B2B2
#define COLOR_FONT_C6C6C6       0xC6C6C6
#define COLOR_FONT_C7C7C7       0xC7C7C7
#define COLOR_FONT_CECECE       0xCECECE
#define COLOR_FONT_FF784A       0xFF784A
#define COLOR_FONT_DBDBDB       0xDBDBDB
#define COLOR_FONT_F9CB35       0xF9CB35
#define COLOR_FONT_F4F4F4       0xF4F4F4
#define COLOR_FONT_888888       0x888888
#define COLOR_FONT_F12F47       0xF12F47
#define COLOR_FONT_464646       0x464646
#define COLOR_FONT_53ABFF       0x53ABFF
#define COLOR_FONT_AAAAAA       0xAAAAAA

// 背景颜色
#define COLOR_BG_F8F8F8         0xF8F8F8
#define COLOR_BG_FFFFFF         0xFFFFFF
#define COLOR_BG_FFDA62         0xFFDA62
#define COLOR_BG_FF1130         0xFF1130
#define COLOR_BG_000000         0x000000
#define COLOR_BG_F4F4F4         0xF4F4F4
#define COLOR_BG_DBDBDB         0xDBDBDB
#define COLOR_BG_D8D8D8         0xD8D8D8
#define COLOR_BG_9B9B9B         0x9B9B9B
#define COLOR_BG_EEEEEE         0xEEEEEE
#define COLOR_BG_FBE684         0xFBE684
#define COLOR_BG_F2F2F2         0xF2F2F2
#define COLOR_BG_C3A851         0xC3A851
#define COLOR_BG_E9E7EF         0xE9E7EF
#define COLOR_BG_ECECEC         0xECECEC
#define COLOR_BG_53ABFF         0x53ABFF
#define COLOR_BG_DCDCDC         0xDCDCDC


// 线条颜色
#define COLOR_LINE_F0F0F0       0xF0F0F0
#define COLOR_LINE_9B9B9B       0x9B9B9B
#define COLOR_LINE_D2D3D5       0xD2D3D5
#define COLOR_LINE_FF1130       0xFF1130
#define COLOR_LINE_F4F4F4       0xF4F4F4
#define COLOR_LINE_EEEEEE       0xEEEEEE
#define COLOR_LINE_ECECEC       0xECECEC
#define COLOR_LINE_FFFFFF       0xFFFFFF
#define COLOR_LINE_E2E2E2       0xE2E2E2


// 消息体20种色值
#define COLOR_MSG_F8E71C        0xF8E71C
#define COLOR_MSG_53AAFF        0x53AAFF
#define COLOR_MSG_A6D3FF        0xA6D3FF
#define COLOR_MSG_F5A623        0xF5A623
#define COLOR_MSG_7ED321        0x7ED321
#define COLOR_MSG_50E3C2        0x50E3C2
#define COLOR_MSG_F4A7B1        0xF4A7B1
#define COLOR_MSG_6E4B00        0x6E4B00
#define COLOR_MSG_FF8B64        0xFF8B64
#define COLOR_MSG_00B3B4        0x00B3B4
#define COLOR_MSG_AAE75C        0xAAE75C
#define COLOR_MSG_FF7995        0xFF7995
#define COLOR_MSG_A1EAFF        0xA1EAFF
#define COLOR_MSG_97F2F3        0x97F2F3
#define COLOR_MSG_F4A5DF        0xF4A5DF
#define COLOR_MSG_FF8BAD        0xFF8BAD
#define COLOR_MSG_E4A2FF        0xE4A2FF
#define COLOR_MSG_FFB000        0xFFB000
#define COLOR_MSG_C3E419        0xC3E419
#define COLOR_MSG_98F2F3        0x98F2F3
#define COLOR_MSG_97F2F3        0x97F2F3
#define COLOR_MSG_DBED54        0xDBED54
#define COLOR_MSG_C4E5A4        0xC4E5A4
#define COLOR_MSG_F6F197        0xF6F197
#define COLOR_MSG_13D6D7        0x13D6D7
#define COLOR_MSG_47FEFF        0x47FEFF

// 分享平台
#define QQPlatform              @"QQ"
#define WeChatPlatform          @"WeChat"
#define QQZPlatform             @"QQZone"
#define MomentsPlatform         @"Moments"
#define SinaPlatform            @"Sina"

// 重连保存的键值
#define ReconnectionRoomID      @"roomid"
#define ReconnectionAnchorID    @"anchorid"
//保存每次进入房间的roomId
#define kSaveRoomId(roomId)     if (roomId != nil && ![roomId isEqualToString:@""]) {\
[[NSUserDefaults standardUserDefaults] setObject:roomId forKey:[NSString stringWithFormat:@"group_roomId%ld", (long)[AccountManager sharedAccountManager].account.uid]];\
[[NSUserDefaults standardUserDefaults] synchronize];\
}
#define kGetRoomId              [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"group_roomId%ld", (long)[AccountManager sharedAccountManager].account.uid]];
//记录设备横竖屏状态
#define kSaveOriention(oriention)        [[NSUserDefaults standardUserDefaults] setObject:oriention forKey:[NSString stringWithFormat:@"koriention%ld", (long)[AccountManager sharedAccountManager].account.uid]];\
[[NSUserDefaults standardUserDefaults] synchronize];
#define kGetOriention           [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"koriention%ld", (long)[AccountManager sharedAccountManager].account.uid]];

// 第一次安装，保存的是否第一次进直播间或者做主播
#define isFirstWatch            @"isFirstWatch"
#define isFirstShow             @"isFirstShow"
#define isFirstStart            @"isFirstStart"

//登录踢出时消息推送
#define NOTIFICATION_LOGINOUTED    @"NOTIFICATION_LOGINOUTED"

// 直播提醒是否接收
#define LIVEREMIND              @"isOP"
// 头像占位图
#define ICON_PLACEHOLDER        [UIImage imageNamed:@"headshot"]

#define X_CONVERT(X)            ((X) / 375. * SCREEN_WIDTH)
#define Y_CONVERT(Y)            ((Y) / 667. * SCREEN_HEIGHT)

// RGB 转 UIColor (16进制)
#define RGB16(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define RGBA16(rgbaValue, a) [UIColor colorWithRed:((float)((rgbaValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbaValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbaValue & 0xFF))/255.0 alpha:a]

#if DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"\nfunction:%s line:%d content:%s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(FORMAT, ...) nil
#endif

#define MyLog(format, ...) do {                                                                          \
fprintf(stderr, "------vliveshow------\n\n<%s : %d> %s\n",                                           \
[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String],  \
__LINE__, __func__);                                                        \
(NSLog)((format), ##__VA_ARGS__);                                           \
fprintf(stderr, "\n------vliveshow------\n");                                               \
} while (0)

#define WS(ws) __weak typeof(self) ws = self;

#endif /* Macro_h */
