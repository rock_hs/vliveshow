//
//  GTVersion.h
//  VLiveShow
//
//  Created by SuperGT on 16/8/31.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTVersion : NSObject

+ (void)checkNewfeatures;

@end
