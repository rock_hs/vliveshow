//
//  AppDelegate.m
//  VLiveShow
//
//  Created by Cavan on 16/5/6.
//  Copyright © 2016年 vliveshow. All rights reserved.
//  comme

#import "AppDelegate.h"
#import <UMSocialCore/UMSocialCore.h>
#import "AccountManager.h"
#import <TLSSDK/TLSHelper.h>
#import "VLSMessageManager.h"
#import "UMSocialQQHandler.h"
#import <QALSDK/QalSDKProxy.h>
#import "VLSTabBarController.h"
#import "UMSocialWechatHandler.h"
#import "VLSMineViewController.h"
#import "VLSLoginViewController.h"
#import "VLSNavigationController.h"
#import "VLSLiveListModel.h"
#import "VLSShowViewController.h"
#import "VLSLiveViewController.h"
#import "VLSMessageViewController.h"
#import "DeviceModel.h"
#import "VLSLiveHttpManager.h"
#import "UMMobClick/MobClick.h"
#import "VLSLiveHttpManager.h"
#import "LiveOffScreenView.h"
#import "ShareLiveOfView.h"
#import "AccountManager.h"
#import "MBProgressHUD+Add.h"
#import "VLSHeartModel.h"
#import "VLSHeartManager.h"
#import "VLSLanguageModel.h"
#import "VLSDoubleUpModel.h"
#import "VLiveShow-Swift.h"
#import "VLSLittleNavigationController.h"
#import "VLSLocationManager.h"
#import "VLSReConnectionManager.h"
#import "TalkingData.h"
#import "TalkingDataAppCpa.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "AdvertisementViewController.h"
#import "VLSGifManager.h"
//自定义消息命令
#define VLS_SEPERATOR @"&"

static NSDictionary *LAUNCHOPTIONS = nil;

@interface AppDelegate ()<VLSIntroduceViewControllerDelegate>
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [[UITabBar appearance] setTranslucent:NO];
    application.applicationIconBadgeNumber = 0;
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
    
    [TalkingData sessionStarted: TALKINGDATA_APP_ID withChannelId: @""];
    [TalkingDataAppCpa init: TALKINGDATA_ADTRACK_ID withChannelId:@"apple"];
    
    [Fabric with: @[[Crashlytics class]]];
    // 查看是否是第一次安装
    [GTVersion checkNewfeatures];
    
    self.countryStr = COUNTRY_AREA_CHINA;
    //监测网络
    [self getNetWorkNotificationCenter];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-100, -100) forBarMetrics:UIBarMetricsDefault];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    [[VLSLocationManager shareManager] loadLocation];
    [[AccountManager sharedAccountManager] loadAccount];
    if ([AccountManager sharedAccountManager].account.uid > 0)
    {
        [[Crashlytics sharedInstance] setUserIdentifier: [NSString stringWithFormat: @"%d", [AccountManager sharedAccountManager].account.uid]];
        [[Crashlytics sharedInstance] setUserName: [NSString stringWithFormat: @"%@", [AccountManager sharedAccountManager].account.nickName]];
    }
    [VLSIAPManager sharedInstance];

    //追踪tracking记录
    [[VLSUserTrackingManager shareManager] trackingOpenAPPTime:[self openTime] backOrFore:@"firstOpen"];
    [[VLSUserTrackingManager shareManager] trackingWelcomeApp];
    [[VLSUserTrackingManager shareManager] trackingActivate];
    
    LAUNCHOPTIONS = launchOptions;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:isFirstStart]) {//检测引导页
        VLSIntroduceViewController* introduceVC = [VLSIntroduceViewController new];
        introduceVC.delegate = self;
        self.window.rootViewController = introduceVC;
    } else {
        [self showIntroduceController:launchOptions];
    }
       /***************以下内容为远程推送配置代码*****************/
    
    double version = [[UIDevice currentDevice].systemVersion doubleValue];
    
    if(version>=8.0f){
        //1.Badge 角标   2.Sound 声音    3.Alert  弹窗效果
        UIUserNotificationSettings *setting = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge |UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
        //注册推送通知
        [application registerUserNotificationSettings:setting];
        [application registerForRemoteNotifications];
        
    }else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
    
    /***************以下内容为 Umeng 登录分享集成板块********************/
    
    [[UMSocialManager defaultManager] setUmSocialAppkey:APPKEY_UMENG];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:WX_APPID appSecret:WX_APPSCRECT redirectURL:REDIRECT_URL];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:QQ_APPID appSecret:nil redirectURL:REDIRECT_URL];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:SINA_APPKEY appSecret:SINA_APPSCRECT redirectURL:REDIRECT_URL];
    
    /***************以下内容为 Umeng 应用统计分析配置********************/
    
    [MobClick setAppVersion:XcodeAppVersion];
    UMConfigInstance.appKey = APPKEY_UMENG;
    [MobClick startWithConfigure:UMConfigInstance];

    //默认将AVAudioSession设置为激活状态
    NSError* error = nil;
    //设置audio session的category
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    //启用audio session
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    
    //切片Manager
    [VLSAOPManager hookManager];
    
    //加载礼物
    [VLSGifManager downloadGif];
    
    return YES;
}

#pragma mark - 展示引导视图
- (void)showIntroduceController:(NSDictionary *)launchOptions
{
//    [self showAdvertisementViewController];
    /*
    if ([AccountManager sharedAccountManager].account.token) {
        
        [self getUserInfo];
        
        if ([AccountManager sharedAccountManager].account.tenderSig) {
            if (![[VLSMessageManager sharedManager] GetLoginState]) {
                [[VLSMessageManager sharedManager] IMLogin];//重连IM
            }
        }
        [self showTabBarController];
        NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if(userInfo){
            //推送信息
            //            [self userinfoHandler:userInfo];
            self.remoteInfo = userInfo;
        }
    }else{
        [self showLoginController];
    }
     */
    AdvertisementModel *model = [AdvertisementViewController getSplashData];
    BOOL __block hasImage = NO;
    if (model) {
        [model.coopenImages enumerateObjectsUsingBlock:^(CoopenImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.image) {
                hasImage = YES;
            }
        }];
    }
    if (hasImage) {
        [self showAdvertisementViewController];
    }else {
        [self judgeLogin:launchOptions];
    }
}

- (void)judgeLogin:(NSDictionary*)launchOptions {
    if ([AccountManager sharedAccountManager].account.token) {
        
        [self getUserInfo];
        
        if ([AccountManager sharedAccountManager].account.tenderSig) {
            if (![[VLSMessageManager sharedManager] GetLoginState]) {
                [[VLSMessageManager sharedManager] IMLogin];//重连IM
            }
        }
        [self showTabBarController];
        NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if(userInfo){
            //推送信息
            //            [self userinfoHandler:userInfo];
            self.remoteInfo = userInfo;
        }
    }else{
        [self showLoginController];
    }
}

#pragma mark - 展示登陆视图
- (void)showLoginController
{
    // 高通的代码
    NSAssert(self.remoteInfo == nil, @"远程推送的remoteInfo没有清除");
    self.remoteInfo = nil;
    // 删除用户数据
    [VLSDBHelper DeleteAllDB];
    [[AccountManager sharedAccountManager] signOut];
    [[VLSReConnectionManager sharedManager] deleteRoomIDandAnchorID];
    
    //增加逻辑判断：如果当前用户正在直播或者正在连线，则被踢出时需同时关闭直播间
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOGINOUTED object:nil];
    
    VLSLoginViewController *loginVC = [[VLSLoginViewController alloc] init];
    VLSLittleNavigationController *nav = [[VLSLittleNavigationController alloc] initWithRootViewController:loginVC];
    self.window.rootViewController = nav;
}

#pragma mark - 相应账号是否在其他设备登录 alterview
- (void)ShowAlterView:(NSString *)str
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:nil message:str preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* cancleAction = [UIAlertAction actionWithTitle:LocalizedString(@"INVITE_CANCEL") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
#pragma mark 登录界面
        [self showLoginController];
    }];
    UIAlertAction* LoginAction = [UIAlertAction actionWithTitle:LocalizedString(@"LOGIN_AGAIN") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (![[VLSMessageManager sharedManager] GetLoginState]) {
            [[VLSMessageManager sharedManager] IMLogin];//重连IM
        }
    }];
    [alertController addAction:cancleAction];
    [alertController addAction:LoginAction];
    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - 封停账号
- (void)accountClosure:(NSString *)content
{
    [MBProgressHUD showMessag:content toView:self.window];
    [self showLoginController];
}

#pragma mark - advertisementViewController 
- (void)showAdvertisementViewController {
    AdvertisementViewController *adVC = (AdvertisementViewController*)[[UIStoryboard storyboardWithName:@"AdvertisementStoryboard" bundle:nil] instantiateInitialViewController];
    WS(ws)
    adVC.skipBlock = ^{
        /*
        if ([AccountManager sharedAccountManager].account.token) {
            
            [self getUserInfo];
            
            if ([AccountManager sharedAccountManager].account.tenderSig) {
                if (![[VLSMessageManager sharedManager] GetLoginState]) {
                    [[VLSMessageManager sharedManager] IMLogin];//重连IM
                }
            }
            [self showTabBarController];
            NSDictionary* userInfo = [LAUNCHOPTIONS objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
            if(userInfo){
                //推送信息
                //            [self userinfoHandler:userInfo];
                self.remoteInfo = userInfo;
            }
        }else{
            [self showLoginController];
        } 
         */
        [ws judgeLogin:LAUNCHOPTIONS];
    };
    [self.window setRootViewController:adVC];
}

#pragma mark - 展示tab视图
- (void)showTabBarController
{
    // 心跳设置
    [self getHeartConfig];
    self.window.rootViewController = nil;
    VLSTabBarController *tabBarController = [[VLSTabBarController alloc] init];
    
    [self.window setRootViewController:tabBarController];
    //上传设备信息
    [self postDeviceInformation];
    //登录成功获取用户最新V钻
    [[VLSIAPManager sharedInstance] loadCurrentBlance:nil];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    // p3 -- RoomId   p4 -- UserId
    if ([url.scheme isEqualToString:@"com.vliveshow.net"]) {
        
        NSDictionary *dict= [self parseURLParams:url.query];
        
        
        NSString *roomid = [dict objectForKey:@"p3"];
        
        if ([[dict objectForKey:@"p2"] isEqualToString:@"LiveBefore"]) {
            
            // 直播前分享
            
            [self getPlayerRoomidWithUserID:dict[@"p4"] roomID:roomid];
        }else if ([[dict objectForKey:@"p2"] isEqualToString:@"LiveProcess"]){
            // 直播中分享
            
            [self getPlayerRoomidWithUserID:dict[@"p4"] roomID:roomid];
            
        }else if ([[dict objectForKey:@"p2"] isEqualToString:@"LiveAfter"]){
            
            // 直播结束后分享
    
            [self getPlayerRoomidWithUserID:dict[@"p4"] roomID:roomid];
            
        }
    }
    if (self.presentLivingController) {
        self.presentLivingController();
    }
    
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (result == FALSE) {
        //调用其他SDK，例如支付宝SDK等
    }
    return result;
}

//获取主播roomid
- (void)getPlayerRoomidWithUserID:(NSString *)userID roomID:(NSString*)roomid
{
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        MyLog(@"result = %@", result);
        if ([result isKindOfClass:[NSNull class]]) {
            // 不存在 roomID，跳转到直播结束页面 (LiveOffScreenView)
            if (roomid.length <=1) {
                ShareLiveOfView *liveOffScreemView = [[ShareLiveOfView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                liveOffScreemView.userID = userID;
                [liveOffScreemView reloadContent];
                [self.window addSubview:liveOffScreemView];
                
            } else {
                ManagerCallBack *callback = [[ManagerCallBack alloc] init];
                callback.updateBlock = ^(id result){
                    
                    NSString *num = [result stringValue];
                    ShareLiveOfView *liveOffScreemView = [[ShareLiveOfView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                    
                    liveOffScreemView.userID = userID;
                    liveOffScreemView.seeNum = num;
                    [liveOffScreemView reloadContent];
                    
                    [self.window addSubview:liveOffScreemView];
                    
                };
                callback.errorBlock = ^(id result){
                    
                };
                
                [VLSLiveHttpManager getWatchNum:roomid callback:callback];
                
                
            }
            
            
        } else {
            
            NSString *currentUserID = [NSString stringWithFormat:@"%ld", [AccountManager sharedAccountManager].account.uid];
            if (![userID isEqualToString:currentUserID]) {
                // 存在 roomID，跳转到直播间（VLSShowViewController）
                
                VLSTabBarController *tabBar = (VLSTabBarController *)self.window.rootViewController;
                VLSNavigationController *nav = tabBar.viewControllers.firstObject;
                
                // 防止点击多次分享 URL，造成直播间重叠
                BOOL flag = NO;
                
                for (UIViewController *vc in nav.viewControllers) {
                    if ([vc isKindOfClass:[VLSShowViewController class]]) {
                        
                        VLSShowViewController *show = (VLSShowViewController *)vc;
                        [show reloadLiveWidthRoomId:result anchorId:userID];
                        
                        flag = YES;
                        
                        VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
                        joinRoom.roomID = result;
                        joinRoom.joinType = @"share";
                        show.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];
                    }
                }
                
                if (!flag) {
                    
                    VLSShowViewController *show = [[VLSShowViewController alloc] init];
                    show.hidesBottomBarWhenPushed = YES;
                    show.roomId = result;
                    show.anchorId = userID;
                    
                    VLSJoinRoomModel *joinRoom = [[VLSJoinRoomModel alloc] init];
                    joinRoom.roomID = result;
                    joinRoom.joinType = @"share";
                    show.roomTrackContext = [[VLSUserTrackingManager shareManager] trackingJoinroom:joinRoom];
                    
                    [nav pushViewController:show animated:YES];
                }
                
                
            }
        }
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            
        }
    };
    
    [VLSLiveHttpManager getRoomId:userID callback:callback];
}


/**
 * 解析URL参数的工具方法。
 */
- (NSDictionary *)parseURLParams:(NSString *)query{
    
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    for (NSString *pair in pairs) {
        
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        
        if (kv.count == 2) {
            
            NSString *val =[[kv objectAtIndex:1] stringByRemovingPercentEncoding];
            [params setObject:val forKey:[kv objectAtIndex:0]];
            
        }
        
    }
    
    return params;
    
}

/*******************远程推送回调方法***************************/

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    if (application.applicationState == UIApplicationStateActive) {
        return ;
    }
    [self userinfoHandler:userInfo];
    
    application.applicationIconBadgeNumber = 0;
    
    
}

// 推送消息处理
- (void)userinfoHandler:(NSDictionary *)userInfo
{
    
    // 收到IM推送消息
    NSAssert(userInfo[@"ext"], @"推送消息没有扩展字段");
    NSString *extstr = userInfo[@"ext"];
    
    NSArray* array = [extstr componentsSeparatedByString:VLS_SEPERATOR];
    
    if (array.count == 1) {
        [self toTheMail];
    }else{
        
        NSString *dictstr = [extstr substringFromIndex:4];
        NSDictionary *dict = [VLSMessageManager parseJSONStringToNSDictionary:dictstr];
        NSAssert(dict, @"字典解析错误");
        int cmd = [array[0] intValue];
        if (cmd == 202) {
            // 进入直播间
            [self toTheRoom:dict];
        }
    }
}

/**
 *  跳转到站内信
 */
- (void)toTheMail
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"mail" object:nil userInfo:nil];
}

/**
 *  跳转到直播间
 *
 *  @param model 包含直播信息的model
 */
- (void)toTheRoom:(NSDictionary *)dict
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"JoinRoom" object:nil userInfo:dict];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *) error {
    NSLog(@"Registfail%@",error);
}
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    //这里的Token就是要告诉服务端的Token码
    NSLog(@"deviceToken%@",deviceToken);
    
    // 获取设备token 上传至腾讯云
    //    NSString *token = [NSString stringWithFormat:@"%@", deviceToken];
    TIMTokenParam * param = [[TIMTokenParam alloc] init];
    
    [param setToken:deviceToken];
    [param setBusiId:BUSID];
    [[TIMManager sharedInstance] setToken:param];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    TIMBackgroundParam * param = [[TIMBackgroundParam alloc] init];
    [param setC2cUnread:0];
    
    [[TIMManager sharedInstance] doBackground:param succ:^() {
        NSLog(@"Succ");
    } fail:^(int code, NSString * err) {
        NSLog(@"Fail: %d->%@", code, err);
    }];
    
    [[VLSUserTrackingManager shareManager] trackingOpenAPPTime:[self openTime] backOrFore:@"enterBackground"];
    [[VLSUserTrackingManager shareManager] trackingEnterIntoBackground];
}

- (NSString *)openTime{

    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY/MM/dd hh:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    NSLog(@"大个子的时间dateString:%@",dateString);
    return dateString;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if (self.presentLivingController) {
        self.presentLivingController();
    }
    self.presentLivingController = nil;
    if (!self.isAlert) {
        
        [self getHeartConfig];

    }
//    [self doubleUpAppWithConfigResult:[VLSHeartManager shareHeartManager].heartModel.doubleup];
    [[VLSLocationManager shareManager] loadLocation];

    [[TIMManager sharedInstance] doForeground];
    
    [[VLSUserTrackingManager shareManager] trackingOpenAPPTime:[self openTime] backOrFore:@"enterForeground"];

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
    [[VLSIAPManager sharedInstance] removeObserverAfterTerminated];
    [[VLSUserTrackingManager shareManager] trackingTerminate];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "vliveshow.VLiveShow" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"VLiveShow" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"VLiveShow.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

-(void)getNetWorkNotificationCenter
{
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:{
                //   [CHObjectClass showMastToast:@"网络无连接!"];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"4G" object:@"nocontect"];
                });
                
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWiFi:{
                //    [CHObjectClass showMastToast:@"您正在使用WIFI网络"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"4G" object:@"wifi"];
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWWAN:{
                //    [CHObjectClass showMastToast:@"您正在使用4G/3G网络"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"4G" object:@"is4G"];
                break;
            }
            default:
                break;
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
}

#pragma mark - 获取心跳信息配置
- (void)getHeartConfig {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        [VLSHeartManager shareHeartManager].heartModel = result;
        NSDictionary *dic = [VLSHeartManager shareHeartManager].heartModel.doubleup;
        [self doubleUpAppWithConfigResult:dic];
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
            
        }
    };
    [AccountManager getHeartConfigCallback:callback];
}

- (void)doubleUpAlertWithMessage:(NSString *)msg AndURL:(NSString *)url {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
    // 确定按钮
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }];
    
    
    [alertController addAction:okAction];
    
    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
}

- (void)doubleUpWithMessage:(NSString *)msg AndURL:(NSString *)url {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
    // 确定按钮
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"CANCLE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.isAlert = YES;
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
}

- (void)doubleUpWithModel:(VLSDoubleUpModel *)upModel {
    self.isPopup = YES;
    if ([upModel.type isEqualToString:@"forcing"]) {
        
        [self doubleUpAlertWithMessage:upModel.msg AndURL:upModel.ok_url];
    } else if ([upModel.type isEqualToString:@"warning"]) {
        
        [self doubleUpWithMessage:upModel.msg AndURL:upModel.ok_url];
    } else {
        
        [self doubleUpAlertWithMessage:upModel.msg AndURL:upModel.ok_url];
    }
}

- (void)testWithArray:(NSArray *)array WithVersion:(NSString *)version {
    
    for (NSDictionary *dic in array) {
        VLSDoubleUpModel *upModel = [[VLSDoubleUpModel alloc] init];
        [upModel setValuesForKeysWithDictionary:dic];
        
        BOOL compareResult = [self compareVersionWithLocalVersion:version AndServeVersion:upModel.max];
        if (!compareResult) {
            
            [self doubleUpWithModel:upModel];
            
            break;
            
        }
        
    }
}
 

#pragma mark -
- (void)doubleUpAppWithConfigResult:(NSDictionary *)result {
    
    VLSLanguageModel *model = [[VLSLanguageModel alloc] init];
    [model setValuesForKeysWithDictionary:result];
    
    NSString *language = [[NSLocale preferredLanguages] firstObject];
    NSString *version = [NSString stringWithFormat:@"%@", [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleShortVersionString"]];

    if ([language containsString:@"Hant"] || [language containsString:@"HK"] || [language containsString:@"TW"]) {
        
        [self testWithArray:model.tw WithVersion:version];
        
        
    } else {
        
        [self testWithArray:model.en WithVersion:version];

        
    }
    
}

#pragma mark - 上传设备信息

- (void)postDeviceInformation
{
    
    if (![AccountManager isPost]) {
        
    } else {
        
        ManagerCallBack  * callback = [[ManagerCallBack alloc] init];
        callback.updateBlock = ^(id result) {
            DeviceModel *deviceModel = [AccountManager getDeviceInformation];
            // 存数据库
            [deviceModel saveToDB];
            
        };
        callback.errorBlock = ^(id result) {
        };
        
        [VLSLiveHttpManager postDeviceInformation:callback];
        
    }
}


#pragma mark 获取用户信息

- (void)getUserInfo {
    
    ManagerCallBack *callback = [[ManagerCallBack alloc] init];
    callback.updateBlock = ^(id result){
        
        
    };
    callback.errorBlock = ^(id result){
        if ([result isKindOfClass:[ManagerEvent class]]) {
           
        }
    };
    [AccountManager getUserProfileCallback:callback];
}

- (BOOL)compareVersionWithLocalVersion:(NSString *)localVersion AndServeVersion:(NSString *)serveVersion{
    
    if ([localVersion compare:serveVersion options:NSNumericSearch] != NSOrderedAscending) {
        
        return YES;
        
    } else {
        return NO;
    }
}

#pragma mark - VLSIntroduceViewControllerDelegate
- (void)jointAction
{
    [self showLoginController];
}

@end
