//
//  AppDelegate.h
//  VLiveShow
//
//  Created by Cavan on 16/5/6.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "GTVersion.h"

#import "VLSUserTrackingManager.h"

typedef void (^PresentLivingController)(void);

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

// 未启动状态下的收到远程推送，保存在属性里
@property (strong, nonatomic) NSDictionary *remoteInfo;
// 是否给与版本更新提示
@property (assign, nonatomic) BOOL isAlert;
// 版本提示框是否出现过
@property (assign, nonatomic) BOOL isPopup;
/// 全局记录国家区号
@property (nonatomic, strong)NSString *countryStr;

/// 网络状态值
@property (nonatomic, assign)AFNetworkReachabilityStatus networkStatus;
@property (nonatomic, copy) PresentLivingController presentLivingController;

#pragma mark - 展示登陆视图
- (void)showLoginController;
#pragma mark - 展示tab视图
- (void)showTabBarController;
#pragma mark - 相应账号是否在其他设备登录 alterview
- (void)ShowAlterView:(NSString *)str;
#pragma mark - 封停账号
- (void)accountClosure:(NSString *)content;

- (void)getHeartConfig;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

