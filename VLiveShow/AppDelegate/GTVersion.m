//
//  GTVersion.m
//  VLiveShow
//
//  Created by SuperGT on 16/8/31.
//  Copyright © 2016年 vliveshow. All rights reserved.
//

#import "GTVersion.h"

@implementation GTVersion

+ (void)checkNewfeatures
{
    
    // 取出当前软件版本号
    NSString *curVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
    NSLog(@"Current Version -- %@",curVersion);
    
    // 取出上次手动存储的版本号
    NSString *oldVersion = [[NSUserDefaults standardUserDefaults] objectForKey:@"VersionKey"];
    NSLog(@"Old Version -- %@",oldVersion);
    
    // 比对当前软件是否第一次打开
    if ([curVersion isEqual:oldVersion]){
        NSLog(@"没有新特性");
    } else {
        NSLog(@"有新特性");
        // 手动将当前版本存入偏好设置
        [[NSUserDefaults standardUserDefaults] setValue:curVersion forKey:@"VersionKey"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:isFirstShow];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:isFirstWatch];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:isFirstStart];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    
}

@end
