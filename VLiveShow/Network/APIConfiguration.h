//
//  APIConfiguration.h
//  Stacks
//
//  Created by 李雷凯 on 15/10/27.
//  Copyright © 2015年 LK. All rights reserved.
//  接口

#import <Foundation/Foundation.h>
@interface APIConfiguration : NSObject

@property (nonatomic, strong) NSString *domain;

// 验证手机号是否存在
@property (nonatomic, copy)NSString *phoneExistUrl;

/// 手机号注册
@property (nonatomic, copy)NSString *registerUrl;

//手机号绑定
@property (nonatomic, copy) NSString *phoneBindUrl;

// 手机验证码登入 POST
@property (nonatomic, copy)NSString *signInUrl;
/// 重置密码后登陆
@property (nonatomic, copy)NSString *resetPwdLoginUrl;
/// 密码登录
@property (nonatomic, copy)NSString *passwordUrl;
// 验证当前用户密码
@property (nonatomic, copy)NSString *verifyCurrentPwdUrl;
// 验证验证码
@property (nonatomic, copy)NSString *verifyVerCodeUrl;
/// 第三方账号登录
@property (nonatomic, copy)NSString *thirdPlatformLoginUrl;
// 获取主播标签列表
@property (nonatomic, copy)NSString *getHostMarkListUrl;

//登出
@property (nonatomic, copy)NSString *logOutUrl;

// 请求发送手机验证码 GET
@property (nonatomic, copy)NSString *sendVerCodeUrl;

// 采集用户信息 PUT
@property (nonatomic, copy)NSString *putUserInfoUrl;
// 获取直播列表
@property (nonatomic, copy)NSString *getLiveListUrl;
//获取直播列表中的广告列表
@property (nonatomic, copy)NSString *getAdListUrl;
//获取直播列表中的课程列表
@property (nonatomic, copy)NSString *getCourseListUrl;

//获取直播信息
@property (nonatomic, copy)NSString *getLiveInfoUrl;

// 获取自己的信息
@property (nonatomic, copy)NSString *getMineUserInfoUrl;

// 获取用户名片信息
@property (nonatomic, copy)NSString *getUserInfoUrl;
// 获取腾讯云签名信息
@property (nonatomic, copy)NSString *getTenderSigUrl;
// 获取心跳信息
@property (nonatomic, copy)NSString *getHeartConfigUrl;


//闯进直播
@property (nonatomic, copy)NSString *getCreateLive;

//退出直播间
@property (nonatomic, copy)NSString *destroyLiveUrl;
//关注列表
@property (nonatomic, copy)NSString *getAttentListUrl;
// 获取关注人员直播情况
@property (nonatomic, copy)NSString *getFocusListUrl;
//粉丝列表
@property (nonatomic, copy)NSString *getFanstListUrl;
//用户关注
@property (nonatomic, copy)NSString *focusUrl;

//取消关注
@property (nonatomic, copy)NSString *cansalFocusUrl;
//获取用户关注list
@property(nonatomic,copy)NSString *userFoucesList;
//获取用户黑名单列表
@property (nonatomic,copy)NSString *userBlackList;
//加入黑名单
@property (nonatomic,copy)NSString *addBlackList;
//取消黑名单
@property (nonatomic,copy)NSString *cancelBlackList;
//互动直播url｀｀
@property (nonatomic, copy)NSString *AVInteractionUrl;
//互动直播请求列表
@property (nonatomic, copy)NSString *AVLinkRequestList;

@property (nonatomic, copy)NSString *signUpUrl;
@property (nonatomic, copy)NSString *sendRecoveryEmailUrl;
/// 初始化密码
@property (nonatomic, copy)NSString *chushiPwdUrl;
@property (nonatomic,copy) NSString *getEndPageInfo;
//互动直播url
//@property (nonatomic, copy)NSString *AVInteractionUrl;
@property (nonatomic, copy) NSString *getBaseInfoUrl;
//心跳上传
@property (nonatomic, copy)NSString *hertBeatUrl;
//用户举报
@property (nonatomic,copy)NSString*reportUrl;
//获取直播用户roomid
@property (nonatomic,copy)NSString*getRoomID;
//获取两者之间关系
@property (nonatomic,copy)NSString*getrelationship;
//传设备信息到后台
@property (nonatomic,copy)NSString*postDeviceInformation;

//上传tracking日志
@property (nonatomic,copy)NSString*postTrackingInformation;
// 用户分享行为跟踪
@property (nonatomic,copy)NSString*behaviorTracking;
//动态key
@property (nonatomic,copy) NSString *RDK;

//上传接收直播提醒信息
@property (nonatomic, copy) NSString *postLiveNoticeUrl;
@property (nonatomic, copy) NSString *getLiveNoticeUrl;

//获取v票排行榜列表
@property (nonatomic,copy)NSString * getRankingList;

// 获取推荐关注列表
@property (nonatomic, copy)NSString *getRecommendListsUrl;

/* 获取礼物列表 */
@property (nonatomic,copy) NSString *getGiftListUrl;

/* 发送礼物 */
@property (nonatomic,copy) NSString *sendGiftUrl;

@property (nonatomic, copy) NSString *getLiveAudiues;

@property (nonatomic, copy) NSString *getWatchNum;

@property (nonatomic, copy) NSString *getAllRelationship;

@property (nonatomic, copy) NSString *getSearchRelationship;
/// 首页关注页面搜索 API
@property (nonatomic, copy)NSString *searchUserUrl;
/**
 *  获取申请状态
 */
@property (nonatomic, copy) NSString *getHostApplicationStatus;

/**
 *  确认申请审批结果
 */
@property (nonatomic, copy) NSString *applicationConfirm;

/**
 *  获取主播申请资料
 */
@property (nonatomic, copy) NSString *getHostApplication;

/**
 *  提交主播申请资料
 */
@property (nonatomic, copy) NSString *postHostApplication;

/**
 *  主播申请图片上传
 */
@property (nonatomic, copy) NSString *postApplicationImage;
/**
 *  主播申请图片删除
 */
@property (nonatomic, copy) NSString *deleteApplicationImage;
@property (nonatomic, copy) NSString *getIAPProductsUrl;

@property (nonatomic, copy) NSString *getMoneyPaymentUrl;

@property (nonatomic, copy) NSString *getUserMoneyUrl;

@property (nonatomic, copy) NSString *uploadIPAReceiptUrl;

@property (nonatomic, copy) NSString *trackActivateUrl;

/******************** 新增课程api *********************begin*/
//我的发布
@property (nonatomic, copy) NSString* coursePublishUrl;
//我的预约
@property (nonatomic, copy) NSString* courseBookingUrl;
//课程列表
@property (nonatomic, copy) NSString* courseListUrl;
//主播下一节课的时间
@property (nonatomic, copy) NSString* courseTimingUrl;
//退款
@property (nonatomic, copy) NSString* coursePaybackUrl;
@property (nonatomic, copy) NSString* lessonRatingUrl;

@property (nonatomic, copy) NSString* liveShowRatingUrl;
//获取roomId
@property (nonatomic, copy) NSString* courseRoomIdUrl;
// 老师发布的未开始的课程
@property (nonatomic, copy) NSString* userPublishUrl;
//我的计划
@property (nonatomic, copy) NSString* scheduleUrl;
/******************** 新增课程api *********************end*/

/******************** 投票 *********************begin*/
@property (nonatomic, copy) NSString* voteUrl;
/******************** 投票 *********************end*/

+ (APIConfiguration *)sharedAPIConfiguration;

/******************** Channel相關 *********************begin*/
@property (nonatomic, copy) __deprecated NSString* channelListUrl;
    
@property (nonatomic, copy) __deprecated NSString* channelContentsUrl;


@property (nonatomic, copy) NSString* channelInfoUrl;
    
@property (nonatomic, copy) NSString* channelDetailsUrl;
//反馈
@property (nonatomic, copy) NSString* feedbackUrl;

@end
