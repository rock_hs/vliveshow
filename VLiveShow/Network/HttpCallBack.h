//
//  HttpCallBack.h
//  Stacks
//
//  Created by 李雷凯 on 15/10/27.
//  Copyright © 2015年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^HttpBasicBlock)(void);
typedef void (^HttpResultBlock)(id result);
typedef void (^HttpProgressBlock)(unsigned long long size, unsigned long long total);

@interface HttpCallBack : NSObject


@property (nonatomic, copy) HttpBasicBlock startBlock;
@property (nonatomic, copy) HttpResultBlock doneBlock;
@property (nonatomic, copy) HttpResultBlock errorBlock;
@property (nonatomic, copy) HttpProgressBlock downProgressBlock;
@property (nonatomic, copy) HttpProgressBlock uploadProgressBlock;

@end
