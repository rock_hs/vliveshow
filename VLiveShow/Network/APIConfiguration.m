//
//  APIConfiguration.m
//  Stacks
//
//  Created by 李雷凯 on 15/10/27.
//  Copyright © 2015年 LK. All rights reserved.
//

#import "APIConfiguration.h"

@implementation APIConfiguration


- (NSString *)APIUrlWithPath:(NSString *)path
{
    return [NSString stringWithFormat:@"%@%@",self.domain,path];
}

//初始化设置
- (void)writeDefaultConfiguration
{
    self.domain = BASE_URL;

    self.phoneBindUrl               = [self APIUrlWithPath:@"/v1/users/phone/bind"];
    self.phoneExistUrl              = [self APIUrlWithPath:@"/v1/users/phone/exist"];
    self.registerUrl                = [self APIUrlWithPath:@"/v1/users/phone/register"];
    self.signInUrl                  = [self APIUrlWithPath:@"/v1/users/phone/verifycode_login"];
    self.resetPwdLoginUrl           = [self APIUrlWithPath:@"/v1/users/phone/password_reset_then_login"];
    self.passwordUrl                = [self APIUrlWithPath:@"/v1/users/phone/password_login"];
    self.chushiPwdUrl               = [self APIUrlWithPath:@"/v1/users/phone/init_pwd"];
    self.thirdPlatformLoginUrl      = [self APIUrlWithPath:@"/v1/users/thirdparty/login"];
    self.logOutUrl                  = [self APIUrlWithPath:@"/v1/users/logout"];
    self.verifyCurrentPwdUrl        = [self APIUrlWithPath:@"/v1/users/check_password"];
    self.verifyVerCodeUrl           = [self APIUrlWithPath:@"/v1/users/phone/check_verification_code"];
    self.sendVerCodeUrl             = [self APIUrlWithPath:@"/v1/users/phone/send_verification_code"];
    self.putUserInfoUrl             = [self APIUrlWithPath:@"/v1/users/profile/base"];
    self.getMineUserInfoUrl         = [self APIUrlWithPath:@"/v1/users/profile/base"];
    self.getHostMarkListUrl         = [self APIUrlWithPath:@"/v1/host/topics"];

    self.getUserInfoUrl             = [self APIUrlWithPath:@"/v1/users/get_base_info"];
    self.getLiveListUrl             = [self APIUrlWithPath:@"/v1/liveshow/list"];
    self.getAdListUrl               = [self APIUrlWithPath:@"/v1/config/adspace/list"];
    self.getCourseListUrl            = [self APIUrlWithPath:@"/v1/course/list"];
    self.getLiveInfoUrl             = [self APIUrlWithPath:@"/v1/liveshow"];
    self.getTenderSigUrl            = [self APIUrlWithPath:@"/v1/tencent/signature"];
    self.getHeartConfigUrl          = [self APIUrlWithPath:@"/v1/liveshow/config"];
    self.getCreateLive              = [self APIUrlWithPath:@"/v1/liveshow/create"];
    self.destroyLiveUrl             = [self APIUrlWithPath:@"/v1/liveshow/"];
    self.getFanstListUrl            = [self APIUrlWithPath:@"/v1/users/fans/list"];
    self.getAttentListUrl           = [self APIUrlWithPath:@"/v1/users/stars/list"];
    self.getFocusListUrl            = [self APIUrlWithPath:@"/v1/liveshow/followed/list"];
    self.getRecommendListsUrl       = [self APIUrlWithPath:@"/v1/users/hosts/recommended/list"];
    self.focusUrl                   = [self APIUrlWithPath:@"/v1/users/follow"];
    self.cansalFocusUrl             = [self APIUrlWithPath:@"/v1/users/unfollow"];
    self.userBlackList              = [self APIUrlWithPath:@"/v1/users/blacklist/list"];
    self.addBlackList               = [self APIUrlWithPath:@"/v1/users/blacklist/create"];
    self.cancelBlackList            = [self APIUrlWithPath:@"/v1/users/blacklist/cancel"];
    
    self.AVInteractionUrl           = [self APIUrlWithPath:@"/v1/liveshow/interact"];
    self.AVLinkRequestList          = [self APIUrlWithPath:@"/v1/liveshow/host/link_request_list"];
    self.hertBeatUrl                = [self APIUrlWithPath:@"/v1/liveshow/heart_beat"];
     self.reportUrl                 = [self APIUrlWithPath:@"/v1/users/report/send"];
    self.getRoomID                  = [self APIUrlWithPath:@"/v1/liveshow/get_live_roomid"];
     self.getrelationship           = [self APIUrlWithPath:@"/v1/users/relationship"];
    self.postDeviceInformation      = [self APIUrlWithPath:@"/v1/users/device_info/collection"];
    self.behaviorTracking           = [self APIUrlWithPath:@"/v1/users/tracking"];
    self.getGiftListUrl = [self APIUrlWithPath:@"/v1/config/gift/list"];
    self.sendGiftUrl = [self APIUrlWithPath:@"/v1/gift/sendgift"];
    self.RDK = [self APIUrlWithPath:@"/v1/key/record/create"];
    self.postLiveNoticeUrl = [self APIUrlWithPath:@"/v1/users/extension/save"];
    self.getLiveNoticeUrl = [self APIUrlWithPath:@"/v1/users/extension/get"];
    self.getEndPageInfo              =[self APIUrlWithPath:@"/v1/liveshow/end_page_info"];
    
    self.getRankingList = [self APIUrlWithPath:@"/v1/money/ranking"];
    self.getHostApplicationStatus  = [self APIUrlWithPath:@"/v1/host/application/status"];
    self.applicationConfirm        = [self APIUrlWithPath:@"/v1/host/application/{applicationId}/confirm"];
    
    
    self.getLiveAudiues = [self APIUrlWithPath:@"/v1/liveshow/current_audiences"];
    self.getWatchNum = [self APIUrlWithPath:@"/v1/liveshow/cumulative_watch_num"];
    self.getAllRelationship = [self APIUrlWithPath:@"/v1/users/related/list"];
    self.getSearchRelationship = [self APIUrlWithPath:@"/v1/users/search/guest/invite"];
    self.searchUserUrl = [self APIUrlWithPath:@"/v1/users/search"];
    
    self.getHostApplication = [self APIUrlWithPath:@"/v1/host/application"];
    self.getIAPProductsUrl = [self APIUrlWithPath: @"/v1/iap/products"];
    self.getUserMoneyUrl = [self APIUrlWithPath: @"/v1/money/user/balance"];
    
    self.getMoneyPaymentUrl = [self APIUrlWithPath: @"/v1/money/payment/list"];
    self.postApplicationImage = [self APIUrlWithPath:@"/v1/host/application/image"];
    self.postHostApplication = [self APIUrlWithPath:@"/v1/host/application"];
    self.deleteApplicationImage = [self APIUrlWithPath:@"/v1/host/application/image"];
    self.uploadIPAReceiptUrl = [self APIUrlWithPath: @"/v1/money/payment/handle"];
    self.postTrackingInformation = [self APIUrlWithPath:@"/v1/users/action/tracking"];//tracking日志API
    self.trackActivateUrl = [self APIUrlWithPath: @"/v1/users/action/activate"];
    
    /******************** 新增课程api *********************begin*/
    self.coursePublishUrl = [self APIUrlWithPath:@"/v1/course/mine"];
    self.courseBookingUrl = [self APIUrlWithPath:@"/v1/course/subscription/mine"];
    self.courseListUrl = [self APIUrlWithPath:@"/v1/course/lessons"];
    self.courseTimingUrl = [self APIUrlWithPath:@"/v1/course/timing/lesson"];
    self.coursePaybackUrl = [self APIUrlWithPath:@"/v1/course/subscription"];
    self.lessonRatingUrl = [self APIUrlWithPath: @"/v1/course/lesson/rating"];
    self.liveShowRatingUrl =  [self APIUrlWithPath: @"/v1/liveshow/rating"];
    self.courseRoomIdUrl = [self APIUrlWithPath:@"/v1/course/lesson/LivingInfo"];
    self.userPublishUrl = [self APIUrlWithPath:@"/v1/course/host/issue"];
    self.scheduleUrl = [self APIUrlWithPath:@"/v1/calendar/list"];
    /******************** 新增课程api *********************end*/
    
    self.channelListUrl = [self APIUrlWithPath: @"/v1/channel/list"];
    self.channelInfoUrl = [self APIUrlWithPath: @"/v1/channel/info"];
    self.channelContentsUrl = [self APIUrlWithPath: @"/v1/channel/%@/list"];
    self.channelDetailsUrl = [self APIUrlWithPath: @"/v1/channel/%@/detail"];
    
    //投票
    self.voteUrl = [self APIUrlWithPath:@"/v1/mamashow/mama/vote"];
    //反馈
    self.feedbackUrl = [self APIUrlWithPath:@"/v1/user/feedback/save"];
}

- (id)init
{
    if (self = [super init]) {
        [self writeDefaultConfiguration];
    }
    return self;
}

+ (APIConfiguration *)sharedAPIConfiguration
{
    static APIConfiguration *sharedAPIConfiguration = nil;
    static dispatch_once_t onceToken;
        
    dispatch_once(&onceToken, ^{
        sharedAPIConfiguration = [[self alloc] init];
    });
    return sharedAPIConfiguration;
}


@end
