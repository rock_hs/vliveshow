//
//  ManagerUtil.m
//  Stacks
//
//  Created by 李雷凯 on 15/10/29.
//  Copyright © 2015年 LK. All rights reserved.
//

#import "ManagerUtil.h"

@implementation ManagerUtil
+(HttpCallBack*)defaultHttpCallBack:(ManagerCallBack*)callback process:(ManagerProcessBlock)processBlock
{
    HttpCallBack* http = [[HttpCallBack alloc] init];
    
    http.startBlock = nil;
    
    http.doneBlock = ^(id result){
        if ([result isKindOfClass:[NSDictionary class]]) {
            NSDictionary* dict = (NSDictionary*)result;
            NSLog(@"result:%@",result);

            BOOL success = [[dict objectForKey:@"success"] boolValue];
            NSInteger code =[[dict objectForKey:@"error_code"] integerValue];
            NSDictionary *results = [dict objectForKey:@"results"];
            
            if (success) {
                //请求成功 回调
                id processResult = processBlock(results);
                callback.updateBlock(processResult);
         
            }else{
                NSString *message=[dict objectForKey:@"message"];

                //请求失败 根据错误代码提示
                if (code == 10001) {
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"SYSTEM_ERROR")];
                    callback.errorBlock(event);
                }else if (code == 20001){
                    ManagerEvent* event = [ManagerEvent expired];
                    callback.errorBlock(event);
                }else if (code == 20002){
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"NICKNAME_ERROR")];
                    callback.errorBlock(event);
                }else if (code == 20003){
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"VERCODE_ERROR")];
                    event.code = 20003;
                    callback.errorBlock(event);
                }else if (code == 20004){
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"PARAM_ERROR")];
                    callback.errorBlock(event);
                }else if (code == 20005){
                    ManagerEvent* event = [ManagerEvent toast:nil title:nil];
                    event.code = 20005;
                    callback.errorBlock(event);
                }else if (code == 20006){
                    ManagerEvent* event = [ManagerEvent alert:nil title:LocalizedString(@"PHONE_EXISTS_ERROR")];
                    callback.errorBlock(event);
                }else if (code == 20008){
                    ManagerEvent* event = [[ManagerEvent alloc] init];
                    event.code = 20008;
                    event.info = message;
                    callback.errorBlock(event);
                }else if (code == 20009){
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"CONTAIN_SENSITIVE_KEYWORD")];
                    event.code = 20009;
                    event.info = message;
                    callback.errorBlock(event);
                } else if (code == 20013) {
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"LOGIN_PAGE_ERROR")];
                    event.code = 20013;
                    event.info = message;
                    callback.errorBlock(event);
                } else if (code == 20011)
                {
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"NO_PERMISSION_TO_BLOCK_USER")];
                    event.code = code;
                    event.info = message;
                    callback.errorBlock(event);
                } else if (code == 20020)
                {
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"CANNOT_BLOCK_SELF")];
                    event.code = code;
                    event.info = message;
                    callback.errorBlock(event);
                }
                else if (code == 20021)
                {
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"USER_HAS_ALREADY_BEEN_BLOCKED")];
                    event.code = code;
                    event.info = message;
                    callback.errorBlock(event);
                }
                else if([message isKindOfClass:[NSNull class]]) {
                    ManagerEvent* event = [ManagerEvent toast:nil title:LocalizedString(@"NULL_ERROR")];
                    callback.errorBlock(event);
                }else{
                    
                    ManagerEvent* event = [ManagerEvent toast:nil title:message];
                    callback.errorBlock(event);
                 
                }
                //过期
//                        ManagerEvent* event = [ManagerEvent expired];
//                        callback.errorBlock(event);

            }
      
        }else {
            if (callback.errorBlock) {
                 ManagerEvent* event = [ManagerEvent toast:LocalizedString(@"ALERT_NETWORK_TRYAGAIN") title:LocalizedString(@"ALERT_NETWORK_ERROR")];
                callback.errorBlock(event);
            }
        }
    };
    
    http.uploadProgressBlock= ^(unsigned long long size, unsigned long long total){
        NSDictionary *dict=@{@"size":[NSNumber numberWithLongLong:size],
                              @"total":[NSNumber numberWithLongLong:total],
                             };
        callback.loadBlock(dict);
    };
    
    http.errorBlock = ^(id result){
        if (callback.errorBlock) {
             NSError *error = result;
            ManagerEvent* event = [ManagerEvent toast:LocalizedString(@"ALERT_NETWORK_TRYAGAIN") title:error.localizedDescription];
            event.error = error;
            MyLog(@"服务器返回的 error 信息：error = %@, error.localizedDescription = %@", error, error.localizedDescription);
            // LocalizedString(@"ALERT_REQUEST_TIMEOUT")
            callback.errorBlock(event);
        }
    };
    
    return http;


}

@end
