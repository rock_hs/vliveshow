//
//  ManagerUtil.h
//  Stacks
//
//  Created by 李雷凯 on 15/10/29.
//  Copyright © 2015年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ManagerCallBack.h"
#import "HttpCallBack.h"
#import "ManagerEvent.h"

@interface ManagerUtil : NSObject
+(HttpCallBack*)defaultHttpCallBack:(ManagerCallBack*)callback process:(ManagerProcessBlock)processBlock;

@end
