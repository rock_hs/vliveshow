//
//  ManagerEvent.m
//  Stacks
//
//  Created by 李雷凯 on 15/10/29.
//  Copyright © 2015年 LK. All rights reserved.
//

#import "ManagerEvent.h"

@implementation ManagerEvent
+(ManagerEvent*)toast:(NSString*)info title:(NSString*)title
{
    ManagerEvent* event = [[ManagerEvent alloc] init];
    event.type = ManagerEventToast;
    event.info = info;
    event.title = title;
    return event;
}

+(ManagerEvent*)alert:(NSString*)info title:(NSString*)title
{
    ManagerEvent* event = [[ManagerEvent alloc] init];
    event.type = ManagerEventAlert;
    event.info = info;
    event.title = title;
    return event;
}

+(ManagerEvent*)load:(NSString*)info title:(NSString*)title
{
    ManagerEvent* event = [[ManagerEvent alloc] init];
    event.type = ManagerEventLoad;
    event.info = info;
    event.title = title;
    return event;
}

+(ManagerEvent*)expired
{
    ManagerEvent* event = [[ManagerEvent alloc] init];
    event.type = ManagerEventExpired;
    return event;
}

+(ManagerEvent*)event:(NSString*)info code:(NSInteger)code
{
    ManagerEvent* event = [[ManagerEvent alloc] init];
    event.info = info;
    event.code = code;
    return event;
}

@end
