//
//  ManagerEvent.h
//  Stacks
//
//  Created by 李雷凯 on 15/10/29.
//  Copyright © 2015年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    ManagerEventToast,          //提示
    ManagerEventAlert,          //弹框
    ManagerEventLoad,           //加载
    ManagerEventExpired,        //过期
} ManagerEventType;

@interface ManagerEvent : NSObject
@property(nonatomic, assign) ManagerEventType type;

@property(nonatomic, assign)NSInteger code;
@property(nonatomic, copy)NSString * info;
@property(nonatomic, copy)NSString * title;
@property (nonatomic,assign)CGSize maxSize;
@property (nonatomic,assign)CGFloat margin;
@property (nonatomic,assign)CGFloat textfont;
@property(nonatomic, strong) NSError* error;

// 通知类事件
+(ManagerEvent*)toast:(NSString*)info title:(NSString*)title;
+(ManagerEvent*)alert:(NSString*)info title:(NSString*)title;

+(ManagerEvent*)load:(NSString*)info title:(NSString*)title;

// 自定义公共事件
+(ManagerEvent*)expired;

+(ManagerEvent*)event:(NSString*)info code:(NSInteger)code;
@end
