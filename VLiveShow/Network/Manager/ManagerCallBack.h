//
//  ManagerCallBack.h
//  Stacks
//
//  Created by 李雷凯 on 15/10/29.
//  Copyright © 2015年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^ManagerBasicBlock)(void);
typedef void (^ManagerResultBlock)(id result);
typedef id   (^ManagerProcessBlock)(NSDictionary* result);

@interface ManagerCallBack : NSObject

@property (nonatomic, copy) ManagerResultBlock loadBlock;
@property (nonatomic, copy) ManagerResultBlock updateBlock;
@property (nonatomic, copy) ManagerResultBlock errorBlock;

@end
