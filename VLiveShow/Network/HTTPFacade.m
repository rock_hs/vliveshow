//
//  HTTPFacade.m
//  Stacks
//
//  Created by 李雷凯 on 15/10/27.
//  Copyright © 2015年 LK. All rights reserved.
//

#import "HTTPFacade.h"
#import "AFNetworking.h"
#import "APIConfiguration.h"
#import "AccountManager.h"
#import "UUIDShortener.h"
#import "VLSLoginRegModel.h"
#import "VLSUserTrackingManager.h"

static NSString *HTTP_HEADER_CLIENT_OS;
static NSString *HTTP_HEADER_VERSION_NAME;
static NSString *HTTP_HEADER_VERSION_CODE;
static NSString *HTTP_HEADER_VERSION_LANGUAGE;

@interface NSLocale(HTTPFacade)
+ (NSString*)currentLanguageString;
@end

@implementation NSLocale(HTTPFacade)

+ (NSString*)currentLanguageString
{
    NSString* lan = [[[NSLocale preferredLanguages] firstObject] lowercaseString];
    if ([lan hasPrefix: @"zh-hans"])
    {
        return @"zh-hans";
    }
    else if ([lan hasPrefix: @"zh-hant"]
             || [lan containsString: @"zh-hk"]
             || [lan containsString: @"zh-tw"]
             )
    {
        return @"zh-hant";
    }
    else if ([lan hasPrefix: @"en"])
    {
        return @"en";
    }
    
    return lan;
}

@end

@implementation HTTPFacade

+(NSDictionary*)defaultParameter
{
    NSString* token =@"1";
    
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        HTTP_HEADER_CLIENT_OS= [UIDevice currentDevice].systemVersion;
        HTTP_HEADER_VERSION_NAME= [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
        HTTP_HEADER_VERSION_CODE = @"1";
        HTTP_HEADER_VERSION_LANGUAGE = [NSLocale currentLanguageString];
        
        
    });
    
    AccountModel* account = [AccountManager sharedAccountManager].account;
    if (account) {
        token = account.token;
    }
    token = token ? token : @"";
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:@"ios" forKey:@"client"];
    [parameter setObject:HTTP_HEADER_CLIENT_OS forKey:@"client_os"];
    [parameter setObject:HTTP_HEADER_VERSION_NAME forKey:@"version_name"];
    [parameter setObject:HTTP_HEADER_VERSION_CODE forKey:@"version_code"];
    [parameter setObject:HTTP_HEADER_VERSION_LANGUAGE forKey:@"client_language"];
    [parameter setObject:token forKey:@"token"];
    
    
    
    
    NSString *version =  [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *bundleId =  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    
    [parameter setObject:version forKey:@"app_version"];
    [parameter setObject:bundleId forKeyedSubscript:@"app_bundleId"];
    
    
    return parameter;
}

+(NSUInteger)download:(NSString *)url folderName:(NSString *)folderName completion:(void (^)())completion{
  
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
  
  NSURL *URL = [NSURL URLWithString:url];
  NSURLRequest *request = [NSURLRequest requestWithURL:URL];
  
  NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    return [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.zip",folderName]];
  } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
    NSLog(@"File downloaded to: %@", filePath);
    if (completion) {
      completion(filePath);
    }
  }];
  [downloadTask resume];
  
  return 0;
}

+(NSUInteger)downGifgiftLoad:(NSString *)url folderName:(NSString *)folderName completion:(void (^)())completion {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.gif", folderName]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (completion) {
            completion(filePath);
        }
    }];
    [downloadTask resume];
    
    return 0;
}

+(NSUInteger)get:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    tag = [self get:url parameter:parameter requestSerializer:@"" callback:callback];
    return tag;
}

+(NSUInteger)get:(NSString*)url parameter:(NSDictionary*)parameter requestSerializer:(id)ser callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 30.0f;
    
    if (ser) {
        manager.requestSerializer =[AFJSONRequestSerializer serializer];
    }
    NSMutableDictionary* para = [ NSMutableDictionary dictionary];
    [para addEntriesFromDictionary:[self defaultParameter]];
    if (parameter) {
        [para addEntriesFromDictionary:parameter];
    }
    
    NSLog(@"%@\n%@",url,para);
  
    [manager GET:url parameters:para progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#ifdef DEBUG
//        [self writeLogToFileWithRequestResult:@"SUCCESS"
//                                   HttpMethod:@"GET"
//                                HttpParameter:parameter
//                                   returnInfo:responseObject];
#else
        
#endif
        NSLog(@"%@\n%@",url,responseObject);
        
        if (callback.doneBlock)
        {
            callback.doneBlock(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error:%@ \n %@",url,error);
        
#ifdef DEBUG
        //        [self writeLogToFileWithRequestResult:@"FAILED"
        //                                   HttpMethod:@"GET"
        //                                HttpParameter:parameter
        //                                   returnInfo:error];
        
#else
        
#endif
        if (callback.errorBlock)
        {
            callback.errorBlock(error);
        }
        
    }];
    
    return tag;
}

#pragma mark - 将拦截到的日志信息存储到本地

+ (void)writeLogToFileWithRequestResult:(NSString *)result HttpMethod:(NSString *)method HttpParameter:(NSDictionary *)pra returnInfo:(id)response {
    
    NSMutableDictionary *logDic = [NSMutableDictionary dictionary];
    
    //    [logDic setObject:result forKey:@"RequestResult"];
    //    [logDic setObject:method forKey:@"HttpMethod"];
    //    [logDic setObject:pra forKey:@"HttpParameter"];
    //    [logDic setObject:response forKey:@"ReturnInfo"];
    //
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *httpLogPath = [cachesPath stringByAppendingPathComponent:@"httplog.txt"];
    MyLog(@"httpLogPath = %@", httpLogPath);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if(![fileManager fileExistsAtPath:httpLogPath]) //如果不存在
    {
        if([logDic writeToFile:httpLogPath atomically:YES])
        {
            NSLog(@"------写入文件------success");
        }
        else
        {
            NSLog(@"------写入文件------fail");
        }
    }
    else//追加写入文件
    {
        NSLog(@"-------文件存在，追加文件----------");
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:httpLogPath];
        
        [fileHandle seekToEndOfFile];  //将节点跳到文件的末尾
        
        
        NSData *stringData  = [logDic mj_JSONData];
        
        [fileHandle writeData:stringData]; //追加写入数据
        
        [fileHandle closeFile];
        
    }
    
    
    MyLog(@"%@", httpLogPath);
    
}

+(NSURLSessionDataTask *)post:(NSString*)url parameter:(NSDictionary*)parameter requestSerializer:(id)ser callback:(HttpCallBack *)callback
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 30.0f;
    
    if (ser) {
        manager.requestSerializer =[AFJSONRequestSerializer serializer];
    }
    
    NSMutableDictionary* para = [ NSMutableDictionary dictionary];
    [para addEntriesFromDictionary:[self defaultParameter]];
    if (parameter) {
        [para addEntriesFromDictionary:parameter];
    }
    NSLog(@"字典内容%@\n%@",url,para);
    
    NSURLSessionDataTask *task = [manager POST:url parameters:para progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"post内容%@\n%@",url,responseObject);
        NSLog(@"字典:%@",parameter);
        callback.doneBlock(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error:%@ \n %@",url,error);
        callback.errorBlock(error);
    }];
    return task;
}
+(NSURLSessionDataTask *)post:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback
{
    NSURLSessionDataTask * task = [self post:url parameter:parameter requestSerializer:nil callback:callback];
    return task;
}

+(NSUInteger)delete:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer =[AFJSONRequestSerializer serializer];
    
    NSMutableDictionary* para = [ NSMutableDictionary dictionary];
    [para addEntriesFromDictionary:[self defaultParameter]];
    if (parameter) {
        [para addEntriesFromDictionary:parameter];
    }
    [manager DELETE:url parameters:para success:^(NSURLSessionDataTask *operation, id responseObject) {
        callback.doneBlock(responseObject);
    }
            failure:^(NSURLSessionDataTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
                callback.errorBlock(error);
            }];
    
    return tag;
}


+ (NSInteger)uploadFile:(NSString*)url file:(PostFileModel *)postFile parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer.timeoutInterval = 30.0f;

    NSMutableDictionary* para = [ NSMutableDictionary dictionary];
    [para addEntriesFromDictionary:[self defaultParameter]];
    if (parameter) {
        [para addEntriesFromDictionary:parameter];
    }
    NSLog(@"%@  %@",para,postFile.fileArray);
    NSString *folderName = [parameter objectForKey:@"folder"];
    
    //上传照片
    [manager POST:url parameters:para constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (int i=0; i<[postFile.fileArray count]; i++) {
            id object=[postFile.fileArray objectAtIndex:i];
            if ([object isKindOfClass:[NSData class]]) {
                [formData appendPartWithFileData:object name:folderName fileName:postFile.fileName mimeType:@"multipart/form-data"];
            }
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"uploadProgress:%@ ",uploadProgress);
        if (callback.uploadProgressBlock) {
            callback.uploadProgressBlock(uploadProgress.completedUnitCount, uploadProgress.totalUnitCount);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject:%@",responseObject);
        callback.doneBlock(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error:%@",error);
        callback.errorBlock(error);
    }];
    
    
    return 0;
}

/** 手机号是否存在 */
+ (NSUInteger)phoneExists:(AccountModel*)account callback:(HttpCallBack *)callback {
    NSUInteger tag = 0;
    
    NSString *url = [APIConfiguration sharedAPIConfiguration].phoneExistUrl;
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    [parameter setObject:account.mobilePhone forKey:@"mobilePhone"];
    [parameter setObject:account.nationCode forKey:@"nationCode"];
    [self get:url parameter:parameter callback:callback];
    
    return tag;
}

+ (NSUInteger)phoneRegister:(AccountModel*)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].registerUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:account.mobilePhone forKey:@"mobilePhone"];
    [parameter setObject:account.verificationCode forKey:@"verificationCode"];
    [parameter setObject:account.nationCode forKey:@"nationCode"];
    [self post:url parameter:parameter callback:callback];
    
    return tag;
}

+ (NSUInteger)verCodeLogin:(AccountModel*)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].signInUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:account.mobilePhone forKey:@"mobilePhone"];
    [parameter setObject:account.verificationCode forKey:@"verificationCode"];
    [parameter setObject:account.nationCode forKey:@"nationCode"];
    
    [self post:url parameter:parameter callback:callback];
    return tag;
}

//绑定手机号
+ (NSUInteger)verCodeBind:(AccountModel *)account callback:(HttpCallBack *)callback{
    
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].phoneBindUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:account.mobilePhone forKey:@"mobilePhone"];
    [parameter setObject:account.verificationCode forKey:@"verificationCode"];
    [parameter setObject:account.nationCode forKey:@"nationCode"];
    [self post:url parameter:parameter callback:callback];
    return tag;
}

/** 重新设置密码后登录 */
+ (NSUInteger)setNewPwdAndLogin:(AccountModel*)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].resetPwdLoginUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:account.mobilePhone forKey:@"mobilePhone"];
    [parameter setObject:account.verificationCode forKey:@"verificationCode"];
    [parameter setObject:account.nationCode forKey:@"nationCode"];
    [parameter setObject:account.resetPwd forKey:@"newPwd"];
    
    
    [self post:url parameter:parameter callback:callback];
    return tag;
}

/** 密码登录 */
+ (NSUInteger)passwordLogin:(AccountModel*)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].passwordUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:account.mobilePhone forKey:@"mobilePhone"];
    [parameter setObject:account.password forKey:@"password"];
    [parameter setObject:account.nationCode forKey:@"nationCode"];
    
    
    [self post:url parameter:parameter callback:callback];
    return tag;
}

/** 验证当前用户密码 */
+ (NSUInteger)verifyCurrentPwd:(AccountModel*)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].verifyCurrentPwdUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:account.password forKey:@"password"];
    
    [self post:url parameter:parameter callback:callback];
    return tag;
}
/** 验证验证码是否正确 */
+ (NSUInteger)verifyVerCode:(AccountModel*)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].verifyVerCodeUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:account.mobilePhone forKey:@"mobilePhone"];
    [parameter setObject:account.verificationCode forKey:@"verificationCode"];
    [parameter setObject:account.nationCode forKey:@"nationCode"];

    [self post:url parameter:parameter callback:callback];
    return tag;
}

/* 获取礼物列表 */
+ (NSInteger)getGiftList:(HttpCallBack *)callback{
  
  NSInteger tag = 0;
  
  NSString * url = [APIConfiguration sharedAPIConfiguration].getGiftListUrl;
  NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
  //  [self post:url parameter:parameter callback:callback];
  [self get:url parameter:parameter callback:callback];
  return tag;
  
}

/** 第三方登录 */
+ (NSUInteger)thirdPlatformLogin:(AccountModel*)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].thirdPlatformLoginUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:account.nickName forKey:@"nickName"];
    [parameter setObject:account.gender forKey:@"gender"];
    [parameter setObject:account.thirdpartyIconUrl forKey:@"thirdpartyIconUrl"];
    [parameter setObject:account.thirdpartyAccount forKey:@"thirdpartyAccount"];
    [parameter setObject:account.thirdpartyPlatform forKey:@"thirdpartyPlatform"];
    [self post:url parameter:parameter callback:callback];
    return tag;
}

+ (NSUInteger)signUp:(AccountModel*)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].signUpUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [self post:url parameter:parameter callback:callback];
    
    return tag;
}

+ (NSUInteger)sendVerCode:(AccountModel *)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString *url = [APIConfiguration sharedAPIConfiguration].sendVerCodeUrl;
    
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:account.mobilePhone forKey:@"mobilePhone"];
    [parameter setObject:account.nationCode forKey:@"nationCode"];
    
    [self post:url parameter:parameter callback:callback];
    return tag;
}


+ (NSUInteger)userLogout:(HttpCallBack*)callback
{
    NSUInteger tag = 0;
    NSString *url = [APIConfiguration sharedAPIConfiguration].logOutUrl;
    [self post:url parameter:nil callback:callback];
    return tag;
}

+ (NSUInteger)forgotPsd:(AccountModel*)account callback:(HttpCallBack *)callback;
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].sendRecoveryEmailUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
//    [parameter setObject:account.username forKey:@"email"];
    [self post:url parameter:parameter callback:callback];
    
    return tag;
}

+ (NSUInteger)setNewPassword:(AccountModel*)account callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSString* url = [APIConfiguration sharedAPIConfiguration].chushiPwdUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:token forKey:@"token"];
    [parameter setObject:account.password forKey:@"password"];
    [self post:url parameter:parameter callback:callback];
    
    return tag;
    
}
+ (NSUInteger)postSelfUserInfo:(AccountModel *)account callback:(HttpCallBack *)callback;
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].putUserInfoUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    
    [parameter setObject:@"avatars" forKey:@"folder"];
    
    if (account.gender) {
        [parameter setObject:account.gender forKey:@"gender"];
    }
    if (account.nickName) {
        [parameter setObject:account.nickName forKey:@"nickName"];
    }
    if (account.area) {
        [parameter setObject:account.area forKey:@"area"];
    }
    if (account.intro) {
        [parameter setObject:account.intro forKey:@"intro"];
    }else{
        
        [parameter setObject:@"" forKey:@"intro"];
    }
    
    tag = [self uploadFile:url file:account.fileModel parameter:parameter callback:callback];
    return tag;
}

+ (NSUInteger)getSelfUserInfoCallback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].putUserInfoUrl;
    tag = [self get:url parameter:nil callback:callback];
    
    return tag;
}

+ (NSUInteger)getTenderSignCallback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getTenderSigUrl;
    tag = [self get:url parameter:nil callback:callback];
    return tag;
}
/** 获取心跳信息配置 */
+ (NSUInteger)getHeartConfigCallback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getHeartConfigUrl;
    tag = [self get:url parameter:nil callback:callback];
    return tag;
}

+ (NSUInteger)postCatImage:(PostFileModel *)postFile callback:(HttpCallBack *)callback ;
{
    NSUInteger tag = 0;
    NSString *url = @"";
    tag = [self uploadFile:url file:postFile parameter:nil callback:callback];
    return tag;
}

+ (NSUInteger)getMyInfoCallback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].getMineUserInfoUrl;
    tag = [self get:url parameter:nil callback:callback];
    
    return tag;
}
  + (NSUInteger)destroyLive:(NSString *)liveShowId callback:(HttpCallBack *)callback
{
    NSUInteger tag = 101;
    NSString* url = [APIConfiguration sharedAPIConfiguration].destroyLiveUrl;
    NSString * urlstr = [NSString stringWithFormat:@"%@%@/destroy",url,liveShowId];
    [self post:urlstr parameter:nil callback:callback];
    return tag;
}


+ (NSUInteger)getUserInfo:(NSString *)userId Callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getUserInfoUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:userId forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}

+ (NSUInteger)getLiveList:(NSInteger)page size:(NSInteger)size callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getLiveListUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:@"pageNo"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)size] forKey:@"pageSize"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}


+ (NSUInteger)getAdCallback:(HttpCallBack *)callback{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getAdListUrl;
//    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    tag = [self get:url parameter:nil callback:callback];
    return tag;
}

+ (NSUInteger)getCourseList:(HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].getCourseListUrl;
    return [self get: url parameter: nil callback: callback];
}

+ (NSUInteger)getUserCourses: (NSString*)userId callback: (HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].userPublishUrl;
    return [self get: url parameter: @{@"hostId": userId} callback: callback];
}

+ (NSUInteger)RDKForRoomId:(NSString *)roomId callBack:(HttpCallBack *)callback{
    
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].RDK;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    AccountModel* account = [AccountManager sharedAccountManager].account;
    if (account) {
        [parameter setObject:account.token forKey:@"token"];
    }
    [parameter setObject:roomId forKey:@"roomId"];
    [self post:url parameter:parameter callback:callback];
    return tag;
    
}

+ (void)sendGift:(GiftModel *)model callback:(HttpCallBack *)callback{
  
  NSString* url = [APIConfiguration sharedAPIConfiguration].sendGiftUrl;
  
  NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
  [parameter setObject:[NSString stringWithFormat:@"%llu",[self GetTimerStamp]] forKey:@"timestamp"];
  NSLog(@"timeStamp--%llu",[self GetTimerStamp]);
  [parameter setObject:model.giftID forKey:@"giftId"];
  [parameter setObject:@"1" forKey:@"num"];
  [parameter setObject:model.roomID forKey:@"roomId"];
  [parameter setObject:[NSString stringWithFormat:@"%ld",(long)model.giftType] forKey:@"giftType"];
  
  NSString *str = [NSString stringWithFormat:@"%@&%@&%@&%@&%@&%ld&%@&%@&%ld&%@&%ld&%f&%@&%ld&%ld",model.userID,model.giftID,model.headImage,model.name,model.giftName,(long)model.currentCount,model.timerStamp,model.giftImage,(long)model.giftType,model.folderName,(long)model.isLocal,model.aspectRatio,model.giftThirdImage,(long)model.playOnceTime,(long)model.repeatTimes];

  [parameter setObject:str forKey:@"remark"];
  
  [self post:url parameter:parameter callback:callback];

}

+ (UInt64)GetTimerStamp{
  
  UInt64 recordTime = [[NSDate date] timeIntervalSince1970]*1000;
  
  return recordTime;
  
}

+ (NSUInteger)createLive:(VLSCreateLiveModel *)createModel callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getCreateLive;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    [parameter setObject:createModel.liveTitle forKey:@"title"];
    if ([createModel.topics length] > 0)
    {
        [parameter setObject:createModel.topics forKey:@"topics"];
    }
//    [parameter setObject:createModel.location forKey:@"location"];
    [parameter setObject:createModel.geoLocaltion forKey:@"geoLocaltion"];
    [parameter setObject:@"YES" forKey:@"OPENRECORDINGKEY"];
    [parameter setObject:@"cover" forKey:@"folder"];
    if (createModel.lessonId)
    {
        [parameter setObject: createModel.lessonId forKey: @"lessonId"];
    }
    tag = [self uploadFile:[NSString stringWithFormat:@"%@?token=%@",url,token] file:createModel.fileModel parameter:parameter callback:callback];
    NSLog(@"task%lu",(unsigned long)tag);
    
    return tag;
}

+ (NSUInteger)getLiveInfo:(NSString* )roomId callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getLiveInfoUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    tag = [self get:[NSString stringWithFormat:@"%@/%@/detail",url,roomId] parameter:parameter callback:callback];
    NSLog(@"task%lu",(unsigned long)tag);
    
    return tag;
}


+ (NSUInteger)getAttentList:(NSInteger)page size:(NSInteger)size userid:(NSInteger)userid callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getAttentListUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)size] forKey:@"pageSize"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:@"pageNum"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)userid] forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
    
}

+ (NSUInteger)getFocusList:(NSInteger)page size:(NSInteger)size userid:(NSInteger)userid  callback:(HttpCallBack *)callback {
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getFocusListUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)size] forKey:@"pageSize"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:@"pageNo"];
//    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)userid] forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}

+ (NSUInteger)getFansList:(NSInteger)page size:(NSInteger)size userid:(NSInteger)userid callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getFanstListUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)size] forKey:@"pageSize"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:@"pageNum"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)userid] forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}



+ (NSInteger)focusToUser:(NSString*)usertoUID callback:(HttpCallBack *)callback;
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].focusUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:usertoUID forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
    
}

+ (NSInteger)cancelfocusToUser:(NSString*)usertoUID callback:(HttpCallBack *)callback;
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].cansalFocusUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    
    [parameter setObject:usertoUID forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}


+(NSInteger)getUserFoucesListWith:(NSString*)userID pageSize:(NSInteger)pagesize pageNum:(NSInteger)pagenum callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].getAttentListUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:userID forKey:@"uID"];
    
    [parameter setObject:@"1000" forKey:@"pageSize"];
    [parameter setObject:@"1" forKey:@"pageNum"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
    
    
}

+(NSInteger)getuserInformation:(NSString*)fromUser Touser:(NSString*)touser callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].getUserInfoUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    
    [parameter setObject:touser forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}


+(NSInteger)getuserBlackList:(NSInteger)page size:(NSInteger)size callback:(HttpCallBack*)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].userBlackList;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)size] forKey:@"pageSize"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:@"pageNum"];
    
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
    
    
}
//加入黑名单
+ (NSInteger)addBlack:(NSString*)usertoUID callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].addBlackList;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:usertoUID forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
    
}
//取消黑名单
+ (NSInteger)cancelBlack:(NSString*)usertoUID callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].cancelBlackList;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    
    [parameter setObject:usertoUID forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
    
}

// 用户跟踪
+ (void)shareTrackingLiveBefore:(NSString *)platform RoomID:(NSString *)roomid callback:(HttpCallBack *)callback
{
    NSString* urlStr = [APIConfiguration sharedAPIConfiguration].postTrackingInformation;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSString *url = [NSString stringWithFormat:@"%@?token=%@",urlStr,token];
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:@"LiveBefore" forKey:@"param2"];
    [parameter setObject:platform forKey:@"param1"];
    [parameter setObject:roomid forKey:@"param3"];
    NSString *userID = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
    [parameter setObject:userID forKey:@"param4"];
    
    [[VLSUserTrackingManager shareManager] trackingShareStatus:@"LiveBefore" platform:platform roomID:roomid];
    [self post:url parameter:parameter requestSerializer:@"json" callback:callback];
}
+ (void)shareTrackingLiveAfter:(NSString *)platform RoomId:(NSString *)roomid callback:(HttpCallBack *)callback
{
    NSString* urlStr = [APIConfiguration sharedAPIConfiguration].postTrackingInformation;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSString *url = [NSString stringWithFormat:@"%@?token=%@",urlStr,token];
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:@"LiveAfter" forKey:@"param2"];
    [parameter setObject:platform forKey:@"param1"];
    [parameter setObject:roomid forKey:@"param3"];
    NSString *userID = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
    [parameter setObject:userID forKey:@"param4"];
    
    [[VLSUserTrackingManager shareManager] trackingShareStatus:@"LiveAfter" platform:platform roomID:roomid];
    
    [self post:url parameter:parameter requestSerializer:@"json" callback:callback];
}
+ (void)shareTrackingLiveProcess:(NSString *)platform RoomId:(NSString *)roomid callback:(HttpCallBack *)callback
{
    NSString* urlStr = [APIConfiguration sharedAPIConfiguration].postTrackingInformation;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSString *url = [NSString stringWithFormat:@"%@?token=%@",urlStr,token];
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:@"LiveProcess" forKey:@"param2"];
    [parameter setObject:platform forKey:@"param1"];
    [parameter setObject:roomid forKey:@"param3"];
    NSString *userID = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
    [parameter setObject:userID forKey:@"param4"];
    
    [[VLSUserTrackingManager shareManager] trackingShareStatus:@"LiveProcess" platform:platform roomID:roomid];
    
    [self post:url parameter:parameter requestSerializer:@"json" callback:callback];
}
+ (void)userActionTracking:(NSString *)action paramDic:(NSDictionary *)dic callback:(HttpCallBack *)callback
{
    NSString* urlStr = [APIConfiguration sharedAPIConfiguration].behaviorTracking;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSString *url = [NSString stringWithFormat:@"%@?token=%@",urlStr,token];
    
    [self post:url parameter:dic requestSerializer:@"json" callback:callback];
}

//++++++++++++++++++++++互动直播＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋
+ (NSUInteger)avInteraction:(VLSAVInteractionModel*)model callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].AVInteractionUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    if (!model.roomId || !model.action || !model.fromAccount || !model.toAccount) {
        return 0;
    }
    [parameter setObject:model.roomId forKey:@"roomId"];
    [parameter setObject:@"GROUP" forKey:@"msgType"];
    [parameter setObject:model.action forKey:@"action"];
    [parameter setObject:model.fromAccount forKey:@"fromAccount"];
    [parameter setObject:model.toAccount forKey:@"toAccount"];
    if (model.layout) {
        [parameter setObject:model.layout forKey:@"layout"];
    }
    [self post:[NSString stringWithFormat:@"%@?token=%@",url,token] parameter:parameter requestSerializer:@"json" callback:callback];
    return tag;
    
}

+ (NSUInteger)avRequestListRoomId:(NSString *)roomId callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].AVLinkRequestList;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:roomId forKey:@"roomId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//是否关注
+ (NSInteger)getRelationShip:(NSString*)usertoUID callback:(HttpCallBack *)callback{
    
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getrelationship;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    
    [parameter setObject:usertoUID forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}

+ (NSUInteger)postLiveInfo:(NSString *)roomId watchNum:(NSInteger)watchNum callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].hertBeatUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:roomId forKey:@"roomId"];
    [parameter setObject:[NSNumber numberWithInteger:watchNum] forKey:@"cNum"];
    [self get:url parameter:parameter callback:callback];
    return tag;
}
//用户举报
+ (NSInteger)postReport:(NSString*)userToID callback:(HttpCallBack*)callback type:(NSString *)type content:(NSString *)content remark:(NSString *)remark
{
    NSUInteger tag = 0;
    //    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSString* url = [APIConfiguration sharedAPIConfiguration].reportUrl;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    //    [parameter setObject:token forKey:@"token"];
    [parameter setObject:userToID forKey:@"uid"];
    if (type!=nil) {
        [parameter setObject:type forKey:@"type"];
        
    }
    if (content !=nil) {
        [parameter setObject:content forKey:@"content"];
        
    }
    if (remark != nil) {
        [parameter setObject:remark forKey:@"remark"];
        
    }
    
    [self post:url parameter:parameter callback:callback];
    
    return tag;
}

+(NSInteger)getRoomId:(NSString*)userID callback:(HttpCallBack*)callback
{
    NSUInteger tag = 0;
    
    
    
    NSString* url = [APIConfiguration sharedAPIConfiguration].getRoomID;
    NSMutableDictionary *parameter=[[NSMutableDictionary alloc] initWithCapacity:0];
    //    [parameter setObject:@"e2a9d5fde129fc1a0d9b37d95f7dab33" forKey:@"token"];
    [parameter setObject:userID forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    
    return tag;
    
}
+(NSInteger)IsAttent:(NSString *)usertoUID callback:(HttpCallBack *)callback{
    
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getUserInfoUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:usertoUID forKey:@"userId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}
+ (NSInteger)postDeviceInformation:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].postDeviceInformation;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    
    DeviceModel *deviceModel = [AccountManager getDeviceInformation];
    
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    
    [parameter setObject:deviceModel.owner forKey:@"owner"];
    [parameter setObject:deviceModel.model forKey:@"model"];
    [parameter setObject:deviceModel.localizedModel forKey:@"localizedModel"];
    [parameter setObject:deviceModel.idfa forKey:@"idfa"];
    [parameter setObject:deviceModel.systemName forKey:@"systemName"];
    [parameter setObject:deviceModel.systemVersion forKey:@"systemVersion"];
    [parameter setObject:deviceModel.uuid forKey:@"uuid"];
    [parameter setObject:deviceModel.ScreenWidth forKey:@"ScreenWidth"];
    [parameter setObject:deviceModel.ScreenHeight forKey:@"ScreenHeight"];
    [parameter setObject:deviceModel.ScreenWidth forKey:@"ScreenWidth"];
    [parameter setObject:deviceModel.carrierName forKey:@"carrierName"];
    [parameter setObject:deviceModel.radioAccessTechnology forKey:@"radioAccessTechnology"];
    [parameter setObject:deviceModel.mac forKey:@"mac"];
    if (deviceModel.locaStr) {
        [parameter setObject:deviceModel.locaStr forKey:@"locaStr"];
    }
    if (deviceModel.GEOLocation) {
        [parameter setObject:deviceModel.GEOLocation forKey:@"GEOLocation"];
    }
    [self post:[NSString stringWithFormat:@"%@?token=%@",url,token] parameter:parameter requestSerializer:@"json" callback:callback];
    
    return tag;
}

+ (NSInteger)postWelcomeInfo:(HttpCallBack *)callback{

    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].postTrackingInformation;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    
    DeviceModel *deviceModel = [AccountManager getDeviceInformation];
    
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    
    [parameter setObject:@"Welcome" forKey:@"Action"];
    [parameter setObject:deviceModel.owner forKey:@"owner"];
    [parameter setObject:deviceModel.model forKey:@"model"];
    [parameter setObject:deviceModel.localizedModel forKey:@"localizedModel"];
    [parameter setObject:deviceModel.idfa forKey:@"idfa"];
    [parameter setObject:deviceModel.systemName forKey:@"systemName"];
    [parameter setObject:deviceModel.systemVersion forKey:@"systemVersion"];
    [parameter setObject:deviceModel.uuid forKey:@"uuid"];
    [parameter setObject:deviceModel.ScreenWidth forKey:@"ScreenWidth"];
    [parameter setObject:deviceModel.ScreenHeight forKey:@"ScreenHeight"];
    [parameter setObject:deviceModel.ScreenWidth forKey:@"ScreenWidth"];
    [parameter setObject:deviceModel.carrierName forKey:@"carrierName"];
    [parameter setObject:deviceModel.radioAccessTechnology forKey:@"radioAccessTechnology"];
    [parameter setObject:deviceModel.mac forKey:@"mac"];
    if ([AccountManager sharedAccountManager].account.uid > 0)
    {
        NSString *str = [NSString stringWithFormat:@"%ld",(long)[AccountManager sharedAccountManager].account.uid];
        [parameter setObject:str forKey:@"userid"];
    }
    
    if (deviceModel.locaStr) {
        [parameter setObject:deviceModel.locaStr forKey:@"locaStr"];
    }
    if (deviceModel.GEOLocation) {
        [parameter setObject:deviceModel.GEOLocation forKey:@"GEOLocation"];
    }
    if (token) {
        [self post:[NSString stringWithFormat:@"%@?token=%@",url,token] parameter:parameter requestSerializer:@"json" callback:callback];
    }else{
    
        [self post:[NSString stringWithFormat:@"%@",url] parameter:parameter requestSerializer:@"json" callback:callback];
    }
    return tag;
}

+ (NSInteger)postLiveNoticeInformation:(NSString*)isReceive callback: (HttpCallBack *)callback{

    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].postLiveNoticeUrl;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:isReceive forKey:@"enableLiveNotice"];
    [self post:[NSString stringWithFormat:@"%@?token=%@",url,token] parameter:parameter requestSerializer:@"json" callback:callback];
    return tag;
}

+ (NSInteger)postLiveInviteInformation:(NSString*)isReceive callback: (HttpCallBack *)callback{
    
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].postLiveNoticeUrl;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:isReceive forKey:@"enableNotFollowInvite"];
    [self post:[NSString stringWithFormat:@"%@?token=%@",url,token] parameter:parameter requestSerializer:@"json" callback:callback];
    return tag;
}

+ (NSInteger)getLiveNoticeInformation:(NSString*)isReceive callback: (HttpCallBack *)callback{
    
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getLiveNoticeUrl;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}
+ (NSInteger)getEndPageInfo:(NSString*)roomID callback:(HttpCallBack*)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getEndPageInfo;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    [parameter setObject:roomID forKey:@"roomId"];
    
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
    

}

+ (NSInteger)getRecommedLists:(HttpCallBack *)callback {
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getRecommendListsUrl;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}


#pragma mark - host application
/**
 *  获取申请状态
 *
 *  @param callback 回调
 */
+ (void)getHostApplicationStatusCallback: (HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].getHostApplicationStatus;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    [self get:url parameter:parameter callback:callback];
    
}

/**
 *  确认申请审批结果
 *
 *  @param applicationId applicationId description
 *  @param callback      应答
 */
+ (void)postHostApplicationConfirm:(NSString *)applicationId callback: (HttpCallBack *)callback
{
    NSString * url = [APIConfiguration sharedAPIConfiguration].applicationConfirm;
    NSString *path = [url stringByReplacingOccurrencesOfString:@"{applicationId}" withString:applicationId];
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    [self post:path parameter:parameter requestSerializer:nil callback:callback];
}

/**
 *  获取主播申请资料
 *
 *  @param callback 应答
 */
+ (void)getHostApplicationCallback:(HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].getHostApplication;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    [self get:url parameter:parameter callback:callback];
}

/**
 *  上传主播申请图片
 *
 *  @param image    imageData
 *  @param callback 应答
 */
+ (void)postHostApplicationImage:(NSData *)image imageName:(NSString*)imageName callback: (HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].postApplicationImage;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:@"imageFile" forKey:@"folder"];

    PostFileModel *model = [[PostFileModel alloc]init];
    model.fileName =  @"imageFile";

    model.fileArray = @[image];
    url = [NSString stringWithFormat:@"%@?token=%@",url,token];
    [self uploadFile:url file:model parameter:parameter callback:callback];
}

/**
 *  提交主播申请资料
 *
 *  @param callback 应答
 */
+ (void)postHostApplication:(NSDictionary *)param callback:(HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].postHostApplication;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionaryWithDictionary:param];
    [parameter setObject:token forKey:@"token"];
    url = [NSString stringWithFormat:@"%@?token=%@&check_mobile=%@",url,token,@"true"];
    [self post:url parameter:parameter requestSerializer:@"json" callback:callback];

}

/**
 *  删除主播申请图片
 *
 *  @param image    imageURL
 *  @param callback 应答
 */
+ (void)deleteHostApplicationImage:(NSString *)imageURL callback: (HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].deleteApplicationImage;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:imageURL forKey:@"url"];
    [parameter setObject:token forKey:@"token"];
    [self delete:url parameter:parameter callback:callback];
}

+ (NSInteger)getAudiuesInfo:(NSString *)roomid size:(NSInteger)size callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getLiveAudiues;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    [parameter setObject:roomid forKey:@"roomId"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)size] forKey:@"size"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}
+ (NSInteger)getWatchNum:(NSString *)roomid callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getWatchNum;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    [parameter setObject:roomid forKey:@"roomId"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
    
}

+ (NSInteger)getAllRelationshipPage:(NSInteger)page size:(NSInteger)size roomid:(NSString *)roomid callback:(HttpCallBack *)callback;
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getAllRelationship;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    [parameter setObject:roomid forKey:@"roomId"];
    [parameter setObject:[NSNumber numberWithInteger:page] forKey:@"pageNum"];
    [parameter setObject:[NSNumber numberWithInteger:size] forKey:@"pageSize"];

    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}
+ (NSInteger)getSearchRelationshipPage:(NSInteger)page size:(NSInteger)size keyword:(NSString *)keyword callback:(HttpCallBack *)callback;
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getSearchRelationship;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    //    [parameter setObject:roomid forKey:@"roomId"];
    if (keyword) {
        [parameter setObject:keyword forKey:@"keyword"];
    }
    [parameter setObject:[NSNumber numberWithInteger:page] forKey:@"pageNo"];
    [parameter setObject:[NSNumber numberWithInteger:size] forKey:@"pageSize"];
    
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}

+ (NSInteger)searchUserWithPage:(NSInteger)page size:(NSInteger)size keyword:(NSString *)keyword callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].searchUserUrl;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    if (keyword) {
        [parameter setObject:keyword forKey:@"keyword"];
    }
    [parameter setObject:[NSNumber numberWithInteger:page] forKey:@"pageNo"];
    [parameter setObject:[NSNumber numberWithInteger:size] forKey:@"pageSize"];
    
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}

+ (void)getIAPProducts: (HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].getIAPProductsUrl;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    [self get:url parameter:parameter callback:callback];
}

+ (void)getMoneyPaymentWithPageNumber: (NSInteger)pageNumber pageSize: (NSInteger)pageSize callback: (HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].getMoneyPaymentUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject: @(pageNumber) forKey: @"pageNum"];
    [parameter setObject: @(pageSize) forKey: @"pageSize"];
    [self get:url parameter:parameter callback:callback];
}

+ (void)getUserMoneyPropertyWithCallback: (HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].getUserMoneyUrl;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [self get:url parameter:parameter callback:callback];
}

+ (void)uploadIAPReceipt: (NSData*)IAPReceiptData callback: (HttpCallBack *)callback
{
    NSString* url = [APIConfiguration sharedAPIConfiguration].uploadIPAReceiptUrl;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    url = [url stringByAppendingFormat: @"?token=%@", token];
    PostFileModel* postData = [[PostFileModel alloc] init];
    postData.fileName = @"receipt";
    postData.fileArray = @[IAPReceiptData];
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject: @"receipt" forKey: @"folder"];
    [self uploadFile: url file: postData parameter: parameter callback: callback];
}
+ (NSInteger)getRankingList:(HttpCallBack*)callback userID:(NSString*)userid
{
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getRankingList;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    [parameter setObject:userid forKey:@"userId"];

    tag = [self get:url parameter:parameter callback:callback];
    return tag;
    
}
+ (NSUInteger)getHostMarkListCallBack:(HttpCallBack *)callback {
    NSUInteger tag = 0;
    NSString* url = [APIConfiguration sharedAPIConfiguration].getHostMarkListUrl;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSMutableDictionary *parameter=[NSMutableDictionary dictionary];
    [parameter setObject:token forKey:@"token"];
    tag = [self get:url parameter:parameter callback:callback];
    return tag;
}


+ (NSInteger)postTrackingInformation:(NSDictionary*)trackingDict callBack:(HttpCallBack *)callback{
    
    NSUInteger tag = 0;
    NSString* urlStr = [APIConfiguration sharedAPIConfiguration].postTrackingInformation;
    NSString *token = [AccountManager sharedAccountManager].account.token;
    NSString *url = nil;
    if (token) {
        url = [NSString stringWithFormat:@"%@?token=%@",urlStr,token];
    }else{
        url = [NSString stringWithFormat:@"%@",urlStr];
    }
    [self post:url parameter:trackingDict requestSerializer:@"json" callback:callback];
    return tag;
}

+ (void)postDeviceActivateTrack: (NSDictionary*)paramDict callBack: (HttpCallBack*)callback
{
    NSString* urlStr = [APIConfiguration sharedAPIConfiguration].trackActivateUrl;
    [self post: urlStr parameter: paramDict requestSerializer:@"json" callback:callback];
}

+ (void)getCourseTiming: (HttpCallBack *)callback
{
    NSString* urlStr = [APIConfiguration sharedAPIConfiguration].courseTimingUrl;
    [self get: urlStr parameter: @{} callback: callback];
}

+ (void)rateLesson: (NSInteger)lessonId rating: (NSInteger)rating content: (NSString*)content  callback: (HttpCallBack *)callback
{
    NSString* urlStr = [[APIConfiguration sharedAPIConfiguration].lessonRatingUrl stringByAppendingFormat: @"?token=%@", [AccountManager sharedAccountManager].account.token];
    if (lessonId == 0 || rating == 0)
    {
        return;
    }
    NSMutableDictionary* paramDict = [@{
                             @"lessonId": @(lessonId),
                             @"rating": @(rating)
                             } mutableCopy];
    if ([content length] > 0)
    {
        paramDict[@"content"] = content;
    }
    
    [self post: urlStr parameter: paramDict requestSerializer:@"json" callback:callback];
}

+ (void)rateLiveShow: (NSString*)roomId rating: (NSInteger)rating content: (NSString*)content callback: (HttpCallBack *)callback
{
    NSString* urlStr = [[APIConfiguration sharedAPIConfiguration].liveShowRatingUrl stringByAppendingFormat: @"?token=%@", [AccountManager sharedAccountManager].account.token];
    if (roomId == 0 || rating == 0)
    {
        return;
    }
    NSMutableDictionary* paramDict = [@{
                                        @"roomId": roomId,
                                        @"rating": @(rating)
                                        } mutableCopy];
    if ([content length] > 0)
    {
        paramDict[@"content"] = content;
    }
    
    [self post: urlStr parameter: paramDict requestSerializer:@"json" callback:callback];
}

+ (void)getChannelList: (HttpCallBack *)callback
{
    NSString* urlStr = [APIConfiguration sharedAPIConfiguration].channelListUrl;
    [self get: urlStr parameter: @{} callback: callback];
}

+ (void)getChannelContents: (NSString*)channelId callback: (HttpCallBack *)callback
{
    NSString* urlStr = [NSString stringWithFormat: [APIConfiguration sharedAPIConfiguration].channelContentsUrl, channelId];
    [self get: urlStr parameter: @{} callback: callback];
}

+ (void)getChannelInfo: (HttpCallBack *)callback
{
    NSString* urlStr = [APIConfiguration sharedAPIConfiguration].channelInfoUrl;
    [self get: urlStr parameter: @{} callback: callback];
}
    
+ (void)getChannelDetails: (NSString*)channelId pageNumber: (NSInteger)pageNumber pageSize: (NSInteger)pageSize callback: (HttpCallBack *)callback;
{
    NSString* urlStr = [NSString stringWithFormat: [APIConfiguration sharedAPIConfiguration].channelDetailsUrl, channelId];
    NSDictionary* params = @{
                            @"pageNo": @(pageNumber),
                            @"pageSize": @(pageSize)
                            };
    [self get: urlStr parameter: params callback: callback];
}
    
+(NSUInteger)put:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    tag = [self put:url parameter:parameter requestSerializer:@"" callback:callback];
    return tag;
}

//新增 put 请求协议
+(NSUInteger)put:(NSString*)url parameter:(NSDictionary*)parameter requestSerializer:(id)ser callback:(HttpCallBack *)callback
{
    NSUInteger tag = 0;
    
    NSString* urlStr = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary* para = [ NSMutableDictionary dictionary];
    [para addEntriesFromDictionary:[self defaultParameter]];
    if (parameter) {
        [para addEntriesFromDictionary:parameter];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSMutableSet<NSString*> *set = [manager.requestSerializer.HTTPMethodsEncodingParametersInURI mutableCopy];
    [set addObject: @"PUT"];
    manager.requestSerializer.HTTPMethodsEncodingParametersInURI = set;
    manager.requestSerializer.timeoutInterval = 30.0f;
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager PUT:urlStr parameters:para success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (callback.doneBlock)
        {
            callback.doneBlock(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (callback.errorBlock)
        {
            callback.errorBlock(error);
        }
    }];
    
    return tag;
}

@end
