//
//  HTTPFacade.h
//  Stacks
//
//  Created by 李雷凯 on 15/10/27.
//  Copyright © 2015年 LK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HttpCallBack.h"
#import "AccountModel.h"
#import "AccountManager.h"
#import "PostFileModel.h"
#import "VLSAVInteractionModel.h"
#import "VLSCreateLiveModel.h"
#import "DeviceModel.h"
#import "GiftModel.h"

@interface HTTPFacade : NSObject

//默认上传参数

+ (NSDictionary*)defaultParameter;

+ (NSUInteger)phoneRegister:(AccountModel*)account callback:(HttpCallBack *)callback;
+ (NSUInteger)setNewPwdAndLogin:(AccountModel*)account callback:(HttpCallBack *)callback;
+ (NSUInteger)phoneExists:(AccountModel*)account callback:(HttpCallBack *)callback;
+ (NSUInteger)verCodeLogin:(AccountModel *)account callback:(HttpCallBack *)callback;
+ (NSUInteger)passwordLogin:(AccountModel*)account callback:(HttpCallBack *)callback;
+ (NSUInteger)thirdPlatformLogin:(AccountModel*)account callback:(HttpCallBack *)callback;
//绑定手机号
+ (NSUInteger)verCodeBind:(AccountModel *)account callback:(HttpCallBack *)callback;

+ (NSUInteger)signUp:(AccountModel *)account callback:(HttpCallBack *)callback;
//验证码
+ (NSUInteger)sendVerCode:(AccountModel *)account callback:(HttpCallBack *)callback;
//登出
+ (NSUInteger)userLogout:(HttpCallBack *)callback;


+ (NSUInteger)forgotPsd:(AccountModel *)account callback:(HttpCallBack *)callback;
+ (NSUInteger)setNewPassword:(AccountModel *)account callback:(HttpCallBack *)callback;
+ (NSUInteger)postCatImage:(PostFileModel *)postFile callback:(HttpCallBack *)callback;
/** 验证当前用户密码 */
+ (NSUInteger)verifyCurrentPwd:(AccountModel*)account callback:(HttpCallBack *)callback;
+ (NSUInteger)verifyVerCode:(AccountModel*)account callback:(HttpCallBack *)callback;

//上传个人信息
+ (NSUInteger)postSelfUserInfo:(AccountModel *)account callback:(HttpCallBack *)callback;
//获取自己的信息
+ (NSUInteger)getSelfUserInfoCallback:(HttpCallBack *)callback;
//获取腾讯签名
+ (NSUInteger)getTenderSignCallback:(HttpCallBack *)callback;
/** 获取心跳信息配置 */
+ (NSUInteger)getHeartConfigCallback:(HttpCallBack *)callback;

//退出直播间
+ (NSUInteger)destroyLive:(NSString *)liveShowId callback:(HttpCallBack *)callback;
//获取自己信息
+ (NSUInteger)getMyInfoCallback:(HttpCallBack *)callback;
//获取用户信息
+ (NSUInteger)getUserInfo:(NSString *)userId Callback:(HttpCallBack *)callback;
//获取直播列表
+ (NSUInteger)getLiveList:(NSInteger)page size:(NSInteger)size callback:(HttpCallBack *)callback;
//获取广告列表
+ (NSUInteger)getAdCallback:(HttpCallBack *)callback;

//获取课程列表
+ (NSUInteger)getCourseList:(HttpCallBack *)callback;

+ (NSUInteger)getUserCourses: (NSString*)userId callback: (HttpCallBack *)callback;

//获取动态key
+ (NSUInteger)RDKForRoomId:(NSString *)roomId callBack:(HttpCallBack *)callback;

//获取直播信息
+ (NSUInteger)getLiveInfo:(NSString* )roomId callback:(HttpCallBack *)callback;
// 获取主播标签列表
+ (NSUInteger)getHostMarkListCallBack:(HttpCallBack *)callback;
//创建直播
+ (NSUInteger)createLive:(VLSCreateLiveModel *)createModel callback:(HttpCallBack *)callback;
//关注列表
+ (NSUInteger)getAttentList:(NSInteger)page size:(NSInteger)size userid:(NSInteger)userid  callback:(HttpCallBack *)callback;
// 获取关注人员的直播情况
+ (NSUInteger)getFocusList:(NSInteger)page size:(NSInteger)size userid:(NSInteger)userid  callback:(HttpCallBack *)callback;
//粉丝列表
+ (NSUInteger)getFansList:(NSInteger)page size:(NSInteger)size userid:(NSInteger)userid callback:(HttpCallBack *)callback;

//关注
+ (NSInteger)focusToUser:(NSString*)usertoUID callback:(HttpCallBack *)callback;
//取消关注
+ (NSInteger)cancelfocusToUser:(NSString*)usertoUID callback:(HttpCallBack *)callback;

//是否关注
+ (NSInteger)getRelationShip:(NSString*)usertoUID callback:(HttpCallBack *)callback;

/************************************************************/


+(NSInteger)getuserInformation:(NSString*)fromUser Touser:(NSString*)touser callback:(HttpCallBack *)callback;
//黑名单列表
+(NSInteger)getuserBlackList:(NSInteger)page size:(NSInteger)size callback:(HttpCallBack*)callback;
//加入黑名单
+ (NSInteger)addBlack:(NSString*)usertoUID callback:(HttpCallBack *)callback;
//取消黑名单
+ (NSInteger)cancelBlack:(NSString*)usertoUID callback:(HttpCallBack *)callback;


/************************用户跟踪*********************************/
// 用户跟踪
+ (void)shareTrackingLiveBefore:(NSString *)platform RoomID:(NSString *)roomid callback:(HttpCallBack *)callback;
+ (void)shareTrackingLiveAfter:(NSString *)platform RoomId:(NSString *)roomid callback:(HttpCallBack *)callback;
+ (void)shareTrackingLiveProcess:(NSString *)platform RoomId:(NSString *)roomid callback:(HttpCallBack *)callback;
+ (void)userActionTracking:(NSString *)action paramDic:(NSDictionary *)dic callback:(HttpCallBack *)callback;

//++++++++++++++++++++++互动直播＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋＋
+ (NSUInteger)avInteraction:(VLSAVInteractionModel*)model callback:(HttpCallBack *)callback;
+ (NSUInteger)avRequestListRoomId:(NSString *)roomId callback:(HttpCallBack *)callback;
//上传心跳信息
+ (NSUInteger)postLiveInfo:(NSString *)roomId watchNum:(NSInteger)watchNum callback:(HttpCallBack *)callback;
//用户举报
+ (NSInteger)postReport:(NSString*)userToID callback:(HttpCallBack*)callback type:(NSString*)type content:(NSString*)content remark:(NSString*)remark;
//获取直播用户id
+(NSInteger)getRoomId:(NSString*)userID callback:(HttpCallBack*)callback;

+ (NSInteger)IsAttent:(NSString*)usertoUID callback:(HttpCallBack *)callback;
//传设备信息
+ (NSInteger)postDeviceInformation:(HttpCallBack *)callback;
//welcome 首次打开app
+ (NSInteger)postWelcomeInfo:(HttpCallBack *)callback;

//上传是否接收直播提醒的信息
+ (NSInteger)postLiveNoticeInformation:(NSString*)isReceive callback: (HttpCallBack *)callback;

+ (NSInteger)postLiveInviteInformation:(NSString*)isReceive callback: (HttpCallBack *)callback;

+ (NSInteger)getLiveNoticeInformation:(NSString*)isReceive callback: (HttpCallBack *)callback;
//获取排行榜列表
+ (NSInteger)getRankingList:(HttpCallBack*)callback userID:(NSString*)userid;
+ (NSInteger)getRecommedLists:(HttpCallBack *)callback;
+ (NSInteger)getAudiuesInfo:(NSString *)roomid size:(NSInteger)size callback:(HttpCallBack *)callback;
+ (NSInteger)getWatchNum:(NSString *)roomid callback:(HttpCallBack *)callback;

+ (NSInteger)getAllRelationshipPage:(NSInteger)page size:(NSInteger)size roomid:(NSString *)roomid callback:(HttpCallBack *)callback;

+ (NSInteger)getEndPageInfo:(NSString*)roomID callback:(HttpCallBack*)callback;
+ (NSInteger)getSearchRelationshipPage:(NSInteger)page size:(NSInteger)size keyword:(NSString *)keyword callback:(HttpCallBack *)callback;

// 根据关键词搜索用户(首页和关注页面的搜索)
+ (NSInteger)searchUserWithPage:(NSInteger)page size:(NSInteger)size keyword:(NSString *)keyword callback:(HttpCallBack *)callback;
/* 获取礼物列表 */
+ (NSInteger)getGiftList:(HttpCallBack *)callback;

/**
 *  发送礼物
 *  @param model 礼物模型
 *  @param callback 回调
 */
+ (void)sendGift:(GiftModel *)model callback:(HttpCallBack *)callback;

#pragma mark - host application
/**
 *  获取申请状态
 *
 *  @param callback 回调
 */
+ (void)getHostApplicationStatusCallback: (HttpCallBack *)callback;

/**
 *  确认申请审批结果
 *
 *  @param applicationId applicationId description
 *  @param callback      应答
 */
+ (void)postHostApplicationConfirm:(NSString *)applicationId callback: (HttpCallBack *)callback;


/**
 *  获取主播申请资料
 *
 *  @param callback 应答
 */
+ (void)getHostApplicationCallback:(HttpCallBack *)callback;

/**
 *  上传主播申请图片
 *
 *  @param image    imageData
 *  @param callback 应答
 */
+ (void)postHostApplicationImage:(NSData *)image imageName:(NSString*)imageName callback: (HttpCallBack *)callback;

/**
 *  获取主播申请资料
 *
 *  @param callback 应答
 */
+ (void)postHostApplication:(NSDictionary *)param callback:(HttpCallBack *)callback;


/**
 *  删除主播申请图片
 *
 *  @param image    imageURL
 *  @param callback 应答
 */
+ (void)deleteHostApplicationImage:(NSString *)imageURL callback: (HttpCallBack *)callback;

#pragma mark - IAP application

+ (void)getIAPProducts: (HttpCallBack *)callback;

+ (void)getMoneyPaymentWithPageNumber: (NSInteger)pageNumber pageSize: (NSInteger)pageSize callback: (HttpCallBack *)callback;

+ (void)getUserMoneyPropertyWithCallback: (HttpCallBack *)callback;

+ (void)uploadIAPReceipt: (NSData*)receiptData callback: (HttpCallBack *)callback;

+ (void)getCourseTiming: (HttpCallBack *)callback;

+ (void)rateLesson: (NSInteger)lessonId rating: (NSInteger)rating content: (NSString*)content callback: (HttpCallBack *)callback;

+ (void)rateLiveShow: (NSString*)roomId rating: (NSInteger)rating content: (NSString*)content callback: (HttpCallBack *)callback;

+ (void)getChannelList: (HttpCallBack *)callback __deprecated;
    
+ (void)getChannelContents: (NSString*)channelId callback: (HttpCallBack *)callback __deprecated;

+ (void)getChannelInfo: (HttpCallBack *)callback;
    
+ (void)getChannelDetails: (NSString*)channelId pageNumber: (NSInteger)pageNumber pageSize: (NSInteger)pageSize callback: (HttpCallBack *)callback;


/* 下载文件 */
+(NSUInteger)download:(NSString *)url folderName:(NSString *)folderName  completion:(void (^)())completion;

/* 下载gif礼物 */
+(NSUInteger)downGifgiftLoad:(NSString *)url folderName:(NSString *)folderName completion:(void (^)())completion;

//上传Tracking日志
+ (NSInteger)postTrackingInformation:(NSDictionary*)trackingDict callBack:(HttpCallBack *)callback;

+ (void)postDeviceActivateTrack: (NSDictionary*)trackingDict callBack: (HttpCallBack*)callback;

+(NSURLSessionDataTask *)post:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback;
    
+(NSURLSessionDataTask *)post:(NSString*)url parameter:(NSDictionary*)parameter requestSerializer:(id)ser callback:(HttpCallBack *)callback;

+(NSUInteger)get:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback;

+(NSUInteger)delete:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback;

//新增 put 请求协议
+(NSUInteger)put:(NSString*)url parameter:(NSDictionary*)parameter callback:(HttpCallBack *)callback;

@end
