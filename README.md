### VLiveShow工程结构分析

	* 开发环境：纯ARC，兼容>=iOS6.0、iPhone横竖屏
	* 目   的：使工程结构更清晰和易于理解
	* 框架模式：MVVM	
	* 创 建 人：Cavan


### Contents

* <font color=#B8860B size=2 face="黑体">AppDelegate</font>  

	该group中放的是AppDelegate和一些系统常量及系统配置文件；
	
* <font color=#B8860B size=2 face="黑体">BaseClass</font>    
	
	一些基类，包括父ViewController和一些公用顶层自定义父类，其他模块的类一般都继承自这里的一些类；

* <font color=#B8860B size=2 face="黑体">CodeClass</font>

	代码区，该 group 中用来做编码开发
	* <font color=#B8860B size=2 face="黑体">Anchor</font> ：主播
	* <font color=#B8860B size=2 face="黑体">Mine</font> ：我的
	* <font color=#B8860B size=2 face="黑体">Showing</font> ：直播
	
		* <font color=#B8860B size=2 face="黑体">Focus</font> ：关注
		* <font color=#B8860B size=2 face="黑体">Hot</font> ：热点
		* <font color=#B8860B size=2 face="黑体">Newest</font> ：最新
		

* <font color=#B8860B size=2 face="黑体">ThirdTools</font>

	用于存放系统中用到的一些第三方工具


* <font color=#B8860B size=2 face="黑体">DataBase</font>

	数据层，封装基于FMDB的sqlite数据库存取和管理，对外提供基于Model层对象的调用接口，封装对数据的存储过程。


* <font color=#B8860B size=2 face="黑体">Handler</font>

	系统业务逻辑层，负责处理系统复杂业务逻辑，上层调用者是ViewController；


* <font color=#B8860B size=2 face="黑体">Network</font>

	网络处理层，封装基于AFNetworking的网络处理层，通过block实现处理结果的回调，上层调用者是Handler层；


* <font color=#B8860B size=2 face="黑体">Storage</font>

	简单数据存储，主要是一些键值对存储及系统外部文件的存取，包括对NSUserDefault和plist存取的封装；


* <font color=#B8860B size=2 face="黑体">Resources</font>

	资源库，包括图片，plist文件等；


* <font color=#B8860B size=2 face="黑体">Categories</font>

	类别，对现有系统类和自定义类的扩展；


* <font color=#B8860B size=2 face="黑体">Others</font>

	其他


